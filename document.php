<?php
	include "config.inc.php";

	$do_post = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($do_post == "content-get") {
		$member_no 	= isset($_COOKIE["member_no"]) ? $mysqli->real_escape_string($_COOKIE["member_no"]) : null;
		$token 			= isset($_COOKIE["token"]) ? $mysqli->real_escape_string($_COOKIE["token"]) : null;
		$sql = "SELECT member_no FROM cmp_imp_member_login_session WHERE member_no = '{$member_no}' AND token = '{$token}' AND is_logout = 0";
		$rs = $mysqli->query($sql);
		if($rs->num_rows == 0) {
?>
<form id="frm" class="form-horizontal">
		<div class="form-group form-group-lg">
				<label for="member_no" class="col-sm-2 col-sm-offset-1 control-label"><i class="fa fa-user"></i></label>
				<div class="col-sm-7">
						<input type="text" class="form-control" id="member_no" name="member_no" maxlength="6" value="" placeholder="เลขที่สมาชิก 6 หลัก" />
				</div>
		</div>
		<div class="form-group form-group-lg">
				<label for="password" class="col-sm-2 col-sm-offset-1 control-label"><i class="fa fa-key"></i></label>
				<div class="col-sm-7">
						<input type="password" class="form-control" id="password" name="password" value="" placeholder="รหัสผ่าน" />
						<h6 class="help-block text-right text-material-light-blue-300"><a href="/dashboard.login.php">ลงทะเบียนสมาชิกคลิกที่นี่</a> | <a href="/dashboard.fwdpwd.php">ลืมหรือไม่ทราบรหัสผ่านคลิกที่นี่</a></h6>
				</div>
		</div>
		 <div class="form-group form-group-lg">
				<div class="col-sm-12 text-center">
						<button type="button" id="btn-login" class="btn btn-lg btn-material-light-blue-300">เข้าสู่ระบบ</button>
				</div>
		</div>
</form>
<?php
			} else {
?>
<form id="frm" class="form-horizontal" method="post"  enctype="multipart/form-data">
		<div class="form-group form-group-lg form-doc_title">
				<label for="member_no" class="col-sm-3 col-sm-offset-0 control-label">หัวข้อเอกสาร</label>
				<div class="col-sm-7">
						<input class="form-control" id="doc_title" name="doc_title" type="text" />
				</div>
		</div>
		<div class="form-group form-group-lg">
				<label for="member_no" class="col-sm-3 col-sm-offset-0 control-label">เลือกเอกสาร</label>
				<div class="col-sm-7">

						<input id="filename_org" name="filename_org" type="hidden" />
						<input id="filename" name="filename" type="hidden" />
						<button type="button" id="btn-choose" class="btn btn-lg btn-success">เลือกเอกสาร</button>
						<div class="help-block help-block-upload"></div>
				</div>
		</div>
		 <div class="form-group form-group-lg">
				<div class="col-sm-12 text-center">
						<button type="button" id="btn-upload" class="btn btn-lg btn-material-light-blue-300">ส่งเอกสาร</button>
				</div>
		</div>
</form>
<?php
			}
			exit();
	}

	if($do_post == "login-set") {
			$member_no = $mysqli->real_escape_string($_POST["member_no"]);
			$password = $mysqli->real_escape_string($_POST["password"]);
			$sql = "SELECT tb1.member_no
								, CASE WHEN tb2.password IS NULL THEN 1 ELSE 0 END AS is_first
							FROM cmp_imp_member AS tb1
							LEFT OUTER JOIN cmp_imp_member_data AS tb2 ON tb1.member_no = tb2.member_no
							WHERE tb1.member_no = '{$member_no}'
								AND CASE WHEN tb2.password IS NULL THEN tb1.card_person ELSE tb2.password END = '{$password}'
							LIMIT 1";
			$rs = $mysqli->query($sql);
			if($rs !==false && $rs->num_rows > 0) {
					$row = $rs->fetch_assoc();
					$token = random_char(16);
					$sql = "INSERT INTO cmp_imp_member_login_session(member_no, token, create_time, create_ip, last_access_time, last_access_ip)
										VALUES('{$member_no}', '{$token}', NOW(), '{$_SERVER["REMOTE_ADDR"]}', NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
					$mysqli->query($sql);
					setcookie("member_no"		, $member_no			, time() + ( 60 * 60 * 8 ), "/", ".upbean.co.th");
					setcookie("token"				, $token					, time() + ( 60 * 60 * 8 ), "/", ".upbean.co.th");
			}
			exit();
	}

	if($do_post == "content-set") {
			$doc_title = $mysqli->real_escape_string($_POST["doc_title"]);
			$idoc_title = $mysqli->real_escape_string($_POST["filename_org"]);

			$sql = "INSERT INTO cmp_document_group(admin_id, member_no, doc_title, publicdate, createdate, updatedate, doc_status)
								VALUES(0, '{$_COOKIE["member_no"]}', '{$doc_title}', NOW(), NOW(), NOW(), 0)";
			$mysqli->query($sql);
			$doc_id = $mysqli->insert_id;

			if(!is_dir("{$_SERVER["DOCUMENT_ROOT"]}/uploads/document/{$doc_id}")){
				mkdir("{$_SERVER["DOCUMENT_ROOT"]}/uploads/document/{$doc_id}", 0777) or die("err");
			}
			@copy("{$_SERVER["DOCUMENT_ROOT"]}/uploads/tmp/{$_POST["filename"]}", "{$_SERVER["DOCUMENT_ROOT"]}/uploads/document/{$doc_id}/{$_POST["filename"]}" );

			$sql = "INSERT INTO cmp_document_item(doc_id, idoc_title, idoc_path, createdate, updatedate, idoc_status)
								VALUES({$doc_id}, '{$idoc_title}', '{$_POST["filename"]}', NOW(), NOW(), 1)";
			$mysqli->query($sql);
			echo "<h3 class='text-center'>ขอบพระคุณค่ะ</h3>";
			echo "<h3 class='text-center'>เอกสารของท่านได้ถูกส่งเข้าสู่ระบบแล้ว</h3>";
			echo "<h3 class='text-center'>ผู้ดูแลระบบจะใช้เวลา 1-2 วัน ในการอนุมัติเอกสารก่อนเผยแพร่</h3>";
			echo "<div class='text-center m-t-xl'><button type='button' class='btn btn-lg btn-default' data-dismiss='modal'>ปิดหน้าต่าง</button></div>";
			exit();
	}

	if($_POST) exit();

	$do_get = isset($_GET["do"]) ? $_GET["do"] : null ;

	if($do_get == "dl") {
			$id = (int)$_GET["id"];
			$member_no = isset($_COOKIE["member_no"]) ? $_COOKIE["member_no"] : "" ;
			$sql = "INSERT INTO cmp_document_item_dl(idoc_id, member_no, dl_time, dl_ip)
								VALUES({$id}, '{$member_no}', NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
			$mysqli->query($sql);

			$sql = "SELECT doc_id, idoc_title, idoc_path
							FROM cmp_document_item
							WHERE idoc_id = {$id}";
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc();

			$doc_id = (int)$row["doc_id"];
			$file = "{$_SERVER["DOCUMENT_ROOT"]}/uploads/document/{$doc_id}/{$row["idoc_path"]}";

			if(isset($row["idoc_path"]{0}) && file_exists($file)) {
					//echo "<br />".$row["idoc_path"];
					$fakeFileName= $row["idoc_title"];
					$realFileName = $row["idoc_path"];
					$fp = fopen($file, 'rb');
					header("Content-Type: application/octet-stream");
					header("Content-Disposition: attachment; filename={$fakeFileName}");
					header("Content-Length: " . filesize($file));
					fpassthru($fp);
			}

			exit();
	}

	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<style>
	.right-top-news { border : 1px solid #f4f4f4 ;   padding: 10px; }
	.right-top-news>.line {border-bottom : 3px solid #888; }
	.right-top-news>.list>.line, .line {border-bottom : 0px solid #888; }
	.download-list > hr.line:last-child {
			border-top : none;
			border-bottom : none;
	}
	hr { border-top: 1px solid #ccc !important; }
	.download-list ul { list-style: none; }
	.download-list ul li { margin-bottom: 10px; }
	.right-top-news .list {padding-top: 20px;}
	.right-top-news .list .item {margin-bottom : 10px  ; /* color :#807f80 */ color : #CCC }
	.right-top-news .list .item h5{margin : 0 ;margin-bottom : 10px;  font-size: 18px; }
	.right-top-news .list .item h5 a{ color: #838384;   }
	.right-top-news .list .item .item-detail .viewcount{ float:left ; width: 50% }
	.right-top-news .list .item .item-detail .shardcount{ float:left ; width: 50% }
	.right-top-news .list .item .view {overflow:hidden;}
	.row.item { overflow: hidden; }
	.dl-stat-no {
			background-color: #888;
			width: 40px;
			line-height: 40px;
			color: #FFF;
			font-weight: bold;
	}

	.modal-dialog {
		/* height: 80% !important; */
		padding-top:10%;
	}

	.modal-content {
		height: 100% !important;
		overflow:visible;
	}

	.modal-body {
		height: 80%;
		overflow: auto;
	}
	.download-info {
			color: #838384;
	}

	ul.document-filter {
			padding: 0px;
			margin: 0px;
	}
	ul.document-filter li {
			display: inline-block;
			list-style: none;
			border-right: 1px solid #888;
			padding: 0 5px;
	}
	ul.document-filter li:first-child {
			padding-left: 0px;
	}
	ul.document-filter li:last-child {
			border-right: 0px solid #888;
	}
	.m-r-xxl {margin-right:10px;}
	
	.content-page-document .download-list h4 {font-size:16px }
	.document-filter {margin-bottom : 10px; }
</style>
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	$smarty->assign("subject", "ดาวน์โหลดเอกสาร");
	$smarty->assign("breadcrumb", array("ดาวน์โหลดเอกสาร" => "document.php" ));
	$smarty->assign("title", "ดาวน์โหลดเอกสาร") ;
	ob_start();
?>
<script src="/scripts/blueimp.fileupload/9.9.3/js/vendor/jquery.ui.widget.js"></script>
<script src="/scripts/blueimp.fileupload/9.9.3/js/jquery.iframe-transport.js"></script>
<script src="/scripts/blueimp.fileupload/9.9.3/js/jquery.fileupload.js"></script>
<script>
		$(document).ready(function() {

				$("body").on("click", ".btn-upload", function() {
						content_get();
				});

				$("body").on("click", "#btn-login", function() {
					$(".modal").modal("hide");
					$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
										"do" 				: "login-set"
									, "member_no" : $("#member_no").val()
									, "password" 	: $("#password").val()
									, "_time" 		: Math.random()
							}
							, async: false
							, success: function(data) {
									content_get();
							}
					});
				});

				$("body").on("click", "#btn-upload", function() {
						var is_error = false;
						$(".form-doc_title").removeClass("has-error");
						if($("#doc_title").val() == "") {
								$(".form-doc_title").addClass("has-error");
								$(".form-doc_title").focus();
								is_error = true;
						}
						//filename_org
						if(!is_error) {
								$.ajax({
										type: "POST"
										, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
										, data: {
													"do" 						: "content-set"
												, "doc_title" 		: $("#doc_title").val()
												, "filename_org" 	: $("#filename_org").val()
												, "filename" 			: $("#filename").val()
												, "_time" 				: Math.random()
										}
										, async: false
										, success: function(data) {
												$(".modal").find(".modal-body").html(data);
												$(".modal").modal({backdrop: 'static'});
										}
								});
						}
				});

				function content_get() {
					$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
										"do" 		: "content-get"
									, "_time" : Math.random()
							}
							, async: false
							, success: function(data) {
									$(".modal").find(".modal-body").html(data);
									$(".modal").modal({backdrop: 'static'});
							}
					});
				}

				/*
				 *	@ File upload
				 *****************************************************/
				$('#fileupload').fileupload({
						url: "document.upload.php",
						dataType: 'json',
						done: function (e, data) {
								console.log(data);
								$.each(data.result.files, function (index, file) {
										//console.log(file.name);
										//console.log(data.files[index].name);
										$("#filename_org").val(data.files[index].name);
										$("#filename").val(file.name);
										$(".help-block-upload").html(data.files[index].name);
								});
								$("#btn-choose").removeAttr("disabled");
						},
						progressall: function (e, data) {
								$(".help-block").html("");
								$("#btn-choose").attr("disabled", "disabled");
								var progress = parseInt(data.loaded / data.total * 100, 10);
								console.log(progress);
								$(".help-block-upload").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;อัพโหลด " + progress + "%");
						}
				}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');


				$("body").on("click", "#btn-choose", function() {
						$('#fileupload').click();
				});
				 
		});
</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();
?>
<section class="section-main content-page content-page-document " >
	<div class="container" >
		<div class="row">
				<div class="col-md-9 download-list">
						<!-- 
						<div class="text-right hide">
								<button class="btn btn-info btn-upload" type="button"><i class="fa fa-upload"></i>&nbsp;อัพโหลดเอกสาร</button>
						</div>
						-->
						<div class="row">
								<div class="col-md-12">
										<ul class="document-filter">
												<li><a href="?">แสดงทั้งหมด</a></li>
												<?php
														$sql = "SELECT cat_id, cat_name
																		FROM cmp_document_cat
																		ORDER BY CONVERT(cat_name USING TIS620)";
														$rs_f = $mysqli->query($sql);
														while(($row_f = $rs_f->fetch_assoc())) {
																$cid = isset($_GET["cid"]) ? $_GET["cid"] : null ;
																$selected = $row_f["cat_id"] == $cid ? " selected" : "" ;
																echo "<li><a href='?cid={$row_f["cat_id"]}'>{$row_f["cat_name"]}</a>";
														}
												?>
										</ul>
								</div>
						</div>
						<?php
								if(!isset($_GET["id"])) {
										$where = "";
										if(isset($_GET["cid"])) {
												$where = " AND tb1.cat_id = '{$_GET["cid"]}' ";
										}
										$sql = "SELECT tb1.doc_id, tb1.doc_title, tb1.publicdate, tb2.admin_name, COUNT(tb4.dl_id) AS num_dl
														, tb5.cat_name
														FROM cmp_document_group AS tb1
														LEFT OUTER JOIN cmp_admin AS tb2 ON tb1.admin_id = tb2.admin_id
														LEFT OUTER JOIN cmp_document_item AS tb3 ON tb1.doc_id = tb3.doc_id
														LEFT OUTER JOIN cmp_document_item_dl AS tb4 ON tb3.idoc_id = tb4.idoc_id
														LEFT OUTER JOIN cmp_document_cat AS tb5 ON tb1.cat_id = tb5.cat_id
														WHERE tb1.publicdate <= NOW()
															AND doc_status = 1
															{$where}
														GROUP BY tb1.doc_id, tb1.doc_title, tb1.publicdate, tb2.admin_name
														ORDER BY tb1.doc_seq ASC, CONVERT(tb1.doc_title USING TIS620) ASC";
										$rs = $mysqli->query($sql);
										while(($row = $rs->fetch_assoc())) {
												$num_dl = number_format($row["num_dl"], 0);
						?>
						<div class="row">
								<div class="col-sm-10">
										<a href="?id=<?php echo $row["doc_id"]; ?>"><h4><?php echo $row["doc_title"]; ?></h4></a>
										<div class="row download-info">
											<div class="col-md-12" >
												<span class="col-xs-3"><?php echo !empty($row["cat_name"]) ? "หมวด {$row["cat_name"]}" : "" ; ?></span>
												<span class="col-xs-3">โดย <?php echo $row["admin_name"]; ?></span>
												<span class="col-xs-6"><i class="fa fa-clock-o"></i>&nbsp;เมื่อ <?php echo mysqldate2thaidate($row["publicdate"], true, true); ?></span>
											</div>
											 
										</div>
								</div>
								<div class="col-sm-2 text-right m-t-md download-info"><i class='fa fa-download'></i>&nbsp;<?php echo $num_dl; ?> ครั้ง</div>
						</div>
						<hr class="m-v-xs line" />
						<?php
										}
								} else {
										$id = (int)$_GET["id"];
										$sql = "SELECT tb1.doc_id, tb1.doc_title, tb1.publicdate, tb2.admin_name, tb5.cat_name
														FROM cmp_document_group AS tb1
														LEFT OUTER JOIN cmp_admin AS tb2 ON tb1.admin_id = tb2.admin_id
														LEFT OUTER JOIN cmp_document_cat AS tb5 ON tb1.cat_id = tb5.cat_id
														WHERE tb1.doc_id = {$id}
														";
										$rs = $mysqli->query($sql);
										$row = $rs->fetch_assoc();
						?>
						<div class="row">
								<div class="col-sm-10">
										<a href="?id=<?php echo $row["doc_id"]; ?>"><h4><?php echo $row["doc_title"]; ?></h4></a>
										<div class="row download-info">
												<div class="col-md-12">
														<?php echo !empty($row["cat_name"]) ? "<div class='pull-left m-r-xxl'>หมวด {$row["cat_name"]}</div>" : "" ; ?>
														<div class="pull-left m-r-xxl">โดย <?php echo $row["admin_name"]; ?></div>
														<div class="pull-left"><i class="fa fa-clock-o"></i>&nbsp;เมื่อ <?php echo mysqldate2thaidate($row["publicdate"], true, true); ?></div>
														<div class="clearfix"></div>
												</div>
										</div>
								</div>
						</div>
						<hr class="m-v-xs" />
						<h4  >ดาวน์โหลดเอกสารแนบ</h4>
						<ul>
								<?php
										$sql = "SELECT tb1.idoc_id, tb1.idoc_title, COUNT(tb2.dl_id) AS num_dl
														FROM cmp_document_item AS tb1
														LEFT OUTER JOIN cmp_document_item_dl AS tb2 ON tb1.idoc_id = tb2.idoc_id
														WHERE tb1.doc_id = {$id}
															AND tb1.idoc_status = 1
														GROUP BY tb1.idoc_id, tb1.idoc_title
														ORDER BY createdate";
										$rs_c = $mysqli->query($sql);
										while(($row_c = $rs_c->fetch_assoc())) {
												$num_dl = number_format($row_c["num_dl"], 0);
												echo "<li class='row'>
																	<div class='col-9'><a href='?do=dl&id={$row_c["idoc_id"]}' target='_blank'><i class='fa fa-download'></i>&nbsp;{$row_c["idoc_title"]}</a></div>
																	<div class='col-3 text-right download-info'><i class='fa fa-download'></i>&nbsp;{$num_dl} ครั้ง</div>
															</li>";
										}
								?>
						</ul>
						<?php
								}
						?>
				</div>
				<div class="col-md-3">


						<div class="right-top-news m-t-xs">
								<h4 style="font-size: 16px;padding-top: 20px;" >ดาวน์โหลดมากที่สุด ในรอบ 7 วัน</h3>
								<hr class="line" />
								<div class="list">
										<?php
												$sql = "SELECT tb1.doc_id, tb1.doc_title, COUNT(tb4.dl_id) AS num_dl, tb5.cat_name
																FROM cmp_document_group AS tb1
																INNER JOIN cmp_document_item AS tb3 ON tb1.doc_id = tb3.doc_id
																INNER JOIN (SELECT * FROM cmp_document_item_dl WHERE DATEDIFF(NOW(), dl_time) <=7) AS tb4 ON tb3.idoc_id = tb4.idoc_id
																LEFT OUTER JOIN cmp_document_cat AS tb5 ON tb1.cat_id = tb5.cat_id
																WHERE tb1.publicdate <= NOW()
																GROUP BY tb1.doc_id, tb1.doc_title, tb5.cat_name
																ORDER BY num_dl DESC
																LIMIT 5";
												$rs = $mysqli->query($sql);
												$i = 1;
												while(($row = $rs->fetch_assoc())) {
										?>
										<div class="row item">
												<div class="col-md-12">
														<div class="dl-stat-no img-circle text-center pull-left m-r-xxs"><?php echo $i++; ?></div>
														<div class="pull-left">
																<a href="?id=<?php echo $row["doc_id"]; ?>" class="download-info"><?php echo $row["doc_title"]; ?></a>
																<div><?php echo !empty($row["cat_name"]) ? "หมวด {$row["cat_name"]}" : "" ; ?></div>
														</div>
														<div class="clearfix"></div>
												</div>
										</div>
										<hr class="line" />
										<?php } ?>
								</div>
						</div>

				</div>
		</div>
		<!-- 
		<form class="form-horizontal" action="" method="post" >
			<input id="fileupload" type="file" name="files[]" class="hide" />
		</form>
		
		<div class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h2 class="modal-title text-center" id="myModalLabel">อัพโหลดเอกสารดาวน์โหลด</h2>
			</div>
			<div class="modal-body">
			  </div>
			  <div class="modal-footer hide">
				<button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
			  </div>
			</div>
		  </div>
		</div>
		-->
	</div>
</section>

<?php
		$content = ob_get_contents();
		ob_end_clean();
		$smarty->assign("content", $content);
		$smarty->display(THEME.".tpl");
