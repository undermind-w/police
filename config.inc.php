<?php
	session_start();
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	define("PATH",$_SERVER["DOCUMENT_ROOT"]) ;

	function curPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}

	define("TITLE"," ") ;
	define("KEYWORD","") ;
	define("DESC","") ;
	define("AUTHOR","") ;

	define("THEME","frontend.policehospital");


	require PATH . "/class/connect.inc.php" ;
	require PATH . "/class/function.inc.php" ;
	require PATH . "/class/smarty.inc.php";
	require PATH . "/class/loguser.class.php";
	
	$log_user = NEW CmpActionLogUser;

	$num_stats = $log_user->get_stats(); //เก็บสถิติการเข้าชมเว็บ
	$num_stats['date_start'] = mysqldate2thaidate($num_stats['date_start'] , "short" , false );
	$smarty->assign("num_stats", $num_stats);

	$base_url = "/" ;
	$smarty->assign("base_url" , $base_url) ;


	$sql = "SELECT * FROM cmp_link WHERE link_status = '1' " ;
	$rs = $mysqli->query($sql);

	$elinks = array();
	while($item = $rs->fetch_assoc()){
		$elinks[] = $item ;
	}

	$smarty->assign("elinks" , $elinks) ;
	$menu1 = array();

	$sql = "SELECT * FROM cmp_menu WHERE menu_status = 1  AND menu_position = 1 AND menu_root = 0 ORDER BY menu_seq ASC " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

	while($item = $rs->fetch_assoc()){
		$sql  = "SELECT * FROM cmp_menu WHERE menu_status = 1 AND menu_root = '{$item["menu_id"]}' ORDER BY menu_seq ASC  " ;
		$rs2 = $mysqli->query($sql);
		$items = array();
		echo $mysqli->error ;
		while($item2 = $rs2->fetch_assoc()){
			$items[] = $item2 ;
		}
		$item["items"] = $items ;
		$menu1["header"][] = $item ;
	}


	$sql = "SELECT * FROM cmp_menu WHERE menu_status = 1  AND menu_position = 2 AND menu_root = 0 ORDER BY menu_seq ASC " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

	while($item = $rs->fetch_assoc()){
		$menu1["footer1"][] = $item ;
	}
	
	$sql = "SELECT * FROM cmp_menu WHERE menu_status = 1  AND menu_position = 3 AND menu_root = 0 ORDER BY menu_seq ASC " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

	while($item = $rs->fetch_assoc()){
		$menu1["footer2"][] = $item ;
	}
	
	$sql = "SELECT * FROM cmp_menu WHERE menu_status = 1  AND menu_position = 4 AND menu_root = 0 ORDER BY menu_seq ASC " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

	while($item = $rs->fetch_assoc()){
		$menu1["footer3"][] = $item ;
	}
	
	

	$smarty->assign("menu1" , $menu1) ;

	$sql = "SELECT * FROM cmp_website WHERE st_id = 1  " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;
	$website = $rs->fetch_assoc();

	$smarty->assign("website" , $website) ;
	$smarty->assign("v" , date('YmdHis')) ;

