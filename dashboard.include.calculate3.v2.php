<?php
    // // เงินกู้สามัญ
    // $sql = "SELECT *, TIMESTAMPDIFF(YEAR, memdtm, CURDATE()) AS memdtm_age
    //         FROM cmp_imp_member
    //         WHERE member_no = '{$_COOKIE["member_no"]}'";
    // $rs = $mysqli->query($sql);
    // $member = $rs->fetch_assoc();
    // $day   = date('d');
    // $month = date('n');
    // $year  = date('Y') + 543;
?>

<div class="panel">
	<div class="panel-heading subject">
		<div class="">
			<div class="pull-left">
				<span class="fa-stack fa-2x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-calculator fa-stack-1x subject"></i>
				</span>
			</div>
			<div class="pull-left m-l-xxs">
				<h3>คำนวณสินเชื่อ</h3>
			</div>
			<div class="pull-right badget"></div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="panel-body">
		<form class="form-horizontal">

			<div class="form-group">
				<span class="col-sm-4 control-label">วงเงินกู้</span>
				<div class="col-sm-8">
                    <input type="text" id="loan" name="loan" class="form-control form-loan number-only inline-block"/>
					<span class="inline-block control-label m-l-xs"> บาท </span>
				</div>
			</div>

			<div class="form-group">
				<span class="col-sm-4 control-label">อัตราดอกเบี้ย</span>
				<div class="col-sm-8">
            <input type="nuumber" id="interest" name="interest"  class="form-control form-loan number-only inline-block" step="0.01" />
					<span class="inline-block control-label m-l-xs"> % </span>
				</div>
			</div>

      <div class="form-group">
				<span class="col-sm-4 control-label"> จำนวน </span>
				<div class="col-sm-8">
            <select id="period_type" name="period_type" class="form-control  inline-block" style="width:242px;">
              <option value="1"> งวดที่ต้องการผ่อน </option>
              <option value="2"> เงินที่ต้องการผ่อนต่องวด </option>
            </select>
            <input type="nuumber" id="period" name="period"  class="form-control form-loan number-only inline-block" />
          <span class="inline-block control-label m-l-xs" id="type_period" > % </span>
				</div>
			</div>

			<div class="form-group">
				<span class="col-sm-4 control-label">วันที่เริ่มชำระ</span>
				<div class="col-sm-8">
          <input type="text" id="day"   name="day"    class="form-control form-loan number-only inline-block" value="<?php echo "$day";?>" />

          <select name="month" id="month" class="form-control inline-block" style="width:120px;">
            <?php $months = array(1=>"ม.ค.",2=>"ก.พ.",3=>"มี.ค.",4=>"เม.ย.",5=>"พ.ค.",6=>"มิ.ย.",7=>"ก.ค.",8=>"ส.ค.",9=>"ก.ย.",10=>"ต.ค.",11=>"พ.ย.",12=>"ธ.ค.");?>
            <?php foreach ($months as $key => $value) {
                    if ($key == $month) { ?>
                        <option value="<?php echo $key?>" selected><?php echo $value?></option>
                    <?php } else { ?>
                        <option value="<?php echo $key?>"><?php echo $value?></option>
                    <?php } ?>
           <?php } ?>
        </select>

          <input type="text" id="year"  name="year"   class="form-control form-loan number-only inline-block" value="<?php echo "$year";?>"/>
				</div>
			</div>

      <div class="form-group">
        <span class="col-sm-4 control-label">ประเภทการชำระเงิน</span>
        <div class="col-sm-8">
          <select id="pay_type" name="pay_type" class="form-control inline-block" style="width:220px;">
            <option value="1">ชำระต้นเท่ากันทุกงวด</option>
            <option value="2">ชำระยอดเท่ากันทุกงวด</option>
          </select>
        </div>
      </div>

	<div class="form-group hidden-print">
		<div class="col-xs-12 text-center p-v-xxl"><button type="button" class="btn btn-lg subject btn-calculate">คำนวณ</button></div>
	</div>

			<div id="result_wrap"></div>

		</form>
	</div>
</div>

  <div class="modal fade" id="alertLaon" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header subject">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">
            <h4>แจ้งเตือน</h4>
          </h4>
        </div>
        <div class="modal-body">
          <p style="font-size:18px;">กรุณาใส่จำนวนวงเงินกู้ด้วยครับ</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="alertInterest" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header subject">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
              <h4>แจ้งเตือน</h4>
            </h4>
          </div>
          <div class="modal-body">
            <p style="font-size:18px;">กรุณาใส่อัตราดอกเบี้ยด้วยครับ</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="alertPeriod" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header subject">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">
                <h4>แจ้งเตือน</h4>
              </h4>
            </div>
            <div class="modal-body">
              <p style="font-size:18px;">กรุณาใส่จำนวนงวด หรือ จำนวนเงินด้วยครับ</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="alertPeriod" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header subject">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                  <h4>แจ้งเตือน</h4>
                </h4>
              </div>
              <div class="modal-body">
                <p style="font-size:18px;">กรุณาใส่จำนวนงวด หรือ จำนวนเงินด้วยครับ</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="alertDay" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header subject">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                  <h4>แจ้งเตือน</h4>
                </h4>
              </div>
              <div class="modal-body">
                <p style="font-size:18px;">กรุณาใส่ วัน ที่ต้องการคำนวณด้วยครับ</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="alertMonth" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header subject">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                  <h4>แจ้งเตือน</h4>
                </h4>
              </div>
              <div class="modal-body">
                <p style="font-size:18px;">กรุณาเลือก เดือน ที่ต้องการคำนวณด้วยครับ</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="alertYear" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header subject">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                  <h4>แจ้งเตือน</h4>
                </h4>
              </div>
              <div class="modal-body">
                <p style="font-size:18px;">กรุณาใส่ ปี ที่ต้องการคำนวณด้วยครับ</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

<style>
	.form-loan {
		width: 120px !important;
		text-align: center;
	}
    .modal-header-info {
        color:#fff;
        padding:9px 15px;
        border-bottom:1px solid #eee;
        background-color: #66cc00;
        -webkit-border-top-left-radius: 5px;
        -webkit-border-top-right-radius: 5px;
        -moz-border-radius-topleft: 5px;
        -moz-border-radius-topright: 5px;
         border-top-left-radius: 5px;
         border-top-right-radius: 5px;
    }
    .modal-dialog {
      margin-top: 250px;
      width: 300px;
    }
</style>

<script src="/scripts/jquery.number_format.js"></script>
<script>
	Number.prototype.format = function(n, x, s, c) {
	    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
		    num = this.toFixed(Math.max(0, ~~n));
	    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	};

	$('document').ready(function() {
		$("body").on("click", ".btn-calculate", function(e){
         if($.trim($('#loan').val()) == ''){
            $('#alertLaon').modal("show");
         } else if($.trim($('#interest').val()) == '') {
            $('#alertInterest').modal("show");
         } else if($.trim($('#period').val()) == '') {
            $('#alertPeriod').modal("show");
         } else if($.trim($('#day').val()) == '') {
            $('#alertDay').modal("show");
         } else if($.trim($('#month').val()) == '') {
            $('#alertMonth').modal("show");
         }else if($.trim($('#year').val()) == '') {
            $('#alertYear').modal("show");
         } else {
            cal();
         }
    });

		function cal(){
      $.ajax({
				type: "POST"
				, url: "dashboard.include.calculate3.v2.ajax.php"
				, data: {
					"ajax" : 1
					, "do" : "cal"
					, "loan" : $("#loan").val().replace(/,/g, "")
					, "pay_period" : $("#pay_period").val()
					, "pay_type" : $("#pay_type").val()
					, "day" : $("#day").val()
          , "month" : $("#month").val()
          , "year" : $("#year").val()
          , "period_type" : $("#period_type").val()
          , "period" : $("#period").val()
					, "interest" : $("#interest").val()
					, "_time" : Math.random()
				}
				, async: true
				, success: function(msg) {
					$("#result_wrap").html(msg);
				}
			});
		}

    $("#select_interest option").filter(function() {
      return $(this).val() == $("#interest").val();
    }).attr('selected', true);

    $("#select_interest").live("change", function() {
        $("#interest").val($(this).find("option:selected").attr("value"));
    });

    $('#loan').keyup(function(event) {
      if(event.which >= 37 && event.which <= 40) return;
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });

    $( "#period_type" )
    .change(function () {
      var str = " ";
      $( "#period_type option:selected" ).each(function() {
        if ($(this).val() == 1) {
          str += "งวด";
        } else {
          str += "บาท";
        }
      });
      $( "#type_period" ).text( str );
    })
    .change();

	});
</script>
