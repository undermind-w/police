<?php
	
	include "config.inc.php";
 	$add_log = $log_user->add_log(); //เก็บสถิติการเข้าชมเว็บ
	
	
	
	
	$fullurl = "http://{$_SERVER["SERVER_NAME"]}{$_SERVER["REQUEST_URI"]}";
	if(@$_POST["do"] == "get-social-count") {
		echo @getFacebooks(@$_POST["url"]) + @getTweets(@$_POST["url"]) + @getPlus1(@$_POST["url"]) ;
		exit();
	}
	
	$first = !empty($_GET["first"]) ? true : false ; 
	if(!$first){
		$month = (int) date("m") ; 
		$day = (int) date("d") ; 
		$sql = "SELECT * FROM cmp_importantday WHERE MONTH(publicdate) = {$month} AND DAY(publicdate) = {$day} LIMIT 1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$intro = $rs->fetch_assoc();
		if(!empty($intro)){
			echo "<script>window.location.href = '/intro.php?index={$intro["day_id"]}' </script>" ; 
		}
	}
	
	ob_start();
?>
 
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	<script>
	$(function(){
		$('#carouselMain').carousel( ) ; 
		$('#committeeCarousel').carousel( ) ; 
		
		$('#formLogin').ajaxForm(function(data) { 
			if(data.status == 1){
				window.location.href = data.url ;  
			}else{
				Swal.fire({
						icon: 'error',
						title: 'มีข้อผิดพลาด',
						text:  data.msg ,
						confirmButtonText: 'ตกลง',
					}) ; 
			}
		}); 
	})
	</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	$smarty->assign("title", "สหกรณ์ออมทรัพย์โรงพยาบาลตำรวจ จำกัดs" );

	ob_start();
	
	$slide = ob_get_contents();
	ob_end_clean();
	$smarty->assign("slide", $slide);


	ob_start();
?>

<section class="section-slide" style="text-align: center;" >
	<div id="carouselMain" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
<?php 
			$sql = "SELECT COUNT(slide_id) _c FROM cmp_slide WHERE  slide_status = 1 ORDER BY slide_seq ASC  LIMIT 0 , 25  " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ; 
			$row = $rs->fetch_assoc() ;
			for($i = 0 ; $i < $row["_c"] ; $i ++ ){
?>
			<li data-target="#carouselMain" data-slide-to="<?php echo $i ?>" <?php echo $i == 0 ? "class=\"active\"" : "" ;?>></li>
<?php 
			}
?>
 		</ol>
		<div class="carousel-inner">
<?php
		$sql = "SELECT * FROM cmp_slide WHERE  slide_status = 1 ORDER BY slide_seq ASC  LIMIT 0 , 25  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$i = 0 ; 
		while($item = $rs->fetch_assoc()){
			$i ++ ; 

?>
				<div class="carousel-item <?php echo $i == 1 ? "active" : "" ;  ?> ">
					
					<div class="item" <?php echo empty($item["slide_background"]) ? "" : "style=\"background-image:url('/uploads/contents/{$item["slide_background"]}') ;background-repeat:x-repeat ;background-position:center center \""  ;  ?> >
						<div class="container" >
							<div class="row" >
								<div class="col-md-12" >
									<a target="_blank" href="<?php echo empty($item["slide_url"]) ? "#" : $item["slide_url"] ?>" >
										<img style="max-width:100%" src="/uploads/contents/<?php echo $item["slide_picture"] ?>" />
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
<?php 
	} 
?>
		</div>
		 
	</div>
</section>

<section class="section-search" >
	<div class="container" >
		<div class="row" >
			<div class="col-md-12" >
				<form class="form-search" method="get" action="/search.php" target="_blank" >
					<input type="text"  name="q" value=""  placeholder="พิมพ์สิ่งที่ต้องการค้นหา" />
					<button id="btnSearch" class="btn btn-default btn-search" >ค้นหา</button>
				</form>
			</div>
		</div>
	</div>
</section>
<?php 
	$navitems = array() ; 
	
	
	$navitems[0]["title"] = "คู่มือสมาชิก"  ; 
	$navitems[0]["icon"] = "icon-nav-1.png"  ; 
	$navitems[0]["url"] = "/page.php?pid=22"  ; 
	
	$navitems[1]["title"] = "ดาวน์โหลดคู่มือและเอกสาร"  ; 
	$navitems[1]["icon"] = "icon-nav-2.png"  ; 
	$navitems[1]["url"] = "/document.php"  ; 
	
	$navitems[2]["title"] = "คำนวณเงินผ่อนชำระ"  ; 
	$navitems[2]["icon"] = "icon-nav-3.png"  ; 
	$navitems[2]["url"] = "/dashboard.calculate.loan.php?type=1"  ; 
	
	$navitems[3]["title"] = "คู่มือสมาชิกออนไลน์"  ; 
	$navitems[3]["icon"] = "icon-nav-4.png"  ; 
	$navitems[3]["url"] = "/page.php?pid=20"  ; 
	
	$navitems[4]["title"] = "แจ้งปัญหา/ข้อเสนอแนะ/ร้องเรียน"  ; 
	$navitems[4]["icon"] = "icon-nav-5.png"  ; 
	$navitems[4]["url"] = "/contact.fancybox.php"  ; 
	
	$navitems[5]["title"] = "วารสารสหกรณ์"  ; 
	$navitems[5]["icon"] = "icon-nav-6.png"  ; 
	$navitems[5]["url"] = "/journal.php?type=journal"  ; 
	
	 
?>
<section class="section-nav" >
	<div class="container" >
		<div class="row" >
			<div class="col-md-9" >
				<div class="nav-custom-1" >
				<?php 
					foreach($navitems as $key => $item ){
				?>
					<div class="item" >
						
						<a href="<?php echo $item["url"] ?>" <?php echo  $key == 4 ? "class=\"fancybox-contact fancybox.ajax\"" : "" ; ?> >
							<img src="/html/frontend.police/images/<?php echo $item["icon"] ?>" />
							<span><?php echo $item["title"] ?></span>
						</a>
					</div>
				<?php 
					}
				?>
				</div>
			</div>
			<div class="col-md-3" >
				<div class="form-login" >
					<h4 class="title" >เข้าสู่ระบบสมาชิกออนไลน์</h4>
					<form id="formLogin" action="/app/login.submit.php" class="" method="post" >
						<div class="form-group">
							<input type="text" class="form-control" id="member_id" name="member_id" placeholder="รหัสสมาชิก" />
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="password" name="password" placeholder="รหัสผ่าน" >
						</div>
						<button type="submit" class="btn btn-default btn-custom-1">เข้าสู่ระบบ</button>
					</form>
					<div class="row" >
						<div class="col-6 register-1" >
							<a href="/app/register.php" class="register-link" >สมัครใช้บริการ</a>
						</div>
						<div class="col-6 password-1" >
							<a href="/app/forgot.php" class="password-link" >ลืมรหัสผ่าน</a>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>

<section class="section-main " >	 
	<div class="container" >
		<div class="row" >
			<div class="col-md-9">
<?php 

	$sql = "SELECT * FROM cmp_news WHERE news_status = 1 AND publicdate < NOW() AND news_type_id = '7' ORDER BY publicdate DESC LIMIT 0 , 3 " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;
	
?>
				<!-- START CONTENT GROUP 1 -->
				<div class="content-group content-group-1" >
					<div class="content-group-head" >
						<div class="row" >
							<div class="col-6"  >
								<h4 class="head-title" >ข่าวประชาสัมพันธ์ </h4>
							</div>
							<div class="col-6"  >
								<a href="/news.php?type=7" class="viewall-link" >++ ดูทั้งหมด</a>
							</div>
						</div>
					</div>
					<div class="content-group-main" >
						<div class="row" >
							<div class="col-sm-8"  >
								<?php $row = $rs->fetch_assoc() ;  ?>
								<?php if(!empty($row)){ ?> 
								<div class="content-item content-item-custom-1" >
									<a href="/news/<?php echo @$row["news_id"] ?>" >
										<?php if(empty($row["news_picture"])){ ?>
										<img class="img-fluid img-1" src="/timthumb.php?src=/html/frontend.police/images/blank.png&w=560&h=402" />
										<?php }else{  ?>
										<img class="img-fluid img-1"  src="/timthumb.php?src=/uploads/contents/<?php echo $row["news_picture"]  ?>&w=560&h=402">
										<?php } ?>
									</a>
									<a target="" class="title-1" href="/news/<?php echo $row["news_id"] ?>" ><?php echo htmlspecialchars($row["news_title"])  ?></a>
								</div>
								<?php } ?>
							</div>
							<div class="col-sm-4"  >
							<?php while($row = $rs->fetch_assoc() ){ ?>
								<div class="content-item content-item-custom-2" >
									<a href="/news/<?php echo @$row["news_id"] ?>" >
										<?php if(empty($row["news_picture"])){ ?>
										<img class="img-fluid img-1" src="/timthumb.php?src=/html/frontend.police/images/blank.png&w=510&h=338" />
										<?php }else{  ?>
										<img class="img-fluid img-1"  src="/timthumb.php?src=/uploads/contents/<?php echo $row["news_picture"]  ?>&w=510&h=338">
										<?php } ?>
									</a>
									<a target="" class="title-1" href="/news/<?php echo $row["news_id"] ?>" ><?php echo htmlspecialchars($row["news_title"])  ?></a>
								</div>
							<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<!-- END CONTENT GROUP 1 -->
<?php 

	$sql = "SELECT * FROM cmp_news WHERE news_status = 1 AND publicdate < NOW() AND news_type_id = '1' ORDER BY publicdate DESC LIMIT 0 , 6 " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;
	
?>
				<!-- START CONTENT GROUP 2 -->
				<div class="content-group content-group-2" >
					<div class="content-group-head" >
						<div class="row" >
							<div class="col-6"  >
								<h4 class="head-title" >ข่าวกิจกรรม</h4>
							</div>
							<div class="col-6"  >
								<a href="/news?type=1" class="viewall-link" >++ ดูทั้งหมด</a>
							</div>
						</div>
					</div>
					<div class="content-group-main" >
						<div class="row" >
							<?php while($row = $rs->fetch_assoc()){ ?>
							<div class="col-sm-4"  >
								<?php if(!empty($row)){ ?> 
								<div class="content-item content-item-custom-1" >
									<a href="/news/<?php echo @$row["news_id"] ?>" >
										<?php if(empty($row["news_picture"])){ ?>
										<img class="img-fluid img-1" src="/timthumb.php?src=/html/frontend.police/images/blank.png&w=560&h=402" />
										<?php }else{  ?>
										<img class="img-fluid img-1"  src="/timthumb.php?src=/uploads/contents/<?php echo $row["news_picture"]  ?>&w=560&h=402">
										<?php } ?>
									</a>
									<a target="" class="title-1" href="/news/<?php echo $row["news_id"] ?>" ><?php echo htmlspecialchars($row["news_title"])  ?></a>
								</div>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
					</div>
					
				</div>
				 
				<!-- END CONTENT GROUP 2 -->
<?php 
				$sql = "SELECT * FROM cmp_journal WHERE jol_status = 1 AND jol_type = 'journal' ORDER BY publicdate DESC  LIMIT 0 , 3 " ;
				$rs = $mysqli->query($sql);
				echo $mysqli->error ; 
?>		
				<!--- START journal GROUP  -->
				<div class="content-group content-journal" >
					<div class="content-group-head" >
						<div class="row" >
							<div class="col-12"  >
								<h4 class="head-title" >วารสารสหกรณ์</h4>
							</div>
						</div>
					</div>
					<div class="content-journal-main" >
						<div class="row" >
						<?php while($row = $rs->fetch_assoc()){ ?>
							<div class="col-md-4"  >
								<div class="journal-img" >
									<a class="journal-download" href="/uploads/journal/<?php echo @$row["jol_pdf"] ?>"   data-id="<?php echo $row["jol_id"] ?>" >
									<?php if(empty($row["jol_picture"])){ ?>
									<img class="img-fluid"  src="/timthumb.php?src=/html/frontend.police/images/blank.png&w=306&h=434">
									<?php }else{ ?>
									<img class="img-fluid" src="/timthumb.php?src=/uploads/contents/<?php echo $row["jol_picture"] ?>&w=306&h=434" />
									<?php } ?>
									</a>
								</div>
								<div class="journal-title" ><a class="journal-download" href="/uploads/journal/<?php echo $row["jol_pdf"] ?>" target="_blank" ><?php echo $row["jol_title"] ?></a> </div>
							</div>
						<?php } ?>
						</div>
					</div>
				</div>
				<!--- END journal GROUP  -->
			</div>
			
			<div class="col-md-3" >
				<!-- START BOARD CONTENT -->
				<div class="board-group" >
					<div class="chairman-block" >
						<h4 class="title" >ประธานกรรมการ</h4>
					
<?php 
					$sql = "SELECT st_board_chair_picture , st_board_chair_name , st_board_chair_position   FROM cmp_website WHERE st_id = 1 LIMIT 0 , 1 " ;
					$rs = $mysqli->query($sql);
					echo $mysqli->error ;
					$chairman = $rs->fetch_assoc();
?>
						<div class="board-item" >
							<div class="board-img" >
								<?php if(empty($chairman["st_board_chair_picture"])){ ?>
								<img class="img-fluid img-circle" src="/timthumb.php?src=/html/frontend.police/images/blank.png&w=400&h=400 " />
								<?php }else{ ?>
								<img class="img-fluid img-circle" src="/timthumb.php?src=/uploads/contents/<?php echo $chairman["st_board_chair_picture"]  ?>&w=400&h=400" />
								<?php }?>
							</div>
							<div class="board-name" ><?php echo $chairman["st_board_chair_name"] ?></div>
							<div class="board-position" ><?php echo $chairman["st_board_chair_position"] ?></div>
						</div>
					</div>
<?php 
				$sql = "SELECT * FROM cmp_board WHERE boa_status = 1  ORDER BY boa_seq ASC LIMIT 0 , 20 " ;
				$rs = $mysqli->query($sql);
				$i = 0 ; 
				if(!($rs->num_rows == 0)){
?>
					<div class="committee-block" >
						<h4 class="title" >คณะกรรมการ</h4>
						<div id="committeeCarousel" class="carousel slide" >
						<div class="carousel-inner" >
<?php 
					
					while($row = $rs->fetch_assoc()){
						$i ++ ; 
?>
							<div class="carousel-item <?php echo $i == 1 ? "active" : "" ; ?>" >
								<div class="board-item" >
									<div class="board-img" >
										<?php if(empty($row["boa_picture"])){ ?>
										<img class="img-fluid img-circle" src="/timthumb.php?src=/images/blank.png&w=400&h=400 " />
										<?php }else{ ?>
										<img class="img-fluid img-circle" src="/timthumb.php?src=/uploads/contents/<?php echo $row["boa_picture"]  ?>&w=400&h=400" />
										<?php }?>
									</div>
									<div class="board-name" ><?php echo $row["boa_name"] ?></div>
									<div class="board-position" ><?php echo $row["boa_position"] ?></div>
								</div>
							</div>
<?php 
					}
?>						</div>
						</div>
					</div>
<?php 
				}
?>
				</div>
				<!-- END BOARD CONTENT -->
				
				<!-- START INTERREST -->
<?php
				$sql = "SELECT * FROM cmp_interest WHERE inr_status = 1  AND inr_code = 'deposit'  ORDER BY inr_seq ASC , inr_id DESC " ;
				$rs = $mysqli->query($sql);
				echo $mysqli->error ; 
?>
 		  
				<div class="interest-group" >
					<h4 class="title ">อัตราดอกเบี้ยเงินฝากต่อปี</h4>
					<div class="interest-block" >
						<ul>
							<li class="interest-head" >
								<div class="left text-center">ประเภท</div>
								<div class="right text-center">อัตรา</div>
							</li>
							<?php

								while($row = $rs->fetch_assoc()){
									$iurl = !empty($row["inr_url"]) ? @$row["inr_url"] : "#" ;
							?>
							<li>
								<div class="left"><?php echo $row["inr_title"] ?></div>
								<div class="right"><?php echo $row["inr_percent"] ?>%</div>
							</li>
							<?php
								}
							?>

						</ul>
						<?php
							$sql = "SELECT st_interest_date  FROM cmp_website WHERE st_id = 1 LIMIT 0 , 1 " ;
							$rs = $mysqli->query($sql);
							$interest1 = $rs->fetch_assoc();
						?>
						<div class="interest-footer">
							<?php echo $interest1["st_interest_date"] ?>
						</div>
					</div>
				</div>
			<?php
				$sql = "SELECT * FROM cmp_interest WHERE inr_status = 1 AND inr_code = 'loan'  ORDER BY inr_seq ASC , inr_id DESC " ;
				$rs = $mysqli->query($sql);
				echo $mysqli->error ; 
			?>
				<div class="interest-group" >
					<h4 class="title ">อัตราดอกเบี้ยเงินกู้ต่อปี</h4>
					<div class="interest-block" >
						<ul>
							<li class="interest-head" >
								<div class="left text-center">ประเภท</div>
								<div class="right text-center">อัตรา</div>
							</li>
							<?php

								while($row = $rs->fetch_assoc()){
									$iurl = !empty($row["inr_url"]) ? @$row["inr_url"] : "#" ;
							?>
							<li>
								<div class="left"><?php echo $row["inr_title"] ?></div>
								<div class="right"><?php echo $row["inr_percent"] ?>%</div>
							</li>
							<?php
								}
							?>

						</ul>
						<?php
							$sql = "SELECT st_interest_date  FROM cmp_website WHERE st_id = 1 LIMIT 0 , 1 " ;
							$rs = $mysqli->query($sql);
							$interest1 = $rs->fetch_assoc();
						?>
						<div class="interest-footer">
							<?php echo $interest1["st_interest_date"] ?>
						</div>
					</div>
				</div>
				<!-- END  -->
			</div>
		</div>
	</div>
</section> 
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display("frontend.policehospital.tpl");
