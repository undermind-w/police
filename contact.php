<?php
	include "config.inc.php";

	$sql = "SELECT * FROM cmp_info_contact WHERE ic_id = '1'  LIMIT 0 , 1 " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error;
	$info = $rs->fetch_assoc();


	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="" />
<style>

</style>
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
	
?>
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script>
	$(function(){
		var pos = new google.maps.LatLng( <?php echo (double)  @$info["ic_latitude"] ?>,<?php echo (double) @$info["ic_longitude"] ?> );
		var options = {
			center: pos,
			zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("googlemaps"), options);

		var marker = new google.maps.Marker({
			position: pos,
			map: map,
			draggable:false,
		 });

		var infowindow = new google.maps.InfoWindow();
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});

	   // infowindow.setContent($("#map_content").html());
		//infowindow.open(map,marker);
		$("#googlemaps").css("display", "block");
	});
	
</script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();

?>
	<style>
		.google-map { height:400px}
	</style>

 <?php
	$slide = ob_get_contents();
	ob_end_clean();
	$smarty->assign("slide", $slide);
	$content = "" ;


	$smarty->assign("title",  @htmlspecialchars("ติดต่อเรา") );
	$smarty->assign("subject", "ติดต่อเรา" );


	ob_start();
	?>
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center" style="">ติดต่อเรา</h2>
				<hr />
				<div class="row" >
					<!-- 
					<div class="col-md-12" >
						<div id="googlemaps" ></div>
					</div>
					-->
				</div >
				<div  class="contact-detail" > 
				
					<?php echo  @htmlspecialchars_decode(@$info["ic_detail"] , ENT_QUOTES ) ?>
				</div>
			</div>
		</div>
	</div>
</section>
	
		
		

	<?php
	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
