<?php
	include "config.inc.php";
	require PATH . "/class/pagination.class.php"  ;
	include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.session.inc.php";
	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<style>
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		/* display: none; <- Crashes Chrome on hover */
		-webkit-appearance: none;
		margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
	}

	.modal-header-loan {
		padding:9px 15px;
		border:1px solid #ff4081;
		background-color: #ff4081;
		color: #FFF;
		-webkit-border-top-left-radius: 5px;
		-webkit-border-top-right-radius: 5px;
		-moz-border-radius-topleft: 5px;
		-moz-border-radius-topright: 5px;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
	}

	.modal-dialog-loan {
		margin:0 auto;
		margin-top: 10%;
	}
	
</style>
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	$smarty->assign("title", "สัญญาเงินกู้");
	ob_start();
?>
	<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
    <script src="/scripts/jquery.validate_loan.js"></script>
	<script>
	function get_select_attr_value(id_from, attr_name, id_to){
		$('#'+id_to).val($('#'+id_from+' :selected').attr(attr_name));
	}
	function submit_form(){
		$('#frm').submit();
	}
	function choose_condition(id){
		if($('#condition_'+id).is(":checked")){
			$('.condition_'+id).removeAttr( "readonly" );
		}else{
			$('.condition_'+id).attr('readonly','readonly');
		}
	}
	</script>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.subject.inc.php";
	ob_start();
?>
<div class="row m-t-md">
	<div class="col-md-3">
		<br>
		<?php include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.left.inc.php"; ?>
	</div>
	<div class="col-md-9">
		<br>
		<div class="panel panel-material-light-blue-300">
			<div class="panel-heading">
				<div class="">
					<div class="pull-left">
						<span class="fa-stack fa-2x">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-dollar fa-stack-1x text-material-light-blue-300"></i>
						</span>
					</div>
				<div class="pull-left m-l-xxs">
					<h3>ขอความอนุเคราะห์เป็นกรณีพิเศษ</h3>
				</div>
				<div class="pull-right badget">

				</div>
				<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel-body">
				<div class="text-center m-l-xxs">
					<?php 
						$sql = "SELECT 
							* 
						FROM 
							loan_application 
							INNER JOIN cmp_admin_loan_money ON loan_application.loan_id = cmp_admin_loan_money.loan_id
						WHERE loan_application_id = '".$_GET['loan_application_id']."'";
						$rs = $mysqli->query($sql);
						echo $mysqli->error;
						$row = $rs->fetch_assoc();
						
						$sql_special_beg = "SELECT * FROM loan_special_beg WHERE loan_id = '".$_GET['loan_id']."'";
						$rs_special_beg = $mysqli->query($sql_special_beg);
						echo $mysqli->error;
						$row_special_beg = $rs_special_beg->fetch_assoc();
					?>
					<form novalidate action="loan_save_special_beg.php"  class="form-horizontal" id="frm" method="post" enctype="multipart/form-data">
					<input name="data[loan_special_beg][loan_id]" value="<?php echo $_GET['loan_id']; ?>" type="hidden" >
					<input name="data[loan_special_beg][loan_application_id]" value="<?php echo $_GET['loan_application_id']; ?>" type="hidden" >
					<input name="data[loan_special_beg][id]" value="<?php echo $row_special_beg['id']; ?>" type="hidden" >
						<div class="form-group m-t-lg">
							<label style="font-weight: 100;" class="col-sm-2 control-label">รหัสสมาชิก</label>
							<div class="col-sm-3">
								<input class="form-control" id="member_id" value="<?php echo $row["member_id"]; ?>" type="text" readonly>
							</div>
							<label style="font-weight: 100;" class="col-sm-2 control-label">ชื่อ - สกุล</label>
							<div class="col-sm-4">
								<input class="form-control" id="member_name" type="text" value="<?php echo $row['member_name']?>" readonly>
							</div>
						</div>
						<div class="form-group m-t-lg">
							<label style="font-weight: 100;" class="col-sm-2 control-label">จำนวนเงินขอกู้</label>
							<div class="col-sm-3">
								<input class="form-control" id="money" value="<?php echo $row['loan_money']; ?>" type="number" readonly>
							</div>
						</div>
						
						<div class="form-group">
							<label style="font-weight: 100;" class="col-sm-2 control-label">เพื่อ</label>
							<div class="col-sm-8">
								<textarea class="form-control" id="money_use" readonly><?php echo $row['member_use']; ?></textarea>
							</div>
						</div>
						<div class="form-group" style="padding:0px;margin:0px">
							<div style="font-weight: 100;" class="col-sm-2 text-right"><input type="checkbox" name="data[loan_special_beg][condition_1]" id="condition_1" value='1' onclick="choose_condition('1')" <?php echo $row_special_beg['condition_1']=='1'?'checked':''; ?>></div>
							<?php 
							if($row_special_beg['condition_1']=='1'){
								$readonly = '';
							}else{
								$readonly = 'readonly';
							}
							?>
							<div class="col-sm-8 text-left">
								ขอผ่อนผันเงินเดือนคงเหลือไม่ถึง 40% หรือ 5,000 บาท โดยมีรายได้พิเศษจาก
								<input type="text" class="form-control condition_1" name="data[loan_special_beg][condition_1_extra_income]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_1_extra_income']; ?>" <?php echo $readonly; ?>> 
								เดือนละ 
								<input type="number" class="form-control condition_1" name="data[loan_special_beg][condition_1_extra_income_per_month]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_1_extra_income_per_month']; ?>" <?php echo $readonly; ?>>
								บาท รายได้คู่สมรส 
								<input type="number" class="form-control condition_1" name="data[loan_special_beg][condition_1_mariage_income]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_1_mariage_income']; ?>" <?php echo $readonly; ?>> 
								บาท
							</div>
						</div>
						<div class="form-group" style="padding:0px;margin:3px">
							<div style="font-weight: 100;" class="col-sm-2 text-right"><input type="checkbox" name="data[loan_special_beg][condition_2]" id="condition_2" value='1' <?php echo $row_special_beg['condition_2']=='1'?'checked':''; ?>></div>
							<div class="col-sm-8 text-left">
								ขอใช้ทุนเรือนหุ้น / เงินฝากออมทรัพย์พิเศษค้ำประกัน 100%
							</div>
						</div>
						<div class="form-group" style="padding:0px;margin:3px">
							<div style="font-weight: 100;" class="col-sm-2 text-right"><input type="checkbox" name="data[loan_special_beg][condition_3]" id="condition_3" value='1' <?php echo $row_special_beg['condition_3']=='1'?'checked':''; ?>></div>
							<div class="col-sm-8 text-left">
								ขอกู้หักลบไม่ครบปี
							</div>
						</div>
						<div class="form-group" style="padding:0px;margin:3px">
							<div style="font-weight: 100;padding-top:8px" class="col-sm-2 text-right"><input type="checkbox" name="data[loan_special_beg][condition_4]" id="condition_4" value='1' onclick="choose_condition('4')" <?php echo $row_special_beg['condition_4']=='1'?'checked':''; ?>></div>
							<?php 
							if($row_special_beg['condition_4']=='1'){
								$readonly = '';
							}else{
								$readonly = 'readonly';
							}
							?>
							<div class="col-sm-8 text-left">
								ขอผ่อนผันอายุของผู้กู้ระหว่างส่งหักจากอายุไม่เกิน 70 ปี เป็น
								<input type="number" class="form-control condition_4" name="data[loan_special_beg][condition_4_year]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_4_year']; ?>" <?php echo $readonly; ?>> 
								ปี ระยะเวลาส่งหัก
								<input type="number" class="form-control condition_4" name="data[loan_special_beg][condition_4_period]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_4_period']; ?>" <?php echo $readonly; ?>>
								งวด 
							</div>
						</div>
						<div class="form-group" style="padding:0px;margin:3px">
							<div style="font-weight: 100;padding-top:8px" class="col-sm-2 text-right"><input type="checkbox" name="data[loan_special_beg][condition_5]" id="condition_5" value='1' onclick="choose_condition('5')" <?php echo $row_special_beg['condition_5']=='1'?'checked':''; ?>></div>
							<?php 
							if($row_special_beg['condition_5']=='1'){
								$readonly = '';
							}else{
								$readonly = 'readonly';
							}
							?>
							<div class="col-sm-8 text-left">
								ระยะเวลาการเป็นสมาชิก
								<input type="number" class="form-control condition_5" name="data[loan_special_beg][condition_5_member_month]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_5_member_month']; ?>" <?php echo $readonly; ?>> 
								เดือน ไม่เป็นไปตามหลักเกณฑ์
							</div>
						</div>
						<div class="form-group" style="padding:0px;margin:3px">
							<div style="font-weight: 100;padding-top:8px" class="col-sm-2 text-right"><input type="checkbox" name="data[loan_special_beg][condition_6]" id="condition_6" value='1' onclick="choose_condition('6')" <?php echo $row_special_beg['condition_6']=='1'?'checked':''; ?>></div>
							<?php 
							if($row_special_beg['condition_6']=='1'){
								$readonly = '';
							}else{
								$readonly = 'readonly';
							}
							?>
							<div class="col-sm-8 text-left">
								อื่นๆ
								<input type="text" class="form-control condition_6" name="data[loan_special_beg][condition_6_detail]" style="display:inline;width:80%;" value="<?php echo $row_special_beg['condition_6_detail']; ?>" <?php echo $readonly; ?>>
							</div>
						</div>
					</form>
					<div class="form-group " style="margin-top:20px;">
						<div class="col-sm-12 text-center">
							<button style="width: 123px;" id="submit_frm" onclick="submit_form()" class="btn btn-material-light-blue-300" >ส่งคำขอ</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");