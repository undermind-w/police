<?php
	include "config.inc.php";
	require PATH . "/class/pagination.class.php" ;

	$member_id_get =  $_GET['member_no'];
?>
<HTML>
<head>
 <?php
	
	include "header_mobile.inc.php";
	
 ?>
	<style>
		body {
			background-color:#FFF;
		}
		.panel-heading {
			border-bottom:1px solid;
		}
		.menu_gruop{
			margin:5px;
		}
		.icon_color{
			color: #d4d4d4;
		}
		.menu_link:active{
			 background-color:rgb(212,212,212);
		} 
		h3{
			margin-top:12px;
			margin-bottom:12px;
		}
		.tr_border{
			border-bottom:1px solid rgb(212,212,212);
		}
		.td_style_icon{
			padding: 10px;
		}
		.td_style{
			padding-left: 10px;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		.text_show{
			font-size: 15px;
		}
		.date_show{
			
			color: #AAA;
			font-size: 16px;
		}
		body{
			font-family:"Sukhumvit Set",sans-serif !important;
		}
		.title_mobile{background: #efefef;}
	</style>
</head>
<body>
<?php
	$icon = 'fa fa-angle-right fa-stack-2x text-material-light-blue-300 icon_color';

?>
<table width="100%" border='0'>
	<tr class="title_mobile">
		<td valign="middle" class="td_style" colspan="2">
			<span class="text_show">สินเชื่อที่ท่านสามารถขอกู้ได้</span>
		</td>
	</tr>
	<?php 
		$sql = "SELECT * FROM loan_setting_detail ORDER BY seq_id ASC limit 2";
		$rs = $mysqli->query($sql);
		while($row = $rs->fetch_assoc()) {

	?>
	<tr class="tr_border"  onclick="link_click('loan_suggestion_mobile.php?member_no=<?php echo $_GET['member_no']; ?>&loan_type=<?php echo $row['id']; ?>')">
        <td  align="center" valign="middle" class="td_style_icon">
				<span class="icon_color fa-stack fa-1x ">
						<i class="<?php echo $icon; ?>"></i>
				</span>
        </td>
        <td width="90%" valign="middle" class="td_style">
            <span class="text_show"><?php echo $row['loan_name'];?></span>
        </td>

	</tr>
	<?php } ?>
</table>

</body>
</HTML>

<script>
function link_click(link){

	location.href = link;

}
</script>

