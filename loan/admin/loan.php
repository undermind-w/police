<?php
session_start();
// header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;
require "menu.inc.php" ;

// $cmpper->check("message") ;

function fn_send_sms($telmobile, $loan_id) {
	global $mysqli;

	if ($res = sendSMS($telmobile)) {
		$sql = "UPDATE cmp_admin_loan SET loan_sms = NOW(), transaction = '$res' WHERE loan_id = $loan_id";

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}

if (in_array($_POST['do'], array('sendSMS'))) {
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);
	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);

	if (fn_send_sms($telmobile, $loan_id)) {
		echo 'true';exit();
	}
	echo 'false';exit();
}
else if (in_array($_POST['do'], array('delfile'))) {
	$file = $mysqli->real_escape_string($_POST['file']);

	if (unlink('../letters/' . $file)) {
		$sql = "DELETE FROM cmp_letters WHERE file = '$file'";
		if ($mysqli->query($sql)) {
			echo 'true';exit();
		}
	}
	echo 'false';exit();
}
else if (in_array($_GET['do'], array('add')) && isset($_POST['member_id'])) {
	$member_id = $mysqli->real_escape_string($_POST['member_id']);
	$member_name = $mysqli->real_escape_string($_POST['member_name']);
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);

	$sql = "INSERT INTO cmp_admin_loan (loan_date, member_id, member_name, telmobile) VALUES (NOW(), '$member_id', '$member_name', '$telmobile')";

	if (!$mysqli->query($sql)) {
		header("Location: loan.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
		exit();
	}
	if (isset($_FILES['letter'])) {
		$lastid = $mysqli->insert_id;
		$sql = "INSERT INTO cmp_letters (loan_id, file_name, file) VALUES ";
		$val = "";

		foreach ($_FILES['letter']['name'] as $key => $filename) {
			$type = str_replace('application/', null,$_FILES['letter']['type'][$key]);

			if (!empty($filename) && in_array($type, array('pdf'))) {
				$tmpname = $_FILES['letter']['tmp_name'][$key];
				$type = str_replace('application/', null, $_FILES['letter']['type'][$key]);
				$rename = strtolower(random_char(30)) . '.' . $type;

				if (!move_uploaded_file($tmpname, '../letters/' . $rename)) {
					header('Location: loan.php?err=เกิดข้อ์ผิดพลาดในการอัพโหลดไฟล');
					exit();
				}

				$val .= "($lastid, '$filename', '$rename'),";
			}
		}

		if (!empty($val)) {
			$sql .= rtrim($val, ',');
			if (!$mysqli->query($sql)) {
				header("Location: loan.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
				exit();
			}
		}
	}
	if ($_POST['saveandsend'] == 'checked') {
		if (!fn_send_sms($telmobile, $lastid)) {
			header("Location: loan.php?err=" . urlencode('เกิดข้อผิดพลาดในการส่ง SMS'));
			exit();
		}
	}

	// header("Location: loan.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
	header("Location: loan.php?res=บันทึกข้อมูลแล้ว");
	exit();

}
else if (in_array($_GET['do'], array('edit')) && isset($_POST['loan_id'])) {
	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$member_id = $mysqli->real_escape_string($_POST['member_id']);
	$member_name = $mysqli->real_escape_string($_POST['member_name']);
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);

	// insert new files uploaded
		$sql = "INSERT INTO cmp_letters (loan_id, file_name, file) VALUES ";
		$val = "";
		// foreach ($_FILES['letter']['name'] as $key => $filename) {
		foreach ($_FILES['letter']['name'] as $key => $filename) {
			$type = str_replace('application/', null,$_FILES['letter']['type'][$key]);

			if (!empty($filename) && in_array($type, array('pdf'))) {
				$tmpname = $_FILES['letter']['tmp_name'][$key];
				$type = str_replace('application/', null, $_FILES['letter']['type'][$key]);
				$rename = strtolower(random_char(30)) . '.' . $type;

				if (move_uploaded_file($tmpname, '../letters/' . $rename)) {
					$val .= "($loan_id, '$filename', '$rename'),";
				} else {
					header('Location: loan.php?err=เกิดข้อ์ผิดพลาดในการอัพโหลดไฟล');
					exit();
				}
			}
		}
		$sql = (empty($val)) ? '' : $sql . rtrim($val, ',');

		if ($sql != '') {
			if (!$mysqli->query($sql)) {
				header('Location: loan.php?err=เกิดข้อผิดพลาดในการเพิ่มไฟล์ใหม่');
				exit();
			}
		}

	// update cmp_admin_loan
		$sql = "UPDATE cmp_admin_loan
				SET member_id = '$member_id', member_name = '$member_name', telmobile = '$telmobile'
				WHERE loan_id = $loan_id";
		if (!$mysqli->query($sql)) {
			header('Location: loan.php?err=เกิดข้อผิดพลาดในการแก้ไขข้อมูล');
			exit();
		}

		if ($_POST['saveandsend'] == 'checked') {
			if (!fn_send_sms($telmobile, $lastid)) {
				header("Location: loan.php?res=เกิดข้อผิดพลาดในการบันทึกสถานะ SMS");
				exit();
			}
		}

	header('Location: loan.php?res=แก้ไขข้อมูลแล้ว');
	exit();

}
else if (in_array($_GET['do'], array('del'))) {
	$loan_id = $mysqli->real_escape_string($_GET['loan_id']);

	$sql = "SELECT file FROM cmp_letters WHERE loan_id = $loan_id";
	$result = $mysqli->query($sql);

	while ($row = $result->fetch_assoc()) {
		if (!unlink('../letters/' . $row['file'])) {
			header('Location: loan.php?err=เกิดข้อผิดพลาดในการลบไฟล์');
			exit();
		}
	}

	$sql = "DELETE FROM cmp_admin_loan WHERE loan_id = '$loan_id'";
	$sql2 = "DELETE FROM cmp_letters WHERE loan_id = '$loan_id'";

	if (!$mysqli->query($sql) && !$mysqli->query($sql2)) {
		header('Location: loan.php?err=เกิดข้อผิดพลาดในการลบข้อมูล');
		exit();
	}

	header('Location: loan.php?res=ลบข้อมูลแล้ว');
	exit();

} else {

ob_start();
?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/validation/js/formValidation.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<script src="/scripts/validation/js/bootstrap.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script>
$(document).ready(function() {

	$('#department_name').on('change', function() {
				 var name_head = $('option:selected', this).attr('name_head');
				 var tel_head = $('option:selected', this).attr('tel_head');
					// console.log(name_head,tel_head);
					$("#name_head").val(name_head);
					$("#telmobile").val(tel_head);
	});

	$('#addFile').on('click', function() {
		$('<input>').attr({
			type: 'file',
			class: 'form-control',
			name: 'letter[]'}).insertBefore($(this).parent()).trigger('click');
	});

	$('.btndel').click(function() {
		var obj = $(this).parents('p');

		if (confirm('ต้องการลบไฟล์นี้')) {
			$.ajax({
				type	: 'POST',
				url	: 'loan.php',
				data	: {
					'do'	 : 'delfile',
					'file' : $(this).prop('name')
				},
				success : function(response) {
					if (response === 'true') {
						obj.remove();
					}
				}
			});
		}
	});

	$('#loan-form').formValidation({
		fields: {
			member_id: {
				validators: {
					notEmpty	: { message: '<p>กรุณากรอกรหัสสมาชิก</p>' },
					stringLength: { message: '<p>กรุณาป้อนรหัสอย่างน้อย 6 ตัว', min: 6 },
					regexp	: { message: '<p>กรุณาป้อนตัวเลขเท่านั้น</p>', regexp: /\d$/ }
				}
			},
			member_name: {
				validators: {
					notEmpty	: { message: '<p>กรุณาป้อนชื่อสมาชิก</p>' },
					regexp	: { message: '<p>กรุณาป้อนตัวอักษรเท่านั้น</p>', regexp: /[a-zก-ฮ\W]$/ }
				}
			},
			telmobile: {
				validators: {
					notEmpty	: { message: '<p>กรุณาป้อนเบอร์โทรศัพท์</p>' },
					regexp	: { message: '<p>กรุณาป้อนเบอร์โทรศัพท์</p>', regexp: /^0[8|9].[0-9]{7}$/ }
				}
			}
		}
	}).on('status.field.fv', function(e, data) {
		if (data.field === 'telmobile') {
			(data.status === 'VALID' && $('#loan_id').val() !== '')
			    ? $('#btn-sms').removeAttr('disabled')
			    : $('#btn-sms').prop('disabled', 'disabled')
		}
	});

	$('#btn-sms').on('click', function() {
		var obj = $(this);
		$.ajax({
			type	: 'POST',
			url	: 'loan.php',
			data	: {
				'do'		: 'sendSMS',
				'loan_id'	: $('#loan_id').val(),
				'telmobile'	: $('#telmobile').val()
			},
			// async: false,
			success : function(response) {
				var res = '';
				if (response == 'true') {
					res = 'ดำเนินการส่ง SMS แล้ว';
					$('.blgres').text(res).addClass('bg-success');
				} else {
					res = 'เกิดข้อผิดพลาดในการส่ง SMS';
					$('.blgres').text(res).addClass('bg-danger');
				}
			}
		});
	});

});
</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$page = (int) @$_GET["page"]  ;
	$smarty->assign("title", "สินเชื่อ" );
	// $smarty->assign("page_title", "สินเชื่อ" );

	ob_start();
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">สินเชื่อ</h4>
			</div>
			<div class="panel-body">
				<?php	if (in_array($_GET['do'], array('add', 'edit'))) {
					if ($_GET['do'] == 'add') {
						$smarty->assign("breadcrumb", array("สินเชื่อ" => "loan.php", "เพิ่มข้อมูล" => "#"));
					} else {
						$smarty->assign("breadcrumb", array("สินเชื่อ" => "loan.php", "แก้ไขข้อมูล" => "#"));

						$sql = "SELECT * FROM cmp_admin_loan WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
						$user = $result->fetch_object();

						$sql = "SELECT * FROM cmp_letters WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
					}
				?>
					<form action="" class="form-horizontal" id="loan-form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="loan_id" id="loan_id" value="<?= (isset($user->loan_id)) ? $user->loan_id : '' ?>">
						<div class="form-group">
							<label for="member_id" class="col-sm-3 control-label">รหัสสมาชิก</label>
							<div class="col-sm-4">
								<input type="text" id="member_id" class="form-control" name="member_id" placeholder="รหัสสมาชิก"
									value="<?= (isset($user->member_id)) ? $user->member_id : '' ?>" autofocus>
							</div>
						</div>
						<div class="form-group">
							<label for="member_name" class="col-sm-3 control-label">ชื่อสกุล</label>
							<div class="col-sm-4">
								<input type="text" id="member_name" class="form-control" name="member_name"
									value="<?= (isset($user->member_name)) ? $user->member_name : '' ?>" placeholder="ชื่อสกุล">
							</div>
						</div>

						<div class="form-group">
						<?php 
							$sql_department = "SELECT * FROM cmp_loan_department"; 
							$rs_department = $mysqli->query($sql_department);

							if ($_GET['do'] == "edit") {

								
								$edit_department = "SELECT * FROM cmp_loan_department WHERE department_head_tel =  $user->telmobile "; 
								$result_edit = $mysqli->query($edit_department);
    							$row_edit = $result_edit->fetch_assoc();

							}
						 ?>
							<label for="department_name" class="col-sm-3 control-label">หน่วยบริการ</label>
							<div class="col-sm-4">
							<select id="department_name" class="form-control">
								<option value="">เลิอกหน่วยบริการ</option>
							    <?php while(($row_department = $rs_department->fetch_assoc())) { ?>
								<option 
								 <?= ($row_department['department_head_tel'] == $row_edit['department_head_tel']) ? "selected" : "" ;?>
								 value="<?=$row_department['department_id']?>"
								 name_head="<?=$row_department['department_head']?>"
								 tel_head="<?=$row_department['department_head_tel']?>"
								 >
								 <?=$row_department['department_name']?>
								 </option>
								<?php } ?>
							</select>
							</div>
						</div>

						<div class="form-group">
							<label for="name_head" class="col-sm-3 control-label">หัวหน้าหน่วย</label>
							<div class="col-sm-4">
								<input type="text" id="name_head" class="form-control" name="name_head" placeholder="หัวหน้าหน่วย"
									value="<?=$row_edit['department_head']?>" readonly>
							</div>
						</div>

						<div class="form-group">
							<label for="telmobile" class="col-sm-3 control-label">มือถือ</label>
							<div class="col-sm-4">
								<input type="text" id="telmobile" class="form-control" name="telmobile" placeholder="มือถือ"
									value="<?= (isset($user->telmobile)) ? $user->telmobile : '' ?>" readonly>
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-3 control-label">สัญญา</label>
							<div class="col-sm-4" id="divfile">
								<!-- <div class="checkbox"> -->
									<?php //if (isset($result)) {
										//while ($file = $result->fetch_object()) { ?>
											<!-- <p><label><input type="checkbox" name="cfile[]" value="<?= $file->file_name ?>"
												checked> <?= $file->file_name ?></label></p> -->
										<?php //}
									//} ?>
								<!-- </div> -->
								<div class="">
						  เอกสารการสัญญาเงินกู้</a> ,

									<?php if (isset($result)) {
										while ($file = $result->fetch_object()) { ?>
											<p><a class="btndel" name="<?= $file->file ?>"><i class="fa fa-trash-o"></i></a>
											&nbsp;&nbsp;
											<a target="_blank" href="download_pdf.php?file=../letters/<?= $file->file ?>"><?= $file->file_name ?></a></p>
										<?php }
									} ?>
								</div>
								<input type="file" name="letter[]" class="form-control" accept=".pdf" multiple>
								<!-- <span class="pull-right">
									<a class="btn btn-success" style="margin-top: 5px" id="addFile">
										<i class="fa fa-plus-circle"></i>
									</a>
								</span> -->
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label"></label>
							<div class="col-sm-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="saveandsend" value="checked" <?= (isset($_GET['loan_id'])) ? '' : 'checked' ?>> บันทึกและส่ง SMS
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-9 control-label">
								<p class="text-center">
									<button type="submit" class="btn btn-success2">บันทึก</button>
									<a class="btn btn-success2" id="btn-sms">ส่ง SMS</a>
								</p>
								<p class="blgres text-center"></p>
							</div>
						</div>
					</form>
				<?php
					} else {
						$smarty->assign("breadcrumb", array("สินเชื่อ" => "?") );

						$sql = "SELECT * FROM cmp_admin_loan";
						$rs = $mysqli->query($sql);
						$num_rows = $rs->num_rows;
						$per_page = 10 ;
						$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

						$pagination = (new Pagination());

						$pagination->setCurrent($page);
						$pagination->setRPP($per_page);
						$pagination->setTotal($num_rows);
						$pagination->setCrumbs(25);
						$paging = $pagination->parse();

						$page_start = (($per_page * $page) - $per_page);
						$sql = "SELECT * FROM cmp_admin_loan
								ORDER BY loan_date DESC
								LIMIT {$page_start} , {$per_page}";

						$rs = $mysqli->query($sql);
						echo $mysqli->error;

						$num_rows -= $page_start;
						?>

						<a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
						<?php if (isset($_GET['err'])) { ?>
							<div class="alert alert-danger resalt">
								<p>ERROR : <?= $_GET['err'] ?></p>
							</div>
						<?php } elseif (isset($_GET['res'])) { ?>
							<div class="alert alert-success resalt">
								<p>! <?= $_GET['res'] ?></p>
							</div>
						<?php } ?>
						<div style="clear: both;margin-bottom:15px"></div>
						<table class="table">
							<thead>
								<tr>
									<th class="text-center">ลำดับ</th>
									<th class="text-center">วันที่</th>
									<th class="text-center">รหัสสมาชิก</th>
									<th class="text-center">ชื่อสกุล</th>
									<th class="text-center">ส่ง SMS</th>
									<th class="text-center">เปิดดูเมื่อ</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php while(($row = $rs->fetch_assoc())) { ?>
									<tr>
										<td class="text-center"><?= $num_rows-- ?></td>
										<td class="text-center"><?php echo ConvertToThaiDate($row["loan_date"], true, true, true); ?></td>
										<td class="text-center"><?php echo $row["member_id"]; ?></td>
										<td class="text-center"><?php echo $row["member_name"]; ?></td>
										<td class="text-center"><?= ($row['loan_sms'] == '0000-00-00 00:00:00') ? 'รอส่ง' : showTime($row['loan_sms']) ?></td>
										<td class="text-center"><?= ($row['read_status'] == '0000-00-00 00:00:00') ? '' : ConvertToThaiDate($row['read_status'], true, true, true) ?></td>
										<td class="text-center">
											<a href="?do=edit&loan_id=<?= $row['loan_id'] ?>" title="แก้ไขข้อมูล"><i class="fa fa-edit"></i></a> |
											<a href="?do=del&loan_id=<?= $row['loan_id'] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้');" title="ลบข้อมูล"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
								<?php
									}
									unset($sql, $rs, $row);
								?>
							</tbody>
						</table>
					<?php echo $paging;  ?>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();
}

$smarty->assign("content", $content);
$smarty->display(THEME.".tpl");
