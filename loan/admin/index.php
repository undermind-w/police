<?php

	session_start();
	require "config.inc.php" ;
	
	require "../../admin/session.inc.php" ;
	require "menu.inc.php" ;

	// echo "<script type='text/javascript'>document.location.href='loan_money.php?view=all';</script>";	
	// exit();
	

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");

	ob_start();
?>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<style>
		.table > tbody > tr > td
		, .table > tbody > tr > th
		, .table > tfoot > tr > td
		, .table > tfoot > tr > th
		, .table > thead > tr > td
		, .table > thead > tr > th
		, .table td{
			padding-left: 5px!important;
			padding-right: 5px!important;
		}
	</style>

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/jqueryui/jquery.ui.datepicker-th.js"></script>
<script src="/scripts/jqueryui/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
<script src="/scripts/validation/js/formValidation.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<script src="/scripts/validation/js/bootstrap.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
	?>
<script>

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();


</script>
	<?php

	 
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);




	$smarty->assign("title", "หน้าแรก" );
	$smarty->assign("breadcrumb", array("Dashboard" => "index.php") );

	 ob_start();
?>
	<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix"><h4 class="panel-title">ระบบเงินกู้ออนไลน์</h4></div>
			<div class="panel-body">

				<div style="clear: both;"></div>
 			</div>
		</div>
	</div>
</div>
<script>
	
</script>
<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
