<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 	
	$cmpper->check("message") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		//echo"<pre>";print_r($_POST);exit;
		foreach($_POST['data'] as $key => $value){
			$sql = "UPDATE loan_setting SET interest_rate = '".$value['interest_rate']."', max_period = '".$value['max_period']."' WHERE loan_type = '".$key."'";
			$mysqli->query($sql);
			$sql = "UPDATE term_of_loan SET credit_limit = '".$value['credit_limit']."' WHERE loan_application_type = '".$key."'";
			$mysqli->query($sql);
		}
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
	ob_start();
?>
	<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
	<script>
		$(document).ready(function() {
			$('.date-picker').datepicker({
				orientation: "top auto" ,
				autoclose: true , 
				format : "yyyy-mm-dd"
			});
			 
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
		$smarty->assign("title", "อัตราดอกเบี้ยและงวดชำระ" );
		$smarty->assign("page_title", "อัตราดอกเบี้ยและงวดชำระ" );
 		$smarty->assign("breadcrumb", array("อัตราดอกเบี้ยและงวดชำระ" => "#"  ) );
		 
		ob_start();
	$sql = "SELECT * FROM loan_setting";
	$rs = $mysqli->query($sql);
	$data = array();
	while($row = $rs->fetch_assoc()){
		$data[$row['loan_type']] = $row;
	}
	$sql = "SELECT * FROM term_of_loan";
	$rs = $mysqli->query($sql);
	while($row = $rs->fetch_assoc()){
		$data[$row['loan_application_type']]['credit_limit'] = $row['credit_limit'];
	}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					<div class="panel-heading clearfix">
						<h4 class="panel-title">เงินกู้สามัญ</h4>
					</div>
					<?php $i='1'; ?>
  						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">อัตราดอกเบี้ย</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="data[<?php echo $i; ?>][interest_rate]" placeholder="" value="<?php echo $data[$i]['interest_rate'] ?>" required>
 							</div>
							<label for="msg_title" class="col-sm-1 control-label" style="text-align:left;">%</label>
						</div>
						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">งวดชำระสูงสุด</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="data[<?php echo $i; ?>][max_period]" placeholder="" value="<?php echo $data[$i]['max_period'] ?>" required>
 							</div>
						</div>
						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">วงเงินกู้สูงสุด</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="data[<?php echo $i; ?>][credit_limit]" placeholder="" value="<?php echo $data[$i]['credit_limit'] ?>" required>
 							</div>
						</div>
					<div class="panel-heading clearfix">
						<h4 class="panel-title">เงินกู้ฉุกเฉิน</h4>
					</div>
					<?php $i='3'; ?>
  						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">อัตราดอกเบี้ย</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="data[<?php echo $i; ?>][interest_rate]" placeholder="" value="<?php echo $data[$i]['interest_rate'] ?>" required>
 							</div>
							<label for="msg_title" class="col-sm-1 control-label" style="text-align:left;">%</label>
						</div>
						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">งวดชำระสูงสุด</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="data[<?php echo $i; ?>][max_period]" placeholder="" value="<?php echo $data[$i]['max_period'] ?>" required>
 							</div>
						</div>
						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">วงเงินกู้สูงสุด</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="data[<?php echo $i; ?>][credit_limit]" placeholder="" value="<?php echo $data[$i]['credit_limit'] ?>" required>
 							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 