<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;
require "menu.inc.php" ;
//require PATH."/class/connect_odbc.inc.php";

// $cmpper->check("message") ;
function fn_sms_member($telmobile, $message,$send_type,$loan_id,$doc_type) {
	global $mysqli;
 	$now = date("Y-m-d H:i:s");

	if ($res = send_sms($telmobile,$message)) {
		
		if ($send_type == "check_doc") {

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='สหกรณ์ กำลังตรวจสอบคำร้องและเอกสารกู้',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				c_check_doc_status = '1',
                                c_check_doc_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}else if($send_type == "contract_doc"){

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='สหกรณ์ กำลังดำเนินการออกสัญญาเงินกู้ให้กับท่าน',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				c_contract_doc = '1',
                                c_contract_doc_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}else if($send_type == "send_doc"){

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='สัญญาเงินกู้ได้จัดส่งเรียบร้อยแล้ว',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				c_send_doc = '1',
                                c_send_doc_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}else if($send_type == "send_approve"){

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='อนุมัติเงินกู้เสร็จสมบรูณ์และทำการโอนเงินเรียบร้อย',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				c_approve = '1',
                                c_approve_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}else{

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='ส่งให้ผู้ขอกู้ : ".$message."',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);
			 
			 $sql_app = "update loan_application set
						loan_status = '3'
					WHERE
						loan_id = '{$loan_id}'
			";
			 $mysqli->query($sql_app);

				   $sql = "update cmp_admin_loan_money set
				 				not_approve = '1',
                                not_approve_date='{$now}',
                                not_approve_message='{$message}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}

function fn_send_bondsman($telmobile, $message , $bondsman_id,$loan_id,$doc_type) {
	global $mysqli;
	if ($res = sms_bondsman($telmobile,$message)) {
 		$now = date("Y-m-d H:i:s");

 		 $status_sql = "update cmp_admin_loan_money set
				 				c_sms_b_status = '1',
                                c_sms_b_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			"; 
		 $mysqli->query($status_sql);

		 $sql = "update loan_application_bondsman set
                                bondsman_sms='{$now}'
                                WHERE
                                bondsman_id = '{$bondsman_id}'
        "; 

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}

function fn_send_bondsman_alone($telmobile, $message , $bondsman_id) {
	global $mysqli;
	if ($res = send_sms($telmobile,$message)) {
 		$now = date("Y-m-d H:i:s");

		 $sql = "update loan_application_bondsman set
                                sms_update='{$now}'
                                WHERE
                                bondsman_id = '{$bondsman_id}'
        "; 

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}

function fn_send_head($telmobile, $message,$loan_id) {
	global $mysqli;
	if ($res = send_sms($telmobile,$message)) {

 		$now = date("Y-m-d H:i:s");

 		 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='สหกรณ์ ส่ง SMS ให้หัวหน้าหน่วยบริการแล้ว',
							status_date='{$now}',
							loan_application_type = 3
					        "; 
		 $mysqli->query($send_status);


		 $sql = "update cmp_admin_loan_money set
                                c_sms_h_status = 1,
                                c_sms_h_date = '{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        "; 

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}

function fn_send_sms($telmobile, $loan_id ,$loan_application_type,$member_id) {
	global $mysqli;

	if ($loan_application_type == 1) {
		$link = "http://loan.cmcoop.or.th/common/$loan_id/1/$member_id";
	}else if ($loan_application_type == 2) {
		$link = "http://loan.cmcoop.or.th/atm/$loan_id/2/$member_id";
	}else{
		$link = "http://loan.cmcoop.or.th/emer/$loan_id/3/$member_id";
	}

	if ($res = sendSMS_2($telmobile,$link)) {

		$sql = "UPDATE cmp_admin_loan_money  SET loan_sms = NOW() WHERE loan_id = $loan_id";

		$sql2 = "INSERT INTO cmp_admin_loan_money_status 
		(loan_id,status_name, status_date,loan_application_type,status_show) VALUES ($loan_id, 'สหกรณ์ได้ส่งแบบฟอร์มขอกู้เรียบร้อยแล้ว', NOW(),$loan_application_type,1)";
		$mysqli->query($sql2);

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}
	if($_GET['confirm_transfer'] == 'ok'){

			$sql_update = "UPDATE loan_application SET 
			confirm_transfer = '1' ,
			admin_confirm_transfer = '".$_SESSION['ADMIN_ID']."' ,
			date_transfer = NOW() 
		WHERE loan_id = '".$_GET['loan_id']."'";
		$mysqli->query($sql_update);


	}
if($_POST['do'] == 'approve_money'){
	$sql = "SELECT 
		t1.member_id ,
		t1.member_tel ,
		t1.loan_application_type,
		t1.loan_application_id,
		t1.member_name,
		t2.loan_number,
		t3.admin_tel,
		t4.interest,
		t4.loan_type_system,
		t5.loan_reason,
		t1.member_use AS loan_reason_id,
		t1.loan_money,
		t1.period
	FROM 
		loan_application t1 
		LEFT JOIN cmp_admin_loan_money t2 ON t1.loan_id = t2.loan_id
		LEFT JOIN loan_admin t3 ON t1.responsible_person_id = t3.admin_id
		LEFT JOIN loan_setting_detail AS t4 ON t4.id = t1.loan_application_type 
		LEFT JOIN loan_reason AS t5 ON t1.member_use = t5.id
	WHERE 
		t1.loan_id = '".$_POST['loan_id']."'";
	$rs = $mysqli->query($sql);
	$row = $rs->fetch_assoc();
	//echo '<pre>'; print_r($_POST); echo '</pre>';
############################################################################
#@start เชื่อมกับระบบบริหาร
############################################################################	
	$approve_date =  date("Y-m-d");
	$day = (double)date("d");
	$month = (double)date("m");
	$year  = (double)date("Y");

	//$loan_id จากระบบบริหาร
	$data_loan = array();
	$data_loan["interest"] 			= 	$row['interest'];
	$data_loan['loan'] 				= 	$_POST['approve_money'];
	$data_loan["period"]			=	$row['period'];
	$data_loan["day"]				=	$day;
	$data_loan["month"]				=	$month;
	$data_loan["year"]				=	$year;
	$data_loan["pay_type"]			=	'1';
	$data_loan["period_type"]		=	'1';

	$get_loan_period = get_loan_period($data_loan);	
	//สร้างสัญญาที่ระบบริหาร
	$sql_loan = "insert coop_loan set
					loan_type = '".$row['loan_type_system']."',
					petition_number = '".$row['loan_number']."',
					member_id = '".$row['member_id']."',
					loan_amount = '".$_POST['approve_money']."',
					loan_reason = '".$row['loan_reason_id']."',					
					interest_per_year = '".$row['interest']."',
					period_amount = '".$row['period']."',
					date_start_period = '".$get_loan_period['first_date_period']."',
					date_period_1 = '".$get_loan_period['first_date_period']."',
					money_period_1 = '".$get_loan_period['first_pay']."',
					date_period_2 = '".$get_loan_period['second_date_period']."',
					money_period_2 = '".$get_loan_period['second_pay']."',
					period_type = '1',
					pay_type = '1',								
					loan_amount_balance = '".$_POST['approve_money']."',
					loan_amount_total = '".$_POST['approve_money']."',
					loan_amount_total_balance = '".$_POST['approve_money']."',
					loan_status = '0',
					createdatetime =  '".date("Y-m-d H:i:s")."',
					loan_type_input  = '1',
					loan_application_id  = '".$_POST['loan_id']."'
	"; 
	//loan_amount_balance = '".$_POST['approve_money_balance']."',
	//loan_amount_total_balance = '".$_POST['approve_money_balance']."',
	$rs = $mysqli_app->query($sql_loan);
	$loan_id_system = $mysqli_app->insert_id;
	//echo $loan_id_system .'<hr>';
	//period_type = ประเภทการคำนวณการชำระเงิน 1=งวดที่ต้องการผ่อน 2=เงินที่ต้องการผ่อนต่องวด
	//pay_type = ประเภทการชำระเงิน 1=ชำระต้นเท่ากันทุกงวด 2=ชำระยอดเท่ากันทุกงวด
	//date_start_period=วันที่เริ่มชำระค่างวด
	//date_period_1 = วันที่ชำระเงินงวดที่1สำหรับเงินกู้ฉุกเฉิน
	//money_period_1 = จำนวนเงินงวดที่1สำหรับเงินกู้ฉุกเฉิน 
	//date_period_2 = วันที่ชำระเงินงวดที่2สำหรับเงินกู้ฉุกเฉิน
	//money_period_2 = จำนวนเงินงวดที่2สำหรับเงินกู้ฉุกเฉิน
	
	//รายละเอียดจำนวนงวด และจำนวนเงิน
	foreach($get_loan_period['peroid_row'] AS $key_period=>$val_period){
		$sql_loan_period = "insert coop_loan_period set
					period_count = '".$val_period['period_count']."',
					outstanding_balance = '".$val_period['outstanding_balance']."',
					date_period = '".$val_period['date_period']."',
					date_count = '".$val_period['date_count']."',
					interest = '".$val_period['interest']."',					
					principal_payment = '".$val_period['principal_payment']."',
					total_paid_per_month = '".$val_period['total_paid_per_month']."',
					loan_id  = '".$loan_id_system."'
		"; 
		$rs_loan_period = $mysqli_app->query($sql_loan_period);
	}	
	
	//เพิ่มเติม รายการหหักกลบด้วย
	//ข้อมูลผู้ค้ำประกัน
	
	//จำนวนผู้ค้ำประกัน เพื่อหาภาระค้ำประกัน
	$sql_count_bondsman = "SELECT COUNT(t1.bondsman_number) AS count_bondsman FROM loan_application_bondsman as t1 WHERE t1.loan_id = '".$_POST['loan_id']."' AND t1.bondsman_number <> ''";
	$rs_count_bondsman = $mysqli->query($sql_count_bondsman);
	$row_count_bondsman = $rs_count_bondsman->fetch_assoc();
	$count_bondsman = $row_count_bondsman['count_bondsman'];
	$guarantee_person_amount = @($_POST['approve_money']/$count_bondsman); //ภาระค้ำประกัน
	$guarantee_person_amount_balance = @($_POST['approve_money']/$count_bondsman); //ภาระค้ำประกันคงเหลือ
	
	$sql_bondsman = "SELECT 
			t1.loan_application_id, 
			t1.bondsman_id,
			t1.bondsman_tel,
			t1.bondsman_number
		FROM 
			loan_application_bondsman as t1
		WHERE 
			t1.loan_id = '".$_POST['loan_id']."'
		ORDER BY t1.bondsman_num";	
	$rs_bondsman = $mysqli->query($sql_bondsman);
	$check_bondsman = 0;
	while($row_bondsman = $rs_bondsman->fetch_assoc()){		
		if($row_bondsman['bondsman_number'] == ''){
			$guarantee_person_amount = '';
			$guarantee_person_amount_balance = '';
		}
		
		$sql_guarantee_person = "insert coop_loan_guarantee_person set
				loan_id = '".$loan_id_system."',
				guarantee_person_id = '".$row_bondsman['bondsman_number']."', 
				guarantee_person_amount = '".$guarantee_person_amount."', 
				guarantee_person_amount_balance = '".$guarantee_person_amount_balance."'"; 
		$rs_guarantee_person = $mysqli_app->query($sql_guarantee_person);	
		if($row_bondsman['bondsman_number'] != ''){
			$check_bondsman++;
		}	
	}
	
	if($check_bondsman > 0){
		//coop_loan_guarantee = เก็บประเภทหลักประกัน guarantee_type=1 คือ ประเภทคนค้ำ
		$sql_guarantee_type = "insert coop_loan_guarantee set
						guarantee_type = '1',
						loan_id  = '".$loan_id_system."'
		"; 
		$rs_guarantee_type = $mysqli_app->query($sql_guarantee_type);	
	}

	//เก็บข้อมูลจำนวนเงินที่จะได้รับหลังหักรายการหักต่างๆ
	$pay_per_month = (@$get_loan_period['second_summonth']=='31')?str_replace(',','',$get_loan_period['second_pay']):str_replace(',','',$get_loan_period['first_pay']);
	$sql_deduct_profile = "insert coop_loan_deduct_profile set
				loan_id = '".$loan_id_system."',
				estimate_receive_money = '".$_POST['approve_money_balance']."',
				pay_per_month = '".$pay_per_month."',
				date_receive_money = '".date("Y-m-d")."',
				date_first_period = '".$get_loan_period['first_date_period']."',
				first_interest = '".$get_loan_period['first_interest_amount']."'
				"; 
	$rs_deduct_profile = $mysqli_app->query($sql_deduct_profile);	

	//ข้อมูลค่าใช้จ่าย  ตาราง coop_loan_cost_mod  	
	$sql_outgoing = "SELECT outgoing_code FROM coop_outgoing WHERE outgoing_status = '1' ORDER BY outgoing_no ASC"; 	
	$rs_outgoing = $mysqli_app->query($sql_outgoing);
	while($row_outgoing = $rs_outgoing->fetch_assoc()){	
		$outgoing_code = $row_outgoing['outgoing_code'];
		$sql_cost_mod = "insert coop_loan_cost_mod set
				loan_id = '".$loan_id_system."',
				member_id = '".$row['member_id']."',
				loan_cost_code = '".$outgoing_code."',
				loan_cost_amount = '0'
				"; 
		$rs_cost_mod = $mysqli_app->query($sql_cost_mod);	
	}
//echo 'บันทึก';
//exit;	
############################################################################
#@end เชื่อมกับระบบบริหาร
############################################################################
	
	
	$sql_update = "UPDATE loan_application SET 
		officer_approve_status = '1' ,
		officer_approve_money = '".$_POST['approve_money']."' ,
		prev_loan = '".str_replace(',','',$_POST['prev_loan'])."' ,
		real_money = '".$_POST['approve_money_balance']."' ,
		officer_approve_date = NOW() 
	WHERE loan_id = '".$_POST['loan_id']."'";
	$mysqli->query($sql_update);
	
	$sql_update = "UPDATE cmp_admin_loan_money SET 
		c_approve = '1' ,
		c_approve_date = NOW() 
	WHERE loan_id = '".$_POST['loan_id']."'";
	$mysqli->query($sql_update);
	
	$message = 'คำร้องขอกู้เงินเลขที่ '.$row['loan_number'].' ผ่านการพิจารณาแล้ว '.number_format($_POST['approve_money'],2).'-. หักกลบคงเหลือ '.number_format($_POST['approve_money_balance'],2).'-. ดาวน์โหลดคำร้องขอกู้และสัญญากู้ได้ที่ แอพพลิเคชันสหกรณ์ เมนูคำร้องขอกู้เงิน หรือ ';
	$message_a = 'คลิกดาวน์โหลดที่นี่';
	// echo $row['member_tel'].'<br>';
	// echo $message.$message_a.'<br>';exit;

	send_sms($row['member_tel'],$message.$message_a);
	
	//$data_get = base64_encode("loan_application_id=".$row['loan_application_id'])."&".base64_encode("loan_application_id=".$row['loan_application_id']);
	$data_get = base64_encode("loan_application_id=".$row['loan_application_id']);
	$message_2 = BASEPATH."/loan_all_pdf.php?".$data_get;
	/*if($row['loan_application_type']=='1'){
		$data_get = base64_encode("loan_application_id=".$row['loan_application_id'])."&".base64_encode("loan_application_id=".$row['loan_application_id']);
	  $message_2 = BASEPATH."/loan_all_pdf.php?".$data_get;
	}else if($row['loan_application_type']=='3'){ 
		$data_get = base64_encode("loan_id=".$_POST['loan_id']);
		$message_2 = BASEPATH."/loan_emergent_pdf.php?loan_id=".base64_encode($_POST['loan_id']);
	}
	*/
	send_sms($row['member_tel'],$message_2);
	$message_2 = '<a href="'.$message_2.'" target="_blank">คลิกดาวน์โหลดที่นี่</a>';
	$send_status = "insert cmp_admin_loan_money_status set
		loan_id = '".$_POST['loan_id']."',
		status_name = 'ส่งให้ผู้ขอกู้ : ".$message.$message_2."',
		status_date = NOW(),
		loan_application_type = '".$row['loan_application_type']."'
	"; 
	//echo $send_status;
	$mysqli->query($send_status);
	
	$sql_bondsman = "SELECT 
		t1.loan_application_id, 
		t1.bondsman_id,
		t2.telmobile,
		t1.bondsman_tel
	FROM 
		loan_application_bondsman as t1
		INNER JOIN cmp_imp_member_data as t2 ON t1.bondsman_number = t2.member_no
	WHERE 
		loan_id = '".$_POST['loan_id']."'";
	$rs_bondsman = $mysqli->query($sql_bondsman);
	while($row_bondsman = $rs_bondsman->fetch_assoc()){
		$bondsman_tel = $row_bondsman['bondsman_tel']!=''?$row_bondsman['bondsman_tel']:$row_bondsman['telmobile'];
		$message = 'อนุมัติการกู้เงินเลขที่ '.$row['loan_number'].' ของคุณ '.$row['member_name']." แล้วกรุณาลงนามในสัญญาค้ำประกันแล้วส่งกลับสหกรณ์ หากไม่ประสงค์ค้ำประกันติดต่อ ".$row['admin_tel']."  ดาวน์โหลดสัญญาคลิกที่นี่";
		send_sms($bondsman_tel,$message);
		$data_get = base64_encode("loan_application_id=".$row_bondsman['loan_application_id'])."&".base64_encode("bondsman_id=".$row_bondsman['bondsman_id']);
		$message = BASEPATH."/loan_guarantee_contact_pdf.php?".$data_get;
		send_sms($bondsman_tel,$message);
	}
	//exit;
	echo "<script>document.location.href='?do=status&loan_id=".$_POST['loan_id']."&type=".$row['loan_application_type']."&type_view=all'</script>";
}
if($_GET['do'] == 'loan_status'){

	$sql_loan = "SELECT t1.member_tel,t1.loan_application_type,t2.admin_name,t3.loan_number,t1.responsible_person_id
			FROM loan_application t1 
		LEFT JOIN loan_admin t2 ON t1.responsible_person_id = t2.admin_id
		LEFT JOIN cmp_admin_loan_money t3 ON t1.loan_id = t3.loan_id
		WHERE t1.loan_id = '".$_GET['loan_id']."'
	";
	$rs_loan = $mysqli->query($sql_loan);
	$row_loan = $rs_loan->fetch_assoc();


	$sql_next_respon = "SELECT admin_id,admin_tel,admin_name FROM loan_admin WHERE admin_id <> ".$row_loan['responsible_person_id']." AND admin_department_id = '2' ORDER BY admin_id ASC limit 1";
	//echo $sql_next_respon;exit;
	$rs_next_respon = $mysqli->query($sql_next_respon);
	$row_next_respon = $rs_next_respon->fetch_assoc();
	
	$sql = "UPDATE loan_application SET loan_status = '".$_GET['status_to']."' , approve_person_admin_id  = '".$row_next_respon['admin_id']."'  WHERE loan_id = '".$_GET['loan_id']."'";
	$mysqli->query($sql);
	// echo $sql;
	// exit;
	
	$message = "เลขที่คำขอกู้ ".$row_loan['loan_number']." เจ้าหน้าที่สินเชื่อคุณ ".$row_loan['admin_name']." รับทราบข้อมูลการกู้ของท่านแล้วอยู่ระหว่างดำเนินการวิเคราะห์ข้อมูลของท่าน";
	send_sms($row_loan['member_tel'],$message);
	$send_status = "insert cmp_admin_loan_money_status set
		loan_id = '".$_GET['loan_id']."',
		status_name = 'ส่งให้ผู้ขอกู้ : ".$message."',
		status_date = NOW(),
		loan_application_type = '".$row_loan['loan_application_type']."'
	"; 
	$mysqli->query($send_status);


	$sql = "update cmp_admin_loan_money set
		sms_receive_loan = '1',
		sms_receive_loan_date = NOW()
	WHERE
		loan_id = '".$_GET['loan_id']."'
	"; 
	
	// //start ส่ง sms หาผู้ค้ำ 
	// 	$message = "ส่ง SMS ถึงผู้ค้ำประกัน รอการยืนยันจากผู้ค้ำประกัน";
	// 	$send_status = "insert cmp_admin_loan_money_status set
	// 		loan_id = '".$_GET['loan_id']."',
	// 		status_name = '".$message."',
	// 		status_date = NOW(),
	// 		loan_application_type = '".$row_loan['loan_application_type']."'
	// 	"; 
	// $mysqli->query($send_status);
	// if ($mysqli) {
	// 	$sql_bondsman = "SELECT bondsman_id,bondsman_name,bondsman_tel,bondsman_num FROM loan_application_bondsman WHERE loan_id = '".$_GET['loan_id']."' ORDER BY bondsman_id ASC ";
	// 	$rs_bondsman = $mysqli->query($sql_bondsman); 

	// 	while($row_bondsman = $rs_bondsman->fetch_assoc()){ 
	// 		$bondsman_num = $row_bondsman['bondsman_num'];
	// 		$bondsman_id = $row_bondsman['bondsman_id'];
	// 		$tel = $row_bondsman['bondsman_tel'];
	// 		$message = $name_member."ได้ทำการขอกู้เงินโดยให้ท่านคลิกลิงก์ที่แนบมาเพื่อยืนยันการเป็นผู้ค้ำประกัน ".BASEPATH."/loan_bondsman_confirm.php?loan_id=".$_GET['loan_id']."&bondsman_id=".$bondsman_id;
	// 		send_sms($row_bondsman['bondsman_tel'],$message);
			
			
	// 		$message2 = $row_bondsman['bondsman_name']." ผู้ค้ำประกันคนที่ ".$bondsman_num." ยังไม่ได้กดยอมรับ";
	// 		$send_status = "insert cmp_admin_loan_money_status set
	// 			loan_id = '".$_GET['loan_id']."',
	// 			status_name = '".$message2."',
	// 			status_date = NOW(),
	// 			loan_application_type = '".$row_loan['loan_application_type']."',
	// 			bondsman_id = '".$bondsman_id."'
	// 		"; 
	// 		$mysqli->query($send_status);		
	// 	}		
	// }
	// //end ส่ง sms หาผู้ค้ำ 
	echo "<script>document.location.href='?do=status&loan_id=".$_GET['loan_id']."&type=".$row_loan['loan_application_type']."&type_view=".$_GET['type_view']."'</script>";
	exit;
}
if ($_POST['doc']) {
			$loan_application_id = @$mysqli->real_escape_string(@$_POST["loan_application_id"]);
			$loan_id             = @$mysqli->real_escape_string(@$_POST["loan_id"]);
			$input2              = $_FILES["document_pic"];
			for($i = 0 ; $i < (count($input2["name"])) ; $i ++ ){
			if(!empty($input2["tmp_name"][$i])) {
				if($input2["type"][$i] == "image/png"||$input2["type"][$i] =="image/x-png"){
					$item_path2 = $input2["name"][$i];
					$rename = strtolower(random_char(30)) . '.png';
				}elseif($input2["type"][$i] == "image/gif"){
					$item_path2 = $input2["name"][$i];
					$rename = strtolower(random_char(30)) . '.gif';
				}elseif($input2["type"][$i] == "image/pjpeg"||$input2["type"][$i] =="image/jpeg"){
					$item_path2 = $input2["name"][$i];
					$rename = strtolower(random_char(30)) . '.jpg';
				}elseif($input2["type"][$i] == "application/pdf") {
					$item_path2 = $input2["name"][$i];
					$rename = strtolower(random_char(30)) . '.pdf';
				}elseif($input2["type"][$i] == "application/msword" || $input2["type"][$i] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
					$item_path2 = $input2["name"][$i];
					$rename = strtolower(random_char(30)) . '.docx';
				}
				if(!empty($item_path2)) {
					move_uploaded_file($input2["tmp_name"][$i], "../loan_doc/" . $rename);
					$sql_doc = "INSERT INTO loan_application_doc ( loan_application_id , loan_id, doc_name, file, doc_date) VALUES ( '{$loan_application_id}','{$loan_id}','{$item_path2}','{$rename}', Now())";
				$mysqli->query($sql_doc);
				}
			}
		}
		echo "<script type=\"text/javascript\">alert('ส่งเอกสารสัญญาเงินกู้เรียบร้อยแล้ว');</script>";
}

if (in_array($_POST['do'], array('sendSMS'))) {
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);
	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$loan_application_type = $mysqli->real_escape_string($_POST['loan_application_type']);

	if (fn_send_sms($telmobile, $loan_id,$application_type)) {
		echo 'true';exit();
	}
	echo 'false';exit();
}
else if (in_array($_POST['do'], array('delfile'))) {
	$file = $mysqli->real_escape_string($_POST['file']);

	if (unlink('../letters/' . $file)) {
		$sql = "DELETE FROM cmp_letters WHERE file = '$file'";
		if ($mysqli->query($sql)) {
			echo 'true';exit();
		}
	}
	echo 'false';exit();
}
else if (in_array($_GET['do'], array('add')) && isset($_POST['member_id'])) {

	$sql_number = "SELECT loan_number
						FROM cmp_admin_loan_money
						ORDER BY loan_number DESC
						LIMIT 1";
			$rs_number = $mysqli->query($sql_number);
			if($row_number = $rs_number->fetch_assoc()) {
				$id = (int)$row_number["loan_number"];
				$loan_number = sprintf("%06d", $id + 1);
			}


	$member_id = $mysqli->real_escape_string($_POST['member_id']);
	$member_name = $mysqli->real_escape_string($_POST['member_name']);
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);
	$loan_application_type = $mysqli->real_escape_string($_POST['loan_application_type']);
	$email = $mysqli->real_escape_string($_POST['email']);

	$sql = "INSERT INTO cmp_admin_loan_money (loan_date, member_id, member_name, telmobile,loan_application_type,email,loan_number) VALUES (NOW(), '$member_id', '$member_name', '$telmobile',$loan_application_type,'$email','$loan_number')";
	

	if (!$mysqli->query($sql)) {
		header("Location: loan_money.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
		exit();
	}

	$lastid = $mysqli->insert_id;

	// if ($_POST['saveandsend'] == 'checked') {
		if (!fn_send_sms($telmobile, $lastid ,$loan_application_type,$member_id)) {
			header("Location: loan_money.php?err=" . urlencode('เกิดข้อผิดพลาดในการส่ง SMS'));
			exit();
		}
	// }

		if ($loan_application_type == 1) {
			$link = "http://loan.cmcoop.or.th/common/$lastid/1/$member_id";
		}else if ($loan_application_type == 2) {
			$link = "http://loan.cmcoop.or.th/atm/$lastid/2/$member_id";
		}else{
			$link = "http://loan.cmcoop.or.th/emer/$lastid/3/$member_id";
		}
				if ($email != "") {

					$msg = 'สวัสดี '.$member_name.' ,<br />
						<br />
						<br />
						<a target="_blank" href="'.$link.'">'.$link.'</a>
						<br />
						ขอขอบคุณ<br />
						ทีมงาน <strong>cmcoop.or.th</strong>';
					@send_mj_mail("คำขอกู้เงิน  " , $msg , $email );
					
				}

	// header("Location: loan_money.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
	header("Location: loan_money.php?res=บันทึกข้อมูลแล้ว&view=all");
	exit();

}
else if (in_array($_GET['do'], array('edit')) && isset($_POST['loan_id'])) {
	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$member_id = $mysqli->real_escape_string($_POST['member_id']);
	$member_name = $mysqli->real_escape_string($_POST['member_name']);
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);

	// update cmp_admin_loan_money
		$sql = "UPDATE cmp_admin_loan_money
				SET member_id = '$member_id', member_name = '$member_name', telmobile = '$telmobile'
				WHERE loan_id = $loan_id";
		if (!$mysqli->query($sql)) {
			header('Location: loan_money.php?err=เกิดข้อผิดพลาดในการแก้ไขข้อมูล');
			exit();
		}

		// if ($_POST['saveandsend'] == 'checked') {
		// 	if (!fn_send_sms($telmobile, $lastid)) {
		// 		header("Location: loan_money.php?res=เกิดข้อผิดพลาดในการบันทึกสถานะ SMS");
		// 		exit();
		// 	}
		// }

	header('Location: loan_money.php?res=แก้ไขข้อมูลแล้ว');
	exit();

}
else if (in_array($_GET['do'], array('del'))) {
	
	$loan_id = $mysqli->real_escape_string($_GET['loan_id']);
    $file = $mysqli->real_escape_string($_GET['file']);
    unlink('../MyPDF/' . $file);

    $sql_img = "SELECT * FROM loan_application_doc WHERE loan_id = '$loan_id' ";
	$rs_img = $mysqli->query($sql_img);
	echo $mysqli->error;
	while(($img = $rs_img->fetch_assoc())) {
    unlink('../loan_doc/' . $img['file']);
	}
	$del_doc = "DELETE FROM loan_application_doc WHERE loan_id = '$loan_id'";
    $mysqli->query($del_doc);

	$del_status = "DELETE FROM cmp_admin_loan_money_status WHERE loan_id = '$loan_id'";
    $mysqli->query($del_status);

    $del_application = "DELETE FROM loan_application WHERE loan_id = '$loan_id'";
    $mysqli->query($del_application);

    $del_bondsman = "DELETE FROM loan_application_bondsman WHERE loan_id = '$loan_id'";
    $mysqli->query($del_bondsman);


	$sql = "DELETE FROM cmp_admin_loan_money WHERE loan_id = '$loan_id'";

	if (!$mysqli->query($sql)) {
		header('Location: loan_money.php?err=เกิดข้อผิดพลาดในการลบข้อมูล');
		exit();
	}

	header('Location: loan_money.php?view='.$_GET['view'].'&res=ลบข้อมูลแล้ว');
	exit();

}
else if (in_array($_POST['do'], array('sms_bondsman'))) {

	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$application_id = $mysqli->real_escape_string($_POST['application_id']);
	$name_member = $mysqli->real_escape_string($_POST['name_member']);
	$doc_type = $mysqli->real_escape_string($_POST['doc_type']);

	if ($doc_type == 1) {
		$doc_name = "คำขอกู้เงินสามัญ";
	}else if ($doc_type == 2) {
		$doc_name = "คำขอกู้เงินสามัญ ATM";
	}else{
		$doc_name = "คำขอกู้เพื่อเหตุฉุกเฉิน";
	}

	$now = date("Y-m-d H:i:s");
	$send_status = "insert cmp_admin_loan_money_status set
								loan_id='{$loan_id}',
								status_name='สหกรณ์ ส่ง SMS ให้ผู้ค้ำประกันยืนยันแล้ว',
								status_date='{$now}',
								loan_application_type ='{$doc_type}'
								"; 
	$mysqli->query($send_status);

	if ($mysqli) {

		$sql_bondsman = "SELECT bondsman_id,bondsman_tel FROM loan_application_bondsman WHERE loan_id = $loan_id AND loan_application_id = $application_id ORDER BY bondsman_id ASC ";
		$rs_bondsman = $mysqli->query($sql_bondsman); 
		while($row_bondsman = $rs_bondsman->fetch_assoc()){ 
		$bondsman_id = $row_bondsman['bondsman_id'];
		$tel = $row_bondsman['bondsman_tel'];
		$message = $name_member."ได้ทำการขอกู้เงินโดยให้ท่านคลิกลิงก์ที่แนบมาเพื่อยืนยันการเป็นผู้ค้ำประกัน http://loan.cmcoop.or.th/confirm/".$loan_id."/".$application_id."/".$bondsman_id."";
		// $message = "คุณ ".$name_member." ได้ทำการขอกู้เงินประเภท ".$doc_name."  กับสหกรณ์ออมทรัพย์ครูเชียงใหม่ โดยให้ท่านเป็นผู้ค้ำประกัน คลิกลิงก์ที่แนบมาเพื่อยืนยันการเป็นผู้ค้ำประกัน http://loan.cmcoop.or.th/confirm.php?loan=".$loan_id."&app=".$application_id."&bon=".$bondsman_id." ";
			fn_send_bondsman($tel,$message,$bondsman_id,$loan_id,$doc_type);
		}
		echo 'true'; exit();
	}
} 
else if (in_array($_POST['do'], array('send_sms'))) {
	$loan_id              = $mysqli->real_escape_string($_POST['loan_id']);
	$tel_member           = $mysqli->real_escape_string($_POST['tel_member']);
	$send_type            = $mysqli->real_escape_string($_POST['send_type']);
	$doc_type             = $mysqli->real_escape_string($_POST['doc_type']);
	$not_approve_message  = $mysqli->real_escape_string($_POST['not_approve_message']);
	
	$sql = "SELECT loan_number FROM cmp_admin_loan_money WHERE loan_id = '".$_POST['loan_id']."'";
	$rs = $mysqli->query($sql); 
	$row = $rs->fetch_assoc();
	
	if ($send_type == "check_doc") {
			if ($doc_type == 3) {
				$message = "คำขอของท่านได้รับการตรวจสอบแล้ว";
			}else{
				$message = "คำขอของท่านได้รับการตรวจสอบแล้ว";
				// $message = "เอกสารของท่านได้รับการตรวจสอบแล้ว เมื่อผ่านการตรวจแล้วจะส่ง SMS ยืนยันไปหาผู้ค้ำประกันที่ท่านได้กรอกไว้";
			}
	} else if($send_type == "contract_doc"){
		$message = "สหกรณ์อนุมัติเงินกู้ของท่านแล้ว ดาวน์โหลดสัญญาได้ในเมนูรับสัญญาเงินกู้";
	}else if($send_type == "send_doc"){
		$message = "สหกรณ์ได้รับสัญญาและเอกสารตัวจริงแล้ว กำลังพิจารณาความถูกต้อง";
	}else if($send_type == "send_approve"){
		$message = "สหกรณ์อนุมัติเงินกู้เสร็จสมบูรณ์และทำการโอนเงินเรียบร้อย";
	}else{
		$message = "คำร้องขอกู้เงินเลขที่ ".$row['loan_number']." ไม่ผ่านการพิจารณา เนื่องจาก".$not_approve_message;
	}

	if (fn_sms_member($tel_member,$message,$send_type,$loan_id,$doc_type)) {
		echo 'true';
		exit();
	}

} 
else if (in_array($_POST['do'], array('bondsman_alone'))){

	//if($_POST){
	//	echo '<pre>'; print_r($_POST); echo '</pre>';
	//	exit;		
	//}


	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$type = $mysqli->real_escape_string($_POST['application_id']);
	$tel_member = $mysqli->real_escape_string($_POST['bondsman_tel']);
	$message = $mysqli->real_escape_string($_POST['bondsman_message']);
	$bondsman_id = $mysqli->real_escape_string($_POST['bondsman_id']);

	if (fn_send_bondsman_alone($tel_member,$message,$bondsman_id)) {
		
		echo '<script> alert("ดำเนินการส่ง SMS แล้ว"); </script>';
        echo "<script> window.location.href = \"?do=status&loan_id=".$loan_id."&type=".$type."&type_view=".$_POST['type_view']."\"</script>" ;
        exit();
	}
		echo '<script> alert("ไม่สำเร็จกรุณาลองใหม่อีกครั้ง"); </script>';
	 	echo "<script> window.location.href = \"?do=status&loan_id=".$loan_id."&type=".$type."&type_view=".$_POST['type_view']."\"</script>" ;
        exit();



}
else if (in_array($_POST['do'], array('sms_head'))) {

	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$application_id = $mysqli->real_escape_string($_POST['application_id']);
	$doc_type = $mysqli->real_escape_string($_POST['doc_type']);
	$name_member = $mysqli->real_escape_string($_POST['name_member']);
	$haed_tel = $mysqli->real_escape_string($_POST['haed_tel']);
	$message = $name_member."ได้ทำการขอกู้เงินฉุกเฉิน โดยให้ท่านคลิกลิงก์ที่แนบมาเพื่อยืนยันการขอกู้เงินฉุกเฉิน
 http://loan.cmcoop.or.th/confirmh/".$loan_id."/".$application_id."/";


	// var_dump(fn_send_head($haed_tel,$message,$loan_id)); die();
	if (fn_send_head($haed_tel,$message,$loan_id)) {
		echo 'true';
		exit();
	}

}
else if (in_array($_POST['do'], array('sms_head_again'))) {

	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$doc_type = $mysqli->real_escape_string($_POST['doc_type']);
	$haed_tel = $mysqli->real_escape_string($_POST['haed_tel']);
	$message = $mysqli->real_escape_string($_POST['message']);


	// var_dump(fn_send_head($haed_tel,$message,$loan_id)); die();
	if (sms_bondsman($haed_tel,$message)) {

			$now = date("Y-m-d H:i:s");
			$sql = "update cmp_admin_loan_money set
	                                dep_head_sms_date_update='{$now}'
	                                WHERE
	                                loan_id = '{$loan_id}'
	        "; 

			if ($mysqli->query($sql)) {
				echo '<script> alert("ดำเนินการส่ง SMS แล้ว"); </script>';
        		echo "<script> window.location.href = \"?do=status&loan_id=".$loan_id."&type=".$doc_type."&type_view=".$_POST['type_view']."\"</script>" ;
			        exit();
			
			}else{
				echo '<script> alert("ไม่สำเร็จกรุณาลองใหม่อีกครั้ง"); </script>';
				echo "<script> window.location.href = \"?do=status&loan_id=".$loan_id."&type=".$doc_type."&type_view=".$_POST['type_view']."\"</script>" ;
			        exit();
			}

	}

} 
else if (in_array($_POST['do'], array('delete_doc'))) {
	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$sql_img = "SELECT * FROM loan_application_doc WHERE loan_id = '$loan_id' ";
	$rs_img = $mysqli->query($sql_img);
	echo $mysqli->error;
	while(($img = $rs_img->fetch_assoc())) {
    unlink('../loan_doc/' . $img['file']);
	}
	$del_doc = "DELETE FROM loan_application_doc WHERE loan_id = '$loan_id'";
    $mysqli->query($del_doc);
	echo 'true';
	exit();
}
{

ob_start();
?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
	.bt_dis{
		cursor: not-allowed !important;
		pointer-events: auto !important;
	}
	.not_line{
		text-decoration: none !important;
	}

	.section {
		clear: both;
		padding: 0px;
		margin: 0px;
	}

	/*  COLUMN SETUP  */
	.col {
		display: block;
		float:left;
		margin: 1% 0 1% 1%;
	}
	.col:first-child { margin-left: 0; }

	/*  GROUPING  */
	.group:before,
	.group:after { content:""; display:table; }
	.group:after { clear:both;}
	.group { zoom:1; /* For IE 6/7 */ }
	/*  GRID OF FIVE  */
	.span_5_of_5 {
		width: 100%;
	}
	.span_4_of_5 {
		width: 79.8%;
	}
	.span_3_of_5 {
		width: 59.6%;
	}
	.span_2_of_5 {
		width: 39.4%;
	}
	.span_1_of_5 {
		/* width: 19.2%; */
		width: 24.2%;
	}

	/*  GO FULL WIDTH BELOW 480 PIXELS */
	@media only screen and (max-width: 480px) {
		.col {  margin: 1% 0 1% 0%; }
		.span_1_of_5, .span_2_of_5, .span_3_of_5, .span_4_of_5, .span_5_of_5 { width: 100%; }
	}
	.btn-primary {
		background: #20449a;
		color: #fff;
	}
	.btn-primary.disabled {
		background: #20449a;
		color: #fff;
		border-color: #fff;
	}
	.btn-primary:hover,.btn-primary:focus,.btn-primary:active,.btn-group.open .btn-primary.dropdown-toggle,.btn.btn-primary.active{
		background-color:#1f3b7d;
		border:1px solid transparent;
	}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
	ob_start();
?>
<script src="/scripts/validation/js/formValidation.js"></script>
<script src="/scripts/validation/js/bootstrap.js"></script>
<script src="/scripts/filestyle/bootstrap-filestyle.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<!-- <script src="/html/meter/admin1/assets/js/pages/dashboard.js"></script> -->
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script>
$(document).ready(function() {
$.uniform.restore();

    $('#checkAll').change(function(){
        var checkboxes = $(this).closest('#lsit_del').find(':checkbox');
        if($(this).prop('checked')) {
          checkboxes.prop('checked', true);
        } else {
          checkboxes.prop('checked', false);
        }
    });

		
	$('#addFile').on('click', function() {
		$('<input>').attr({
			type: 'file',
			class: 'form-control',
			name: 'letter[]'}).insertBefore($(this).parent()).trigger('click');
	});

	$('.btndel').click(function() {
		var obj = $(this).parents('p');

		if (confirm('ต้องการลบไฟล์นี้')) {
			$.ajax({
				type	: 'POST',
				url	: 'loan_money.php',
				data	: {
					'do'	 : 'delfile',
					'file' : $(this).prop('name')
				},
				success : function(response) {
					if (response === 'true') {
						obj.remove();
					}
				}
			});
		}
	});

	$('#loan-form').formValidation({
		fields: {
			member_id: {
				validators: {
					notEmpty	: { message: '<p>กรุณากรอกรหัสสมาชิก</p>' },
					stringLength: { message: '<p>กรุณาป้อนรหัสอย่างน้อย 6 ตัว', min: 6 },
					regexp	: { message: '<p>กรุณาป้อนตัวเลขเท่านั้น</p>', regexp: /\d$/ }
				}
			},
			member_name: {
				validators: {
					notEmpty	: { message: '<p>กรุณาป้อนชื่อสมาชิก</p>' },
					regexp	: { message: '<p>กรุณาป้อนตัวอักษรเท่านั้น</p>', regexp: /[a-zก-ฮ\W]$/ }
				}
			},
			telmobile: {
				validators: {
					notEmpty	: { message: '<p>กรุณาป้อนเบอร์โทรศัพท์</p>' },
					regexp	: { message: '<p>กรุณาป้อนเบอร์โทรศัพท์</p>', regexp: /^0[8|9].[0-9]{7}$/ }
				}
			},
			// email: {
			// 	validators: {
			// 		notEmpty	: { message: '<p>กรุณาป้อน email</p>' },
			// 		regexp	: { message: '<p>กรุณาป้อน email</p>', regexp: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/ }
			// 	}
			// },
			loan_application_type: {
				validators: {
					notEmpty	: { message: '<p>กรุณาเลือกรูปแบบการกู้เงิน</p>' }
				}
			}
		}
	}).on('status.field.fv', function(e, data) {
		if (data.field === 'telmobile') {
			(data.status === 'VALID' && $('#loan_id').val() !== '')
			    ? $('#btn-sms').removeAttr('disabled')
			    : $('#btn-sms').prop('disabled', 'disabled')
		}
	});

	$('#btn-sms').on('click', function() {
		var obj = $(this);
		var loan_application_type = $('input[name=loan_application_type]:checked').val();

		$.ajax({
			type	: 'POST',
			url	: 'loan_money.php',
			data	: {
				'do'		: 'sendSMS',
				'loan_id'	: $('#loan_id').val(),
				'telmobile'	: $('#telmobile').val(),
				'loan_application_type'	: loan_application_type,
			},
			// async: false,
			success : function(response) {
				var res = '';
				if (response == 'true') {
					res = 'ดำเนินการส่ง SMS แล้ว';
					$('.blgres').text(res).addClass('bg-success');
				} else {
					res = 'เกิดข้อผิดพลาดในการส่ง SMS';
					$('.blgres').text(res).addClass('bg-danger');
				}
			}
		});
	});

	$('#sms_bondsman').on('click', function() {
		var confirm_bondsman = confirm('ต้องการที่จะส่ง SMS ถึงผู้ค้ำประกันทั้งหมด');

		var loan_id = $(this).attr('loan_id');
		var application_id = $(this).attr('application_id');
		var name_member = $(this).attr('name_member');
		var doc_type = $(this).attr('doc_type');

		if (confirm_bondsman) {
			$.ajax({
			type	: 'POST',
			url	: 'loan_money.php',
			data	: {
				'do'		: 'sms_bondsman',
				'loan_id'	: loan_id,
				'application_id'	: application_id,
				'name_member' : name_member,
				'doc_type' : doc_type
			},
			// async: false,
			success : function(msg) {
				// console.log(msg); return false;
				if (msg == 'true') {
					alert("ดำเนินการส่ง SMS ถึงผู้คำประกันแล้ว");
					location.reload();
				} else {
					alert("เกิดข้อผิดพลาดในการส่ง SMS");
					location.reload();
				}
				
			}
		});
		} 
		
	});

	$('#sms_head').on('click', function() {
		var confirm_bondsman = confirm('ต้องการที่จะส่ง SMS ถึงหัวหน้าหน่วยบริการ');

		var loan_id = $(this).attr('loan_id');
		var application_id = $(this).attr('application_id');
		var name_member = $(this).attr('name_member');
		var doc_type = $(this).attr('doc_type');
		var haed_tel = $(this).attr('haed_tel');

		if (confirm_bondsman) {
			$.ajax({
			type	: 'POST',
			url	: 'loan_money.php',
			data	: {
				'do'		: 'sms_head',
				'loan_id'	: loan_id,
				'application_id'	: application_id,
				'name_member' : name_member,
				'doc_type' : doc_type,
				'haed_tel' : haed_tel
			},
			// async: false,
			success : function(msg) {
				// console.log(msg); return false;
				if (msg == 'true') {
					alert("ดำเนินการส่ง SMS ถึงหัวหน้าหน่วยบริการเรียบร้อยแล้ว");
					location.reload();
				} else {
					alert("เกิดข้อผิดพลาดในการส่ง SMS");
					location.reload();
				}
				
			}
		});
		} 
		
	});

	$('.coop_send').on('click', function() {
		var confirm_sms = confirm('ต้องการที่จะส่ง SMS ถึงผู้กู้สัญญา');
		var loan_id    = $(this).attr('loan_id');
		var tel_member = $(this).attr('tel_member');
		var send_type  = $(this).attr('send_type');
		var doc_type   = $(this).attr('doc_type');

		if (confirm_sms) {
			$.ajax({
			type	: 'POST',
			url	: 'loan_money.php',
			data	: {
				'do'		 : 'send_sms',
				'loan_id'	 : loan_id,
				'doc_type'	 : doc_type,
				'tel_member' : tel_member,
				'send_type'  : send_type
			},
			// async: false,
			success : function(msg) {
				console.log(msg);
				// var msg = '';
				if (msg == 'true') {
					alert("ดำเนินการส่ง SMS แล้ว")
					location.reload();
				} else {
					alert("เกิดข้อผิดพลาดในการส่ง SMS")
					location.reload();
				}
			}
		});
		} 
		
	});

	$('.not_approve').on('click', function() {

		var not_approve_message = $("#not_approve_message").val();
		
		if (not_approve_message == "") {

			alert("กรุณากรอกข้อความ");
			return false;

		}else{


		//var confirm_sms = confirm('ต้องการที่จะไม่อนุมัติคำขอกู้');

		var send_type = $(this).attr('send_type');
		var loan_id = $(this).attr('loan_id');
		var tel_member = $(this).attr('tel_member');
		var doc_type = $(this).attr('doc_type');
		
		swal({
			title: "",
			text: "ท่านไม่อนุมัติคำร้องขอกู้เงินใช้หรือไม่?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#20449a',
			confirmButtonText: 'ยืนยัน',
			cancelButtonText: "ยกเลิก",
			closeOnConfirm: false,
			closeOnCancel: true
		 },
		 function(isConfirm){
		   if (isConfirm){
			 $.ajax({
					type	: 'POST',
					url	: 'loan_money.php',
					data	: {
						'do'		: 'send_sms',
						'loan_id'	: loan_id,
						'doc_type'	: doc_type,
						'tel_member' : tel_member,
						'send_type' : send_type,
						'not_approve_message' : not_approve_message
					},
					// async: false,
					success : function(msg) {
						// var msg = '';
						if (msg == 'true') {
							swal("ดำเนินการส่ง SMS แล้ว")
							location.reload();
						} else {
							swal("เกิดข้อผิดพลาดในการส่ง SMS")
							location.reload();
						}
					}
				});

			} else {
			}
		 });

		}
		
	});

	});

 $('#search_mem').keyup(function(){  
           var txt = $(this).val();  
           if(txt != '')  
           {  
                $.ajax({  
                     url:"/ajax/ajax_search_member.php",  
                     method:"post",  
                     data:{search:txt},  
                     dataType:"text",  
                     success:function(data)  
                     {  
                     // console.log(data); 
                      $('#result_member').html(data);  
                     }  
                });  
           }else{
            
           }
      });




 $(document).on('click', '.add_member', function(e) {

	var id_mem = $(this).attr('id_mem');
 	var name_mem = $(this).attr('name_mem');
 	var emer_mem = $(this).attr('email_mem');
 	var tel_mem = $(this).attr('tel_mem');

 	$("#member_id").val(id_mem);
 	$("#member_name").val(name_mem);
 	$("#telmobile").val(tel_mem);
 	$("#email").val(emer_mem);

 	$("#search_mem").val('');
    $('#result_member').html("");  
 	$('#myModal').modal('toggle');

});

// subscribe to the "update.maxlength" event
$("textarea").bind("update.maxlength", function(event, element, lastLength, length, maxLength, left){
    // console.log(event, element, lastLength, length, maxLength, left);
});

$("#various3").fancybox({
				'width'				: '75%',
				'height'			: '75%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});

$('#btn_doc').on('click', function() {
	var confirm_doc = confirm('ต้องการที่จะลบเอกสารใช่หรือไม่');
	var loan_id = $(this).data('doc');
	if (confirm_doc) {
		$.ajax({
		type	: 'POST',
		url	    : '?',
		data	: {
			'do'	  : 'delete_doc',
			'loan_id' : loan_id,
		},
		success : function(msg) {
			console.log(msg);
			if (msg == 'true') {
				alert("ลบเอกสารเรียบร้อยแล้ว")
				location.reload();
			} else {
				alert("เกิดข้อผิดพลาดในการลบเอกสาร")
				location.reload();
			}
		}
	});
	} 	
});
			
</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$page = (int) @$_GET["page"]  ;
	$smarty->assign("title", "ระบบคำขอกู้เงิน" );
	// $smarty->assign("page_title", "ระบบคำขอกู้เงิน" );

	ob_start();
?>

<?php 	
	// del_list

	if ($_POST['del_list']) {


		foreach ($_POST['check_del'] as $val_loan =>  $app) {

				$file = "PDF_".$val_loan."_".$app.".pdf";
    			unlink('../MyPDF/' . $file);

			    $sql_img = "SELECT * FROM loan_application_doc WHERE loan_id = '$val_loan' ";
				$rs_img = $mysqli->query($sql_img);
				echo $mysqli->error;
				while(($img = $rs_img->fetch_assoc())) {
			    unlink('../loan_doc/' . $img['file']);
				}

				$del_doc = "DELETE FROM loan_application_doc WHERE loan_id = '$val_loan'";
			    $mysqli->query($del_doc);

				$del_status = "DELETE FROM cmp_admin_loan_money_status WHERE loan_id = '$val_loan'";
			    $mysqli->query($del_status);

			    $del_application = "DELETE FROM loan_application WHERE loan_id = '$val_loan'";
			    $mysqli->query($del_application);

			    $del_bondsman = "DELETE FROM loan_application_bondsman WHERE loan_id = '$val_loan'";
			    $mysqli->query($del_bondsman);


				$sql = "DELETE FROM cmp_admin_loan_money WHERE loan_id = '$val_loan'";
				
				}

				if (!$mysqli->query($sql)) {
					header('Location: loan_money.php?view='.$_POST['view'].'&err=เกิดข้อผิดพลาดในการลบข้อมูล');
					exit();
				}

				header('Location: loan_money.php?view='.$_POST['view'].'&res=ลบข้อมูลแล้ว');
				exit();


		} 

	// del_list


	function percen($num,$maxnum){

		return $percen = ($num / $maxnum) * 100;

	}
	
	function count_type($type) {

	global $mysqli;

	if ($type == "all") {

	// all
	$sql_count_all = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    		 cmp_admin_loan_money_status.loan_application_type
						    ";
	$rs_all = $mysqli->query($sql_count_all);
	$count_all = $rs_all->fetch_assoc();
	$count = $rs_all->num_rows;
	return  $count;
	// all


	}else if ($type == "process") {

	// process
	 $sql_count_process = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    WHERE cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0 AND cmp_admin_loan_money.c_sms_b_status = 0
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    cmp_admin_loan_money_status.loan_application_type
						    ";
	// $sql_count_process = " SELECT COUNT(*) FROM `cmp_admin_loan_money` WHERE not_approve = 0 AND c_approve = 0 AND c_sms_b_status = 0";
	$rs_process = $mysqli->query($sql_count_process);
	$count_process = $rs_process->fetch_assoc();
	$count = $rs_process->num_rows;
	return  $count;
	// process

	}else if ($type == "bondsman"){

	// bondsman
	$sql_count_bondsman = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    WHERE cmp_admin_loan_money.c_sms_b_status = 1 AND cmp_admin_loan_money.c_sms_h_status = 1 AND cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    cmp_admin_loan_money_status.loan_application_type
						    ";
	// $sql_count_bondsman = " SELECT COUNT(*) FROM `cmp_admin_loan_money` WHERE c_sms_b_status = 1 AND not_approve = 0 AND c_approve = 0";
	$rs_bondsman = $mysqli->query($sql_count_bondsman);
	$count_bondsman = $rs_bondsman->fetch_assoc();
	$count = $rs_bondsman->num_rows;
	return  $count;
	// bondsman

	}else if($type == "not_approve"){

	// not approve
	// $sql_not_approve = " SELECT COUNT(*) FROM `cmp_admin_loan_money` WHERE not_approve = 1";
	$sql_not_approve = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    WHERE cmp_admin_loan_money.not_approve = 1
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    cmp_admin_loan_money_status.loan_application_type
						    ";
	$rs_not_approve = $mysqli->query($sql_not_approve);
	$count_not_approve = $rs_not_approve->fetch_assoc();
	$count = $rs_not_approve->num_rows;
	return  $count;
	// not approve

	}else{

	//  approve
	// $sql_approve = " SELECT COUNT(*) FROM `cmp_admin_loan_money` WHERE c_approve = 1";
	$sql_approve = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    WHERE cmp_admin_loan_money.c_approve = 1
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    cmp_admin_loan_money_status.loan_application_type
						    ";
	$rs_approve = $mysqli->query($sql_approve);
	$count_approve = $rs_approve->fetch_assoc();
	$count = $rs_approve->num_rows;
	return  $count;
	//  approve

	}

	
	}

	
 ?>
				
				<?php if ($_GET['view']) { ?>

					<div class="row" style="margin-left: 0;margin-right: 0;">
						
						<?php

						$set_type  = array(

						'all' => 	  array(
												"color" => "info",
												"title" => "คำขอกู้ทั้งหมด"
										   ),
						'process' =>  array(
												"color" => "primary",
												"title" => "อยู่ในกระบวนการ"
										   ),
						'not_approve' => array(
												"color" => "danger",
												"title" => "ไม่อนุมัติ"
										      ),
						'approve' => array(
												"color" => "success",
												"title" => "อนุมัติแล้ว"
										)
						);

						 ?>
					
						<?php foreach ($set_type as $key => $value) { ?>
						
                        <div class="col span_1_of_5" >
                            <div class="panel info-box panel-white" <?php echo ($key == $_GET['view']) ? "style='background: #eee;'" : "" ; ?>>
                                <div class="panel-body" style="padding: 10px;">
                                    <div class="info-box-stats">
                                        <a class="not_line" href="?view=<?=$key?>&per_view=<?php echo $_GET['per_view']; ?>">
                                        	<div class="info-box-icon">
	                                        	<i class="fa fa-users pull-right"></i>
		                                    </div>
                                        <p class="counter"><?php echo count_type($key); ?>
	                                        
	                                    </p>
                                        <span style="color: #4E5E6A;"  class="info-box-title">
                                        <?=$value['title']?> | ดูรายละเอียด
                                        </span>
                                        </a>
                                    </div>
                                    <div class="info-box-progress">
                                        <div class="progress progress-xs progress-squared bs-n">
                                            <div class="progress-bar progress-bar-<?=$value['color']?>" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:<?=percen(count_type($key),count_type("all"))?>%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                    </div>

                <?php } ?>


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">ระบบคำขอกู้เงิน 
				<?php 
				if(in_array($_GET['do'], array('status'))){
					echo " > สถานะการขอกู้";
				}else{ 
					if($_GET['per_view']=='' || $_GET['per_view']=='own'){
				?>
					(<a href="?view=<?php echo $_GET['view']?>&per_view=all">ดูทั้งหมด</a>)
				<?php 
					}else{ 
				?>
					(<a href="?view=<?php echo $_GET['view']?>&per_view=own">ดูเฉพาะของตนเอง</a>)	
				<?php }
				} ?>
				</h4>
			</div>
			<div class="panel-body">
				<?php	if (in_array($_GET['do'], array('add', 'edit'))) {
					if ($_GET['do'] == 'add') {
						$smarty->assign("breadcrumb", array("ระบบคำขอกู้เงิน" => "loan_money.php?view=all", "เพิ่มข้อมูล" => "#"));
					} else {
						$smarty->assign("breadcrumb", array("ระบบคำขอกู้เงิน" => "loan_money.php?view=all", "แก้ไขข้อมูล" => "#"));

						$sql = "SELECT * FROM cmp_admin_loan_money WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
						$user = $result->fetch_object();

						$sql = "SELECT * FROM cmp_letters WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
					}
				?>
					<form action="" class="form-horizontal" id="loan-form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="loan_id" id="loan_id" value="<?= (isset($user->loan_id)) ? $user->loan_id : '' ?>">
						<div class="form-group">
							<label for="member_id" class="col-sm-3 control-label">รหัสสมาชิก</label>
							<div class="col-sm-4">
								<input type="text" id="member_id" class="form-control" name="member_id" placeholder="รหัสสมาชิก"
									value="<?= (isset($user->member_id)) ? $user->member_id : '' ?>" autofocus>
							</div>
							<div class="col-sm-4">
                            <!-- Button trigger modal -->
							 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i>
                              </button>
                             <!-- Button trigger modal -->
							</div>

						</div>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">ค้นหาสมาชิก</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                       <div class="form-group">
															<label for="search" class="col-sm-1 control-label">ค้นหา</label>
															<div class="col-sm-5">
																<input type="text" id="search_mem" class="form-control" name="search" placeholder="กรอกรหัสสมาชิกหรือชื่อ"
																	value="">
															</div>
														</div>

														<div class="bs-example" data-example-id="striped-table">
															 <table class="table table-striped">

															 <thead>
				                                                <tr>
				                                                    <th width="20px">#</th>
				                                                    <th class="text-center" width="100">รหัสสมาชิก</th>
				                                                    <th  width="300">ชื่อ - สกุล</th>
				                                                </tr>
				                                            </thead> 

										                <tbody id="result_member">
										                
										                </tbody>

														 
															  </table> 
															</div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Modal -->

						<div class="form-group">
							<label for="member_name" class="col-sm-3 control-label">ชื่อสกุล</label>
							<div class="col-sm-4">
								<input type="text" id="member_name" class="form-control" name="member_name"
									value="<?= (isset($user->member_name)) ? $user->member_name : '' ?>" placeholder="ชื่อสกุล">
							</div>
						</div>
						<div class="form-group">
							<label for="telmobile" class="col-sm-3 control-label">มือถือ</label>
							<div class="col-sm-4">
								<input type="text" id="telmobile" class="form-control" name="telmobile" placeholder="มือถือ"
									value="<?= (isset($user->telmobile)) ? $user->telmobile : '' ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="telmobile" class="col-sm-3 control-label">E-mail</label>
							<div class="col-sm-4">
								<input type="text" id="email" class="form-control" name="email" placeholder="email"
									value="<?= (isset($user->email)) ? $user->email : '' ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">รูปแบบการกู้เงิน</label>
							<div class="col-sm-6" id="divfile">
							<label class="radio-inline">
							  <input <?= ($user->loan_application_type == 1) ? "checked" : "" ; ?> type="radio" name="loan_application_type" id="application" value="1"> กู้เงินสามัญ
							</label>
							<label class="radio-inline">
							  <input  <?= ($user->loan_application_type == 2) ? "checked" : "" ; ?> type="radio" name="loan_application_type" id="application" value="2"> กู้เงินสามัญ ATM
							</label>
							<label class="radio-inline">
							  <input  <?= ($user->loan_application_type == 3) ? "checked" : "" ; ?> type="radio" name="loan_application_type" id="application" value="3"> กู้เหตุฉุกเฉิน
							</label>
							</div>
						</div>
						<!-- <div class="form-group">
							<label for="" class="col-sm-3 control-label"></label>
							<div class="col-sm-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="saveandsend" value="checked" <?= (isset($_GET['loan_id'])) ? '' : 'checked' ?>> บันทึกและส่ง SMS
									</label>
								</div>
							</div>
						</div> -->

						<div class="form-group">
							<div class="col-sm-9 control-label">
								<p class="text-center">
									<button type="submit" class="btn btn-success2">บันทึก</button>
									<a href="?view=all" class="btn btn-success2" id="btn-sms">กลับ</a>
								</p>
								<p class="blgres text-center"></p>
							</div>
						</div>
					</form>
				<?php
					} else if ((in_array($_GET['do'], array('status')))) {

						$smarty->assign("breadcrumb", array("ระบบคำขอกู้เงิน" => "loan_money.php?view=all", "สถานะการกู้" => "#"));

						$loan_id = $_GET['loan_id'];
						$doc_type = $_GET['type'];

						$update_status = "update cmp_admin_loan_money_status set
                                status_show='1'
                                WHERE
                                loan_id='{$_GET['loan_id']}'
                                AND
                                loan_application_type = '{$_GET['type']}' 
				        ";
				        $mysqli->query($update_status); 

						$sql = "SELECT * FROM cmp_admin_loan_money WHERE loan_id = '".$loan_id."'";
						$rs = $mysqli->query($sql);
   						echo $mysqli->error;
						$row = $rs->fetch_assoc();
						
					

    					$sql_application = "SELECT loan_application.*, loan_admin.admin_name FROM loan_application 
						INNER JOIN loan_admin ON loan_application.responsible_person_id = loan_admin.admin_id
						WHERE loan_id = '".$loan_id."' AND loan_application_type = '".$doc_type."' ";
						$rs_application = $mysqli->query($sql_application);
   						echo $mysqli->error;
    					$row_application = $rs_application->fetch_assoc();
    					$application_id = $row_application['loan_application_id'];

    						// update pdf
 	 					if($_GET['type'] == 1){

 	 					file_get_contents(BASEPATH."/loan_common_pdf.php?loan=$loan_id&application=$application_id&do=edit");

						}else if($_GET['type'] == 2){ 

 	 					file_get_contents(BASEPATH."/loan_atm_pdf.php?loan=$loan_id&application=$application_id&do=edit");

						}else{ 

 	 					file_get_contents(BASEPATH."/loan_emer_pdf.php?loan=$loan_id&application=$application_id&do=edit");
						}
 	 					// update pdf
						  
						$loan_application_type = array();
						$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
						$rs_type = $mysqli->query($sql_type);
						while($row_type = $rs_type->fetch_assoc()){
							$loan_application_type[$row_type['id']] = $row_type['loan_name'];


							
						}
						$add_money_loan = $row_application["loan_money"] - $row_application["outstanding"];
					?>
					<div class="text-left">

						<div class="col-sm-12">
						<p class="col-sm-2">วันที่</p>
						<p class="col-sm-9"><?= date_time($row["loan_date"], true, false,true);?></p>
						</div>

						<div class="col-sm-12">
						<p class="col-sm-2">รหัสสมาชิก</p>
						<p class="col-sm-9"><?=$row["member_id"]?></p>
						</div>

						<div class="col-sm-12">
						<p class="col-sm-2">จำนวนเงินที่เคยกู้</p>
						<p class="col-sm-9"><?=number_format($row_application["outstanding"])?>  บาท</p>
						</div>
						<div class="col-sm-12">
						<p class="col-sm-2">จำนวนเงินที่ขอกู้เพิ่มเติม</p>
						<p class="col-sm-9"><?=number_format($add_money_loan)?>  บาท</p>
						</div>
						<div class="col-sm-12">
						<p class="col-sm-2">จำนวนเงินที่ขอกู้ทั้งหมด</p>
						<p class="col-sm-9"><?=number_format($row_application["loan_money"])?>  บาท</p>
						</div>
						<div class="col-sm-12">
						<p class="col-sm-2">ชื่อ-สกุล</p>
						<p class="col-sm-9"><?=$row["member_name"]?></p>
						</div>
						
						<div class="col-sm-12">
						<p class="col-sm-2">ประเภทคำขอกู้ : </p>
						<p class="col-sm-9"><?php echo $loan_application_type[$_GET['type']];?></p>
						<!--<?php if($_GET['type'] == 1){ ?>
						<p class="col-sm-2">ประเภทคำขอกู้ : </p>
						<p class="col-sm-9">สามัญ</p>
						<?php }else{ ?>
						<p class="col-sm-2">ประเภทคำขอกู้ : </p>
						<p class="col-sm-9">ฉุกเฉิน</p>
						<?php } ?>
						-->
						</div>
						
						<div class="col-sm-12" >

						<?php if ($row_application['application_status'] == 1) { ?>
						 
						<p class="col-sm-2">เอกสารคำขอกู้</p>
						<p class="col-sm-10">
							<a target="_blank" href="../loan_all_pdf.php?<?php echo base64_encode("loan_application_id=".$row_application['loan_application_id']); ?>">ดาวน์โหลดคำร้องขอกู้เงิน</a>
						  
						<!--
						  <?php if($_GET['type']=='1'){ ?>
						  <a target="_blank" href="../loan_petition_pdf.php?<?php echo base64_encode("loan_application_id=".$row_application['loan_application_id']); ?>">ดาวน์โหลดคำร้องขอกู้เงิน</a>
						  
						  <?php
							$sql_special_beg = "SELECT id FROM loan_special_beg WHERE loan_id = '".$loan_id."'";
							$rs_special_beg = $mysqli->query($sql_special_beg);
							echo $mysqli->error;
							$row_special_beg = $rs_special_beg->fetch_assoc();
							if($row_special_beg['id']!=''){ ?>
							
							/ <a target="_blank" href="../loan_special_beg_pdf.php?id=<?php echo $row_special_beg['id']; ?>">Download คำขอความอนุเคราะห์เป็นกรณีพิเศษ</a> 
														
							<?php } ?>
						<?php }else if($_GET['type']=='3'){ ?>
							<a target="_blank" href="../loan_emergent_pdf.php?<?php echo base64_encode("loan_id=".$loan_id); ?>">ดาวน์โหลดคำร้องขอกู้เงิน</a> 
						<?php } ?>
						-->
						</p>
						
						<p class="col-sm-2">เอกสารสัญญาค้ำประกันเงินกู้</p>
						<p class="col-sm-10">
							<a target="_blank" href="../loan_guarantee_person_pdf.php?<?php echo base64_encode("loan_application_id=".$row_application['loan_application_id']); ?>">ดาวน์โหลดเอกสารสัญญาค้ำประกันเงินกู้</a>
						</p>
						<?php } ?>
						
						</div>
						<?php 
							$sql_file ="SELECT
								loan_application_file.*,
								loan_setting_list_file.list_file_name
							FROM
								loan_application_file
							LEFT JOIN loan_setting_list_file ON loan_application_file.input_name = loan_setting_list_file.list_file_input_name
							WHERE
								loan_id = '".$_GET['loan_id']."'
							ORDER BY
								loan_setting_list_file.order_by,
								loan_application_file.bondsman_num";
							$rs_file = $mysqli->query($sql_file);
							$file_data = array();
							$bondsman_file_data = array();
							$output_dir = "../../uploads/loan/";
							while($row_file = $rs_file->fetch_assoc()){
								if($row_file['bondsman_num']!=''){
									$bondsman_file_data[$row_file['bondsman_num']][$row_file['input_name']][] = $row_file;
								}else{
									$file_data[$row_file['input_name']][] = $row_file;
								}
							}
							//echo"<pre>";print_r($file_data);exit;
							$i=1;
							foreach($file_data as $key => $value){
								$j=1;
								foreach($value as $key2 => $value2){ ?>
									<div class="col-sm-12">
										<p class="col-sm-2"><?php echo $i==1?'เอกสารที่ผู้กู้แนบมา':''?></p>
										<p class="col-sm-4"><?php echo $j==1?'ลำดับที่ '.$i.' '.$value2['list_file_name']:''?></p>
										<p class="col-sm-6"><?php echo 'ไฟล์แนบที่ '.$j." <a href='".HTTP.$_SERVER["HTTP_HOST"]."/loan/admin/watermark.php?img=".$output_dir.$value2['loan_file_name']."' target='_blank'>".$value2['loan_file_old_name']."</a>"; ?></p>
									</div>
							<?php 
								$j++; } ?>
								<div class="col-sm-12">
										<p class="col-sm-2"></p>
										<p class="col-sm-6" style="border-bottom:1px solid; opacity:0.5;"></p>
									</div>
							<?php $i++;} 
							
							foreach($bondsman_file_data as $key => $value){
								foreach($value as $key2 => $value2){
									$j=1;
									foreach($value2 as $key3 => $value3){ ?>
									<div class="col-sm-12">
										<p class="col-sm-2"></p>
										<p class="col-sm-4"><?php echo $j==1?'ลำดับที่ '.$i.' '.$value3['list_file_name']."ลำดับที่ ".$key:''?></p>
										<p class="col-sm-6"><?php echo 'ไฟล์แนบที่ '.$j." <a href='".$output_dir.$value3['loan_file_name']."' target='_blank'>".$value3['loan_file_old_name']."</a>"; ?></p>
									</div>
							<?php 	$j++;} ?>
									<div class="col-sm-12">
										<p class="col-sm-2"></p>
										<p class="col-sm-6" style="border-bottom:1px solid; opacity:0.5;"></p>
									</div>
								<?php $i++;}
							} ?>
							<div class="col-sm-12">
								<p class="col-sm-2">ผู้ค้ำประกัน</p>
								<p class="col-sm-9">
									<?php
										$sql_bondsman = "SELECT 
															t1.bondsman_num,
															t1.loan_application_id, 
															t1.bondsman_id,
															t1.bondsman_tel,
															t1.bondsman_number,
															t1.bondsman_name
														FROM 
															loan_application_bondsman as t1
														WHERE 
															t1.loan_id = '".$_GET['loan_id']."'
															AND t1.bondsman_number != ''
														ORDER BY t1.bondsman_num";	
										$rs_bondsman = $mysqli->query($sql_bondsman);
										
										$check_bondsman = 0;
										while($row_bondsman = $rs_bondsman->fetch_assoc()){	
											echo 'ลำดับที่ '.@$row_bondsman['bondsman_num'].'  '.@$row_bondsman['bondsman_name'].'<br>';
										}
									?>	
								</p>
							</div>
							<div class="col-sm-12">
								<p class="col-sm-2">ผู้รับผิดชอบ</p>
								<p class="col-sm-9"><?php echo $row_application['admin_name']; ?></p>
							</div>
							<?php
							if($row_application["loan_status"] == '0'){
								$disabled1 = '';
								$disabled2 = 'disabled';
								$disabled3 = 'disabled';
								$disabled4 = 'disabled';
							}else if($row_application["loan_status"] == '1'){
								$disabled1 = 'disabled';
								//$disabled2 = '';
								//$disabled3 = '';								
								// $sql_confirm = "SELECT SUM(if(bondsman_approve = 1, 1, 0)) AS sum_confirm, count(bondsman_approve) AS sum_all  
								// 				FROM loan_application_bondsman WHERE loan_id = '".$_GET['loan_id']."' ";
								// $rs_confirm = $mysqli->query($sql_confirm);
								// $row_confirm = $rs_confirm->fetch_assoc();
								// $disabled2 = ($row_confirm['sum_confirm'] == $row_confirm['sum_all'])?'':'disabled';
								// $disabled3 = ($row_confirm['sum_confirm'] == $row_confirm['sum_all'])?'':'disabled';
								
								$disabled4 = 'disabled';
							}else{
								$disabled1 = 'disabled';
								$disabled2 = 'disabled';
								$disabled3 = 'disabled';
								if($row_application["loan_status"] == '2' && $row_application["approve_status"] == '1' && $row_application["officer_approve_status"] == '0'){
									$disabled3 = '';
								}else{
									$disabled3 = 'disabled';
								}
								if($row_application["loan_status"] == '2' && $row_application["approve_status"] == '1' && $row_application["officer_approve_status"] == '0'){
									$disabled4 = '';
								}else{
									$disabled4 = 'disabled';
								}
								
							}
							//เก่า 
							// if($row_application["loan_status"] == '0'){
							// 	$disabled1 = '';
							// 	$disabled2 = 'disabled';
							// 	$disabled3 = 'disabled';
							// 	$disabled4 = 'disabled';
							// }else if($row_application["loan_status"] == '1'){
							// 	$disabled1 = 'disabled';
							// 	//$disabled2 = '';
							// 	//$disabled3 = '';								
							// 	$sql_confirm = "SELECT SUM(if(bondsman_approve = 1, 1, 0)) AS sum_confirm, count(bondsman_approve) AS sum_all  
							// 					FROM loan_application_bondsman WHERE loan_id = '".$_GET['loan_id']."' ";
							// 	$rs_confirm = $mysqli->query($sql_confirm);
							// 	$row_confirm = $rs_confirm->fetch_assoc();
							// 	$disabled2 = ($row_confirm['sum_confirm'] == $row_confirm['sum_all'])?'':'disabled';
							// 	$disabled3 = ($row_confirm['sum_confirm'] == $row_confirm['sum_all'])?'':'disabled';
								
							// 	$disabled4 = 'disabled';
							// }else{
							// 	$disabled1 = 'disabled';
							// 	$disabled2 = 'disabled';
							// 	$disabled3 = 'disabled';
							// 	if($row_application["loan_status"] == '2' && $row_application["approve_status"] == '1' && $row_application["officer_approve_status"] == '0'){
							// 		$disabled4 = '';
							// 	}else{
							// 		$disabled4 = 'disabled';
							// 	}
								
							// }
							//เก่า 
							 if($row_application["confirm_transfer"] == '1' || $row_application["loan_status"] == '0'  || $row_application["loan_status"] == '2'){
								$disabled5 = 'disabled';
							 }

							?>
						<div class="col-sm-12">
							<p class="col-sm-2"><input type="button" class="btn btn-primary <?php echo $disabled1; ?>" value="ตอบรับคำร้องขอกู้เงิน" style="width: 150px;" onclick="approve_loan()"></p>
							<p class="col-sm-2"><input type="button" class="btn btn-primary <?php echo $disabled2; ?>" value="ส่งให้ผู้มีอำนาจอนุมัติ" style="width: 150px;" onclick="send_to_appover()"></p>
							<p class="col-sm-2"><input type="button" class="btn btn-primary <?php echo $disabled3; ?>" data-toggle="modal" data-target="#not_approve_modal" value="ไม่อนุมัติคำร้อง" style="width: 150px;"></p>
							<p class="col-sm-2"><input type="button" class="btn btn-primary <?php echo $disabled4; ?>" data-toggle="modal" data-target="#officer_approve" value="อนุมัติคำร้อง" style="width: 150px;"></p>
							<p class="col-sm-2"><input type="button" class="btn btn-primary" data-toggle="modal" data-target="#send_massage" value="ส่งข้อความถึงสมาชิก" style="width: 150px;"></p>
							<!--<p class="col-sm-2"><input type="button" class="btn btn-info <?php echo $disabled5; ?>" onclick="confirm_transfer()" value="อนุมัติการโอนเงิน" style="width: 150px;" <?php echo $row_application["loan_status"]==3?'disabled':''; ?>></p>-->

						</div>

					
				<script>
					function confirm_transfer(){
						swal({
							title: "",
							text: "ยืนยันการอนุมัติการโอนเงิน",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: '#20449a',
							confirmButtonText: 'ยืนยัน',
							cancelButtonText: "ยกเลิก",
							closeOnConfirm: false,
							closeOnCancel: true
						},
						function(isConfirm){
						if (isConfirm){
								document.location.href = '?do=status&loan_id=<?php echo $_GET['loan_id']?>&type=1&type_view=all&confirm_transfer=ok';
							} else {
								
							}
						});
						
					}
				</script>
						
						<h2 class="text-center" >สถานะการดำเนินการ</h2>

						<?php if ($row['not_approve'] == 1) { ?>
						<div class="alert alert-danger text-center" role="alert" style="background: red;color: #fff;">
							 <?php 
							 	 $cut_massage = str_replace("สัญญาของท่านไม่ผ่านการอนุมัติ เนื่องจาก","", $row["not_approve_message"]);
							  ?>
                             <?php echo ConvertToThaiDate($row["not_approve_date"], true, true, true)." ไม่อนุมัติคำขอกู้ ".$cut_massage ?>     
                        </div>
                        <?php } ?>
					<div style="clear: both;margin-bottom:15px"></div>
						<table class="table table-striped">
							<thead>
								<tr>
									<th width="100" class="text-center">วันที่/เวลา</th>
									<th width="100" class="text-left">สถานะ</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sql_status = "SELECT * FROM cmp_admin_loan_money_status WHERE loan_id = '".$_GET['loan_id']."' ORDER BY status_id DESC";
									$rs_status = $mysqli->query($sql_status);
									while($row_status = $rs_status->fetch_assoc()){
								?>
								<tr>
									<td class="text-center"><?=($row_status['status_date'] != "") ? date_time($row_status["status_date"], true, true, true) : "" ;?></td>
									<td class="text-left">
										<?php echo $row_status['status_name']; ?>
										<div class="pull-right">
										<?php 
											$bondsman_id = $row_status['bondsman_id'];
											$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_id = '".$_GET['loan_id']."' AND bondsman_id = '".$bondsman_id."'";
					                     	$rs_bondsman = $mysqli->query($sql_bondsman);
					   						echo $mysqli->error;
											$row_bondsman = $rs_bondsman->fetch_assoc();
										    
											if(!empty($row_bondsman) && $row_bondsman['bondsman_approve'] == 0) { 
										?>
										<button class='btn btn-info'
												data-toggle="modal"
										 		data-target="#bs-example-modal-sms<?php echo $bondsman_id;?>"
												type="button"
												>ส่ง SMS เตือนอีกครั้ง
										</button>
										<?php } ?>
										</div>
										
										<!-- modal -->
											<div id="bs-example-modal-sms<?php echo $bondsman_id;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title text-center" id="mySmallModalLabel">ส่ง SMS หาผู้ค้ำอีกครั้ง</h4>
														</div>
														<form action="?" class="form-horizontal" method="post" enctype="multipart/form-data">
															<div class="modal-body">
																<label class="control-label">ข้อความแจ้งผู้ค้ำ</label>												
																<input type="hidden" name="type_view" value="<?php echo $_GET['type_view']?>">
																<input type="hidden" name="do" value="bondsman_alone">
																<input type="hidden" name="bondsman_id" value="<?php echo $row_bondsman["bondsman_id"]?>">
																<input type="hidden" name="loan_id" value="<?php echo $loan_id?>">
																<input type="hidden" name="application_id" value="<?php echo $_GET['type']?>">
																<input type="hidden" name="bondsman_tel" value="<?php echo $row_bondsman["bondsman_tel"]?>">
																<textarea maxlength="134" id="bondsman_message" class="form-control" rows="3" name="bondsman_message" id="not_approve_message"><?php echo $message = "".$row_bondsman["bondsman_name"]."ได้ทำการขอกู้เงินโดยให้ท่านคลิกลิงก์ที่แนบมาเพื่อยืนยันการเป็นผู้ค้ำประกัน ".BASEPATH."/loan_bondsman_confirm.php?loan_id=".$_GET['loan_id']."&bondsman_id=".$bondsman_id.""; ?></textarea>
															</div>
															<div class="modal-footer">
																 <button type="submit" class="btn btn-info">ยืนยัน</button>
																  <button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
															</div>
														</form>
													</div>
                                                </div>
                                            </div>
										<!-- modal -->
							
									</td>
								</tr>	
								<?php } ?>
							</tbody>
						</table>
						
						<?php if ($row['c_approve'] == 0) { ?>

							<div class="text-center">
								<a href="?view=<?=$_GET['type_view']?>">
								<button style="width: 111px;" type="button" class="btn btn-info">กลับ</button>
								</a>
							</div>


							<div class="modal fade bs-example-modal-sm" id="not_approve_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title text-center" id="mySmallModalLabel">ไม่อนุมัติคำขอกู้</h4>
                                  </div>
                                        <div class="modal-body">
                                        <label class="control-label">ข้อความแจ้งผู้ขอกู้</label>
										<textarea maxlength="90" id="not_approve_message" class="form-control" rows="3" name="not_approve_message" id="not_approve_message"></textarea>
                                         </div>
                                    <div class="modal-footer">
                                         <button type="button" class="btn btn-info not_approve" data-dismiss="modal"
 										 tel_member = "<?=$row["telmobile"]?>" 
										 send_type = "not_approve"
										 loan_id="<?=$loan_id?>"
										 doc_type="<?=$row['loan_application_type']?>"
                                         >ยืนยัน</button>
                                          <button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
                                    </div>
									</div>
								</div>
							</div>
							<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="officer_approve">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title text-center" id="mySmallModalLabel">อนุมัติคำร้อง</h4>
										</div>
									<form action="" id="form_approve_money" method="POST">
									<input type="hidden" name="loan_id" id="loan_id" value="<?php echo $_GET['loan_id']; ?>">
									<input type="hidden" name="do" value="approve_money">
										<div class="modal-body">
											<div class="row" style="margin-bottom:10px">
												<div class="form-group">
													<label class="col-sm-3 control-label">จำนวนเงินที่ขอกู้</label>
													<div class="col-sm-6">
														<?php echo number_format($row_application['loan_money']); ?> บาท
													</div>
													<input type="hidden" id="max_approve_money" value="<?php echo $row_application['approve_money']; ?>">
												</div>
											</div>
											<div class="row" style="margin-bottom:10px">
												<div class="form-group">
													<label class="col-sm-3 control-label">จำนวนเงินที่อนุมัติ</label>
													<div class="col-sm-6">
														<input type="hidden" onkeyup="cal_approve_money()" class="form-control" id="approve_money" name="approve_money" value="<?php echo $row_application['approve_money']; ?>">
														<?php echo number_format($row_application['approve_money']); ?> บาท
													</div>
													<input type="hidden" id="max_approve_money" value="<?php echo $row_application['approve_money']; ?>">
												</div>
											</div>
											<?php 
											$sql_prev_loan = "SELECT loancontract_no, principal_balance FROM cmp_imp_loan_master WHERE member_no = '".$row['member_id']."'";
											$rs_prev_loan = $mysqli->query($sql_prev_loan);
											$prev_loan = 0;
											while($row_prev_loan = $rs_prev_loan->fetch_assoc()){
												if($row_application['loan_application_type'] == '1'){
													if(substr(trim($row_prev_loan['loancontract_no']),0,2) == 'สส' || substr(trim($row_prev_loan['loancontract_no']),0,2) == 'ฉฉ'){
														$prev_loan += $row_prev_loan['principal_balance'];
													}
												}else{
													if(substr(trim($row_prev_loan['loancontract_no']),0,2) == 'ฉฉ'){
														$prev_loan += $row_prev_loan['principal_balance'];
													}
												}
											}
											?>
											<div class="row" style="margin-bottom:10px">
												<div class="form-group">
													<label class="col-sm-3 control-label">หนี้เดิมเพื่อหักกลบ</label>
													<div class="col-sm-6">
														<input type="text" onkeyup="format_the_number(this);cal_approve_money()" class="form-control" id="prev_loan" name="prev_loan" value="<?php echo number_format($prev_loan); ?>">
													</div>
												</div>
											</div>
											<?php
												$balance = $row_application['approve_money']-$prev_loan;
											?>
											<div class="row" style="margin-bottom:10px">
												<div class="form-group">
													<label class="col-sm-3 control-label">คงเหลืออนุมัตื</label>
													<div class="col-sm-6" id="approve_money_balance_txt"><?php echo number_format($balance,2); ?></div>
													<input type="hidden" id="approve_money_balance" name="approve_money_balance" value="<?php echo $balance; ?>">
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-info" onclick="check_approve_money()">ยืนยัน</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
										</div>
									</form>
									</div>
								</div>
							</div>

						<?php } ?>
						
						<!--<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="transfer">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title text-center" id="mySmallModalLabel">อนุมัติการโอนเงิน </h4>
										</div>
									<form action="loan_send_massage.php" id="form_send_massage" method="POST">
									<input type="hidden" name="loan_id" value="<?php echo $_GET['loan_id']; ?>">
									<input type="hidden" name="loan_type" value="<?php echo $_GET['type']; ?>">
										<div class="modal-body">
											<div class="row" style="margin-bottom:10px">
												<div class="form-group">
													<label class="col-sm-12 control-label">ยืนยันการอนุมัติการโอนเงิน</label>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-info" ">อนุมัติการโอนเงิน</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
										</div>
									</form>
									</div>
								</div>

						</div>-->
						<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="send_massage">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title text-center" id="mySmallModalLabel">ส่งข้อความถึงสมาชิก</h4>
										</div>
									<form action="loan_send_massage.php" id="form_send_massage" method="POST">
									<input type="hidden" name="loan_id" value="<?php echo $_GET['loan_id']; ?>">
									<input type="hidden" name="loan_type" value="<?php echo $_GET['type']; ?>">
										<div class="modal-body">
											<div class="row" style="margin-bottom:10px">
												<div class="form-group">
													<label class="col-sm-3 control-label">ข้อความถึงสมาชิก</label>
													<div class="col-sm-8">
														<textarea name="massage_txt" id="massage_txt" rows="3" class="form-control"></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-info" onclick="check_send_massage()">ยืนยัน</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
										</div>
									</form>
									</div>
								</div>
								<script>
									function check_send_massage(){
										var text_alert = '';
										if($('#massage_txt').val()==''){
											text_alert += 'ข้อความถึงสมาชิก\n';
										}
										if(text_alert!=''){
											swal('กรุณากรอกข้อมูลต่อไปนี้',text_alert,'warning');
										}else{
											$('#form_send_massage').submit();
										}
									}
								</script>
						</div>


				<?php 	} else {

							$page_type = $_GET['view'];
							$per_view = $_GET['per_view'];
							if(($per_view=='own' || $per_view=='') && $_SESSION['ADMIN_PER']!='2' && $_SESSION['ADMIN_PER']!='1'){
								$where_per_view = " AND loan_application.responsible_person_id = '".$_SESSION['ADMIN_ID']."' ";
								// $where_per_view = " AND loan_application.approver_id = '".$_SESSION['ADMIN_PER']."'";
							}else{
								$where_per_view = "";
							}
							if ($page_type == "") {
								$page_type = "all";
							}else{
								$page_type = $_GET['view'];
							}
							
							$smarty->assign("breadcrumb", array("ระบบคำขอกู้เงิน" => "?") );

							if ($page_type == "all") {
									// all
									// $sql = " SELECT * FROM `cmp_admin_loan_money`";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
											INNER JOIN loan_application ON cmp_admin_loan_money.loan_id = loan_application.loan_id
											WHERE 1=1 ".$where_per_view."
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "";
									// all
							}else if ($page_type == "process") {
									// process
									// $sql = " SELECT * FROM `cmp_admin_loan_money` WHERE not_approve = 0 AND c_approve = 0 AND c_sms_b_status = 0";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
											INNER JOIN loan_application ON cmp_admin_loan_money.loan_id = loan_application.loan_id
										    WHERE cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0 AND cmp_admin_loan_money.c_sms_b_status = 0 ".$where_per_view."
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "WHERE cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0 AND c_sms_b_status = 0 ";
									// process
							}else if ($page_type == "bondsman"){
									// bondsman
									// $sql = " SELECT * FROM `cmp_admin_loan_money` WHERE c_sms_b_status = 1 AND not_approve = 0 AND c_approve = 0";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
											INNER JOIN loan_application ON cmp_admin_loan_money.loan_id = loan_application.loan_id
										    WHERE cmp_admin_loan_money.c_sms_b_status = 1 AND cmp_admin_loan_money.c_sms_h_status = 1 AND cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0 ".$where_per_view."
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "WHERE c_sms_b_status = 1 AND c_sms_h_status = 1 AND  not_approve = 0 AND c_approve = 0";
									// bondsman
							}else if($page_type == "not_approve"){
									// not approve
									// $sql = " SELECT * FROM `cmp_admin_loan_money` WHERE not_approve = 1";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
											INNER JOIN loan_application ON cmp_admin_loan_money.loan_id = loan_application.loan_id
										    WHERE cmp_admin_loan_money.not_approve = 1 ".$where_per_view."
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "WHERE not_approve = 1 ";
									// not approve
							}else{
									//  approve
									// $sql = " SELECT * FROM `cmp_admin_loan_money` WHERE c_approve = 1";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
											INNER JOIN loan_application ON cmp_admin_loan_money.loan_id = loan_application.loan_id
										    WHERE cmp_admin_loan_money.c_approve = 1 ".$where_per_view."
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "WHERE c_approve = 1 ";
									//  approve
							}
						// $sql = "SELECT * FROM cmp_admin_loan_money";
						$rs = $mysqli->query($sql);
						$num_rows = $rs->num_rows;
						$per_page = 20 ;
						$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

						$pagination = (new Pagination());

						$pagination->setCurrent($page);
						$pagination->setRPP($per_page);
						$pagination->setTotal($num_rows);
						$pagination->setCrumbs(25);
						$paging = $pagination->parse();

						$page_start = (($per_page * $page) - $per_page);

						  $sql_max = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
							INNER JOIN loan_application ON cmp_admin_loan_money.loan_id = loan_application.loan_id
						    {$set_and} ".$where_per_view."
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    		 cmp_admin_loan_money_status.loan_application_type
						    ORDER BY MAX(cmp_admin_loan_money_status.status_id) DESC
						    LIMIT {$page_start} , {$per_page} ";
						
						$rs_max = $mysqli->query($sql_max);

						$list = array();
						while(($row_max = $rs_max->fetch_assoc())) {
								 $sql = "SELECT * FROM cmp_admin_loan_money_status 
						    		 LEFT JOIN cmp_admin_loan_money ON cmp_admin_loan_money_status.loan_id = cmp_admin_loan_money.loan_id
						    		 WHERE cmp_admin_loan_money_status.status_id = '{$row_max['MAX_ID']}'
						       ";
						       $rs = $mysqli->query($sql);
						       while($row = $rs->fetch_assoc( )){
							        $list[] = $row ; 
							   }
						}

						 $page_start++;

						?>


						<?php if (isset($_GET['err'])) { ?>
							<div class="alert alert-danger resalt">
								<p>ERROR : <?= $_GET['err'] ?></p>
							</div>
						<?php } elseif (isset($_GET['res'])) { ?>
							<div class="alert alert-success resalt">
								<p>! <?= $_GET['res'] ?></p>
							</div>
						<?php } ?>
						<form id="lsit_del" action="?" method="post">
						<div style="clear: both;margin-bottom:15px"></div>
						<div class="table-responsive">
						<table class="table">
						<input type="hidden" name="del_list" value="del_list" >
						<input type="hidden" value="<?=$_GET['view']?>" name="view">
							<thead>
								<tr>
									<th width="10" class="text-center">
									 	<div>
											<div class="checkbox">
												<label>
	                                           		<input id="checkAll" type="checkbox">
												</label>
											</div>
										</div>
									</th>
									<th width="50" class="text-center"></th>
									<th width="105" class="text-left">เลขที่คำขอกู้</th>
									<th width="150" class="text-center">วันที่ทำรายการ</th>
									<th width="150" class="text-center">ชื่อ-สกุล</th>
									<th width="150" class="text-center">ประเภทการขอกู้เงิน</th>
									<th width="150" class="text-center">สถานะล่าสุด</th>
									<th width="170" class="text-center">ผู้รับผิดชอบ</th>
									<th width="170" class="text-center">สถานะการโอนเงิน</th>
									<th width="40" class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($list as $key => $row) { ?>

								<?php 
								$loan_application_type = array();
								$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
								$rs_type = $mysqli->query($sql_type);
								while($row_type = $rs_type->fetch_assoc()){
									$loan_application_type[$row_type['id']] = $row_type['loan_name'];
								}
					
								$sql_application = "SELECT loan_application.loan_application_id, loan_admin.admin_name,loan_application.confirm_transfer FROM loan_application
								LEFT JOIN loan_admin ON loan_application.responsible_person_id = loan_admin.admin_id
								WHERE loan_id = '{$row["loan_id"]}' AND loan_application_type = '{$row["loan_application_type"]}' "; 
								$rs_application = $mysqli->query($sql_application);
   								echo $mysqli->error;
    							$row_application = $rs_application->fetch_assoc();
    							$application_id = $row_application['loan_application_id'];

								 ?>
									<tr>
									    <td>
											<div>
												<div class="checkbox">
														<label>
															<input   
															type="checkbox"
															name="check_del[<?=$row['loan_id']?>]"
															value="<?=$application_id?>"
															>
								
														</label>
												</div>
										</div>
									    </td>
										<td class="text-center">
										<?=($row["not_approve"] == 1) ? "<span class='label label-danger'>ไม่อนุมัติ</span>" : "" ;?>
										<?=($row["c_approve"] == 1) ? "<span class='label label-success '>อนุมัติเรียบร้อย</span>" : "" ;?>
										<?= ($row["status_show"] == 0) ? "<span class='label label-info'>Update</span>" : "" ;?>
										</td>
										<td class="text-left">
										<a href="?do=status&loan_id=<?= $row['loan_id'] ?>&type=<?=$row['loan_application_type']?>&type_view=<?=$_GET['view']?>" title="สถานะ"><?=$row['loan_number']?></a>
										</td>
										<td class="text-center"><?php echo date_time($row["status_date"], true, true, true); ?></td>
										<td class="text-left"><?php echo $row["member_name"]; ?></td>

										<td class="text-left">
											<?php echo $loan_application_type[$row['loan_application_type']];?>
										</td>


										<td class="text-left">
											 <?php echo $row['status_name']?>
										</td>
										<td class="text-center">
											<?php echo $row_application['admin_name']; ?>
										</td>
										<td class="text-center">
										<?php echo $row_application['confirm_transfer']==1?'โอนเงินแล้ว':'ยังไม่ได้โอนเงิน' ; ?>
										</td>
										<td class="text-center">
											<a href="?do=del&loan_id=<?= $row['loan_id'] ?>&file=PDF_<?=$row["loan_id"]?>_<?=$application_id?>.pdf&view=<?=$_GET['view']?>" onclick="return confirm('ต้องการลบข้อมูลนี้');" title="ลบข้อมูล" >ลบ</a>
										</td>
									</tr>
							
								<?php
									}
									unset($sql, $rs, $row);
								?>
										<tr>
										<td colspan="9">
											<button onclick="return confirm('ต้องการลบข้อมูลนี้');"  type="submit" class="btn btn-danger">
											<i class="fa fa-trash-o"></i>  ลบข้อมูล
											</button>
										</td>

										
									</tr>
							</tbody>
						</table>
						</div>
					<?php echo $paging;  ?>
				<?php } ?>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	function approve_loan(){
		swal({
			title: "",
			text: "ท่านต้องการตอบรับคำขอกู้เงินใช่หรือไม่",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#20449a',
			confirmButtonText: 'ยืนยัน',
			cancelButtonText: "ยกเลิก",
			closeOnConfirm: false,
			closeOnCancel: true
		 },
		 function(isConfirm){
		   if (isConfirm){
				document.location.href = '?do=loan_status&status_to=1&loan_id=<?php echo $_GET['loan_id']?>&type=1&type_view=all';
			} else {
				
			}
		 });
		 
	}
	function send_to_appover(){
		swal({
			title: "",
			text: "ท่านต้องการส่งคำร้องให้ผู้มีอำนาจอนุมัติใช่หรือไม่",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#20449a',
			confirmButtonText: 'ยืนยัน',
			cancelButtonText: "ยกเลิก",
			closeOnConfirm: false,
			closeOnCancel: true
		 },
		 function(isConfirm){
		   if (isConfirm){
				document.location.href = 'loan_send_approve.php?loan_id=<?php echo $_GET['loan_id']; ?>';
			} else {
				
			}
		 });
	}
	function cal_approve_money(){
		if($('#approve_money').val() != '' && $('#prev_loan').val() != ''){
			if(parseFloat($('#max_approve_money').val()) < parseFloat($('#approve_money').val())){
				$('#approve_money_balance_txt').html('ท่านไม่สามารถอนุมัติเงินกู้มากกว่าจำนวนเงินที่ผู้มีอำนาจอนุมัติได้');
				$('#approve_money_balance').val('0');
			}else{
				var balance = parseFloat($('#approve_money').val()) - parseFloat($('#prev_loan').val().replace(/,/g ,''));
				if(balance >= 0){
					balance = balance.toFixed(2);
					$('#approve_money_balance_txt').html(parseFloat(balance).toLocaleString());
					$('#approve_money_balance').val(balance);
				}else{
					$('#approve_money_balance_txt').html('ท่านไม่สามารถอนุมัติเงินกู้น้อยกว่าเงินหักกลบได้');
					$('#approve_money_balance').val('0');
				}
			}
		}else{
			$('#approve_money_balance_txt').html('กรุณากรอกข้อมูลให้ครบถ้วน');
			$('#approve_money_balance').val('0');
		}
	}
	function check_approve_money(){
		var text_alert = '';
		if(parseFloat($('#approve_money_balance').val()) <= 0){
			text_alert += $('#approve_money_balance_txt').html();
		}
		if(text_alert!=''){
			swal('',text_alert,'warning');
		}else{
			$('#form_approve_money').submit();
		}
		
	}
	function format_the_number(ele){
		var value = $('#'+ele.id).val();
		if(value!=''){
			value = value.replace(',','');
			value = parseInt(value);
			value = value.toLocaleString();
			if(value == 'NaN'){
				$('#'+ele.id).val('');
			}else{
				$('#'+ele.id).val(value);
			}
		}else{
			$('#'+ele.id).val('');
		}
	}
</script>
<?php
	$content = ob_get_contents();
	ob_end_clean();
}

$smarty->assign("content", $content);
$smarty->display(THEME.".tpl");
