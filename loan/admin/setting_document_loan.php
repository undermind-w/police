<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$document_id = (int) @$_POST["document_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
		$document_root = (int) @$_POST["document_root"] ; 
		
  		$document_type = 1  ;
		$document_title = @$mysqli->real_escape_string(@$_POST["document_title"]) ;
		$document_refid = @$mysqli->real_escape_string(@$_POST["document_refid"]) ;
		$document_url = @$mysqli->real_escape_string(@$_POST["document_url"]) ;
		$document_position = @$mysqli->real_escape_string(@$_POST["document_position"]) ;
		
  		$document_status = (int) @$_POST["document_status"] ; 
		
		
	 
		if(empty($document_id))
		{
			
			$sql = "SELECT MAX(document_seq) as _max  FROM loan_setting_document_check WHERE document_position = '{$document_position}' AND document_root = '{$document_root}'  " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error  ; 
			$max = $rs->fetch_assoc();
			$document_seq = (int) $max["_max"] + 1 ; 	
			
			$sql = "INSERT INTO loan_setting_document_check 
											( 
											admin_id
											, document_seq 
											, document_root
											, document_type
											, document_position 
											, document_title 
 											, document_refid 
											, document_url 
   											, createdate
											, updatedate
											, document_status 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$document_seq}'
											, '{$document_root}'
											, '{$document_type}'
											, '{$document_position}'
											, '{$document_title}'
 											, '{$document_refid}'
											, '{$document_url}'
 											, NOW() 
											, NOW() 
											, {$document_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$document_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("menu-add" , "เพิ่มเมนู "  , $document_id    ) ;
		}
		else
		{	
  			$sql = "UPDATE loan_setting_document_check SET document_type = '{$document_type}' 
												, document_title = '{$document_title}'  
												, document_refid = '{$document_refid}'  
												, document_url = '{$document_url}'  
												, document_type = '{$document_type}' 
   												, updatedate = NOW() 
												, document_status = {$document_status}
												WHERE 
												document_id = '{$document_id}' LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("menu-edit" , "แก้ไขเมนู"  , $document_id    ) ;
		}
		 
		echo "<script> window.location.href = \"?position={$document_position}\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("menu"))  )
	{
		$document_id = (int) @$_GET["mid"] ;  
		$document_position = (int) @$_GET["position"] ;  
 		$sql = "DELETE FROM loan_setting_document_check WHERE document_id = '{$document_id}'  LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("menu-delete" , "ลบเมนู"  , $document_id    ) ;
		echo "<script> window.location.href = \"?position={$document_position}\"</script>" ;
		exit();
	}
	
	
	if(@in_array(@$_GET["do"],array("down","up"))) {
		
		$document_root = (int) @$_GET["rid"] ;  
		$document_id = (int) @$_GET["mid"] ;  
		$document_position = (int) @$_GET["position"] ;  
		
		$sql = "SELECT document_id , document_seq FROM loan_setting_document_check WHERE document_position = '{$document_position}' AND  document_root = '{$document_root}'  ORDER BY document_seq ASC ";
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$i = 0;
		while(($row = $rs->fetch_assoc())) {
			if($row["document_id"]== @ $document_id ) {
				$pos = $i;
			} else{
				$array[$i++] = $row["document_id"];
			}
		}

		if(@$_GET["do"]=="down") {
			$pos++;
		} else {
			$pos--;
		}
			

		$count = count($array)+1;
		
		for($i=0;$i<$count;$i++) {
			if($i == $pos) {
				$tmp[$i] = @$document_id ;
			} else {
				$tmp[$i] = @$array[0];
				array_shift($array);
			}
		}
		for($i = 0 ; $i<count($tmp) ;$i++) {
			$index = $i + 1 ; 
			$sql = "UPDATE loan_setting_document_check SET document_seq = '{$index}'  WHERE document_id = '{$tmp[$i]}'";
			$mysqli->query($sql);
			echo $mysqli->error; 
		}
		
		$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
		echo "<script type=\"text/javascript\">document.location.href='?';</script>";
 		exit();
	}
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_menu").validate();
		$("#position1").change(function(){
			window.location.href = '?position=' + $(this).val(); 
		});
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
		
		if($("#page_detail").length) {
			$("#page_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
		}
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	
	$document_position = (int) empty($_GET["position"]) ? 1 : $_GET["position"] ;
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		$document_root = (int) $_GET["rid"] ; 
		if(@in_array($_GET["do"] , array("edit"))){
			$document_id = (int) @$_GET["mid"] ; 
			$sql = "SELECT * FROM loan_setting_document_check WHERE document_id = {$document_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "จัดการเอกสาร" );
		$smarty->assign("page_title", "จัดการเอกสาร" );
 		$smarty->assign("breadcrumb", array("จัดการเอกสาร" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$document_id = (int) @$row["document_id"]  ; 
 		$document_status = (int) @$row["page_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">จัดการเอกสาร</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_page" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="document_id" value="<?php echo $document_id ;  ?>"/>
						<input type="hidden" name="document_root" value="<?php echo $document_root ;  ?>"/>
						<input type="hidden" name="document_position" value="<?php echo $document_position ;  ?>"/>
						
						<div class="form-group">
							<label for="document_title" class="col-sm-3 control-label">ชื่อเอกสาร</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="document_title" name="document_title" placeholder="" value="<?php echo htmlspecialchars(@$row["document_title"]) ?>" required="required" >
 							</div>
						</div>
						
						 <!-- <div class="form-group">
							<label for="document_url" class="col-sm-3 control-label">ลื้งค์</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="document_url" name="document_url" placeholder="" value="<?php echo htmlspecialchars(@$row["document_url"]) ?>" required="required" >
 							</div>
						</div> -->
 
  
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="document_status" name="document_status" <?php echo $row["document_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "จัดการเอกสาร" );
		$smarty->assign("page_title", "จัดการเอกสาร" );
		$smarty->assign("breadcrumb", array("จัดการเอกสาร" => "#") );
		
		ob_start();
		/*
		$sql = "SELECT COUNT(page_id) as _c FROM cmp_menu" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		*/
		
		$document_type = (int) $_GET["type"] ; 
		if(empty($document_type)){
			$document_type = 1 ; 
		}
		
		
		$sql = "SELECT * FROM  loan_setting_document_check WHERE document_root = 0 AND document_position = '{$document_position}'  ORDER BY document_seq ASC " ;
		$rs1 = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows - ($per_page * ($page - 1 ) )  ; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">จัดการเอกสาร</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <!-- <select class="form-control pull-left" id="position1" style="width:200px" >
				<option value="1" <?php echo $document_position == 1 ? "selected" :"" ;  ?> >ตำแหน่งด้าบน</option>
				<option value="2" <?php echo $document_position == 2 ? "selected" :"" ;  ?>  >ตำแหน่งล่าง</option>
			 </select> -->
			 <a class="btn btn-info pull-right" href="?do=add&position=<?php echo $document_position ?>"  ><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
						<th class="text-center" colspan="" width="100" >ลำดับ</th>
						<th class="text-center" colspan="" width="100" ></th>
 						<th class="text-center" >ชื่อเอกสาร</th>
  						<th class="text-center" width="250" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					
					<?php 
					$i = 1 ; 
					while($row1 = $rs1->fetch_assoc()){
 
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i    ?></td>
						<td style="text-align:center" colspan="2" width="200" >
							<?php if($i != $rs1->num_rows ){ ?>
								<a href="?do=down&in=menu&mid=<?php echo $row1["document_id"]; ?>&position=<?php echo $document_position ?>"><i class="fa fa-arrow-down"></i></a>
							<?php } ?>
							
							<?php if($i != 1 ){ ?>
								<a href="?do=up&in=menu&mid=<?php echo $row1["document_id"]; ?>&position=<?php echo $document_position ?>"><i class="fa fa-arrow-up"></i></a>
							<?php }  ?>
						</td>
						<td><a target="_blank" href="#" ><?php echo @$row1["document_title"] ?></a></td>
  						<td style="text-align:center" >
							<?php if($document_position == 1 ){ ?>
							<a href="?in=menu&do=add&rid=<?php echo @$row1["document_id"] ?>&position=<?php echo $document_position ?>" >เพิ่มเมนูย่อย</a> 
							<?php } ?>
							<a href="?in=menu&do=edit&mid=<?php echo @$row1["document_id"] ?>&position=<?php echo $document_position ?>" ><i class="fa fa-edit"></i></a> 
							<a href="?in=menu&do=delete&mid=<?php echo @$row1["document_id"] ?>&position=<?php echo $document_position ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>  
 						</td>
                      </tr>
					<?php 
						$sql  = "SELECT * FROM loan_setting_document_check WHERE document_root = '{$row1["document_id"]}' ORDER BY document_seq ASC" ; 
						$rs2 = $mysqli->query($sql);
						echo $mysqli->error;  
						$j = 1 ; 
						while($row2 = $rs2->fetch_assoc()){
					?>
						 <tr>
							<td style="text-align:center" ></td>
							<td style="text-align:center" width="100"><?php echo $j ?></td>
							<td style="text-align:center" width="100" >
								<?php if($j != $rs2->num_rows ){ ?>
								<a href="?do=down&in=menu&mid=<?php echo $row2["document_id"]; ?>&position=<?php echo $document_position ?>&rid=<?php echo $row1["document_id"] ?>"><i class="fa fa-arrow-down"></i></a>
							<?php } ?>
 							<?php if($j != 1 ){ ?>
								<a href="?do=up&in=menu&mid=<?php echo $row2["document_id"]; ?>&position=<?php echo $document_position ?>&rid=<?php echo $row1["document_id"] ?>"><i class="fa fa-arrow-up"></i></a>
							<?php }  ?>
							</td>
							<td><a target="_blank" href="#" ><?php echo @$row2["document_title"] ?></a></td>
							<td style="text-align:center" >
								<a href="?in=menu&do=edit&mid=<?php echo @$row2["document_id"] ?>&position=<?php echo $document_position ?>" ><i class="fa fa-edit"></i></a> 
								<a href="?in=menu&do=delete&mid=<?php echo @$row2["document_id"] ?>&position=<?php echo $document_position ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>  
							</td>
						  </tr>
					<?php 	
							$j ++ ; 
						}
						
						$i ++ ; 
					} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 