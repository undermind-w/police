<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;

$sql = "SELECT 
		t1.loan_money,
		t1.loan_application_type,
		t1.member_tel,
		t2.loan_number,
		t3.admin_tel
	FROM 
		loan_application t1 
		LEFT JOIN cmp_admin_loan_money t2 ON t1.loan_id = t2.loan_id
		INNER JOIN loan_admin as t3 on t1.approve_person_admin_id = t3.admin_id

	WHERE 
		t1.loan_id = '".$_GET['loan_id']."'";
$rs = $mysqli->query($sql);
$row = $rs->fetch_assoc();
$message = 'มีผู้ขอกู้รออนุมัติในระบบคำร้องขอกู้เงิน เลขที่ '.$row['loan_number'];
send_sms($row['admin_tel'],$message);
$sql_approver = "SELECT 
		t2.admin_id,
		t2.admin_tel,
		t2.admin_department_id
	FROM 
		loan_application_approve_setting t1
		INNER JOIN loan_admin t2 ON t1.admin_department_id = t2.admin_department_id AND t2.admin_status = '1'
	WHERE 
		t1.min_loan_amount <= ".$row['loan_money']." ORDER BY min_loan_amount DESC limit 1";
$rs_approver = $mysqli->query($sql_approver);
while($row_approver = $rs_approver->fetch_assoc()){
	$admin_department_id = $row_approver['admin_department_id'];
	$message = 'มีผู้ขอกู้รออนุมัติในระบบคำร้องขอกู้เงิน เลขที่ '.$row['loan_number'].'  '.BASEPATH."/admin/loan_approve.php";
	send_sms($row['admin_tel'],$message);
}

$sql_update = "UPDATE loan_application SET 
	approver_id = '".$admin_department_id."',
	loan_status = '2'
WHERE 
	loan_id = '".$_GET['loan_id']."'";
$mysqli->query($sql_update);



$message = 'คำร้องขอกู้เงินเลขที่ '.$row['loan_number'].' อยู่ระหว่างการพิจารณา';
send_sms($row['member_tel'],$message);
$send_status = "insert cmp_admin_loan_money_status set
	loan_id = '".$_GET['loan_id']."',
	status_name = 'ส่งให้ผู้ขอกู้ : ".$message."',
	status_date = NOW(),
	loan_application_type = '".$row['loan_application_type']."'
"; 
$mysqli->query($send_status);

$sql = "update cmp_admin_loan_money set
		send_to_approver = '1',
		send_to_approver_date = NOW()
	WHERE
		loan_id = '".$_GET['loan_id']."'
";
$mysqli->query($sql);

echo"<script> document.location.href='loan_money.php?do=status&loan_id=".$_GET['loan_id']."&type=".$row['loan_application_type']."&type_view=all' </script>";

exit;
?>