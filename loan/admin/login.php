<?php
	session_start();
	require "config.inc.php";
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	 
	if($_POST){
		if(@$_POST["do"] == "login"){
			$to = @urlencode(@$_POST["to"]);
			$username = $mysqli->real_escape_string($_POST["username"]);
			$password = $mysqli->real_escape_string($_POST["password"]);
			$sql = "SELECT * FROM loan_admin WHERE admin_status = 1 AND admin_username = '{$username}' AND admin_password = '{$password}' LIMIT 0 , 1 " ;
 			$rs = $mysqli->query($sql);
			echo $mysqli->error;
			$admin = $rs->fetch_assoc();
 			if(!empty($admin)){
				$token = @date("YmdHis") . $admin["admin_id"] .  @random_char(10);
				$admin_id = (int) @$admin["admin_id"] ; 
				$sql = "UPDATE loan_admin SET admin_token = '{$token}' WHERE admin_id = {$admin_id} LIMIT 1  " ;
				 $mysqli->query($sql);
				 echo $mysqli->error;
				 
				
				echo "<script type='text/javascript'>document.location.href='login.submit.php?token={$token}&to={$to}';</script>";
				exit();
			}
			
			echo "<script type='text/javascript'> alert('รหัสผ่านหรือชื่อผู้ใช้ผิดพลาด !!! ') ; document.location.href='login.php?';</script>";
			exit();
			
			exit();
		}
		
	}
	 
?><!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 1.0
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <title>สหกรณ์ออมทรัพย์ครูสมุทรปราการ จำกัด</title>

  <!-- Favicons-->
  <link rel="icon" href="/html/images/logosp11.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="/html/images/logosp11.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <!-- 
  <meta name="msapplication-TileImage" content="/html/materialize/images/favicon/mstile-144x144.png">
  -->
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  
  <link href="/html/materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/html/materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/html/materialize/css/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="/html/materialize/css/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/html/materialize/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  <link href="/html/frontend/font/font-thaisansneue/thaisansneue.css" type="text/css" rel="stylesheet" media="screen,projection">
 <style>
 .cyan {
    background-color: #fff !important;
}
.input-field .prefix.active {
    color: #548159;
}
 </style>
</head>

<body class="cyan">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->



  <div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
      <form id="frm_login" class="login-form" action="?" method="post" >
		<input type="hidden" name="do" value="login" />
		<input type="hidden" name="to" value="<?php echo urlencode(@$_GET["to"]) ?>" />
        <div class="row">
          <div class="input-field col s12 center">
            <img src="/html/images/logosp11.png" alt="" class="circle responsive-img valign profile-image-login" style="height: auto !important;">
            <p class="center font-thaisans" style="font-size: 22px;margin-top: 0;;">สหกรณ์ออมทรัพย์ครูสมุทรปราการ จำกัด</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="username" name="username" type="text">
            <label for="username" class="center-align">Username</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password"  name="password" type="password">
            <label for="password">Password</label>
          </div>
        </div>
     
        <div class="row">
          <div class="input-field col s12">
            <a href="#" id="btnLogin" onclick="login()" class="btn waves-effect waves-light col s12 font-thaisans" style="font-size:20px;background-color:#548159;" >เข้าสู่ระบบ</a>
          </div>
        </div>

      </form>
    </div>
  </div>



  <!-- ================================================
    Scripts
    ================================================ -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="/html/materialize/js/jquery-1.11.2.min.js"></script>
  <!--materialize js-->
  <script type="text/javascript" src="/html/materialize/js/materialize.js"></script>
  <!--prism-->
  <script type="text/javascript" src="/html/materialize/js/prism.js"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="/html/materialize/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

  <!--plugins.js - Some Specific JS codes for Plugin Settings-->
  
  <script type="text/javascript" src="/html/materialize/js/plugins.js"></script>
	<script>
		$(function(){
			$(this).on("click" , "#btnLogin" , function(){
				$("#frm_login").submit() ; 
			});
			
		});
		function login(){
			$("#frm_login").submit() ; 
		}
	</script>
</body>

</html>