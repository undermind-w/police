<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	if($_POST){
		if($_POST['re_pass']=='1'){
			$sql = "SELECT card_person FROM cmp_imp_member WHERE member_no = '".$_POST['member_no']."'";
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc();
			
			$sql_repass = "UPDATE cmp_imp_member_data SET password = '".$row['card_person']."' WHERE member_no = '".$_POST['member_no']."'";
			$rs_repass = $mysqli->query($sql_repass);
			exit;
		}else{
			$sql = "UPDATE cmp_imp_member_data SET telmobile = '".$_POST['telmobile']."' WHERE member_no = '".$_POST['member_no']."'";
			$rs = $mysqli->query($sql);
			echo $_POST['telmobile'];
			exit;
		}
	}
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");

	ob_start();
?>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<style>
		.table > tbody > tr > td
		, .table > tbody > tr > th
		, .table > tfoot > tr > td
		, .table > tfoot > tr > th
		, .table > thead > tr > td
		, .table > thead > tr > th
		, .table td{
			padding-left: 5px!important;
			padding-right: 5px!important;
		}
	</style>

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/jqueryui/jquery.ui.datepicker-th.js"></script>
<script src="/scripts/jqueryui/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
<script src="/scripts/validation/js/formValidation.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<script src="/scripts/validation/js/bootstrap.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script>

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();


$(document).ready(function() {
	 $("#datepicker_start").datepicker();
	 $("#datepicker_end").datepicker();
});
function change_telmobile(member_no, telmobile){
	$('#telmobile').val(telmobile);
	$('#member_no').val(member_no);
	$('#change_telmobile').modal('show');
}
function change_telmobile_save(){
	var telmobile = $('#telmobile').val();
	var member_no = $('#member_no').val();
	$.ajax({
		type:'POST',
		url:'register_info.php',
		data:{
			telmobile : telmobile,
			member_no : member_no
		},
		success : function(response) {
			$('#telmobile_'+member_no).html(telmobile);
			$('#btn_telmobile_'+member_no).attr('onclick','');
			$('#btn_telmobile_'+member_no).attr('onclick',"change_telmobile('"+member_no+"','"+telmobile+"')");
			swal('แก้ไขข้อมูลหมายเลขโทรศัพท์สำเร็จ');
			$('#change_telmobile').modal('hide');
		}
	});
}
function reset_password(member_no){
	swal({
        title: "ท่านต้องการรีเซ็ทรหัสผ่านใช่หรือไม่?",
        text: "รหัสผ่านของสมาชิกที่ท่านเลือกจะเปลี่ยนเป็นเลขประจำตัวประชาชน",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#22BAA0',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: "ยกเลิก",
        closeOnConfirm: false,
        closeOnCancel: true
    },
    function(isConfirm) {
        if (isConfirm) {
			$.ajax({
				type:'POST',
				url:'register_info.php',
				data:{
					re_pass : '1',	
					member_no : member_no
				},
				success : function(response) {
					swal({
					  title: "สำเร็จ",
					  text: "รหัสผ่านถูกรีเซ็ทเป็นหมายเลขบัตรประชาชน 13 หลักเรียบร้อยแล้ว",
					  type: "success",
					  timer: 3000
				   }),function () {
		   
					};
				}
			});
        } else {
			
        }
    });
}
</script>
<?php
	 
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);




	$smarty->assign("title", "ผู้สมัครแอปพลิเคชัน" );
	$smarty->assign("breadcrumb", array("Dashboard" => "index.php") );

	 ob_start();
?>

	<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix"><h4 class="panel-title" style="white-space: nowrap;">ผู้สมัครแอปพลิเคชัน</h4></div>
			<div class="panel-body">
<?php 
			
				$smarty->assign("breadcrumb", array("ผู้สมัครแอปพลิเคชัน" => "?") );
				$where = '1=1';
				if($_GET['search']!=''){
					$where .= " AND (t1.member_no LIKE '%".$_GET['search']."%' OR t1.memname LIKE '%".$_GET['search']."%')";
				}
				if($_GET['date_start']!=''){
					$date_start_arr = explode('/',$_GET['date_start']);
					$date_start = ($date_start_arr[2])."-".sprintf("%02d",$date_start_arr[1])."-".sprintf("%02d",$date_start_arr[0])." 00:00:00";
				}
				if($_GET['date_end']!=''){
					$date_end_arr = explode('/',$_GET['date_end']);
					$date_end = ($date_end_arr[2])."-".sprintf("%02d",$date_end_arr[1])."-".sprintf("%02d",$date_end_arr[0])." 23:59:59";
				}
				if($_GET['date_start'] != '' && $_GET['date_end'] != ''){
					$where .= " AND t2.create_time BETWEEN '".$date_start."' AND '".$date_end."' ";
				}else if($_GET['date_start'] == '' && $_GET['date_end'] != ''){
					$where .= " AND t2.create_time <= '".$date_end."' ";
				}else if($_GET['date_start'] != '' && $_GET['date_end'] == ''){
					$where .= " AND t2.create_time >= '".$date_start."' ";
				}

				$sql = "SELECT * FROM cmp_imp_member t1
						INNER JOIN cmp_imp_member_data t2 ON t1.member_no = t2.member_no
						WHERE ".$where;
						//echo $sql;
				$rs = $mysqli->query($sql);
				$num_rows = $rs->num_rows;
				$per_page = 10 ;
				$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

				$pagination = (new Pagination());

				$pagination->setCurrent($page);
				$pagination->setRPP($per_page);
				$pagination->setTotal($num_rows);
				$pagination->setCrumbs(25);
				$paging = $pagination->parse();

				$page_start = (($per_page * $page) - $per_page);
				$sql = "SELECT * FROM cmp_imp_member t1
						INNER JOIN cmp_imp_member_data t2 ON t1.member_no = t2.member_no 
						WHERE ".$where."
								ORDER BY t2.create_time DESC LIMIT {$page_start} , {$per_page}";
				$rs = $mysqli->query($sql);
				echo $mysqli->error;
				$num_rows -= $page_start;
?>
				
				<div style="clear: both;margin-bottom:15px">
				<form action="" method="GET">
					<div class="col-sm-12" style="margin-bottom:15px">
						<div class="col-sm-1" style="text-align:right; padding-top:4px;padding-left:0px;padding-right:0px"><p>ค้นหา : </p></div> 
						<div class="col-sm-3">
							<input type="text" name="search" id="search" placeholder="รหัสสมาชิก หรือ ชื่อสมาชิก" class="form-control" value="<?php echo $_GET['search']; ?>">
						</div>
						<div class="col-sm-1">
							<input type="submit" class="btn btn-success" value="ค้นหา">
						</div>
					</div>
					<div class="col-sm-12" style="margin-bottom:15px">
						<div class="col-sm-1" style="text-align:right; padding-top:4px;padding-left:0px;padding-right:0px">วันที่สมัคร : </div> 
						<div class="col-sm-2">
							<input type="text" id="datepicker_start" class="form-control form-group" name="date_start"	value="<?php echo $_GET['date_start']; ?>">
						</div>
						<div class="col-sm-1" style="text-align:right; padding-top:4px;padding-left:0px;padding-right:0px;width:25px">ถึง : </div> 
						<div class="col-sm-2">
							<input type="text" id="datepicker_end" class="form-control form-group"  name="date_end" value="<?php echo $_GET['date_end']; ?>">
						</div>
						<div class="col-sm-1">
							<input type="submit" class="btn btn-success" value="ค้นหา">
						</div>
						<div class="col-sm-1">
							 <a href="register_info_excel_2.php?search=<?php echo $_GET['search']?>&date_start=<?php echo rawurlencode($_GET['date_start'])?>&date_end=<?php echo rawurlencode($_GET['date_end'])?>" title="ส่งออก Excel" target="_blank"><button type="button" class="btn btn-success">ส่งออก Excel</button></a>
						</div>
					</div>
				</form>
				</div>
				
				<div class="text-right p-v-xs">
					<?php
					$sql = "SELECT login_type, COUNT(member_no) AS regis_count
								FROM (
									SELECT member_no, login_type
									FROM cmp_imp_member_login_session 
									WHERE member_no IN (SELECT member_no FROM cmp_imp_member_data) AND login_type <> ''
									GROUP BY member_no, login_type
								) tmp
								GROUP BY login_type";
					$rs_stat = $mysqli->query($sql);
					$stats = [];
					$stat_total = 0;
					while($row_stat = $rs_stat->fetch_assoc()) {
						$stats[$row_stat["login_type"]] = $row_stat["regis_count"];
						$stat_total += $row_stat["regis_count"];
					}
					
					foreach($stats as $key => $value) {
						echo '<span class="p-l-xs text-capitalize">'.$key.' '.round($value / $stat_total * 100, 1).'% ('.number_format($value).')</span> ';
					}
					?>
				</div>
				
				<table class="table table-bordered">
					<thead>
						<tr>
 							<th class="text-center" width="175" >วันที่สมัคร</th>
							<th class="text-center"  width="150">รหัสสมาชิก</th>
							<th class="text-center" width="200">ชื่อสกุล</th>
							<th class="text-center"  width="100" >หมายเลขโทรศัพท์</th>
							<th class="text-center"  width="100" >สมัครผ่าน</th>
							<th class="text-center"  width="100">จัดการ</th>
						</tr>
					</thead>
					<tbody>
					<?php while(($row = $rs->fetch_assoc())) {
						$sql = "SELECT *
										FROM cmp_imp_member_login_session
										WHERE member_no = '{$row["member_no"]}'
										ORDER BY create_time DESC
										LIMIT 1" ;
						$rs_session = $mysqli->query($sql);
						$row_session = $rs_session->fetch_assoc();
						?>
						<tr>
 							<td class="text-center"><?php echo ConvertToThaiDate($row["create_time"], true); ?></td>
							<td class="text-center"><?php echo $row["member_no"]; ?></td>
							<td class=""><?php echo $row["memname"]; ?></td>
							<td class="text-center"><span id="telmobile_<?php echo $row["member_no"]; ?>"><?php echo $row['telmobile'] ?></span></td>
							<td class="text-center"><?php echo $row_session["login_type"] ?></td>
 							<td class="text-center">
								<a style="cursor:pointer;font-size:16px" id="btn_telmobile_<?php echo $row["member_no"]; ?>" title="แก้ไขหมายเลยโทรศัพท์" onclick="change_telmobile('<?php echo $row["member_no"]; ?>','<?php echo $row["telmobile"]; ?>')"><i class="fa fa-phone-square" aria-hidden="true"></i></a> 
								| 
								<a style="cursor:pointer;font-size:16px" title="รีเซ็ทรหัสผ่าน" onclick="reset_password('<?php echo $row["member_no"]; ?>')"><i class="fa fa-lock" aria-hidden="true"></i></a>
							</td>
						</tr>
					<?php
						}
 					?>
					</tbody>
				</table>
				<?php 
					echo $paging ;    
 				?>
 			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="change_telmobile" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="mySmallModalLabel">แก้ไขหมายเลขโทรศัพท์</h4>
			</div>
			<div class="modal-body">
				<label class="control-label">หมายเลขโทรศัพท์ : </label>
				<input type="hidden" id="member_no" name="member_no">
				<input type="text" name="telmobile" id="telmobile" class="form-control">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" onclick="change_telmobile_save()">ยืนยัน</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
			</div>
		</div>
	</div>
</div>
<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
