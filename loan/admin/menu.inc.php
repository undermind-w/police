<?php
	ob_start();
	$file = basename($_SERVER['PHP_SELF']);
	$perlog = $cmpper->get_login_permission_code() ;
 ?>
	<style>
		.submenu{
			background: #3e4f6d !important;
		}
		.submenu>li{
			margin-left:30px
		}
	</style>
	<?php
		if($_SESSION["ADMIN_PER"]=='1'){
			$display_for_admin = '';
		}else{
			$display_for_admin = 'display:none;';
		}


	?>
	<ul class="menu accordion-menu">
		<!--<li class="active" style="<?php echo $display_for_admin; ?>"><a href="index.php" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>หน้าหลัก</p></a></li>-->
		<li style="">
			<a class="waves-effect waves-button" onclick="open_submenu('submenu_satting')"><span class="menu-icon glyphicon glyphicon-cog"></span><p>ตั้งค่าระบบ</p></a>
			<ul id="submenu_satting" class="submenu" is_show='0' style="display:none;">
				<!--li class="waves-effect waves-button"><a href="setting.php"><p>คำร้องกู้เงินออนไลน์</p></a></li-->
				<!--li class="waves-effect waves-button"><a href="setting_email.php"><p>ข้อมูลสมาชิก</p></a></li-->
				<!--<li class="waves-effect waves-button"><a href="setting_loan_data.php"><p>อัตราดอกเบี้ยและงวดชำระ</p></a></li>-->
				<!--<li class="waves-effect waves-button"><a href="setting_loan_suggestion.php"><p>คำแนะนำการขอกู้เงินออนไลน์</p></a></li>-->
				<li class="waves-effect waves-button"><a href="setting_loan.php"><p>ตั้งค่าเงินกู้</p></a></li>
			</ul>
		</li>
		<!--<li style="<?php echo $display_for_admin; ?>"><a href="import_excel.php" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-import"></span><p>นำเข้าข้อมูล</p></a></li>-->
		
		<li><a href="password.php" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-floppy-save"></span><p>เปลี่ยนรหัสผ่าน</p></a></li>
		<!--<li style="<?php echo $display_for_admin; ?>"><a href="register_info.php" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-user"></span><p>ผู้สมัครแอปพลิเคชัน</p></a></li>-->
		
		<li><a href="loan_approve.php" class="waves-effect waves-button"><span class="menu-icon fa fa-buysellads"></span><p>รายการรออนุมัติ</p></a></li>
		<li><a href="loan_money.php?view=all" class="waves-effect waves-button"><span class="menu-icon fa fa-money"></span><p>ระบบคำร้องขอกู้เงิน</p></a></li>
		<li><a href="loan_report.php" class="waves-effect waves-button"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;&nbsp;<p>รายงานคำขอกู้</p></a></li>
		<!--<li><a href="/admin/index.php" class="waves-effect waves-button"><span class="menu-icon fa fa-envelope-o"></span><p>กลับสู่หลังบ้าน</p></a></li>-->
 		
		<li style="<?php echo $display_for_admin; ?>"><a href="user_manager.php" class="waves-effect waves-button"><span class="menu-icon fa fa-street-view"></span><p>ผู้ใช้งาน</p></a></li>
		<li><a href="logout.php" class="waves-effect waves-button"><span class="menu-icon fa fa-desktop"></span><p>ออกจากระบบ</p></a></li>
	</ul>
	<script>
	function open_submenu(id){
		if($('#'+id).attr('is_show')=='0'){
			$('#'+id).show();
			$('#'+id).attr('is_show','1');
		}else{
			$('#'+id).hide();
			$('#'+id).attr('is_show','0');
		}
	}
</script>

	<?php
	unset($perlog) ;
	$menu_right = ob_get_contents();
	ob_end_clean();
	$smarty->assign("menu_right", $menu_right);

