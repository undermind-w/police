<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 	
	$cmpper->check("message") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$send_id    = (int) @$_POST["send_id"] ; 
  		$send_email = @$mysqli->real_escape_string(@$_POST["send_email"]);

		if(empty($send_id))
		{
			$sql = "INSERT INTO cmp_admin_send_email (  send_email ) VALUES ('{$send_email}' ) ";
			$mysqli->query($sql);
			$send_id =  (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
		}
		else
		{	
 			$sql = "UPDATE cmp_admin_send_email  SET   send_email = '{$send_email}' WHERE  send_id = {$send_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;						
			$msg->add("s" , "! แก้ไขข้อมูลแล้ว" ); 
		}
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array($_GET["do"] , array("delete")) ){
		$send_id = @$_GET["mid"] ;  
 		$sql = "DELETE FROM cmp_admin_send_email WHERE send_id = {$send_id} LIMIT 1 " ;
		$mysqli->query($sql);	
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
	ob_start();
?>
	<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
	<script>
		$(document).ready(function() {
			$('.date-picker').datepicker({
				orientation: "top auto" ,
				autoclose: true , 
				format : "yyyy-mm-dd"
			});
			 
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$send_id = (int) $_GET["mid"] ; 
			$sql = "SELECT * FROM cmp_admin_send_email WHERE send_id = {$send_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "ตั้งค่าระบบ" );
		$smarty->assign("page_title", "ตั้งค่าระบบ" );
 		$smarty->assign("breadcrumb", array("ตั้งค่าระบบ" => "?" ,  "{$btitle}" => "#"  ) );

		$send_id    = (int) @$row["send_id"] ; 
 		$send_email = @$row["send_email"];  
		 
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">กำหนดอีเมล์</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="send_id" value="<?php echo $send_id ; ?>"/>
  						<div class="form-group">
							<label for="msg_title" class="col-sm-4 control-label">อีเมล์</label>
							<div class="col-sm-5">
								<input type="email" class="form-control" name="send_email" placeholder="" value="<?php echo @$send_email ?>" required>
 							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}

	else

	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		$smarty->assign("title", "ตั้งค่าระบบ" );
		$smarty->assign("page_title", "ตั้งค่าระบบ" );
		$smarty->assign("breadcrumb", array("ตั้งค่าระบบ" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(send_id) as _c FROM cmp_admin_send_email " ;
		$rs = $mysqli->query($sql);
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_admin_send_email ORDER BY send_id DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">กำหนดอีเมล์</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display(); ?>
			 <a class="btn btn-info pull-right" href="?do=add">เพิ่มข้อมูลอีเมล์</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="80" style="text-align:center" >ลำดับ</th>
						<th width="350" style="text-align:center" >อีเมล์</th>
                        <th width="100" style="text-align:center" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()) {
					?>
					 <tr>
						<td style="text-align:center" ><?php echo $i ++  ?></td>
						<td style="text-align:center" ><?php echo @$row["send_email"]?></td>
						<td style="text-align:center" >
							<a href="?in=message&do=edit&mid=<?php echo @$row["send_id"] ?>" title="แก้ไขข้อมูล" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=message&do=delete&mid=<?php echo @$row["send_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " title="ลบข้อมูล"  ><i class="fa fa-trash-o"></i></a>
						</td>
                      </tr>
					<?php 
					  } 
					?>
				</tbody>
			</table>
			<?php echo $paging?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
	$content = ob_get_contents();
	ob_end_clean();
		
	}
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 