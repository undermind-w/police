<?php
	session_start();
	require "config.inc.php" ;
	require "../../admin/session.inc.php" ;
	require "menu.inc.php" ;
 	
	$cmpper->check("message") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	
	if($_POST)
	{ 	
		// echo '<pre>';print_r($_POST);echo '</pre>';
		//exit;
		$loan_type_id    = (int) @$_POST["loan_type_id"] ; 
  		$loan_name = @$mysqli->real_escape_string(@$_POST["loan_name"]);
		$max_loan_type   = @$mysqli->real_escape_string(@$_POST["max_loan_type"]); 
		$max_loan   = @$mysqli->real_escape_string(@$_POST["max_loan"]); 
		$share_num   = @$mysqli->real_escape_string(@$_POST["share_num"]); 
		$percent_deposit   = @$mysqli->real_escape_string(@$_POST["percent_deposit"]); 
		$percent_cost_estimate   = @$mysqli->real_escape_string(@$_POST["percent_cost_estimate"]); 
		$salary_percent   = @$mysqli->real_escape_string(@$_POST["salary_percent"]); 
		$salary_num   = @$mysqli->real_escape_string(@$_POST["salary_num"]); 
		$interest   = @$mysqli->real_escape_string(@$_POST["interest"]); 
		$long_period_type   = @$mysqli->real_escape_string(@$_POST["long_period_type"]); 
		$start_period   = @$mysqli->real_escape_string(@$_POST["start_period"]); 
		$end_period   = @$mysqli->real_escape_string(@$_POST["end_period"]); 
		$loan_less   = @$mysqli->real_escape_string(@$_POST["loan_less"]); 
		$period_num_min   = @$mysqli->real_escape_string(@$_POST["period_num_min"]); 
		$period_num   = @$mysqli->real_escape_string(@$_POST["period_num"]); 
		$period_num_max   = @$mysqli->real_escape_string(@$_POST["period_num_max"]); 
		$min_member_month   = @$mysqli->real_escape_string(@$_POST["min_member_month"]); 
		$pay_age_max   = @$mysqli->real_escape_string(@$_POST["pay_age_max"]); 
		$guarantee_person_num   = @$mysqli->real_escape_string(@$_POST["guarantee_person_num"]); 
		$approve_num   = @$mysqli->real_escape_string(@$_POST["approve_num"]); 
		$loan_suggestion   = @$mysqli->real_escape_string(@$_POST["loan_suggestion"]); 
		$period_num_min_over   = @$mysqli->real_escape_string(@$_POST["period_num_min_over"]); 
		$loan_money_min   = @$mysqli->real_escape_string(@$_POST["loan_money_min"]); 
		$percent_share   = @$mysqli->real_escape_string(@$_POST["percent_share"]); 
		$show_data_loan_detail   = @$mysqli->real_escape_string(@$_POST["show_data_loan_detail"]); 
		$loan_type_system   = @$mysqli->real_escape_string(@$_POST["loan_type_system"]); 

				if($show_data_loan_detail == 'on'){
					$show_data_loan_detail = 1;
				}else{
					$show_data_loan_detail = 0;
				}
		
		if(empty($loan_type_id))
		{
			$sql = "INSERT INTO loan_setting_detail 
											( 
												loan_name 
												,max_loan_type
												,max_loan
												,share_num
												,percent_share
												,percent_deposit
												,percent_cost_estimate
												,salary_percent
												,interest
												,long_period_type
												,start_period
												,end_period
												,loan_less
												,period_num_min
												,period_num
												,period_num_max
												,period_num_min_over
												,loan_money_min
												,min_member_month
												,pay_age_max
												,guarantee_person_num
												,approve_num
												,show_data_loan_detail
												,loan_suggestion
												,loan_type_system
												,createdate
												,updatedate
												,salary_num
											) 
											VALUES  
											(
												'{$loan_name}'
												,'{$max_loan_type}' 
												,'{$max_loan}' 
												,'{$share_num}' 
												,'{$percent_share}' 
												,'{$percent_deposit}' 
												,'{$percent_cost_estimate}' 
												,'{$salary_percent}' 
												,'{$interest}' 
												,'{$long_period_type}' 
												,'{$start_period}' 
												,'{$end_period}' 
												,'{$loan_less}' 
												,'{$period_num_min}' 
												,'{$period_num}' 
												,'{$period_num_max}' 
												,'{$period_num_min_over}' 
												,'{$loan_money_min}' 
												,'{$min_member_month}' 
												,'{$pay_age_max}' 
												,'{$guarantee_person_num}' 
												,'{$approve_num}' 
												,'{$show_data_loan_detail}' 
												,'{$loan_suggestion}' 
												,'{$loan_type_system}' 
												, NOW()
												, NOW()
												,'{$salary_num}'
											)";
					// echo $sql.'<br>';

			$mysqli->query($sql);
			$loan_type_id =  (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
		}
		else
		{	
 			$sql = "UPDATE loan_setting_detail  SET   loan_name = '{$loan_name}' 
												, max_loan_type   = '{$max_loan_type}'  
												, max_loan   = '{$max_loan}'  
												, share_num   = '{$share_num}'  
												, percent_share   = '{$percent_share}'  
												, percent_deposit   = '{$percent_deposit}'  
												, percent_cost_estimate   = '{$percent_cost_estimate}'  
												, salary_percent   = '{$salary_percent}'  
												, interest   = '{$interest}'  
												, long_period_type   = '{$long_period_type}'  
												, start_period   = '{$start_period}'  
												, end_period   = '{$end_period}'  
												, loan_less   = '{$loan_less}'  
												, period_num_min   = '{$period_num_min}'  
												, period_num   = '{$period_num}'  
												, period_num_max   = '{$period_num_max}'  
												, period_num_min_over   = '{$period_num_min_over}'  
												, loan_money_min   = '{$loan_money_min}'  
												, min_member_month   = '{$min_member_month}'  
												, pay_age_max   = '{$pay_age_max}'  
												, guarantee_person_num   = '{$guarantee_person_num}'  
												, approve_num   = '{$approve_num}'  
												, show_data_loan_detail='{$show_data_loan_detail}' 
												, loan_suggestion   = '{$loan_suggestion}'  
												, loan_type_system   = '{$loan_type_system}'  
												, updatedate   = NOW()
												, salary_num   = '{$salary_num}'
												  WHERE 
												  id = {$loan_type_id} LIMIT 1  " ;
												 // echo $sql.'<br>';
			$mysqli->query($sql);
			echo $mysqli->error ;						
			$msg->add("s" , "! แก้ไขข้อมูลแล้ว" ); 
		}
		// echo $loan_type_id;
		// exit;
						//เอกสารที่ใช้

		$sql = "DELETE FROM loan_set_document_check_list WHERE loan_type_id = '{$loan_type_id}' " ;
		$mysqli->query($sql);
		
		$arr_list_file = $_POST['list_file_id'];
		$arr_list_file_status = $_POST['list_file_status'];
		foreach($arr_list_file AS $key=>$value){
			$list_file_type = $key;			
			foreach($value AS $key2=>$value2){
				$list_file_id = $value2;
				$list_file_status = $arr_list_file_status[$key][$key2];
				$sql = "INSERT INTO loan_set_document_check_list 
											( 
												list_file_id 
												,list_file_type
												,list_file_status
												,loan_type_id
											) 
											VALUES  
											(
												'{$list_file_id}'
												,'{$list_file_type}' 
												,'{$list_file_status}' 
												,'{$loan_type_id}'
											)";
				$mysqli->query($sql); 
			}	
			
		}
				//เอกสารที่ใช้

		// echo $loan_type_id;

		// exit;
		//หลักประกันที่ใช้	
		$sql = "DELETE FROM loan_guarantee_setting WHERE loan_type_id = '{$loan_type_id}' " ;
		$mysqli->query($sql);
		
		$guarantee_list = $_POST['guarantee_list'];
		foreach($guarantee_list AS $key=>$value){	
			$guarantee_id = $value;
			$sql = "INSERT INTO loan_guarantee_setting 
										( 
											guarantee_id 
											,loan_type_id
										) 
										VALUES  
										(
											'{$guarantee_id}'
											,'{$loan_type_id}'
										)";
			$mysqli->query($sql); 	
			
		}
		
		//คำร้องขอกู้และสัญญากู้	
		$sql = "DELETE FROM loan_document_setting WHERE loan_type_id = '{$loan_type_id}' " ;
		$mysqli->query($sql);
		
		$list_doc_id = $_POST['list_doc_id'];
		foreach($list_doc_id AS $key=>$value){	
			$document_id = $value;
			$sql = "INSERT INTO loan_document_setting 
										( 
											document_id 
											,loan_type_id
										) 
										VALUES  
										(
											'{$document_id}'
											,'{$loan_type_id}'
										)";
			$mysqli->query($sql); 	
			
		}
		
		//echo '<pre>'; print_r($sql); echo '</pre>';
		//exit; 	
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array($_GET["do"] , array("delete")) ){
		$loan_type_id = @$_GET["id"] ;  
 		$sql = "DELETE FROM loan_setting_detail WHERE id = {$loan_type_id} LIMIT 1 " ;
		$mysqli->query($sql);	
		
		$sql = "DELETE FROM loan_setting_detail_file WHERE loan_type_id = '{$loan_type_id}' " ;
		$mysqli->query($sql);
		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
	ob_start();
?>
	<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
	<style>
		
	</style>
	
	<script>
		$(document).ready(function() {
			$('.date-picker').datepicker({
				orientation: "top auto" ,
				autoclose: true , 
				format : "yyyy-mm-dd"
			});
			 
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$loan_type_id = (int) $_GET["id"] ; 
			$sql = "SELECT * FROM loan_setting_detail WHERE id = {$loan_type_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;			
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "ตั้งค่าระบบ" );
		$smarty->assign("page_title", "ตั้งค่าระบบ" );
 		$smarty->assign("breadcrumb", array("ตั้งค่าระบบ" => "?" ,  "{$btitle}" => "#"  ) );

		$alert_id    = (int) @$row["loan_type_id"] ; 
 		//$alert_email = @$row["alert_email"];  
		//$alert_tel   = @$row["alert_tel"];   
		 
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ตั้งค่าเงินกู้</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="loan_type_id" id="loan_type_id" value="<?php echo $loan_type_id ; ?>"/>
  						<div class="row">
							<label for="loan_name" class="col-sm-4 control-label">ชื่อเงินกู้</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="loan_name" id="loan_name" placeholder="" value="<?php echo @$row['loan_name']; ?>" required>
 							</div>
						</div>
						<div class="row">
							<label class="col-sm-4 control-label">ประเภทของสัญญาเงินกู้</label>
							<div class="col-sm-5">	
								<select class="form-control" name="loan_type_system" id="loan_type_system" required="required">
									<option value="">เลือกเหตุผลการขอกู้</option>
									<?php 
										$sql_loan_name ="SELECT * FROM coop_loan_name";
										$rs_loan_name = $mysqli_app->query($sql_loan_name);
										while($row_loan_name = $rs_loan_name->fetch_assoc()){
									
										?>
											<option value="<?php echo $row_loan_name['loan_name_id']; ?>" <?php echo $row_loan_name['loan_name_id']==$row['loan_type_system']?'selected':''; ?>><?php echo $row_loan_name['loan_name']; ?></option>	
										<?php 
											}
										?>
								</select>	
							</div>
						</div>
						<div class="row">
							<label class="col-sm-4 control-label">วงเงินกู้สูงสุด</label>
							<div class="col-sm-5">	
							</div>
						</div>	
					
						<div class="row">
							<label class="col-sm-4"></label>
							<div class="col-sm-5">
								<div class="col-small">							
									<!--<input type="radio" id="max_loan_type1" name="max_loan_type" value="1" onchange="change_max_loan_type_radio()" <?php echo @$row['max_loan_type']=='1' || empty($row)?'checked':''; ?>>
									-->
									<label class="control-label_2"> วงเงินสูงสุด </label>
									<input type="number" class="form-control col-small-input-2" name="max_loan" id="max_loan" value="<?php echo (@$row['max_loan'] == '0')?'':@$row['max_loan']; ?>" maxlength="4">
								</div>	
							</div>
						</div>

						<div class="row">
							<label class="col-sm-4"></label>
							<div class="col-sm-5">
								<div class="col-small">							
									<input type="radio" id="max_loan_type2" name="max_loan_type" value="2" onchange="change_max_loan_type_radio()" <?php echo @$row['max_loan_type']=='2'?'checked':''; ?>>
									<input type="number" class="form-control col-small-input-1" name="share_num" id="share_num" value="<?php echo (@$row['share_num'] == '0')?'':@$row['share_num']; ?>" maxlength="4">
									<label class="control-label_2"> เท่าของหุ้น  หรือ</label>
									<input type="number" class="form-control col-small-input-1" name="percent_share" id="percent_share" value="<?php echo (@$row['percent_share'] == '0')?'':@$row['percent_share']; ?>" maxlength="4">
									<label class="control-label_2"> % ของหุ้น </label>
									
								</div>	
							</div>
							
						</div>
						<div class="row">
							<label class="col-sm-4"></label>
							<div class="col-sm-5">
								<div class="col-small">							
									<input type="radio" id="max_loan_type3" name="max_loan_type" value="3" onchange="change_max_loan_type_radio()" <?php echo @$row['max_loan_type']=='3'?'checked':''; ?>>
									<input type="number" class="form-control col-small-input-1" name="percent_deposit" id="percent_deposit" value="<?php echo (@$row['percent_deposit'] == '0')?'':@$row['percent_deposit']; ?>" maxlength="4">
									<label class="control-label_2"> % ของเงินฝาก </label>
								</div>	
							</div>
						</div>
						<div class="row">
							<label class="col-sm-4"></label>
							<div class="col-sm-5">
								<div class="col-small">							
									<input type="radio" id="max_loan_type4" name="max_loan_type" value="4" onchange="change_max_loan_type_radio()" <?php echo @$row['max_loan_type']=='4'?'checked':''; ?>>
									<input type="number" class="form-control col-small-input-1" name="percent_cost_estimate" id="percent_cost_estimate" value="<?php echo (@$row['percent_cost_estimate'] == '0')?'':@$row['percent_cost_estimate']; ?>" maxlength="4">
									<label class="control-label_2"> % ของราคาประเมิน </label>
								</div>	
							</div>
						</div>
						<div class="row">
							<label class="col-sm-4"></label>
							<div class="col-sm-5">
								<div class="col-small">							
									<input type="radio" id="max_loan_type5" name="max_loan_type" value="5" onchange="change_max_loan_type_radio()" <?php echo @$row['max_loan_type']=='5'?'checked':''; ?>>
									<input type="number" class="form-control col-small-input-1" name="salary_percent" id="salary_percent" value="<?php echo (@$row['salary_percent'] == '0')?'':@$row['salary_percent']; ?>" maxlength="4">
									<label class="control-label_2"> % เงินเดือน </label>
								</div>	
							</div>
						</div>
						<div class="row">
							<label class="col-sm-4"></label>
							<div class="col-sm-5">
								<div class="col-small">							
									<input type="radio" id="max_loan_type6" name="max_loan_type" value="6" onchange="change_max_loan_type_radio()" <?php echo @$row['max_loan_type']=='6'?'checked':''; ?>>
									<input type="number" class="form-control col-small-input-1" name="salary_num" id="salary_num" value="<?php echo (@$row['salary_num'] == '0')?'':@$row['salary_num']; ?>" maxlength="4">
									<label class="control-label_2"> เท่าของเงินเดือน </label>
								</div>	
							</div>
						</div>
						<div class="row">
							<label for="interest" class="col-sm-4 control-label">ดอกเบี้ย</label>
							<div class="col-sm-1">
								<input type="text" class="form-control" name="interest" placeholder="" value="<?php echo @$row['interest']; ?>" required>
 							</div>
							<label class="col-sm-1 control-label text-left">%</label>
						</div>
						<div class="row">
							<label for="interest" class="col-sm-4 control-label">ขั้นต่ำต่องวด</label>
							<div class="col-sm-1">
								<input type="text" class="form-control" name="loan_money_min" id="loan_money_min" placeholder="" value="<?php echo @$row['loan_money_min']; ?>" required>
 							</div>
							<label class="col-sm-1 control-label text-left">บาท</label>
						</div>
						<div class="row">
							<label for="msg_title" class="col-sm-4 control-label">ระยะเวลาผ่อน</label>
							<div class="col-sm-5">	
							</div>
						</div>						
						<div class="row">
							<label class="col-sm-4"></label>
							<div class="col-sm-5">
								<div class="col-small">							
									<input type="radio" id="long_period_type1" name="long_period_type" value="1" onchange="change_period_type_radio()" <?php echo @$row['long_period_type']=='1' || empty($row)?'checked':''; ?>>
									<input type="number" class="form-control col-small-input-2" name="start_period" id="start_period" value="<?php echo @$row['start_period']; ?>" maxlength="4">
									<label class="control-label_2"> - </label>
									<input type="number" class="form-control col-small-input-2" name="end_period" id="end_period" value="<?php echo @$row['end_period']; ?>" maxlength="4">
									<label class="control-label_2"> งวด </label>
								</div>	
							</div>
						</div>					
						<div class="row">
							<label class="col-sm-4"></label>
							<div class="col-sm-5">
								<div class="col-small">							
									<input type="radio" id="long_period_type2" name="long_period_type" value="2" onchange="change_period_type_radio()" <?php echo @$row['long_period_type']=='2'?'checked':''; ?>>
									<label class="control-label_2"> วงเงินกู้ไม่เกิน </label>
									<input type="number" class="form-control col-small-input-2" name="loan_less" id="loan_less" value="<?php echo (@$row['loan_less'] == '0')?'':@$row['loan_less']; ?>" maxlength="11">
									<label class="control-label_2"> บาท   ผ่อนได้ </label>
									<input type="number" class="form-control col-small-input-2" name="period_num_min" id="period_num_min" value="<?php echo (@$row['period_num_min'] == '0')?'':@$row['period_num_min']; ?>" maxlength="4">  
									<label class="control-label_2"> ถึง </label>
									<input type="number" class="form-control col-small-input-2" name="period_num" id="period_num" value="<?php echo (@$row['period_num'] == '0')?'':@$row['period_num']; ?>" maxlength="4">
									<label class="control-label_2"> งวด หากเกินผ่อนได้ </label>
									<input type="number" class="form-control col-small-input-2" name="period_num_min_over" id="period_num_min_over" value="<?php echo (@$row['period_num_min_over'] == '0')?'':@$row['period_num_min_over']; ?>" maxlength="4">  
									<label class="control-label_2"> ถึง </label>
									<input type="number" class="form-control col-small-input-2" name="period_num_max" id="period_num_max" value="<?php echo (@$row['period_num_max'] == '0')?'':@$row['period_num_max']; ?>" maxlength="4">
									<label class="control-label_2"> งวด</label>
								</div>	
							</div>
						</div>
						<div class="row">
							<label for="min_member_month" class="col-sm-4 control-label">อายุสมาชิก</label>
							<div class="col-sm-1">
								<input type="text" class="form-control" name="min_member_month" id="min_member_month" placeholder="" value="<?php echo (@$row['min_member_month'] == '0')?'':@$row['min_member_month']; ?>">
 							</div>
							<label class="col-sm-1 control-label text-left">เดือน</label>
						</div>
						<div class="row">
							<label for="pay_age_max" class="col-sm-4 control-label">อายุผ่อนต้องไม่เกิน</label>
							<div class="col-sm-1">
								<input type="text" class="form-control" name="pay_age_max" id="pay_age_max" placeholder="" value="<?php echo (@$row['pay_age_max'] == '0')?'':@$row['pay_age_max']; ?>">
 							</div>
							<label class="col-sm-1 control-label text-left">ปี</label>
						</div>
						<div class="row">
							<label for="msg_title" class="col-sm-4 control-label">จำนวนผู้ค้ำ</label>
							<div class="col-sm-1">
								<input type="text" class="form-control" name="guarantee_person_num" id="guarantee_person_num" placeholder="" value="<?php echo (@$row['guarantee_person_num'] == '0')?'':@$row['guarantee_person_num']; ?>">
 							</div>
							<label class="col-sm-1 control-label text-left">คน</label>
						</div>
						<div class="row">
							<label for="msg_title" class="col-sm-4 control-label">จำนวนผู้อนุมัติ</label>
							<div class="col-sm-1">
								<input type="text" class="form-control" name="approve_num" id="approve_num" placeholder="" value="<?php echo (@$row['approve_num'] == '0')?'':@$row['approve_num']; ?>">
 							</div>
							<label class="col-sm-1 control-label text-left">คน</label>
						</div>
						<!-- <div class="row">
							<label for="msg_title" class="col-sm-4 control-label">แสดงข้อมูลการกู้เงินหรือไม่</label>
							<div class="col-sm-5"><input type="checkbox" id="show_data_loan_detail" name="show_data_loan_detail" <?php echo $row['show_data_loan_detail']==1?'checked':''; ?>> </div>
						</div> -->
						<div class="row">
							<label for="msg_title" class="col-sm-4 control-label">หลักประกันที่ใช้</label>
							<div class="col-sm-5"></div>
						</div>
						
						<?php 
						
							$sql_guarantee_setting = "SELECT * FROM loan_guarantee_setting WHERE loan_type_id = '{$loan_type_id}'" ;
							$rs_guarantee_setting = $mysqli->query($sql_guarantee_setting);
							echo $mysqli->error ;
							$guarantee_list = array();
							while($row_guarantee_setting = $rs_guarantee_setting->fetch_assoc()) {
								$guarantee_list[$row_guarantee_setting['guarantee_id']] = $row_guarantee_setting['guarantee_id'];
							}
							
							$sql_guarantee = "SELECT * FROM loan_guarantee ORDER BY order_by" ;
							$rs_guarantee = $mysqli->query($sql_guarantee);
							echo $mysqli->error ;
							while($row_guarantee = $rs_guarantee->fetch_assoc()) {
								$guarantee_setting_id = $guarantee_list[$row_guarantee['guarantee_id']];
						?>
							<div class="row">
								<label class="col-sm-4"></label>
								<div class="col-sm-5">
									<input type="checkbox" id="guarantee_list[]" name="guarantee_list[]" value="<?php echo $row_guarantee['guarantee_id'];?>" <?php echo $guarantee_setting_id==$row_guarantee['guarantee_id']?'checked':''; ?>> 
									<label class="control-label_2"> <?php echo $row_guarantee['guarantee_detail'];?> </label>									
								</div>
							</div>
						<?php	
							}
						?>
						
						<div class="row">
							<label for="msg_title" class="col-sm-4 control-label">เอกสารที่ใช้ประกอบการขอกู้</label><label for="msg_title" class="col-sm-4 control-label"> <a href="setting_document_loan.php" >ตั้งค่าเอกสารที่ใช้ประกอบการขอกู้</a></label>
							<div class="col-sm-5"></div>
						</div>
						<?php 
							$sql_f = "SELECT * FROM loan_setting_document_check WHERE document_root = 0 " ;
							$rs_f = $mysqli->query($sql_f);
							echo $mysqli->error ;
							$row_file = array();
							while($row_f = $rs_f->fetch_assoc()) {								
							
						?>
						<div class="row">
							<label for="msg_title" class="col-sm-4 control-label"><?php echo $row_f['document_title']; ?></label>
							<label for="msg_title" class="col-sm-4 control-label text-left"></label>
						</div>

						<?php	
							$sql_f_seq = "SELECT * FROM loan_setting_document_check WHERE document_root = {$row_f['document_id']}" ;
							$rs_f_seq = $mysqli->query($sql_f_seq);
							echo $mysqli->error ;
							while($row_f_seq = $rs_f_seq->fetch_assoc()) {		
								$list_file_status = $row_f_seq['requesred_status'];
								$list_file_id = $row_f_seq['document_status'];
								$ch = '';
								$ch_requeed = '';
								// $check_document_main[$row_f['document_id']][$row_f_seq['document_id']];
								// echo $check_document_main[$row_f['document_id']][$row_f_seq['document_id']];

								$sql_checked = "SELECT id,list_file_status FROM loan_set_document_check_list WHERE list_file_type = {$row_f['document_id']} AND list_file_id = {$row_f_seq['document_id']} AND loan_type_id = '{$_GET['id']}' " ;
								$rs_checked = $mysqli->query($sql_checked);
								$row_checked = $rs_checked->fetch_assoc();
								// echo $sql_checked ;
									if(!empty($row_checked['id'])){
										$ch = 'checked';
									}else{
										$ch = '';
									}

									if($row_checked['list_file_status'] == 1){
										$ch_requeed = 'checked';
									}else{
										$ch_requeed = '';
									}
							?>
							<div class="row">
								<label class="col-sm-4"></label>
								<div class="col-sm-5">
									<input type="checkbox" id="list_file_id[<?php echo $row_f['document_id'];?>][<?php echo $row_f_seq['document_id'];?>]" name="list_file_id[<?php echo $row_f['document_id'];?>][<?php echo $row_f_seq['document_id'];?>]" value="<?php echo $row_f_seq['document_id'];?>" <?php echo $ch; ?>>
									<label class="control-label_2"> <?php echo $row_f_seq['document_title'];?> </label>									
								</div>
								<div class="col-sm-3">
									<input type="checkbox" id="list_file_status[<?php echo $row_f['document_id'];?>][<?php echo $row_f_seq['document_id'];?>]" name="list_file_status[<?php echo $row_f['document_id'];?>][<?php echo $row_f_seq['document_id'];?>]" value="1" <?php echo $ch_requeed; ?>>
									<label class="control-label_2"> บังคับแนบไฟล์</label>									
								</div>
							</div>
							<?php 


							}
							?>
								<?php	
							}
							?>
						<div class="row">
							<label for="msg_title" class="col-sm-4 control-label">คำร้องขอกู้และสัญญากู้</label>
							<label for="msg_title" class="col-sm-4 control-label text-left"></label>
						</div>
						<?php 
							
							$sql_set = "SELECT * FROM loan_document_setting WHERE loan_type_id = '{$loan_type_id}'" ;
							$rs_set = $mysqli->query($sql_set);
							echo $mysqli->error ;
							$list_doc_set = array();							
							while($row_set = $rs_set->fetch_assoc()) {
								$list_doc_set[$row_set['document_id']] = $row_set['document_id'];
							}
							
							$sql_doc = "SELECT * FROM loan_document ORDER BY order_by" ;
							$rs_doc = $mysqli->query($sql_doc);
							echo $mysqli->error ;
							while($row_doc = $rs_doc->fetch_assoc()) {
								$list_doc = $list_doc_set[$row_doc['id']];
						?>
							<div class="row">
								<label class="col-sm-4"></label>
								<div class="col-sm-5">
									<input type="checkbox" id="list_doc_id[<?php echo $row_doc['id'];?>]" name="list_doc_id[<?php echo $row_doc['id'];?>]" value="<?php echo $row_doc['id'];?>" <?php echo $list_doc==$row_doc['id']?'checked':''; ?>>
									<label class="control-label_2"> <?php echo $row_doc['description'];?> </label>				
									<label class="control-label_2"><a href="/Document/<?php echo $row_doc['document_link'];?>" target="_blank">ตัวอย่าง</a></label>									
									
								</div>
							</div>
						<?php	
							}
						?>
						<div class="form-group">
							<label for="loan_suggestion" class="col-sm-4 control-label">คำแนะนำการกู้</label>
							<div class="col-sm-8" style="padding-top: 10px;">
								<textarea id="loan_suggestion" name="loan_suggestion" ><?php echo $row['loan_suggestion']; ?></textarea>
							</div>
						</div>
						
						
						<div class="row">&nbsp;</div>
						<div class="row">
							<div class="col-sm-offset-4 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}

	else

	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		$smarty->assign("title", "ตั้งค่าระบบ" );
		$smarty->assign("page_title", "ตั้งค่าระบบ" );
		$smarty->assign("breadcrumb", array("ตั้งค่าระบบ" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(id) as _c FROM loan_setting_detail " ;
		$rs = $mysqli->query($sql);
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  loan_setting_detail ORDER BY id ASC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">ตั้งค่าเงินกู้</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display(); ?>
			 <a class="btn btn-info pull-right" href="?do=add">เพิ่มเงินกู้ใหม่</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="80" style="text-align:center" >ลำดับ</th>
						<th width="350" style="text-align:center" >ชื่อเงินกู้</th>
                        <th width="200" style="text-align:center" >วงเงินกู้สูงสุด</th>
                        <th width="100" style="text-align:center" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()) {
					?>
					 <tr>
						<td style="text-align:center" ><?php echo $i ++  ?></td>
						<td style="text-align:left" ><?php echo @$row['loan_name'];?></td>
                        <td style="text-align:center" >
							<?php
								$max_loan = '';	 
								if(@$row['max_loan'] > 0){
									$max_loan .= 'วงเงินกู้สูงสุด '.number_format(@$row['max_loan'],2);
								}
								
								$max_loan_perfix = ' ไม่เกิน ';	
								if(@$row['max_loan_type'] == '2'){
									$max_loan .= @$max_loan_perfix.@$row['share_num'].' เท่าของหุ้น';
								}else if(@$row['max_loan_type'] == '3'){
									$max_loan .= @$max_loan_perfix.@$row['percent_deposit'].' % ของเงินฝาก';									
								}else if(@$row['max_loan_type'] == '4'){
									$max_loan .= @$max_loan_perfix.@$row['percent_cost_estimate'].' % ของราคาประเมิน';
								}else if(@$row['max_loan_type'] == '5'){
									$max_loan .= @$max_loan_perfix.@$row['salary_percent'].' % เงินเดือน';
								}else if(@$row['max_loan_type'] == '6'){
									$max_loan .= @$max_loan_perfix.@$row['salary_num'].' เท่าของเงินเดือน';
								}
								echo $max_loan;
							?>
						</td>
						<td style="text-align:center" >
							<a href="?in=message&do=edit&id=<?php echo @$row["id"] ?>" title="แก้ไขข้อมูล" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=message&do=delete&id=<?php echo @$row["id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " title="ลบข้อมูล"  ><i class="fa fa-trash-o"></i></a>
						</td>
                      </tr>
					<?php 
					  } 
					?>
				</tbody>
			</table>
			<?php echo $paging?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
	$content = ob_get_contents();
	ob_end_clean();
		
	}
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
?>
<script type="text/javascript" src="../../scripts/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../scripts/ckeditor/adapters/jquery.js"></script>
<script>
	$(document).ready(function() {
		
		if($("#loan_suggestion").length) {
			$("#loan_suggestion").ckeditor({ height : 500 ,width : 300 , customConfig : '/scripts/ckeditor/config-admin.js'   });
		}
		change_max_loan_type_radio();
		change_period_type_radio();
	});
	
	function change_max_loan_type_radio(){
		if($('#max_loan_type2').is(':checked')){
			//$('#max_loan').attr("disabled", true);
			$('#share_num').attr("disabled", false);
			$('#percent_share').attr("disabled", false);
			$('#percent_deposit').attr("disabled", true);
			$('#percent_cost_estimate').attr("disabled", true);
			$('#salary_percent').attr("disabled", true);
			$('#salary_num').attr("disabled", true);
		}else if($('#max_loan_type3').is(':checked')){
			//$('#max_loan').attr("disabled", true);
			$('#share_num').attr("disabled", true);
			$('#percent_share').attr("disabled", true);
			$('#percent_deposit').attr("disabled", false);
			$('#percent_cost_estimate').attr("disabled", true);
			$('#salary_percent').attr("disabled", true);
			$('#salary_num').attr("disabled", true);

		}else if($('#max_loan_type4').is(':checked')){
			//$('#max_loan').attr("disabled", true);
			$('#share_num').attr("disabled", true);
			$('#percent_share').attr("disabled", true);
			$('#percent_deposit').attr("disabled", true);
			$('#percent_cost_estimate').attr("disabled", false);
			$('#salary_percent').attr("disabled", true);
			$('#salary_num').attr("disabled", true);

		}else if($('#max_loan_type5').is(':checked')){
			//$('#max_loan').attr("disabled", true);
			$('#share_num').attr("disabled", true);
			$('#percent_share').attr("disabled", true);
			$('#percent_deposit').attr("disabled", true);
			$('#percent_cost_estimate').attr("disabled", true);
			$('#salary_percent').attr("disabled", false);
			$('#salary_num').attr("disabled", true);

		}else if($('#max_loan_type6').is(':checked')){
			//$('#max_loan').attr("disabled", true);
			$('#share_num').attr("disabled", true);
			$('#percent_share').attr("disabled", true);
			$('#percent_deposit').attr("disabled", true);
			$('#percent_cost_estimate').attr("disabled", true);
			$('#salary_percent').attr("disabled", true);
			$('#salary_num').attr("disabled", false);

		}else{
			//$('#max_loan').attr("disabled", false);
			$('#share_num').attr("disabled", true);
			$('#percent_share').attr("disabled", true);
			$('#percent_deposit').attr("disabled", true);
			$('#percent_cost_estimate').attr("disabled", true);
			$('#salary_percent').attr("disabled", true);
			$('#salary_num').attr("disabled", true);
		}
	}
	
	function change_period_type_radio(){
		if($('#long_period_type2').is(':checked')){
			$('#start_period').attr("disabled", true);
			$('#end_period').attr("disabled", true);
			$('#loan_less').attr("disabled", false);
			$('#period_num').attr("disabled", false);
			$('#period_num_max').attr("disabled", false);
		}else{
			$('#start_period').attr("disabled", false);
			$('#end_period').attr("disabled", false);
			$('#loan_less').attr("disabled", true);
			$('#period_num').attr("disabled", true);
			$('#period_num_max').attr("disabled", true);
		}
	}
</script>