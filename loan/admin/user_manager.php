<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");

	ob_start();
	
	if($_GET['do']=='delete_user'){
		$sql_delete="DELETE FROM loan_admin WHERE admin_id = '".$_GET['user_id']."'";
		$mysqli->query($sql_delete);
		echo"<script>document.location.href = 'user_manager.php';</script>";
		exit;
	}
	if($_POST['do'] == 'check_delete'){
		$sql = "SELECT 
		t1.loan_id 
		FROM loan_application t1
		INNER JOIN cmp_admin_loan_money t2 ON t1.loan_id = t2.loan_id
		WHERE (t1.approver_id = '".$_POST['user_id']."' OR t1.responsible_person_id = '".$_POST['user_id']."') 
		AND (t2.c_approve <> '1' AND t2.not_approve <> '1')
		";
		$rs = $mysqli->query($sql);
		$row = $rs->fetch_assoc();
		if($row['loan_id']!=''){
			echo 'ไม่สามารถลบผู้ใช้งานได้เนื่องจากมีชื่อเป็นผู้รับผิดชอบการกู้เงินอยู่'; 
		}else{
			echo 'success';
		}
		exit;
	}
?>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<style>
		.table > tbody > tr > td
		, .table > tbody > tr > th
		, .table > tfoot > tr > td
		, .table > tfoot > tr > th
		, .table > thead > tr > td
		, .table > thead > tr > th
		, .table td{
			padding-left: 5px!important;
			padding-right: 5px!important;
		}
	</style>

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/jqueryui/jquery.ui.datepicker-th.js"></script>
<script src="/scripts/jqueryui/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
<script src="/scripts/validation/js/formValidation.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<script src="/scripts/validation/js/bootstrap.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
	?>
<script>

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
</script>
	<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	$smarty->assign("title", "ผู้ใช้งาน" );
	$smarty->assign("breadcrumb", array("Dashboard" => "index.php") );

	 ob_start();
?>
	<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix"><h4 class="panel-title">ผู้ใช้งาน</h4></div>
			<div class="panel-body">
<?php 
			
				$smarty->assign("breadcrumb", array("สมาชิกแจ้งแก้ไขข้อมูล" => "?") );

				$sql = "SELECT * FROM cmp_admin";
				$rs = $mysqli->query($sql);
				$num_rows = $rs->num_rows;
				$per_page = 20 ;
				$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

				$pagination = (new Pagination());

				$pagination->setCurrent($page);
				$pagination->setRPP($per_page);
				$pagination->setTotal($num_rows);
				$pagination->setCrumbs(25);
				$paging = $pagination->parse();

				$page_start = (($per_page * $page) - $per_page);
				$sql = "SELECT * FROM loan_admin t1
					LEFT JOIN cmp_admin_department t2 ON t1.admin_department_id = t2.admin_department_id
				ORDER BY admin_id DESC LIMIT {$page_start} , {$per_page}";
				$rs = $mysqli->query($sql);
				echo $mysqli->error;
				$types = array(1 => "แก้ไขอีเมล์"  , "แก้ไขเบอร์โทร" , "แก้ไขที่อยู่" );

				$num_rows -= $page_start;
				
?>
				<div class="col-sm-12" style="margin-bottom:5px;margin-right:0px;padding-right:0px;">
					<div class="col-sm-9"></div>
					<div class="col-sm-3" style="text-align:right;margin-right:0px;padding-right:0px;float:right;">
						<a class="btn btn-success" href="user_manager_add.php">เพิ่มผู้ใช้งาน</a>
					</div>
				</div>
				<div style="clear: both;"></div>
				<table class="table table-bordered">
					<thead>
						<tr>
 							<th class="text-center" width="10%">ลำดับ</th>
							<th class="text-center" >ผู้ใช้งาน</th>
							<th class="text-center" width="20%">ฝ่าย</th>
							<th class="text-center" width="20%">เบอร์โทร</th>
							<th class="text-center" width="10%">จัดการ</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					$i = 1;
					while(($row = $rs->fetch_assoc())) { ?>
						<tr>
 							<td class="text-center"><?php echo $i++; ?></td>
							<td class="text-center"><?php echo $row['admin_name']; ?></td>
							<td class="text-center"><?php echo $row['admin_department']; ?></td>
							<td class="text-center"><?php echo $row['admin_tel']; ?></td>
							<td class="text-center">
								<a href='user_manager_add.php?admin_id=<?php echo $row['admin_id']; ?>'>แก้ไข</a> 
							<?php if($row['admin_super'] == 0){?>
								| <a style="cursor:pointer;" onclick="delete_user('<?php echo $row['admin_id']; ?>');">ลบ</a>
							<?php }?>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
				<?php 
					echo $paging ;    
				?>
 			</div>
		</div>
	</div>
</div>
<script>
	function delete_user(user_id){
		swal({
			title: "",
			text: "ท่านต้องการลบข้อมูลใช่หรือไม่?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#fb0997',
			confirmButtonText: 'ยืนยัน',
			cancelButtonText: "ยกเลิก",
			closeOnConfirm: false,
			closeOnCancel: true
		 },
		 function(isConfirm){
		   if (isConfirm){
			   $.ajax({
					type	: 'POST',
					url	: '',
					data	: {
						'do'	 : 'check_delete',
						'user_id' : user_id
					},
					success : function(response) {
						if(response == 'success'){
							document.location.href = '?do=delete_user&user_id='+user_id;
						}else{
							swal('',response,'warning');
						}
					}
				});
			} else {
				
			}
		 });
	}
</script>
<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
