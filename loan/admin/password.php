<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($do == "chgpwd") {
			$pre = str_replace('\\', null, $mysqli->real_escape_string($_POST["pre"]));
			$val = $mysqli->real_escape_string($_POST["val"]);
			$sql = "UPDATE loan_admin SET admin_password = '{$val}' WHERE admin_name = \"{$pre}\"";
			$mysqli->query($sql);
			exit();
	}
	if($_POST) exit();

	// header("Content-Type:text/html;charset=utf-8");
	// date_default_timezone_set('Asia/Bangkok');
	// $year=date("Y")>2500?date("Y")-543:date("Y");

	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<!-- <link href="/html/frontend/css/custom.css" rel="stylesheet" type="text/css"/> -->
	<style>
		.modal-content {
			margin-top: 50%;
		}
	</style>

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();

	?>
	<script>
		$(document).ready(function() {
			$("body").on("click", "#btn-submit", function() {
					var is_error = false;
					$(".form-group").removeClass("has-error");
					$("#password_confirm").parents(".form-group").find(".help-block").html("");
					if($("#password").val() == "") {
							$("#password").parents(".form-group").addClass("has-error");
							is_error = true;
					}
					if($("#password_confirm").val() == "") {
							$("#password_confirm").parents(".form-group").addClass("has-error");
							is_error = true;
					}
					if($("#password").val() != $("#password_confirm").val()) {
							$("#password_confirm").parents(".form-group").addClass("has-error");
							$("#password_confirm").parents(".form-group").find(".help-block").html("รหัสผ่านใหม่ และยืนยันรหัสผ่านใหม่ ต้องเหมือนกันค่ะ");
							is_error = true;
					}
					if(!is_error) {
					$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
									"do" 	: "chgpwd" ,
									"pre" : $('span.user-name').text(),
									"val"	: $("#password").val() ,
									"_time" : Math.random()
							}
							, async: true
							, success: function(response) {
								console.log(response);
									$("#modal-password").modal("show");
									$('input[type="password"]').val('');
							}
					});
					}
			});
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "เปลี่ยนรหัสผ่าน");
	// $smarty->assign("page_title", "เปลี่ยนรหัสผ่าน" );
	$smarty->assign("breadcrumb", array("เปลี่ยนรหัสผ่าน" => "#"));

	 ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">เปลี่ยนรหัสผ่าน</h4>
				</div>
				<div class="panel-body">
					<form id="frm" class="form-horizontal">
							<div class="form-group">
									<label for="member_no" class="col-sm-offset-1 col-sm-4 control-label"><h4 class="no-m">รหัสผ่านใหม่</h4></label>
									<div class="col-sm-4">
											<input class="form-control" id="password" name="password" type="password" autocomplete="off" />
									</div>
							</div>
							<div class="form-group">
									<label for="member_no" class="col-sm-offset-1 col-sm-4 control-label"><h4 class="no-m">ยืนยันรหัสผ่านใหม่</h4></label>
									<div class="col-sm-4">
											<input class="form-control" id="password_confirm" name="password_confirm" type="password" autocomplete="off" />
											<div class="help-block"></div>
									</div>
							</div>
							 <div class="form-group">
									<div class="col-sm-12 text-center">
											<button class="btn btn-info" type="button" id="btn-submit">เปลี่ยนรหัสผ่าน</button>
									</div>
							</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" id="modal-password" tabindex="-1" role="dialog" aria-labelledby="modal-password-label" aria-hidden="true">
		<div class="modal-dialog">
		    	<div class="modal-content">
		      	<div class="modal-body text-center">
					<h3>เปลี่ยนรหัสผ่านเรียบร้อยแล้วค่ะ</h3>
					<button type="button" class="btn btn-lg btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
		      	</div>
		    	</div>
	    	</div>
	</div>
<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
