<?php
	session_start();
	require "config.inc.php" ;
	//require PATH."/class/connect_odbc.inc.php";
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");

	ob_start();
?>
<link rel="stylesheet" href="<?php echo PATH;?>ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<style>
		.table > tbody > tr > td
		, .table > tbody > tr > th
		, .table > tfoot > tr > td
		, .table > tfoot > tr > th
		, .table > thead > tr > td
		, .table > thead > tr > th
		, .table td{
			padding-left: 5px!important;
			padding-right: 5px!important;
		}
	</style>

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/jqueryui/jquery.ui.datepicker-th.js"></script>
<script src="/scripts/jqueryui/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
<script src="/scripts/validation/js/formValidation.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<script src="/scripts/validation/js/bootstrap.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
	?>
<script>

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

</script>
	<?php

	 
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);




	$smarty->assign("title", "หน้าแรก" );
	$smarty->assign("breadcrumb", array("Dashboard" => "index.php") );

	 ob_start();
	
	$member_id = $_GET['member_no']; 
	$sql = "SELECT
				t1.*,
				CONCAT(
					t2.prename_full,
					t1.firstname_th,
					' ',
					t1.lastname_th
				) AS member_name,
				t7.mem_group_name,
				CONCAT(t1.c_address_no,' ',t1.c_address_moo,' ',t4.district_name,' ',t5.amphur_name,' ',t6.province_name,' ',t1.c_zipcode) AS member_address,
				t8.mem_type_name AS member_type				
			FROM
				coop_mem_apply AS t1
			LEFT JOIN coop_prename AS t2 ON t1.prename_id = t2.prename_id
			LEFT JOIN coop_district as t4 ON t1.c_district_id = t4.district_id
			LEFT JOIN coop_amphur as t5 ON t1.c_amphur_id = t5.amphur_id
			LEFT JOIN coop_province as t6 ON t1.c_province_id = t6.province_id
			LEFT JOIN coop_mem_group as t7 ON t1.level = t7.id
			LEFT JOIN coop_mem_type AS t8 ON t1.mem_type_id = t8.mem_type_id
			WHERE
				t1.member_id = '{$member_id}'";		
	//echo $sql.'<hr>';			
	$rs = $mysqli_app->query($sql);
	$row = $rs->fetch_assoc();
	/*$sql_profile = "SELECT FLOMMAST.*, FLOPDEPT.DEPT_NAME,FLOPSTATUS.*, FLOPSTATUS.STS_TYPE_DESC
					FROM LOAN.dbo.FLOMMAST FLOMMAST
						INNER JOIN LOAN.dbo.FLOPDEPT FLOPDEPT ON FLOMMAST.MAST_MEMB_DEPT = FLOPDEPT.DEPT_NO
						INNER JOIN LOAN.dbo.FLOPSTATUS FLOPSTATUS ON FLOMMAST.MAST_MEMB_TYPE = FLOPSTATUS.STS_TYPE_ID AND FLOPSTATUS.STS_CODE = '01'
					WHERE FLOMMAST.MAST_MEMB_NO = '{$member_no}'";
				
	$rs_profile = odbc_exec($odbc_conn, $sql_profile) or die(odbc_errormsg());
	if($row_profile = odbc_fetch_array($rs_profile)) {
		$row_profile = t2u_array($row_profile);
		$bd_date = new DateTime(ConvertToSQLDate($row_profile["MAST_BIRTH_YMD"]));
		$bd_interval = $bd_date->diff(new DateTime("now"));
		$md_date = new DateTime(ConvertToSQLDate($row_profile["MAST_YMD_IN"]));
		$md_interval = $md_date->diff(new DateTime("now"));
		
		$sql_data = "SELECT email, telmobile, mem_image
					FROM cmp_imp_member_data
					WHERE member_no = '{$member_no}'";
		$rs_data = $mysqli->query($sql_data);
		$row_data = $rs_data->fetch_assoc();
		
		if(!empty($row_data['mem_image']) ){ 
			$mem_image = "http://".$_SERVER["HTTP_HOST"]."/uploads/members/{$row_data['mem_image']}";
		}else{
			$mem_image = "http://".$_SERVER["HTTP_HOST"].'/images/avatar_man.png';
		}	
		
		$row = [
				'member_no' => $row_profile['MAST_MEMB_NO']
				,	'member_name' => $row_profile['MAST_FNAME'].' '.$row_profile['MAST_NAME']
				,	'member_age' => $bd_interval->format('%Y ปี %m เดือน')
				,	'member_register' =>  ConvertToThaiDate(ConvertToSQLDate($row_profile['MAST_YMD_IN']))
				,	'member_register_age' => $md_interval->format('%Y ปี %m เดือน')
				,	'member_email' => $row_profile['MAST_EMAIL']
				,	'member_mobile' => $row_profile['MAST_MOBILE']
				,	'member_address' => sprintf('%s %s', $row_profile['MAST_ADDR'], $row_profile['MAST_ZIP'])
				,	'member_img' => $mem_image
				,	'member_group' => $row_profile['DEPT_NAME']					// หน่วยงาน
				,	'member_type' => $row_profile['STS_TYPE_DESC']			// ประเภทสมาชิก
				,	'member_bd' => ConvertToThaiDate(ConvertToSQLDate($row_profile['MAST_BIRTH_YMD'])) // วันเกิด
				,	'member_idcard' => $row_profile['MAST_CARD_ID']			// เลขบัตรประชาชน
				,	'member_salary' => $row_profile['MAST_SALARY']
				];
	}
	*/
	
?>
	<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix"><h4 class="panel-title">ข้อมูลผู้กู้</h4></div>
			<div class="panel-body">
				<div class="col-xs-1"></div>
				<div class="col-xs-8">
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">เลขทะเบียน</div>
							<div class="col-xs-8 col-md-5"><?php echo $row["member_id"]; ?></div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">ชื่อ - สกุล</div>
							<div class="col-xs-8 col-md-5"><?php echo $row["member_name"]; ?></div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">หน่วยงาน</div>
							<div class="col-xs-8 col-md-5"><?php echo $row["mem_group_name"]; ?></div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">ประเภทสมาชิก</div>
							<div class="col-xs-8 col-md-5"><?php echo $row["member_type"]; ?></div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">วันเดือนปีเกิด</div>
							<div class="col-xs-8 col-md-5"><?php echo ConvertToThaiDate(ConvertToSQLDate($row["birthday"]), true); ?></div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">อายุ</div>
							<div class="col-xs-8 col-md-5"><?php echo cal_age($row["birthday"]); ?> ปี</div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">เลขบัตรประชาชน</div>
							<div class="col-xs-8 col-md-5"><?php echo $row["id_card"]; ?></div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">เงินเดือน</div>
							<div class="col-xs-8 col-md-2"><?php echo number_format($row["salary"], 2); ?> บาท</div>
					</div>
					<div class="row hidden-xs">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">วันที่เข้าเป็นสมาชิก</div>
							<div class="col-xs-8 col-md-8">
									<?php echo ConvertToThaiDate(ConvertToSQLDate($row["member_date"]), true); ?>
									<span class="m-l-xs f-bold">อายุการเป็นสมาชิก</span>
									<span class="m-l-md"><?php echo cal_age($row["member_date"]); ?> ปี</span>
							</div>
					</div>
					<div class="row hidden-lg hidden-md hidden-sm">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">วันที่เข้าเป็นสมาชิก</div>
							<div class="col-xs-8 col-md-2">
									<?php echo ConvertToThaiDate(ConvertToSQLDate($row["member_date"]), true); ?>
							</div>
					</div>
					<div class="row hidden-lg hidden-md hidden-sm">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">อายุการเป็นสมาชิก</div>
							<div class="col-xs-8 col-md-2">
									<?php echo cal_age($row["member_date"]); ?> ปี
							</div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">ที่อยู่</div>
							<div class="col-xs-8 col-md-10">
									<span class="span-addr">
									<?php echo $row["member_address"]; ?>
									</span>
									<span class="m-l-xxl cursor-pointer text-info edit-profile hide" ref="addr"><i class="fa fa-pencil m-r-xxs"></i>แก้ไข</span>
							</div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">โทรศัพท์มือถือ</div>
							<div class="col-xs-8 col-md-3">
									<span class="span-telmobile">
											<?php echo $row["mobile"]; ?>
									</span>
									<span class="m-l-xxl cursor-pointer text-info edit-profile hide" ref="telmobile"><i class="fa fa-pencil m-r-xxs"></i>แก้ไข</span>
							</div>
					</div>
					<div class="row">
							<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">อีเมล์</div>
							<div class="col-xs-8 col-md-5">
									<span class="span-email">
											<?php echo $row["email"]; ?>
									</span>
									<span class="m-l-xxl cursor-pointer text-info edit-profile hide" ref="email"><i class="fa fa-pencil m-r-xxs"></i>แก้ไข</span>
							</div>
					</div>
				</div>
				<div class="col-xs-2">
					<div class="col-xs-12text-center">
						<img id="mem_image" src="<?php echo $row["member_img"]; ?>" alt="" style="width: 100%; max-width: 150px;" />
					</div>
				</div>					
			</div>
		</div>
	</div>
</div>
<script>
	
</script>
<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
