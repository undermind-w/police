<?php
session_start();

header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;
require "menu.inc.php" ;

// $cmpper->check("message") ;

function fn_sms_member($telmobile, $message,$send_type,$loan_id,$doc_type) {
	global $mysqli;
 	$now = date("Y-m-d H:i:s");

	if ($res = send_sms($telmobile,$message)) {
		
		if ($send_type == "check_doc") {

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='สหกรณ์ กำลังตรวจสอบคำร้องและเอกสารกู้',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				c_check_doc_status = '1',
                                c_check_doc_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}else if($send_type == "contract_doc"){

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='สหกรณ์ กำลังดำเนินการออกสัญญาเงินกู้ให้กับท่าน',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				c_contract_doc = '1',
                                c_contract_doc_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}else if($send_type == "send_doc"){

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='สัญญาเงินกู้ได้จัดส่งเรียบร้อยแล้ว',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				c_send_doc = '1',
                                c_send_doc_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}else if($send_type == "send_approve"){

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='อนุมัติเงินกู้เสร็จสมบรูณ์และทำการโอนเงินเรียบร้อย',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				c_approve = '1',
                                c_approve_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}else{

			 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='ไม่อนุมัติคำขอกู้',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
			 $mysqli->query($send_status);

				   $sql = "update cmp_admin_loan_money set
				 				not_approve = '1',
                                not_approve_date='{$now}',
                                not_approve_message='{$message}'
                                WHERE
                                loan_id = '{$loan_id}'
        			";

		}

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}


function fn_send_bondsman($telmobile, $message , $bondsman_id,$loan_id,$doc_type) {
	global $mysqli;
	if ($res = sms_bondsman($telmobile,$message)) {
 		$now = date("Y-m-d H:i:s");

 		 $status_sql = "update cmp_admin_loan_money set
				 				c_sms_b_status = '1',
                                c_sms_b_date='{$now}'
                                WHERE
                                loan_id = '{$loan_id}'
        			"; 
		 $mysqli->query($status_sql);

		 $sql = "update loan_application_bondsman set
                                bondsman_sms='{$now}'
                                WHERE
                                bondsman_id = '{$bondsman_id}'
        "; 

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}

function fn_send_bondsman_alone($telmobile, $message , $bondsman_id) {
	global $mysqli;
	if ($res = sms_bondsman($telmobile,$message)) {
 		$now = date("Y-m-d H:i:s");

		 $sql = "update loan_application_bondsman set
                                sms_update='{$now}'
                                WHERE
                                bondsman_id = '{$bondsman_id}'
        "; 

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}



function fn_send_sms($telmobile, $loan_id ,$loan_application_type,$member_id) {
	global $mysqli;

	if ($loan_application_type == 1) {
		$link = "http://loan.cmcoop.or.th/common/$loan_id/1/$member_id";
	}else if ($loan_application_type == 2) {
		$link = "http://loan.cmcoop.or.th/atm/$loan_id/2/$member_id";
	}else{
		$link = "http://loan.cmcoop.or.th/emer/$loan_id/3/$member_id";
	}

	if ($res = sendSMS_2($telmobile,$link)) {

		$sql = "UPDATE cmp_admin_loan_money  SET loan_sms = NOW() WHERE loan_id = $loan_id";

		 $sql2 = "INSERT INTO cmp_admin_loan_money_status 
		(loan_id,status_name, status_date,loan_application_type,status_show) VALUES ($loan_id, 'ส่งแบบฟอร์มขอกู้เรียบร้อยแล้ว', NOW(),$loan_application_type,1)";
		$mysqli->query($sql2);

		if ($mysqli->query($sql)) {
			return true;
		}
	}
	return false;
}

if (in_array($_POST['do'], array('sendSMS'))) {
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);
	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$loan_application_type = $mysqli->real_escape_string($_POST['loan_application_type']);

	if (fn_send_sms($telmobile, $loan_id,$application_type)) {
		echo 'true';exit();
	}
	echo 'false';exit();
}
else if (in_array($_POST['do'], array('delfile'))) {
	$file = $mysqli->real_escape_string($_POST['file']);

	if (unlink('../letters/' . $file)) {
		$sql = "DELETE FROM cmp_letters WHERE file = '$file'";
		if ($mysqli->query($sql)) {
			echo 'true';exit();
		}
	}
	echo 'false';exit();
}
else if (in_array($_GET['do'], array('add')) && isset($_POST['member_id'])) {

	$sql_number = "SELECT loan_number
						FROM cmp_admin_loan_money
						ORDER BY loan_number DESC
						LIMIT 1";
			$rs_number = $mysqli->query($sql_number);
			if($row_number = $rs_number->fetch_assoc()) {
				$id = (int)$row_number["loan_number"];
				$loan_number = sprintf("%06d", $id + 1);
			}


	$member_id = $mysqli->real_escape_string($_POST['member_id']);
	$member_name = $mysqli->real_escape_string($_POST['member_name']);
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);
	$loan_application_type = $mysqli->real_escape_string($_POST['loan_application_type']);
	$email = $mysqli->real_escape_string($_POST['email']);

	$sql = "INSERT INTO cmp_admin_loan_money (loan_date, member_id, member_name, telmobile,loan_application_type,email,loan_number) VALUES (NOW(), '$member_id', '$member_name', '$telmobile',$loan_application_type,'$email','$loan_number')";
	

	if (!$mysqli->query($sql)) {
		header("Location: loan_money.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
		exit();
	}

	$lastid = $mysqli->insert_id;

	// if ($_POST['saveandsend'] == 'checked') {
		if (!fn_send_sms($telmobile, $lastid ,$loan_application_type,$member_id)) {
			header("Location: loan_money.php?err=" . urlencode('เกิดข้อผิดพลาดในการส่ง SMS'));
			exit();
		}
	// }

		if ($loan_application_type == 1) {
		$link = "http://loan.cmcoop.or.th/common/$lastid/1/$member_id";
		}else if ($loan_application_type == 2) {
			$link = "http://loan.cmcoop.or.th/atm/$lastid/2/$member_id";
		}else{
			$link = "http://loan.cmcoop.or.th/emer/$lastid/3/$member_id";
		}
				if ($email != "") {

					$msg = 'สวัสดี '.$member_name.' ,<br />
						<br />
						สหกรณ์ออมทรัพย์ครูเชียงใหม่ ได้ส่งแบบฟอร์มขอกู้ให้ท่านแล้ว กรุณากรอกข้อมูลและส่งเอกสารตามลิงก์ที่ส่งมานี้<br />
						<a target="_blank" href="'.$link.'">'.$link.'</a>
						<br />
						ขอขอบคุณ<br />
						ทีมงาน <strong>cmcoop.or.th</strong>';
					@send_mj_mail("คำขอกู้เงิน สหกรณ์ออมทรัพย์ครูเชียงใหม่ " , $msg , $email );
					
				}

	// header("Location: loan_money.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
	header("Location: loan_money.php?res=บันทึกข้อมูลแล้ว&view=all");
	exit();

}
else if (in_array($_GET['do'], array('edit')) && isset($_POST['loan_id'])) {
	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$member_id = $mysqli->real_escape_string($_POST['member_id']);
	$member_name = $mysqli->real_escape_string($_POST['member_name']);
	$telmobile = $mysqli->real_escape_string($_POST['telmobile']);

	// update cmp_admin_loan_money
		$sql = "UPDATE cmp_admin_loan_money
				SET member_id = '$member_id', member_name = '$member_name', telmobile = '$telmobile'
				WHERE loan_id = $loan_id";
		if (!$mysqli->query($sql)) {
			header('Location: loan_money.php?err=เกิดข้อผิดพลาดในการแก้ไขข้อมูล');
			exit();
		}

		// if ($_POST['saveandsend'] == 'checked') {
		// 	if (!fn_send_sms($telmobile, $lastid)) {
		// 		header("Location: loan_money.php?res=เกิดข้อผิดพลาดในการบันทึกสถานะ SMS");
		// 		exit();
		// 	}
		// }

	header('Location: loan_money.php?res=แก้ไขข้อมูลแล้ว');
	exit();

}
else if (in_array($_GET['do'], array('del'))) {

	$loan_id = $mysqli->real_escape_string($_GET['loan_id']);
    $file = $mysqli->real_escape_string($_GET['file']);

    unlink('../MyPDF/' . $file);

    $sql_img = "SELECT * FROM loan_application_doc WHERE loan_id = '$loan_id' ";
	$rs_img = $mysqli->query($sql_img);
	echo $mysqli->error;
	while(($img = $rs_img->fetch_assoc())) {
    unlink('../loan_doc/' . $img['file']);
	}

	$del_doc = "DELETE FROM loan_application_doc WHERE loan_id = '$loan_id'";
    $mysqli->query($del_doc);

	$del_status = "DELETE FROM cmp_admin_loan_money_status WHERE loan_id = '$loan_id'";
    $mysqli->query($del_status);

    $del_application = "DELETE FROM loan_application WHERE loan_id = '$loan_id'";
    $mysqli->query($del_application);

    $del_bondsman = "DELETE FROM loan_application_bondsman WHERE loan_id = '$loan_id'";
    $mysqli->query($del_bondsman);


	$sql = "DELETE FROM cmp_admin_loan_money WHERE loan_id = '$loan_id'";

	if (!$mysqli->query($sql)) {
		header('Location: loan_money.php?err=เกิดข้อผิดพลาดในการลบข้อมูล');
		exit();
	}

	header('Location: loan_money.php?res=ลบข้อมูลแล้ว');
	exit();

}
else if (in_array($_POST['do'], array('sms_bondsman'))) {

$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
$application_id = $mysqli->real_escape_string($_POST['application_id']);
$name_member = $mysqli->real_escape_string($_POST['name_member']);
$doc_type = $mysqli->real_escape_string($_POST['doc_type']);

  if ($doc_type == 1) {
  	$doc_name = "คำขอกู้เงินสามัญ";
  }else if ($doc_type == 2) {
  	$doc_name = "คำขอกู้เงินสามัญ ATM";
  }else{
  	$doc_name = "คำขอกู้เพื่อเหตุฉุกเฉิน";
  }

$now = date("Y-m-d H:i:s");
 $send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name='สหกรณ์ ส่ง SMS ให้ผู้ค้ำประกันยืนยันแล้ว',
							status_date='{$now}',
							loan_application_type ='{$doc_type}'
					        "; 
$mysqli->query($send_status);

if ($mysqli) {

 $sql_bondsman = "SELECT bondsman_id,bondsman_tel FROM loan_application_bondsman WHERE loan_id = $loan_id AND loan_application_id = $application_id ORDER BY bondsman_id ASC ";
$rs_bondsman = $mysqli->query($sql_bondsman); 

while($row_bondsman = $rs_bondsman->fetch_assoc()){ 

$bondsman_id = $row_bondsman['bondsman_id'];
$tel = $row_bondsman['bondsman_tel'];
$message = $name_member."ได้ทำการขอกู้เงินโดยให้ท่านคลิกลิงก์ที่แนบมาเพื่อยืนยันการเป็นผู้ค้ำประกัน http://loan.cmcoop.or.th/confirm/".$loan_id."/".$application_id."/".$bondsman_id."";
// $message = "คุณ ".$name_member." ได้ทำการขอกู้เงินประเภท ".$doc_name."  กับสหกรณ์ออมทรัพย์ครูเชียงใหม่ โดยให้ท่านเป็นผู้ค้ำประกัน คลิกลิงก์ที่แนบมาเพื่อยืนยันการเป็นผู้ค้ำประกัน http://loan.cmcoop.or.th/confirm.php?loan=".$loan_id."&app=".$application_id."&bon=".$bondsman_id." ";
	fn_send_bondsman($tel,$message,$bondsman_id,$loan_id,$doc_type);
}

echo 'true'; exit();


}





}else if (in_array($_POST['do'], array('sms_member'))){

$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
$tel_member = $mysqli->real_escape_string($_POST['tel_member']);
$send_type = $mysqli->real_escape_string($_POST['send_type']);
$doc_type = $mysqli->real_escape_string($_POST['doc_type']);
$not_approve_message = $mysqli->real_escape_string($_POST['not_approve_message']);

if ($send_type == "check_doc") {
	if ($doc_type == 3) {
		$message = "คำขอของท่านได้รับการตรวจสอบแล้ว";
	}else{
		$message = "คำขอของท่านได้รับการตรวจสอบแล้ว สหกรณ์ฯจะส่ง SMS ไปยังผู้ค้ำประกันเพื่อยืนยันการค้ำประกัน";
		// $message = "เอกสารของท่านได้รับการตรวจสอบแล้ว เมื่อผ่านการตรวจแล้วจะส่ง SMS ยืนยันไปหาผู้ค้ำประกันที่ท่านได้กรอกไว้";
	}
}else if($send_type == "contract_doc"){
$message = "คำขออยู่ระหว่างตรวจสอบสิทธิกู้และผู้ค้ำประกันได้ยืนยันค้ำประกันเรียบร้อยแล้ว";
// $message = "สหกรณ์ได้ดำเนินการออกสัญญาเงินกู้ให้กับท่านเรียบร้อยแล้ว กรุณารอขั้นตอนต่อไปจากทาง SMS";
}else if($send_type == "send_doc"){
$message = "สัญญาเงินกู้ได้จัดส่งเรียบร้อยแล้วกรุณาติดต่อหน่วยบริการ";
}else if($send_type == "send_approve"){
$message = "อนุมัติเงินกู้เสร็จสมบรูณ์และทำการโอนเงินเรียบร้อย";
}else{
$message = "สัญญาของท่านไม่ผ่านการอนุมัติ เนื่องจาก ".$not_approve_message;
}

if (fn_sms_member($tel_member,$message,$send_type,$loan_id,$doc_type)) {
		echo 'true';
		exit();
	}

}else if (in_array($_POST['do'], array('bondsman_alone'))){

	$loan_id = $mysqli->real_escape_string($_POST['loan_id']);
	$type = $mysqli->real_escape_string($_POST['application_id']);
	$tel_member = $mysqli->real_escape_string($_POST['bondsman_tel']);
	$message = $mysqli->real_escape_string($_POST['bondsman_message']);
	$bondsman_id = $mysqli->real_escape_string($_POST['bondsman_id']);

	if (fn_send_bondsman_alone($tel_member,$message,$bondsman_id)) {
		
		echo '<script> alert("ดำเนินการส่ง SMS แล้ว"); </script>';
        echo "<script> window.location.href = \"?do=status&loan_id=".$loan_id."&type=".$type."&type_view=".$_POST['type_view']."\"</script>" ;
        exit();
	}
		echo '<script> alert("ไม่สำเร็จกรุณาลองใหม่อีกครั้ง"); </script>';
	 	echo "<script> window.location.href = \"?do=status&loan_id=".$loan_id."&type=".$type."&type_view=".$_POST['type_view']."\"</script>" ;
        exit();


} else {

ob_start();
?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
	.bt_dis{
		cursor: not-allowed !important;
		pointer-events: auto !important;
	}
	.not_line{
		text-decoration: none !important;
	}

/*  SECTIONS  */
.section {
	clear: both;
	padding: 0px;
	margin: 0px;
}

/*  COLUMN SETUP  */
.col {
	display: block;
	float:left;
	margin: 1% 0 1% 1%;
}
.col:first-child { margin-left: 0; }

/*  GROUPING  */
.group:before,
.group:after { content:""; display:table; }
.group:after { clear:both;}
.group { zoom:1; /* For IE 6/7 */ }
/*  GRID OF FIVE  */
.span_5_of_5 {
	width: 100%;
}
.span_4_of_5 {
	width: 79.8%;
}
.span_3_of_5 {
	width: 59.6%;
}
.span_2_of_5 {
	width: 39.4%;
}
.span_1_of_5 {
	width: 19.2%;
}

/*  GO FULL WIDTH BELOW 480 PIXELS */
@media only screen and (max-width: 480px) {
	.col {  margin: 1% 0 1% 0%; }
	.span_1_of_5, .span_2_of_5, .span_3_of_5, .span_4_of_5, .span_5_of_5 { width: 100%; }
}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/validation/js/formValidation.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<script src="/scripts/validation/js/bootstrap.js"></script>
<!-- <script src="/html/meter/admin1/assets/js/pages/dashboard.js"></script> -->

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script>
$(document).ready(function() {
	$('#addFile').on('click', function() {
		$('<input>').attr({
			type: 'file',
			class: 'form-control',
			name: 'letter[]'}).insertBefore($(this).parent()).trigger('click');
	});

	$('.btndel').click(function() {
		var obj = $(this).parents('p');

		if (confirm('ต้องการลบไฟล์นี้')) {
			$.ajax({
				type	: 'POST',
				url	: 'loan_money.php',
				data	: {
					'do'	 : 'delfile',
					'file' : $(this).prop('name')
				},
				success : function(response) {
					if (response === 'true') {
						obj.remove();
					}
				}
			});
		}
	});

	$('#loan-form').formValidation({
		fields: {
			member_id: {
				validators: {
					notEmpty	: { message: '<p>กรุณากรอกรหัสสมาชิก</p>' },
					stringLength: { message: '<p>กรุณาป้อนรหัสอย่างน้อย 6 ตัว', min: 6 },
					regexp	: { message: '<p>กรุณาป้อนตัวเลขเท่านั้น</p>', regexp: /\d$/ }
				}
			},
			member_name: {
				validators: {
					notEmpty	: { message: '<p>กรุณาป้อนชื่อสมาชิก</p>' },
					regexp	: { message: '<p>กรุณาป้อนตัวอักษรเท่านั้น</p>', regexp: /[a-zก-ฮ\W]$/ }
				}
			},
			telmobile: {
				validators: {
					notEmpty	: { message: '<p>กรุณาป้อนเบอร์โทรศัพท์</p>' },
					regexp	: { message: '<p>กรุณาป้อนเบอร์โทรศัพท์</p>', regexp: /^0[8|9].[0-9]{7}$/ }
				}
			},
			// email: {
			// 	validators: {
			// 		notEmpty	: { message: '<p>กรุณาป้อน email</p>' },
			// 		regexp	: { message: '<p>กรุณาป้อน email</p>', regexp: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/ }
			// 	}
			// },
			loan_application_type: {
				validators: {
					notEmpty	: { message: '<p>กรุณาเลือกรูปแบบการกู้เงิน</p>' }
				}
			}
		}
	}).on('status.field.fv', function(e, data) {
		if (data.field === 'telmobile') {
			(data.status === 'VALID' && $('#loan_id').val() !== '')
			    ? $('#btn-sms').removeAttr('disabled')
			    : $('#btn-sms').prop('disabled', 'disabled')
		}
	});

	$('#btn-sms').on('click', function() {
		var obj = $(this);
		var loan_application_type = $('input[name=loan_application_type]:checked').val();

		$.ajax({
			type	: 'POST',
			url	: 'loan_money.php',
			data	: {
				'do'		: 'sendSMS',
				'loan_id'	: $('#loan_id').val(),
				'telmobile'	: $('#telmobile').val(),
				'loan_application_type'	: loan_application_type,
			},
			// async: false,
			success : function(response) {
				var res = '';
				if (response == 'true') {
					res = 'ดำเนินการส่ง SMS แล้ว';
					$('.blgres').text(res).addClass('bg-success');
				} else {
					res = 'เกิดข้อผิดพลาดในการส่ง SMS';
					$('.blgres').text(res).addClass('bg-danger');
				}
			}
		});
	});


	$('#sms_bondsman').on('click', function() {
		var confirm_bondsman = confirm('ต้องการที่จะส่ง SMS ถึงผู้ค้ำประกันทั้งหมด');

		var loan_id = $(this).attr('loan_id');
		var application_id = $(this).attr('application_id');
		var name_member = $(this).attr('name_member');
		var doc_type = $(this).attr('doc_type');

		if (confirm_bondsman) {
			$.ajax({
			type	: 'POST',
			url	: 'loan_money.php',
			data	: {
				'do'		: 'sms_bondsman',
				'loan_id'	: loan_id,
				'application_id'	: application_id,
				'name_member' : name_member,
				'doc_type' : doc_type
			},
			// async: false,
			success : function(msg) {
				// console.log(msg); return false;
				if (msg == 'true') {
					alert("ดำเนินการส่ง SMS ถึงผู้คำประกันแล้ว");
					location.reload();
				} else {
					alert("เกิดข้อผิดพลาดในการส่ง SMS");
					location.reload();
				}
				
			}
		});
		} 
		
	});

	$('.coop_send').on('click', function() {
		var confirm_sms = confirm('ต้องการที่จะส่ง SMS ถึงผู้กู้สัญญา');

		var loan_id = $(this).attr('loan_id');
		var tel_member = $(this).attr('tel_member');
		var send_type = $(this).attr('send_type');
		var doc_type = $(this).attr('doc_type');

		if (confirm_sms) {
			$.ajax({
			type	: 'POST',
			url	: 'loan_money.php',
			data	: {
				'do'		: 'sms_member',
				'loan_id'	: loan_id,
				'doc_type'	: doc_type,
				'tel_member' : tel_member,
				'send_type' : send_type
			},
			// async: false,
			success : function(msg) {
				// console.log(msg); return false;
				// var msg = '';
				if (msg == 'true') {
					alert("ดำเนินการส่ง SMS แล้ว")
					location.reload();
				} else {
					alert("เกิดข้อผิดพลาดในการส่ง SMS")
					location.reload();
				}
			}
		});
		} 
		
	});

	$('.not_approve').on('click', function() {

		var not_approve_message = $("#not_approve_message").val();
		
		if (not_approve_message == "") {

			alert("กรุณากรอกข้อความ");
			return false;

		}else{


		var confirm_sms = confirm('ต้องการที่จะไม่อนุมัติคำขอกู้');

		var send_type = $(this).attr('send_type');
		var loan_id = $(this).attr('loan_id');
		var tel_member = $(this).attr('tel_member');
		var doc_type = $(this).attr('doc_type');

		if (confirm_sms) {
			$.ajax({
			type	: 'POST',
			url	: 'loan_money.php',
			data	: {
				'do'		: 'sms_member',
				'loan_id'	: loan_id,
				'doc_type'	: doc_type,
				'tel_member' : tel_member,
				'send_type' : send_type,
				'not_approve_message' : not_approve_message
			},
			// async: false,
			success : function(msg) {
				// var msg = '';
				if (msg == 'true') {
					alert("ดำเนินการส่ง SMS แล้ว")
					location.reload();
				} else {
					alert("เกิดข้อผิดพลาดในการส่ง SMS")
					location.reload();
				}
			}
		});
		} 

		}
		
	});

});

 $('#search_mem').keyup(function(){  
           var txt = $(this).val();  
           if(txt != '')  
           {  
                $.ajax({  
                     url:"/ajax/ajax_search_member.php",  
                     method:"post",  
                     data:{search:txt},  
                     dataType:"text",  
                     success:function(data)  
                     {  
                     // console.log(data); 
                      $('#result_member').html(data);  
                     }  
                });  
           }else{
            
           }
      });




 $(document).on('click', '.add_member', function(e) {

	var id_mem = $(this).attr('id_mem');
 	var name_mem = $(this).attr('name_mem');
 	var emer_mem = $(this).attr('email_mem');
 	var tel_mem = $(this).attr('tel_mem');

 	$("#member_id").val(id_mem);
 	$("#member_name").val(name_mem);
 	$("#telmobile").val(tel_mem);
 	$("#email").val(emer_mem);

 	$("#search_mem").val('');
    $('#result_member').html("");  
 	$('#myModal').modal('toggle');

});

// subscribe to the "update.maxlength" event
$("textarea").bind("update.maxlength", function(event, element, lastLength, length, maxLength, left){
    // console.log(event, element, lastLength, length, maxLength, left);
});

</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$page = (int) @$_GET["page"]  ;
	$smarty->assign("title", "ระบบคำขอกู้เงิน" );
	// $smarty->assign("page_title", "ระบบคำขอกู้เงิน" );

	ob_start();
?>

<?php 

	function percen($num,$maxnum){

		return $percen = ($num / $maxnum) * 100;

	}
	
	function count_type($type) {

	global $mysqli;

	if ($type == "all") {

	// all
	$sql_count_all = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    		 cmp_admin_loan_money_status.loan_application_type
						    ";
	$rs_all = $mysqli->query($sql_count_all);
	$count_all = $rs_all->fetch_assoc();
	$count = $rs_all->num_rows;
	return  $count;
	// all


	}else if ($type == "process") {

	// process
	 $sql_count_process = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    WHERE cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0 AND cmp_admin_loan_money.c_sms_b_status = 0
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    cmp_admin_loan_money_status.loan_application_type
						    ";
	// $sql_count_process = " SELECT COUNT(*) FROM `cmp_admin_loan_money` WHERE not_approve = 0 AND c_approve = 0 AND c_sms_b_status = 0";
	$rs_process = $mysqli->query($sql_count_process);
	$count_process = $rs_process->fetch_assoc();
	$count = $rs_process->num_rows;
	return  $count;
	// process

	}else if ($type == "bondsman"){

	// bondsman
	$sql_count_bondsman = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    WHERE cmp_admin_loan_money.c_sms_b_status = 1 AND cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    cmp_admin_loan_money_status.loan_application_type
						    ";
	// $sql_count_bondsman = " SELECT COUNT(*) FROM `cmp_admin_loan_money` WHERE c_sms_b_status = 1 AND not_approve = 0 AND c_approve = 0";
	$rs_bondsman = $mysqli->query($sql_count_bondsman);
	$count_bondsman = $rs_bondsman->fetch_assoc();
	$count = $rs_bondsman->num_rows;
	return  $count;
	// bondsman

	}else if($type == "not_approve"){

	// not approve
	// $sql_not_approve = " SELECT COUNT(*) FROM `cmp_admin_loan_money` WHERE not_approve = 1";
	$sql_not_approve = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    WHERE cmp_admin_loan_money.not_approve = 1
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    cmp_admin_loan_money_status.loan_application_type
						    ";
	$rs_not_approve = $mysqli->query($sql_not_approve);
	$count_not_approve = $rs_not_approve->fetch_assoc();
	$count = $rs_not_approve->num_rows;
	return  $count;
	// not approve

	}else{

	//  approve
	// $sql_approve = " SELECT COUNT(*) FROM `cmp_admin_loan_money` WHERE c_approve = 1";
	$sql_approve = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    WHERE cmp_admin_loan_money.c_approve = 1
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    cmp_admin_loan_money_status.loan_application_type
						    ";
	$rs_approve = $mysqli->query($sql_approve);
	$count_approve = $rs_approve->fetch_assoc();
	$count = $rs_approve->num_rows;
	return  $count;
	//  approve

	}

	
	}

	
 ?>
				
				<?php if ($_GET['view']) { ?>

					<div class="row" style="margin-left: 0;margin-right: 0;">
						
						<?php

						$set_type  = array(

						'all' => 	  array(
												"color" => "info",
												"title" => "คำขอกู้ทั้งหมด"
										   ),
						'process' =>  array(
												"color" => "primary",
												"title" => "อยู่ในกระบวนการ"
										   ),
						'bondsman' => array(
												"color" => "warning",
												"title" => "รอผู้ค้ำยืนยัน"
										   ),
						'not_approve' => array(
												"color" => "danger",
												"title" => "ไม่อนุมัติ"
										      ),
						'approve' => array(
												"color" => "success",
												"title" => "อนุมัติแล้ว"
										)
						);

						 ?>
					
						<?php foreach ($set_type as $key => $value) { ?>
						
                        <div class="col span_1_of_5" >
                            <div class="panel info-box panel-white" <?php echo ($key == $_GET['view']) ? "style='background: #eee;'" : "" ; ?>>
                                <div class="panel-body" style="padding: 10px;">
                                    <div class="info-box-stats">
                                        <a class="not_line" href="?view=<?=$key?>">
                                        	<div class="info-box-icon">
	                                        	<i class="fa fa-users pull-right"></i>
		                                    </div>
                                        <p class="counter"><?php echo count_type($key); ?>
	                                        
	                                    </p>
                                        <span style="color: #4E5E6A;"  class="info-box-title">
                                        <?=$value['title']?> | ดูรายละเอียด
                                        </span>
                                        </a>
                                    </div>
                                    <div class="info-box-progress">
                                        <div class="progress progress-xs progress-squared bs-n">
                                            <div class="progress-bar progress-bar-<?=$value['color']?>" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:<?=percen(count_type($key),count_type("all"))?>%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                    </div>

                <?php } ?>


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">ระบบคำขอกู้เงิน <?= (in_array($_GET['do'], array('status'))) ? " > สถานะการขอกู้" : "" ; ?></h4>
			</div>
			<div class="panel-body">
				<?php	if (in_array($_GET['do'], array('add', 'edit'))) {
					if ($_GET['do'] == 'add') {
						$smarty->assign("breadcrumb", array("ระบบคำขอกู้เงิน" => "loan_money.php?view=all", "เพิ่มข้อมูล" => "#"));
					} else {
						$smarty->assign("breadcrumb", array("ระบบคำขอกู้เงิน" => "loan_money.php?view=all", "แก้ไขข้อมูล" => "#"));

						$sql = "SELECT * FROM cmp_admin_loan_money WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
						$user = $result->fetch_object();

						$sql = "SELECT * FROM cmp_letters WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
					}
				?>
					<form action="" class="form-horizontal" id="loan-form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="loan_id" id="loan_id" value="<?= (isset($user->loan_id)) ? $user->loan_id : '' ?>">
						<div class="form-group">
							<label for="member_id" class="col-sm-3 control-label">รหัสสมาชิก</label>
							<div class="col-sm-4">
								<input type="text" id="member_id" class="form-control" name="member_id" placeholder="รหัสสมาชิก"
									value="<?= (isset($user->member_id)) ? $user->member_id : '' ?>" autofocus>
							</div>
							<div class="col-sm-4">
                            <!-- Button trigger modal -->
							 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i>
                              </button>
                             <!-- Button trigger modal -->
							</div>

						</div>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">ค้นหาสมาชิก</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                       <div class="form-group">
															<label for="search" class="col-sm-1 control-label">ค้นหา</label>
															<div class="col-sm-5">
																<input type="text" id="search_mem" class="form-control" name="search" placeholder="กรอกรหัสสมาชิกหรือชื่อ"
																	value="">
															</div>
														</div>

														<div class="bs-example" data-example-id="striped-table">
															 <table class="table table-striped">

															 <thead>
				                                                <tr>
				                                                    <th width="20px">#</th>
				                                                    <th class="text-center" width="100">รหัสสมาชิก</th>
				                                                    <th  width="300">ชื่อ - สกุล</th>
				                                                </tr>
				                                            </thead> 

										                <tbody id="result_member">
										                
										                </tbody>

														 
															  </table> 
															</div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Modal -->

						<div class="form-group">
							<label for="member_name" class="col-sm-3 control-label">ชื่อสกุล</label>
							<div class="col-sm-4">
								<input type="text" id="member_name" class="form-control" name="member_name"
									value="<?= (isset($user->member_name)) ? $user->member_name : '' ?>" placeholder="ชื่อสกุล">
							</div>
						</div>
						<div class="form-group">
							<label for="telmobile" class="col-sm-3 control-label">มือถือ</label>
							<div class="col-sm-4">
								<input type="text" id="telmobile" class="form-control" name="telmobile" placeholder="มือถือ"
									value="<?= (isset($user->telmobile)) ? $user->telmobile : '' ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="telmobile" class="col-sm-3 control-label">E-mail</label>
							<div class="col-sm-4">
								<input type="text" id="email" class="form-control" name="email" placeholder="email"
									value="<?= (isset($user->email)) ? $user->email : '' ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">รูปแบบการกู้เงิน</label>
							<div class="col-sm-6" id="divfile">
							<label class="radio-inline">
							  <input <?= ($user->loan_application_type == 1) ? "checked" : "" ; ?> type="radio" name="loan_application_type" id="application" value="1"> กู้เงินสามัญ
							</label>
							<label class="radio-inline">
							  <input  <?= ($user->loan_application_type == 2) ? "checked" : "" ; ?> type="radio" name="loan_application_type" id="application" value="2"> กู้เงินสามัญ ATM
							</label>
							<label class="radio-inline">
							  <input  <?= ($user->loan_application_type == 3) ? "checked" : "" ; ?> type="radio" name="loan_application_type" id="application" value="3"> กู้เหตุฉุกเฉิน
							</label>
							</div>
						</div>
						<!-- <div class="form-group">
							<label for="" class="col-sm-3 control-label"></label>
							<div class="col-sm-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="saveandsend" value="checked" <?= (isset($_GET['loan_id'])) ? '' : 'checked' ?>> บันทึกและส่ง SMS
									</label>
								</div>
							</div>
						</div> -->

						<div class="form-group">
							<div class="col-sm-9 control-label">
								<p class="text-center">
									<button type="submit" class="btn btn-success2">บันทึก</button>
									<a href="?view=all" class="btn btn-success2" id="btn-sms">กลับ</a>
								</p>
								<p class="blgres text-center"></p>
							</div>
						</div>
					</form>
				<?php
					} else if ((in_array($_GET['do'], array('status')))) {

						$smarty->assign("breadcrumb", array("ระบบคำขอกู้เงิน" => "loan_money.php?view=all", "สถานะการกู้" => "#"));

						$loan_id = $_GET['loan_id'];
						$doc_type = $_GET['type'];

						$update_status = "update cmp_admin_loan_money_status set
                                status_show='1'
                                WHERE
                                loan_id='{$_GET['loan_id']}'
                                AND
                                loan_application_type = '{$_GET['type']}' 
				        ";
				        $mysqli->query($update_status); 

						$sql = "SELECT * FROM cmp_admin_loan_money WHERE loan_id = $loan_id";
						$rs = $mysqli->query($sql);
   						echo $mysqli->error;
    					$row = $rs->fetch_assoc();

    					$sql_application = "SELECT * FROM loan_application WHERE loan_id = $loan_id AND loan_application_type = $doc_type ";
						$rs_application = $mysqli->query($sql_application);
   						echo $mysqli->error;
    					$row_application = $rs_application->fetch_assoc();
    					$application_id = $row_application['loan_application_id'];

    						// update pdf
 	 					if($_GET['type'] == 1){

 	 					file_get_contents("http://loan.cmcoop.or.th/loan_common_pdf.php?loan=$loan_id&application=$application_id&do=edit");

						}else if($_GET['type'] == 2){ 

 	 					file_get_contents("http://loan.cmcoop.or.th/loan_atm_pdf.php?loan=$loan_id&application=$application_id&do=edit");

						}else{ 

 	 					file_get_contents("http://loan.cmcoop.or.th/loan_emer_pdf.php?loan=$loan_id&application=$application_id&do=edit");
						}
 	 					// update pdf
					?>
					<div class="text-left">

						<div class="col-sm-12">
						<p class="col-sm-2">วันที่</p>
						<p class="col-sm-9"><?= ConvertToThaiDate($row["loan_date"], true, true, true);?></p>
						</div>

						<div class="col-sm-12">
						<p class="col-sm-2">รหัสสมาชิก</p>
						<p class="col-sm-9"><?=$row["member_id"]?></p>
						</div>

						<div class="col-sm-12">
						<p class="col-sm-2">ชื่อ-สกุล</p>
						<p class="col-sm-9"><?=$row["member_name"]?></p>
						</div>
						
						<div class="col-sm-12">
						<?php if($_GET['type'] == 1){ ?>
						<p class="col-sm-2">ประเภทใบคำกู้ : </p>
						<p class="col-sm-9">ใบคำขอกู้เงินสามัญ</p>
						<?php }else if($_GET['type'] == 2){ ?>
						<p class="col-sm-2">ประเภทใบคำกู้ : </p>
						<p class="col-sm-9">ใบคำขอกู้เงินสามัญ ATM</p>
						<?php }else{ ?>
						<p class="col-sm-2">ประเภทใบคำกู้ : </p>
						<p class="col-sm-9">ใบคำขอกู้เหตุฉุกเฉิน</p>
						<?php } ?>
						</div>


						<div class="col-sm-12" style="margin-bottom: 2em;">

						<?php if ($row_application['application_status'] == 1) { ?>
						 
						<p class="col-sm-2">หลักฐานการขอกู้</p>
						<p class="col-sm-10">
						ดาวน์โหลด >
						  <a target="_blank" href="download_pdf.php?file=../MyPDF/PDF_<?=$loan_id?>_<?=$application_id?>.pdf">เอกสารการสมัคร</a> ,
						<?php 
    					 $sql_doc = "SELECT * FROM loan_application_doc WHERE loan_id = $loan_id AND loan_application_id = $application_id ";
						 $rs_doc = $mysqli->query($sql_doc);
						 $i = 1;
                         while($row_doc = $rs_doc->fetch_assoc()){ ?>
						 <a target="_blank"  href="download_doc.php?filename=<?=$row_doc['file']?>">เอกสารการเงิน <?=$i?></a>
						<?php $i++; } ?>
						</p>
						<?php }else{ ?>
						<p class="col-sm-2">หลักฐานการขอกู้</p>
						<p class="col-sm-10">ดาวน์โหลด > ยังไม่ได้ส่งเอกสาร</p>
						<?php } ?>

						</div>

					</div>
					
						
						<h2 class="text-center" >สถานะการดำเนินการ</h2>

						<?php if ($row['not_approve'] == 1) { ?>
						<div class="alert alert-danger text-center" role="alert" style="background: red;color: #fff;">
							 <?php 
							 	 $cut_massage = str_replace("สัญญาของท่านไม่ผ่านการอนุมัติ เนื่องจาก "," ", $row["not_approve_message"]);
							  ?>
                             <?php echo ConvertToThaiDate($row["not_approve_date"], true, true, true)." ไม่อนุมัติคำขอกู้ เนื่องจาก ".$cut_massage ?>     
                        </div>
                        <?php } ?>
					<div style="clear: both;margin-bottom:15px"></div>
						<table class="table table-striped">
							<thead>
								<tr>
									<th width="100" class="text-center">วันที่/เวลา</th>
									<th width="100" class="text-left">สถานะ</th>
								</tr>
							</thead>
							<tbody>
									<!-- <tr>
										<td class="text-center"><?= ConvertToThaiDate($row["loan_date"], true, true, true);?></td>
										<td class="text-left">สหกรณ์ : ส่งแบบฟอร์มขอกู้เรียบร้อยแล้ว</td>
									</tr> -->
									<tr>
										<td class="text-center"><?=($row_application['approve_time'] != "") ? ConvertToThaiDate($row_application["approve_time"], true, true, true) : "" ;?></td>
										<td class="text-left">ผู้ขอกู้ : ป้อนรายละเอียดและส่งคำขอกู้</td>
									</tr>
									<tr> 
										<td class="text-center">
										<?php if ($row["c_check_doc_status"] == 1) { ?>
										<?=ConvertToThaiDate($row["c_check_doc_date"], true, true, true)?>
										<?php }else{ ?>

										<?php if ($row["not_approve"] == 0) { ?>
										<button 
												tel_member = "<?=$row["telmobile"]?>" 
												send_type = "check_doc"
												loan_id="<?=$loan_id?>"
												doc_type="<?=$row['loan_application_type']?>"
										 <?= ($row['b_send_doc_status'] == 0) ? "disabled class='btn btn-info bt_dis' " : "class='btn btn-info coop_send'" ;?> type="button" >ส่ง SMS</button>
										 <?php } ?>

										<?php } ?>
										</td>
										<td class="text-left">สหกรณ์ : กำลังตรวจสอบคำร้องและเอกสารกู้</td>
									</tr>
									
									<?php  if ($doc_type != 3) { ?>
									<tr>
										<td class="text-center">
										<?php if ($row['c_sms_b_status'] == 1) { ?>
											<?=ConvertToThaiDate($row["c_sms_b_date"], true, true, true)?>
										<?php }else{ ?>

										<?php if ($row["not_approve"] == 0) { ?>
										<button 
										<?= ($row['c_check_doc_status'] == 0) ? "disabled class='btn btn-info bt_dis' " : "class='btn btn-info'" ;?>
												id="sms_bondsman"
												type="button"
												loan_id="<?=$loan_id?>"
												application_id="<?=$row_application['loan_application_id']?>"
												name_member="<?=$row['member_name']?>"
												doc_type="<?=$doc_type?>"  >ส่ง SMS</button>
										<?php } ?>

										<?php } ?>
										</td>
										<td class="text-left">สหกรณ์ : ส่ง SMS ถึงผู้ค้ำประกัน รอการยืนยันจากผู้ค้ำประกัน</td>
									</tr>

					<?php 
					$i = 1;
    				 $sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_id = $loan_id AND loan_application_id = '{$row_application['loan_application_id']}' ORDER BY bondsman_id ASC ";
                     	$rs_bondsman = $mysqli->query($sql_bondsman);
                        while($row_bondsman = $rs_bondsman->fetch_assoc()){ 
   						echo $mysqli->error;

								 ?>
									<tr>
										<td class="text-center">
										<?= ($row_bondsman['sms_update'] != "0000-00-00 00:00:00") ? "ส่งไปอีกครั้งเมื่อ ".ConvertToThaiDate($row_bondsman["sms_update"], true, true, true)."<br>" : "" ; ?>
										<?= ($row_bondsman['bondsman_approve'] == 1) ? ConvertToThaiDate($row_bondsman["approve_time"], true, true, true) : "ผู้ค้ำยังไม่ได้ดำเนินการ" ;?>
    									</td>
										<td class="text-left">ผู้ค้ำประกัน : <?=$row_bondsman['bondsman_name']?> ผู้ค้ำประกันคนที่ <?=$row_bondsman['bondsman_num']?> <?=($row_bondsman['bondsman_approve'] == 1) ? "ได้กดยอมรับแล้ว" : "ยังไม่ได้กดยอมรับ" ;?>
											
										<?php if ($row["not_approve"] == 0) { ?>
										<div class="pull-right">
										<?php if ($row_bondsman['bondsman_approve'] == 0) { ?>
										<button  
										<?= ($row['c_sms_b_status'] == 0) ? "disabled class='btn btn-info bt_dis' " : "class='btn btn-info'" ;?>
												data-toggle="modal"
										 		data-target="#bs-example-modal-sms<?=$i?>"
												type="button"
												 >ส่ง SMS เตือนอีกครั้ง
										</button>
										<?php } ?>
										</div>

						<!-- modal -->
							<div id="bs-example-modal-sms<?=$i?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title text-center" id="mySmallModalLabel">ส่ง SMS หาผู้ค้ำอีกครั้ง</h4>
                                  </div>
                                        <div class="modal-body">
                                        <label class="control-label">ข้อความแจ้งผู้ค้ำ</label>
										<form action="?" class="form-horizontal" method="post" enctype="multipart/form-data">

                                        <input type="hidden" name="type_view" value="<?=$_GET['type_view']?>">
                                        <input type="hidden" name="do" value="bondsman_alone">
                                        <input type="hidden" name="bondsman_id" value="<?=$row_bondsman["bondsman_id"]?>">
                                        <input type="hidden" name="loan_id" value="<?=$loan_id?>">
                                        <input type="hidden" name="application_id" value="<?=$_GET['type']?>">
                                        <input type="hidden" name="bondsman_tel" value="<?=$row_bondsman["bondsman_tel"]?>">
										<textarea maxlength="134" id="bondsman_message" class="form-control" rows="3" name="bondsman_message" id="not_approve_message"><?php echo $message = "".$row["member_name"]." ได้ทำการขอกู้เงินโดยให้ท่านคลิกลิงก์ที่แนบมาเพื่อยืนยันการเป็นผู้ค้ำประกัน http://loan.cmcoop.or.th/confirm/".$loan_id."/".$application_id."/".$row_bondsman["bondsman_id"].""; ?></textarea>
                                         </div>
                                    <div class="modal-footer">
                                         <button type="submit" class="btn btn-info">ยืนยัน</button>
                                          <button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
                                    </div>
                                    </form>
                                                    </div>
                                                </div>
                                            </div>

							<!-- modal -->

										<?php } ?>

										</td>

									</tr>
								<?php $i++;	} ?>
								<?php } ?>
									<tr>
									<?php  if ($doc_type == 3) { ?>

									<!-- <td class="text-center">
										<?php if ($row['c_contract_doc'] == 1) { ?>
											<?=ConvertToThaiDate($row["c_contract_doc_date"], true, true, true)?>
										<?php }else{ ?>
										<button <?= ($row['c_check_doc_status'] == 0) ? "disabled class='btn btn-info bt_dis' " : "class='btn btn-info coop_send'" ;?>
										 type="button"
										 tel_member = "<?=$row["telmobile"]?>" 
										 send_type = "contract_doc"
										 loan_id="<?=$loan_id?>"
										 doc_type="<?=$row['loan_application_type']?>"
										 >ส่ง SMS</button>
										<?php } ?>
										</td>
										<td class="text-left">สหกรณ์ : สหกรณ์ กำลังดำเนินการออกสัญญาเงินกู้ให้กับท่าน</td> -->

									<?php }else{ ?>

									<td class="text-center">
										<?php if ($row['c_contract_doc'] == 1) { ?>
											<?=ConvertToThaiDate($row["c_contract_doc_date"], true, true, true)?>
										<?php }else{ ?>

										<?php if ($row["not_approve"] == 0) { ?>
										<button <?= ($row['c_sms_b_status'] == 0) ? "disabled class='btn btn-info bt_dis' " : "class='btn btn-info coop_send'" ;?>
										 type="button"
										 tel_member = "<?=$row["telmobile"]?>" 
										 send_type = "contract_doc"
										 loan_id="<?=$loan_id?>"
										 doc_type="<?=$row['loan_application_type']?>"
										 >ส่ง SMS</button>
										<?php } ?>

										<?php } ?>
										</td>
										<td class="text-left">สหกรณ์ : สหกรณ์ กำลังดำเนินการออกสัญญาเงินกู้ให้กับท่าน</td>

										
									<?php } ?>
									</tr>

									<?php if ($doc_type != 3) { ?>

									<tr>
										<td class="text-center">
										<?php if ($row['c_send_doc'] == 1) { ?>
											<?=ConvertToThaiDate($row["c_send_doc_date"], true, true, true)?>
										<?php }else{ ?>

										<?php if ($row["not_approve"] == 0) { ?>
										<button <?= ($row['c_contract_doc'] == 0) ? "disabled class='btn btn-info bt_dis' " : "class='btn btn-info coop_send'" ;?>
										 type="button"
										 tel_member = "<?=$row["telmobile"]?>" 
										 send_type = "send_doc"
										 loan_id="<?=$loan_id?>"
										 doc_type="<?=$row['loan_application_type']?>"
										 >ส่ง SMS</button>
										<?php } ?>

										<?php } ?>
										</td>
										<td class="text-left">สหกรณ์ : สัญญาเงินกู้ได้จัดส่งเรียบร้อยแล้ว ติดต่อหน่วยบริการ</td>
									</tr>

									<?php } ?>	


									<?php if ($doc_type == 3) { ?>

									<tr>
									<td class="text-center">
										<?php if ($row['c_approve'] == 1) { ?>
											<?=ConvertToThaiDate($row["c_approve_date"], true, true, true)?>
										<?php }else{ ?>

										<?php if ($row["not_approve"] == 0) { ?>
										<button <?= ($row['c_check_doc_status'] == 0) ? "disabled class='btn btn-info bt_dis' " : "class='btn btn-info coop_send'" ;?>
										 type="button"
										 tel_member = "<?=$row["telmobile"]?>" 
										 send_type = "send_approve"
										 loan_id="<?=$loan_id?>"
										 doc_type="<?=$row['loan_application_type']?>"
										 >ส่ง SMS</button>
										<?php } ?>

										<?php } ?>
										</td>
										<td class="text-left">สหกรณ์ : อนุมัติเงินกู้เสร็จสมบรูณ์และทำการโอนเงินเรียบร้อย</td>
									</tr>

									<?php }else{ ?>

									<tr>
									<td class="text-center">
										<?php if ($row['c_approve'] == 1) { ?>
											<?=ConvertToThaiDate($row["c_approve_date"], true, true, true)?>
										<?php }else{ ?>

										<?php if ($row["not_approve"] == 0) { ?>
										<button <?= ($row['c_send_doc'] == 0) ? "disabled class='btn btn-info bt_dis' " : "class='btn btn-info coop_send'" ;?>
										 type="button"
										 tel_member = "<?=$row["telmobile"]?>" 
										 send_type = "send_approve"
										 loan_id="<?=$loan_id?>"
										 doc_type="<?=$row['loan_application_type']?>"
										 >ส่ง SMS</button>
										<?php } ?>

										<?php } ?>
										</td>
										<td class="text-left">สหกรณ์ : อนุมัติเงินกู้เสร็จสมบรูณ์และทำการโอนเงินเรียบร้อย</td>
									</tr>

									<?php } ?>



									

							</tbody>
						</table>
						
						<?php if ($row['c_approve'] == 0) { ?>

							<div class="text-center">
							<?php if ($row['not_approve'] == 0) { ?>
								<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm">ไม่อนุมัติคำขอกู้</button>
							<?php } ?>
								<a href="?view=<?=$_GET['type_view']?>">
								<button style="width: 111px;" type="button" class="btn btn-info">กลับ</button>
								</a>
							</div>


							<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title text-center" id="mySmallModalLabel">ไม่อนุมัติคำขอกู้</h4>
                                  </div>
                                        <div class="modal-body">
                                        <label class="control-label">ข้อความแจ้งผู้ขอกู้</label>
										<textarea maxlength="90" id="not_approve_message" class="form-control" rows="3" name="not_approve_message" id="not_approve_message"></textarea>
                                         </div>
                                    <div class="modal-footer">
                                         <button type="button" class="btn btn-info not_approve" data-dismiss="modal"
 										 tel_member = "<?=$row["telmobile"]?>" 
										 send_type = "not_approve"
										 loan_id="<?=$loan_id?>"
										 doc_type="<?=$row['loan_application_type']?>"
                                         >ยืนยัน</button>
                                          <button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
                                    </div>
                                                    </div>
                                                </div>
                                            </div>

						<?php } ?>


				<?php 	} else {

							$page_type = $_GET['view'];
							if ($page_type == "") {
								$page_type = "all";
							}else{
								$page_type = $_GET['view'];
							}

							$smarty->assign("breadcrumb", array("ระบบคำขอกู้เงิน" => "?") );

							if ($page_type == "all") {
									// all
									// $sql = " SELECT * FROM `cmp_admin_loan_money`";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money
										    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "";
									// all
							}else if ($page_type == "process") {
									// process
									// $sql = " SELECT * FROM `cmp_admin_loan_money` WHERE not_approve = 0 AND c_approve = 0 AND c_sms_b_status = 0";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money
										    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
										    WHERE cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0 AND cmp_admin_loan_money.c_sms_b_status = 0
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "WHERE cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0 AND c_sms_b_status = 0 ";
									// process
							}else if ($page_type == "bondsman"){
									// bondsman
									// $sql = " SELECT * FROM `cmp_admin_loan_money` WHERE c_sms_b_status = 1 AND not_approve = 0 AND c_approve = 0";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money
										    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
										    WHERE cmp_admin_loan_money.c_sms_b_status = 1 AND cmp_admin_loan_money.not_approve = 0 AND cmp_admin_loan_money.c_approve = 0
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "WHERE c_sms_b_status = 1 AND not_approve = 0 AND c_approve = 0";
									// bondsman
							}else if($page_type == "not_approve"){
									// not approve
									// $sql = " SELECT * FROM `cmp_admin_loan_money` WHERE not_approve = 1";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money
										    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
										    WHERE cmp_admin_loan_money.not_approve = 1
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "WHERE not_approve = 1 ";
									// not approve
							}else{
									//  approve
									// $sql = " SELECT * FROM `cmp_admin_loan_money` WHERE c_approve = 1";
									$sql = "SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
										    FROM cmp_admin_loan_money_status 
										    INNER JOIN cmp_admin_loan_money
										    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
										    WHERE cmp_admin_loan_money.c_approve = 1
										    GROUP BY cmp_admin_loan_money_status.loan_id,
										    		 cmp_admin_loan_money_status.loan_application_type
										    ORDER BY MAX(cmp_admin_loan_money_status.status_id) ";
									$set_and = "WHERE c_approve = 1 ";
									//  approve
							}
						// $sql = "SELECT * FROM cmp_admin_loan_money";
						$rs = $mysqli->query($sql);
						$num_rows = $rs->num_rows;
						$per_page = 20 ;
						$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

						$pagination = (new Pagination());

						$pagination->setCurrent($page);
						$pagination->setRPP($per_page);
						$pagination->setTotal($num_rows);
						$pagination->setCrumbs(25);
						$paging = $pagination->parse();

						$page_start = (($per_page * $page) - $per_page);

						  $sql_max = " SELECT max(cmp_admin_loan_money_status.status_id) AS MAX_ID
						    FROM cmp_admin_loan_money_status 
						    INNER JOIN cmp_admin_loan_money
						    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						    {$set_and}
						    GROUP BY cmp_admin_loan_money_status.loan_id,
						    		 cmp_admin_loan_money_status.loan_application_type
						    ORDER BY MAX(cmp_admin_loan_money_status.status_id) DESC
						    LIMIT {$page_start} , {$per_page} ";
						
						$rs_max = $mysqli->query($sql_max);

						$list = array();
						while(($row_max = $rs_max->fetch_assoc())) {
								 $sql = "SELECT * FROM cmp_admin_loan_money_status 
						    		 LEFT JOIN cmp_admin_loan_money ON cmp_admin_loan_money_status.loan_id = cmp_admin_loan_money.loan_id
						    		 WHERE cmp_admin_loan_money_status.status_id = '{$row_max['MAX_ID']}'
						       ";

						       $rs = $mysqli->query($sql);
						       while($row = $rs->fetch_assoc( )){
							        $list[] = $row ; 
							   }
						}
						 // $sql_set = " SELECT cmp_admin_loan_money_status.*,cmp_admin_loan_money.*
						 //    FROM cmp_admin_loan_money_status 
						 //    LEFT JOIN cmp_admin_loan_money
						 //    ON cmp_admin_loan_money.loan_id = cmp_admin_loan_money_status.loan_id
						 //    {$set_and}
						 //    GROUP BY cmp_admin_loan_money_status.loan_id,
						 //    		 cmp_admin_loan_money_status.loan_application_type
						 //    ORDER BY MAX(cmp_admin_loan_money_status.status_id) DESC
						 //    LIMIT {$page_start} , {$per_page} "; 

						 $page_start++;

						?>

						<!-- <a class="btn btn-info pull-right" href="?do=add"  >เพิ่มคำขอ</a> -->
						<?php if (isset($_GET['err'])) { ?>
							<div class="alert alert-danger resalt">
								<p>ERROR : <?= $_GET['err'] ?></p>
							</div>
						<?php } elseif (isset($_GET['res'])) { ?>
							<div class="alert alert-success resalt">
								<p>! <?= $_GET['res'] ?></p>
							</div>
						<?php } ?>
						<div style="clear: both;margin-bottom:15px"></div>
						<table class="table">
							<thead>
								<tr>
									<th width="50" class="text-center"></th>
									<th width="105" class="text-left">เลขที่คำขอกู้</th>
									<th width="150" class="text-center">วันที่ทำรายการ</th>
									<th width="150" class="text-center">ประเภทการขอกู้เงิน</th>
									<th width="90" class="text-left">จำนวน</th>
									<th width="150" class="text-center">สถานะล่าสุด</th>
									<th width="160" class="text-center">ล่าสุดเมื่อ</th>
									<th width="40" class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($list as $key => $row) { ?>
									<tr>
										<td class="text-center">
										<?=($row["not_approve"] == 1) ? "<span class='label label-danger'>ไม่อนุมัติ</span>" : "" ;?>
										<?=($row["c_approve"] == 1) ? "<span class='label label-success '>อนุมัติเรียบร้อย</span>" : "" ;?>
										<?= ($row["status_show"] == 0) ? "<span class='label label-info'>Update</span>" : "" ;?>
										</td>
										<!-- <td class="text-center"><?= $page_start++ ?></td> -->
										<td class="text-left">
										<a href="?do=status&loan_id=<?= $row['loan_id'] ?>&type=<?=$row['loan_application_type']?>&type_view=<?=$_GET['view']?>" title="สถานะ"><?=$row['loan_number']?></a>
										</td>
										<td class="text-center"><?php echo ConvertToThaiDate($row["status_date"], true, true, true); ?></td>
										<!-- <td class="text-center"><?php echo $row["member_id"]; ?></td> -->
										<td class="text-center">
											<?php if($row['loan_application_type'] == 1){ ?>
											ใบคำขอกู้เงินสามัญ
											<?php }else if($row['loan_application_type'] == 2){ ?>
											ใบคำขอกู้เงินสามัญ ATM
											<?php }else{ ?>
											ใบคำขอกู้เหตุฉุกเฉิน
											<?php } ?>
										</td>

										<!-- <td class="text-left"><?php echo $row["member_name"]; ?></td> -->
										<td class="text-left">
										<?php 
										 $sql_money = "SELECT loan_money FROM loan_application WHERE loan_id = '{$row["loan_id"]}' AND loan_application_type = '{$row["loan_application_type"]}' "; 
											$rs_money = $mysqli->query($sql_money);
   											echo $mysqli->error;
    										$row_money = $rs_money->fetch_assoc();

    										echo ($row_money['loan_money'] == "") ? "" : number_format($row_money['loan_money'],2) ;
										 ?>
										</td>

										<td class="text-left">
											 <?php echo $row['status_name']?>
											 <?php //echo ConvertToThaiDate($row["status_date"], true, true, true); ?>
										</td>
										<td class="text-left">
											เมื่อ <?php echo ConvertToThaiDate($row["status_date"], true, true, true); ?>
										</td>

										<td class="text-center">
											<?php //if ($row['b_send_doc_status'] == 1) { ?>
											<?php 
											//  $sql_application = "SELECT loan_application_id FROM loan_application WHERE loan_id = '{$row["loan_id"]}' AND loan_application_type = '{$row["loan_application_type"]}' "; 
											// $rs_application = $mysqli->query($sql_application);
   								// 			echo $mysqli->error;
    							// 			$row_application = $rs_application->fetch_assoc();
    							// 			$application_id = $row_application['loan_application_id'];
											 ?>
											<!-- <a target="_blank" href="download_pdf.php?file=../MyPDF/PDF_<?=$row["loan_id"]?>_<?=$application_id?>.pdf" title="เอกสาร"></a> -->
											<!-- ส่งเอกสารแล้ว -->
											<?php //}else{ ?>
											<!-- ยังไม่ส่งเอกสาร -->
											<?php //} ?>
											<!--  |
											<a href="?do=status&loan_id=<?= $row['loan_id'] ?>&type=<?=$row['loan_application_type']?>" title="สถานะ">สถานะ</a> | -->
											<!-- <a href="?do=edit&loan_id=<?= $row['loan_id'] ?>" title="แก้ไขข้อมูล">แก้ไข</a> | -->
											<a href="?do=del&loan_id=<?= $row['loan_id'] ?>&file=PDF_<?=$row["loan_id"]?>_<?=$application_id?>.pdf" onclick="return confirm('ต้องการลบข้อมูลนี้');" title="ลบข้อมูล" >ลบ</a>
										</td>
									</tr>
								<?php
									}
									unset($sql, $rs, $row);
								?>
							</tbody>
						</table>
					<?php echo $paging;  ?>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();
}

$smarty->assign("content", $content);
$smarty->display(THEME.".tpl");
