<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	
	header("Content-type: application/vnd.ms-excel;charset=utf-8;");
	header("Content-Disposition: attachment; filename=export.xls"); 
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");
?>
<?php 
				$where = '1=1';
				if($_GET['search']!=''){
					$where .= " AND (t1.member_no LIKE '%".$_GET['search']."%' OR t1.memname LIKE '%".$_GET['search']."%')";
				}
				if($_GET['date_start']!=''){
					$date_start_arr = explode('/',$_GET['date_start']);
					$date_start = ($date_start_arr[2])."-".sprintf("%02d",$date_start_arr[1])."-".sprintf("%02d",$date_start_arr[0])." 00:00:00";
				}
				if($_GET['date_end']!=''){
					$date_end_arr = explode('/',$_GET['date_end']);
					$date_end = ($date_end_arr[2])."-".sprintf("%02d",$date_end_arr[1])."-".sprintf("%02d",$date_end_arr[0])." 23:59:59";
				}
				if($_GET['date_start'] != '' && $_GET['date_end'] != ''){
					$where .= " AND t0.createdate BETWEEN '".$date_start."' AND '".$date_end."' ";
				}else if($_GET['date_start'] == '' && $_GET['date_end'] != ''){
					$where .= " AND t0.createdate <= '".$date_end."' ";
				}else if($_GET['date_start'] != '' && $_GET['date_end'] == ''){
					$where .= " AND t0.createdate >= '".$date_start."' ";
				}
				
				$sql = "SELECT * FROM cmp_imp_member_data_change t0
					INNER JOIN cmp_imp_member t1 ON  t0.member_no =  t1.member_no
					INNER JOIN cmp_imp_member_data t2 ON t1.member_no = t2.member_no
					WHERE ".$where."
								ORDER BY t0.createdate DESC";
				$rs = $mysqli->query($sql);
				echo $mysqli->error;
				$types = array(1 => "แก้ไขอีเมล์"  , "แก้ไขเบอร์โทร" , "แก้ไขที่อยู่" );
?>
<pre>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		.num {
  mso-number-format:General;
}
.text{
  mso-number-format:"\@";/*force text*/
}
.text-center{
	text-align: center;
}
.text-left{
	text-align: left;
}
	</style>

</head>
<body>

<table class="table table-bordered" border='1'>
					<thead>
						<tr>
 							<th class="text-center" width="175" >วันที่</th>
							<th class="text-center"  width="150">รหัสสมาชิก</th>
							<th class="text-center" width="200">ชื่อสกุล</th>
							<th class="text-center"  width="100" >ประเภท</th>
 							<th class="text-center"  width="200">ข้อมูล</th>
							<th class="text-center"  width="100">สถานะ</th>
						</tr>
					</thead>
					<tbody>
					<?php while(($row = $rs->fetch_assoc())) { ?>
						<tr>
 							<td class="text-center"><?php echo ConvertToThaiDate($row["createdate"], true); ?></td>
							<td class="text-center"><?php echo $row["member_no"]; ?><font color="white">'</font></td>
							<td class=""><?php echo $row["memname"]; ?></td>
							<td class="text-center"><?php echo $types[$row["type"]]; ?></td>
							<td class=""><?php echo htmlspecialchars($row["value1"]) ?><font color="white">'</font></td>
 							<td class="">
								<?php if($row["status"] != 1){ ?>
								รอดำเนินการ
								<?php }else{ ?>
								ดำเนินการแล้ว
								<?php } ?>
							</td>
						</tr>
					<?php
						}
 					?>
					</tbody>
				</table>



</body>
</html></pre>
