<?php
	//error_reporting(0);
	//ini_set('display_errors', 0);
	$check_http = (@$_SERVER["HTTPS"] ? "https://" : "http://");
	define("HTTP",$check_http);
	
	define("PATH",$_SERVER["DOCUMENT_ROOT"]) ;
	define("BASEPATH",$check_http.$_SERVER["HTTP_HOST"]."/loan") ;
	
	function curPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}
 	
	define("TITLE"," ") ;
	define("KEYWORD","") ;
	define("DESC","") ;
	
	define("THEME","admin_loan");
	
	
	require PATH . "/class/connect.inc.php" ;
	require PATH . "/class/function.inc.php" ;
	require PATH . "/class/smarty.inc.php";
 	require PATH . "/class/messages.class.php"  ;
	require PATH . "/class/pagination.class.php"  ;
	require PATH . "/class/logging.class.php"  ;
	require PATH . "/class/permission.class.php"  ;
	require PATH . "/class/function.loan.inc.php" ;
	
	$admin_id = (int) !empty($_SESSION["ADMIN_ID"]) ? $_SESSION["ADMIN_ID"] : 0   ; 
	
	$msg = new Messages();
	$cmplog = new CmpActionLogging();
	$cmpper = new CmpPermission();
 
	function get_breadcrumb($data = array()){
		$nav = "" ; 
		$nav .= "<div class=\"row\">" ; 
        $nav .= "<div class=\"col-md-12\">" ; 
		$nav .= "<!--breadcrumbs start -->" ; 
		$nav .= "<ul class=\"breadcrumb\">" ; 
		
		$nav .= "<li><a href=\"#\"><i class=\"fa fa-home\"></i> หน้าแรก </a></li>" ; 
		foreach($data as $key => $value){
			$nav .= "<li><a href=\"{$value}\">{$key}</a></li> " ;  
		}
		$nav .= "</ul>" ;  
		$nav .= "</div>" ; 
		$nav .= "</div>" ; 
                     
        return $nav ;
	}
	
	$base_url = "/admin/" ; 
	$smarty->assign("base_url" , $base_url) ; 
	
	$sql = "SELECT * FROM cmp_website WHERE st_id = 1  " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	$website = $rs->fetch_assoc();
	
	$smarty->assign("website" , $website) ;
	
	$sql_coop_profile = "SELECT coop_name_th,coop_name_en FROM coop_profile LIMIT 1";
	$rs_coop_profile = $mysqli_app->query($sql_coop_profile);
	$row_coop_profile = $rs_coop_profile->fetch_assoc();	
	define("COOP_NAME_TH",$row_coop_profile['coop_name_th']);
	define("COOP_NAME_EN",$row_coop_profile['coop_name_en']);
	
	$smarty->assign("HTTP" , $check_http) ;
	
	
	
	
	