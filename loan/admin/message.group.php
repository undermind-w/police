<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 	
	$cmpper->check("message") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
		if(@in_array($_POST["in"] , array("group")) ){
			 
			$group_id = (int) @$_POST["group_id"] ; 
			$group_title = @$mysqli->real_escape_string(@$_POST["group_title"]) ;
			$group_member = @$mysqli->real_escape_string(@$_POST["group_member"]) ;
			$group_status = (int) @$_POST["group_status"] ; 
		 
			if(empty($group_id))
			{
				$sql = "INSERT INTO cmp_message_group 
												( 
												group_id 
												, group_title
												, group_member 
												, createdate
												, updatedate
												, group_status 
												) 
												VALUES  
												(
												'{$group_id}'
												, '{$group_title}'
												, '{$group_member}'
												, NOW() 
												, NOW() 
												, {$group_status} ) " ;
				$mysqli->query($sql);
				echo $mysqli->error ;
				$group_id =  (int) $mysqli->insert_id ; 
				
				$cmplog->add_log("groupmessage-edit" , "เพิ่มกลุ่มส่งข้อความ  " , $group_id ) ;
				$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
				 
				
			}
			else
			{	
				$sql = "UPDATE cmp_message_group SET group_title = '{$group_title}' 
													, group_member = '{$group_member}'  
													, updatedate = NOW() 
													, group_status = {$group_status}
													WHERE 
													group_id = {$group_id} LIMIT 1  " ;
				$mysqli->query($sql);
				echo $mysqli->error ;						
				$cmplog->add_log("groupmessage-edit" , "แก้ไขกลุ่มส่งข้อความ" , $group_id ) ;
				$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			}
			
			echo "<script> window.location.href = \"?\"</script>" ;
			exit();
		}elseif(@in_array($_POST["in"] , array("member"))){
			
			$member_no =@$mysqli->real_escape_string(@$_POST["member_no"]) ; 
			$group_id = @$mysqli->real_escape_string(@$_POST["group_id"]) ;
			
			$sql = "SELECT * FROM cmp_imp_member WHERE member_no = '{$member_no}' LIMIT 0 , 1  " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ; 
			$mem = $rs->fetch_assoc() ; 
 			
			if(empty($mem)){
				echo "<script> alert( 'ไม่มี ID ในระบบ ' ) ;window.location.href = \"?in=group&do=member&gid={$group_id}\"</script>" ;
				exit();
			}
			
			
			$sql = "SELECT id FROM cmp_message_group_member WHERE group_id = '{$group_id}' AND member_no = '{$member_no}' LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ; 
			$tmp = $rs->fetch_assoc() ; 
			
			if(!empty($tmp)){
				
				echo "<script> alert( 'ไม่มี ID นี้แล้วในกลุ่ม' ) ;window.location.href = \"?in=group&do=member&gid={$group_id}\"</script>" ;
				exit();
			}
			
			
 			if(empty($tmp))
			{
				$sql = "INSERT INTO cmp_message_group_member 
												( 
													group_id 
													, member_no
													, createdate
 												) 
												VALUES  
												(
													'{$group_id}'
													, '{$member_no}'
													, NOW() 
												) " ;
				$mysqli->query($sql);
				echo $mysqli->error ;
				$id =  (int) $mysqli->insert_id ; 
				
				$cmplog->add_log("membergropmessage-edit" , "เพิ่มกลุ่มส่งข้อความ  " , $group_id ) ;
				$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
				 
				
			} 
			
			echo "<script> window.location.href = \"?in=group&do=member&gid={$group_id}\"</script>" ;
			exit();
			
		}
	} 
	
	if( @in_array($_GET["do"] , array("delete")) && @in_array($_GET["in"] , array("group"))  ){
		$group_id = (int) @$_GET["gid"] ;  
 		$sql = "DELETE FROM cmp_message_group WHERE group_id = {$group_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;	
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("groupmessage-delete" , "ลบส่งกลุ่มส่งข้อความ" , $group_id ) ;
 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	if( @in_array($_GET["do"] , array("delete")) && @in_array($_GET["in"] , array("member"))  ){
		
		$group_id = (int) @$_GET["gid"] ;  
		$id = (int) @$_GET["id"] ;  
		
		
 		$sql = "DELETE FROM cmp_message_group_member WHERE id = {$id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;	
		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("membergropmessage-delete" , "ลบส่งกลุ่มส่งข้อความ" , $group_id ) ;
 
		echo "<script> window.location.href = \"?in=group&do=member&gid={$group_id}\"</script>" ;
		exit();
	}

		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
	<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
	<script>
		$(document).ready(function() {
			$('.date-picker').datepicker({
				orientation: "top auto" ,
				autoclose: true , 
				format : "yyyy-mm-dd"
			});
			 
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	if(@in_array($_GET["do"] , array("member") )){ 
	
		$group_id = (int) @$_GET["gid"]  ; 
 		$page = (int) @$_GET["page"]  ; 
		$smarty->assign("title", "จัดการสมาชิกในกลุ่ม" );
		$smarty->assign("page_title", "จัดการสมาชิกในกลุ่ม" );
		$smarty->assign("breadcrumb", array("จัดการกลุ่มส่งข้อความ" => "#" , "จัดการสมาชิกในกลุ่ม" => "#" ) );
		
		ob_start();
		
		$sql = "SELECT COUNT(id) as _c FROM cmp_message_group_member WHERE group_id = '{$group_id}' " ;
		$rs = $mysqli->query($sql);
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 50 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT cmp_message_group_member.id 
								, cmp_message_group_member.member_no 
								, cmp_imp_member.memname 
							FROM cmp_message_group_member  
								LEFT JOIN cmp_imp_member ON cmp_message_group_member.member_no = cmp_imp_member.member_no
							WHERE cmp_message_group_member.group_id = '{$group_id}'
							ORDER BY cmp_message_group_member.createdate 
							DESC LIMIT {$page_start} , {$per_page} " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		  
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">จัดการสมาชิกในกลุ่ม</h4>
		</div>
		<div class="panel-body">
			<?php echo $msg->display() ;  ?>
 			<div style="clear: both;margin-bottom:15px"></div>
			<form action="?" class="form-inline" name="" style="margin-bottom:20px;" method="post" >
				<input type="hidden" name="in" value="member"/>
				<input type="hidden" name="do" value="add"/>
				<input type="hidden" name="group_id" value="<?php echo $group_id ?>"/>
 				<label >กรุณากรอกรหัสสมาชิกเพื่อเพื่มสมาชิก</label>
				<input type="text" class="form-control" id="member_no" name="member_no" value="" /> 
				
			</form>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="100" style="text-align:center" >ลำดับ</th>
						<th width="200" style="text-align:center" >รหัส</th>
                        <th style="text-align:center" >ชื่อ</th>
                        <th width="150" style="text-align:center" >จัดการ</th>
						
					</tr>
				</thead>
				<tbody>
					<?php 
					
					while($row = $rs->fetch_assoc()){
						
					?>
					 <tr>
						<td style="text-align:center" ><?php echo $i ++  ?></td>
						<td><?php echo @$row["member_no"] ?></td>
                        <td><?php echo @$row["memname"] ?></td>
						<td style="text-align:center" >
 							<a href="?in=member&do=delete&gid=<?php echo @$group_id ?>&id=<?php echo $row["id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " title="ลบข้อมูล"  ><i class="fa fa-trash-o"></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
	
		
	
 	}
	elseif(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			
			$group_id = (int) $_GET["gid"] ; 
			$sql = "SELECT * FROM cmp_message_group WHERE group_id = {$group_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "จัดการกลุ่มส่งข้อความ" );
		$smarty->assign("page_title", "จัดการกลุ่มส่งข้อความ" );
 		$smarty->assign("breadcrumb", array("จัดการกลุ่มส่งข้อความ" => "?" ,  "{$btitle}" => "#"  ) );

		$group_id = (int) @$row["group_id"] ; 
  
 		$group_status = (int) @$row["group_status"]  ;  
		
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">จัดการกลุ่มส่งข้อความ</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="group_id" value="<?php echo $group_id ;  ?>"/>
						<input type="hidden" name="in" value="group"/>
						<input type="hidden" name="do" value="add"/>
  						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">ชื่อกลุ่ม</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="group_title" name="group_title" placeholder="" value="<?php echo htmlspecialchars($row["group_title"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">รหัสสมาชิก (ใส่เลขสมาชิกตามด้วย ,)</label>
							<div class="col-sm-9">
								<textarea class="form-control" id="group_member" name="group_member" rows="5" ><?php echo htmlspecialchars($row["group_member"]) ?></textarea>
 							</div>
						</div>
						 
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="group_status" name="group_status" <?php echo $group_status == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1"> แสดงข้อมูล
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		$smarty->assign("title", "จัดการกลุ่มส่งข้อความ" );
		$smarty->assign("page_title", "จัดการกลุ่มส่งข้อความ" );
		$smarty->assign("breadcrumb", array("จัดการกลุ่มส่งข้อความ" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(group_id) as _c FROM cmp_message_group " ;
		$rs = $mysqli->query($sql);
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_message_group  ORDER BY createdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">จัดการกลุ่มส่งข้อความ</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="100" style="text-align:center" >ลำดับ</th>
                        <th style="text-align:center" >ชื่อกลุ่ม</th>
						
                        <th width="150" style="text-align:center" >จัดการ</th>
						
					</tr>
				</thead>
				<tbody>
					<?php 
					
					while($row = $rs->fetch_assoc()){
						
					?>
					 <tr>
						<td style="text-align:center" ><?php echo $i ++  ?></td>
                        <td><?php echo @$row["group_title"] ?></td>
						
						<td style="text-align:center" >
							<!-- 
							<a href="?in=group&do=member&gid=<?php echo @$row["group_id"] ?>" title="จัดการสมาชิก" ><i class="fa fa-users"></i></a> | 
							-->
							<a href="?in=group&do=edit&gid=<?php echo @$row["group_id"] ?>" title="แก้ไขข้อมูล" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=group&do=delete&gid=<?php echo @$row["group_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " title="ลบข้อมูล"  ><i class="fa fa-trash-o"></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			 <div style="clear: both;margin-top:15px"></div>
			 <a class="btn btn-info pull-left" href="message2.php"  >กลับ</a>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 