<?php
session_start();
// header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;
require "menu.inc.php" ;

// $cmpper->check("message") ;

 if (in_array($_GET['do'], array('add')) && isset($_POST['department_name'])) {
	$department_name = $mysqli->real_escape_string($_POST['department_name']);
	$department_head = $mysqli->real_escape_string($_POST['department_head']);
	$department_head_tel = $mysqli->real_escape_string($_POST['department_head_tel']);

	$sql = "INSERT INTO cmp_loan_department (department_name, department_head, department_head_tel) VALUES ( '$department_name', '$department_head', '$department_head_tel')";

	if (!$mysqli->query($sql)) {
		header("Location: loan_department.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
		exit();
	}
	// header("Location: loan_department.php?err=เกิดข้อผิดพลาดในการบันทึกข้อมูล");
	header("Location: loan_department.php?res=บันทึกข้อมูลแล้ว");
	exit();

}
else if (in_array($_GET['do'], array('edit')) && isset($_POST['department_id'])) {
	$department_id = $mysqli->real_escape_string($_POST['department_id']);
	$department_name = $mysqli->real_escape_string($_POST['department_name']);
	$department_head = $mysqli->real_escape_string($_POST['department_head']);
	$department_head_tel = $mysqli->real_escape_string($_POST['department_head_tel']);

	// update cmp_loan_department
		$sql = "UPDATE cmp_loan_department
				SET department_name = '$department_name', department_head = '$department_head', department_head_tel = '$department_head_tel'
				WHERE department_id = $department_id";
		if (!$mysqli->query($sql)) {
			header('Location: loan_department.php?err=เกิดข้อผิดพลาดในการแก้ไขข้อมูล');
			exit();
		}


	header('Location: loan_department.php?res=แก้ไขข้อมูลแล้ว');
	exit();

}
else if (in_array($_GET['do'], array('del'))) {
	$department_id = $mysqli->real_escape_string($_GET['department_id']);
	
	$sql = "DELETE FROM cmp_loan_department WHERE department_id = '$department_id'";
	$result = $mysqli->query($sql);

	if (!$mysqli->query($sql) && !$mysqli->query($sql2)) {
		header('Location: loan_department.php?err=เกิดข้อผิดพลาดในการลบข้อมูล');
		exit();
	}

	header('Location: loan_department.php?res=ลบข้อมูลแล้ว');
	exit();

} else {

ob_start();
?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/validation/js/formValidation.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<script src="/scripts/validation/js/bootstrap.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script>
$(document).ready(function() {

	$('#loan-form').formValidation({
		fields: {
			department_name: {
				validators: {
					notEmpty	: { message: '<p>กรุณากรอกชื่อหน่วย</p>' }
				}
			},
			department_head: {
				validators: {
					notEmpty	: { message: '<p>กรุณากรอกชื่อหัวหน้าหน่วย</p>' },
					regexp	: { message: '<p>กรุณาป้อนตัวอักษรเท่านั้น</p>', regexp: /[a-zก-ฮ\W]$/ }
				}
			},
			department_head_tel: {
				validators: {
					notEmpty	: { message: '<p>กรุณาป้อนเบอร์โทรศัพท์</p>' },
					regexp	: { message: '<p>กรุณาป้อนเบอร์โทรศัพท์</p>', regexp: /^0[8|9].[0-9]{7}$/ }
				}
			}
		}
	}).on('status.field.fv', function(e, data) {
		if (data.field === 'department_head_tel') {
			(data.status === 'VALID' && $('#department_id').val() !== '')
			    ? $('#btn-sms').removeAttr('disabled')
			    : $('#btn-sms').prop('disabled', 'disabled')
		}
	});

});
</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$page = (int) @$_GET["page"]  ;
	$smarty->assign("title", "หน่วยบริการ" );
	// $smarty->assign("page_title", "หน่วยบริการ" );

	ob_start();
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">หน่วยบริการ</h4>
			</div>
			<div class="panel-body">
				<?php	if (in_array($_GET['do'], array('add', 'edit'))) {
					if ($_GET['do'] == 'add') {
						$smarty->assign("breadcrumb", array("หน่วยบริการ" => "loan_department.php", "เพิ่มข้อมูล" => "#"));
					} else {
						$smarty->assign("breadcrumb", array("หน่วยบริการ" => "loan_department.php", "แก้ไขข้อมูล" => "#"));

						$sql = "SELECT * FROM cmp_loan_department WHERE department_id = {$_GET['department_id']}";
						$result = $mysqli->query($sql);
						$user = $result->fetch_object();
					}
				?>
					<form action="" class="form-horizontal" id="loan-form" method="post" enctype="multipart/form-data">
						<input type="hidden" name="department_id" id="department_id" value="<?= (isset($user->department_id)) ? $user->department_id : '' ?>">
						<div class="form-group">
							<label for="department_name" class="col-sm-3 control-label">ชื่อหน่วย</label>
							<div class="col-sm-4">
								<input type="text" id="department_name" class="form-control" name="department_name" placeholder="ชื่อหน่วย"
									value="<?= (isset($user->department_name)) ? $user->department_name : '' ?>" autofocus>
							</div>
						</div>

						<div class="form-group">
							<label for="department_head" class="col-sm-3 control-label">ชื่อหัวหน้าหน่วย</label>
							<div class="col-sm-4">
								<input type="text" id="department_head" class="form-control" name="department_head"
									value="<?= (isset($user->department_head)) ? $user->department_head : '' ?>" placeholder="ชื่อหัวหน้าหน่วย">
							</div>
						</div>

						<div class="form-group">
							<label for="department_head_tel" class="col-sm-3 control-label">มือถือ</label>
							<div class="col-sm-4">
								<input type="text" id="department_head_tel" class="form-control" name="department_head_tel" placeholder="มือถือ"
									value="<?= (isset($user->department_head_tel)) ? $user->department_head_tel : '' ?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-9 control-label">
								<p class="text-center">
									<button type="submit" class="btn btn-success2">บันทึก</button>
									<a href="?" class="btn btn-success2">กลับ</button></a>
								</p>
								<p class="blgres text-center"></p>
							</div>
						</div>
					</form>
				<?php
					} else {
						$smarty->assign("breadcrumb", array("หน่วยบริการ" => "?") );

						$sql = "SELECT * FROM cmp_loan_department";
						$rs = $mysqli->query($sql);
						$num_rows = $rs->num_rows;
						$per_page = 10 ;
						$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

						$pagination = (new Pagination());

						$pagination->setCurrent($page);
						$pagination->setRPP($per_page);
						$pagination->setTotal($num_rows);
						$pagination->setCrumbs(25);
						$paging = $pagination->parse();

						$page_start = (($per_page * $page) - $per_page);
						$sql = "SELECT * FROM cmp_loan_department
								LIMIT {$page_start} , {$per_page}";

						$rs = $mysqli->query($sql);
						echo $mysqli->error;

						$num_rows -= $page_start;
						$page_start++;
						?>

						<a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
						<?php if (isset($_GET['err'])) { ?>
							<div class="alert alert-danger resalt">
								<p>ERROR : <?= $_GET['err'] ?></p>
							</div>
						<?php } elseif (isset($_GET['res'])) { ?>
							<div class="alert alert-success resalt">
								<p>! <?= $_GET['res'] ?></p>
							</div>
						<?php } ?>
						<div style="clear: both;margin-bottom:15px"></div>
						<table class="table">
							<thead>
								<tr>
									<th class="text-center">ลำดับ</th>
									<th class="text-left">หน่วยบริการ</th>
									<th width="200" class="text-left">ชื่อหัวหน้าหน่วย</th>
									<th class="text-center">เบอร์</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php while(($row = $rs->fetch_assoc())) { ?>
									<tr>
										<td class="text-center"><?= $page_start++ ?></td>
										<td class="text-left">
										<?php echo $row["department_name"]; ?>
										</td>
										<td class="text-left">
										<?php echo $row["department_head"]; ?>
										</td>
										<td class="text-center">
										<?php echo $row["department_head_tel"]; ?>
										</td>
										<td class="text-center">
											<a href="?do=edit&department_id=<?= $row['department_id'] ?>" title="แก้ไขข้อมูล"><i class="fa fa-edit"></i></a> |
											<a href="?do=del&department_id=<?= $row['department_id'] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้');" title="ลบข้อมูล"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
								<?php
									}
									unset($sql, $rs, $row);
								?>
							</tbody>
						</table>
					<?php echo $paging;  ?>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();
}

$smarty->assign("content", $content);
$smarty->display(THEME.".tpl");
