<?php
	session_start();
	require "config.inc.php" ;
 	require "session.inc.php" ;
	require "menu.inc.php" ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year = date("Y")>2500?date("Y")-543:date("Y");

	function U2T($text) { return mb_convert_encoding(trim($text), "TIS-620", "auto"); }
	function T2U($text) { return mb_convert_encoding(trim($text), "UTF-8", "auto"); }
	function ENC_T2U($txt) { return iconv("tis-620", "utf-8", $txt); }

	function process_address($file){
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,memb_addr,addr_group,soi,mooban,road,tambol,district_desc,province_desc,postcode,mem_tel,memb_addrc,addr_groupc,soic,moobanc,roadc,tambolc,district_descc,province_descc,postcodec,phone,mem_telmobile";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_address ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	/*
	function process_ag($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "member_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 4; $i++) {
									if($i == 1)	$tmp[] = "'".ConvertToSQLDate(T2U($row[$i]))."'";
									elseif($i == 2)	$tmp[] = (double)$row[$i];
									else	$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_ag(member_no, mutuallydtm, assit_amt, mutsts) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	function process_dept_master($file){
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,deptaccount_no,deptaccount_name,depttype_desc,prncbal,deptmthamt,deptclose_status";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_dept_master ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	function process_dept_statement($file) {
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "deptaccount_no,seq_no,oprdtm,deptitemtype_desc,deptamt,withamt,prncbal";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_dept_statement($fields) VALUES('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	function process_divavg($file){
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,account_year,div_amt,avg_amt,ins_ssak_amt,ins_sschsa_amt,ins_amt,lon_amt,et4_amt,loan_bal,divavg_amt";
					
					$data[10] = $data[2] + $data[3];
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_divavg ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
	    	fclose($handle);
			unset($fields);
		}
	}

	function process_keep_from($file) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no', 'ref_membno', 'recv_period');

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_keep_from ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
	    	fclose($handle);
			unset($fields);
		}
	}

	/*
	function process_insurance1($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "member_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 3; $i++) {
									if($i == 2)													$tmp[] = "'".ConvertToSQLDate(T2U($row[$i]))."'";
									else																$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_insurance1(member_no, insdesc, insdtm) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	function process_insurance2($data) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no', 'ins_no', 'insdtm');

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_insurance2 ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
	    	fclose($handle);
			unset($fields);
		}
	}

	function process_keep($file) {
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,recv_period,operdtm,receipt_no,keepitemtype_code,keepdesc,period,principal_payment,interest_payment,item_payment,itembal,loancontract_no,seq,keeptype";

					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_keep ($fields) VALUES('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	/*function process_keepdep($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// reformat date
					list($m,$d,$y) = explode('/', $data[2]);
					$data[2] = "$y-$m-$d";

					// member_no,	recv_period,	operdtm,		receipt_no,		account_no_1,	deposit_amt_1,		account_no_2,		deposit_amt_2,		account_no_3,	deposit_amt_3,	account_no_4,	deposit_amt_4,	account_no_5,	deposit_amt_5,	apply_fee
					// 04425,		201512,			12/25/2015,		590101551,		0202232,		2000,				1202736,			1000,				1202737,		1000,			,				0,				,				0,				0

					// $data = array();
					$detail = array_slice($data, 0, 4);
					$index = 4;
					$accounts = array();
					while($index < 10){
						array_push($accounts, array_slice($data, $index, 2));
						$index += 2;
					}

					foreach($accounts as $k => $account){
						if($account[1] > 0){
							$data = array_merge($detail, $account);

							// convert to utf-8
							foreach($data as $i => $row){
								$data[$i] = iconv("tis-620", "utf-8", $row);
							}

							// lists fields
							$fields = array('member_no','recv_period','operdtm','receipt_no','keepitemtype_code','item_payment');

							// insert new data
							$fields = implode(",", $fields);
							$value = implode("','", $data);
							$sql = "INSERT INTO cmp_imp_keepdep ($fields) VALUES('{$value}')";
							$mysqli->query($sql);
							unset($where, $value, $sql);
						}
					}

					// // convert to utf-8
					// foreach($data as $i => $row){
					// 	$data[$i] = iconv("tis-620", "utf-8", $row);
					// }
					//
					// // lists fields
					// $fields = array('member_no','recv_period','operdtm','receipt_no','keepitemtype_code',
					// 		'keepdesc','period','principal_payment','interest_payment','item_payment','itembal');
					//
					// // insert new data
					// $fields = implode(",", $fields);
					// $value = implode("','", $data);
					// $sql = "INSERT INTO cmp_imp_keepdep ($fields) VALUES('{$value}')";
					// $mysqli->query($sql);
					// unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}*/

	function process_keepint($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','interest_accum','sharestk_value');

					// insert new data
					$fields = implode(",", $fields);
					// $value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_keepint ($fields) VALUES ('{$data['0']}', '{$data['3']}', '{$data['4']}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_keepshare($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// delete duplicate data
					$fields = array('member_no','recv_period','sharestk_value');
					$where = array();

					/*foreach($fields as $i => $index){
						// convert to utf-8
						$data[$i] = iconv("tis-620", "utf-8", $data[$i]);
						// add conditions
						array_push($where, "{$index} = '{$data[$i]}'");
					}
					$where = implode(" AND ", $where);
					$sql = "DELETE FROM cmp_imp_keepshare WHERE {$where}";
					$mysqli->query($sql);*/

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_keepshare ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	function process_loan_coll($file) {
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,loancontract_no,seq_no,ref_collno,colltype_code,description,collactive_amt";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_loan_coll ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_loan_master($file){
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,lastdtm,loancontract_no,startdtm,loanapprove_amt,period_payamt,period_payment,principal_balance,loansts";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_loan_master ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_loan_statement($file) {
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,loancontract_no,seq_no,operdtm,loanitemtype_code,period,principal_payment,interest_payment,principal_balance,ref_docno";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_loan_statement ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	function process_member($file) {
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,memname,membgroup_code,membgroup_desc,membtype_desc,birthdtm,card_person,position_desc,salary_amount,incomeetc_amount,memdtm";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_member ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_member_gain($file) {
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,gain_name,seq_no";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_member_gain ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_share_master($file) {
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$fields = "member_no,lastdtm,sharebegin_amt,periodshareval,last_period,sharestkval,paymentsts";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_share_master ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
				$i++;
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_share_statement($file){
		global $mysqli;
		$i = 0;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if($i++ > 0){
					// lists fields
					$field = "member_no,seq_no,operdtm,shritemtype_desc,period,shrval,shrbalval,ref_docno";
					
					// insert new data
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_share_statement($field) VALUES('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
	        }
	        fclose($handle);
			unset($field);
	    }
	}

	/*
	function process_wf($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "member_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 3; $i++) {
									if(in_array($i, array(2)))					$tmp[] = "'".ConvertToSQLDate(T2U($row[$i]))."'";
									else																$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_wf(member_no, deptaccount_no, deptdtm) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	/*
	function process_wfkeep($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "member_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 8; $i++) {
									if(in_array($i, array(5, 6, 7)))			$tmp[] = (float)$row[$i];
									elseif(in_array($i, array(3)))				$tmp[] = "'".ConvertToSQLDate(T2U($row[$i]))."'";
									else																	$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_wfkeep(member_no, wfmember_no, wfaccount_name, operdtm, receipt_no, carcass_amt, fee_year, fee_app) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	/*
	function process_wfkeepname($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "deptaccount_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 4; $i++) {
									$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_wfkeepname(deptaccount_no, prename_desc, deptaccount_name, deptaccount_sname) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;
	if($do == "process-check"){
			if(file_exists(PATH."/uploads/tmp/import_process.inc.php")){
					$data = unserialize(file_get_contents(PATH."/uploads/tmp/import_process.inc.php"));
					$process = (int)$data["process"];
					$total = (int)$data["total"];
					echo "<i class='fa fa-spin fa-refresh'></i>&nbsp;ประมวลผล {$process} / {$total}";
			} else echo "<i class='fa fa-spin fa-refresh'></i>&nbsp;ประมวลผลไฟล์";
			exit();
	}

	if($do == "process"){
			$tmp = pathinfo($_POST["fname"]);
			$ext = $tmp['extension'];
			if ($ext == 'csv') {
				$file = "{$_SERVER["DOCUMENT_ROOT"]}/uploads/tmp/{$_POST["fname"]}";

				switch($_POST["ref"]) {
						case "address" 			: process_address($file); 				break;
						case "dept_master" 		: process_dept_master($file); 			break;
						case "dept_statement" 	: process_dept_statement($file); 		break;
						case "divavg" 			: process_divavg($file); 				break;
						case "keep_from" 		: process_keep_from($file); 			break;
						case "keep" 			: process_keep($file); 					break;
						case "keepdep" 			: process_keepdep($file); 				break;
						case "keepint" 			: process_keepint($file); 				break;
						case "keepshare" 		: process_keepshare($file);				break;
						case "loan_coll" 		: process_loan_coll($file); 			break;
						case "loan_master" 		: process_loan_master($file); 			break;
						case "loan_statement" 	: process_loan_statement($file); 		break;
						case "member" 			: process_member($file); 				break;
						case "member_gain" 			: process_member_gain($file); 				break;
						case "share_master" 	: process_share_master($file); 			break;
						case "share_statement" 	: process_share_statement($file); 		break;
						case "wf" 				: process_wf($file); 					break;
						default : break;
				}
				// unset($data);
			}

			$id = genid();
			$sql = "INSERT INTO cmp_imp_stat(stat_id, admin_id, import_file , update_time, update_ip) VALUES('{$id}', {$_SESSION["ADMIN_ID"]}, '{$_POST["ref"]}' , NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
			$mysqli->query($sql);
			exit();
	}

	if($do == "unlink") {
			if($_POST["fname"]) @unlink($_SERVER["DOCUMENT_ROOT"]."/uploads/tmp/{$_POST["fname"]}");
			exit();
	}

	if($do == "record_count") {
			$sql = "SELECT COUNT(*) AS _c FROM cmp_imp_{$_POST["ref"]}";
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc();
			$c = number_format($row["_c"], 0);
			echo "ข้อมูลปัจจุบัน {$c} Rec.";
			exit();
	}

	if($do == "truncate") {
			$mysqli->query("TRUNCATE TABLE cmp_imp_{$_POST["ref"]}");
			exit();
	}

	if($do == "delete_period") {
		$ref = $_POST["ref"];
		$table = "cmp_imp_{$_POST["ref"]}";
		if($table == "cmp_imp_dept_statement") {
			$sql = "SELECT YEAR(oprdtm) AS yy, MONTH(oprdtm) AS mm, COUNT(oprdtm) AS _count
								FROM `cmp_imp_dept_statement`
								WHERE oprdtm <> '0000-00-00'
								GROUP BY YEAR(oprdtm) , MONTH(oprdtm)
								ORDER BY YEAR(oprdtm) DESC, MONTH(oprdtm) DESC";
		} elseif($table == "cmp_imp_keep") {
			$sql = "SELECT YEAR(`operdtm`) AS yy, MONTH(`operdtm`) AS mm, COUNT(`operdtm`) AS _count
								FROM `cmp_imp_keep`
								WHERE operdtm <> '0000-00-00'
								GROUP BY YEAR(`operdtm`) , MONTH(`operdtm`)
								ORDER BY YEAR(`operdtm`) DESC, MONTH(`operdtm`) DESC";
		} elseif($table == "cmp_imp_loan_statement") {
			$sql = "SELECT YEAR(`operdtm`) AS yy, MONTH(`operdtm`) AS mm, COUNT(`operdtm`) AS _count
								FROM `cmp_imp_loan_statement`
								WHERE operdtm <> '0000-00-00'
								GROUP BY YEAR(`operdtm`) , MONTH(`operdtm`)
								ORDER BY YEAR(`operdtm`) DESC, MONTH(`operdtm`) DESC";
		} elseif($table == "cmp_imp_share_statement") {
			$sql = "SELECT YEAR(`operdtm`) AS yy, MONTH(`operdtm`) AS mm, COUNT(`operdtm`) AS _count
								FROM `cmp_imp_share_statement`
								WHERE operdtm <> '0000-00-00'
								GROUP BY YEAR(`operdtm`) , MONTH(`operdtm`)
								ORDER BY YEAR(`operdtm`) DESC, MONTH(`operdtm`) DESC";
		} elseif($table == "cmp_imp_keepshare") {
			$sql = "SELECT SUBSTRING(recv_period, 1, 4) AS yy, SUBSTRING(recv_period, 5, 2) AS mm, COUNT(`recv_period`) AS _count
								FROM `cmp_imp_keepshare`
								WHERE recv_period <> ''
								GROUP BY SUBSTRING(recv_period, 1, 4), SUBSTRING(recv_period, 5, 2)
								ORDER BY SUBSTRING(recv_period, 1, 4) DESC, SUBSTRING(recv_period, 5, 2) DESC";
		} elseif($table == "cmp_imp_keep_from") {
			$sql = "SELECT SUBSTRING(recv_period, 1, 4) AS yy, SUBSTRING(recv_period, 5, 2) AS mm, COUNT(`recv_period`) AS _count
								FROM `cmp_imp_keep_from`
								WHERE recv_period <> ''
								GROUP BY SUBSTRING(recv_period, 1, 4), SUBSTRING(recv_period, 5, 2)
								ORDER BY SUBSTRING(recv_period, 1, 4) DESC, SUBSTRING(recv_period, 5, 2) DESC";
		} elseif($table == "cmp_imp_keepint") {
			$sql = "SELECT SUBSTRING(recv_period, 1, 4) AS yy, SUBSTRING(recv_period, 5, 2) AS mm, COUNT(`recv_period`) AS _count
								FROM `cmp_imp_keepint`
								WHERE recv_period <> ''
								GROUP BY SUBSTRING(recv_period, 1, 4), SUBSTRING(recv_period, 5, 2)
								ORDER BY SUBSTRING(recv_period, 1, 4) DESC, SUBSTRING(recv_period, 5, 2) DESC";
		}
		
		if(isset($sql)) {
			$rs = $mysqli->query($sql);
			echo "<div class='form-horizontal'>";
			while(($row = $rs->fetch_assoc())) {
				$month_name = MonthName((int)$row["mm"]);
				if($table != "cmp_imp_keepshare") {
					$year_th = $row['yy'] + 543 ;
				} else {
					$year_th = $row['yy'];
				}
				$record = number_format($row["_count"], 0);
				echo "
								<div class='form-group'>
									<label class='col-xs-9 control-label text-left'>
										<span class='pull-left text-left' style='width: 150px;'>เดือน {$month_name} {$year_th}</span>
										<span class='pull-left text-left'>จำนวน {$record} Rec.</span>
										<div class='clearfix'></div>
									</label>
									<div class='col-xs-3'>
										<button class='btn btn-danger btn-block btn-period' data-table='{$table}' data-yy='{$row["yy"]}' data-mm='{$row["mm"]}'>ลบข้อมูล</button>
									</div>
								</div>
								<div class='clearfix'></div>
							";
			}
			echo "</div";
		}
		exit();
	}

	if($do == "delete_period_confirm") {
		$table = $_POST["table"];
		$yy = (int)$_POST["yy"];
		$mm = (int)$_POST["mm"];
		if($table == "cmp_imp_dept_statement") {
			$sql = "DELETE FROM `cmp_imp_dept_statement`
								WHERE YEAR(oprdtm) = {$yy}
									AND  MONTH(oprdtm) = {$mm}";
		} elseif($table == "cmp_imp_keep") {
			$sql = "DELETE FROM `cmp_imp_keep`
								WHERE YEAR(`operdtm`) = {$yy}
									AND MONTH(`operdtm`) = {$mm}";
		} elseif($table == "cmp_imp_loan_statement") {
			$sql = "DELETE FROM `cmp_imp_loan_statement`
								WHERE YEAR(`operdtm`) = {$yy}
									AND MONTH(`operdtm`) = {$mm}";
		} elseif($table == "cmp_imp_share_statement") {
			$sql = "DELETE FROM `cmp_imp_share_statement`
								WHERE YEAR(`operdtm`) = {$yy}
									AND MONTH(`operdtm`) = {$mm}";
		} elseif($table == "cmp_imp_keepshare") {
			$mm = sprintf('%02d', $mm);
			$sql = "DELETE FROM `cmp_imp_keepshare`
								WHERE recv_period = '{$yy}{$mm}'";
		} elseif($table == "cmp_imp_keep_from") {
			$mm = sprintf('%02d', $mm);
			$sql = "DELETE FROM `cmp_imp_keep_from`
								WHERE recv_period = '{$yy}{$mm}'";
		} elseif($table == "cmp_imp_keepint") {
			$mm = sprintf('%02d', $mm);
			$sql = "DELETE FROM `cmp_imp_keepint`
								WHERE recv_period = '{$yy}{$mm}'";
		}
		$mysqli->query($sql);
		exit();
	}
	
	ob_start();
?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.modal-header { height: auto; }
	.modal-title > h3, .modal-body h3 { margin-top: 0; }
</style>
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/blueimp.fileupload/9.9.3/js/vendor/jquery.ui.widget.js"></script>
<script src="/scripts/blueimp.fileupload/9.9.3/js/jquery.iframe-transport.js"></script>
<script src="/scripts/blueimp.fileupload/9.9.3/js/jquery.fileupload.js"></script>
<script>
		$(document).ready(function(){
	/*
	 *	@ File upload
	 *****************************************************/
				var url = "import_excel.upload.php";
				var obj;

				$('#fileupload').fileupload({
						url: url,
						dataType: 'json',
						done: function (e, data) {
								//obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;กำลังประมวลผลไฟล์");
								$.each(data.result.files, function (index, file) {
									//console.log(file.name);
									process_file(file.name, obj.attr("ref"));

								});
								//obj.parents(".form-group").find(".help-block").html("");
								//$(".btn-import").removeAttr("disabled");
								//obj = null;
						},
						progressall: function (e, data) {
								$(".help-block").html("");
								$(".btn-import").attr("disabled", "disabled");
								var progress = parseInt(data.loaded / data.total * 100, 10);
								obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;อัพโหลด " + progress + "%");
						}
				}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');


	/*
	*			End File upload
	*****************************************************/

				$("body").on("click", ".btn-import", function() {
						obj = $(this);
						$('#fileupload').click();
				});

				//var timer_process;

				function process_file(filename, ref) {
						obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;กำลังประมวลผลไฟล์");
						//timer_process = setInterval(function(){ check_process() }, 2000);
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "process"
											, "fname" : filename
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: true
							, success: function(data) {
									console.log(data);
									console.log("process " + filename + " complete");
									var obj_count = obj.parents(".form-group").find(".record-count");
									get_rec_count(obj_count, ref);

									//check_process_stop();
									obj.parents(".form-group").find(".help-block").html("");
									$(".btn-import").removeAttr("disabled");
									obj = null;
									// unlink(filename);
							}
						});
				}

				function check_process() {
						console.log("process-check");
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "process-check"
											, "_time" : Math.random()
										}
							, async: true
							, success: function(data) {
									if(obj) {
										console.log(data);
										obj.parents(".form-group").find(".help-block").html(data);
									}
							}
						});
				}

				function check_process_stop() {
						clearInterval(timer_process);
				}

				if($(".record-count").length) {
						$(".record-count").each(function() {
								get_rec_count($(this), $(this).attr("ref"));
						});
				}

				function unlink(filename) {
					$.ajax({
						type: "POST"
						, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
						, data: {
										"do" : "unlink"
										, "fname" : filename
										, "_time" : Math.random()
									}
						, async: false
						, success: function(data) {
								console.log("del " + filename + " complete")
						}
					});
				}

				function get_rec_count(obj_count, ref) {
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "record_count"
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: false
							, success: function(data) {
									obj_count.html(data);
							}
						});
				}

				$("body").on("click", ".btn-truncate-confirm", function() {
						var ref = $(this).attr("ref");
						var obj_count = obj.parents(".form-group").find(".record-count");
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "truncate"
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: false
							, success: function(data) {
									$("#modal").modal("hide");
									obj_count.html("");
									get_rec_count(obj_count, ref);
									obj = null;
							}
						});
				});
				
				function get_period() {
					$(".btn-truncate").addClass('disabled');
					$(obj).html('<i class="fa fa-refresh fa-spin m-r-xxs"></i>รอสักครู่');
					$.ajax({
						type: "POST"
						, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
						, data: {
										"do" : "delete_period"
										, "ref" : obj.attr("ref")
										, "_time" : Math.random()
									}
						, async: true
						, success: function(data) {
							obj.html('ล้างข้อมูล');
							$(".btn-truncate").removeClass('disabled');
							$("#modal").find(".modal-title").html("<h3><q class='m-r-sm'>" + obj.attr("title") + "</q>เลือกเดือน ที่ต้องการล้างข้อมูล</h3>");
							$("#modal").find(".modal-body").html(data);
							$("#modal").find(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>ปิดหน้าต่าง</button>");
							$("#modal").modal();
						}
					});
				}
				
				$("body").on("click", ".btn-period", function() {
					$.ajax({
						type: "POST"
						, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
						, data: {
											"do" 		: "delete_period_confirm"
										, "table" : $(this).data("table")
										, "yy" 		: $(this).data("yy")
										, "mm" 		: $(this).data("mm")
										, "_time" : Math.random()
									}
						, async: true
						, success: function(data) {
							console.log(data);
							get_period();
						}
					});
				});
				
				$("body").on("click", ".btn-truncate", function() {
						obj = $(this);
						var ref = obj.attr("ref");
						if(obj.data('delete_period') == 1) {
							get_period();
						} else {
							$("#modal").find(".modal-title").html("<h3>ยืนยันการล้างข้อมูล</h3>");
							$("#modal").find(".modal-body").html("<div class='text-center'><h3>ต้องการล้างข้อมูล</h3><q>" + $(this).attr("title") + "</q></div>");
							$("#modal").find(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>ปิดหน้าต่าง</button><button type='button' class='btn btn-warning btn-truncate-confirm' ref='" + $(this).attr("ref") + "'><i class='fa fa-trash'></i>&nbsp;ต้องการล้างข้อมูล</button>");
							$("#modal").modal("toggle");
						}
				});

		});
</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();
?>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "นำเข้าข้อมูล" );
	$smarty->assign("breadcrumb", array("นำเข้าข้อมูล" => "import_excel.php") );

	 ob_start();
?>
		<div class="row">
				<div class="col-md-12">
						<div class="panel panel-white">
								<div class="panel-heading clearfix">
										<h4 class="panel-title">&nbsp;</h4>
								</div>
								<div class="panel-body">
										<?php echo $msg->display() ;  ?>
										<?php
												$import_file = array(
																								array("caption" => "ข้อมูลสมาชิก"							, "ref" => "member", "delete-period" => 0, "group" => ""),
																								array("caption" => "ข้อมูลที่อยู่สมาชิก"					, "ref" => "address", "delete-period" => 0, "group" => ""),
																								array("caption" => "ข้อมูลเงินฝาก"							, "ref" => "dept_master", "delete-period" => 0, "group" => ""),
																								array("caption" => "รายการเคลื่อนไหวเงินฝาก"		, "ref" => "dept_statement", "delete-period" => 1, "group" => "oprdtm"),
																								array("caption" => "รายการเรียกเก็บรายเดือน"		, "ref" => "keep", "delete-period" => 1, "group" => "operdtm"),
																								//array("caption" => "KeepFrom"			, "ref" => "keep_from"),
																								//array("caption" => "Keepint"			, "ref" => "keepint"),
																								//array("caption" => "Keepshare"			, "ref" => "keepshare"),
																								array("caption" => "ข้อมูลผู้ค้ำประกัน"					, "ref" => "loan_coll", "delete-period" => 0, "group" => ""),
																								array("caption" => "ข้อมูลเงินกู้"							, "ref" => "loan_master", "delete-period" => 0, "group" => ""),
																								array("caption" => "รายการเคลื่อนไหวเงินกู้"			, "ref" => "loan_statement", "delete-period" => 1, "group" => "operdtm"),
																								array("caption" => "ข้อมูลทุนเรือนหุ้น"					, "ref" => "share_master", "delete-period" => 0, "group" => ""),
																								array("caption" => "รายการเคลื่อนไหวทุนเรือนหุ้น"	, "ref" => "share_statement", "delete-period" => 1, "group" => "operdtm"),
																								array("caption" => "ข้อมูลปันผล-เฉลี่ยคืน"				, "ref" => "divavg", "delete-period" => 0, "group" => ""),
																								array("caption" => "ข้อมูลผู้รับผลประโยชน์"			, "ref" => "member_gain", "delete-period" => 0, "group" => ""),
																								//array("caption" => "Insurance"			, "ref" => "insurance2"),
																										// array("caption" => "AG"					, "ref" => "ag"),
																										// array("caption" => "Insurance1"			, "ref" => "insurance1"),
																										// array("caption" => "Keepdep"			, "ref" => "keepdep"),
																										// array("caption" => "WF"					, "ref" => "wf"),
																										// array("caption" => "WFKeep"				, "ref" => "wfkeep"),
																										// array("caption" => "WFKeepName"			, "ref" => "wfkeepname")
																						);
										?>
										<form class="form-horizontal" action="" method="post" >
												<input id="fileupload" type="file" name="files[]" class="hide" accept=".csv" />
												<?php foreach($import_file as $key => $val) { ?>
												<div class="form-group">
														<label class="col-sm-3 control-label">ไฟล์ <?php echo $val["caption"]; ?></label>
														<div class="col-sm-3">
																<button type="button" class="btn btn-info btn-block btn-import" ref="<?php echo $val["ref"]; ?>">นำเข้าข้อมูล</button>
																<div class="help-block text-center text-info"></div>
														</div>
														<div class="col-sm-3 form-control-static record-count" ref="<?php echo $val["ref"]; ?>"></div>
														<div class="col-sm-3">
																<button type="button" class="btn btn-danger btn-block btn-truncate" ref="<?php echo $val["ref"]; ?>" data-delete_period="<?php echo $val["delete-period"]; ?>" data-groupby="<?php echo $val["group"]; ?>" title="<?php echo $val["caption"]; ?>">ล้างข้อมูล</button>
														</div>
												</div>
												<?php } ?>
										</form>
								</div>
						</div>
				</div>
		</div>

		<div class="modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
						<div class="modal-content">
								<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<div class="modal-title" id="myModalLabel"></div>
								</div>
								<div class="modal-body"></div>
								<div class="modal-footer"></div>
						</div>
				</div>
		</div>
<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
