<?php
session_start();
date_default_timezone_set('Asia/Bangkok');
// header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;
require "menu.inc.php" ;
// $cmpper->check("message") ;
ob_start();
?>

<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
 <link rel="stylesheet" href="<?php echo HTTP;?>ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />

<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="/scripts/jqueryui/jquery.ui.datepicker-th.js"></script>
<script src="/scripts/jqueryui/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
<script src="/scripts/validation/js/formValidation.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<script src="/scripts/validation/js/bootstrap.js"></script>


<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script>

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();


$(document).ready(function() {

	 $("#datepicker_start").datepicker();
	 $("#datepicker_end").datepicker();


	$('#department_name').on('change', function() {
				 var name_head = $('option:selected', this).attr('name_head');
				 var tel_head = $('option:selected', this).attr('tel_head');
					// console.log(name_head,tel_head);
					$("#name_head").val(name_head);
					$("#telmobile").val(tel_head);
	});


});
</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$page = (int) @$_GET["page"]  ;
	$smarty->assign("title", "รายงานคำขอกู้" );
	// $smarty->assign("page_title", "รายงานคำขอกู้" );

	ob_start();
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">รายงานคำขอกู้</h4>
			</div>
			<div class="panel-body">
				<?php	if (in_array($_GET['do'], array('add', 'edit'))) {
					if ($_GET['do'] == 'add') {
						$smarty->assign("breadcrumb", array("รายงานคำขอกู้" => "loan_reprot.php", "เพิ่มข้อมูล" => "#"));
					} else {
						$smarty->assign("breadcrumb", array("รายงานคำขอกู้" => "loan_reprot.php", "แก้ไขข้อมูล" => "#"));

						$sql = "SELECT * FROM cmp_admin_loan WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
						$user = $result->fetch_object();

						$sql = "SELECT * FROM cmp_letters WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
					}
				?>
					
				<?php
					} else {
						$smarty->assign("breadcrumb", array("รายงานคำขอกู้" => "?") );
					
				 file_get_contents("http://loan.cmcoop.or.th/loan_report.pdf.php?type_loan=".$_GET['type_loan']."&day_start=".$_GET['day_start']."&day_end=".$_GET['day_end']." ");

						if ($_GET['type_loan']) {

							$loan_application_type = array();
							$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
							$rs_type = $mysqli->query($sql_type);
							while($row_type = $rs_type->fetch_assoc()){
								$loan_application_type[$row_type['id']] = $row_type['loan_name'];
							}

	  						$getdate_start = @$mysqli->real_escape_string(ConvertToSQLDate(@$_GET["day_start"]));
							$time_start = "00:00:00" ;
							$start = date("Y-m-d H:i:s", strtotime($getdate_start.' '.$time_start));

	  						$getdate_end = @$mysqli->real_escape_string(ConvertToSQLDate(@$_GET["day_end"]));
							$time_end = "23:59:59" ;
							$end = date("Y-m-d H:i:s", strtotime($getdate_end.' '.$time_end));
							
								$sql = "SELECT * FROM 	cmp_admin_loan_money 
								WHERE (loan_date BETWEEN '{$start}' AND '{$end}')
								AND loan_application_type = '{$_GET['type_loan']}'
								AND b_send_doc_status = 1
								ORDER BY loan_date ASC ";

								$rs = $mysqli->query($sql);
								$count_row = $rs->num_rows;

								echo $mysqli->error;

						}
					

						?>
						<form action="?" method="get">
							<div class="col-md-4">
								<div class="form-group">
                                            <label style="margin-top: 6px;" class="col-md-4 control-label">ประเภทคำขอกู้ :</label>
                                            <div class="col-md-8">												
                                                <select class="form-control m-b-sm" name="type_loan">
													<?php
													$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
													$rs_type = $mysqli->query($sql_type);
													while($row_type = $rs_type->fetch_assoc()){
													
													?>
                                                    <option <?php echo ($_GET["type_loan"] == $row_type['id']) ? "selected" : "" ;?> value="<?php echo $row_type['id']?>">คำขอ<?php echo $row_type['loan_name'];?></option>
                                                    <?php 
													}
													?>
												</select>
                                            </div>
                                 </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                            <label style="margin-top: 6px;" class="col-md-2 control-label">วันที่ :</label>
                                            <div class="col-md-4">
											<?php
											if ($_GET['day_start']) {
												$time = strtotime($_GET['day_start']);
												$myFormatForView_start = $_GET['day_start'];
											}else{
				  								$time = date("Y-m-d");
												$date = str_replace('/', '/', $time);
											    $myFormatForView_start = date('d/m/Y', strtotime($date));
											}

											?>
										<input  
												 type="text"
												 id="datepicker_start"
												 class="form-control form-group" 
												 name="day_start" 
												 value="<?=$myFormatForView_start ?>"
												 required="required"
										>
											</div>

										  <label style="margin-top: 6px;" class="col-md-2 control-label">ถึงวันที่ :</label>
                                            <div class="col-md-4">
											<?php
											if ($_GET['day_end']) {
												$time_end = strtotime($_GET['day_end']);
												$myFormatForView_end = $_GET['day_end'];
											}else{
				  								$time_end = date("Y-m-d");
												$date_end = str_replace('/', '/', $time_end);
											    $myFormatForView_end = date('d/m/Y', strtotime($date_end));
											}
											?>
										<input  
												 type="text"
												 id="datepicker_end"
												 class="form-control form-group" 
												 name="day_end" 
												 value="<?=$myFormatForView_end ?>"
												 required="required"
										>

											</div>

							</div>
							</div>

							<div class="col-md-1">
								<button type="submit" class="btn btn-info">ตกลง</button>
							</div>
						</form>
						<?php if (isset($_GET['err'])) { ?>
							<div class="alert alert-danger resalt">
								<p>ERROR : <?= $_GET['err'] ?></p>
							</div>
						<?php } elseif (isset($_GET['res'])) { ?>
							<div class="alert alert-success resalt">
								<p>! <?= $_GET['res'] ?></p>
							</div>
						<?php } ?>
						<div style="clear: both;margin-bottom:15px;margin-top: 6em;"></div>
						<table class="table">
							<thead>
								<tr>
									<th class="text-center">ลำดับ</th>
									<th class="text-center">วันที่</th>
									<th class="text-center">ชื่อ-สกุล</th>
									<th class="text-center">ประเภท</th>
								</tr>
							</thead>
							<tbody>

								<?php if ($count_row == 0) { ?>

									<tr>
									<td colspan="4" class="text-center">
										<?php if ($_GET['type_loan']) { ?>
											<h1>ไม่พบใบคำขอกู้</h1>
										<?php } ?>
									</td>
									</tr>

								<?php }else{ ?>

								<?php $i=1; while(($row = $rs->fetch_assoc())) { ?>
									<tr>
										<td class="text-center"><?=$i;?></td>
										<td class="text-center"><?php echo date_time($row["loan_date"], true, false, true); ?></td>
										<td width="220" class="text-left"><?php echo $row["member_name"]; ?></td>
										<td class="text-center">
										<?php echo 'คำขอ'.$loan_application_type[$row['loan_application_type']];?>
										<?php
										 /*if($row['loan_application_type'] == 1){
										 	echo "คำขอกู้เงินสามัญ";
										 }else if($row['loan_application_type'] == 2){ 
										 	echo "ขอกู้เงินสามัญ ATM";
										 }else{
										 	echo "คำขอกู้เหตุฉุกเฉิน";
										 }*/
										 ?>
										
										</td>
									</tr>
								<?php
									$i++;	}
								}
									unset($sql, $rs, $row);
								?>
							</tbody>
						</table>
					
					<?php if ($_GET['type_loan']) { ?>
						<?php if ($count_row != 0) { ?>
						
					<div class="text-center" style="margin-top:10px;" >

					<a target="_blank" href="loan_report.excle.php?type_loan=<?php echo $_GET['type_loan'] ?>&day_start=<?php echo $_GET['day_start'] ?>&day_end=<?php echo $_GET['day_end'] ?>">
						<button type="button" class="btn btn-primary">
							<span class="glyphicon glyphicon-print" aria-hidden="true"></span>&nbsp;&nbsp;พิมพ์ Excel
						</button>
						</a>
						<!--a target="_blank" href="download_pdf.php?file=../PDF_Report/Report.pdf">
						<button style="width:105.94px; " type="button" class="btn btn-primary">
							<span class="glyphicon glyphicon-print" aria-hidden="true"></span>&nbsp;&nbsp; PDF
						</button>
						</a-->
						<?php
							$file_pdf = BASEPATH."/admin/loan_report_pdf.php?type_loan=".$_GET['type_loan']."&day_start=".$_GET['day_start']."&day_end=".$_GET['day_end'];
						?>
						<a target="_blank" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/html2pdf_service_utf8/html2pdf.service.php?filename=test&Format=A4&Orientation=P&HtmlFile=<?php echo rawurlencode($file_pdf); ?>">
						<button style="width:105.94px; " type="button" class="btn btn-primary">
							<span class="glyphicon glyphicon-print" aria-hidden="true"></span>&nbsp;&nbsp; PDF
						</button>
						</a>
					</div>


					<?php } ?>
					<?php } ?>

				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();

$smarty->assign("content", $content);
$smarty->display(THEME.".tpl");
