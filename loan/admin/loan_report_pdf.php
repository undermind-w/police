<?php
session_start();
date_default_timezone_set('Asia/Bangkok');
require "config.inc.php" ;
//require "session.inc.php" ;
//require "menu.inc.php" ;
header("Content-Type:text/html;charset=utf-8");
?>

<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
	.text-center{
		text-align:center;
	}
	.all_border{
		border: 1px solid;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title" style="text-align:center">รายงานคำขอกู้</h4>
			</div>
			<div class="panel-body" >
				<?php	if (in_array($_GET['do'], array('add', 'edit'))) {
					if ($_GET['do'] == 'add') {
						$smarty->assign("breadcrumb", array("รายงานคำขอกู้" => "loan_reprot.php", "เพิ่มข้อมูล" => "#"));
					} else {
						$smarty->assign("breadcrumb", array("รายงานคำขอกู้" => "loan_reprot.php", "แก้ไขข้อมูล" => "#"));

						$sql = "SELECT * FROM cmp_admin_loan WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
						$user = $result->fetch_object();

						$sql = "SELECT * FROM cmp_letters WHERE loan_id = {$_GET['loan_id']}";
						$result = $mysqli->query($sql);
					}
				?>
					
				<?php
					} else {
				 file_get_contents("http://loan.cmcoop.or.th/loan_report.pdf.php?type_loan=".$_GET['type_loan']."&day_start=".$_GET['day_start']."&day_end=".$_GET['day_end']." ");

						if ($_GET['type_loan']) {



	  						$getdate_start = @$mysqli->real_escape_string(ConvertToSQLDate(@$_GET["day_start"]));
							$time_start = "00:00:00" ;
							$start = date("Y-m-d H:i:s", strtotime($getdate_start.' '.$time_start));

	  						$getdate_end = @$mysqli->real_escape_string(ConvertToSQLDate(@$_GET["day_end"]));
							$time_end = "23:59:59" ;
							$end = date("Y-m-d H:i:s", strtotime($getdate_end.' '.$time_end));
							
								$loan_application_type = array();
								$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
								$rs_type = $mysqli->query($sql_type);
								while($row_type = $rs_type->fetch_assoc()){
									$loan_application_type[$row_type['id']] = $row_type['loan_name'];
								}	
					
					
								$sql = "SELECT * FROM 	cmp_admin_loan_money 
								WHERE (loan_date BETWEEN '{$start}' AND '{$end}')
								AND loan_application_type = '{$_GET['type_loan']}'
								AND b_send_doc_status = 1
								ORDER BY loan_date ASC ";

								$rs = $mysqli->query($sql);
								$count_row = $rs->num_rows;

								echo $mysqli->error;

						}
					
						
						?>
						<?php if (isset($_GET['err'])) { ?>
							<div class="alert alert-danger resalt">
								<p>ERROR : <?= $_GET['err'] ?></p>
							</div>
						<?php } elseif (isset($_GET['res'])) { ?>
							<div class="alert alert-success resalt">
								<p>! <?= $_GET['res'] ?></p>
							</div>
						<?php } ?>
						<div style="clear: both;margin-bottom:15px;margin-top: 6em;"></div>
						<table class="table" width="100%" cellspacing=0>
							<thead>
								<tr>
									<th class="text-center all_border">ลำดับ</th>
									<th class="text-center all_border">วันที่</th>
									<th class="text-center all_border">ชื่อ-สกุล</th>
									<th class="text-center all_border">ประเภท</th>
								</tr>
							</thead>
							<tbody>

								<?php if ($count_row == 0) { ?>

									<tr>
									<td colspan="4" class="text-center">
										<?php if ($_GET['type_loan']) { ?>
											<h1>ไม่พบใบคำขอกู้</h1>
										<?php } ?>
									</td>
									</tr>

								<?php }else{ ?>

								<?php $i=1; while(($row = $rs->fetch_assoc())) { ?>
									<tr>
										<td class="text-center all_border"><?=$i;?></td>
										<td class="text-center all_border"><?php echo date_time($row["loan_date"], true, false, true); ?></td>
										<td width="220" class="text-left all_border"><?php echo $row["member_name"]; ?></td>
										<td class="text-center all_border">
										<?php echo 'คำขอ'.$loan_application_type[$row['loan_application_type']]; ?>
										
										</td>
									</tr>
								<?php
									$i++;	}
								}
									unset($sql, $rs, $row);
								?>
							</tbody>
						</table>
					
					<?php if ($_GET['type_loan']) { ?>
						<?php if ($count_row != 0) { ?>
					<?php } ?>
					<?php } ?>

				<?php } ?>
			</div>
		</div>
	</div>
</div>
