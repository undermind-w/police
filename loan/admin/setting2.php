<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php";
	require "menu.inc.php" ;
	if($_POST["email"]) {
			$email = $mysqli->real_escape_string($_POST["email"]);
			$sql = "UPDATE loan_admin SET admin_email = '{$email}' WHERE admin_id = '{$_SESSION['ADMIN_ID']}'";
			$mysqli->query($sql);
	} else if ($_POST["tel"]){
			$tel = $mysqli->real_escape_string($_POST["tel"]);
			$sql = "UPDATE loan_admin SET admin_tel = '{$tel}' WHERE admin_id = '{$_SESSION['ADMIN_ID']}'";
			$mysqli->query($sql);
	}
	ob_start();
?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.modal-content {
		margin-top: 50%;
	}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
	ob_start();
?>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();
?>
<script>
	$(document).ready(function() {
		$('#email').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var email = button.data('email');
			var modal = $(this);
			modal.find('.modal-body #email_val').val(email);
		});	
		$('#tel').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var tel = button.data('tel');
			var modal = $(this);
			modal.find('.modal-body #tel_val').val(tel);
		});	
	});
</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	$smarty->assign("title", "ตั้งค่าระบบ");
	$smarty->assign("breadcrumb", array("ตั้งค่าระบบ" => "#"));
	ob_start();
?>
<?php 
	$sql = "SELECT admin_email,admin_tel FROM loan_admin WHERE admin_id = '{$_SESSION['ADMIN_ID']}'";
	$rs  = $mysqli->query($sql);
	$row = $rs->fetch_assoc();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">กำหนดอีเมล์และเบอร์แจ้งเตือน เมื่อมีคำร้องขอกู้เงินออนไลน์</h4>
				</div>
				<br>
				<div class="panel-body">
					<form id="frm" class="form-horizontal">
							<div class="form-group">
									<label for="member_no" class="col-sm-offset-1 col-sm-4 control-label"><h4 class="no-m">กำหนดอีเมล์ฝ่ายสินเชื่อ :  </h4></label>
									<div class="col-sm-3">
											<p style="font-size:15px;margin-top:2px;"><?=$row['admin_email']?></p>
									</div>
									<div class="col-sm-3">
										<button class="btn btn-info" type="button" style="width:112.63px;" data-toggle="modal" data-target="#email" data-email="<?=$row['admin_email']?>"> เปลี่ยนอีเมล์</button>
									</div>
							</div>

							<div class="form-group">
									<label for="member_no" class="col-sm-offset-1 col-sm-4 control-label"><h4 class="no-m">หมายเลขรับ SMS แจ้งเตือน : </h4></label>
									<div class="col-sm-3">
											<p style="font-size:15px;margin-top:2px;"><?=$row['admin_tel']?></p>
									</div>
									<div class="col-sm-3">
										<button class="btn btn-info" type="button" style="width:112.63px;" data-toggle="modal" data-target="#tel" data-tel="<?=$row['admin_tel']?>"> เปลี่ยนหมายเลข</button>
									</div>
							</div>

					</form>
				</div>
			</div>
		</div>
	</div>

<div class="modal" id="modal-password" tabindex="-1" role="dialog" aria-labelledby="modal-password-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body text-center">
				<h3>เปลี่ยนรหัสผ่านเรียบร้อยแล้วค่ะ</h3>
				<button type="button" class="btn btn-lg btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="email" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST">
          <div class="form-group">
            	<label for="recipient-name" class="control-label col-sm-2 " style="font-size:15px;"> อีเมล์ : </label>
				<div class="col-sm-10">
            		<input type="email" class="form-control" id="email_val" name="email">
				</div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> ปิด </button>
        <button type="submit" class="btn btn-info"> บันทึก </button>
      </div>
	  </form>
    </div>
  </div>
</div>

<div class="modal fade" id="tel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST">
          <div class="form-group">
            	<label for="recipient-name" class="control-label col-sm-2 " style="font-size:15px;"> หมายเลข : </label>
				<div class="col-sm-10">
            		<input type="tel" class="form-control" id="tel_val" name="tel" maxlength="10">
				</div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> ปิด </button>
        <button type="submit" class="btn btn-info"> บันทึก </button>
      </div>
	  </form>
    </div>
  </div>
</div>


<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
