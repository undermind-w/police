<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;
require "menu.inc.php" ;
ob_start();
?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
	.bt_dis{
		cursor: not-allowed !important;
		pointer-events: auto !important;
	}
	.not_line{
		text-decoration: none !important;
	}

	.section {
		clear: both;
		padding: 0px;
		margin: 0px;
	}

	/*  COLUMN SETUP  */
	.col {
		display: block;
		float:left;
		margin: 1% 0 1% 1%;
	}
	.col:first-child { margin-left: 0; }

	/*  GROUPING  */
	.group:before,
	.group:after { content:""; display:table; }
	.group:after { clear:both;}
	.group { zoom:1; /* For IE 6/7 */ }
	/*  GRID OF FIVE  */
	.span_5_of_5 {
		width: 100%;
	}
	.span_4_of_5 {
		width: 79.8%;
	}
	.span_3_of_5 {
		width: 59.6%;
	}
	.span_2_of_5 {
		width: 39.4%;
	}
	.span_1_of_5 {
		/* width: 19.2%; */
		width: 24.2%;
	}

	/*  GO FULL WIDTH BELOW 480 PIXELS */
	@media only screen and (max-width: 480px) {
		.col {  margin: 1% 0 1% 0%; }
		.span_1_of_5, .span_2_of_5, .span_3_of_5, .span_4_of_5, .span_5_of_5 { width: 100%; }
	}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
	ob_start();
?>
<script src="/scripts/validation/js/formValidation.js"></script>
<script src="/scripts/validation/js/bootstrap.js"></script>
<script src="/scripts/filestyle/bootstrap-filestyle.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<!-- <script src="/html/meter/admin1/assets/js/pages/dashboard.js"></script> -->
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script>

</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$page = (int) @$_GET["page"]  ;
	$smarty->assign("title", "ระบบคำขอกู้เงิน" );
	// $smarty->assign("page_title", "ระบบคำขอกู้เงิน" );

	ob_start();
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">ระบบคำขอกู้เงิน >วิเคราะห์เงินกู้ สำหรับเจ้าหน้าที่ </h4>
			</div>
			<div class="panel-body">
				<div class="text-left">
					<?php 
					$loan_application_type = array('1'=>'กู้เงินสามัญ','3'=>'กู้เหตุฉุกเฉิน');
					$sql = "SELECT 
						* 
					FROM 
						cmp_admin_loan_money t1 
						INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
					WHERE t1.loan_id = '".$_GET['loan_id']."'"; 
					$rs = $mysqli->query($sql);
					$row = $rs->fetch_assoc();
					?>
					<div class="col-sm-12">
						<p class="col-sm-2">วันที่</p>
						<p class="col-sm-9"><?= date_time(date('Y-m-d'), true);?></p>
					</div>

					<div class="col-sm-12">
						<p class="col-sm-2">รหัสสมาชิก</p>
						<p class="col-sm-9"><?php echo $row["member_id"]; ?></p>
					</div>

					<div class="col-sm-12">
						<p class="col-sm-2">ชื่อ-สกุล</p>
						<p class="col-sm-9"><?php echo $row["member_name"]; ?></p>
					</div>
					
					<div class="col-sm-12">
						<p class="col-sm-2">ประเภทคำขอกู้ : </p>
						<p class="col-sm-9"><?php echo $loan_application_type[$row["loan_application_type"]]; ?></p>
					</div>
					
					<div class="col-sm-12">
						<p class="col-sm-2">จำนวนเงินที่ขอกู้ : </p>
						<p class="col-sm-9"><?php echo number_format($row["loan_money"],2); ?> บาท</p>
					</div>
					
					<div class="col-sm-12">
						<p class="col-sm-2">วัตถุประสงค์การขอกู้ : </p>
						<p class="col-sm-9"><?php echo $row["member_use"]; ?></p>
					</div>
				<form action="loan_officer_analys_save.php" id="form1" method="POST">
					<?php
						$sql_result = "SELECT * FROM loan_result WHERE loan_id='".$_GET['loan_id']."'";
						$rs_result = $mysqli->query($sql_result);
						$row_result = $rs_result->fetch_assoc();
					?>
					<input type="hidden" name="loan_result[id]" value="<?php echo $row_result['id']; ?>">
					<input type="hidden" name="loan_result[loan_id]" value="<?php echo $_GET['loan_id']; ?>">
					<div class="col-sm-12">
						<p class="col-sm-2">จำนวนเงินที่อนุมัติ : </p>
						<p class="col-sm-2"><input type="number" name="loan_result[loan_approve_money]" class="form-control" value="<?php echo $row_result['loan_approve_money']; ?>"></p>
						<p class="col-sm-1">บาท</p>
					</div>
					
					<div class="col-sm-12">
						<p class="col-sm-2">ชำระเงินกู้เดือนละ : </p>
						<p class="col-sm-2"><input type="number" name="loan_result[pay_per_month]" class="form-control" value="<?php echo $row_result['pay_per_month']; ?>"></p>
						<p class="col-sm-1">บาท</p>
					</div>
					
					<div class="col-sm-12">
						<p class="col-sm-2">งวดสุดท้ายชำระ : </p>
						<p class="col-sm-2"><input type="number" name="loan_result[pay_last_month]" class="form-control" value="<?php echo $row_result['pay_last_month']; ?>"></p>
						<p class="col-sm-1">บาท</p>
					</div>
					
					<div class="col-sm-12">
						<p class="col-sm-2">จำนวนเดือน : </p>
						<p class="col-sm-2"><input type="number" name="loan_result[month_period]" class="form-control" value="<?php echo $row_result['month_period']; ?>"></p>
						<p class="col-sm-1">เดือน</p>
					</div>
					
					<div class="col-sm-12">
						<p class="col-sm-2">อัตราดอกเบี้ยร้อยละ : </p>
						<p class="col-sm-2"><input type="number" name="loan_result[interest_rate]" class="form-control" value="<?php echo $row_result['interest_rate']; ?>"></p>
						<p class="col-sm-1"></p>
					</div>
					<div class="col-sm-12">
						<p class="col-sm-2">หลักประกัน : </p>
						<p class="col-sm-2"><input type="checkbox" name="loan_result[guarantee_by_person]" <?php echo $row_result['guarantee_by_person']=='1'?'checked':'';?> value="1" class="form-control" > ใช้ผู้ค้ำประกัน</p>
						<p class="col-sm-2"><input type="checkbox" name="loan_result[guarantee_by_share]" <?php echo $row_result['guarantee_by_share']=='1'?'checked':'';?> value="1" class="form-control" > ใช้หุ้น</p>
					</div>
				</div>
				<div class="text-center">
					<input style="width: 111px;" type="submit" class="btn btn-info" value="บันทึก">
				</div>
			</form>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="text-center">ลำดับ</th>
							<th class="text-center">ชื่อเอกสาร</th>
							<th class="text-center"></th>
						</tr>
					</thead>
					<tbody>
					<?php 
					$sql_doc = "SELECT * FROM loan_document WHERE loan_application_type = '".$row['loan_application_type']."' ORDER BY order_by ASC"; 
					$rs_doc = $mysqli->query($sql_doc);
					$i = 1;
					while($row_doc = $rs_doc->fetch_assoc()){
						if($row_result['guarantee_by_person']=='' && $row_doc['type']=='1'){
							continue;
						}
						if($row_doc['type']=='1'){ ?>
							<?php 
								$sql_bondsman="SELECT * FROM loan_application_bondsman WHERE loan_id = '".$_GET['loan_id']."' ORDER BY bondsman_num ASC";
								$rs_bondsman = $mysqli->query($sql_bondsman);
								while($row_bondsman = $rs_bondsman->fetch_assoc()){ ?>
									<tr>
										<td align="center" width="10%"><?php echo $i++; ?></td>
										<td width="40%"><?php echo $row_doc['description']." ".$row_bondsman['bondsman_num']; ?></td>
										<td width="20%" align="center">
											<a class="btn btn-info" target="_blank" href="/<?php echo $row_doc['document_link']; ?>?loan_application_id=<?php echo $row['loan_application_id']; ?>&loan_id=<?php echo $_GET['loan_id']; ?>&bondsman_id=<?php echo $row_bondsman['bondsman_id']; ?>">Download</a>
										</td>
									</tr>
								<?php } ?>
						<?php }else{ ?>
						<tr>
							<td align="center" width="10%"><?php echo $i++; ?></td>
							<td width="40%"><?php echo $row_doc['description']; ?></td>
							<td width="20%" align="center">
								<a class="btn btn-info" target="_blank" href="/<?php echo $row_doc['document_link']; ?>?loan_application_id=<?php echo $row['loan_application_id']; ?>&loan_id=<?php echo $_GET['loan_id']; ?>">Download</a>
							</td>
						</tr>
					<?php 
						}
					} 
					?>
					</tbody>
				</table>					
			</div>
		</div>
	</div>
</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();


$smarty->assign("content", $content);
$smarty->display(THEME.".tpl");
