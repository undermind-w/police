<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;
require "menu.inc.php" ;

ob_start();
if($_POST){
	if($_POST['do'] == 'check_approve'){	
		//จำนวนผู้นุมัติ จากการตั้งค่าเงินกู้
		$sql_num = "SELECT approve_num FROM loan_setting_detail WHERE id = '".$_POST['loan_application_type']."'";
		$rs_num = $mysqli->query($sql_num);
		$row_num = $rs_num->fetch_assoc();
		$approve_num = $row_num['approve_num'];
		
		//จำนวนผู้อนุมัติ ที่อนุมัติ และไม่อนุมัติ
		$sql_num_person = "SELECT COUNT(id) AS num_person_approve FROM loan_person_approve 
					WHERE loan_application_type_id = '".$_POST['loan_application_type']."' AND loan_application_id = '".$_POST['loan_application_id']."'";
		$rs_num_person = $mysqli->query($sql_num_person);
		$row_num_person = $rs_num_person->fetch_assoc();
		$num_person_approve = $row_num_person['num_person_approve'];
		$list_approve = '';
		if($approve_num == $num_person_approve){
			$list_approve = '';
		}else{
			$sql_person = "SELECT t1.*,
							t2.admin_name 
							FROM loan_person_approve t1 
							LEFT JOIN loan_admin t2 ON t1.responsible_person_id = t2.admin_id
							WHERE 
								t1.loan_application_type_id = '".$_POST['loan_application_type']."' 
								AND t1.loan_application_id = '".$_POST['loan_application_id']."'";
			$rs_person = $mysqli->query($sql_person);
			while($row_person = $rs_person->fetch_assoc()){
				$status_date = ($row_person['status_date'] != "") ? date_time($row_person["status_date"], true, true, true) : "";
				if($row_person['approve_status'] == '1'){
					$list_approve .=  'คุณ '.$row_person['admin_name'].' อนุมัติแล้วเมื่อ '.$status_date.' จำนวน '.number_format($row_person['approve_money'],2).' บาท <br>';
				}else if($row_person['approve_status'] == '2'){
					$list_approve .= 'คุณ  '.$row_person['admin_name'].' ไม่อนุมัติ เมื่อ '.$status_date.' เหตุผล '.$row_person['approve_reason'].'<br>';
				}
			}	
		}
		echo $list_approve;
		exit;	
	}else{	
		$sql = "SELECT 
			t1.member_id ,
			t1.member_name,
			t1.member_tel ,
			t1.loan_application_type,
			t1.loan_application_id,
			t3.loan_number
		FROM 
			loan_application t1 
			LEFT JOIN cmp_admin_loan_money t3 ON t1.loan_id = t3.loan_id
		WHERE 
			t1.loan_id = '".$_POST['loan_id']."'";
		$rs = $mysqli->query($sql);
		$row = $rs->fetch_assoc();
		
		$sql_admin = "SELECT * FROM loan_admin WHERE admin_id = '".$_SESSION['ADMIN_ID']."'";
		$rs_admin = $mysqli->query($sql_admin);
		$row_admin = $rs_admin->fetch_assoc();
		$admin_tel = $row_admin['admin_tel'];
		$admin_id = $_SESSION['ADMIN_ID'];
		$admin_name = $row_admin['admin_name'];
		
		if($_POST['consider_result'] == '1'){
			$balance = str_replace(',','',$_POST['approve_money']);
			
			$sql_update = "UPDATE loan_application SET 
				approve_status = '1' ,
				approve_money = '".str_replace(',','',$_POST['approve_money'])."' ,
				approve_date = NOW() 
			WHERE loan_id = '".$_POST['loan_id']."'";
			$mysqli->query($sql_update);
			
			$message = 'คำร้องข้อกู้เลขที่ '.$row['loan_number'].' ของ '.$row['member_name'].' รหัสสมาชิก '.$row['member_id'].' ได้รับการพิจารณาแล้ว  โดย '.$admin_name;
			//send_sms($row['admin_tel'],$message);
			$send_status = "insert cmp_admin_loan_money_status set
				loan_id = '".$_POST['loan_id']."',
				status_name = 'ส่งให้เจ้าหน้าที่สินเชื่อ : ".$message."',
				status_date = NOW(),
				loan_application_type = '".$row['loan_application_type']."'
			"; 
			$mysqli->query($send_status);
			
		}else if($_POST['consider_result'] == '2'){
			$message = 'คำร้องข้อกู้เลขที่ '.$row['loan_number'].' ไม่ผ่านการพิจารณาเนื่องจาก '.$_POST['not_approve_reason'].' โดย '.$admin_name;
			
			$sql = "update cmp_admin_loan_money set
						not_approve = '1',
						not_approve_date = NOW(),
						not_approve_message='".$message."'
						WHERE
						loan_id = '".$_POST['loan_id']."'
			";

			$mysqli->query($sql);

				$sql_app = "update loan_application set
							loan_status = '3'
						WHERE
							loan_id = '{$_POST['loan_id']}'
				";
				$mysqli->query($sql_app);
			
			send_sms($row['member_tel'],$message);
			$send_status = "insert cmp_admin_loan_money_status set
				loan_id = '".$_POST['loan_id']."',
				status_name = 'ส่งให้ผู้ขอกู้ : ".$message."',
				status_date = NOW(),
				loan_application_type = '".$row['loan_application_type']."'
			"; 
			$mysqli->query($send_status);
			
		}else if($_POST['consider_result'] == '3'){
			$sql = "SELECT admin_id, admin_tel, admin_department_id FROM loan_admin WHERE admin_department_id = '6' AND admin_status = '1'";
			$rs = $mysqli->query($sql);
			while($row = $rs->fetch_assoc()){
				$admin_department_id = $row['admin_department_id'];
				$message = 'มีผู้ขอกู้รออนุมัติในระบบคำร้องขอกู้เงิน เลขที่ '.$row['loan_number'].' เนื่องจาก '.$_POST['send_forward_reason'].' '.BASEPATH."/admin/loan_approve.php";
				//send_sms($row['admin_tel'],$message);
			}
			
			//$sql_update = "UPDATE loan_application SET approver_id = '".$admin_department_id."', send_forward_reason = '".$_POST['send_forward_reason']."' WHERE loan_id = '".$_POST['loan_id']."'";
			//$mysqli->query($sql_update);		
			
		}		
		
		$send_status = "insert loan_person_approve set
			loan_application_id = '".$row['loan_application_id']."',
			loan_id = '".$_POST['loan_id']."',
			loan_application_type_id = '".$row['loan_application_type']."',
			responsible_person_id = '".$admin_id."',
			approve_status = '".$_POST['consider_result']."',
			approve_money = '".str_replace(',','',$_POST['approve_money'])."',
			approve_reason = '".$_POST['not_approve_reason']."',
			status_date = NOW()
		"; 
		$mysqli->query($send_status);
		
		//จำนวนผู้นุมัติ จากการตั้งค่าเงินกู้
		$sql_num = "SELECT approve_num FROM loan_setting_detail WHERE id = '".$row['loan_application_type']."'";
		$rs_num = $mysqli->query($sql_num);
		$row_num = $rs_num->fetch_assoc();
		$approve_num = $row_num['approve_num'];
		
		//จำนวนผู้อนุมัติ ที่อนุมัติ และไม่อนุมัติ
		$sql_num_person = "SELECT COUNT(id) AS num_person_approve FROM loan_person_approve 
					WHERE loan_application_type_id = '".$row['loan_application_type']."' AND loan_application_id = '".$row['loan_application_id']."'";
		$rs_num_person = $mysqli->query($sql_num_person);
		$row_num_person = $rs_num_person->fetch_assoc();
		$num_person_approve = $row_num_person['num_person_approve'];
		
		//if($approve_num == $num_person_approve){
			$message = 'ผู้อนุมัติพิจารณาคำร้องที่ '.$row['loan_number'].'  ครบแล้ว';
			
			/*$sql_analyst = "SELECT * FROM loan_admin WHERE admin_department_id = '3'";
			$rs_analyst = $mysqli->query($sql_analyst);
			while($row_analyst = $rs_analyst->fetch_assoc()){
				send_sms($row_analyst['admin_tel'],$message);	
			}	*/		
			
			$send_status = "insert cmp_admin_loan_money_status set
				loan_id = '".$_POST['loan_id']."',
				status_name = 'ส่งให้เจ้าหน้าที่สินเชื่อ : ".$message."',
				status_date = NOW(),
				loan_application_type = '".$row['loan_application_type']."'
			"; 
			$mysqli->query($send_status);
			
			$sql_update = "UPDATE loan_application SET 
				approve_status = '1' ,
				approve_money = '".str_replace(',','',$_POST['approve_money'])."' ,
				approve_date = NOW() 
			WHERE loan_id = '".$_POST['loan_id']."'";
			$mysqli->query($sql_update);
			
		//}else{
		//	$message = '';
		//}
		echo "<script> document.location.href=''; </script>";
	}
}
?>
<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
	.bt_dis{
		cursor: not-allowed !important;
		pointer-events: auto !important;
	}
	.not_line{
		text-decoration: none !important;
	}

	.section {
		clear: both;
		padding: 0px;
		margin: 0px;
	}

	/*  COLUMN SETUP  */
	.col {
		display: block;
		float:left;
		margin: 1% 0 1% 1%;
	}
	.col:first-child { margin-left: 0; }

	/*  GROUPING  */
	.group:before,
	.group:after { content:""; display:table; }
	.group:after { clear:both;}
	.group { zoom:1; /* For IE 6/7 */ }
	/*  GRID OF FIVE  */
	.span_5_of_5 {
		width: 100%;
	}
	.span_4_of_5 {
		width: 79.8%;
	}
	.span_3_of_5 {
		width: 59.6%;
	}
	.span_2_of_5 {
		width: 39.4%;
	}
	.span_1_of_5 {
		/* width: 19.2%; */
		width: 24.2%;
	}

	/*  GO FULL WIDTH BELOW 480 PIXELS */
	@media only screen and (max-width: 480px) {
		.col {  margin: 1% 0 1% 0%; }
		.span_1_of_5, .span_2_of_5, .span_3_of_5, .span_4_of_5, .span_5_of_5 { width: 100%; }
	}
	tbody {
		overflow-y: auto;    /* Trigger vertical scroll    */
		overflow-x: auto;  /* Trigger the horizontal scroll */
	}
	
	.radio {
		min-height: 20px !important;
	}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
	ob_start();
?>
<script src="/scripts/validation/js/formValidation.js"></script>
<script src="/scripts/validation/js/bootstrap.js"></script>
<script src="/scripts/filestyle/bootstrap-filestyle.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$page = (int) @$_GET["page"]  ;
	$smarty->assign("title", "รายการรออนุมัติ" );

	ob_start();
	
	$smarty->assign("breadcrumb", array("รายการรออนุมัติ" => "#"));
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">รายการรออนุมัติ</h4>
			</div>
			<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th class="text-center">เลขที่คำขอกู้</th>
						<th class="text-center">วันที่ขออนุมัติ</th>
						<th class="text-center">ชื่อ-สกุล</th>
						<th class="text-center">ประเภท</th>
						<th class="text-center">ยอดขอกู้</th>
						<th class="text-center">วันที่ส่งคำร้อง</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				<tbody>
					<?php					
					$loan_application_type = array();
					$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
					$rs_type = $mysqli->query($sql_type);
					while($row_type = $rs_type->fetch_assoc()){
						$loan_application_type[$row_type['id']] = $row_type['loan_name'];
					}	
					
					$where = '';
					if($_SESSION['ADMIN_PER']=='1'){
						$where = '';
					}else{
						$where = " AND approver_id = '".$_SESSION['ADMIN_PER']."' ";
					}
						$sql_loan = "SELECT 
							t1.*,
							t2.loan_number,
							t2.loan_date,
							t2.send_to_approver_date
						FROM 
							loan_application t1
							INNER JOIN cmp_admin_loan_money t2 ON t1.loan_id = t2.loan_id
						WHERE 
							1=1 AND loan_status = '2' ".$where." ORDER BY t2.send_to_approver_date DESC";
						$rs_loan = $mysqli->query($sql_loan);
					
						//echo $sql_loan.'<hr>';
						/*$sql_admin = "SELECT * FROM loan_admin WHERE admin_department_id IN ('1','2')";
						$rs_admin = $mysqli->query($sql_admin);
						$row_admin = $rs_admin->fetch_assoc();
						*/
						//$loan_application_type = array('1'=>'สามัญ', '3'=>'ฉุกเฉิน');
							?>
								<script>
								</script>
							<?php 
						while($row_loan = $rs_loan->fetch_assoc()){
							
					?>
					<tr>
						<td class="text-center"><?php echo $row_loan['loan_number']; ?></td>
						<td class="text-center"><?php echo ($row_loan['send_to_approver_date'] != "") ? date_time($row_loan["send_to_approver_date"], true, true, true) : "" ;?></td>
						<td class="text-center"><?php echo $row_loan['member_name']; ?></td>
						<td class="text-center"><?php echo $loan_application_type[$row_loan['loan_application_type']]; ?></td>
						<td class="text-center"><?php echo number_format($row_loan['loan_money'],2); ?></td>
						<td class="text-center"><?php echo ($row_loan['loan_date'] != "") ? date_time($row_loan["loan_date"], true, true, true) : ""; ?></td>
						<td class="text-center">
						<?php

							$sql_approve = "SELECT * FROM loan_person_approve WHERE responsible_person_id = '".$_SESSION['ADMIN_ID']."' AND loan_application_id  = '".$row_loan['loan_application_id']."'";
							$rs_approve = $mysqli->query($sql_approve);
							$row_approve = $rs_approve->fetch_assoc();
							?>
							<script>
							</script>
							<?php 
						
							//จำนวนผู้นุมัติ จากการตั้งค่าเงินกู้
							$sql_num = "SELECT approve_num FROM loan_setting_detail WHERE id = '".$row_loan['loan_application_type']."'";
							$rs_num = $mysqli->query($sql_num);
							$row_num = $rs_num->fetch_assoc();
							$approve_num = $row_num['approve_num'];
							
							//จำนวนผู้อนุมัติ ที่อนุมัติ และไม่อนุมัติ
							$sql_num_person = "SELECT COUNT(id) AS num_person_approve FROM loan_person_approve 
										WHERE loan_application_type_id = '".$row_loan['loan_application_type']."' AND loan_application_id = '".$row_loan['loan_application_id']."'";
							$rs_num_person = $mysqli->query($sql_num_person);
							$row_num_person = $rs_num_person->fetch_assoc();
							$num_person_approve = $row_num_person['num_person_approve'];

							if($approve_num == $num_person_approve){
								$message = 'ผู้อนุมัติพิจารณาคำร้องที่ '.$row['loan_number'].' แล้ว';
								//send_sms($row['admin_tel'],$message);	
							}else{
								$message = '';
							}
							// echo $row_approve.'/'.$approve_num.'/'.$num_person_approve.'/'.$row_loan['loan_application_type'].'/'.$row_loan['loan_application_id'];
							if(empty($row_approve) & $approve_num >= $num_person_approve){ 
						?>
                              </button>
							<input type="button" class="btn btn-primary" onclick="open_consider(
							'<?php echo $row_loan['loan_id']; ?>',
							'<?php echo $row_loan['loan_number']; ?>',
							'<?php echo ($row_loan['send_to_approver_date'] != "") ? date_time($row_loan["send_to_approver_date"], true, true, true) : "" ;?>',
							'<?php echo $row_loan['member_name']; ?>',
							'<?php echo $loan_application_type[$row_loan['loan_application_type']]; ?>',
							'<?php echo number_format($row_loan['loan_money']); ?>',
							'<?php echo ($row_loan['loan_date'] != "") ? date_time($row_loan["loan_date"], true, true, true) : ""; ?>',
							'<?php echo number_format($row_loan['loan_money']); ?>',
							'<?php echo $row_loan['loan_application_id']; ?>',
							'<?php echo $row_loan['loan_application_type']; ?>',
							'<?php echo $row_loan['member_id']; ?>'
							)" value="พิจารณาคำร้อง">
						<?php } ?>
						</td>
					</tr>	
					<?php } ?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="consider_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="mySmallModalLabel">พิจารณาคำร้อง</h4>
			</div>
		<form action="" id="form1" method="POST">
		<input type="hidden" name="loan_id" id="loan_id">
			<div class="modal-body">	
				<div class="row div_approve">
					<div class="form-group show_approve">
					</div>
				</div>
				<!---->
				<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label">เลขที่คำขอกู้</label>
						<div class="col-sm-9" id="loan_number"></div>
						<input type="hidden" id="max_loan_money">
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label">วันที่ขออนุมัติ</label>
						<div class="col-sm-9" id="send_to_approver_date"></div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label">ชื่อ - สกุล</label>
						<div class="col-sm-9" id="member_name"></div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label">ยอดขอกู้</label>
						<div class="col-sm-9" id="loan_money"></div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label">วันที่ส่งคำร้อง</label>
						<div class="col-sm-9" id="loan_date"></div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9" id="show_link"></div>
					</div>
				</div>
				<div class="row" style="padding-bottom:5px">
					<div class="form-group">
						<label class="col-sm-3 control-label">ผลการพิจารณา</label>
						<div class="col-sm-9">
							<input type="radio" class="consider_radio" style="margin-left: -10px;" id="consider_result_1" name="consider_result" value='1'> อนุมัติ ( ยอดเงินที่อนุมัติ <input type="text" name="approve_money" id="approve_money" onkeyup="format_the_number(this)" class="form-inline"> )
						</div>
					</div>
				</div>
				<!--<?php if($_SESSION['ADMIN_PER']!='6'){?>
				<div class="row" style="padding-bottom:5px">
					<div class="form-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<input type="radio" class="consider_radio" style="margin-left: -10px;" id="consider_result_3" name="consider_result" value='3'> ส่งคณะกรรมการเงินกู้ เหตุผล <input type="text" name="send_forward_reason" id="send_forward_reason" class="form-inline"> 
						</div>
					</div>
				</div>
				<?php } ?>
				-->
				<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<input type="radio" class="consider_radio" style="margin-left: -10px;" id="consider_result_2" name="consider_result" value='2'> ไม่อนุมัติ  เหตุผล <input type="text" name="not_approve_reason" id="not_approve_reason" class="form-inline"> 
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" onclick="check_form()">ยืนยัน</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
			</div>
		</form>
		</div>
	</div>
</div>

<?php
	$content = ob_get_contents();
	ob_end_clean();


$smarty->assign("content", $content);
$smarty->display(THEME.".tpl");
?>
<script>	

	
		
	function open_consider(loan_id,loan_number,send_to_approver_date,member_name,loan_application_type,loan_money,loan_date,approve_money,loan_application_id,loan_application_type_id,member_no){
		$('#loan_id').val(loan_id);
		$('#loan_number').html(loan_number);
		$('#send_to_approver_date').html(send_to_approver_date);
		$('#member_name').html(member_name);
		$('#loan_application_type').html(loan_application_type);
		$('#loan_money').html(loan_money+' บาท');
		$('#max_loan_money').val(approve_money);
		$('#approve_money').val(approve_money);
		$('#loan_date').html(loan_date);
		var loan_link = '';
		var encode_loan_application_id = window.btoa('loan_application_id='+loan_application_id);
		var encode_loan_id = window.btoa('loan_id='+loan_id);
		//if(loan_application_type_id == '1'){
		//	loan_link+='<a target="_blank" href="../loan_petition_pdf.php?'+encode_loan_application_id+'">ดูคำร้องขอกู้</a>';
		//}else if(loan_application_type_id == '3'){
		//	loan_link+='<a target="_blank" href="../loan_emergent_pdf.php?'+encode_loan_id+'">ดูคำร้องขอกู้</a>';
		//}
		
		//loan_link+='<a target="_blank" href="../loan_pdf.php?'+encode_loan_id+'">ดูคำร้องขอกู้</a>';
		loan_link+='<a target="_blank" href="../loan_all_pdf.php?'+encode_loan_application_id+'">ดูคำร้องขอกู้</a>';
		loan_link+=',<a target="_blank" href="../loan_guarantee_person_pdf.php?'+encode_loan_application_id+'">ดูสัญญาค้ำประกันเงินกู้</a>';
		loan_link+=',<a href="profile_member.php?member_no='+member_no+'" target="_blank">ดูข้อมูลผู้กู้</a>';
		
		$('#show_link').html(loan_link);
		
		show_list_approve(loan_application_id,loan_id,loan_application_type_id);
		
		$('#consider_modal').modal('show');
	}
	function check_form(){
		var text_alert = '';
		var i=0;
		$('.consider_radio').each(function(){
			if($(this).is(':checked')){
				i++;
			}
		});
		
		if(i==0){
			text_alert += 'กรุณาระบุเหตุผล\n';
		}
		
		if($('#consider_result_1').is(':checked')){
			if($('#approve_money').val()==''){
				text_alert += 'กรุณาระบุจำนวนเงินที่ต้องการอนุมัติ\n';
			}
			
			var max_loan_money = $('#max_loan_money').val();
			var approve_money = $('#approve_money').val();
			max_loan_money = parseFloat(max_loan_money.replace(/,/g, ''));
			approve_money = parseFloat(approve_money.replace(/,/g, ''));
			
			//  console.log(approve_money);
		    //  console.log(max_loan_money);


			if(approve_money > max_loan_money){
				text_alert += 'ท่านไม่สามารถอนุมัติเงินกู้มากกว่าจำนวนเงินที่ขอกู้ได้\n';
			}
		}
		if($('#consider_result_2').is(':checked')){
			if($('#not_approve_reason').val()==''){
				text_alert += 'กรุณาระบุเหตุผลที่ไม่อนุมัติ\n';
			}
		}
		
		
		
		if(text_alert != ''){
			swal(text_alert,'','warning');
			//swal('เกิดข้อผิดพลาด',text_alert,'warning');

		}else{
			$('#form1').submit();
		}
	}

		
		
function format_the_number(ele){
	var value = $('#'+ele.id).val();
	if(value!=''){
		value = value.replace(',','');
		value = parseInt(value);
		value = value.toLocaleString();
		if(value == 'NaN'){
			$('#'+ele.id).val('');
		}else{
			$('#'+ele.id).val(value);
		}
	}else{
		$('#'+ele.id).val('');
	}
}

function show_list_approve(loan_application_id,loan_id,loan_application_type_id){
	//
	$.ajax({
		type	: 'POST',
		url	: 'loan_approve.php',
		data	: {
			'do'	 : 'check_approve',
			'loan_application_id' : loan_application_id,
			'loan_id' : loan_id,
			'loan_application_type' : loan_application_type_id
		},
		success : function(response) {
			//console.log(response);
			if(response.trim() != ''){
				var list_approve = '';
				list_approve+='<label class="col-sm-12 control-label">'+response+'</label>';
				$('.div_approve').show();
				$('.show_approve').html(list_approve);
			}else{
				$('.div_approve').hide();
			}
		}
	});
}

</script>
