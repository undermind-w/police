<?php
	include "config.inc.php";
	//require PATH."/class/connect_odbc.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	//function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}	
	
	$url_img = $_GET['img'];
	$rest = substr($url_img, -3);
	// echo $rest;
	// exit;
	if($rest =='PDF' || $rest =='pdf'){
		$pdf = new FPDI('L','mm','A4');
		$filename = $url_img ;
		$pageCount = $pdf->setSourceFile($filename);
		for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	

				$pdf->AddPage();
				$tplIdx = $pdf->importPage($pageNo); 
				$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);

				$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
				$pdf->SetFont('THSarabunNew', '', 13 );



		//$pdf->Image($url_img,45,30,220,'JPG');

		//$pdf->Image(HTTP.$_SERVER["HTTP_HOST"].'/uploads/watermrk2.png',1,1,300,'PNG');
		}
	}else{
		$pdf = new FPDI();
	
				$pdf->AddPage();

				$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
				$pdf->SetFont('THSarabunNew', '', 13 );

		$pdf->Image($url_img,0,0,220,'JPG');

		//$pdf->Image(HTTP.$_SERVER["HTTP_HOST"].'/uploads/watermrk2.png',1,1,220,'PNG');
	}

	$pdf->Output();