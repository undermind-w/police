<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

session_start();
require "config.inc.php";
error_reporting(E_ALL | E_STRICT);
require PATH.'/scripts/blueimp.fileupload/9.9.3/php/UploadHandler.php';
$options = array(
    // This option will disable creating thumbnail images and will not create that extra folder.
    // However, due to this, the images preview will not be displayed after upload
    'upload_dir' => PATH.'/uploads/tmp/'
		, 'upload_url' => $_SERVER["HTTP_HOST"].'/coop/uploads/tmp/'
    , 'image_versions' => array()
);
$upload_handler = new UploadHandler($options);