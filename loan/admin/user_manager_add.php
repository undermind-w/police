<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');
require "config.inc.php" ;
require "session.inc.php" ;
require "menu.inc.php" ;

if($_POST){
	if($_POST['check_tel']=='1'){
		$sql = "SELECT admin_id FROM loan_admin WHERE admin_username = '".$_POST['admin_tel']."'";
		$rs = $mysqli->query($sql);
		$row = $rs->fetch_assoc();
		
		if(!empty($row)){
			echo "error";
		}else{
			echo "success";
		}
		exit;
	}else{
		//echo"<pre>";print_r($_POST);exit;
		if($_POST['admin_id']==''){
			$sql = "INSERT INTO loan_admin SET
				admin_name = '".$_POST['admin_name']."',
				admin_department_id = '".$_POST['admin_department_id']."',
				admin_tel = '".$_POST['admin_tel']."',
				admin_username = '".$_POST['admin_tel']."',
				admin_password = '".$_POST['admin_password']."',
				admin_status = '1',
				createdate = NOW()
			";
		}else{
			$sql = "UPDATE loan_admin SET
				admin_name = '".$_POST['admin_name']."',
				admin_department_id = '".$_POST['admin_department_id']."',
				admin_tel = '".$_POST['admin_tel']."',
				admin_username = '".$_POST['admin_tel']."',
				";
				
			if($_POST['admin_password']!=''){
				$sql .= " admin_password = '".$_POST['admin_password']."', ";
			}
			$sql .= " updatedate = NOW()";
			$sql .= " WHERE admin_id = '".$_POST['admin_id']."'";
		}
		$rs = $mysqli->query($sql);
		echo "<script> document.location.href = 'user_manager.php'</script>";
	}
	
}

ob_start();
?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
<style>
	.label-warning, .btndel {
		cursor: pointer;
	}
	.btn-success2 {
		background: #4FC3F7;
		color: #fff;
		padding: 6px 35px;
	}
	.resalt {
		clear: both;
		margin-top: 40px;
	}
	.blgres {
		margin-top: 30px;
	}
	.bt_dis{
		cursor: not-allowed !important;
		pointer-events: auto !important;
	}
	.not_line{
		text-decoration: none !important;
	}

	.section {
		clear: both;
		padding: 0px;
		margin: 0px;
	}

	/*  COLUMN SETUP  */
	.col {
		display: block;
		float:left;
		margin: 1% 0 1% 1%;
	}
	.col:first-child { margin-left: 0; }

	/*  GROUPING  */
	.group:before,
	.group:after { content:""; display:table; }
	.group:after { clear:both;}
	.group { zoom:1; /* For IE 6/7 */ }
	/*  GRID OF FIVE  */
	.span_5_of_5 {
		width: 100%;
	}
	.span_4_of_5 {
		width: 79.8%;
	}
	.span_3_of_5 {
		width: 59.6%;
	}
	.span_2_of_5 {
		width: 39.4%;
	}
	.span_1_of_5 {
		/* width: 19.2%; */
		width: 24.2%;
	}

	/*  GO FULL WIDTH BELOW 480 PIXELS */
	@media only screen and (max-width: 480px) {
		.col {  margin: 1% 0 1% 0%; }
		.span_1_of_5, .span_2_of_5, .span_3_of_5, .span_4_of_5, .span_5_of_5 { width: 100%; }
	}
</style>
<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
	ob_start();
?>
<script src="/scripts/validation/js/formValidation.js"></script>
<script src="/scripts/validation/js/bootstrap.js"></script>
<script src="/scripts/filestyle/bootstrap-filestyle.js"></script>
<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
<!-- <script src="/html/meter/admin1/assets/js/pages/dashboard.js"></script> -->
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script>

</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$page = (int) @$_GET["page"]  ;
	$smarty->assign("title", "เพิ่มผู้ใช้งาน" );

	ob_start();
	
	$sql = "SELECT * FROM loan_admin WHERE admin_id = '".$_GET['admin_id']."'";
	$rs = $mysqli->query($sql);
	$row = $rs->fetch_assoc();
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading clearfix">
				<h4 class="panel-title">ผู้ใช้งาน > เพิ่มผู้ใช้งาน</h4>
			</div>
			<div class="panel-body">
				<form action="" method="POST" id="form1">
				<input type="hidden" name="admin_id" id="admin_id" value="<?php echo $row['admin_id']; ?>">
					<div class="text-left">
						<div class="col-sm-12">
							<p class="col-sm-2">ชื่อผู้ใช้งาน : </p>
							<p class="col-sm-4"><input type="text" class="form-control" name="admin_name" id="admin_name" value="<?php echo $row['admin_name']; ?>"></p>
						</div>
						<div class="col-sm-12">
							<p class="col-sm-2">ฝ่าย : </p>
							<p class="col-sm-4">
								<select class="form-control" name="admin_department_id" id="admin_department_id">
									<option value="">เลือกฝ่าย</option>
									<?php 
										$sql_admin_department = "SELECT * FROM cmp_admin_department";
										$rs_admin_department = $mysqli->query($sql_admin_department);
										while($row_admin_department = $rs_admin_department->fetch_assoc()){
									?>
									<option value="<?php echo $row_admin_department['admin_department_id']; ?>" <?php echo $row['admin_department_id']==$row_admin_department['admin_department_id']?'selected':''; ?>><?php echo $row_admin_department['admin_department']; ?></option>
									<?php		
										}
									?>
								</select>
							</p>
						</div>
						<div class="col-sm-12">
							<p class="col-sm-2">หมายเลขโทรศัพท์มือถือ : </p>
							<p class="col-sm-4"><input type="text" class="form-control" name="admin_tel" id="admin_tel" onKeyPress="CheckNum()" maxlength='10' value="<?php echo $row['admin_tel']; ?>"></p>
							<input type="hidden" id="admin_tel_old" value="<?php echo $row['admin_tel']; ?>">
							<p class="col-sm-3">ใช้เป็น username เข้าระบบ</p>
						</div>
						<div class="col-sm-12">
							<p class="col-sm-2">รหัสผ่าน : </p>
							<p class="col-sm-4"><input type="password" class="form-control" name="admin_password" id="admin_password"></p>
						</div>
						<div class="col-sm-12">
							<p class="col-sm-2">ยืนยันรหัสผ่าน : </p>
							<p class="col-sm-4"><input type="password" class="form-control" id="admin_password2"></p>
						</div>
					</div>
					<div class="text-center">
						<input style="width: 111px;" type="button" class="btn btn-info" onclick="check_form()" value="บันทึก">
					</div>
				</form>			
			</div>
		</div>
	</div>
</div>
<script>
	function CheckNum(){
		if (event.keyCode < 48 || event.keyCode > 57){
		      event.returnValue = false;
	    	}
	}
	function check_form(){
		var text_alert = '';
		if($('#admin_name').val() == ''){
			text_alert += '- ชื่อผู้ใช้งาน\n';
		}
		if($('#admin_department_id').val() == ''){
			text_alert += '- ฝ่าย\n';
		}
		if($('#admin_tel').val() == ''){
			text_alert += '- หมายเลขโทรศัพท์มือถือ\n';
		}
		if($('#admin_password').val() == '' && $('#admin_id').val() == ''){
			text_alert += '- รหัสผ่าน\n';
		}
		if($('#admin_password').val() != $('#admin_password2').val()){
			text_alert += '- การยืนยันรหัสผ่านไม่ตรงกัน\n';
		}
		if(text_alert != ''){
			swal('กรุณากรอกข้อมูลต่อไปนี้',text_alert,'warning');
		}else{
			if($('#admin_tel_old').val() != $('#admin_tel').val()){
				$.ajax({
					type: "POST"
					, url: 'user_manager_add.php'
					, data: {check_tel:'1', admin_tel:$('#admin_tel').val()}
					, success: function(data) {
						if(data == 'success'){
							$('#form1').submit();
						}else{
							swal('เกิดข้อผิดพลาก','หมายเลขโทรศัพท์ซ้ำกับชื่อผู้ใช้ในระบบ','warning');
						}
					}
				});
			}else{
				$('#form1').submit();
			}
			
			
		}
	}
</script>
<?php
	$content = ob_get_contents();
	ob_end_clean();


$smarty->assign("content", $content);
$smarty->display(THEME.".tpl");
