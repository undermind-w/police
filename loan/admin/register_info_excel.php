<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");
	?>
	
<?php
	$where = '1=1';
	if($_GET['search']!=''){
		$where .= " AND (t1.member_no LIKE '%".$_GET['search']."%' OR t1.memname LIKE '%".$_GET['search']."%')";
	}
	if($_GET['date_start']!=''){
		$date_start_arr = explode('/',rawurldecode($_GET['date_start']));
		$date_start = ($date_start_arr[2])."-".sprintf("%02d",$date_start_arr[1])."-".sprintf("%02d",$date_start_arr[0]);
	}
	if($_GET['date_end']!=''){
		$date_end_arr = explode('/',rawurldecode($_GET['date_end']));
		$date_end = ($date_end_arr[2])."-".sprintf("%02d",$date_end_arr[1])."-".sprintf("%02d",$date_end_arr[0]);
	}
	if($_GET['date_start'] != '' && $_GET['date_end'] != ''){
		$where .= " AND t2.create_time BETWEEN '".$date_start."' AND '".$date_end."' ";
	}else if($_GET['date_start'] == '' && $_GET['date_end'] != ''){
		$where .= " AND t2.create_time <= '".$date_end."' ";
	}else if($_GET['date_start'] != '' && $_GET['date_end'] == ''){
		$where .= " AND t2.create_time >= '".$date_start."' ";
	}
	$sql = "SELECT * FROM cmp_imp_member t1
			INNER JOIN cmp_imp_member_data t2 ON t1.member_no = t2.member_no 
			WHERE ".$where."
			ORDER BY t2.create_time DESC";
	$rs = $mysqli->query($sql);
	echo $mysqli->error;
?>
<div id="exportData">
	<table class="table table-bordered" border='1'>
		<thead>
			<tr>
				<th class="text-center" width="175" >วันที่สมัคร</th>
				<th class="text-center"  width="150">รหัสสมาชิก</th>
				<th class="text-center" width="200">ชื่อสกุล</th>
				<th class="text-center"  width="100" >หมายเลขโทรศัพท์</th>
				<th class="text-center"  width="100" >สมัครผ่าน</th>
			</tr>
		</thead>
		<tbody>
		<?php while(($row = $rs->fetch_assoc())) {
			$sql = "SELECT *
							FROM cmp_imp_member_login_session
							WHERE member_no = '{$row["member_no"]}'
							ORDER BY create_time DESC
							LIMIT 1" ;
			$rs_session = $mysqli->query($sql);
			$row_session = $rs_session->fetch_assoc();
			?>
			<tr>
				<td class="text-center"><?php echo ConvertToThaiDate($row["create_time"], true); ?></td>
				<td class="text-center"><?php echo (string)$row["member_no"]; ?><font color="white">'</font></td>
				<td class=""><?php echo (string)$row["memname"]; ?></td>
				<td class="text-center"><?php echo (string)$row['telmobile'] ?><font color="white">'</font></td>
				<td class="text-center"><?php echo $row_session["login_type"] ?><font color="white">'</font></td>
			</tr>
		<?php
			}
		?>
		</tbody>
	</table>
</div>
<script src="/html/meter/admin1/assets/plugins/jquery/jquery-2.1.3.min.js"></script>
<script>
    function exportExcel(){
            var headContent = $('head').html();
            var bodyContent = $('#exportData').html();
            $.ajax({
                method: "POST",
                url: "/html2doc/html2doc_setContent.php",
                data: {htmlHead : headContent, htmlBody : bodyContent, apptype : 'application/vnd.ms-excel', filetype : 'xls', exportData : 'on', filename : '', logFile : '', logPathFile : ''}
            }) .done(function( msg ) {
                if($.trim(msg) == 'ok'){
                    window.open('/html2doc/html2doc.php','_blank');
                    window.close();
                }
            });
    }
</script>
<script>
    $(document).ready(function(){
        exportExcel();
    });
</script>