<?php 
	include "config.inc.php";
	$deposit_account ='';
	if(!empty($_POST['deposit'])){
		foreach($_POST['deposit'] as $key => $value){
			$deposit_account .= $value.'/';
		}
	}

	function create_file_name($output_dir,$file_name){
		$list_dir = array();
		$cdir = scandir($output_dir);
		foreach ($cdir as $key => $value) {
			if (!in_array($value,array(".",".."))) {
				if (@is_dir(@$dir . DIRECTORY_SEPARATOR . $value)){
					$list_dir[$value] = dirToArray(@$dir . DIRECTORY_SEPARATOR . $value);
				}else{
					if(substr($value,0,8) == date('Ymd')){
						$list_dir[] = $value;
					}
				}
			}
		}
		$explode_arr=array();
		foreach($list_dir as $key => $value){
			$task = explode('.',$value);
			$task2 = explode('_',$task[0]);
			$explode_arr[] = $task2[1];
		}
		$max_run_num = sprintf("%04d",count($explode_arr)+1);
		$explode_old_file = explode('.',$file_name);
		$new_file_name = date('Ymd')."_".$max_run_num.".".$explode_old_file[(count($explode_old_file)-1)];
		return $new_file_name;
	}
	
	//gen เลขคำขอกู้
	$sql_number = "SELECT loan_number
						FROM cmp_admin_loan_money
						ORDER BY loan_number DESC
						LIMIT 1";
			$rs_number = $mysqli->query($sql_number);
			if($row_number = $rs_number->fetch_assoc()) {
				$id = (int)$row_number["loan_number"];
				$loan_number = sprintf("%06d", $id + 1);
			}else{
				$loan_number = sprintf("%06d", 1);
			}

	$member_id = $mysqli->real_escape_string($_POST['member_id']);
	$member_name = $mysqli->real_escape_string($_POST['member_name']);
	
	$sql = "SELECT
				t1.member_id AS member_no,
				t1.apply_date ,
				t1.mobile AS telmobile,
				t1.id_card AS card_person,
				CONCAT(
					t2.prename_full,
					t1.firstname_th,
					' ',
					t1.lastname_th
				) AS memname,
				email,
				share_month,
				'' AS position_desc,
				'' AS membgroup_desc,
				'' AS member_sector_name,
				'' AS member_department_name
			FROM
				coop_mem_apply AS t1
			LEFT JOIN coop_prename AS t2 ON t1.prename_id = t2.prename_id
			WHERE
				t1.member_id = '{$member_id}'";
	//echo $sql.'<hr>';			
	$rs = $mysqli_app->query($sql);
	$row_member = $rs->fetch_assoc();	
	
	
	if($_POST['telmobile']!=''){
		$telmobile = $mysqli->real_escape_string($_POST['telmobile']);
	}else{
		$telmobile = $mysqli->real_escape_string($row_member['telmobile']);
	}
	$loan_application_type = $mysqli->real_escape_string($_POST['loan_application_type']);
	if($_POST['email']!=''){
		$email = $mysqli->real_escape_string($_POST['email']);
	}else{
		$email = $mysqli->real_escape_string($row_member['email']);
	}
	$department_name = $mysqli->real_escape_string($_POST['department_name']);
	$department_head = $mysqli->real_escape_string($_POST['department_head']);
	$department_head_tel = $mysqli->real_escape_string($_POST['department_head_tel']);
	if($_POST['loan_id']!=''){
		$sql="UPDATE cmp_admin_loan_money SET
			loan_date = NOW(),
			member_id = '".$member_id."',
			member_name = '".$member_name."',
			telmobile = '".$telmobile."',
			loan_application_type = '".$loan_application_type."',
			email = '".$email."',
			loan_number = '".$loan_number."',
			department_name = '".$department_name."',
			department_head = '".$department_head."',
			department_head_tel = '".$department_head_tel."'
		WHERE loan_id = '".$_POST['loan_id']."'
		";
	}else{
	 $sql = "INSERT INTO cmp_admin_loan_money (
			 loan_date,
			 member_id,
			 member_name,
			 telmobile,
			 loan_application_type ,
			 email,
			 loan_number,
			 department_name,
			 department_head,
			 department_head_tel
	      ) VALUES (
		      NOW(),
		      '$member_id',
		      '$member_name',
		      '$telmobile',
		       $loan_application_type,
		      '$email',
		      '$loan_number',
		      '$department_name',
		      '$department_head',
		      '$department_head_tel'
	       )";
	}
	
	if (!$mysqli->query($sql)) {
		echo "false";
		exit();
	}
	
	if($_POST['loan_id']!=''){
		$lastid = $_POST['loan_id'];
	}else{
		$lastid = $mysqli->insert_id;
	}
	
	$data = array();
	$data['loan_application']['loan_id'] = $lastid;
	$data['loan_application']['loan_application_type'] = $_POST['loan_application_type'];
	$data['loan_application']['write_at'] = COOP_NAME_TH;
	$data['loan_application']['employee_id'] = $_POST['employee_id'];
	$data['loan_application']['money_receive_type'] = $_POST['money_receive_type']; 
	$data['loan_application']['cost_of_living'] = $_POST['cost_of_living']; 	
	$data['loan_application']['member_name'] = $row_member['memname'];
	$data['loan_application']['member_id'] = $member_id;
	$data['loan_application']['application_date'] = date('Y-m-d H:i:s');
	$data['loan_application']['member_position'] = $row_member['position_desc'];
	
	//ข้อมูลที่อยู่
	$data_addrc = '';
	/*
	$sql_addrc = "SELECT * FROM cmp_imp_address  WHERE member_no = '".$member_id."' LIMIT 1 ";
	  $rs_addrc = $mysqli->query($sql_addrc);
	  echo $mysqli->error;
	  $row_addrc = $rs_addrc->fetch_assoc();
	  $data_addrc .= $row_addrc["memb_addr"];
	  $data_addrc .= !empty($row_addrc["addr_group"]) ? " หมู่ {$row_addrc["addr_group"]} " : "" ;
	  $data_addrc .= !empty($row_addrc["soi"]) ? " ซ. {$row_addrc["soi"]} " : "" ;
	  $data_addrc .= !empty($row_addrc["mooban"]) ? " หมู่บ้าน {$row_addrc["mooban"]} " : "" ;
	  $data_addrc .= !empty($row_addrc["road"]) ? " ถ. {$row_addrc["road"]} " : "" ;
	  $data_addrc .= !empty($row_addrc["tambol"]) ? " ต. {$row_addrc["tambol"]} " : "" ;
	  $data_addrc .= !empty($row_addrc["district_desc"]) ? " อ. {$row_addrc["district_desc"]} " : "" ;
	  $data_addrc .= !empty($row_addrc["province_desc"]) ? " จ. {$row_addrc["province_desc"]} " : "" ;
	  $data_addrc .= !empty($row_addrc["postcode"]) ? "{$row_addrc["postcode"]} " : "" ;
	*/
	
	$data['loan_application']['address'] = $data_addrc;
	$data['loan_application']['card_id'] = $row_member['card_person'];
	$data['loan_application']['member_branch'] = '1'; //กลุ่มงาน / สาขา (เลือก)
	$data['loan_application']['member_branch_name'] = $row_member['membgroup_desc']; 
	$data['loan_application']['member_sector'] = ''; //ส่วน / เขต
	$data['loan_application']['member_sector_name'] = $_POST['member_sector_name'];
	$data['loan_application']['member_department'] = '';
	$data['loan_application']['member_department_name'] = $_POST['member_department_name'];
	$data['loan_application']['member_tel'] = $_POST['member_tel']!=''?$_POST['member_tel']:$row_member['telmobile']; 
	$data['loan_application']['email'] = $_POST['email']; 
	
	////ข้อมูลที่อยู่
	$data_addr_card_id = '';
	/*
		$data_addr_card_id .= $row_addrc["memb_addrc"];
		$data_addr_card_id .= !empty($row_addrc["addr_groupc"]) ? " หมู่ {$row_addrc["addr_groupc"]} " : "" ;
		$data_addr_card_id .= !empty($row_addrc["soic"]) ? " ซ. {$row_addrc["soic"]} " : "" ;
		$data_addr_card_id .= !empty($row_addrc["moobanc"]) ? " หมู่บ้าน {$row_addrc["moobanc"]} " : "" ;
		$data_addr_card_id .= !empty($row_addrc["roadc"]) ? " ถ. {$row_addrc["roadc"]} " : "" ;
		$data_addr_card_id .= !empty($row_addrc["tambolc"]) ? " ต. {$row_addrc["tambolc"]} " : "" ;
		$data_addr_card_id .= !empty($row_addrc["district_descc"]) ? " อ. {$row_addrc["district_descc"]} " : "" ;
		$data_addr_card_id .= !empty($row_addrc["province_descc"]) ? " จ. {$row_addrc["province_descc"]} " : "" ;
		$data_addr_card_id .= !empty($row_addrc["postcodec"]) ? "{$row_addrc["postcodec"]} " : "" ;
	*/
	$data['loan_application']['address_card_id'] = $data_addr_card_id; 
	//$data['loan_application']['member_salary'] = $_POST['member_salary'];//$row_member['salary_amount']; 
	$data['loan_application']['member_salary'] = str_replace(',','',$_POST['salary']);//$row_member['salary_amount']; 
	$data['loan_application']['loan_money'] = str_replace(',','',$_POST['money']); //เงินที่ขอกู้
	$data['loan_application']['loan_money_text'] = convert($_POST['money']); //เงินที่ขอกู้เป็นตัวอักษร
	$data['loan_application']['member_use'] = $_POST['money_use']; //เหตุผลที่ขอกู้
	$data['loan_application']['outstanding'] = str_replace(',','',$_POST['outstanding']); //outstanding
	$data['loan_application']['account_bank_number'] = str_replace('-','',$deposit_account);
	$data['loan_application']['max_money_loan'] = str_replace(',','',$_POST['share_accu']);

	//ข้อมูลหุ้น
	$sql_money_share = "SELECT share_collect_value, share_collect, member_id FROM coop_mem_share WHERE member_id = '{$member_no}' ORDER BY share_date DESC,share_id DESC limit 1  ";
	$rs_money_share = $mysqli_app->query($sql_money_share);
	$row_money_share = $rs_money_share->fetch_assoc();
	$sum_share  =  $row_money_share['share_collect_value'];
	/*
	$sql_share = "SELECT * FROM cmp_imp_share_master  WHERE member_no = '".$member_id."'";
	$rs_share = $mysqli->query($sql_share);
	echo $mysqli->error;
	$row_share = $rs_share->fetch_assoc();
	*/
	
	$data['loan_application']['member_share'] = $row_share['share_collect']; //จำนวนหุ้นที่มีในสหกรณ์
	$data['loan_application']['member_money'] = $row_share['share_collect_value']; //มูลค่าหุ้นหุ้นที่มีในสหกรณ์
	$data['loan_application']['share_rate'] = $row_member['share_month']; //ค่าหุ้นรายเดือน
	
	///////////////////////////////////ข้าม ข้อ3
	//$data['loan_application']['loan_date'] = ''; //วันที่ของหัวข้อ ข้าพเจ้ามีหนี้สินอยู่ต่อสหกรณ์ในฐานะผู้กู้ 
	//$data['loan_application']['book_loan'] = ''; //หนังสือกู้ที่ ตำแหนง่ที่ 1
	//$data['loan_application']['book_two'] = ''; //หนังสือกู้ที่ ตำแหนง่ที่ 2
	//$data['loan_application']['loan_for'] = ''; //เหตุผลที่ขอกู้ในหนี้เก่า
	//$data['loan_application']['loan_remain'] = ''; //เงินต้นคงเหลือในหนี้เก่า
	///////////////////////////////////ข้าม ข้อ3
	
	//$data['loan_application']['insurance_type'] = ''; 
	$sql_deposit = "SELECT * FROM cmp_imp_dept_master WHERE member_no = '".$row_member['member_no']."' AND deptaccount_no = '".$_POST['deposit_no']."'";
	$rs_deposit = $mysqli->query($sql_deposit);
	echo $mysqli->error;
	$row_deposit = $rs_deposit->fetch_assoc();
	
	$sql_count_deposit = "SELECT count(deptaccount_no) as count_account FROM cmp_imp_dept_master WHERE member_no = '".$row_member['member_no']."'";
	$rs_count_deposit = $mysqli->query($sql_count_deposit);
	echo $mysqli->error;
	$row_count_deposit = $rs_count_deposit->fetch_assoc();
	
	$data['loan_application']['share_money'] = $row_share['share_collect_value']; //ทุนเรือนหุ้นที่มีอยู่ในสหกรณ์
	$data['loan_application']['deposit_type'] = $row_deposit['depttype_desc']; //ประเภทสมุดเงินฝากของสหกรณ์       
	$data['loan_application']['deposit_no'] = $_POST['deposit_no']; //เลขที่สมุดเงินฝากของสหกรณ์       
	$data['loan_application']['deposit_amount'] = $row_deposit['prncbal']; //จำนวนเงินในสมุดเงินฝากของสหกรณ์       
	$data['loan_application']['daposit_issue'] = $row_count_deposit['count_account']; //จำนวนสมุดเงินฝากของสหกรณ์       
	
	//สำหรับเจ้าหน้าที่
	$data['loan_application']['period'] = ''; //เงินต้นที่จะส่งแต่ละงวด
	$sql_interest_rate = "SELECT interest_rate FROM loan_setting WHERE loan_type = '".$loan_application_type."'";
	$rs_interest_rate = $mysqli->query($sql_interest_rate);
	$row_interest_rate = $rs_interest_rate->fetch_assoc();
	if($_POST['period'] == 0){
		$interest_rate = 0;
	}else{
		$interest_rate = $row_interest_rate['interest_rate'];
	}
	$data['loan_application']['interest_rate'] = $row_interest_rate['interest_rate']; //%ดอกเบี้ยต่อปี
	$data['loan_application']['interest_period'] = ''; //จำนวนงวดที่จะชำระ
	$data['loan_application']['account_bank'] = ''; //เลขที่บัญชีที่ให้เงินกู้เข้า
	$data['loan_application']['cost_live'] = ''; 
	$data['loan_application']['send_money'] = ''; 
	$data['loan_application']['month'] = ''; 
	$data['loan_application']['guarantor'] = ''; 
	$data['loan_application']['guarantor_id'] = ''; 
	$data['loan_application']['guarantor_tel'] = ''; 
	$data['loan_application']['application_status'] = ''; 
	$data['loan_application']['approve_time'] = ''; 
	$data['loan_application']['guarantee_person'] = $_POST['guarantee_person']; 
	$data['loan_application']['guarantee_share'] = $_POST['guarantee_share']; 
	$data['loan_application']['payment_method'] = $_POST['payment_method'];
	$data['loan_application']['guarantee_type'] = $_POST['guarantee_type'];
	$data['loan_application']['account_bank_type'] = $_POST['account_bank_type'];
	//$data['loan_application']['account_bank_number'] = $_POST['account_bank_number'];
	$data['loan_application']['estimate_money_period'] = str_replace(',','',$_POST['estimate_money_period']);
	$data['loan_application']['period'] = $_POST['period'];
	$data['loan_application']['guarantee_share_value'] = str_replace(',','',$_POST['guarantee_share_value']);
	$data['loan_application']['guarantee_position'] = $_POST['guarantee_position'];
	
	$sql_respon_person = "SELECT responsible_person_id FROM loan_application ORDER BY loan_id DESC limit 1";
	$rs_respon_person = $mysqli->query($sql_respon_person);
	$row_respon_person = $rs_respon_person->fetch_assoc();
	if($row_respon_person['responsible_person_id']!=''){
		$sql_next_respon = "SELECT admin_id,admin_tel,admin_name FROM loan_admin WHERE admin_id > ".$row_respon_person['responsible_person_id']." AND admin_department_id = '3' ORDER BY admin_id ASC limit 1";
		//echo $sql_next_respon;exit;
		$rs_next_respon = $mysqli->query($sql_next_respon);
		$row_next_respon = $rs_next_respon->fetch_assoc();
		
		if($row_next_respon['admin_id'] == ''){
			$sql_next_respon = "SELECT admin_id,admin_tel,admin_name FROM loan_admin WHERE admin_department_id = '3' ORDER BY admin_id ASC limit 1";
			$rs_next_respon = $mysqli->query($sql_next_respon);
			$row_next_respon = $rs_next_respon->fetch_assoc();
			
			$data['loan_application']['responsible_person_id'] = $row_next_respon['admin_id'];
			$admin_tel = $row_next_respon['admin_tel'];
			$admin_name = $row_next_respon['admin_name'];
		}else{
			$data['loan_application']['responsible_person_id'] = $row_next_respon['admin_id'];
			$admin_tel = $row_next_respon['admin_tel'];
			$admin_name = $row_next_respon['admin_name'];
		}
	}else{
		$sql_next_respon = "SELECT admin_id,admin_tel,admin_name FROM loan_admin WHERE admin_department_id = '3' ORDER BY admin_id ASC limit 1";
		$rs_next_respon = $mysqli->query($sql_next_respon);
		$row_next_respon = $rs_next_respon->fetch_assoc();
		
		$data['loan_application']['responsible_person_id'] = $row_next_respon['admin_id'];
		$admin_tel = $row_next_respon['admin_tel'];
		$admin_name = $row_next_respon['admin_name'];
	}
	
	//echo"<pre>";print_r($data);echo"</pre>";exit;
	if($_POST['loan_id']!=''){
		$sql_application = "UPDATE loan_application SET ";
		foreach($data['loan_application'] as $key => $value){
			$sql_application .= " ".$key."='".$value."',";
		}
		$sql_application = substr($sql_application,0,-1);
		$sql_application .= " WHERE loan_id = '".$_POST['loan_id']."'";
	}else{
		$sql_application = "INSERT INTO loan_application SET ";
		foreach($data['loan_application'] as $key => $value){
			$sql_application .= " ".$key."='".$value."',";
		}
		$sql_application = substr($sql_application,0,-1);
	}
	//echo $sql_application."<br>";exit;
	$mysqli->query($sql_application);
	if($_POST['loan_id']!=''){
		$sql_app_id = "SELECT loan_application_id FROM loan_application WHERE loan_id = '".$_POST['loan_id']."'";
		$rs_app_id = $mysqli->query($sql_app_id);
		$row_app_id = $rs_app_id->fetch_assoc();
		$last_app_id = $row_app_id['loan_application_id'];
	}else{
		$last_app_id = $mysqli->insert_id;
	}
	
	$output_dir = $_SERVER['DOCUMENT_ROOT']."/uploads/loan/";
	foreach($_FILES as $key => $value){
		if($key != 'loan_application_bondsman'){
			foreach($value['name'] as $key2 => $value2){
				if($value2 == ''){continue;}
				$file_name = create_file_name($output_dir, $value2);
				copy($_FILES[$key]["tmp_name"][$key2], $output_dir.$file_name);
				$sql_file_idcard = "INSERT INTO loan_application_file SET
					loan_file_name = '".$file_name."',
					loan_file_old_name = '".$value2."',
					input_name = '".$key."',
					loan_id = '".$lastid."',
					loan_application_id = '".$last_app_id."'
				";
				$mysqli->query($sql_file_idcard);
			}
		}else{	
			foreach($value['name'] as $key2 => $value2){
				if($_POST['data']['loan_application_bondsman'][$key2]['bondsman_name']==''){continue;}
				
				//echo"<pre>";print_r($value2[$key2]);echo"</pre>";
				foreach($value2 as $key3 => $value3){
					if($value3 == ''){continue;}
					foreach($value3 as $key4 => $value4){
						if($value4 == ''){continue;}
						$file_name = create_file_name($output_dir, $value4);
						copy($_FILES[$key]["tmp_name"][$key2][value4][$key3], $output_dir.$file_name);
						$sql_file_bondsman = "INSERT INTO loan_application_file SET
							loan_file_name = '".$file_name."',
							loan_file_old_name = '".$value4."',
							input_name = '".$key3."',
							loan_id = '".$lastid."',
							loan_application_id = '".$last_app_id."',
							bondsman_num = '".$key2."'
						";
						
						$mysqli->query($sql_file_bondsman);
					}
				}
				
				/*foreach($value2['file_bondsman_salary'] as $key3 => $value3){
					if($value3 == ''){continue;}
					$file_name = create_file_name($output_dir, $value3);
					copy($_FILES[$key]["tmp_name"][$key2]['file_bondsman_salary'][$key3], $output_dir.$file_name);
					$sql_file_idcard = "INSERT INTO loan_application_file SET
						loan_file_name = '".$file_name."',
						loan_file_old_name = '".$value3."',
						input_name = 'file_bondsman_salary',
						loan_id = '".$lastid."',
						loan_application_id = '".$last_app_id."',
						bondsman_num = '".$key2."'
					";
					$mysqli->query($sql_file_idcard);
				}
				foreach($value2['file_bondsman_idcard'] as $key3 => $value3){
					if($value3 == ''){continue;}
					$file_name = create_file_name($output_dir, $value3);
					copy($_FILES[$key]["tmp_name"][$key2]['file_bondsman_idcard'][$key3], $output_dir.$file_name);
					$sql_file_idcard = "INSERT INTO loan_application_file SET
						loan_file_name = '".$file_name."',
						loan_file_old_name = '".$value3."',
						input_name = 'file_bondsman_idcard',
						loan_id = '".$lastid."',
						loan_application_id = '".$last_app_id."',
						bondsman_num = '".$key2."'
					";
					$mysqli->query($sql_file_idcard);
				}
				foreach($value2['file_bondsman_register_home'] as $key3 => $value3){
					if($value3 == ''){continue;}
					$file_name = create_file_name($output_dir, $value3);
					copy($_FILES[$key]["tmp_name"][$key2]['file_bondsman_register_home'][$key3], $output_dir.$file_name);
					$sql_file_idcard = "INSERT INTO loan_application_file SET
						loan_file_name = '".$file_name."',
						loan_file_old_name = '".$value3."',
						input_name = 'file_bondsman_register_home',
						loan_id = '".$lastid."',
						loan_application_id = '".$last_app_id."',
						bondsman_num = '".$key2."'
					";
					$mysqli->query($sql_file_idcard);
				}
				foreach($value2['file_bondsman_sk4'] as $key3 => $value3){
					if($value3 == ''){continue;}
					$file_name = create_file_name($output_dir, $value3);
					copy($_FILES[$key]["tmp_name"][$key2]['file_bondsman_sk4'][$key3], $output_dir.$file_name);
					$sql_file_idcard = "INSERT INTO loan_application_file SET
						loan_file_name = '".$file_name."',
						loan_file_old_name = '".$value3."',
						input_name = 'file_bondsman_sk4',
						loan_id = '".$lastid."',
						loan_application_id = '".$last_app_id."',
						bondsman_num = '".$key2."'
					";
					$mysqli->query($sql_file_idcard);
				}
				*/
			}
		}
	}
	
	//echo"<pre>";print_r($_POST);echo"</pre>";
	foreach($_POST['data']['loan_application_bondsman'] as $key => $value){
		/*$sql_member_bondsman = "SELECT 
			cmp_imp_member.salary_amount,
			cmp_imp_member.position_desc,
			cmp_imp_member_data.telmobile,
			cmp_imp_member.membgroup_desc
		FROM 
			cmp_imp_member 
			LEFT JOIN cmp_imp_member_data ON cmp_imp_member.member_no = cmp_imp_member_data.member_no
		WHERE 
			cmp_imp_member.member_no = '".$value['bondsman_number']."'";
			//echo $sql_member_bondsman; 
		$rs_member_bondsman = $mysqli->query($sql_member_bondsman);
		echo $mysqli->error;
		$row_member_bondsman = $rs_member_bondsman->fetch_assoc();
		*/
		if($value['bondsman_number'] != ''){
			$sql_member_bondsman = "SELECT
					t1.mobile AS telmobile,
					t1.id_card AS card_person,
					salary AS salary_amount,
					'' AS position_desc,
					'' AS membgroup_desc
				FROM
					coop_mem_apply AS t1
				LEFT JOIN coop_prename AS t2 ON t1.prename_id = t2.prename_id
				WHERE
					t1.member_id = '".$value['bondsman_number']."'";
					
			echo $sql_member_bondsman.'<hr>';			
			$rs_member_bondsman = $mysqli_app->query($sql_member_bondsman);
			$row_member_bondsman = $rs_member_bondsman->fetch_assoc();	
			//exit;
			$bondsman_tel = $value['bondsman_tel']!=''?$value['bondsman_tel']:$row_member_bondsman['telmobile'];
			$bondsman_salary = $row_member_bondsman['salary_amount'];
		}else{
			$bondsman_tel = '';
			$bondsman_salary = '';
		}
		if($_POST['loan_id']!=''){
			$sql_bondsman = "UPDATE loan_application_bondsman SET
							loan_application_id ='".$last_app_id."',
							loan_id ='".$lastid."', 
							bondsman_name ='".$value['bondsman_name']."',
							bondsman_number ='".$value['bondsman_number']."',
							bondsman_affiliation ='".$row_member_bondsman['membgroup_desc']."',
							bondsman_salary ='".$bondsman_salary."',
							bondsman_tel ='".$bondsman_tel."',
							bondsman_position ='".$row_member_bondsman['position_desc']."',
							employee_id ='".$value['employee_id']."',
							bondsman_num ='".$key."',
							bondsman_idcard ='".$value['bondsman_idcard']."'
						WHERE loan_id = '".$lastid."' AND bondsman_num = '".$key."'";
				//echo $sql_bondsman.'<hr>';		
		}else{
			$sql_bondsman = "INSERT INTO loan_application_bondsman SET
							loan_application_id ='".$last_app_id."',
							loan_id ='".$lastid."', 
							bondsman_name ='".$value['bondsman_name']."',
							bondsman_number ='".$value['bondsman_number']."',
							bondsman_affiliation ='".$row_member_bondsman['membgroup_desc']."',
							bondsman_salary ='".$bondsman_salary."',
							bondsman_tel ='".$bondsman_tel."',
							bondsman_position ='".$row_member_bondsman['position_desc']."',
							employee_id ='".$value['employee_id']."',
							bondsman_num ='".$key."',
							bondsman_idcard ='".$value['bondsman_idcard']."'";
		}
		$mysqli->query($sql_bondsman);
		//echo $sql_bondsman."<br>";
	}
		//exit;
	if($_POST['from_mobile']=='1'){
		if($_POST['special_beg']=='1'){
		$link = "loan_special_beg_mobile.php?".base64_encode("loan_id=".$lastid)."&".base64_encode("loan_application_id=".$last_app_id); 
		}else{
			$sql_delete_special_beg = "DELETE FROM loan_special_beg WHERE loan_id = '".$lastid."'";
			$mysqli->query($sql_delete_special_beg);
			$link = "loan_confirm_mobile.php?".base64_encode("loan_id=".$lastid)."&".base64_encode("loan_application_id=".$last_app_id); 
		}
	}else{
		if($_POST['special_beg']=='1'){
		$link = "loan_special_beg.php?loan_id=".$lastid."&loan_application_id=".$last_app_id; 
		}else{
			$sql_delete_special_beg = "DELETE FROM loan_special_beg WHERE loan_id = '".$lastid."'";
			$mysqli->query($sql_delete_special_beg);
			$link = "/common/".$lastid."/".$last_app_id."/true"; 
		}
	}
	//echo 'lastid='.$lastid.'<br>';
	//echo $link; exit;
	//exit;
	header('Location: '.$link);
	exit;
?>