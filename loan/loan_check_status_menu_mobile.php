<?php
	include "config.inc.php";
	require PATH . "/class/pagination.class.php"  ;
	function date_thai_format($date,$long_month='L',$with_time="1"){
		$long_month_arr = array('1'=>'มกราคม','2'=>'กุุมภาพันธ์','3'=>'มีนาคม','4'=>'เมษายน','5'=>'พฤษภาคม','6'=>'มิถุนายน','7'=>'กรกฎาคม','8'=>'สิงหาคม','9'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
		$short_month_arr = array('1'=>'ม.ค.','2'=>'ก.พ.','3'=>'มี.ค.','4'=>'เม.ย.','5'=>'พ.ค.','6'=>'มิ.ย.','7'=>'ก.ค.','8'=>'ส.ค.','9'=>'ก.ย.','10'=>'ต.ค.','11'=>'พ.ย.','12'=>'ธ.ค.');
		
		$day = date('d',strtotime($date));
		$month = date('m',strtotime($date));
		$year = date('Y',strtotime($date))+543;
		$time = date('H:i น.',strtotime($date));
		
		if($long_month == 'L'){
			$text = (int)$day." ".$long_month_arr[(int)$month]." ".$year;
		}else{
			$text = (int)$day." ".$short_month_arr[(int)$month]." ".$year;
		}
		if($with_time == '1'){
			$text .= " ".$time;
		}
		return $text;
	}
	$param = '';
	foreach($_GET as $key => $value){
		$param .= $key."&";
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	$param = substr($param,0,-1);
?>
<HTML>
<head>
 <?php
	include "header_mobile.inc.php"
 ?>
<style>
	body {
		background-color:#FFF;
	}
	.panel-heading {
		border-bottom:1px solid;
	}
	.menu_gruop{
		margin:5px;
	}
	.icon_color{
		color: rgb(235,6,141);
	}
	.menu_link:active{
		 background-color:rgb(212,212,212);;
	} 
	h3{
		margin-top:12px;
		margin-bottom:12px;
	}
	.tr_border{
		border-bottom:1px solid rgb(212,212,212);
	}
	.td_style_icon{
			padding: 10px;
		}
		.td_style{
			padding-top: 10px;
			padding-bottom: 10px;
		}
	.text_show{
		font-size: 14px;
	}
	.date_show{
		
		color: #AAA;
		font-size: 12px;
	}
	body{
		font-family:"Sukhumvit Set",sans-serif !important;
	}
</style>
</head>
<body>
<?php 
$icon = 'fa fa-chevron-circle-right fa-stack-2x text-material-light-blue-300'; 
$loan_type_arr = array();
$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
$rs_type = $mysqli->query($sql_type);
while($row_type = $rs_type->fetch_assoc()){
	$loan_type_arr[$row_type['id']] = $row_type['loan_name'];
}
?>
<table width="100%" >
<?php 
	$sql = "SELECT * , cmp_admin_loan_money.loan_date
	FROM 
		cmp_admin_loan_money 
		INNER JOIN loan_application ON cmp_admin_loan_money.loan_id = loan_application.loan_id
	WHERE cmp_admin_loan_money.member_id='".$_GET['member_no']."' AND loan_application.application_status='1' ORDER BY cmp_admin_loan_money.loan_id DESC";
	$rs = $mysqli->query($sql);
	while($row = $rs->fetch_assoc()){
?>
		<tr class="tr_border menu_link" onclick="link_click('loan_check_status_mobile.php?<?php echo base64_encode("loan_id=".$row['loan_id']); ?>')">
			<td  align="center" valign="middle" class="td_style_icon">
				<span class="icon_color fa-stack fa-1x ">
						<i class="<?php echo $icon; ?>"></i>
				</span>
			</td>
			<td width="90%" valign="middle" class="td_style">
				<span class="text_show"><?php echo $loan_type_arr[$row['loan_application_type']]; ?></span>
				<br>
				<span class="date_show"><?php echo date_thai_format($row['loan_date']); ?></span>
			</td>
		</tr>
	<?php } ?>
</table>
<div class="row">
	<div style="margin-top: 1.5em;" class="text-center">
		<div class="col-sm-12">
			<div class="col-sm-12" style="margin-bottom:15px;">
				<a onclick="link_click('loan_main_menu_mobile.php?<?php echo $param; ?>')">
					<button style="font-size: 22px;font-family: 'thaisans_neueregular' !important;width:70%" type="button" class="btn btn-material-light-blue-300">กลับหน้าหลัก</button>
				</a>
			</div>
		</div>
	</div>
</div>
<script>
function link_click(link){
	location.href = link;
}
</script>
</body>
</HTML>
