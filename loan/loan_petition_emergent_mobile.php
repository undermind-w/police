<?php
	include "config.inc.php";
	//require PATH."/class/connect_odbc.inc.php";
	require PATH . "/class/pagination.class.php"  ;
	//include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.session.inc.php";
?>
<HTML>
<head>
 <?php
	include "header_mobile.inc.php";
	
	$param = '';
	if($_GET['member_no']!=''){
		$param = "member_no=".$_GET['member_no'];
	}else{
		foreach($_GET as $key => $value){
			$param .= $key."&";
			$decode = base64_decode($key);
			$decode = explode('=',$decode);
			$_GET[$decode[0]] = $decode[1];
		}
		$param = substr($param,0,-1);
	}
 ?>
	<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
    <script src="/scripts/jquery.validate_loan.js"></script>
<style>
	.input-group[class*=col-]{
		padding-right: 15px;
		padding-left: 15px;
	}
	.icon_color{
		background-color: rgb(235,6,141);
		color: #FFF;
	}
	.error{
		color:red;
	}
	/* unvisited link */
	a:link {
		color: #FFF;
	}

	/* visited link */
	a:visited {
		color: #FFF;
	}

	/* mouse over link */
	a:hover {
		color: #FFF;
	}

	/* selected link */
	a:active {
		color: #FFF;
	}
	body{
		font-family:"Sukhumvit Set",sans-serif !important;
	}
</style>
</head>
<body>
<div class="panel-body">
	<div class="row">
		<div class="col-md-10" >
		<div style="text-align:center;"><h3>กรุณากรอกข้อมูลต่อไปนี้</h3></div>
			<?php 
				$member_no = $_GET["member_no"];
				if(DB_MEMBER == 'dbo'){	
					$sql_profile = "SELECT FLOMMAST.*, FLOPDEPT.DEPT_NAME,FLOPSTATUS.*, FLOPSTATUS.STS_TYPE_DESC
						FROM LOAN.dbo.FLOMMAST FLOMMAST
							INNER JOIN LOAN.dbo.FLOPDEPT FLOPDEPT ON FLOMMAST.MAST_MEMB_DEPT = FLOPDEPT.DEPT_NO
							INNER JOIN LOAN.dbo.FLOPSTATUS FLOPSTATUS ON FLOMMAST.MAST_MEMB_TYPE = FLOPSTATUS.STS_TYPE_ID AND FLOPSTATUS.STS_CODE = '01'
						WHERE FLOMMAST.MAST_MEMB_NO = '{$member_no}'";
					$rs_profile = odbc_exec($odbc_conn, $sql_profile) or die(odbc_errormsg());
					if($row_profile = odbc_fetch_array($rs_profile)) {
						$row_profile = t2u_array($row_profile);
						$bd_date = new DateTime(ConvertToSQLDate($row_profile["MAST_BIRTH_YMD"]));
						$bd_interval = $bd_date->diff(new DateTime("now"));
						$md_date = new DateTime(ConvertToSQLDate($row_profile["MAST_YMD_IN"]));
						$md_interval = $md_date->diff(new DateTime("now"));
						
						$sql_data = "SELECT email, telmobile
									FROM cmp_imp_member_data
									WHERE member_no = '{$member_no}'";
						$rs_data = $mysqli->query($sql_data);
						$row_data = $rs_data->fetch_assoc();
						
						//echo '<pre>'; print_r($row_profile); echo '</pre>';
						$row = [
									'member_no' => $row_profile['MAST_MEMB_NO']
								,	'memname' => $row_profile['MAST_FNAME'].' '.$row_profile['MAST_NAME']
								,	'member_age' => $bd_interval->format('%Y ปี %m เดือน')
								,	'memdtm' =>  ConvertToSQLDate($row_profile['MAST_YMD_IN'])
								,	'member_register' =>  ConvertToThaiDate(ConvertToSQLDate($row_profile['MAST_YMD_IN']))
								,	'member_register_age' => $md_interval->format('%Y ปี %m เดือน')
								,	'email' => (!empty($row_data['email']))?$row_data['email']:$row_profile['MAST_EMAIL']
								,	'telmobile' => (!empty($row_data['telmobile']))?$row_data['telmobile']:$row_profile['MAST_MOBILE']
								,	'member_address' => sprintf('%s %s', $row_profile['MAST_ADDR'], $row_profile['MAST_ZIP'])
								,	'member_group' => $row_profile['DEPT_NAME']					// หน่วยงาน
								,	'member_type_id' => $row_profile['STS_TYPE_ID']			// รหัสประเภทสมาชิก
								,	'member_type' => $row_profile['STS_TYPE_DESC']			// ประเภทสมาชิก
								,	'member_bd' => ConvertToThaiDate(ConvertToSQLDate($row_profile['MAST_BIRTH_YMD'])) // วันเกิด
								,	'card_person' => $row_profile['MAST_CARD_ID']			// เลขบัตรประชาชน
								,	'salary_amount' => $row_profile['MAST_SALARY']
								];
					}
				}else{
					$sql="SELECT cmp_imp_member.memname , cmp_imp_member_data.telmobile , cmp_imp_member_data.email FROM cmp_imp_member 
					INNER JOIN cmp_imp_member_data ON cmp_imp_member_data.member_no = cmp_imp_member.member_no 
					WHERE cmp_imp_member.member_no = '".$member_no."' ";
					$rs = $mysqli->query($sql);
					echo $mysqli->error;
					$row = $rs->fetch_assoc();
				}
				
				$sql_loan = "SELECT 
				* 
				FROM 
				cmp_admin_loan_money t1
				LEFT JOIN loan_application t2 ON t1.loan_id = t2.loan_id
				WHERE 
				t1.loan_id = '".$_GET['loan_id']."'";
				$rs_loan = $mysqli->query($sql_loan);
				echo $mysqli->error;
				$row_loan= $rs_loan->fetch_assoc();
				
				$sql_setting = "SELECT * FROM loan_setting WHERE loan_type = '3'";
				$rs_setting = $mysqli->query($sql_setting);
				$row_setting = $rs_setting->fetch_assoc();
			?>
			<form action="loan_save.php"  class="form-horizontal" id="frm" method="post" enctype="multipart/form-data">
			<input type="hidden" id="loan_application_type" name="loan_application_type" value="3"  />
			<input type="hidden" id="add_by_user" name="add_by_user" value=""  />
			<input type="hidden" id="from_mobile" name="from_mobile" value="1"  />
			<input type="hidden" id="special_beg" name="special_beg" value=""  />
			<input type="hidden" id="loan_id" name="loan_id" value="<?php echo $_GET['loan_id']; ?>"  />
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" for="member_no" class="col-sm-2 control-label">รหัสสมาชิก</label>
					<div class="col-sm-3">
						<input class="form-control" id="member_id" name="member_id" value="<?php echo $_GET["member_no"]; ?>" required="required" readonly type="text" / >
					</div>
					<label style="font-weight: 100;" for="member_no" class="col-sm-3 control-label">ชื่อ - สกุล</label>
					<div class="col-sm-4">
						<input class="form-control" id="member_name"  required="required" name="member_name" type="text" value="<?php echo $row['memname']; ?>" readonly />
					</div>
				</div>
				
			<div class="form-group m-t-lg">
				<label style="font-weight: 100;" for="member_tel" class="col-sm-2 control-label">หมายเลขโทรศัพท์มือถือ</label>
				<div class="col-sm-3">
					<input class="form-control" id="member_tel" name="member_tel" value="<?php echo $row_loan['member_tel']!=''?$row_loan['member_tel']:$row['telmobile']; ?>" required="required" type="text" onkeypress="return chkNumber(this);" maxlength='10'>
				</div>
			</div>

			<div class="form-group m-t-lg">
				<label style="font-weight: 100;" for="money" class="col-sm-2 control-label">จำนวนเงินขอกู้</label>
				<div class="col-sm-3">
					<input class="form-control" id="money" name="money" value="<?php echo $row_loan['loan_money']!=''?number_format($row_loan['loan_money'],2):''; ?>" required="required" type="text" autofocus onchange="cal_estimate_money_period('<?php echo $row_setting['interest_rate']; ?>')" onkeyup="format_the_number(this)">
				</div>
			</div>
			<div class="form-group m-t-lg">
				<label style="font-weight: 100;" for="period" class="col-sm-2 control-label">จำนวนงวดชำระ</label>
				<div class="col-sm-3">
					<select class="form-control" name="period" id="period" onchange="cal_estimate_money_period('<?php echo $row_setting['interest_rate']; ?>')">
						<option value="">เลือกจำนวนงวดชำระ</option>
						<?php for($i=0; $i <= $row_setting['max_period']; $i++){ ?>
						<option value="<?php echo $i; ?>" <?php echo $i===$row_loan['period']?'selected':''; ?>><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			
			<div class="form-group m-t-lg">
				<label style="font-weight: 100;" for="estimate_money_period" class="col-sm-2 control-label">เงินชำระต่องวด <span style="color:red;"> *ตัวเลขนี้เป็นการคำนวณเบื้องต้นเท่านั้น </span></label>
				<div class="col-sm-3">
					<input class="form-control" id="estimate_money_period" name="estimate_money_period" value="<?php echo $row_loan['estimate_money_period']!=''?number_format($row_loan['estimate_money_period'],2):''; ?>" readonly type="text">
				</div>
			</div>
			<!--div class="form-group">
				<label class="col-sm-2"></label>
				<div class="col-sm-3">
					<span style="color:red;"> *ตัวเลขนี้เป็นการคำนวณเบื้องต้นเท่านั้น </span>
				</div>
			</div-->

			<div class="form-group m-t-lg">
				<label style="font-weight: 100;" for="money_use" class="col-sm-2 control-label">โดยนำไปใช้เพื่อ(เหตุผลการขอกู้) </label>
				<div class="col-sm-8">
					<!--textarea class="form-control" name="money_use" id="money_use" required="required"><?php echo $row_loan['member_use']; ?></textarea-->
					<select class="form-control" name="money_use" id="money_use" required="required">
						<option value="">เลือกเหตุผลการขอกู้</option>
						<?php 
							$sql="SELECT * FROM loan_reason";
							$rs = $mysqli->query($sql);
							while($row = $rs->fetch_assoc()){
						?>
							<option value="<?php echo $row['id']; ?>" <?php echo $row_loan['member_use']==$row['id']?'selected':''; ?>><?php echo $row['loan_reason']; ?></option>	
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group m-t-lg">
				<label style="font-weight: 100; font-weight: bold;" class="col-sm-2 control-label">เลือกรูปแบบหลักประกันที่จะใช้</label>
				<div class="col-sm-4">
					<input type="radio" name="guarantee_type" id="guarantee_type_1" <?php echo $row_loan['guarantee_type'] == '1'?'checked':''?> value="1" onclick="choose_guarantee_type('1');"> สมาชิกค้ำประกัน
				</div>
				<!--div class="col-sm-4">
					<input type="radio" name="guarantee_type" id="guarantee_type_3" <?php echo $row_loan['guarantee_type'] == '3'?'checked':''?> value="3" onclick="choose_guarantee_type('3');"> ตำแหน่งค้ำประกัน
				</div-->
				<div class="col-sm-4">
					<input type="radio" name="guarantee_type" id="guarantee_type_4" <?php echo $row_loan['guarantee_type'] == '4'?'checked':''?> value="4" onclick="choose_guarantee_type('4');"> สิทธิค้ำประกันตนเอง
				</div>
			</div>
			<?php
				if($_GET['loan_id']!=''){
					$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_id = '".$_GET['loan_id']."'";
					$rs_bondsman = $mysqli->query($sql_bondsman);
					$bondsman = array();
					while($row_bondsman = $rs_bondsman->fetch_assoc()){
						$bondsman[$row_bondsman['bondsman_num']] = $row_bondsman;
					}
				}
			?>
			<span class="guarantee_by_position" style="display:none;">
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ตำแหน่งค้ำประกัน</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="guarantee_position" id="guarantee_position" value="<?php echo $row_loan['guarantee_position']; ?>">
					</div>
				</div>
			</span>
			<span class="guarantee_by_share" style="display:none;">
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">สิทธิค้ำประกันตนเอง</label>
					<div class="col-sm-8">
						<?php 
							$sql_share = "SELECT sharestkval FROM cmp_imp_share_master WHERE member_no = '".$_GET["member_no"]."'";
							$rs_share = $mysqli->query($sql_share);
							$row_share = $rs_share->fetch_assoc();
						?>
						<input type="text" readonly class="form-control" name="guarantee_share_value" id="guarantee_share_value" value="<?php echo number_format($row_share['sharestkval']); ?>">
					</div>
				</div>
			</span>
			<span class="guarantee_by_person" style="display:none;">
				<div class="form-group ">
					<label style="font-weight: 100; font-weight: bold;" class="col-sm-12 control-label">ผู้ค้ำประกัน</label>
				</div>
				<div class="form-group ">
					<label style="font-weight: 100;" class="col-sm-2 control-label">รหัสสมาชิกของผู้ค้ำ ลำดับที่ 1</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input class="form-control" id="bondsman_number_1" name="data[loan_application_bondsman][1][bondsman_number]" value="<?php echo $bondsman[1]['bondsman_number']; ?>" type="text" onblur="search_guarantee_person('1')" />
							<span class="input-group-addon icon_color"><i class="fa fa-search" aria-hidden="true"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ชื่อ-นามสกุลผู้ค้ำ ลำดับที่ 1</label>
					<div class="col-sm-8">
						<input class="form-control" id="bondsman_name_1" name="data[loan_application_bondsman][1][bondsman_name]" value="<?php echo $bondsman[1]['bondsman_name']; ?>" type="text" readonly />
					</div>
				</div>
				<div class="form-group" style="display:none">
					<label style="font-weight: 100;" class="col-sm-2 control-label">เลขที่บัตรประชาชนผู้ค้ำ ลำดับที่ 1</label>
					<div class="col-sm-8">
						<input class="form-control guarantee_idcard" id="bondsman_idcard_1" name="data[loan_application_bondsman][1][bondsman_idcard]" value="<?php echo $bondsman[1]['bondsman_idcard']; ?>" type="text" readonly />
					</div>
				</div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">หมายเลขโทรศัพท์มือถือผู้ค้ำ ลำดับที่ 1</label>
					<div class="col-sm-3">
						<input class="form-control" id="bondsman_tel_1" name="data[loan_application_bondsman][1][bondsman_tel]" value="<?php echo $bondsman[1]['bondsman_tel']; ?>" required="required" type="text" onkeypress="return chkNumber(this);" maxlength='10'>
					</div>
				</div>
				<div class="col-sm-12" style="border-bottom:1px solid;opacity: 0.1;"></div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">รหัสสมาชิกของผู้ค้ำ ลำดับที่ 2</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input class="form-control" id="bondsman_number_2" name="data[loan_application_bondsman][2][bondsman_number]" value="<?php echo $bondsman[2]['bondsman_number']; ?>" type="text" onblur="search_guarantee_person('2')" />
							<span class="input-group-addon icon_color"><i class="fa fa-search" aria-hidden="true"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ชื่อ-นามสกุลผู้ค้ำ ลำดับที่ 2</label>
					<div class="col-sm-8">
						<input class="form-control" id="bondsman_name_2" name="data[loan_application_bondsman][2][bondsman_name]" value="<?php echo $bondsman[2]['bondsman_name']; ?>" type="text" readonly />
					</div>
				</div>
				<div class="form-group" style="display:none">
					<label style="font-weight: 100;" class="col-sm-2 control-label">เลขที่บัตรประชาชนผู้ค้ำ ลำดับที่ 2</label>
					<div class="col-sm-8">
						<input class="form-control guarantee_idcard" id="bondsman_idcard_2" name="data[loan_application_bondsman][2][bondsman_idcard]" value="<?php echo $bondsman[2]['bondsman_idcard']; ?>" type="text" readonly />
					</div>
				</div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">หมายเลขโทรศัพท์มือถือผู้ค้ำ ลำดับที่ 2</label>
					<div class="col-sm-3">
						<input class="form-control" id="bondsman_tel_2" name="data[loan_application_bondsman][2][bondsman_tel]" value="<?php echo $bondsman[2]['bondsman_tel']; ?>" required="required" type="text" onkeypress="return chkNumber(this);" maxlength='10'>
					</div>
				</div>
				<div class="col-sm-12" style="border-bottom:1px solid;opacity: 0.1;"></div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">รหัสสมาชิกของผู้ค้ำ ลำดับที่ 3</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input class="form-control" id="bondsman_number_3" name="data[loan_application_bondsman][3][bondsman_number]" value="<?php echo $bondsman[3]['bondsman_number']; ?>" type="text" onblur="search_guarantee_person('3')" />
							<span class="input-group-addon icon_color"><i class="fa fa-search" aria-hidden="true"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group ">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ชื่อ-นามสกุลผู้ค้ำ ลำดับที่ 3</label>
					<div class="col-sm-8">
						<input class="form-control" id="bondsman_name_3" name="data[loan_application_bondsman][3][bondsman_name]" value="<?php echo $bondsman[3]['bondsman_name']; ?>" type="text" readonly />
					</div>
				</div>
				<div class="form-group " style="display:none">
					<label style="font-weight: 100;" class="col-sm-2 control-label">เลขที่บัตรประชาชนผู้ค้ำ ลำดับที่ 3</label>
					<div class="col-sm-8">
						<input class="form-control guarantee_idcard" id="bondsman_idcard_3" name="data[loan_application_bondsman][3][bondsman_idcard]" value="<?php echo $bondsman[3]['bondsman_idcard']; ?>" type="text" readonly />
					</div>
				</div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">หมายเลขโทรศัพท์มือถือผู้ค้ำ ลำดับที่ 3</label>
					<div class="col-sm-3">
						<input class="form-control" id="bondsman_tel_3" name="data[loan_application_bondsman][3][bondsman_tel]" value="<?php echo $bondsman[3]['bondsman_tel']; ?>" required="required" type="text" onkeypress="return chkNumber(this);" maxlength='10'>
					</div>
				</div>
			</span>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-12 control-label">กรุณาแนบรูปภาพเอกสารประกอบการขอกู้ <a href="http://www.gsb-coop.com/index.php/gsbm4" target="_blank" style="color:#ff4081;">ดาวน์โหลดเอกสารที่นี่</a></label>
				</div-->
			<?php
				$sql_file = "SELECT * FROM loan_application_file WHERE loan_id = '".$_GET['loan_id']."'";
				$rs_file = $mysqli->query($sql_file);
				$file_data = array();
				$output_dir = "../../uploads/loan/";
				while($row_file = $rs_file->fetch_assoc()){
					if($row_file['bondsman_num']!=''){
						$file_data[$row_file['input_name']][$row_file['bondsman_num']][] = $row_file;
					}else{
						$file_data[$row_file['input_name']][] = $row_file;
					}
				}
				//echo"<pre>";print_r($file_data);echo"</pre>";exit;
			?>
			<div id="loan_owner_file">
				<div class="form-group m-t-lg" style="background-color:rgb(240,239,240); height:40px;">
					<label style="font-weight: 100;padding-top:10px;" class="col-sm-12 control-label">แนบเอกสารสำหรับผู้ขอกู้</label>
				</div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 1 เอกสารการรับเงินเดือน เดือนสุดท้าย (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_salary" name="file_salary[]" type="file" multiple />
					</div>
				</div>
				<?php
				if($file_data['file_salary']){
					foreach($file_data['file_salary'] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 2 สำเนาบัตรประชาชน (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_idcard" name="file_idcard[]" type="file" multiple />
					</div>
				</div>
				<?php
				if($file_data['file_idcard']){
					foreach($file_data['file_idcard'] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 3 สำเนาทะเบียนบ้าน (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_register_home" name="file_register_home[]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_register_home']){
					foreach($file_data['file_register_home'] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 4 หนังสือยินยอมให้ธนาคารหักชำระหนี้ (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_allow_the_bank" name="file_allow_the_bank[]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_allow_the_bank']){
					foreach($file_data['file_allow_the_bank'] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 5 แบบ สก. 4 (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_sk4" name="file_sk4[]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_sk4']){
					foreach($file_data['file_sk4'] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 5 หน้าสมุดเงินฝากธนาคาร (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_account_bank_page" name="file_account_bank_page[]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_account_bank_page']){
					foreach($file_data['file_account_bank_page'] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 6 ใบขอความอนุเคราะห์พิเศษ (ถ้ามี)</label>
					<div class="col-sm-8">
						<input id="file_special_beg" name="file_special_beg[]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_special_beg']){
					foreach($file_data['file_special_beg'] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
			</div>
			<div id="guarantee_file_1" class="guarantee_by_person" style="display:none;">
				<div class="form-group m-t-lg" style="background-color:rgb(240,239,240);height:40px;">
					<label style="font-weight: bold;padding-top:10px" class="col-sm-12 control-label">แนบเอกสารสำหรับผู้ค้ำประกันลำดับที่ 1</label>
				</div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 1 เอกสารการรับเงินเดือน เดือนสุดท้าย (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_salary_1" class="guarantee_file_1" name="loan_application_bondsman[1][file_bondsman_salary][]" type="file" multiple />
					</div>
				</div>
				<?php
				if($file_data['file_bondsman_salary']){
					foreach($file_data['file_bondsman_salary'][1] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 2 สำเนาบัตรประชาชน (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_idcard_1" class="guarantee_file_1" name="loan_application_bondsman[1][file_bondsman_idcard][]" type="file" multiple />
					</div>
				</div>
				<?php
				if($file_data['file_bondsman_idcard'][1]){
					foreach($file_data['file_bondsman_idcard'][1] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 3 สำเนาทะเบียนบ้าน (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_register_home_1" class="guarantee_file_1" name="loan_application_bondsman[1][file_bondsman_register_home][]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_bondsman_register_home'][1]){
					foreach($file_data['file_bondsman_register_home'][1] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 3 แบบสก. 4 (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_sk4_1" class="guarantee_file_1" name="loan_application_bondsman[1][file_bondsman_sk4][]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_bondsman_sk4'][1]){
					foreach($file_data['file_bondsman_sk4'][1] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
			</div>
			<div id="guarantee_file_2" class="guarantee_by_person" style="display:none;">			
				<div class="form-group m-t-lg" style="background-color:rgb(240,239,240);height:40px;">
					<label style="font-weight: bold;padding-top:10px;" class="col-sm-12 control-label">แนบเอกสารสำหรับผู้ค้ำประกันลำดับที่ 2</label>
				</div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 1 เอกสารการรับเงินเดือน เดือนสุดท้าย (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_salary_1" class="guarantee_file_2" name="loan_application_bondsman[2][file_bondsman_salary][]" type="file" multiple />
					</div>
				</div>
				<?php
				if($file_data['file_bondsman_salary']){
					foreach($file_data['file_bondsman_salary'][2] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 2 สำเนาบัตรประชาชน (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_idcard_1" class="guarantee_file_2" name="loan_application_bondsman[2][file_bondsman_idcard][]" type="file" multiple />
					</div>
				</div>
				<?php
				if($file_data['file_bondsman_idcard'][2]){
					foreach($file_data['file_bondsman_idcard'][2] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 3 สำเนาทะเบียนบ้าน (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_register_home_1" class="guarantee_file_2" name="loan_application_bondsman[2][file_bondsman_register_home][]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_bondsman_register_home'][2]){
					foreach($file_data['file_bondsman_register_home'][2] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 3 แบบสก. 4 (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_sk4_1" class="guarantee_file_2" name="loan_application_bondsman[2][file_bondsman_sk4][]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_bondsman_sk4'][2]){
					foreach($file_data['file_bondsman_sk4'][2] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
			</div>
			<div id="guarantee_file_3" class="guarantee_by_person" style="display:none;">
				<div class="form-group m-t-lg" style="background-color:rgb(240,239,240);height:40px;">
					<label style="font-weight: bold;padding-top:10px" class="col-sm-12 control-label">แนบเอกสารสำหรับผู้ค้ำประกันลำดับที่ 3</label>
				</div>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 1 เอกสารการรับเงินเดือน เดือนสุดท้าย (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_salary_1" class="guarantee_file_3" name="loan_application_bondsman[3][file_bondsman_salary][]" type="file" multiple />
					</div>
				</div>
				<?php
				if($file_data['file_bondsman_salary']){
					foreach($file_data['file_bondsman_salary'][3] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 2 สำเนาบัตรประชาชน (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_idcard_1" class="guarantee_file_3" name="loan_application_bondsman[3][file_bondsman_idcard][]" type="file" multiple />
					</div>
				</div>
				<?php
				if($file_data['file_bondsman_idcard'][3]){
					foreach($file_data['file_bondsman_idcard'][3] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 3 สำเนาทะเบียนบ้าน (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_register_home_1" class="guarantee_file_3" name="loan_application_bondsman[3][file_bondsman_register_home][]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_bondsman_register_home'][3]){
					foreach($file_data['file_bondsman_register_home'][3] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
				<!--div class="form-group m-t-lg">
					<label style="font-weight: 100;" class="col-sm-2 control-label">ลำดับที่ 3 แบบสก. 4 (ต้องมี)</label>
					<div class="col-sm-8">
						<input id="file_bondsman_sk4_1" class="guarantee_file_3" name="loan_application_bondsman[3][file_bondsman_sk4][]" type="file" multiple />
					</div>
				</div-->
				<?php
				if($file_data['file_bondsman_sk4'][3]){
					foreach($file_data['file_bondsman_sk4'][3] as $key => $value){
						echo "<div class='form-group m-t-lg' id='file_id_".$value['loan_file_id']."'><div style='font-weight: 100;' class='col-sm-12'><a style='color:blue;' href='".$output_dir.$value['loan_file_name']."'>".$value['loan_file_old_name']."</a><input type='button' class='btn btn-danger' style='float:right;' value='ลบ' onclick=\"delete_file('".$value['loan_file_id']."')\"><br></div></div>";
					}
				}
				?>
			</div>
		</form>
				<div class="col-sm-12" style="border-bottom:1px solid;opacity: 0.5;"></div>						
				<div class="form-group m-t-lg">
					<div class="col-sm-12 text-center">
						<button style="width: 123px;" id="submit_frm" class="btn btn-material-light-blue-300" >บันทึกข้อมูล</button>
						<a href="/loan_main_menu_mobile.php">
							<button style="width: 123px;" class="btn  btn-material-light-blue-300" type="button" >ออก</button>
						</a>
					</div>
				</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="margin-top:10%;">
      <div class="modal-content">
        <div class="modal-header modal-header-loan">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ข้อมูลผู้ค้ำประกัน</h4>
        </div>
        <div class="modal-body">
			<input class="form-control" type="text" placeholder="กรอกรหัสผู้ค้ำประกันหรือชื่อ-สกุล" name="search_text" id="search_mem" style="font-size:20px;" autofocus>
			<input type="hidden" value="" id="id">								
			<span class="icon icon-search input-icon"></span>
			<div class="bs-example" data-example-id="striped-table">
			<font size="4.5">
			<table class="table table-bordered table-striped table-center" style="margin-top:25px;">
				<input type="hidden" id="id_to" value="">			
                <tbody id="result_member"></tbody>
			</table>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" id="close" class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
        </div>
      </div>
    </div>
  </div>
<div class="modal fade" id="loan_warning" role="dialog">
	<div class="modal-dialog" style="margin-top:10%;">
		<div class="modal-content">
			<div class="modal-header modal-header-loan">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">แจ้งเตือน</h4>
			</div>
			<div class="modal-body text-center">
				<span id="warning_msg"></span><br>
				ท่านต้องการขอความอนุเคราะห์เป็นกรณีพิเศษหรือไม่ ?
			</div>
			<div class="modal-footer" style="text-align:center !important;">
				<button type="button" style="width:90%" class="btn btn-default" data-dismiss="modal">ยกเลิก</button><br>
				<button type="button" style="width:90%;margin-top:5px;" class="btn btn-warning" onclick="submit_special_beg()">ขอความอนุเคราะห์เป็นกรณีพิเศษ</button>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$("#submit_frm").click(function(event) {
		//if ($("#frm").valid() == true) {
			check_term_of_loan();
		//} else {
			//return false;
		//}
	});
	$('#search_mem').keyup(function(){  
			var txt = $(this).val();
			var mem = $("#id_to").val();
			if(txt != '')  
			{  
				$.ajax({  
					url:"ajax/ajax_search_guarantor_2.php",  
					method:"post",  
					data:{search:txt , mem: mem},  
					dataType:"text",  
					success:function(data)  
					{  
					console.log(data); 
					$('#result_member').html(data);  
					}  
				});  
			}else{
			
			}
			
		});

		$('#myModal').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var id = button.data('id');
			var modal = $(this);
			modal.find('.modal-body #id').val(id);
		});
		choose_guarantee_type('<?php echo $row_loan['guarantee_type'] ?>');
});
	function get_select_attr_value(id_from, attr_name, id_to){
		$('#'+id_to).val($('#'+id_from+' :selected').attr(attr_name));
	}
	
	function choose_guarantor(member_id, member_name, member_group){
		var id_to = $('#id_to').val();
		$('#member_id_'+id_to).val(member_id);
		$('#member_name_'+id_to).val(member_name);
		$('#member_group_'+id_to).val(member_group);
	}
	
	function get_guarantor_number(number){
		//alert(number);
		$('#id_to').val(number);
	}
	
	function check_term_of_loan(){
		var txt_alert = '';
		
		var member_id = $('#member_id').val();
		var loan_money = $('#money').val().replace(',','');
		var loan_application_type = $('#loan_application_type').val();
		if(loan_money == ''){
			txt_alert += ' - ระบุจำนวนเงินกู้\n';
			swal('ไม่สามารถบันทึกคำร้องได้\nกรุณาดำเนินการต่อไปนี้', txt_alert);
		}else{
			$.ajax({
				url: "check_trem_of_loan.php",
				type: "POST",
				data : {member_id : member_id, loan_money: loan_money, loan_application_type:loan_application_type},
				cache: false,
				success: function(msg){
					if (msg != "success") {
						txt_alert += msg+'\n';
					}
					if($('#member_tel').val()==''){
						txt_alert += ' - ระบุหมายเลขโทรศัพท์มือถือ\n';
					}
					if($('#money_use').val()==''){
						txt_alert += ' - ระบุเหตุผลการขอกู้\n';
					}
					if(!$('#guarantee_type_1').is(':checked') &&  !$('#guarantee_type_3').is(':checked') && !$('#guarantee_type_4').is(':checked')){
						txt_alert += ' - ระบุประเภทการค้ำประกัน\n';
					}

					if($('#file_idcard').val()=='' && $('#loan_id').val()==''){
						txt_alert += ' - แนบสำเนาบัตรประชาชน\n';
					}
					if($('#file_salary').val()=='' && $('#loan_id').val()==''){
						txt_alert += ' - แนบเอกสารการรับเงินเดือน\n';
					}
					//if($('#file_register_home').val()=='' && $('#loan_id').val()==''){
						//txt_alert += ' - แนบสำเนาทะเบียนบ้าน\n';
					//}
					//if($('#allow_the_bank').val()=='' && $('#loan_id').val()==''){
						//txt_alert += ' - แนบหนังสือยินยอมให้ธนาคารหักชำระหนี้\n';
					//}
					//if($('#allow_the_bank').val()=='' && $('#loan_id').val()==''){
						//txt_alert += ' - แนบหนังสือยินยอมให้ธนาคารหักชำระหนี้\n';
					//}
					//if($('#allow_the_bank').val()=='' && $('#loan_id').val()==''){
						//txt_alert += ' - แนบหนังสือยินยอมให้ธนาคารหักชำระหนี้\n';
					//}
					//if($('#file_sk4').val()=='' && $('#loan_id').val()==''){
						//txt_alert += ' - แนบแบบ สก. 4\n';
					//}
					//if($('#file_account_bank_page').val()=='' && $('#loan_id').val()==''){
						//txt_alert += ' - แนบสำเนาหน้าสมุดเงินฝากธนาคาร\n';
					//}
					
					if($('#guarantee_type_1').is(':checked')){
						var i=0;
						$('.guarantee_idcard').each(function(){
							if($(this).val()!=''){
								i++;
							}
						});
						if(i==0){
							txt_alert += ' - ข้อมูลผู้ค้ำประกันอย่างน้อย 1 คน\n';
						}
						if($('#bondsman_idcard_1').val()!='' && $('#loan_id').val()==''){
							var i=0;
							$('.guarantee_file_1').each(function(){
								if($(this).val()==''){
									i++;
								}
							});
							if(i!=0){
								txt_alert += ' - กรุณาแนบเอกสารของผู้ค้ำประกันลำดับที่1ให้ครบถ้วน\n';
							}
						}
						if($('#bondsman_idcard_2').val()!='' && $('#loan_id').val()==''){
							var i=0;
							$('.guarantee_file_2').each(function(){
								if($(this).val()==''){
									i++;
								}
							});
							if(i!=0){
								txt_alert += ' - กรุณาแนบเอกสารของผู้ค้ำประกันลำดับที่2ให้ครบถ้วน\n';
							}
						}
						if($('#bondsman_idcard_3').val()!='' && $('#loan_id').val()==''){
							var i=0;
							$('.guarantee_file_3').each(function(){
								if($(this).val()==''){
									i++;
								}
							});
							if(i!=0){
								txt_alert += ' - กรุณาแนบเอกสารของผู้ค้ำประกันลำดับที่3ให้ครบถ้วน\n';
							}
						}
					}
					if($('#guarantee_type_3').is(':checked')){
						if($('#guarantee_position').val()==''){
							txt_alert += ' - ตำแหน่งค้ำประกัน\n';
						}
					}
					if($('#guarantee_type_4').is(':checked')){
						if($('#guarantee_share_value').val()=='0'){
							txt_alert += ' - ท่านไม่มีหุ้นสำหรับค้ำประกัน\n';
						}
					}
					
					if(txt_alert != ''){
						swal('ไม่สามารถบันทึกคำร้องได้\nกรุณาดำเนินการต่อไปนี้', txt_alert);
					}else{
						$("#frm").submit();
					}

				 },
				  async: false
			});
		}
		
		
		
	}
	
	function search_guarantee_person(id){
			var member_id = $('#bondsman_number_'+id).val();
			if(member_id!=''){
				$.ajax({  
					url:"ajax/ajax_search_guarantee_person.php",  
					method:"post",  
					data:{ member_id:member_id , loan_type : '3'},  
					dataType:"text",  
					success:function(data)  
					{  
						if(data!='error'){
							var result = JSON.parse(data);
							if(result['can_guarantee'] == 'yes'){
								$('#bondsman_number_'+id).val(result['member_no']);
								$('#bondsman_name_'+id).val(result['memname']);
								$('#bondsman_idcard_'+id).val(result['card_person']);
								$('#bondsman_tel_'+id).val(result['telmobile']);
							}else{
								swal('เกิดข้อผิดพลาด',result['memname']+' ไม่มีสิทธิค้ำประกัน','warning');
								$('#bondsman_number_'+id).val('');
								$('#bondsman_name_'+id).val('');
								$('#bondsman_idcard_'+id).val('');
								$('#bondsman_tel_'+id).val('');
							}
							//console.log(result);
							
						}else{
							swal('ไม่พบข้อมูลสมาชิก');
							$('#bondsman_number_'+id).val('');
							$('#bondsman_name_'+id).val('');
							$('#bondsman_idcard_'+id).val('');
							$('#bondsman_tel_'+id).val('');
						}
					}  
				});
			}else{
				$('#bondsman_number_'+id).val('');
				$('#bondsman_name_'+id).val('');
				$('#bondsman_idcard_'+id).val('');
				$('#bondsman_tel_'+id).val('');
			}
	}
	
	function submit_special_beg(){
		$('#special_beg').val('1');
		$("#frm").submit();
	}
	function choose_guarantee_type(type){
		if(type == '1'){
			$('.guarantee_by_person').show();
			$('.guarantee_by_account_bank').hide();
			$('.guarantee_by_position').hide();
			$('.guarantee_by_share').hide();
		}else if(type == '2'){
			$('.guarantee_by_person').hide();
			$('.guarantee_by_account_bank').show();
			$('.guarantee_by_position').hide();
			$('.guarantee_by_share').hide();
		}else if(type == '3'){
			$('.guarantee_by_person').hide();
			$('.guarantee_by_account_bank').hide();
			$('.guarantee_by_position').show();
			$('.guarantee_by_share').hide();
		}else if(type == '4'){
			$('.guarantee_by_person').hide();
			$('.guarantee_by_account_bank').hide();
			$('.guarantee_by_position').hide();
			$('.guarantee_by_share').show();
		}
	}
	function delete_file(file_id){
		swal({
			title: "ท่านต้องการลบไฟล์ใช่หรือไม่",
			text: "",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'ยืนยัน',
			cancelButtonText: "ยกเลิก",
			closeOnConfirm: true,
			closeOnCancel: true
		 },
		 function(isConfirm){
		   if (isConfirm){
				$.ajax({  
					url:"ajax/ajax_loan_file_delete.php",  
					method:"post",  
					data:{ file_id:file_id },  
					dataType:"text",  
					success:function(data)  
					{  
						$('#file_id_'+file_id).hide()
					}  
				});
			} else {
				
			}
		 });
	}
	function cal_estimate_money_period(interest_rate){
		var money = $('#money').val().replace(/,/g ,'');
		var period = $('#period').val();
		//console.log(money);
		if(money != '' && period != ''){
			$.ajax({  
				url:"ajax/ajax_cal_estimate_money_period.php",  
				method:"post",  
				data:{ 
					money:money, 
					period:period, 
					interest_rate:interest_rate ,
					loan_type : '3'
				},  
				dataType:"text",  
				success:function(data)  
				{  
					var result = JSON.parse(data);
					$('#estimate_money_period').val(result['money_per_period']);
				}  
			});
		}else{
			$('#estimate_money_period').val('');
		}
	} 
function format_the_number(ele){
	var value = $('#'+ele.id).val();
	if(value!=''){
		value = value.replace(',','');
		value = parseInt(value);
		value = value.toLocaleString();
		//console.log(value);
		if(value == 'NaN'){
			$('#'+ele.id).val('');
		}else{
			$('#'+ele.id).val(value);
		}
	}else{
		$('#'+ele.id).val('');
	}
}
function chkNumber(ele){
	var vchar = String.fromCharCode(event.keyCode);
	if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
	ele.onKeyPress=vchar;
}
</script>
</body>
</HTML>
