<?php
	include "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}
	function cal_age($birthday,$type = 'y'){     //รูปแบบการเก็บค่าข้อมูลวันเกิด
		$birthday = date("Y-m-d",strtotime($birthday)); 
		$today = date("Y-m-d");   //จุดต้องเปลี่ยน
		list($byear, $bmonth, $bday)= explode("-",$birthday);       //จุดต้องเปลี่ยน
		list($tyear, $tmonth, $tday)= explode("-",$today);                //จุดต้องเปลี่ยน
		$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear);
		$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
		$mage = ($mnow - $mbirthday);
		//echo "วันเกิด $birthday"."<br>\n";
		//echo "วันที่ปัจจุบัน $today"."<br>\n";
		//echo "รับค่า $mage"."<br>\n";
		$u_y=date("Y", $mage)-1970;
		$u_m=date("m",$mage)-1;
		$u_d=date("d",$mage)-1;
		if($type=='y'){
			return $u_y;
		}else if($type=='m'){
			return $u_m;
		}else{
			return $u_d;
		}
	}

	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_ageement_1.pdf" ;
	
	
	$pdf = new FPDI();
	
	$pageCount_1 = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount_1; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* 
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_result t3 ON t1.loan_id = t3.loan_id
			WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member t1
		LEFT JOIN cmp_imp_address t2 ON t1.member_no = t2.member_no
		WHERE t1.member_no = '".$row_loan['member_id']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 67;
			$pdf->SetXY( 94, $y_point );
			$pdf->MultiCell(40, 5, U2T(format_date($row_loan['application_date'])), $border, 1);
			
			$y_point = 75;
			$pdf->SetXY( 48, $y_point );
			$pdf->MultiCell(49, 5, U2T($row_loan['member_name']), $border, 1);
			$pdf->SetXY( 104, $y_point );
			$pdf->MultiCell(9, 5, U2T(cal_age($row_member['birthdtm'])), $border, 'R');
			
			$y_point = 83;
			$pdf->SetXY( 31, $y_point );
			$pdf->MultiCell(49, 5, U2T($row_loan['member_id']), $border, 1);
			$pdf->SetXY( 113, $y_point );
			$pdf->MultiCell(49, 5, U2T($row_loan['member_position']), $border, 1);
			
			$y_point = 90;
			$pdf->SetXY( 39, $y_point );
			$pdf->MultiCell(63, 5, U2T($row_loan['member_branch_name']), $border, 1);
			$pdf->SetXY( 120, $y_point );
			$pdf->MultiCell(28, 5, U2T($row_member['memb_addr']), $border, 1);
			$pdf->SetXY( 156, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_member['road']), $border, 1);
			
			$y_point = 98;
			$pdf->SetXY( 28, $y_point );
			$pdf->MultiCell(47, 5, U2T($row_member['soi']), $border, 1);
			$pdf->SetXY( 93, $y_point );
			$pdf->MultiCell(44, 5, U2T($row_member['tambol']), $border, 1);
			$pdf->SetXY( 153, $y_point );
			$pdf->MultiCell(44, 5, U2T($row_member['district_desc']), $border, 1);
			
			$y_point = 106;
			$pdf->SetXY( 23, $y_point );
			$pdf->MultiCell(52, 5, U2T($row_member['province_desc']), $border, 1);
			$pdf->SetXY( 88, $y_point );
			$pdf->MultiCell(46, 5, U2T($row_loan['member_tel']), $border, 1);
			
			$y_point = 136;
			$pdf->SetXY( 96, $y_point );
			$pdf->MultiCell(94, 5, U2T(num_format($row_loan['loan_approve_money'])), $border, 'R');
			
			$y_point = 144;
			$pdf->SetXY( 15, $y_point );
			$pdf->MultiCell(96, 5, U2T(convert($row_loan['loan_approve_money'])), $border, 'C');
			
			$y_point = 152;
			$pdf->SetXY( 13, $y_point );
			$pdf->MultiCell(73, 5, U2T($row_loan['member_use']), $border, 1);
			
			$y_point = 160;
			$pdf->SetXY( 107, $y_point );
			$pdf->MultiCell(80, 5, U2T(num_format($row_loan['pay_per_month'])), $border, 'R');
			
			$y_point = 167;
			$pdf->SetXY( 14, $y_point );
			$pdf->MultiCell(81, 5, U2T(convert($row_loan['pay_per_month'])), $border, 'C');
			
			$y_point = 175;
			$pdf->SetXY( 112, $y_point );
			$pdf->MultiCell(41, 5, U2T($row_loan['interest_rate']), $border, 'R');
			
			$y_point = 182;
			$pdf->SetXY( 23, $y_point );
			$pdf->MultiCell(38, 5, U2T(num_format($row_loan['pay_last_month'])), $border, 'R');
			$pdf->SetXY( 154, $y_point );
			$pdf->MultiCell(17, 5, U2T($row_loan['month_period']), $border, 'R');
			
			$y_point = 190;
			$pdf->SetXY( 38, $y_point );
			$pdf->MultiCell(38, 5, U2T(''), $border, 1);
			
			$y_point = 205;
			$pdf->SetXY( 102, $y_point );
			$pdf->MultiCell(20, 5, U2T(''), $border, 1);
		}
		
	}
	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_ageement_2.pdf" ;
	$pageCount_2 = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount_2; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* 
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_result t3 ON t1.loan_id = t3.loan_id
			WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member t1
		LEFT JOIN cmp_imp_address t2 ON t1.member_no = t2.member_no
		WHERE t1.member_no = '".$row_loan['member_id']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 12;
			$pdf->SetXY( 119, $y_point );
			$pdf->MultiCell(13, 5, U2T(''), $border, 1);
			
			$y_point = 167;
			$pdf->SetXY( 85, $y_point );
			$pdf->MultiCell(61, 5, U2T($row_loan['member_name']), $border, 'C');
			
		}
		
	}
	
	$pdf->Output();