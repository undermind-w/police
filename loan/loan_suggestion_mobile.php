<?php
	include "config.inc.php";
	require PATH . "/class/pagination.class.php"  ;
?>
<HTML>
<head>
 <?php
	include "header_mobile.inc.php"
 ?>
	<style>
		body {
			background-color:#FFF;
		}
		.panel-heading {
			border-bottom:1px solid;
		}
		.menu_gruop{
			margin:5px;
		}
		.icon_color{
			color: #EB068D;
		}
		.menu_link:active{
			 background-color:rgb(212,212,212);;
		} 
		h3{
			margin-top:12px;
			margin-bottom:12px;
		}
		.tr_border{
			border-bottom:1px solid rgb(212,212,212);
		}
		.td_style_icon{
			padding: 10px;
		}
		.td_style{
			padding-top: 10px;
			padding-bottom: 10px;
		}
		.text_show{
			font-size: 16px;
		}
		.date_show{
			
			color: #AAA;
			font-size: 16px;
		}
		body{
		font-family:"Sukhumvit Set",sans-serif !important;
	}
	</style>
</head>
<body>
<?php
	$icon = 'fa fa-chevron-circle-right fa-stack-2x text-material-light-blue-300 icon_color'; 
	
	// $param = '';
	// if($_GET['member_no']!=''){
	// 	$param = "member_no=".$_GET['member_no'];
	// }else{		
	// 	foreach($_GET as $key => $value){
	// 		$param .= $key;
	// 		$decode = base64_decode($key);
	// 		$decode = explode('=',$decode);
	// 		$_GET[$decode[0]] = $decode[1];			
	// 	}
	// }

	$param = base64_encode("member_no=".$_GET['member_no'])."&".base64_encode("loan_type=".$_GET['loan_type']);
	$sql = "SELECT * FROM loan_setting_detail WHERE id='".$_GET['loan_type']."'";
	$rs = $mysqli->query($sql);
	$row = $rs->fetch_assoc();
	//$loan_application_id = "&".base64_encode("loan_application_id=".$row['id']);
?>
<div class="panel-body">
	<div class="row">
		<div style="margin-top: 1.5em;" class="text-center">
			<div class="col-xs-12">
				<h3 class="text-center" style="font-size: 25px; font-family: 'thaisans_neueregular' !important;"><?php echo $row['loan_name'];?></h3>
			</div>
			<div class="col-xs-12" class="text-center" style="padding-right: 5px;padding-left: 5px;">
				<iframe width="100%" frameBorder="0" height="350px" src="loan_suggestion_show_mobile.php?<?php echo $param; ?>" style="border: 1px solid #ddd;"></iframe>
			</div>
			<div class="col-xs-12">
			<br>		
				<div class="col-sm-12" style="margin-bottom:15px;">
					<a onclick="link_click('loan_petition_normal_mobile.php?<?php echo $param; ?>','1','<?php echo $_GET['member_no']; ?>')">
						<button style="font-size: 20px;font-family: 'thaisans_neueregular' !important;width:100%" type="button" class="btn btn-material-light-blue-300">ฉันได้อ่านรายละเอียดและทำความเข้าใจแล้ว</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function link_click(link){
	location.href = link;
}
</script>
</body>
</HTML>
