<?php
	include "config.inc.php";
	require PATH . "/class/pagination.class.php"  ;
	//include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.session.inc.php";
?>
<HTML>
<head>
 <?php
	include "header_mobile.inc.php"
 ?>
	<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
    <script src="/scripts/jquery.validate_loan.js"></script>
<style>
	.input-group[class*=col-]{
		padding-right: 15px;
		padding-left: 15px;
	}
	.icon_color{
		background-color: rgb(235,6,141);
		color: #FFF;
	}
	/* unvisited link */
	a:link {
		color: #FFF;
	}

	/* visited link */
	a:visited {
		color: #FFF;
	}

	/* mouse over link */
	a:hover {
		color: #FFF;
	}

	/* selected link */
	a:active {
		color: #FFF;
	}
</style>
</head>
<body>
<div class="panel-body">
	<div class="row">
		<div class="col-md-10" >
			<?php 
				$sql = "SELECT 
					* 
				FROM 
					loan_application 
					INNER JOIN cmp_admin_loan_money ON loan_application.loan_id = cmp_admin_loan_money.loan_id
				WHERE loan_application_id = '".$_GET['loan_application_id']."'";
				$rs = $mysqli->query($sql);
				echo $mysqli->error;
				$row = $rs->fetch_assoc();
				
				$sql_special_beg = "SELECT * FROM loan_special_beg WHERE loan_id = '".$_GET['loan_id']."'";
				$rs_special_beg = $mysqli->query($sql_special_beg);
				echo $mysqli->error;
				$row_special_beg = $rs_special_beg->fetch_assoc();
			?>
			<form novalidate action="loan_save_special_beg.php"  class="form-horizontal" id="frm" method="post" enctype="multipart/form-data">
			<input name="data[loan_special_beg][loan_id]" value="<?php echo $_GET['loan_id']; ?>" type="hidden" >
			<input name="data[loan_special_beg][loan_application_id]" value="<?php echo $_GET['loan_application_id']; ?>" type="hidden" >
			<input name="data[loan_special_beg][id]" value="<?php echo $row_special_beg['id']; ?>" type="hidden" >
			<div class="form-group m-t-lg">
				<label style="font-weight: 100;" class="col-sm-2 control-label">รหัสสมาชิก</label>
				<div class="col-sm-3">
					<input class="form-control" id="member_id" value="<?php echo $row["member_id"]; ?>" type="text" readonly>
				</div>
				<label style="font-weight: 100;" class="col-sm-2 control-label">ชื่อ - สกุล</label>
				<div class="col-sm-4">
					<input class="form-control" id="member_name" type="text" value="<?php echo $row['member_name']?>" readonly>
				</div>
			</div>

			<div class="form-group m-t-lg">
				<label style="font-weight: 100;" class="col-sm-2 control-label">จำนวนเงินขอกู้</label>
				<div class="col-sm-3">
					<input class="form-control" id="money" value="<?php echo $row['loan_money']; ?>" type="number" readonly>
				</div>
			</div>
			
			<div class="form-group">
				<label style="font-weight: 100;" class="col-sm-2 control-label">เพื่อ</label>
				<div class="col-sm-8">
					<textarea class="form-control" id="money_use" readonly><?php echo $row['member_use']; ?></textarea>
				</div>
			</div>

			<div class="form-group" style="padding:0px;margin:0px">
				<input type="checkbox" name="data[loan_special_beg][condition_1]" id="condition_1" value='1' onclick="choose_condition('1')" <?php echo $row_special_beg['condition_1']=='1'?'checked':''; ?>>
				<?php 
				if($row_special_beg['condition_1']=='1'){
					$readonly = '';
				}else{
					$readonly = 'readonly';
				}
				?>
				
					ขอผ่อนผันเงินเดือนคงเหลือไม่ถึง 40% หรือ 5,000 บาท โดยมีรายได้พิเศษจาก
					<input type="text" class="form-control condition_1" name="data[loan_special_beg][condition_1_extra_income]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_1_extra_income']; ?>" <?php echo $readonly; ?>> 
					เดือนละ 
					<input type="number" class="form-control condition_1" name="data[loan_special_beg][condition_1_extra_income_per_month]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_1_extra_income_per_month']; ?>" <?php echo $readonly; ?>>
					บาท รายได้คู่สมรส 
					<input type="number" class="form-control condition_1" name="data[loan_special_beg][condition_1_mariage_income]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_1_mariage_income']; ?>" <?php echo $readonly; ?>> 
					บาท
			</div>
			<div class="form-group" style="padding:0px;margin:0px">
				<input type="checkbox" name="data[loan_special_beg][condition_2]" id="condition_2" value='1' <?php echo $row_special_beg['condition_2']=='1'?'checked':''; ?>>
				ขอใช้ทุนเรือนหุ้น / เงินฝากออมทรัพย์พิเศษค้ำประกัน 100%
			</div>
			<div class="form-group" style="padding:0px;margin:0px">
				<input type="checkbox" name="data[loan_special_beg][condition_3]" id="condition_3" value='1' <?php echo $row_special_beg['condition_3']=='1'?'checked':''; ?>>
					ขอกู้หักลบไม่ครบปี
			</div>
			<div class="form-group" style="padding:0px;margin:0px">
				<input type="checkbox" name="data[loan_special_beg][condition_4]" id="condition_4" value='1' onclick="choose_condition('4')" <?php echo $row_special_beg['condition_4']=='1'?'checked':''; ?>>
				<?php 
				if($row_special_beg['condition_4']=='1'){
					$readonly = '';
				}else{
					$readonly = 'readonly';
				}
				?>
				
					ขอผ่อนผันอายุของผู้กู้ระหว่างส่งหักจากอายุไม่เกิน 70 ปี เป็น
					<input type="number" class="form-control condition_4" name="data[loan_special_beg][condition_4_year]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_4_year']; ?>" <?php echo $readonly; ?>> 
					ปี ระยะเวลาส่งหัก
					<input type="number" class="form-control condition_4" name="data[loan_special_beg][condition_4_period]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_4_period']; ?>" <?php echo $readonly; ?>>
					งวด 
			</div>
			<div class="form-group" style="padding:0px;margin:0px">
				<input type="checkbox" name="data[loan_special_beg][condition_5]" id="condition_5" value='1' onclick="choose_condition('5')" <?php echo $row_special_beg['condition_5']=='1'?'checked':''; ?>>
				<?php 
				if($row_special_beg['condition_5']=='1'){
					$readonly = '';
				}else{
					$readonly = 'readonly';
				}
				?>
				
				ระยะเวลาการเป็นสมาชิก
				<input type="number" class="form-control condition_5" name="data[loan_special_beg][condition_5_member_month]" style="display:inline;width:100px;" value="<?php echo $row_special_beg['condition_5_member_month']; ?>" <?php echo $readonly; ?>> 
				เดือน ไม่เป็นไปตามหลักเกณฑ์
			</div>
			<div class="form-group" style="padding:0px;margin:0px">
				<input type="checkbox" name="data[loan_special_beg][condition_6]" id="condition_6" value='1' onclick="choose_condition('6')" <?php echo $row_special_beg['condition_6']=='1'?'checked':''; ?>>
				<?php 
				if($row_special_beg['condition_6']=='1'){
					$readonly = '';
				}else{
					$readonly = 'readonly';
				}
				?>
				อื่นๆ
				<input type="text" class="form-control condition_6" name="data[loan_special_beg][condition_6_detail]" style="display:inline;width:80%;" value="<?php echo $row_special_beg['condition_6_detail']; ?>" <?php echo $readonly; ?>>
			</div>
		</form>
									
				<div class="form-group " style="margin-top:20px;">
					<div class="col-sm-12 text-center">
						<button style="width: 123px;" id="submit_frm" onclick="submit_form()" class="btn btn-material-light-blue-300" >ส่งคำขอ</button>
					</div>
				</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<script>
function get_select_attr_value(id_from, attr_name, id_to){
		$('#'+id_to).val($('#'+id_from+' :selected').attr(attr_name));
	}
	function submit_form(){
		$('#frm').submit();
	}
	function choose_condition(id){
		if($('#condition_'+id).is(":checked")){
			$('.condition_'+id).removeAttr( "readonly" );
		}else{
			$('.condition_'+id).attr('readonly','readonly');
		}
	}
</script>
</body>
</HTML>
