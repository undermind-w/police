<?php
	include "config.inc.php";
	//require PATH."/class/connect_odbc.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}	
	
	foreach($_GET as $key => $value){
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	//echo '<pre>'; print_r($_GET); echo '</pre>'; exit;
	$pdf = new FPDI();
	
	//หาคำร้องขอกู้
	$sql_doc = "SELECT
			t1.loan_application_id,
			t1.loan_application_type,
			t3.description,
			t3.document_link
		FROM
			loan_application t1
		INNER JOIN loan_document_setting t2 ON t1.loan_application_type = t2.loan_type_id
		INNER JOIN loan_document t3 ON t2.document_id = t3.id
		WHERE
			t1.loan_application_id = '".$_GET['loan_application_id']."' 
		ORDER BY t3.order_by";
		$rs_doc= $mysqli->query($sql_doc);
		$arr_doc_use =array();
		while($row_doc = $rs_doc->fetch_assoc()){
			$arr_doc_use[]  = $row_doc;
			
		}
		
		
		$loan_application_type = array();
		$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
		$rs_type = $mysqli->query($sql_type);
		while($row_type = $rs_type->fetch_assoc()){			
			$loan_application_type[$row_type['id']] = $row_type['loan_name'];
		}
	
		$sql_loan = "SELECT * ,t3.loan_reason as member_use
						FROM 
							cmp_admin_loan_money t1
							INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
							LEFT JOIN loan_reason t3 ON t2.member_use = t3.id
						WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		//echo $sql_loan.'<hr>';
		
		//เอกสารแนบประกอบ
		$list_file = array();
		$sql_file= "SELECT list_file_id FROM loan_setting_detail_file WHERE loan_type_id = '".$row_loan['loan_application_type']."' AND list_file_type = '1'";
		$rs_file = $mysqli->query($sql_file);
		while($row_file = $rs_file->fetch_assoc()){	
			$list_file[$row_file['list_file_id']] = $row_file['list_file_id'];				 
		}
		
		$member_no = $row_loan['member_id'];
		$application_date = $row_loan["application_date"];

		$sql = "SELECT
				t1.*,
				CONCAT(
					t2.prename_full,
					t1.firstname_th,
					' ',
					t1.lastname_th
				) AS memname,
				t4.district_name,
				t5.amphur_name,
				t6.province_name,
				t7.mem_group_name
			FROM
				coop_mem_apply AS t1
			LEFT JOIN coop_prename AS t2 ON t1.prename_id = t2.prename_id
			LEFT JOIN coop_district as t4 ON t1.c_district_id = t4.district_id
			LEFT JOIN coop_amphur as t5 ON t1.c_amphur_id = t5.amphur_id
			LEFT JOIN coop_province as t6 ON t1.c_province_id = t6.province_id
			LEFT JOIN coop_mem_group as t7 ON t1.level = t7.id
			WHERE
				t1.member_id = '{$member_no}'";		
		//echo $sql.'<hr>';			
		$rs = $mysqli_app->query($sql);
		$row_member = $rs->fetch_assoc();							
	
	$sql_doc_all = "SELECT * FROM loan_document	ORDER BY order_by";
	$rs_doc_all= $mysqli->query($sql_doc_all);
	//while($row_doc_all = $rs_doc_all->fetch_assoc()){
		
		
	foreach($arr_doc_use AS $key=> $row_doc_all){	
		// echo '<pre>'; print_r($row_doc_all); echo '</pre>';
		//$document_link = $row_doc_all['document_link'];
		$document_link = $row_doc_all['document_link'];
		$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/".$document_link;
		$pageCount = $pdf->setSourceFile($filename);
		for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
			$pdf->AddPage();
			$tplIdx = $pdf->importPage($pageNo); 
			$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
			
			$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
			$pdf->SetFont('THSarabunNew', '', 13 );
			
			$border = 0;
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetAutoPageBreak(true,0);
			
			$pay_type = array('cash'=>'เงินสด', 'cheque'=>'เช็คธนาคาร', 'transfer'=>'เงินโอน');
			if($pageNo == '1'){
				$y_point = 16;
				$pdf->SetXY( 169, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_loan['loan_number']), $border, 1);//เลขที่สัญญา
				
				$y_point = 22;
				$pdf->SetXY( 161, $y_point );
				$pdf->MultiCell(40, 5, U2T(mysqldate2thaidate($application_date,0,0)), $border, 1);//วันที่
				
				$y_point = 49;
				$pdf->SetXY( 127, $y_point );
				$pdf->MultiCell(60, 5, U2T($row_loan['write_at']), $border, 1);
				
				$y_point = 57;
				$pdf->SetXY( 125, $y_point );
				$pdf->MultiCell(40, 5, U2T(mysqldate2thaidate($application_date,0,0)), $border, 1); //วันที
				
				$y_point = 67;
				$pdf->SetXY( 38, $y_point );
				$pdf->MultiCell(85, 5, U2T($row_member['memname']), $border, 1);
				$pdf->SetXY( 140, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['member_id']), $border, 1);
				
				$y_point = 74;
				$pdf->SetXY( 51, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['id_card']), $border, 1);
				$pdf->SetXY( 106, $y_point );
				$pdf->MultiCell(11, 5, U2T(cal_age($row_member['birthday'])), $border, 'C');
				$pdf->SetXY( 158, $y_point );
				$pdf->MultiCell(20, 5, U2T($row_member['c_address_no']), $border, 1);			
				
				$y_point = 80.5;
				$pdf->SetXY( 24, $y_point );
				$pdf->MultiCell(15, 5, U2T($row_member['c_address_moo']), $border, 1);
				$pdf->SetXY( 42, $y_point );
				$pdf->MultiCell(38, 5, U2T($row_member['district_name']), $border, 1);
				$pdf->SetXY( 77, $y_point );
				$pdf->MultiCell(38, 5, U2T($row_member['amphur_name']), $border, 1);
				$pdf->SetXY( 114, $y_point );
				$pdf->MultiCell(38, 5, U2T($row_member['province_name']), $border, 1);
				$pdf->SetXY( 171, $y_point );
				$pdf->MultiCell(38, 5, U2T($row_member['c_zipcode']), $border, 1);
				
				$y_point = 87;
				$pdf->SetXY( 61, $y_point );
				$pdf->MultiCell(60, 5, U2T($row_member['position']), $border, 1);
				$pdf->SetXY( 139, $y_point );
				$pdf->MultiCell(60, 5, U2T($row_member['mem_group_name']), $border, 1);
				
				$y_point = 93;
				$pdf->SetXY( 137, $y_point );
				$pdf->MultiCell(30, 5, U2T($row_member['tel']), $border, 1);
				
				$y_point = 100.5;
				$pdf->SetXY( 36, $y_point );
				$pdf->MultiCell(30, 5, U2T($row_member['mobile']), $border, 1);
				$pdf->SetXY( 140, $y_point );
				$pdf->MultiCell(35, 5, U2T(num_format($row_member['salary'])), $border, 'R');
				
				$y_point = 127;
				$pdf->SetXY( 76, $y_point );
				$pdf->MultiCell(31, 5, U2T(num_format($row_loan['loan_money'])), $border, 'R');
				$pdf->SetXY( 115, $y_point );
				$pdf->MultiCell(76, 5, U2T($row_loan['loan_money_text']), $border, 'C');
				
				$y_point = 134;
				$pdf->SetXY( 62, $y_point );
				$pdf->MultiCell(120, 5, U2T($row_loan['member_use']), $border, 1);
			}else if($pageNo == '2'){
				$y_point = 135;
				$pdf->SetXY( 123, $y_point );
				$pdf->MultiCell(63, 5, U2T($row_member['firstname_th']." ".$row_member['lastname_th']), $border, 'C');
				
				$y_point = 264;
				$pdf->SetXY( 98, $y_point );
				$pdf->MultiCell(53, 5, U2T($row_member['firstname_th']." ".$row_member['lastname_th']), $border, 'C');
			}else if($pageNo == '3'){
				$y_point = 47;
				$pdf->SetXY( 117, $y_point );
				$pdf->MultiCell(40, 5, U2T(mysqldate2thaidate($application_date,0,0)), $border, 1);
				
				$y_point = 62;
				$pdf->SetXY( 57, $y_point );
				$pdf->MultiCell(85, 5, U2T($row_member['memname']), $border, 1);
				$pdf->SetXY( 152, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['member_id']), $border, 1);
				
				$y_point = 69;
				$pdf->SetXY( 55, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['id_card']), $border, 1);
				$pdf->SetXY( 115, $y_point );
				$pdf->MultiCell(11, 5, U2T(cal_age($row_member['birthday'])), $border, 'C');
				$pdf->SetXY( 162, $y_point );
				$pdf->MultiCell(20, 5, U2T($row_member['c_address_no']), $border, 1);
				
				$y_point = 76;
				$pdf->SetXY( 23, $y_point );
				$pdf->MultiCell(10, 5, U2T($row_member['c_address_moo']), $border, 'C');
				$pdf->SetXY( 42, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['district_name']), $border, 1);
				$pdf->SetXY( 93, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['amphur_name']), $border, 1);
				$pdf->SetXY( 145, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['province_name']), $border, 1);
				
				$y_point = 83;
				$pdf->SetXY( 37, $y_point );
				$pdf->MultiCell(25, 5, U2T($row_member['c_zipcode']), $border, 1);
				$pdf->SetXY( 113, $y_point );
				$pdf->MultiCell(80, 5, U2T($row_member['position']), $border, 1);
				
				$y_point = 90;
				$pdf->SetXY( 40, $y_point );
				$pdf->MultiCell(60, 5, U2T($row_member['mem_group_name']), $border, 1);
				
				$y_point = 97;
				$pdf->SetXY( 37, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['tel']), $border, 1);
				$pdf->SetXY( 93, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['mobile']), $border, 1);
				$pdf->SetXY( 140, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['email']), $border, 1);
				
				$y_point = 112;
				$pdf->SetXY( 56, $y_point );
				$pdf->MultiCell(31, 5, U2T(num_format($row_loan['loan_money'])), $border, 'R');
				$pdf->SetXY( 95, $y_point );
				$pdf->MultiCell(73, 5, U2T($row_loan['loan_money_text']), $border, 'C');
				
				$y_point = 119;
				$pdf->SetXY( 42, $y_point );
				$pdf->MultiCell(31, 5, U2T($data['contract_number']), $border, 1);
				
				$y_point = 126;
				$pdf->SetXY( 96, $y_point );
				$pdf->MultiCell(31, 5, U2T(num_format($row_loan['loan_money'])), $border, 'R');
				$pdf->SetXY( 135, $y_point );
				$pdf->MultiCell(53, 5, U2T($row_loan['loan_money_text']), $border, 'C');
				
				$y_point = 190.5;
				$pdf->SetXY( 52, $y_point );
				$pdf->MultiCell(20, 5, U2T($row_loan['period']), $border, 'R');
				
				$y_point = 219;
				$pdf->SetXY( 103, $y_point );
				$pdf->MultiCell(30, 5, U2T($row_loan['estimate_money_period']), $border, 'R');
				
				$y_point = 265;
				$pdf->SetXY( 26, $y_point );
				$pdf->MultiCell(67, 5, U2T($row_member['firstname_th']." ".$row_member['lastname_th']), $border, 'C');
				
			}
		}	
	}	
	
	$pdf->Output();