<?php
	include "config.inc.php";
	require PATH . "/class/pagination.class.php"  ;
	function date_thai_format($date,$long_month='L',$with_time="1"){
		$long_month_arr = array('1'=>'มกราคม','2'=>'กุุมภาพันธ์','3'=>'มีนาคม','4'=>'เมษายน','5'=>'พฤษภาคม','6'=>'มิถุนายน','7'=>'กรกฎาคม','8'=>'สิงหาคม','9'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
		$short_month_arr = array('1'=>'ม.ค.','2'=>'ก.พ.','3'=>'มี.ค.','4'=>'เม.ย.','5'=>'พ.ค.','6'=>'มิ.ย.','7'=>'ก.ค.','8'=>'ส.ค.','9'=>'ก.ย.','10'=>'ต.ค.','11'=>'พ.ย.','12'=>'ธ.ค.');
		
		$day = date('d',strtotime($date));
		$month = date('m',strtotime($date));
		$year = date('Y',strtotime($date))+543;
		$time = date('H:i น.',strtotime($date));
		
		if($long_month == 'L'){
			$text = (int)$day." ".$long_month_arr[(int)$month]." ".$year;
		}else{
			$text = (int)$day." ".$short_month_arr[(int)$month]." ".$year;
		}
		if($with_time == '1'){
			$text .= " ".$time;
		}
		return $text;
	}
	$param = '';
	foreach($_GET as $key => $value){
		$param .= $key."&";
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	$param = substr($param,0,-1);
?>
<HTML>
<head>
 <?php
	include "header_mobile.inc.php"
 ?>
<style>
	body {
		background-color:#FFF;
	}
	.panel-heading {
		border-bottom:1px solid;
	}
	.menu_gruop{
		margin:5px;
	}
	.icon_color{
		color: rgb(235,6,141);
	}
	.menu_link:active{
		 background-color:rgb(212,212,212);;
	} 
	h3{
		margin-top:12px;
		margin-bottom:12px;
	}
	.tr_border{
		border-bottom:1px solid rgb(212,212,212);
	}
	.td_style_icon{
			padding: 10px;
		}
		.td_style{
			padding-top: 10px;
			padding-bottom: 10px;
		}
	.text_show{
		font-size: 14px;
	}
	.date_show{
		
		color: #AAA;
		font-size: 12px;
	}
	body{
		font-family:"Sukhumvit Set",sans-serif !important;
	}
</style>
</head>
<body>
<?php 
	$sql = "SELECT 
		* 
	FROM 
		cmp_admin_loan_money t1
		INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id
	WHERE 
		t1.loan_id='".$_GET['loan_id']."'";
	$rs = $mysqli->query($sql);
	$row = $rs->fetch_assoc();
?>
<center>
<?php 
$icon_success = 'fa fa-circle fa-stack-1x text-material-light-blue-300'; 
$icon_inprogress = 'fa fa-circle-thin fa-stack-1x text-material-light-blue-300';
?>
<table width="100%" >
	<?php
	$sql_status = "SELECT * FROM cmp_admin_loan_money_status WHERE loan_id = '".$row['loan_id']."' ORDER BY status_date DESC";
	//echo $sql_status;
	$rs_status = $mysqli->query($sql_status);
	while($row_status = $rs_status->fetch_assoc()){
	?>
	<tr class="tr_border">
		<td width="20%" align="center" valign="middle">
			<span class="icon_color fa-stack fa-1x">
					<i class="<?php echo $icon_success; ?>"></i>
			</span>
		</td>
		<td valign="middle">
			<span class="text_show"><?php echo $row_status['status_name']; ?></span>
			<br>
			<span class="date_show"><?php echo date_thai_format($row_status['status_date']); ?></span>
		</td>
	</tr>
	<?php } ?>
</table>
<div class="row">
	<div style="margin-top: 1.5em;" class="text-center">
		<div class="col-sm-12">
			<div class="col-sm-12" style="margin-bottom:15px;">
				<a onclick="link_click('loan_check_status_menu_mobile.php?<?php echo base64_encode("member_no=".$row['member_id']); ?>')">
					<button style="font-size: 22px;font-family: 'thaisans_neueregular' !important;width:70%" type="button" class="btn btn-material-light-blue-300">ย้อนกลับ</button>
				</a>
			</div>
		</div>
	</div>
</div>
</center>
<script>
function link_click(link){
	location.href = link;
}
</script>
</body>
</HTML>
