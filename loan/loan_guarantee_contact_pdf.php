<?php
	include "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}
	function cal_age($birthday,$type = 'y'){     //รูปแบบการเก็บค่าข้อมูลวันเกิด
		$birthday = date("Y-m-d",strtotime($birthday)); 
		$today = date("Y-m-d");   //จุดต้องเปลี่ยน
		list($byear, $bmonth, $bday)= explode("-",$birthday);       //จุดต้องเปลี่ยน
		list($tyear, $tmonth, $tday)= explode("-",$today);                //จุดต้องเปลี่ยน
		$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear);
		$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
		$mage = ($mnow - $mbirthday);
		//echo "วันเกิด $birthday"."<br>\n";
		//echo "วันที่ปัจจุบัน $today"."<br>\n";
		//echo "รับค่า $mage"."<br>\n";
		$u_y=date("Y", $mage)-1970;
		$u_m=date("m",$mage)-1;
		$u_d=date("d",$mage)-1;
		if($type=='y'){
			return $u_y;
		}else if($type=='m'){
			return $u_m;
		}else{
			return $u_d;
		}
	}

	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/guarantee_contract_1.pdf" ;
	//$_GET['loan_application_id'] = base64_decode($_GET['loan_application_id']);
	//$_GET['bondsman_id'] = base64_decode($_GET['bondsman_id']);
	
	foreach($_GET as $key => $value){
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	//exit;
	
	$pdf = new FPDI();
	
	$pageCount_1 = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount_1; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* ,t4.loan_reason as member_use
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_result t3 ON t1.loan_id = t3.loan_id
			LEFT JOIN loan_reason t4 ON t2.member_use = t4.id
			WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE bondsman_id = '".$_GET['bondsman_id']."'";
		$rs_bondsman = $mysqli->query($sql_bondsman);
		$row_bondsman = $rs_bondsman->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member t1
		LEFT JOIN cmp_imp_address t2 ON t1.member_no = t2.member_no
		WHERE t1.member_no = '".$row_bondsman['bondsman_number']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 89;
			$pdf->SetXY( 133, $y_point );
			$pdf->MultiCell(50, 5, U2T(format_date($row_loan['application_date'])), $border, 1);
			
			$y_point = 95;
			$pdf->SetXY( 36, $y_point );
			$pdf->MultiCell(160, 5, U2T($row_bondsman['bondsman_name']), $border, 1);
			
			$y_point = 102;
			$pdf->SetXY( 30, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_bondsman['bondsman_number']), $border, 1);
			$pdf->SetXY( 85, $y_point );
			$pdf->MultiCell(18, 5, U2T(cal_age($row_member['birthdtm'])), $border, 'R');
			$pdf->SetXY( 140, $y_point );
			$pdf->MultiCell(50, 5, U2T($row_member['card_person']), $border, 1);
			
			$y_point = 109;
			$pdf->SetXY( 25, $y_point );
			$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_position']), $border, 1);
			$pdf->SetXY( 94, $y_point );
			$pdf->MultiCell(100, 5, U2T($row_bondsman['bondsman_affiliation']), $border, 1);
			
			$y_point = 116;
			$pdf->SetXY( 44, $y_point );
			$pdf->MultiCell(30, 5, U2T($row_member['memb_addr']), $border, 1);
			$pdf->SetXY( 82, $y_point );
			$pdf->MultiCell(12, 5, U2T($row_member['addr_group']), $border, 1);
			$pdf->SetXY( 102, $y_point );
			$pdf->MultiCell(28, 5, U2T($row_member['soi']), $border, 1);
			$pdf->SetXY( 138, $y_point );
			$pdf->MultiCell(60, 5, U2T($row_member['road']), $border, 1);
			
			$y_point = 122;
			$pdf->SetXY( 29, $y_point );
			$pdf->MultiCell(45, 5, U2T($row_member['tambol']), $border, 1);
			$pdf->SetXY( 89, $y_point );
			$pdf->MultiCell(50, 5, U2T($row_member['district_desc']), $border, 1);
			$pdf->SetXY( 149, $y_point );
			$pdf->MultiCell(50, 5, U2T($row_member['province_desc']), $border, 1);
			
			$y_point = 129;
			$pdf->SetXY( 32, $y_point );
			$pdf->MultiCell(32, 5, U2T($row_member['postcode']), $border, 1);
			$pdf->SetXY( 77, $y_point );
			$pdf->MultiCell(32, 5, U2T($row_loan['member_tel']), $border, 1);
			
			$y_point = 142;
			$pdf->SetXY( 44, $y_point );
			$pdf->MultiCell(75, 5, U2T($row_loan['member_name']), $border, 1);
			
			$y_point = 149;
			$pdf->SetXY( 23, $y_point );
			$pdf->MultiCell(33, 5, U2T(num_format($row_loan['real_money'])), $border, 'R');
			$pdf->SetXY( 65, $y_point );
			$pdf->MultiCell(61, 5, U2T(convert($row_loan['real_money'])), $border, 'C');
			$pdf->SetXY( 135, $y_point );
			$pdf->MultiCell(61, 5, U2T($row_loan['member_use']), $border, 1);
			
			$y_point = 155;
			$pdf->SetXY( 68, $y_point );
			$pdf->MultiCell(48, 5, U2T(''), $border, 1);//เลขที่หนังสือ
			$pdf->SetXY( 128, $y_point );
			$pdf->MultiCell(17, 5, U2T(''), $border, 1);
			$pdf->SetXY( 153, $y_point );
			$pdf->MultiCell(17, 5, U2T(''), $border, 1);
			$pdf->SetXY( 178, $y_point );
			$pdf->MultiCell(17, 5, U2T(''), $border, 1);
			
			$y_point = 162;
			$pdf->SetXY( 23, $y_point );
			$pdf->MultiCell(21, 5, U2T(''), $border, 1);
			$pdf->SetXY( 53, $y_point );
			$pdf->MultiCell(20, 5, U2T(''), $border, 1);//เลขที่หนังสือ
			$pdf->SetXY( 79, $y_point );
			$pdf->MultiCell(20, 5, U2T(''), $border, 1);//เลขที่หนังสือ
			
			$y_point = 175;
			$pdf->SetXY( 177, $y_point );
			//$pdf->MultiCell(25, 5, U2T(num_format($row_loan['loan_approve_money'])), $border, 1);
			
			$y_point = 182;
			$pdf->SetXY( 21, $y_point );
			//$pdf->MultiCell(55, 5, U2T(convert($row_loan['loan_approve_money'])), $border, 'C');
			
			$y_point = 189;
			$pdf->SetXY( 133, $y_point );
			$pdf->MultiCell(65, 5, U2T($row_member['memb_addr']), $border, 1);
			
			$y_point = 196;
			$pdf->SetXY( 20, $y_point );
			$pdf->MultiCell(16, 5, U2T($row_member['addr_group']), $border, 1);
			$pdf->SetXY( 42, $y_point );
			$pdf->MultiCell(18, 5, U2T($row_member['soi']), $border, 1);
			$pdf->SetXY( 67, $y_point );
			$pdf->MultiCell(33, 5, U2T($row_member['road']), $border, 1);
			$pdf->SetXY( 116, $y_point );
			$pdf->MultiCell(34, 5, U2T($row_member['tambol']), $border, 1);
			$pdf->SetXY( 165, $y_point );
			$pdf->MultiCell(34, 5, U2T($row_member['district_desc']), $border, 1);
			
			$y_point = 202;
			$pdf->SetXY( 23, $y_point );
			$pdf->MultiCell(42, 5, U2T($row_member['province_desc']), $border, 1);
			$pdf->SetXY( 84, $y_point );
			$pdf->MultiCell(30, 5, U2T($row_member['postcode']), $border, 1);
			
		}
		
	}
	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/guarantee_contract_2.pdf" ;
	$pageCount_2 = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount_2; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* 
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_result t3 ON t1.loan_id = t3.loan_id
			WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE bondsman_id = '".$_GET['bondsman_id']."'";
		$rs_bondsman = $mysqli->query($sql_bondsman);
		$row_bondsman = $rs_bondsman->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member t1
		LEFT JOIN cmp_imp_address t2 ON t1.member_no = t2.member_no
		WHERE t1.member_no = '".$row_bondsman['bondsman_number']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 119;
			$pdf->SetXY( 80, $y_point );
			$pdf->MultiCell(63, 5, U2T($row_bondsman['bondsman_name']), $border, 'C');
			
		}
		
	}
	
	$pdf->Output();