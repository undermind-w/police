<?php
	include "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	//function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function cal_age($birthday,$type = 'y'){     //รูปแบบการเก็บค่าข้อมูลวันเกิด
		$birthday = date("Y-m-d",strtotime($birthday)); 
		$today = date("Y-m-d");   //จุดต้องเปลี่ยน
		list($byear, $bmonth, $bday)= explode("-",$birthday);       //จุดต้องเปลี่ยน
		list($tyear, $tmonth, $tday)= explode("-",$today);                //จุดต้องเปลี่ยน
		$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear);
		$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
		$mage = ($mnow - $mbirthday);
		//echo "วันเกิด $birthday"."<br>\n";
		//echo "วันที่ปัจจุบัน $today"."<br>\n";
		//echo "รับค่า $mage"."<br>\n";
		$u_y=date("Y", $mage)-1970;
		$u_m=date("m",$mage)-1;
		$u_d=date("d",$mage)-1;
		if($type=='y'){
			return $u_y;
		}else if($type=='m'){
			return $u_m;
		}else{
			return $u_d;
		}
	}
	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_special_beg.pdf" ;
	
	
	$pdf = new FPDI();
	
	$pageCount = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$sql_loan = "SELECT 
			* 
		FROM 
			loan_special_beg t1
			INNER JOIN cmp_admin_loan_money t2 ON t1.loan_id = t2.loan_id 
			INNER JOIN loan_application t3 ON t1.loan_id = t3.loan_id 
		WHERE t1.id = '".$_GET['id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member WHERE member_no = '".$row_loan['member_id']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 26;
			$pdf->SetXY( 145, $y_point );
			$pdf->MultiCell(65, 5, U2T($row_loan['write_at']), $border, 1);//เขียนที่
			
			$y_point = 34;
			$pdf->SetXY( 145, $y_point );
			$pdf->MultiCell(80, 5, format_date(U2T($row_loan['application_date']), $border, 1));//วันที่
			
			$y_point = 56;
			$pdf->SetXY( 48, $y_point );
			$pdf->MultiCell(95, 5, U2T($row_loan['member_name']), $border, 1);//ชื่อผู้กู้
			$pdf->SetXY( 146, $y_point );
			$pdf->MultiCell(15, 5, U2T(cal_age($row_member['birthdtm'])), $border, 'R');
			
			$y_point = 64;
			$pdf->SetXY( 86, $y_point );
			$pdf->MultiCell(30, 5, U2T($row_loan['member_id']), $border, 1);
			$pdf->SetXY( 136, $y_point );
			$pdf->MultiCell(21, 5, U2T($row_loan['employee_id']), $border, 1);
			$pdf->SetXY( 169, $y_point );
			$pdf->MultiCell(35, 5, U2T($row_loan['member_position']), $border, 1);
			
			$y_point = 72;
			$pdf->SetXY( 43, $y_point );
			$pdf->MultiCell(29, 5, U2T($row_loan['member_salary']), $border, 'R');
			$pdf->SetXY( 106, $y_point );
			$pdf->MultiCell(37, 5, U2T($row_loan['member_branch_name']), $border, 1);
			$pdf->SetXY( 156, $y_point );
			$pdf->MultiCell(37, 5, U2T($row_loan['member_sector_name']), $border, 1);
			
			$y_point = 80;
			$pdf->SetXY( 37, $y_point );
			$pdf->MultiCell(26, 5, U2T($row_loan['member_department_name']), $border, 1);
			$pdf->SetXY( 75, $y_point );
			$pdf->MultiCell(24, 5, U2T($row_loan['member_tel']), $border, 1);
			$pdf->SetXY( 128, $y_point );
			$pdf->MultiCell(13, 5, U2T(cal_age($row_member['memdtm'])), $border, 'R');
			$pdf->SetXY( 145, $y_point );
			$pdf->MultiCell(19, 5, U2T(cal_age($row_member['memdtm'],'m')), $border, 'R');
			
			$y_point = 87;
			$pdf->SetXY( 28, $y_point );
			$pdf->MultiCell(50, 5, U2T(number_format($row_loan['loan_money'],2)), $border, 'R');
			$pdf->SetXY( 85, $y_point );
			$pdf->MultiCell(53, 5, U2T($row_loan['member_use']), $border, 1);
			
			if($row_loan['condition_1']=='1'){
				$y_point = 96;
				$pdf->SetXY( 32, $y_point );
				$pdf->MultiCell(5, 5, '/', $border, 1);
				
				$y_point = 102;
				$pdf->SetXY( 68, $y_point );
				$pdf->MultiCell(35, 5, U2T($row_loan['condition_1_extra_income']), $border, 1);
				$pdf->SetXY( 113, $y_point );
				$pdf->MultiCell(24, 5, U2T(number_format($row_loan['condition_1_extra_income_per_month'],2)), $border, 'R');
				$pdf->SetXY( 162, $y_point );
				$pdf->MultiCell(24, 5, U2T(number_format($row_loan['condition_1_mariage_income'],2)), $border, 'R');
			}
			
			if($row_loan['condition_2']=='1'){
				$y_point = 111;
				$pdf->SetXY( 32, $y_point );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}
			
			if($row_loan['condition_3']=='1'){
				$y_point = 119;
				$pdf->SetXY( 32, $y_point );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}
			
			if($row_loan['condition_4']=='1'){
				$y_point = 126;
				$pdf->SetXY( 32, $y_point );
				$pdf->MultiCell(5, 5, '/', $border, 1);
				$pdf->SetXY( 119, $y_point );
				$pdf->MultiCell(7, 5, $row_loan['condition_4_year'], $border, 'R');
				$pdf->SetXY( 150, $y_point );
				$pdf->MultiCell(10, 5, $row_loan['condition_4_period'], $border, 'R');
			}
			
			if($row_loan['condition_5']=='1'){
				$y_point = 134;
				$pdf->SetXY( 32, $y_point );
				$pdf->MultiCell(5, 5, '/', $border, 1);
				$pdf->SetXY( 70, $y_point-1 );
				$pdf->MultiCell(25, 5, $row_loan['condition_5_member_month'], $border, 'R');
			}
			
			if($row_loan['condition_6']=='1'){
				$y_point = 142;
				$pdf->SetXY( 32, $y_point );
				$pdf->MultiCell(5, 5, '/', $border, 1);
				$pdf->SetXY( 44, $y_point-1 );
				$pdf->MultiCell(160, 5, $row_loan['condition_6_detail'], $border, 1);
			}
			
			$y_point = 179;
			$pdf->SetXY( 95, $y_point );
			$pdf->MultiCell(55, 5, U2T($row_loan['member_name']), $border, 'C');//ชื่อผู้กู้
			
			$y_point = 195;
			$pdf->SetXY( 43, $y_point );
			$pdf->MultiCell(44, 5, U2T($row_loan['member_name']), $border, 1);//ชื่อผู้กู้
			$pdf->SetXY( 103, $y_point );
			$pdf->MultiCell(7, 5, U2T(cal_age($row_member['memdtm'])), $border, 'R');
			$pdf->SetXY( 112, $y_point );
			$pdf->MultiCell(10, 5, U2T(cal_age($row_member['memdtm'],'m')), $border, 'R');
			$pdf->SetXY( 147, $y_point );
			$pdf->MultiCell(33, 5, U2T(number_format($row_loan['loan_money'],2)), $border, 'R');
			
		}else if($pageNo == '2'){
			
		}
		
	}
	
	$pdf->Output();