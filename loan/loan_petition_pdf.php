<?php
	include "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	//function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}

	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_petition.pdf" ;
	
	//$_GET['loan_application_id'] = base64_decode($_GET['loan_application_id']);
	foreach($_GET as $key => $value){
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	$pdf = new FPDI();
	
	$pageCount = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* ,t3.loan_reason as member_use
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_reason t3 ON t2.member_use = t3.id
		WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 40;
			$pdf->SetXY( 14, $y_point );
			$pdf->MultiCell(65, 5, U2T('เลขที่คำขอกู้เงินออนไลน์ '.$row_loan['loan_number']), $border, 1);
			
			$y_point = 52;
			$pdf->SetXY( 146, $y_point );
			$pdf->MultiCell(65, 5, U2T($row_loan['write_at']), $border, 1);//เขียนที่
			
			$y_point = 60;
			$pdf->SetXY( 146, $y_point );
			$pdf->MultiCell(80, 5, format_date(U2T($row_loan['application_date']), $border, 1));//วันที่
			
			$y_point = 79;
			$pdf->SetXY( 36, $y_point );
			$pdf->MultiCell(60, 5, U2T($row_loan['member_name']), $border, 1);//ชื่อผู้กู้
			$pdf->SetXY( 116, $y_point );
			$pdf->MultiCell(30, 5, U2T($row_loan['member_id']), $border, 1);//เลขที่สมาชิกผู้กู้
			$pdf->SetXY( 160, $y_point );
			$pdf->MultiCell(45, 5, U2T($row_loan['member_position']), $border, 1);//ตำแหน่งผู้กู้
			
			$y_point = 87;
			$pdf->SetXY( 57, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_loan['card_id']), $border, 1);//เลขประจำตัว ปชช
			
			$pdf->SetXY( 105, $y_point );
			$pdf->MultiCell(20, 5, U2T($row_loan['employee_id']), $border, 1);
			
			$y_point = 95;
			if(U2T($row_loan['member_branch']) == '1' && U2T($row_loan['member_branch_name']) != ''){
				$pdf->SetXY( 23, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}else if(U2T($row_loan['member_branch']) == '2' && U2T($row_loan['member_branch_name']) != ''){
				$pdf->SetXY( 44, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}
			
			$pdf->SetXY( 58, $y_point );
			$pdf->MultiCell(150, 5, U2T($row_loan['member_branch_name']), $border, 1);
			
			$y_point = 103;
			if(U2T($row_loan['member_sector']) == '1' && U2T($row_loan['member_sector_name']) != ''){
				$pdf->SetXY( 14, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}else if(U2T($row_loan['member_sector']) == '2' && U2T($row_loan['member_sector_name']) != ''){
				$pdf->SetXY( 29, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}
			$pdf->SetXY( 41, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_loan['member_sector_name']), $border, 1);
			
			if(U2T($row_loan['member_department']) == '1' && U2T($row_loan['member_department_name']) != ''){
				$pdf->SetXY( 84, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}else if(U2T($row_loan['member_department']) == '2' && U2T($row_loan['member_department_name']) != ''){
				$pdf->SetXY( 102, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}
			$pdf->SetXY( 114, $y_point );
			$pdf->MultiCell(35, 5, U2T($row_loan['member_department_name']), $border, 1);
			$pdf->SetXY( 160, $y_point );
			$pdf->MultiCell(35, 5, U2T($row_loan['member_tel']), $border, 1);
			
			$y_point = 111;
			$pdf->SetXY( 50, $y_point );
			$pdf->MultiCell(35, 5, U2T(num_format($row_loan['member_salary'])), $border, 'R');
			
			$y_point = 118;
			$pdf->SetXY( 102, $y_point );
			$pdf->MultiCell(35, 5, U2T(num_format($row_loan['loan_money'])), $border, 'R');
			$pdf->SetXY( 144, $y_point );
			$pdf->MultiCell(53, 5, U2T($row_loan['loan_money_text']), $border, 'C');
			$y_point = 133;
			$pdf->SetXY( 12, $y_point );
			$pdf->MultiCell(185, 5, U2T($row_loan['member_use']), $border, 1);
			
			$y_point = 141;
			$pdf->SetXY( 98, $y_point );
			$pdf->MultiCell(25, 5, U2T(num_format(($row_loan['member_money']/10))), $border, 'R');
			$pdf->SetXY( 143, $y_point );
			$pdf->MultiCell(47, 5, U2T(num_format($row_loan['member_money'])), $border, 'R');
			
			$y_point = 151;
			$pdf->SetXY( 80, $y_point );
			//$pdf->MultiCell(54, 5, U2T(num_format($row_loan['share_rate'])), $border, 'R');
			
		if($row_loan['guarantee_type']=='1'){
			$y_point = 203;
			$pdf->SetXY( 41, $y_point );
			$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
		}
			
			$y_point = 237;
			
			$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_application_id = '".$_GET['loan_application_id']."' ORDER BY bondsman_num ASC";
			$rs_bondsman = $mysqli->query($sql_bondsman);
			
			while($row_bondsman = $rs_bondsman->fetch_assoc()){
				if($row_bondsman['bondsman_name'] != ''){
				$pdf->SetXY( 11, $y_point );
				$pdf->MultiCell(14, 5, U2T($row_bondsman['bondsman_num']), $border, 'C');
				$pdf->SetXY( 25, $y_point );
				$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_name']), $border, 1);
				$pdf->SetXY( 25, $y_point+6 );
				//$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_position']." ".$row_bondsman['bondsman_affiliation']), $border, 1);
				$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_tel']), $border, 1);
				$pdf->SetXY( 85, $y_point );
				$pdf->MultiCell(22, 5, U2T($row_bondsman['bondsman_number']), $border, 'C');
				$pdf->SetXY( 108, $y_point );
				$pdf->MultiCell(25, 5, U2T(num_format($row_bondsman['bondsman_salary'])), $border, 'R');
				$pdf->SetXY( 133, $y_point );
				$pdf->MultiCell(30, 5, '', $border, 1);
				$pdf->SetXY( 163, $y_point );
				$pdf->MultiCell(34, 5, '', $border, 1);
				$y_point += 15;
				}
			}
		}else if($pageNo == '2'){
			$y_point = 29;
			if($row_loan['guarantee_type']=='4'){
				$pdf->SetXY( 41, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			$y_point = 28;
			$pdf->SetXY( 112, $y_point );
			$pdf->MultiCell(46, 5, U2T(num_format($row_loan['guarantee_share_value'])), $border, 'R');
			
			
			
			$y_point = 36;
			if($row_loan['guarantee_type']=='2'){
				$pdf->SetXY( 41, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			$pdf->SetXY( 102, $y_point );
			$pdf->MultiCell(37, 5, U2T($row_loan['account_bank_type']), $border, 1);
			$pdf->SetXY( 147, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_loan['account_bank_number']), $border, 1);
			
			$y_point = 43;
			$pdf->SetXY( 64, $y_point );
			$pdf->MultiCell(46, 5, U2T(num_format($row_loan['deposit_amount'])), $border, 'R');
			
			$y_point = 52;
			if($row_loan['guarantee_type']=='3'){
				$pdf->SetXY( 41, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			$y_point = 51;
			$pdf->SetXY( 64, $y_point );
			$pdf->MultiCell(120, 5, U2T(num_format($row_loan['guarantee_position'])), $border, 'R');
			
			$y_point = 67;
			$pdf->SetXY( 103, $y_point );
			//$pdf->MultiCell(40, 5, U2T($row_loan['daposit_issue']), $border, 'R');
			$pdf->MultiCell(40, 5, U2T(''), $border, 'R');
		}
		
	}
	
	$pdf->Output();