<?php
	include "config.inc.php";
	$smarty->assign("title", "คำขอกู้เงินสามัญ" );
if ($_POST) {
	if ($_POST['add_by_user']) {
		header("Location: ".$_POST['add_by_user']);
		exit();
	}
	  $do                     = @$mysqli->real_escape_string(@$_POST["do"]);
	  $loan_id                = @$mysqli->real_escape_string(@$_POST["loan_id"]);
	  $loan_application_type  = @$mysqli->real_escape_string(@$_POST["loan_application_type"]);
	  $write_at               = @$mysqli->real_escape_string(@$_POST["write_at"]);
	  $application_date       = @$mysqli->real_escape_string(ConvertToSQLDate(@$_POST["application_date"]));
	  $member_name            = @$mysqli->real_escape_string(@$_POST["member_name"]);
	  $member_id              = @$mysqli->real_escape_string(@$_POST["member_id"]);
	  $member_position        = @$mysqli->real_escape_string(@$_POST["member_position"]);
	  $address                = @$mysqli->real_escape_string(@$_POST["address"]);
	  $card_id                = @$mysqli->real_escape_string(@$_POST["card_id"]);
	  $member_code            = @$mysqli->real_escape_string(@$_POST["member_code"]);
	  $member_branch 		  = @$mysqli->real_escape_string(@$_POST["member_branch"]);
	  $member_branch_name 	  = @$mysqli->real_escape_string(@$_POST["member_branch_name"]);
	  $member_sector 		  = @$mysqli->real_escape_string(@$_POST["member_sector"]);
	  $member_sector_name 	  = @$mysqli->real_escape_string(@$_POST["member_sector_name"]);
	  $member_department 	  = @$mysqli->real_escape_string(@$_POST["member_department"]);
	  $member_department_name = @$mysqli->real_escape_string(@$_POST["member_department_name"]);
	  $member_tel             = @$mysqli->real_escape_string(@$_POST["member_tel"]);
	  $address_card_id        = @$mysqli->real_escape_string(@$_POST["address_card_id"]);
	  $member_salary          = @$mysqli->real_escape_string(@$_POST["member_salary"]);
	  $loan_money             = @$mysqli->real_escape_string(@$_POST["loan_money"]);
	  $loan_money_text        = @$mysqli->real_escape_string(@$_POST["loan_money_text"]);
	  $member_use             = @$mysqli->real_escape_string(@$_POST["member_use"]); 
	  $member_share           = @$mysqli->real_escape_string(@$_POST["member_share"]); 
	  $member_money           = @$mysqli->real_escape_string(@$_POST["member_money"]);
      $share_rate             = @$mysqli->real_escape_string(@$_POST["share_rate"]);
	  $book_loan_1            = @$mysqli->real_escape_string(@$_POST["book_loan_1"]);  // หนังสือที่.../...
	  $book_loan_2            = @$mysqli->real_escape_string(@$_POST["book_loan_2"]);  
	  $loan_date              = @$mysqli->real_escape_string(ConvertToSQLDate(@$_POST["loan_date"]));
	  $loan_for               = @$mysqli->real_escape_string(@$_POST["loan_for"]); 
	  $loan_remain            = @$mysqli->real_escape_string(@$_POST["loan_remain"]); 
	  $insurance_type         = @$mysqli->real_escape_string(@$_POST["insurance_type"]); 
	  $share_money            = @$mysqli->real_escape_string(@$_POST["share_money"]);
	  $deposit_type           = @$mysqli->real_escape_string(@$_POST["deposit_type"]);
	  $deposit_no             = @$mysqli->real_escape_string(@$_POST["deposit_no"]);
	  $deposit_amount         = @$mysqli->real_escape_string(@$_POST["deposit_amount"]);
	  $daposit_issue          = @$mysqli->real_escape_string(@$_POST["daposit_issue"]); 
	  $period                 = @$mysqli->real_escape_string(@$_POST["period"]); 
	  $interest_rate          = @$mysqli->real_escape_string(@$_POST["interest_rate"]); 
	  $interest_period        = @$mysqli->real_escape_string(@$_POST["interest_period"]); 
	  $account_bank           = @$mysqli->real_escape_string(@$_POST["account_bank"]); 

      $table = "loan_application";
      if ($do == "add") {
     	$sql = "insert {$table} set
                                loan_id                ='{$loan_id}',
                                loan_application_type  ='{$loan_application_type}',
                                write_at               ='{$write_at}',
                                application_date       ='{$application_date}',
                                member_name            ='{$member_name}',
                                member_id              ='{$member_id}',
                                member_position        ='{$member_position}',
                                address                ='{$address}',
                                card_id                ='{$card_id}',
                                member_code            ='{$member_code}',
                                member_branch          ='{$member_branch}',
                                member_branch_name     ='{$member_branch_name}',
                                member_sector          ='{$member_sector}',
                                member_sector_name     ='{$member_sector_name}',
                                member_department      ='{$member_department}',
                                member_department_name ='{$member_department_name}',
                                member_tel             ='{$member_tel}',
                                address_card_id        ='{$address_card_id}',
                                member_salary          ='{$member_salary}',
                                loan_money             ='{$loan_money}',
                                loan_money_text        ='{$loan_money_text}',
                                member_use             ='{$member_use}',
                                member_share           ='{$member_share}',
                                member_money           ='{$member_money}',
								share_rate             ='{$share_rate}',
                                book_loan              ='{$book_loan_1}',
								book_two               ='{$book_loan_2}',
								loan_date              ='{$loan_date}',
								loan_for               ='{$loan_for}',
                                loan_remain            ='{$loan_remain}',
                                insurance_type         ='{$insurance_type}',
                                share_money            ='{$share_money}',
                                deposit_type           ='{$deposit_type}',
                                deposit_no             ='{$deposit_no}',
                                deposit_amount         ='{$deposit_amount}', 
								daposit_issue          ='{$daposit_issue}', 
								period                 ='{$period}', 
								interest_rate          ='{$interest_rate}', 
								interest_period        ='{$interest_period}', 
								account_bank           ='{$account_bank}'
					";
        $mysqli->query($sql);
        if ($mysqli) {
					$loan_application_id = $mysqli->insert_id;
					$input2 = $_FILES["document_pic"];
					for($i = 0 ; $i < (count($input2["name"])) ; $i ++ ){
					if(!empty($input2["tmp_name"][$i])){
						$ints2 = date("YmdGis") . @random_char(7);
						if($input2["type"][$i] == "image/png"||$input2["type"][$i] =="image/x-png"){
							$item_path2 = $ints2.".png";
							$rename = strtolower(random_char(30)) . '.png';
						}elseif($input2["type"][$i] == "image/gif"){
							$item_path2 = $ints2.".gif";
							$rename = strtolower(random_char(30)) . '.gif';
						}elseif($input2["type"][$i] == "image/pjpeg"||$input2["type"][$i] =="image/jpeg"){
							$item_path2 = $ints2.".jpg";
							$rename = strtolower(random_char(30)) . '.jpg';
						}elseif($input2["type"][$i] == "application/pdf") {
							$item_path2 = $ints2.".pdf";
							$rename = strtolower(random_char(30)) . '.pdf';
						}elseif($input2["type"][$i] == "application/msword" || $input2["type"][$i] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
							$item_path2 = $ints2.".docx";
							$rename = strtolower(random_char(30)) . '.docx';
						}
						if(!empty($item_path2)) {
							move_uploaded_file($input2["tmp_name"][$i], 'loan_doc/' . $rename);
							$item_type2 = "image" ;
							$item_title2 = $mysqli->real_escape_string($input2["name"][$i]) ;
							$sql_doc = "INSERT INTO loan_application_doc
									( loan_application_id , loan_id,doc_name,file)
								VALUES
									( '{$loan_application_id}','{$loan_id}','{$item_path2}','{$rename}')";
						$mysqli->query($sql_doc);
					}
				}
			}
		}
    } else {
	  	$loan_application_id = @$mysqli->real_escape_string(@$_POST["application"]);
     	$sql = "update {$table} set
                                loan_application_type  ='{$loan_application_type}',
                                write_at               ='{$write_at}',
                                application_date       ='{$application_date}',
                                member_name            ='{$member_name}',
                                member_id              ='{$member_id}',
                                member_position        ='{$member_position}',
                                address                ='{$address}',
                                card_id                ='{$card_id}',
                                member_code            ='{$member_code}',
                                member_branch          ='{$member_branch}',
                                member_branch_name     ='{$member_branch_name}',
                                member_sector          ='{$member_sector}',
                                member_sector_name     ='{$member_sector_name}',
                                member_department      ='{$member_department}',
                                member_department_name ='{$member_department_name}',
                                member_tel             ='{$member_tel}',
                                address_card_id        ='{$address_card_id}',
                                member_salary          ='{$member_salary}',
                                loan_money             ='{$loan_money}',
                                loan_money_text        ='{$loan_money_text}',
                                member_use             ='{$member_use}',
                                member_share           ='{$member_share}',
                                member_money           ='{$member_money}',
								share_rate             ='{$share_rate}',
                                book_loan              ='{$book_loan_1}',
								book_two               ='{$book_loan_2}',
								loan_date              ='{$loan_date}',
								loan_for               ='{$loan_for}',
                                loan_remain            ='{$loan_remain}',
                                insurance_type         ='{$insurance_type}',
                                share_money            ='{$share_money}',
                                deposit_type           ='{$deposit_type}',
                                deposit_no             ='{$deposit_no}',
                                deposit_amount         ='{$deposit_amount}', 
								daposit_issue          ='{$daposit_issue}', 
								period                 ='{$period}', 
								interest_rate          ='{$interest_rate}', 
								interest_period        ='{$interest_period}', 
								account_bank           ='{$account_bank}'
                                WHERE
                                loan_id='{$loan_id}'
                                AND
                                loan_application_id = '{$loan_application_id}' ";
        $mysqli->query($sql);
        if ($mysqli) {
           			if ($_FILES["document_pic"]) {
					$input2 = $_FILES["document_pic"];
					for($i = 0 ; $i < (count($input2["name"])) ; $i ++ ){
					if(!empty($input2["tmp_name"][$i])){
						$ints2 = date("YmdGis") . @random_char(7);
						if($input2["type"][$i] == "image/png"||$input2["type"][$i] =="image/x-png"){
							$item_path2 = $ints2.".png";
							$rename = strtolower(random_char(30)) . '.png';
						}elseif($input2["type"][$i] == "image/gif"){
							$item_path2 = $ints2.".gif";
							$rename = strtolower(random_char(30)) . '.gif';
						}elseif($input2["type"][$i] == "image/pjpeg"||$input2["type"][$i] =="image/jpeg"){
							$item_path2 = $ints2.".jpg";
							$rename = strtolower(random_char(30)) . '.jpg';
						}elseif($input2["type"][$i] == "application/pdf"){
							$item_path2 = $ints2.".pdf";
							$rename = strtolower(random_char(30)) . '.pdf';
						}elseif($input2["type"][$i] == "application/msword" || $input2["type"][$i] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
							$item_path2 = $ints2.".docx";
							$rename = strtolower(random_char(30)) . '.docx';
						}
						if(!empty($item_path2)) {
							move_uploaded_file($input2["tmp_name"][$i], 'loan_doc/' . $rename);
							$item_type2 = "image" ;
							$item_title2 = $mysqli->real_escape_string($input2["name"][$i]) ;
						    $sql_doc = "INSERT INTO loan_application_doc
									( loan_application_id , loan_id,doc_name,file)
								VALUES
									( '{$loan_application_id}','{$loan_id}','{$item_path2}','{$rename}')";
							$mysqli->query($sql_doc);
						}
					}
				}
           	}
		}
 	}

	if (isset($_POST['bondsman'])) {
				$del_bondsman = "DELETE FROM loan_application_bondsman WHERE loan_application_id = '$loan_application_id'";
        		$mysqli->query($del_bondsman);
        					$i=1;
							foreach ($_POST['bondsman'] as $key => $value) {
							  $sql2 = "insert loan_application_bondsman set
					                                loan_id              ='{$loan_id}',
					                                loan_application_id  ='{$loan_application_id}',
					                                bondsman_name        ='{$value['name']}',
					                                bondsman_number      ='{$value['number']}',
					                                bondsman_position    ='{$value['position']}',
					                                bondsman_affiliation ='{$value['affiliation']}',
													bondsman_salary      ='{$value['salary']}',
													bondsman_tel         ='{$value['tel']}',
					                                bondsman_num         ='{$i}' ";
        						 $mysqli->query($sql2);
					$i++; }
					}
	file_get_contents("http://gsbcoop.upbean.co.th/loan_common_pdf.php?loan=$loan_id&application=$loan_application_id&do=edit");
	header("Location: http://gsbcoop.upbean.co.th/common/$loan_id/$loan_application_id/true");
    }
	ob_start();
?>

<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />

<link href="http://gsbcoop.upbean.co.th/html/frontend/font/font-angsana/stylesheet.css" rel="stylesheet">

<style type="text/css">
.form-horizontal .control-label {
    padding-top: 1px !important;
    margin-bottom: 0;
}
@media only screen and (max-device-width: 480px) {
	.text_input_mobile {
        margin-top: 1px !important;
    }
}
html, body, div, textarea, applet, object, iframe,
		p, blockquote, pre,
		a, abbr, acronym, address, big, cite, code,
		del, dfn, em, img, ins, kbd, q, s, samp,
		small, strike, strong, sub, sup, tt, var,
		b, u, i, center,
		dl, dt, dd, ol, ul, li,
		fieldset, form, label, legend,
		table, caption, tbody, tfoot, thead, tr, th, td,
		article, aside, canvas, details, embed,
		figure, figcaption, footer, header, hgroup,
		menu, nav, output, ruby, section, summary,
		time, mark, audio, video , input , select {
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 100%;
			font: inherit;
			vertical-align: baseline;
			font-family: 'Angsana New' !important;
			font-style: normal !important;
			font-size: 20px !important;
		}
h1,h2,h3,h4,h5,h6{
    		font-family: "Angsana New"  !important;
		}

	.navbar{
		display: none;
	}
	.add-on-container{
		display: none;
	}
	.footer{
		display: none;
	}
	.border1 {
    border: solid 1px #ccc;
    padding: 0 15px;
}
.form-horizontal .form-group {
     margin-right: 0px !important;
     margin-left: 0px !important;
}
.set_text_left{
	text-align: left !important;
}
.m-l-1{
	margin-left: 1em;
}
.m-t-10{
	margin-top: 10px;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 100 !important;
    color: #333;
}
.checkbox-inline, .radio-inline {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 100;
    vertical-align: middle;
    cursor: pointer;
}
.glyphicon-folder-open:before {
    font-size: 12px;
    content: "\e118";
    margin-right: 5px;
}
.btn .badge {
    position: relative;
    top: -1px;
    margin-left: 5px;
}
.set_img_doc{
	height: 105px!important;
	width: 100%!important;
	object-fit: cover!important; /* Do not scale the image */
    object-position: center!important; /* Center the image within the element */
}

.btn-primary {
    color: #fff;
    background-color: #4fc3f7 !important;
    border-color: #4fc3f7 !important;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

</style>

<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scrip_include", $scrip_include);
	ob_start();
?>
<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	ob_start();
?>
<div class="row">
<div class="text-center" style="margin-top: 1em;">
	<img style="margin:auto;" width="100" height="100" src="/timthumb.php?a=c&zc=2&w=170&h=140&src=/images/logo.png" class="img-responsive">
</div>

<?php if ($_GET['do'] == "true") { ?>
<?php
    $sql = "SELECT application_status,loan_application_type FROM loan_application WHERE loan_id = '{$_GET['loan'] }' AND loan_application_id =  '{$_GET['application']}' ";
	$rs = $mysqli->query($sql);
    echo $mysqli->error;
    $row = $rs->fetch_assoc();
	$loan_application_type=$row['loan_application_type'];
?>
<?php 
if($row['loan_application_type']=='1'){?>
<h1 class="text-center"> คำขอกู้เงินสามัญ </h1>	
<?php }else if($row['loan_application_type']=='3'){ ?>
<h1 class="text-center"> คำขอกู้เงินฉุกเฉิน</h1>
<?php } ?>




<?php if ($row['application_status'] == 1) { ?>
	<?php 
		$sql = "SELECT alert_email, alert_tel FROM cmp_admin_alert";
		$rs  = $mysqli->query($sql);
		$row = $rs->fetch_assoc();

		$sql2 = "SELECT memname FROM cmp_imp_member WHERE member_no = '{$_COOKIE['member_no']}'";
		$rs2   = $mysqli->query($sql2);
		$memname = $rs2->fetch_assoc();
		while($row = $rs->fetch_assoc()) {
			$smarty->assign("server_name",  @$_SERVER["SERVER_NAME"]);
			$smarty->assign("memname", $memname["memname"]);
			$smarty->assign("member_no", $_COOKIE['member_no']);
			$smarty->assign("date", date_thai(date("Y-m-d"), true, true, true));
			$smarty->assign("time", date("H:i:s"));
			$html = @$smarty->fetch("alert.tpl");
			@send_mj_mail(" คุณ {$memname["memname"]} ยื่นคำร้องขอกู้เงินออนไลน์" , $html , $row["alert_email"]);
			$message = $memname['memname']." ได้ยื่นคำร้องขอกู้เงินออนไลน์";
			@sendSMS_alert( $row["alert_tel"] , $message);	
		}
	?>
	<h3 class="text-center">ระบบบันทึกข้อมูลของท่านเรียบร้อยแล้ว </h3>
	<h3 class="text-center">สหกรณ์จะทำการตรวจสอบหลักฐานคำขอกู้เงินของท่านต่อไป</h3>
	<h3 class="text-center">โดยท่านสามารถตรวจสอบสถานะการดำเนินงานได้ที่ ระบบสมาชิกออนไลน์ และในแอปพลิเคชันสหกรณ์</h3>
	<?php if($loan_application_type=='1'){ ?>
	<div style="margin-top: 2em;" class="text-center">
		<div style="margin-top: 2em;" class="text-center">
		<a target="_blank" href="/loan_petition_pdf.php?loan_id=<?php echo $_GET['loan']; ?>&loan_application_id=<?php echo $_GET['application'];?> ">
		<button style="font-size: 20px;background-color:#20449a;border-color: #20449a;color:#fff;" type="button" class="btn">Download แบบฟอร์มที่ท่านกรอกไว้</button>
		</a>
		</div>
	</div>
	<?php }else if($loan_application_type=='3'){ ?>
	<div style="margin-top: 2em;" class="text-center">
		<div style="margin-top: 2em;" class="text-center">
		<a target="_blank" href="/loan_emergent_pdf.php?loan_id=<?php echo $_GET['loan']; ?>&loan_application_id=<?php echo $_GET['application'];?> ">
		<button style="font-size: 20px;background-color:#20449a;border-color: #20449a;color:#fff;" type="button" class="btn">Download แบบฟอร์มที่ท่านกรอกไว้</button>
		</a>
		</div>
	</div>
	<?php } ?>

	<div style="margin-top: 2em;margin-bottom: 1em;" class="text-center">
		<div style="margin-top: 2em;" class="text-center">
		<a href="/index.php?do=status">
		<button style="font-size: 20px;background-color: #20449a;border-color: #20449a;color:#fff;" type="button" class="btn" >ปิดหน้าต่าง</button>
		</a>
	</div>
</div>

<?php } else { ?>

<div style="margin-top: 1.5em;" class="text-center">

	<div class="col-xs-12">
    <h3 class="text-center" style="font-size: 25px; font-family: 'thaisans_neueregular' !important; margin-bottom:50px;">กรุณากดยืนยันเพื่อทำการส่งข้อมูล</h3>
	<?php if($row['loan_application_type']=='1'){ ?>
		<a href="/loan_petition_pdf.php?loan_application_id=<?php echo $_GET['application']; ?>" target="_blank">ดูตัวอย่างคำร้องขอกู้เงินสามัญ</a>
		<br>
		<?php 
			$sql_special_beg = "SELECT id FROM loan_special_beg WHERE loan_id = '".$_GET['loan']."'";
			$rs_special_beg = $mysqli->query($sql_special_beg);
			$row_special_beg = $rs_special_beg->fetch_assoc();
			if($row_special_beg['id']!=''){
		?>
		<a href="/loan_special_beg_pdf.php?id=<?php echo $row_special_beg['id']; ?>" target="_blank">ดูตัวอย่างคำร้องขออนุเคราะห์เป็นกรณีพิเศษ</a>
		<?php }
	}else if($row['loan_application_type']=='3'){ ?>
		<a href="/loan_emergent_pdf.php?loan_id=<?php echo $_GET['loan']; ?>" target="_blank">ดูตัวอย่างคำร้องขอกู้เงินฉุกเฉิน</a>
		<br>
	<?php } ?>
	
	<br>
	<br>
	<a onclick="return confirm('ยืนยันคำขอกู้เงิน')" href="http://gsbcoop.upbean.co.th/common/<?php echo $_GET['loan'] ?>/<?php echo $_GET['application'] ?>/approve">
	<button style="font-size: 22px;background-color: #20449a;border-color: #20449a;color:#fff;margin: 5px;border-radius: 0px;font-family: 'thaisans_neueregular' !important;" type="button" class="btn">ยืนยันและส่งข้อมูล</button>
	</a>
	<a href="/index.php?id=1&loan_id=<?php echo $_GET['loan'] ?>">
	<button style="font-size: 22px;background-color: #969596;border-color: #969596;color:#fff;margin: 5px;border-radius: 0px;font-family: 'thaisans_neueregular' !important;" type="button" class="btn">แก้ไขข้อมูลคำร้อง</button>
	</a>
	</div>

</div>
<?php } ?>

<?php }else if ($_GET['do'] == "approve") { ?>

<?php
  $now = date("Y-m-d H:i:s");
  $sql = "update loan_application set
                                application_status='1',
                                approve_time='{$now}'
                                WHERE
                                loan_id='{$_GET['loan']}'
                                AND
                                loan_application_id = '{$_GET['application']}'
        ";
       $mysqli->query($sql);
       if ($mysqli) {
       	$send_doc = "update cmp_admin_loan_money set
                                b_send_doc_status='1',
                                b_send_doc_date='{$now}'
                                WHERE
                                loan_id='{$_GET['loan']}'
        ";//AND loan_application_type = '1'
        $mysqli->query($send_doc);
        $send_status = "insert cmp_admin_loan_money_status set
					                                loan_id='{$_GET['loan']}',
					                                status_name='ผู้ขอกู้ป้อนรายละเอียดและส่งคำขอกู้เรียบร้อยแล้ว',
					                                status_date='{$now}',
					                                loan_application_type ='1'
					        		";
		$mysqli->query($send_status);
		header("Location: http://gsbcoop.upbean.co.th/common/".$_GET['loan']."/".$_GET['application']."/true");
       }
 ?>

<?php }else{ ?>

<!-- เช็ค -->
<?php
if ($_GET['do'] == "edit") {
	$sql = "SELECT * FROM loan_application WHERE loan_id = '{$_GET['loan'] }' AND loan_application_id =  '{$_GET['application']}' ";
	$rs = $mysqli->query($sql);
    echo $mysqli->error;
    $row = $rs->fetch_assoc();
}else{


if ($_GET['m']) {

	 	$sql = "SELECT * FROM loan_application WHERE loan_id = '{$_GET['loan'] }' AND loan_application_type = '1' ";
		$rs = $mysqli->query($sql);
		echo $mysqli->error;
		$row = $rs->fetch_assoc();

		if ($row) {
			header("Location: http://gsbcoop.upbean.co.th/common/".$_GET['loan']."/".$row['loan_application_id']."/true");

		}else{

		$sql = "SELECT
	  	  cmp_imp_member.memname,
	  	  cmp_imp_member.position_desc,
	  	  cmp_imp_member.salary_amount,
	  	  cmp_imp_member.birthdtm,
	  	  cmp_imp_member.incomeetc_amount,
	  	  cmp_imp_member.membgroup_desc,
		  cmp_imp_member.card_person,
		  cmp_imp_member_data.telmobile
	  	  -- cmp_imp_member_data.addrc
	  	   FROM cmp_imp_member LEFT JOIN cmp_imp_member_data ON cmp_imp_member.member_no = cmp_imp_member_data.member_no WHERE cmp_imp_member.member_no = '{$_GET['m'] }' LIMIT 1 ";
	  $rs = $mysqli->query($sql);
      echo $mysqli->error;
      $row_first['incomeetc_amount'] = "0.00";
      $row_first = $rs->fetch_assoc();

		}

	}
}
 ?>
<!-- เช็ค -->
<div class="col-xs-12 col-sm-12">
<div class="col-xs-12 col-sm-12">
<h3 class="text-center">คำขอกู้เงินสามัญ</h3>
<h3 class="text-center" style="margin-bottom:1em;">กรุณากรอกข้อมูลลงในแบบฟอร์มด้านล่างให้ครบทุกช่อง</h3>
</div>
</div>
<form action="" class="form-horizontal" id="loan-form" method="post" enctype="multipart/form-data">
						<?php if ($row) { ?>
						<input type="hidden" name="do" value="edit">
						<input type="hidden" name="application" value="<?php echo $_GET['application'] ?>">
						<?php } else { ?>
						<input type="hidden" name="do" value="add">
						<?php } ?>
						<input type="hidden" name="loan_application_type" value="1">
						<input type="hidden" name="loan_id" id="loan_id" value="<?php echo $_GET['loan'] ?>">
						<div class="col-xs-12 col-sm-12">
						<div class="">
						<div class="col-xs-12  col-sm-7 add_has form-group" style="margin-bottom: 0;">
						</div>
							<div class="col-xs-12  col-sm-5 form-group">
								<div class="row">
								<div class="col-sm-4"></div>
								<label for="loan_add_by" class="col-sm-4 control-label">เขียนที่</label>
								<div class="col-sm-4" style="margin-bottom: 10px;">
									<input type="text" id="write_at" class="form-control" name="write_at"
										value="<?=$row['write_at']?>" autofocus required="required">
								</div>
								<div class="col-sm-4"></div>
									<label for="loan_date" class="col-xs-12 col-sm-4 control-label">วันที่</label>
								<div class="col-xs-6 col-sm-4">
							<?php
							if ($row['application_date']) {
								$time = strtotime($row['application_date']);
								$myFormatForView = date("d/m/Y", $time);
							}else{
  								$time = date("Y-m-d");
								$date = str_replace('/', '/', $time);
							    $myFormatForView = date('d/m/Y', strtotime($date));
							}
							?>
								<input  type="text" id="datepicker" class="form-control" name="application_date"
									value="<?=$myFormatForView ?>"  required="required">
							</div>
							</div>
							</div>
							</div>
							</div>


						<div class="col-xs-12 col-sm-12">
						<div class="">
							<div class="col-xs-12 col-sm-12 form-group">
								<label class="control-label" style="margin-bottom: 0;">เรียน คณะกรรมการดำเนินการ<?php echo COOP_NAME_TH;?></label>
							</div>
						</div>
						</div>


						<div class="col-xs-12 col-sm-12">
							<div class="row">
							<div class="col-xs-12 col-sm-4 form-group">
							<label for="member_name" class="col-sm-2 control-label">ข้าพเจ้า</label>
							<div class="col-sm-10">
							<input type="text" id="member_name" class="form-control" name="member_name"
									value="<?= ($_GET['do'] == "edit") ? $row['member_name'] : $row_first['memname'] ;?>" required="required" >
							</div>
							</div>
							<div class="col-xs-12 col-sm-4 form-group">
							<label for="member_id" class="col-xs-12 col-sm-4 control-label">สมาชิกเลขที่</label>
							<div class="col-xs-12 col-sm-8">
								<input type="text" id="member_id" class="form-control" name="member_id"
									value="<?= ($_GET['do'] == "edit") ? $row['member_id'] : $_GET['m'] ;?>" required="required" >
							</div>
							</div>
							<div class="col-xs-12 col-sm-4 form-group">
							<label  for="member_rank" class="col-sm-2 control-label">ตำแหน่ง</label>
							<div class="col-sm-10">
								<input type="text" id="member_position" class="form-control" name="member_position"
									value="<?= ($_GET['do'] == "edit") ? $row['member_position'] : $row_first['position_desc'] ;?>" required="required" >
							</div>
							</div>
						</div>
						</div>

						<div class="col-xs-12 col-sm-12">
						<div class="row">
						<div class="col-xs-12 col-sm-12 form-group">
							<label for="member_name" class="col-sm-1 control-label set_text_left">ที่อยู่</label>
							<div class="col-sm-11">
							<?php
							 $sql_addrc = "SELECT * FROM cmp_imp_address  WHERE member_no = '{$_GET['m']}' LIMIT 1 ";
								  $rs_addrc = $mysqli->query($sql_addrc);
							      echo $mysqli->error;
							      $row_addrc = $rs_addrc->fetch_assoc();
							   	  $data_addrc  = $row_addrc["memb_addr"];
								  $data_addrc .= !empty($row_addrc["addr_group"]) ? " หมู่ {$row_addrc["addr_group"]} " : "" ;
								  $data_addrc .= !empty($row_addrc["soi"]) ? " ซ. {$row_addrc["soi"]} " : "" ;
								  $data_addrc .= !empty($row_addrc["mooban"]) ? " หมู่บ้าน {$row_addrc["mooban"]} " : "" ;
								  $data_addrc .= !empty($row_addrc["road"]) ? " ถ. {$row_addrc["road"]} " : "" ;
								  $data_addrc .= !empty($row_addrc["tambol"]) ? " ต. {$row_addrc["tambol"]} " : "" ;
								  $data_addrc .= !empty($row_addrc["district_desc"]) ? " อ. {$row_addrc["district_desc"]} " : "" ;
								  $data_addrc .= !empty($row_addrc["province_desc"]) ? " จ. {$row_addrc["province_desc"]} " : "" ;
								  $data_addrc .= !empty($row_addrc["postcode"]) ? "{$row_addrc["postcode"]} " : "" ;
							 ?>
							<textarea type="text" id="address" class="form-control" name="address"
								 required="required"rows="2"><?= ($_GET['do'] == "edit") ? $row['address'] : $data_addrc ;?></textarea>
							</div>
							</div>

						</div>
						</div>
						
						<div class="col-xs-12 col-sm-12">
						<div class="row">
							<div class="col-xs-12 col-sm-5 form-group">
								<label for="card_id" class="col-xs-12 col-sm-4  control-label set_text_left">เลขที่บัตรประชาชน</label>
								<div class="col-xs-6 col-sm-8 ">
									<input type="text" id="card_id" class="form-control" name="card_id"  value="<?= ($_GET['do'] == "edit") ? $row['card_id'] : $row_first['card_person'] ;?>">
								</div>
							</div>

							<!-- <div class="col-xs-12 col-sm-3 form-group ">
								<label for="member_code" class="col-xs-12 col-sm-2 control-label">รหัส</label>
								<div class="col-xs-6 col-sm-10">
									<input type="text" id="" class="form-control" name="member_code" value="<?=$row['member_code']?>">
								</div>
							</div> -->
							
								<div class="col-xs-12 col-sm-offset-1 col-sm-7 form-group">
								<div class="col-sm-1 control-label set_text_left">สังกัด</div>
								<label class="col-xs-10 col-sm-offset-0 col-sm-2">
							  		<input class="checkbox_status marry_mobile" <?= ($row['member_branch'] == 1) ? "checked" : "" ;?> type="checkbox" name="member_branch"  value="1"> กลุ่มงาน
								</label>
								<label class="  col-xs-10 col-sm-offset-0 col-sm-2">
							  		<input class="checkbox_status marry_mobile" <?= ($row['member_branch'] == 2) ? "checked" : "" ;?> type="checkbox" name="member_branch"  value="2"> สาขา
								</label>
								<label class=" col-xs-10 col-sm-offset-0 col-sm-7">
							  		<input type="text" id="marry_name_mobile" class="form-control" name="member_branch_name" value="<?=$row['member_branch_name']?>" <?= ($row['member_branch_name'] == "") ? "readonly='readonly'" : "" ;?>>
								</label>
							</div>
						</div>
					</div>

						<div class="col-xs-12 col-sm-12">
						<div class="row">
						<div class="col-xs-12 col-sm-offset-0 col-sm-5 form-group">
							<label class="  col-xs-10 col-sm-offset-0 col-sm-2">
							  <input class="checkbox_status sector_mobile" <?= ($row['member_sector'] == 1) ? "checked" : "" ;?>  type="checkbox" name="member_sector"  value="1"> ส่วน  
							</label>
							<label class="  col-xs-10 col-sm-offset-0 col-sm-2">
							  <input class="checkbox_status sector_mobile" <?= ($row['member_sector'] == 2) ? "checked" : "" ;?> type="checkbox" name="member_sector" value="2"> เขต
							</label>
							<label class=" col-xs-10 col-sm-offset-0 col-sm-8">
							  <input type="text" id="sector_mobile" class="form-control" name="member_sector_name" value="<?=$row['member_sector_name']?>" <?= ($row['member_sector_name'] == "") ? "readonly='readonly'" : "" ;?>>
							</label>
						</div>
						<div class="col-xs-12 col-sm-offset-0 col-sm-7 form-group">
							<label class="  col-xs-10 col-sm-offset-0 col-sm-2">
							  <input class="checkbox_status department_mobile" <?= ($row['member_department'] == 1) ? "checked" : "" ;?> type="checkbox" name="member_department"  value="1"> ฝ่าย 
							</label>
							<label class="  col-xs-10 col-sm-offset-0 col-sm-2">
							  <input class="checkbox_status department_mobile" <?= ($row['member_department'] == 2) ? "checked" : "" ;?>  type="checkbox" name="member_department"  value="2"> ภาค 
							</label>
							<label class=" col-xs-10 col-sm-offset-0 col-sm-4">
							  <input type="text" id="department_mobile" class="form-control" name="member_department_name" value="<?=$row['member_department_name']?>" <?= ($row['member_department_name'] == "") ? "readonly='readonly'" : "" ;?>>
							</label>
							<label for="loan_tel" class="col-xs-10 col-sm-offset-0 col-sm-1">โทร</label>
							<label class=" col-xs-10 col-sm-offset-0 col-sm-3">
							   <input type="tel" class="form-control" name="member_tel" value="<?= ($_GET['do'] == "edit") ? $row['member_tel'] : $row_first['telmobile'] ;?>" maxlength="10">
							</label>
						</div>


						<div class="col-xs-12 col-sm-12">
						<div class="row">
						<div class="col-xs-12 col-sm-12">
						<div class="row">
							<div class="col-xs-12 col-sm-12 form-group">
							<label for="address" class="col-sm-2 control-label set_text_left"> ที่อยู่ตามบัตรประชาชน </label>
							<div class="col-sm-10">
							<?php
							 $sql_addrc = "SELECT * FROM cmp_imp_address  WHERE member_no = '{$_GET['m'] }' LIMIT 1 ";
								$rs_addrc = $mysqli->query($sql_addrc);
							    echo $mysqli->error;
							    $row_addrc = $rs_addrc->fetch_assoc();
							    $data_addrc  = $row_addrc["memb_addrc"];
								$data_addrc .= !empty($row_addrc["addr_groupc"]) ? " หมู่ {$row_addrc["addr_groupc"]} " : "" ;
								$data_addrc .= !empty($row_addrc["soic"]) ? " ซ. {$row_addrc["soic"]} " : "" ;
								$data_addrc .= !empty($row_addrc["moobanc"]) ? " หมู่บ้าน {$row_addrc["moobanc"]} " : "" ;
								$data_addrc .= !empty($row_addrc["roadc"]) ? " ถ. {$row_addrc["roadc"]} " : "" ;
								$data_addrc .= !empty($row_addrc["tambolc"]) ? " ต. {$row_addrc["tambolc"]} " : "" ;
								$data_addrc .= !empty($row_addrc["district_descc"]) ? " อ. {$row_addrc["district_descc"]} " : "" ;
								$data_addrc .= !empty($row_addrc["province_descc"]) ? " จ. {$row_addrc["province_descc"]} " : "" ;
								$data_addrc .= !empty($row_addrc["postcodec"]) ? "{$row_addrc["postcodec"]} " : "" ;
							 ?>
							<textarea type="text" id="address" class="form-control" name="address_card_id"
								 required="required"rows="2"><?= ($_GET['do'] == "edit") ? $row['address_card_id'] : $data_addrc ;?></textarea>
								<!-- <input  > -->
							</div>
							</div>
						</div>
						</div>

						<div class="col-xs-12 col-sm-12">
						<div class="row">
						<div class="col-xs-12 col-sm-6 form-group">
							<label for="member_salary" class="col-xs-12 col-sm-3 control-label set_text_left">มีเงินได้รายเดือน </label>
							<div class="col-xs-6 col-sm-8 ">
								<input type="number" id="member_salary" class="form-control" name="member_salary" required="required"  value="<?= ($_GET['do'] == "edit") ? $row['member_salary'] : $row_first['salary_amount'] ;?>">
							</div>
							<label  for="member_salary" class="col-sm-1 col-xs-2 control-label text_input_mobile">บาท</label>
						</div>
						<div class="col-xs-12 col-sm-6 form-group ">
							<label for="member_com" class="col-xs-12 col-sm-8 control-label set_text_left"> ขอเสนอคำขอกู้สามัญ เพื่อโปรดพิจารณาดังต่อไปนี้ </label>
						</div>
						</div>
						</div>

						<div class="col-xs-12 col-sm-12">
						<div class="row">
						<div class="col-xs-12 col-sm-6 form-group">
							<label for="member_salary" class="col-xs-12 col-sm-7 control-label set_text_left"> ข้อ 1. ข้าพเจ้าขอกู้เงินสามัญของสหกรณ์ จำนวน </label>
							<div class="col-xs-10 col-sm-4">
								<input type="number" id="send_money" class="form-control loan_money_req number_money" name="loan_money" value="<?=$row['loan_money']?>" required="required" >
							</div>
							<label  for="member_salary" class="col-sm-1 col-xs-2 control-label text_input_mobile">บาท</label>
						</div>
							
						<div class="col-xs-10 col-sm-5 form-group">
							<div class="col-xs-12 col-sm-12 ">
							<input id="send_money_text" type="text"  class="form-control loan_money_req"  name="loan_money_text"
									value="<?= ($row['loan_money'] != "") ? convert($row['loan_money']) : "" ;?>" readonly="readonly">
							</div>		
						</div>

						</div>
						</div>

						<div class="col-xs-12 col-sm-12">
						<div class="row">
						<div class="col-xs-12 col-sm-12 form-group">
							<label for="member_name" class="col-sm-12 control-label set_text_left">โดยจะนำไปใช้เพื่อการดังต่อไปนี้ ( ชี้แจงความมุ่งหมายและเหตุผลของการกู้โดยละเอียด )</label>
							<div class="col-sm-12">
							<textarea type="text" id="member_use" class="form-control" name="member_use"
								 required="required"rows="2"><?= ($_GET['do'] == "edit") ? $row['member_use'] : "" ;?></textarea>
							</div>
							</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12">
						<div class="row">

						<div class="col-xs-12 col-sm-6 form-group">
							<label for="member_salary" class="col-xs-12 col-sm-7 control-label set_text_left">ข้อ 2. ในเวลานี้ ข้าพเจ้ามีหุ้นอยู่ในสหกรณ์ รวม </label>
							<div class="col-xs-10 col-sm-4">
								<input type="number" id="send_money" class="form-control loan_money_req number_money" name="member_share"
									value="<?=$row['member_share']?>" required="required" >
							</div>
							<label  for="member_salary" class="col-sm-1 col-xs-2 control-label text_input_mobile">หุ้น</label>
						</div>
							
						<div class="col-xs-12 col-sm-6 form-group ">
							<label for="member_com" class="col-xs-12 col-sm-2 control-label set_text_left">เป็นเงิน</label>
							<div class="col-xs-10 col-sm-8">
								<input type="number" id="member_salary" class="form-control" name="member_money" required="required"  value="<?= ($_GET['do'] == "edit") ? $row['member_money'] : "" ;?>">
							</div>
							<label  for="member_salary" class="col-sm-2 col-xs-2 control-label text_input_mobile">บาท</label>
						</div>

						<div class="col-xs-12 col-sm-6 form-group">
							<label for="member_salary" class="col-xs-12 col-sm-7 control-label set_text_left">และข้าพเจ้าส่งค่าหุ้นรายเดือนอยู่ในอัตรา เดือนละ</label>
							<div class="col-xs-10 col-sm-4">
								<input type="number" id="send_money" class="form-control loan_money_req number_money" name="share_rate"
									value="<?=$row['share_rate']?>" required="required" >
							</div>
							<label  for="member_salary" class="col-sm-1 col-xs-2 control-label text_input_mobile">บาท</label>
						</div>

						</div>
						</div>

						<div class="col-xs-12 col-sm-12">
						<div class="row">
							<div class="col-xs-12 col-sm-12 form-group">
								<label for="member_salary" class="col-xs-12 col-sm-4 control-label set_text_left">ข้อ 3. ข้าพเจ้ามีหนี้สินอยู่ต่อสหกรณ์ในฐานะผู้กู้ ดังต่อไปนี้</label>
							</div>
							<div class="col-xs-12 col-sm-12 form-group">
								<label for="member_com" class="col-xs-12 col-sm-2 control-label">3.1. หนังสือกู้ที่ </label>
								<div class="col-xs-10 col-sm-3">
									<input type="text" id="member_salary" class="form-control" name="book_loan_1" required="required"  value="<?=$row['book_loan'];?>"> 
								</div>
								<label for="member_com" class="col-xs-12 col-sm-1 control-label" style="text-align:center;width:30px;"> <b> / </b></label>
								<div class="col-xs-10 col-sm-3">
									<input type="text" id="member_salary" class="form-control" name="book_loan_2" required="required"  value="<?=$row['book_two'];?>"> 
								</div>
								<label for="loan_date" class="col-xs-12 col-sm-1 control-label">วันที่</label>
								<div class="col-xs-10 col-sm-2">
										<?php
										if ($row['loan_date']) {
											$time = strtotime($row['loan_date']);
											$myFormatForView = date("d/m/Y", $time);
										}else{
											$time = date("Y-m-d");
											$date = str_replace('/', '/', $time);
											$myFormatForView = date('d/m/Y', strtotime($date));
										}
										?>
											<input  type="text" id="datepicker_two" class="form-control" name="loan_date"
												value="<?=$myFormatForView ?>"  required="required">
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-2 control-label"> เพื่อ </label>
									<div class="col-xs-10 col-sm-4">
										<input type="text" id="member_salary" class="form-control" name="loan_for" required="required"  value="<?=$row['loan_for']?>"> 
									</div>
									<label for="member_com" class="col-xs-12 col-sm-2 control-label" > ต้นเงินคงเหลือ </label>
									<div class="col-xs-10 col-sm-3">
										<input type="number" id="member_salary" class="form-control" name="loan_remain" required="required"  value="<?=$row['loan_remain']?>"> 
									</div>
									<label  for="member_salary" class="col-sm-1 col-xs-2 control-label set_text_left">บาท</label>
								</div>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_salary" class="col-xs-12 col-sm-6 control-label set_text_left">ข้อ 4. นอกจากค่าหุ้น ซึ่งข้าพเจ้ามีอยู่ในสหกรณ์ ข้าพเจ้าขอเสนอหลักประกัน ดังต่อไปนี้ คือ &nbsp;&nbsp;&nbsp;&nbsp;	</label>
								</div>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-3 control-label">
			<!--checkbox_status -->		<input class="" id="insurance_1" <?= ($row['insurance_type'] == 1) ? "checked" : "" ;?> required="required"  type="checkbox" name="insurance_type"  value="1" > 
										  <span style="margin-left:20px;"> 4.1. สมาชิกสหกรณ์ </span>
									</label>
								</div>
								<?php for($i = 1; $i <= 3; $i++) { 
								if ($_GET['do'] == "edit") {
									$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_id = '{$_GET['loan'] }' AND loan_application_id =  '{$_GET['application']}' AND bondsman_num = '{$i}'";
									$rs_bondsman = $mysqli->query($sql_bondsman);
									$row_bondsman = $rs_bondsman->fetch_assoc();
								}
								?>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-2 control-label">ลำดับที่ <?php echo $i ?> </label>
								</div>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-2 control-label">ชื่อ</label>
									<div class="col-xs-10 col-sm-3">
										<input type="text" id="insurance_name" class="form-control set_text_left" name="bondsman[<?=$i?>][name]"  value="<?=$row_bondsman['bondsman_name']?>" <?= ($row_bondsman['bondsman_name'] == "") ? "readonly='readonly'" : "" ;?>> 
									</div>
									<label for="member_com" class="col-xs-12 col-sm-1 control-label" style="width:120px;">สมาชิกเลขที่</label>
									<div class="col-xs-10 col-sm-2">
										<input type="number" id="insurance_number" class="form-control set_text_left" name="bondsman[<?=$i?>][number]"  value="<?=$row_bondsman['bondsman_number']?>" <?= ($row_bondsman['bondsman_number'] == "") ? "readonly='readonly'" : "" ;?>> 
									</div>
									<label for="member_com" class="col-xs-12 col-sm-1 control-label" style="width:150px;">พนักงานตำแหน่ง</label>
									<div class="col-xs-10 col-sm-2">
										<input type="text" id="insurance_position" class="form-control set_text_left" name="bondsman[<?=$i?>][position]"   value="<?=$row_bondsman['bondsman_position']?>" style="width:280;" <?= ($row_bondsman['bondsman_position'] == "") ? "readonly='readonly'" : "" ;?>> 
									</div>
								</div>	
								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-2 control-label">สังกัด </label>
									<div class="col-xs-10 col-sm-3">
										<input type="text" id="insurance_affiliation" class="form-control set_text_left" name="bondsman[<?=$i?>][affiliation]"  value="<?=$row_bondsman['bondsman_affiliation']?>" <?= ($row_bondsman['bondsman_affiliation'] == "") ? "readonly='readonly'" : "" ;?>> 
									</div>
									<label for="member_com" class="col-xs-12 col-sm-1 control-label" style="width:120px;"> เงินได้รายเดือน </label>
									<div class="col-xs-10 col-sm-2">
										<input type="number" id="insurance_salary" class="form-control set_text_left" name="bondsman[<?=$i?>][salary]"  value="<?=$row_bondsman['bondsman_salary']?>" <?= ($row_bondsman['bondsman_salary'] == "") ? "readonly='readonly'" : "" ;?>> 
									</div>
									<label for="member_com" class="col-xs-12 col-sm-1 control-label" style="width:150px;"> เบอร์โทรผู้ค้ำประกัน  </label>
									<div class="col-xs-10 col-sm-2">
										<input type="tel" id="insurance_salary" class="form-control set_text_left" name="bondsman[<?=$i?>][tel]"  value="<?=$row_bondsman['bondsman_tel']?>" <?= ($row_bondsman['bondsman_tel'] == "") ? "readonly='readonly'" : "" ;?> maxlength="10"> 
									</div>

									<!-- <label  for="member_salary" class="col-sm-4 col-xs-10 control-label set_text_left hidden-xs">บาท <span style="margin-left:15px;">เป็นผู้ค้ำประกันรายอื่นตามหนังสือ ค้ำประกันที่และชื่อผู้กู้ </span> </label>
									<label  for="member_salary" class="col-sm-4 col-xs-10 control-label set_text_left hidden-sm hidden-md hidden-lg">บาท <br> เป็นผู้ค้ำประกันรายอื่นตามหนังสือ ค้ำประกันที่และชื่อผู้กู้ </label> -->
								</div>
								<div class="col-xs-12 col-sm-12 form-group">
									<label  for="member_salary" class="col-sm-offset-1 col-sm-4 col-xs-10 control-label set_text_left hidden-xs"> <span style="margin-left:15px;">เป็นผู้ค้ำประกันรายอื่นตามหนังสือ ค้ำประกันที่และชื่อผู้กู้ </span> </label>
									<label  for="member_salary" class="col-sm-offset-1 col-sm-4 col-xs-10 control-label set_text_left hidden-sm hidden-md hidden-lg"> <br> เป็นผู้ค้ำประกันรายอื่นตามหนังสือ ค้ำประกันที่และชื่อผู้กู้ </label>
								</div>
								<?php } ?>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-4 control-label">
										  <input class="" id="insurance_2" <?= ($row['insurance_type'] == 2) ? "checked" : "" ;?>  type="checkbox" name="insurance_type" value="2"> 
										  <span style="margin-left:20px;"> 4.2. ทุนเรือนหุ้นที่มีอยู่ในสหกรณ์ &nbsp;&nbsp;&nbsp;&nbsp;</span>
									</label>
									<label for="member_com" class="col-xs-12 col-sm-1 control-label set_text_left">จำนวนเงิน </label>
									<div class="col-xs-10 col-sm-2">
										<input type="number" id="share_money" class="form-control" name="share_money"  value="<?= ($_GET['do'] == "edit") ? $row['share_money'] : "";?>" <?= ($row['share_money'] == "") ? "readonly='readonly'" : "" ;?>>
									</div>
									<label  for="member_salary" class="col-sm-2 col-xs-1 control-label set_text_left"> บาท</label>
								</div>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-4 control-label">
										  <input class="" id="insurance_3" <?= ($row['insurance_type'] == 3) ? "checked" : "" ;?>  type="checkbox" name="insurance_type" value="3"> 
										  <span style="margin-left:20px;"> 4.3. สมุดเงินฝากของสหกรณ์ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
									</label>
									<label for="member_com" class="col-xs-12 col-sm-1 control-label set_text_left">ประเภท  </label>
									<div class="col-xs-10 col-sm-3">
										<input type="text" id="deposit_type" class="form-control" name="deposit_type" value="<?=$row['deposit_type']?>" <?= ($row['deposit_type'] == "") ? "readonly='readonly'" : "" ;?>>
									</div>
									<label for="member_com" class="col-xs-12 col-sm-1 control-label">เลขที่</label>
									<div class="col-xs-10 col-sm-3">
										<input type="number" id="deposit_no" class="form-control set_text_left" name="deposit_no" value="<?=$row['deposit_no']?>" <?= ($row['deposit_no'] == "") ? "readonly='readonly'" : "" ;?>> 
									</div>
								</div>	
								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-offset-1 col-sm-1 control-label"> จำนวนเงิน </label>
									<div class="col-xs-10 col-sm-3">
										<input type="number" id="deposit_amount" class="form-control" name="deposit_amount" value="<?= ($_GET['do'] == "edit") ? $row['deposit_amount'] : "" ;?>" <?= ($row['deposit_amount'] == "") ? "readonly='readonly'" : "" ;?>>
									</div>
									<label  for="member_money" class="col-sm-1 col-xs-1 control-label set_text_left"> บาท</label>
								</div>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-offset-1 col-sm-8  control-label">
											ข้าพเจ้าขอเสนอทุนเรือนหุ้น หรือสมุดเงินฝากสหกรณ์ ที่จะจำนำเป็นประกันซึ่งข้าพเจ้าได้รับรองว่าเป็นสำเนาอันถูกต้อง มาพร้อมคำกู้นี้ 
									</label>
									<label for="member_com" class="col-xs-2 col-sm-1 control-label">รวม </label>
									<div class="col-xs-7 col-sm-1">
										<input type="number" id="member_salary" class="form-control" name="daposit_issue" value="<?=$row['daposit_issue']?>">
									</div>
									<label  for="member_salary" class="col-sm-1 col-xs-1 control-label set_text_left">ฉบับ</label>
								</div>
								
								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_salary" class="col-xs-12 col-sm-4 control-label set_text_left"> ข้อ 5. ถ้าข้าพเจ้าได้รับเงินกู้ข้าพเจ้าขอส่งต้นเงินกู้เป็นงวดรายเดือน </label>
									<label for="member_com" class="col-xs-3 col-sm-1 control-label">งวดละ </label>
									<div class="col-xs-6 col-sm-2">
										<input type="number" id="member_salary" class="form-control" name="period"  value="<?=$row['period']?>">
									</div>
									<label  for="member_salary" class="col-sm-1 col-xs-1 control-label set_text_left">บาท </label>

									<label for="member_salary" class="col-xs-12 col-sm-2 control-label">(พร้อมดอกเบี้ยอัตราร้อยละ</label>
									<div class="col-xs-6 col-sm-1">
										<input type="number" id="member_salary" class="form-control" name="interest_rate" value="<?=$row['interest_rate']?>">
									</div>
									<label for="member_salary" class="col-xs-2 col-sm-1 control-label set_text_left">ต่อปี</label>
								</div>
								
								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-4 control-label set_text_left">
										หรือตามประกาศอัตราดอกเบี้ยเงินกู้ของสหกรณ์)
									</label>
									<label for="member_com" class="col-xs-4 col-sm-1 control-label">เป็นจำนวน </label>
									<div class="col-xs-5 col-sm-1">
										<input type="number" id="member_salary" class="form-control" name="interest_period"  value="<?=$row['interest_period']?>">
									</div>
									<label for="member_salary" class="col-xs-1 col-sm-1 control-label set_text_left">งวด</label>
									<label for="member_salary" class="col-xs-12 col-sm-4 control-label set_text_left"> ตั้งแต่เดือนถัดจากเดือนที่ สหกรณ์ จ่ายเงินกู้ให้</label>
								</div>

								<div class="col-xs-12 col-sm-12 form-group">
									<label for="member_com" class="col-xs-12 col-sm-9 control-label set_text_left">
										ข้อ 6.ในการรับเงินกู้ ข้าพเจ้าจะได้ทำหนังสือกู้สำหรับเงินกู้สามัญให้ไว้ต่อสหกรณ์ ตามแบบที่สหกรณ์กำหนดโดยโอนเงินเข้าบัญชีเงินเดือน 
									</label>
									<label for="member_com" class="col-xs-3 col-sm-1 control-label set_text_left">เลขที่ </label>
									<div class="col-xs-8 col-sm-2">
										<input type="number" id="member_salary" class="form-control" name="account_bank" required="required"  value="<?=$row['account_bank']?>">
									</div>
								</div>

						<!-- <div class="col-xs-12 col-sm-12 form-group">
						<label for="member_com" class="col-xs-8 col-sm-4 form-group" style="margin-top: 6.5px">
							แนบสำเนาเอกสารประกอบการจ่ายเงินเดือน ( เดือนสุดท้าย ) 
						</label>
						<input class="col-xs-6 col-sm-10 " name="document_pic[]" type="file" id="input03" multiple="multiple" <?= ($_GET['do'] == "edit") ? "" : "required" ;?>>
						</div>

						<?php if ($_GET['do'] == "edit") { ?>
						<div class="col-xs-12 col-sm-12 form-group">
						<?php
						if ($_GET['do'] == "edit") {
						$sql_img = "SELECT * FROM loan_application_doc WHERE loan_id = '{$_GET['loan'] }' AND loan_application_id =  '{$_GET['application']}' ";
						$rs_img = $mysqli->query($sql_img);
					    echo $mysqli->error;
					    $i = 1;
						while(($img = $rs_img->fetch_assoc())) { ?>
						<div class="col-sm-2 col-xs-6 num_del<?=$i?>" >
						<span num="<?=$i?>" img_name="<?=$img['file']?>" img_id="<?=$img['doc_id']?>" style="float: right;color: red;cursor: pointer;" class="glyphicon glyphicon-remove del_img" aria-hidden="true"></span>
							<a target="_blank" href="http://gsbcoop.upbean.co.th/loan_doc/<?=$img['file']?>">
								<img  src="http://gsbcoop.upbean.co.th/loan_doc/<?=$img['file']?>" alt="" class="img-responsive set_img_doc">
							</a>
						</div>
						 <?php $i++; } ?>
						 <?php  } ?>
						</div>
						 <?php  } ?>

						</div> -->
						<input type="hidden" name="admin" value="<?=$_GET['admin']?>">	
						<div class="form-group text-center" >
							<div class="col-xs-12 col-sm-12 control-label">
								<p class="text-center">
									<button style="margin-top:0.5em;font-size:20px;background-color: #20449a;border-color: #20449a;color:#fff;" type="submit" class="btn btn-material-light-blue-300 btn-lg"> บันทึกและส่งแบบฟอร์ม </button>
								</p>
								<p class="blgres text-center"></p>
							</div>
						</div>

					</form>

<?php } ?>
</div>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="http://loan.cmcoop.or.th/scripts/jqueryui/jquery.ui.datepicker-th.js"></script>
    <script src="http://loan.cmcoop.or.th/scripts/jqueryui/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
    <script src="http://loan.cmcoop.or.th/scripts/jquery.validate_loan.js"></script>
	<script src="http://loan.cmcoop.or.th/scripts/bootstrap-filestyle/bootstrap-filestyle.js"></script>
	<script src="http://loan.cmcoop.or.th/scripts/jquery.number_input.js"></script>

 <script>


 // เช็คเบอร์มือภือซ้ำ

$(function(){

$('.check_tel').change(function() {
    var $current = $(this);
    var tel_user = $("#tel").val();

    if ($(this).val() == tel_user) {
    	alert("เบอร์มือถือซ้ำกับผู้กู้สัญญา กรุณาเปลี่ยนใหม่ค่ะ");
    	$(this).val("");
    	$(this).focus();
    	return false;
    }

    $('.check_tel').each(function() {
        if ($(this).val() == $current.val() && $(this).attr('id') != $current.attr('id'))
        {
    		alert("เบอร์มือถือซ้ำกับผู้ค้ำสัญญาท่านอื่น กรุณาเปลี่ยนใหม่ค่ะ");
    		$current.val("");
    		$current.focus();
    		return false;
        }

    });
  });


// $('.number_money').number( true, 2 );

});

// เช็คเบอร์มือภือซ้ำ

confirmExit = "";
window.onbeforeunload = confirmExit;
function windowClose(){
    alert("confirm exit is being called");
    return false;
}

    $(document).ready(function(){

    	$('#input03').filestyle({
				input : false,
				buttonName : 'btn-material-light-blue-300'
		});

  	var check_edit = "<?=$_GET['do']?>";
  	if (check_edit == "edit") {
  		var required_pic = false;
  	}else{
  		var required_pic = true;
  	}

  		$( "#loan-form" ).validate( {
				rules: {
					transfer_money_id: "required",
					member_tel: {
						required: true,
						tel: true
					},
					"document_pic[]": {
						required: required_pic
					},
					use_money_detail:{
					maxlength: 125
					},
					pay_debt_id: "required",
					use_money: "required",
					work_at: "required",
					province_id: "required",
					amphur_id: "required",
					district_id: "required",
					application_type_req: "required"
				},
				messages: {
					transfer_money_id: "กรุณาเลือกวิธีการโอนเงิน",
					member_tel: {
						required: "กรุณากรอกเบอร์โทร",
						tel: "รูปแบบเบอร์มือถือไม่ถูกต้อง"
					},
					use_money_detail: {
					maxlength: "กรุณากรอกไม่เกิน 200 ตัวอักษร"
					},
					"document_pic[]": "กรุณาแนบหลักฐานการกู้"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				// highlight: function ( element, errorClass, validClass ) {
				// 	var cars = ["1", "2", "3", "4", "5", "6","7","8","9","10","11","12"];
				// 	for (i = 0; i < cars.length; i++) {
				// 		    $( element ).parents( ".col-sm-"+cars[i]+"" ).addClass( "has-error" ).removeClass( "has-success" );
				// 		}

				// },
				// unhighlight: function (element, errorClass, validClass) {
				// 	var cars = ["1", "2", "3", "4", "5", "6","7","8","9","10","11","12"];
				// 	for (i = 0; i < cars.length; i++) {
				// 		$( element ).parents(  ".col-sm-"+cars[i]+"" ).addClass( "has-success" ).removeClass( "has-error" );
				// 		}
				// }
			} );

      });

$(".checkbox_status").on('click', function() {
	  var $box = $(this);
	  if ($box.is(":checked")) {
	    var group = "input:checkbox[name='" + $box.attr("name") + "']";
	    $(group).prop("checked", false);
	    $box.prop("checked", true);
	  } else {
	    $box.prop("checked", false);
	  }
});

$("#send_money").change(function() {
	var money =  $(this).val();
	$.ajax({
                    method: 'POST',
                    url: '/ajax/ajax_chang_money.php',
                    data: { "money": money },
                    success: function(msg){
                      // console.log(msg); return false;
                       $("#send_money_text").val(msg)
                    }
                });
});

$(".del_img").click(function() {
	var confirm_del = confirm('ต้องลบไฟล์เอกสาร');
	var img_id =  $(this).attr('img_id');
	var img_name = $(this).attr('img_name');
	var num = $(this).attr('num');
	if (confirm_del) {
		$.ajax({
                    method: 'POST',
                    url: '/ajax/ajax_del_img.php',
                    data: { "img_id": img_id , "img_name" : img_name},
                    success: function(msg){
                      console.log(msg);
                      if (msg == 1) {
                       $(".num_del"+num).hide();
                      }
                      // console.log(msg); return false;
                    }
                });
	}

});

 $("#datepicker").datepicker();
 
 $("#datepicker_two").datepicker();

$(".type_req").click(function() {
			if($(this).prop('checked')) {
				$("#type_req_detail").val("");
				$("#loan_type_req_detail2").val("");
				$("#type_req_detail").attr('disabled', 'disabled');
				$("#type_req_detail").removeAttr('required', 'required');

			}
		});


$("#type_req_detail").change(function() {
		var detail = $(this).val();
		$("#loan_type_req_detail2").val(detail)
});

// $("#loan_type_req4").click(function() {
// 			if($(this).prop('checked')) {
//
// 			}
// 		});

$("#type_req4").click(function() {
			if($(this).prop('checked')) {
				$(".pay_debt_check").attr('disabled','disabled');
				$(".pay_debt_check").removeAttr('required');
				$(".pay_debt_check").removeAttr('name');
				$(".pay_debt_check").attr("checked", false);
				$(".pay_debt").attr('disabled','disabled');
				$(".pay_debt2").attr('disabled','disabled');
				$(".pay_debt").val("");
				$(".pay_debt2").val("");
				$("#type_req_detail-error").hide();
				$(".add_has").removeClass('has-error');
				$(".add_has").addClass('has-success');
				$(".add_has2").removeClass('has-error');
				$(".add_has2").addClass('has-success');
				$("#pay_debt_id-error").hide();
				$("#pay_debt-error").hide();
				$("#pay_debt_times-error").hide();
			}
		});

$(".check_pay").click(function() {
			if($(this).prop('checked')) {
				$(".pay_debt_check").attr('required', 'required');
				$(".pay_debt_check").attr('name', 'pay_debt_id');
				$(".pay_debt_check").removeAttr('disabled');
				$("#type_req_detail-error").hide();
				$(".add_has").removeClass('has-error');
				$(".add_has").addClass('has-success');
			}
		});


$("#loan_type_req4").click(function() {
			if($(this).prop('checked')) {
				$("#type_req_detail").removeAttr('disabled');
				$("#type_req_detail").focus();
				$("#type_req_detail").attr('required', 'required');
			}
		});

$("#guarantee2_check").click(function() {
			if($(this).prop('checked')) {
				$(".guarantee2").removeAttr('disabled');
			}else{
				$(".guarantee2").attr('disabled', 'disabled');
			}

		});


$(".marry_mobile").click(function() {
			if($(this).prop('checked')) {
				$("#marry_name_mobile").removeAttr('readonly');
				$("#marry_name_mobile").focus();
				$("#marry_name_mobile").attr('required', 'required');
			}else{
				$("#marry_name_mobile").attr('readonly', 'readonly');
				$("#marry_name_mobile").removeAttr('required', 'required');
				$("#marry_name_mobile").val('');
			}
});


$(".sector").click(function() {
		if($(this).prop('checked')) {
				$("#sector").removeAttr('readonly');
				$("#sector").focus();
				$("#sector").attr('required', 'required');
		}else{
				$("#sector").attr('readonly', 'readonly');
				$("#sector").removeAttr('required', 'required');
				$("#sector").val('');
		}
});

$("#month").click(function() {
		if($(this).prop('checked')) {
				$("#month_num").removeAttr('readonly');
				$("#month_num").focus();
				$("#month_num").attr('required', 'required');
		}else{
				$("#month_num").attr('readonly', 'readonly');
				$("#month_num").removeAttr('required', 'required');
				$("#month_num").val('');
		}
});

$(".department").click(function() {
			if($(this).prop('checked')) {
				$("#department").removeAttr('readonly');
				$("#department").focus();
				$("#department").attr('required', 'required');
			}else{
				$("#department").attr('readonly', 'readonly');
				$("#department").removeAttr('required', 'required');
				$("#department").val('');
			}
	});

$(".sector_mobile").click(function() {
			if($(this).prop('checked')) {
				$("#sector_mobile").removeAttr('readonly');
				$("#sector_mobile").focus();
				$("#sector_mobile").attr('required', 'required');
			}else{
				$("#sector_mobile").attr('readonly', 'readonly');
				$("#sector_mobile").removeAttr('required', 'required');
				$("#sector_mobile").val('');
			}
	});

$(".department_mobile").click(function() {
	if($(this).prop('checked')) {
		$("#department_mobile").removeAttr('readonly');
		$("#department_mobile").focus();
		$("#department_mobile").attr('required', 'required');
	}else{
		$("#department_mobile").attr('readonly', 'readonly');
		$("#department_mobile").removeAttr('required', 'required');
		$("#department_mobile").val('');
	}
});

$("#insurance_1").click(function() {
	if($(this).prop('checked')) {
		$("input[name*='bondsman']").removeAttr('readonly');
		$("input[name*='bondsman']").attr('required', 'required');

		// $("#share_money").attr('readonly', 'readonly');
		// $("#share_money").removeAttr('required', 'required');
		// $("#deposit_type").attr('readonly', 'readonly');
		// $("#deposit_type").removeAttr('required', 'required');
		// $("#deposit_no").attr('readonly', 'readonly');
		// $("#deposit_no").removeAttr('required', 'required');
		// $("#deposit_amount").attr('readonly', 'readonly');
		// $("#deposit_amount").removeAttr('required', 'required');
	}else{
		$("input[name*='bondsman']").attr('readonly', 'readonly');
		$("input[name*='bondsman']").removeAttr('required', 'required');
		$("input[name*='bondsman']").val('');
	}
});

$("#insurance_2").click(function() {
	if($(this).prop('checked')) {
		$("#share_money").removeAttr('readonly');
		$("#share_money").focus();
		$("#share_money").attr('required', 'required');

		// $("input[name*='bondsman']").attr('readonly', 'readonly');
		// $("input[name*='bondsman']").removeAttr('required', 'required');
		// $("#deposit_type").attr('readonly', 'readonly');
		// $("#deposit_type").removeAttr('required', 'required');
		// $("#deposit_no").attr('readonly', 'readonly');
		// $("#deposit_no").removeAttr('required', 'required');
		// $("#deposit_amount").attr('readonly', 'readonly');
		// $("#deposit_amount").removeAttr('required', 'required');
	}else{
		$("#share_money").attr('readonly', 'readonly');
		$("#share_money").removeAttr('required', 'required');
		$("#share_money").val('');
	}
});

$("#insurance_3").click(function() {
	if($(this).prop('checked')) {
		$("#deposit_type").removeAttr('readonly');
		$("#deposit_type").attr('required', 'required');
		$("#deposit_no").removeAttr('readonly');
		$("#deposit_no").attr('required', 'required');
		$("#deposit_amount").removeAttr('readonly');
		$("#deposit_amount").attr('required', 'required');

		// $("input[name*='bondsman']").attr('readonly', 'readonly');
		// $("input[name*='bondsman']").removeAttr('required', 'required');
		// $("#share_money").attr('readonly', 'readonly');
		// $("#share_money").removeAttr('required', 'required');

	}else{
		$("#deposit_type").attr('readonly', 'readonly');
		$("#deposit_type").removeAttr('required', 'required');
		$("#deposit_type").val('');

		$("#deposit_no").attr('readonly', 'readonly');
		$("#deposit_no").removeAttr('required', 'required');
		$("#deposit_no").val('');

		$("#deposit_amount").attr('readonly', 'readonly');
		$("#deposit_amount").removeAttr('required', 'required');
		$("#deposit_amount").val('');
	}
});




  $("#check_submit").click(function() {
			if($(this).prop('checked')) {
				$("#bt_submit").removeAttr('disabled');
			}else{
				$("#bt_submit").attr('disabled', 'disabled');
			}
		});



   $("#type_req1").click(function() {
			if($(this).prop('checked')) {
				$(".loan_money_req").attr('disabled','disabled');
				$(".loan_money_req").removeAttr('required');
			}
		});



$("#use_money").click(function() {
			var $box = $(this);
			  if ($box.is(":checked")) {
			    var group = "input:checkbox[name='" + $box.attr("name") + "']";
			    $(group).prop("checked", false);
			    $box.prop("checked", true);
			  } else {
			    $box.prop("checked", false);
			  }
			if($(this).prop('checked')) {
				$("#use_money_detail").removeAttr('disabled','disabled');
				$("#use_money_detail").attr('required','required');
				$("#use_money_detail").focus();
			}else{
				$("#use_money_detail").attr('disabled','disabled');
				$("#use_money_detail").removeAttr('required','required');
				$("#use_money_detail").val('');
			}
		});

$("#use_money_check").click(function() {
	var $box = $(this);
	  if ($box.is(":checked")) {
	    var group = "input:checkbox[name='" + $box.attr("name") + "']";
	    $(group).prop("checked", false);
	    $box.prop("checked", true);
	  } else {
	    $box.prop("checked", false);
	  }
			if($(this).prop('checked')) {
				$("#use_money_detail").removeAttr('required','required');
				$("#use_money_detail").attr('disabled','disabled');
				$("#use_money_detail").val("");
			}
		});

$("#pay_debt").click(function() {

			var $box = $(this);
			  if ($box.is(":checked")) {
			    var group = "input:checkbox[name='" + $box.attr("name") + "']";
			    $(group).prop("checked", false);
			    $box.prop("checked", true);
			  } else {
			    $box.prop("checked", false);
			  }

			if($(this).prop('checked')) {
				$(".pay_debt").removeAttr('disabled','disabled');
				$(".pay_debt").attr('required','required');
				$(".pay_debt2").attr('disabled','disabled');
				$(".pay_debt2").removeAttr('required','required');
				$(".pay_debt2").val("");
				$(".pay_debt_focus").focus();
			}else{
				$(".pay_debt").attr('disabled','disabled');
				$(".pay_debt").removeAttr('required','required');
				$(".pay_debt").val("");
			}
		});

$("#pay_debt2").click(function() {

			var $box = $(this);
			  if ($box.is(":checked")) {
			    var group = "input:checkbox[name='" + $box.attr("name") + "']";
			    $(group).prop("checked", false);
			    $box.prop("checked", true);
			  } else {
			    $box.prop("checked", false);
			  }

			if($(this).prop('checked')) {
				$(".pay_debt").attr('disabled','disabled');
				$(".pay_debt").removeAttr('required','required');
				$(".pay_debt").val("");
				$(".pay_debt2").removeAttr('disabled','disabled');
				$(".pay_debt2").attr('required','required');
				$(".pay_debt_focus2").focus();
			}else{
				$(".pay_debt2").attr('disabled','disabled');
				$(".pay_debt2").removeAttr('required','required');
				$(".pay_debt2").val("");
			}
		});

$("#transfer_money1").click(function() {

					var $box = $(this);
					  if ($box.is(":checked")) {
					    var group = "input:checkbox[name='" + $box.attr("name") + "']";
					    $(group).prop("checked", false);
					    $box.prop("checked", true);
					  } else {
					    $box.prop("checked", false);
					  }

			if($(this).prop('checked')) {
				$("#transfer_detail_1").removeAttr('disabled','disabled');
				$("#transfer_detail_1").attr('required','required');
				$("#transfer_detail_2").attr('disabled','disabled');
				$("#transfer_detail_2").removeAttr('required','required');
				$("#transfer_detail_2").val("");
				$("#transfer_detail_1").focus();
			}else{
				$("#transfer_detail_1").val("");
				$("#transfer_detail_1").attr('disabled','disabled');
				$("#transfer_detail_1").removeAttr('required','required');
			}
		});

$("#transfer_money2").click(function() {

			var $box = $(this);
			  if ($box.is(":checked")) {
			    var group = "input:checkbox[name='" + $box.attr("name") + "']";
			    $(group).prop("checked", false);
			    $box.prop("checked", true);
			  } else {
			    $box.prop("checked", false);
			  }

			if($(this).prop('checked')) {
				$("#transfer_detail_2").removeAttr('disabled','disabled');
				$("#transfer_detail_2").attr('required','required');
				$("#transfer_detail_1").attr('disabled','disabled');
				$("#transfer_detail_1").removeAttr('required','required');
				$("#transfer_detail_1").val("");
				$("#transfer_detail_2").focus();
			}else{
				$("#transfer_detail_2").val("");
				$("#transfer_detail_2").attr('disabled','disabled');
				$("#transfer_detail_2").removeAttr('required','required');
			}
		});

$(".radio_check").click(function() {
			$(".type_req2").prop('checked', false);

			 var $box = $(this);
			  if ($box.is(":checked")) {
			    var group = "input:checkbox[name='" + $box.attr("name") + "']";
			    $(group).prop("checked", false);
			    $box.prop("checked", true);
			  } else {
			    $box.prop("checked", false);
			  }

			if($(this).prop('checked')) {
				var check = $(this).attr('radio');
				$("."+check).attr('checked', 'checked');
			}
		});



            $('.province').change(function(){
                  var province_id = $(this).val();
                  $.ajax({
                    method: 'POST',
                    url: '/ajax/ajax_select_address.php',
                    data: { "province_id": province_id },
                    success: function(msg){
                      // console.log(msg); return false;
                       $(".amphur").html(msg)
                    }
                });
              });

                $('.amphur').change(function(){
                  var amphur_id = $(this).val();
                  $.ajax({
                    method: 'POST',
                    url: '/ajax/ajax_select_address.php',
                    data: { "amphur_id": amphur_id},
                    success: function(msg){
                      // console.log(msg); return false;
                       $(".district").html(msg)
                    }
                });
              });

              //   $('.district').change(function(){
              //     var district_id = $(this).val();
              //     $.ajax({
              //       method: 'POST',
              //       url: '/ajax/ajax_select_address.php',
              //       data: { "district_id": district_id},
              //       success: function(msg){
              //          $(".zipcode").html(msg)
              //       }
              //   });
              // });

                  </script>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
