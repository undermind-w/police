<?php
	include "config.inc.php";
	require PATH . "/class/pagination.class.php"  ;
	function date_thai_format($date,$long_month='L',$with_time="1"){
		$long_month_arr = array('1'=>'มกราคม','2'=>'กุุมภาพันธ์','3'=>'มีนาคม','4'=>'เมษายน','5'=>'พฤษภาคม','6'=>'มิถุนายน','7'=>'กรกฎาคม','8'=>'สิงหาคม','9'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
		$short_month_arr = array('1'=>'ม.ค.','2'=>'ก.พ.','3'=>'มี.ค.','4'=>'เม.ย.','5'=>'พ.ค.','6'=>'มิ.ย.','7'=>'ก.ค.','8'=>'ส.ค.','9'=>'ก.ย.','10'=>'ต.ค.','11'=>'พ.ย.','12'=>'ธ.ค.');
		
		$day = date('d',strtotime($date));
		$month = date('m',strtotime($date));
		$year = date('Y',strtotime($date))+543;
		$time = date('H:i น.',strtotime($date));
		
		if($long_month == 'L'){
			$text = (int)$day." ".$long_month_arr[(int)$month]." ".$year;
		}else{
			$text = (int)$day." ".$short_month_arr[(int)$month]." ".$year;
		}
		if($with_time == '1'){
			$text .= " ".$time;
		}
		return $text;
	}
?>
<HTML>
<head>
 <?php
	include "header_mobile.inc.php";
	$param = '';
	foreach($_GET as $key => $value){
		$param .= $key."&";
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	$param = substr($param,0,-1);
 ?>
<style>
	body {
		background-color:#FFF;
	}
	.panel-heading {
		border-bottom:1px solid;
	}
	.menu_gruop{
		margin:5px;
	}
	.icon_color{
		color: rgb(235,6,141);
	}
	.menu_link:active{
		 background-color:rgb(212,212,212);;
	} 
	h3{
		margin-top:12px;
		margin-bottom:12px;
	}
	.tr_border{
		border-bottom:1px solid rgb(212,212,212);
	}
	.text_show{
		font-size: 14px;
	}
	.date_show{
		
		color: #AAA;
		font-size: 12px;
	}
	body{
		font-family:"Sukhumvit Set",sans-serif !important;
	}
</style>
</head>
<body>
<?php 
$icon = 'fa fa-chevron-circle-right fa-stack-2x text-material-light-blue-300'; 
$loan_type_arr = array('1'=>'การกู้สามัญ','3'=>'การกู้ฉุกเฉิน');
?>
<table width="100%" >
<?php 
	$sql = "SELECT * , cmp_admin_loan_money.loan_date,  cmp_admin_loan_money.email
	FROM 
		cmp_admin_loan_money 
		INNER JOIN loan_application ON cmp_admin_loan_money.loan_id = loan_application.loan_id
	WHERE cmp_admin_loan_money.member_id='".$_GET['member_no']."' AND loan_application.application_status='1' ORDER BY cmp_admin_loan_money.loan_id DESC";
	$rs = $mysqli->query($sql);
	//echo $sql;
	while($row = $rs->fetch_assoc()){
		$sql_result = "SELECT * FROM loan_result WHERE loan_id='".$row['loan_id']."'";
		$rs_result = $mysqli->query($sql_result);
		$row_result = $rs_result->fetch_assoc();
		
		$sql_check_doc = "SELECT * FROM loan_application_doc WHERE loan_id = '".$row['loan_id']."' AND loan_application_id = '".$row['loan_application_id']."' ";
		$rs_check_doc = $mysqli->query($sql_check_doc);
		$row_check_doc = $rs_check_doc->fetch_assoc();
		//if(empty($row_check_doc)){ continue; }
?>
		<tr class="tr_border">
			<td width="20%" align="center" valign="middle">
				<span class="icon_color fa-stack fa-1x">
						<i class="<?php echo $icon; ?>"></i>
				</span>
			</td>
			<td valign="middle">
				<span class="text_show"><?php echo $loan_type_arr[$row['loan_application_type']]; ?></span>
				<br>
				<span class="date_show"><?php echo date_thai_format($row['loan_date']); ?></span>
				
				<?php 
				/*if($row["c_contract_doc"] == 1){
					$link = "loan_all_pdf.php?loan_application_id=".$row['loan_application_id']."&loan_id=".$row['loan_id'];
				}else{ 
					 $link = "loan_petition_pdf.php?loan_application_id=".$row['loan_application_id']."&loan_id=".$row['loan_id'];
				} */
				?>
			</td>
			<td>
				<?php
				if($row['loan_application_type']=='3'){
					$link = "loan_emergent_pdf.php?".base64_encode("loan_application_id=".$row['loan_application_id'])."&".base64_encode("loan_id=".$row['loan_id']);
				}else{
					$link = "loan_all_pdf.php?".base64_encode("loan_application_id=".$row['loan_application_id'])."&".base64_encode("loan_id=".$row['loan_id']);
				}
				?>
				<a target="_blank" id="btn_download"  style="margin-top:10px;margin-bottom:10px; font-size: 12px;background-color: #20449a;border-color: #20449a;color:#fff;" class='btn' href="<?php echo $link; ?>">Download</a>
				<!--a style="margin-top:10px;margin-bottom:10px; font-size: 12px;background-color: #20449a;border-color: #20449a;color:#fff;" class='btn' onclick="send_email('<?php echo $row['loan_id']; ?>','<?php echo $row['loan_application_id']; ?>','<?php echo $row['email']; ?>','<?php echo $link; ?>')">ส่งเข้า email</a-->
			</td>
		</tr>
	<?php } ?>
</table>
<div class="row">
	<div style="margin-top: 1.5em;" class="text-center">
		<div class="col-sm-12">
			<div class="col-sm-12" style="margin-bottom:15px;">
				<a onclick="link_click('loan_main_menu_mobile.php?<?php echo $param; ?>')">
					<button style="font-size: 22px;font-family: 'thaisans_neueregular' !important;width:70%" type="button" class="btn btn-material-light-blue-300">กลับหน้าหลัก</button>
				</a>
			</div>
		</div>
	</div>
</div>
<script>
function link_click(link){
	location.href = link;
}
function change_link(){
	$('#btn_download').attr('href',$('#loan_document').val());
}
function send_email(loan_id , loan_application_id , email , link){
	$.post("ajax/ajax_send_loan_email.php", 
	{	
		loan_id : loan_id,
		loan_application_id : loan_application_id,
		email : email,
		link : link
	}
	, function(result){
		swal('ส่ง email สำเร็จ');
	});
}
</script>
</body>
</HTML>
