<?php
	include "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}
	function cal_age($birthday,$type = 'y'){     //รูปแบบการเก็บค่าข้อมูลวันเกิด
		$birthday = date("Y-m-d",strtotime($birthday)); 
		$today = date("Y-m-d");   //จุดต้องเปลี่ยน
		list($byear, $bmonth, $bday)= explode("-",$birthday);       //จุดต้องเปลี่ยน
		list($tyear, $tmonth, $tday)= explode("-",$today);                //จุดต้องเปลี่ยน
		$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear);
		$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
		$mage = ($mnow - $mbirthday);
		//echo "วันเกิด $birthday"."<br>\n";
		//echo "วันที่ปัจจุบัน $today"."<br>\n";
		//echo "รับค่า $mage"."<br>\n";
		$u_y=date("Y", $mage)-1970;
		$u_m=date("m",$mage)-1;
		$u_d=date("d",$mage)-1;
		if($type=='y'){
			return $u_y;
		}else if($type=='m'){
			return $u_m;
		}else{
			return $u_d;
		}
	}

	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/book_acceptance.pdf" ;
	
	
	$pdf = new FPDI();
	
	$pageCount = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* 
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_result t3 ON t1.loan_id = t3.loan_id
			WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member WHERE member_no = '".$row_loan['member_id']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 77;
			$pdf->SetXY( 132, $y_point );
			$pdf->MultiCell(50, 5, U2T(format_date($row_loan['application_date'])), $border, 1);
			
			$y_point = 84;
			$pdf->SetXY( 37, $y_point );
			$pdf->MultiCell(96, 5, U2T($row_loan['member_name']), $border, 1);
			$pdf->SetXY( 154, $y_point );
			$pdf->MultiCell(38, 5, U2T($row_loan['employee_id']), $border, 1);
			
			$y_point = 91;
			$pdf->SetXY( 25, $y_point );
			$pdf->MultiCell(70, 5, U2T($row_loan['member_position']), $border, 1);
			$pdf->SetXY( 137, $y_point );
			$pdf->MultiCell(60, 5, U2T($row_loan['member_branch_name']), $border, 1);
			
			$y_point = 98;
			$pdf->SetXY( 38, $y_point );
			$pdf->MultiCell(57, 5, U2T($row_loan['member_sector_name']), $border, 1);
			$pdf->SetXY( 121, $y_point );
			$pdf->MultiCell(53, 5, U2T($row_loan['member_department_name']), $border, 1);
			
			$y_point = 104;
			$pdf->SetXY( 97, $y_point );
			$pdf->MultiCell(63, 5, U2T($row_loan['member_id']), $border, 1);//เลขที่สมาชิก
			
			$y_point = 118;
			$pdf->SetXY( 107, $y_point );
			$pdf->MultiCell(54, 5, U2T(''), $border, 1);//เลขที่หนังสือกู้เงิน
			$pdf->SetXY( 172, $y_point );
			$pdf->MultiCell(30, 5, U2T(''), $border, 1);//วันที่หนังสือกู้เงิน
			
			$y_point = 125;
			$pdf->SetXY( 90, $y_point );
			$pdf->MultiCell(32, 5, U2T(num_format($row_loan['loan_approve_money'])), $border, 'R');
			$pdf->SetXY( 131, $y_point );
			$pdf->MultiCell(64, 5, U2T(convert($row_loan['loan_approve_money'])), $border, 'C');
			
			$y_point = 138;
			$pdf->SetXY( 20, $y_point );
			$pdf->MultiCell(45, 5, U2T($row_loan['deposit_no']), $border, 1);
			$pdf->SetXY( 80, $y_point );
			$pdf->MultiCell(54, 5, U2T(num_format($row_loan['deposit_amount'])), $border, 'R');
			
			$y_point = 195;
			$pdf->SetXY( 80, $y_point );
			$pdf->MultiCell(63, 5, U2T($row_loan['member_name']), $border, 'C');
		}
		
	}
	
	$pdf->Output();