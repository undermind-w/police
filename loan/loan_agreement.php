
<?php
	include "config.inc.php";
	define('FPDF_FONTPATH',"{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/font/");
	require_once("{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/fpdf.php");
	require_once("{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php");
	function U2T($text) { return iconv("UTF-8", "TIS-620", trim($text)); }
	
	$sql = "SELECT * FROM loan_application WHERE loan_id = '864' AND loan_application_id =  '706' ";
	$rs = $mysqli->query($sql);
	$row = $rs->fetch_assoc();
	
	$name = "นาย ทินกร จุมปี";

	$pdf = new FPDI('P','mm','A4');
	$pdf->AddFont('angsa', null, 'angsa.php');
	$pdf->SetFont('angsa', null, 14);

	$pageCount_1 = $pdf->setSourceFile('Document/loan_ageement_1.pdf');
	for ($pageNo = 1; $pageNo <= $pageCount_1; $pageNo++) {

		$templateId = $pdf->importPage($pageNo);

		$size = $pdf->getTemplateSize($templateId);

		if ($size['w'] > $size['h']) {
			$pdf->AddPage('L', array($size['w'], $size['h']));
		} else {
			$pdf->AddPage('P', array($size['w'], $size['h']));
		}

		$pdf->useTemplate($templateId);
		
		$pdf->SetXY(34, 53);
		$pdf->Write(5, U2T("0000001"));

		$pdf->SetXY(100, 67);
		$pdf->Write(5, U2T("12/09/2560"));

		$pdf->SetXY(57, 74.5);
		$pdf->Write(5, U2T($name));
		$pdf->SetXY(106, 74.5);
		$pdf->Write(5, U2T("25"));

		$pdf->SetXY(47, 83);
		$pdf->Write(5, U2T("123456"));
		$pdf->SetXY(135, 83);
		$pdf->Write(5, U2T("Tester"));

		$pdf->SetXY(55, 90);
		$pdf->Write(5, U2T("เชียงใหม่"));
		$pdf->SetXY(132, 90);
		$pdf->Write(5, U2T("82"));
		$pdf->SetXY(165, 90);
		$pdf->Write(5, U2T("เชียงใหม่ - พร้าว"));

		$pdf->SetXY(45, 98);
		$pdf->Write(5, U2T("2"));
		$pdf->SetXY(104, 98);
		$pdf->Write(5, U2T("แม่แฝก"));
		$pdf->SetXY(163, 98);
		$pdf->Write(5, U2T("สันทราย"));

		$pdf->SetXY(36, 105);
		$pdf->Write(5, U2T("เชียงใหม่"));
		$pdf->SetXY(103, 105);
		$pdf->Write(5, U2T("0918595385"));

		$pdf->SetXY(125, 136);
		$pdf->Write(5, U2T(number_format("150000",0,".",",")));

		$pdf->SetXY(33, 144);
		$pdf->Write(5, U2T("หนึ่งแสนห้าหมื่นบาทถ้วน"));

		$pdf->SetXY(20, 151);
		$pdf->Write(5, U2T("เพื่อลงทุน IOT"));
		
		$pdf->SetXY(125, 159.5);
		$pdf->Write(5, U2T(number_format("15000",0,".",",")));

		$pdf->SetXY(33, 167);
		$pdf->Write(5, U2T("หนึ่งหมื่นห้าพันบาทถ้วน"));

		$pdf->SetXY(128, 175);
		$pdf->Write(5, U2T("15.67"));

		$pdf->SetXY(33, 182.5);
		$pdf->Write(5, U2T(number_format("15000",0,".","")));

		$pdf->SetXY(161, 182.5);
		$pdf->Write(5, U2T("2"));

		$pdf->SetXY(48, 190.5);
		$pdf->Write(5, U2T("กันยายน"));

		$pdf->SetXY(109, 205.5);
		$pdf->Write(5, U2T("25.67"));

	}

	$pageCount_2 = $pdf->setSourceFile('Document/loan_ageement_2.pdf');
	for ($pageNo = 1; $pageNo <= $pageCount_2; $pageNo++) {

		$templateId = $pdf->importPage($pageNo);

		$size = $pdf->getTemplateSize($templateId);

		if ($size['w'] > $size['h']) {
			$pdf->AddPage('L', array($size['w'], $size['h']));
		} else {
			$pdf->AddPage('P', array($size['w'], $size['h']));
		}

		$pdf->useTemplate($templateId);
		
		$pdf->SetXY(124, 12.5);
		$pdf->Write(5, U2T("2"));

		$pdf->SetXY(102, 166);
		$pdf->Write(5, U2T($name));

		$pdf->SetXY(102, 190);
		$pdf->Write(5, U2T($name));

		$pdf->SetXY(102, 213.5);
		$pdf->Write(5, U2T($name));

		$pdf->SetXY(102, 237.5);
		$pdf->Write(5, U2T($name));

	}


	$pdf->SetTitle('Loan_ageement');
	$pdf->Output('Loan_ageement.pdf','I');  

	?>