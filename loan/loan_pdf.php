<?php
	include "config.inc.php";
	//require PATH."/class/connect_odbc.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	//function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}	
	
	//bG9hbl9pZD0xMDgz
	//echo '<pre>'; print_r(base64_encode("loan_id=".$_GET['loan_id'])); echo '</pre>'; exit;
	//$_GET['loan_application_id'] = base64_decode($_GET['loan_application_id']);
	foreach($_GET as $key => $value){
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	
	$pdf = new FPDI();
	
	//หาคำร้องขอกู้
	$sql_doc = "SELECT
			t1.loan_application_id,
			t1.loan_application_type,
			t3.description,
			t3.document_link
		FROM
			loan_application t1
		INNER JOIN loan_document_setting t2 ON t1.loan_application_type = t2.loan_type_id
		INNER JOIN loan_document t3 ON t2.document_id = t3.id
		WHERE
			t1.loan_id = '".$_GET['loan_id']."' 
		ORDER BY t3.order_by";
		$rs_doc= $mysqli->query($sql_doc);
		$arr_doc_use =array();
		while($row_doc = $rs_doc->fetch_assoc()){
			$arr_doc_use[]  = $row_doc;
			
		}
		//echo '<pre>'; print_r($arr_doc_use); echo '</pre>';
		//exit;		
		//$row_doc = $rs_doc->fetch_assoc();
		//$document_link = $row_doc['document_link'];
		
		$loan_application_type = array();
		$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
		$rs_type = $mysqli->query($sql_type);
		while($row_type = $rs_type->fetch_assoc()){
			
			$loan_application_type[$row_type['id']] = $row_type['loan_name'];
		}
	
		$sql_loan = "SELECT 
			* ,t3.loan_reason as member_use
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_reason t3 ON t2.member_use = t3.id
		WHERE t2.loan_id = '".$_GET['loan_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$member_no = $row_loan['member_id'];
		$sql_profile = "SELECT FLOMMAST.*, FLOPDEPT.DEPT_NAME,FLOPSTATUS.*, FLOPSTATUS.STS_TYPE_DESC
			FROM LOAN.dbo.FLOMMAST FLOMMAST
				INNER JOIN LOAN.dbo.FLOPDEPT FLOPDEPT ON FLOMMAST.MAST_MEMB_DEPT = FLOPDEPT.DEPT_NO
				INNER JOIN LOAN.dbo.FLOPSTATUS FLOPSTATUS ON FLOMMAST.MAST_MEMB_TYPE = FLOPSTATUS.STS_TYPE_ID AND FLOPSTATUS.STS_CODE = '01'
			WHERE FLOMMAST.MAST_MEMB_NO = '{$member_no}'";
		$rs_profile = odbc_exec($odbc_conn, $sql_profile) or die(odbc_errormsg());
		if($row_profile = odbc_fetch_array($rs_profile)) {
			$row_profile = t2u_array($row_profile);
			$bd_date = new DateTime(ConvertToSQLDate($row_profile["MAST_BIRTH_YMD"]));
			$bd_interval = $bd_date->diff(new DateTime("now"));
			$md_date = new DateTime(ConvertToSQLDate($row_profile["MAST_YMD_IN"]));
			$md_interval = $md_date->diff(new DateTime("now"));
			
			$sql_data = "SELECT email, telmobile
						FROM cmp_imp_member_data
						WHERE member_no = '{$member_no}'";
			$rs_data = $mysqli->query($sql_data);
			$row_data = $rs_data->fetch_assoc();
			
			//echo '<pre>'; print_r($row_profile); echo '</pre>';
			$row_member = [
						'member_no' => $row_profile['MAST_MEMB_NO']
					,	'memname' => $row_profile['MAST_FNAME'].' '.$row_profile['MAST_NAME']
					,	'member_age' => $bd_interval->format('%Y')
					//,	'member_age' => $bd_interval->format('%Y ปี %m เดือน')
					,	'memdtm' =>  ConvertToSQLDate($row_profile['MAST_YMD_IN'])
					,	'member_register' =>  ConvertToThaiDate(ConvertToSQLDate($row_profile['MAST_YMD_IN']))
					,	'member_register_age' => $md_interval->format('%Y ปี %m เดือน')
					,	'email' => (!empty($row_data['email']))?$row_data['email']:$row_profile['MAST_EMAIL']
					,	'telmobile' => (!empty($row_data['telmobile']))?$row_data['telmobile']:$row_profile['MAST_MOBILE']
					,	'member_address' => sprintf('%s %s', $row_profile['MAST_ADDR'], $row_profile['MAST_ZIP'])
					,	'member_group' => $row_profile['DEPT_NAME']					// หน่วยงาน
					,	'member_type_id' => $row_profile['STS_TYPE_ID']			// รหัสประเภทสมาชิก
					,	'member_type' => $row_profile['STS_TYPE_DESC']			// ประเภทสมาชิก
					,	'member_bd' => ConvertToThaiDate(ConvertToSQLDate($row_profile['MAST_BIRTH_YMD'])) // วันเกิด
					,	'card_person' => $row_profile['MAST_CARD_ID']			// เลขบัตรประชาชน
					,	'salary_amount' => $row_profile['MAST_SALARY']
					];
					
					$application_date = $row_loan["application_date"];
					$loan_day = date('d', strtotime($application_date));
					$loan_month = MonthName((int)date('m', strtotime($application_date)));
					$loan_year = date('Y', strtotime($application_date))+543;
			
							
		}
		if($row_loan['account_bank_number'] != ''){
				$sql = "SELECT FSSMAS.*, FSSRATE_TYPE.RATE_DESC
				FROM SAVING.dbo.FSSMAS FSSMAS
					INNER JOIN SAVING.dbo.FSSRATE_TYPE FSSRATE_TYPE ON FSSMAS.MAS_RATE_CODE = FSSRATE_TYPE.RATE_CODE
				WHERE FSSMAS.MAS_MEMB_NO = '{$member_no}' AND FSSMAS.MAS_REC_STS <> 'C'";
				$rs = odbc_exec($odbc_conn, $sql) or die(odbc_errormsg());
				$is_found = false;
				$sum = 0;
				while($row = odbc_fetch_array($rs)) {
					$row = t2u_array($row);
					$is_found = true;
					
					$sql = "SELECT ATM_ID
								FROM LOAN.dbo.FLOMATM_D FLOMATM_D
								WHERE ATM_ITEM_TYPE = 'D' AND ATM_REF_NO = '{$row["MAS_BRANCH"]}{$row["MAS_AC_TYPE"]}{$row["MAS_AC_NO"]}{$row["MAS_AC_CD"]}'";
					$rs_atm = odbc_exec($odbc_conn, $sql);
					$suffix = "";
					if($row_atm = odbc_fetch_array($rs_atm)) {
						$suffix = " (SIB)";
					}
					$post_account_pk  = $row_loan['account_bank_number'];
					$account_pk = $row['MAS_BRANCH']."-".$row['MAS_AC_TYPE']."-".$row['MAS_AC_NO']."-".$row['MAS_AC_CD'];
					if($post_account_pk == $account_pk){
						$row_account = 
								[
									'account_branch' 		=> $row['MAS_BRANCH']
								,	'account_type_code' 	=> $row['MAS_RATE_CODE']
								,	'account_no' 	=> $row['MAS_BRANCH'].$row['MAS_AC_TYPE']."-".$row['MAS_AC_NO']."-".$row['MAS_AC_CD'].$suffix
								,	'account_cd' 	=> $row['MAS_AC_CD']
								,	'account_pk' 	=> $row['MAS_BRANCH']."-".$row['MAS_AC_TYPE']."-".$row['MAS_AC_NO']."-".$row['MAS_AC_CD']
								,	'account_name' 	=> $row['MAS_AC_NAME']
								,	'balance' 			=> $row['MAS_BOOK_BALANCE']
								,	'account_type' 	=> $row['RATE_DESC']
								];
							
					}										
				}
			}
						
	
	$sql_doc_all = "SELECT * FROM loan_document	ORDER BY order_by";
	$rs_doc_all= $mysqli->query($sql_doc_all);
	//while($row_doc_all = $rs_doc_all->fetch_assoc()){
		
		
	foreach($arr_doc_use AS $key=> $row_doc_all){	
		//echo '<pre>'; print_r($row_doc_all); echo '</pre>';
		//$document_link = $row_doc_all['document_link'];
		$document_link = $row_doc_all['document_link'];
		$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/".$document_link;
		$pageCount = $pdf->setSourceFile($filename);
		
		for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
			$pdf->AddPage();
			$tplIdx = $pdf->importPage($pageNo); 
			$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
			
			$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
			$pdf->SetFont('THSarabunNew', '', 13 );
			
			$border = 0;
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetAutoPageBreak(true,0);			
			
				if($document_link == 'loan_petition.pdf'){	
					if($pageNo == '1'){
						$y_point = 37.5;
						$pdf->SetXY( 148, $y_point );
						$pdf->MultiCell(65, 5, U2T($row_loan['member_id']), $border, 1); //เลขที่สมาชิก
						
						$y_point = 63;
						$pdf->SetXY( 44, $y_point );
						$pdf->MultiCell(65, 5, U2T($loan_application_type[$row_loan['loan_application_type']]), $border, 1); //ประเภทเงินกู้					
						
						$y_point = 85.5;
						$pdf->SetXY( 123, $y_point );
						$pdf->MultiCell(10, 5, U2T($loan_day), $border, 'C');//วันที่
						$pdf->SetXY( 138, $y_point );
						$pdf->MultiCell(25, 5, U2T($loan_month), $border, 'C');//เดือน
						$pdf->SetXY( 167, $y_point );
						$pdf->MultiCell(15, 5, U2T($loan_year), $border, 'C');//พ.ศ.
						
						$y_point = 96.5;
						$pdf->SetXY( 75, $y_point );
						$pdf->MultiCell(65, 5, U2T($row_loan['member_name']), $border, 1); //ชื่อผู้กู้
						$pdf->SetXY( 127, $y_point );
						$pdf->MultiCell(65, 5, U2T($row_loan['member_name']), $border, 1); //นามสกุลผู้กู้				
						$pdf->SetXY( 184, $y_point );
						$pdf->MultiCell(14, 5, U2T($row_member['member_age']), $border, 'C'); //อายุ
						
						$y_point = 104;
						$pdf->SetXY( 75, $y_point );
						$pdf->MultiCell(65, 5, U2T($row_loan['member_use']), $border, 1); //โดยนำไปใช้เพื่อ (เหตุผลการกู้)
						
						$y_point = 111.5;
						$pdf->SetXY( 29, $y_point );
						$pdf->MultiCell(32, 5, U2T(num_format($row_loan['loan_money'])), $border, 'C');	//จำนวนเงิน			
						$pdf->SetXY( 70, $y_point );
						$pdf->MultiCell(58, 5, U2T($row_loan['loan_money_text']), $border,  'C'); //จำนวนเงิน ตัวอักษร
						
						$pdf->SetFont('THSarabunNew', '', 9 );
						$y_point = 120;
						$pdf->SetXY( 26, $y_point );
						$pdf->MultiCell(45, 5, U2T($row_loan['member_position']), $border, 1);//ตำแหน่งผู้กู้
						
						$y_point = 119.5;
						$pdf->SetFont('THSarabunNew', '', 13 );
						$pdf->SetXY( 68, $y_point );
						$pdf->MultiCell(40, 5, U2T(num_format($row_loan['member_salary'])), $border, 'C');//เงินเดือน
						
						
						//$y_point = 127;
						//$pdf->SetXY( 28, $y_point );
						//$pdf->MultiCell(40, 5, U2T($row_member['member_address']), $border, 1);//ที่อยุ่
						
						/*$y_point = 127;
						$pdf->SetXY( 28, $y_point );
						$pdf->MultiCell(40, 5, U2T('หมู่บ้าน'), $border, 1);//ห้องเลขที่
						$pdf->SetXY( 49, $y_point );
						$pdf->MultiCell(40, 5, U2T('หมู่บ้าน'), $border, 1);//ชั้นที่
						$pdf->SetXY( 73, $y_point );
						$pdf->MultiCell(40, 5, U2T('หมู่บ้าน'), $border, 1);//หมู่บ้าน
						$pdf->SetXY( 106, $y_point );
						$pdf->MultiCell(15, 5, U2T('เลขที่'), $border, 'C');//เลขที่
						$pdf->SetXY( 125, $y_point );
						$pdf->MultiCell(15, 5, U2T('หมู่ที่'), $border, 'C');//หมู่ที่	
						*/
					}else if($pageNo == '2'){
						$y_point = 228;
						$pdf->SetXY( 25, $y_point );
						$pdf->MultiCell(52, 5, U2T($row_loan['member_name']), $border, 'C'); //ชื่อ นามสกุลผู้กู้
					}
				}
				
				if($document_link == 'loan_special.pdf'){				
					if($pageNo == '1'){
						$y_point = 48.5;
						$pdf->SetXY( 80, $y_point );
						$pdf->MultiCell(10, 5, U2T($loan_day), $border, 'C');//วันที่
						$pdf->SetXY( 93, $y_point );
						$pdf->MultiCell(25, 5, U2T($loan_month), $border, 'C');//เดือน
						$pdf->SetXY( 119, $y_point );
						$pdf->MultiCell(15, 5, U2T($loan_year), $border, 'C');//พ.ศ.
						
						$y_point = 62.5;
						$pdf->SetXY( 35, $y_point );
						$pdf->MultiCell(65, 5, U2T($row_loan['member_name']), $border, 'C'); //ชื่อ นามสกุลผู้กู้	
						$pdf->SetXY( 127, $y_point );
						$pdf->MultiCell(30, 5, U2T($row_loan['member_id']), $border, 'C'); //เลขที่สมาชิก
						
						$y_point = 77;
						$pdf->SetXY( 128, $y_point );
						$pdf->MultiCell(40, 5, U2T($row_loan['member_tel']), $border, 'C'); //เบอร์โทรผู้กู้	
						
						$y_point = 98.5;
						$pdf->SetXY( 94, $y_point );
						$pdf->MultiCell(65, 5, U2T($row_loan['member_use']), $border, 1); //โดยนำไปใช้เพื่อ (เหตุผลการกู้)
						
						$y_point = 105.5;
						$pdf->SetXY( 37, $y_point );
						$pdf->MultiCell(40, 5, U2T(num_format($row_loan['loan_money'])), $border, 'C');	//จำนวนเงิน	
						
						$pdf->SetXY( 92, $y_point );
						$pdf->MultiCell(85, 5, U2T($row_loan['loan_money_text']), $border,  'C'); //จำนวนเงิน ตัวอักษร
						
						$y_point = 112.5;
						$pdf->SetXY( 28, $y_point );
						$pdf->MultiCell(28, 5, U2T(num_format($row_loan['estimate_money_period'])), $border,  'C'); //ชำระงวดละ
						
						$y_point = 112.5;
						$pdf->SetXY( 92, $y_point );
						$pdf->MultiCell(15, 5, U2T($row_loan['period']), $border,  'C'); //จำนวนงวด
						
						if($row_loan['guarantee_share_value'] != ''){
							$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 25,128,4,0, '', '', true);
							
							$y_point = 127;
							$pdf->SetXY( 132, $y_point );
							$pdf->MultiCell(38, 5, U2T(num_format($row_loan['guarantee_share_value'])), $border,  'C'); //จำนวนหุ้น บาท
						}						
						
						if($row_loan['account_bank_number'] != ''){
							$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 25,141,4,0, '', '', true);
							
							$y_point = 148;
							$pdf->SetXY( 22, $y_point );
							$pdf->MultiCell(38, 5, U2T($row_loan['account_bank_number']), $border,  1); //เลขบัญชี
							$pdf->SetXY( 82, $y_point );
							$pdf->MultiCell(38, 5, U2T($row_account['account_type']	), $border,  1); //ประเภทบัญชี
							$pdf->SetXY( 138, $y_point );
							$pdf->MultiCell(38, 5, U2T($row_account['account_name']), $border,  1); //ชื่อบัญชี
						}
						
						/*$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 17,173,4,0, '', '', true);
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 17,180,4,0, '', '', true);
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 17,187,4,0, '', '', true);
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 17,194,4,0, '', '', true);
						*/
						$y_point = 188;
						$pdf->SetXY( 120, $y_point );
						$pdf->MultiCell(51, 5, U2T($row_loan['member_name']), $border, 'C'); //ชื่อ นามสกุลผู้กู้	
						
					}
				}

				if($document_link == 'loan_ageement_special.pdf'){				
					if($pageNo == '1'){	

						$y_point = 42.5;
						$pdf->SetXY( 34, $y_point );
						$pdf->MultiCell(65, 5, U2T($row_loan['loan_number']), $border, 1); //เลขที่สัญญา
						
						$y_point = 48;
						$pdf->SetXY( 93, $y_point );
						$pdf->MultiCell(60, 5, U2T($loan_day.'  '.$loan_month.'  '.$loan_year), $border, 1);//วันที่ เดือน พ.ศ.
						
						
						$y_point = 60.5;
						$pdf->SetXY( 25, $y_point );
						$pdf->MultiCell(65, 5, U2T($row_loan['member_name']), $border, 1); //ชื่อ นามสกุลผู้กู้				
						$pdf->SetXY( 85, $y_point );
						$pdf->MultiCell(10, 5, U2T($row_member['member_age']), $border, 'C'); //อายุ		
						$pdf->SetXY( 125, $y_point );
						$pdf->MultiCell(30, 5, U2T($row_loan['card_id']), $border, 'C'); //เลขบัตรประชาชน
						
						$y_point = 87;
						$pdf->SetXY( 144, $y_point );
						$pdf->MultiCell(40, 5, U2T(num_format($row_loan['loan_money'])), $border, 'C');	//จำนวนเงิน	
						
						
						$y_point = 93.5;
						$pdf->SetXY( 35, $y_point );
						$pdf->MultiCell(30, 5, U2T($row_loan['loan_money_text']), $border,  'C'); //จำนวนเงิน ตัวอักษร
						
						
						if($row_loan['guarantee_share_value'] != ''){
							$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 27,121,4,0, '', '', true);
							
							$y_point = 126.5;
							$pdf->SetXY( 10, $y_point );
							$pdf->MultiCell(38, 5, U2T(num_format($row_loan['guarantee_share_value'])), $border,  'C'); //จำนวนหุ้น บาท
							$pdf->SetXY( 70, $y_point );
							$pdf->MultiCell(70, 5, U2T(convert($row_loan['guarantee_share_value'])), $border,  'C'); //จำนวนหุ้น บาท ตัวหนังสือ
						}						
						
						if($row_loan['account_bank_number'] != ''){
							$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 27,153,4,0, '', '', true);
							
							$y_point = 160;
							$pdf->SetXY( 104, $y_point );
							$pdf->MultiCell(38, 5, U2T($row_loan['account_bank_number']), $border,  1); //เลขบัญชี
							$y_point = 166;
							$pdf->SetXY( 31, $y_point );
							$pdf->MultiCell(38, 5, U2T($row_account['account_type']), $border,  1); //ประเภทบัญชี
							$pdf->SetXY( 87, $y_point );
							$pdf->MultiCell(38, 5, U2T($row_account['account_name']), $border,  1); //ชื่อบัญชี
						}
						
					}
				}		
			
			
		}	
	}
	//echo '<pre>'; print_r($arr_doc_use); echo '</pre>';
	//exit;					
	//echo $row_doc['document_link'].'<hr>';
	
	
	/*for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* ,t3.loan_reason as member_use
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_reason t3 ON t2.member_use = t3.id
		WHERE t2.loan_id = '".$_GET['loan_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$member_no = $row_loan['member_id'];
		$sql_profile = "SELECT FLOMMAST.*, FLOPDEPT.DEPT_NAME,FLOPSTATUS.*, FLOPSTATUS.STS_TYPE_DESC
			FROM LOAN.dbo.FLOMMAST FLOMMAST
				INNER JOIN LOAN.dbo.FLOPDEPT FLOPDEPT ON FLOMMAST.MAST_MEMB_DEPT = FLOPDEPT.DEPT_NO
				INNER JOIN LOAN.dbo.FLOPSTATUS FLOPSTATUS ON FLOMMAST.MAST_MEMB_TYPE = FLOPSTATUS.STS_TYPE_ID AND FLOPSTATUS.STS_CODE = '01'
			WHERE FLOMMAST.MAST_MEMB_NO = '{$member_no}'";
		$rs_profile = odbc_exec($odbc_conn, $sql_profile) or die(odbc_errormsg());
		if($row_profile = odbc_fetch_array($rs_profile)) {
			$row_profile = t2u_array($row_profile);
			$bd_date = new DateTime(ConvertToSQLDate($row_profile["MAST_BIRTH_YMD"]));
			$bd_interval = $bd_date->diff(new DateTime("now"));
			$md_date = new DateTime(ConvertToSQLDate($row_profile["MAST_YMD_IN"]));
			$md_interval = $md_date->diff(new DateTime("now"));
			
			$sql_data = "SELECT email, telmobile
						FROM cmp_imp_member_data
						WHERE member_no = '{$member_no}'";
			$rs_data = $mysqli->query($sql_data);
			$row_data = $rs_data->fetch_assoc();
			
			//echo '<pre>'; print_r($row_profile); echo '</pre>';
			$row_member = [
						'member_no' => $row_profile['MAST_MEMB_NO']
					,	'memname' => $row_profile['MAST_FNAME'].' '.$row_profile['MAST_NAME']
					,	'member_age' => $bd_interval->format('%Y')
					//,	'member_age' => $bd_interval->format('%Y ปี %m เดือน')
					,	'memdtm' =>  ConvertToSQLDate($row_profile['MAST_YMD_IN'])
					,	'member_register' =>  ConvertToThaiDate(ConvertToSQLDate($row_profile['MAST_YMD_IN']))
					,	'member_register_age' => $md_interval->format('%Y ปี %m เดือน')
					,	'email' => (!empty($row_data['email']))?$row_data['email']:$row_profile['MAST_EMAIL']
					,	'telmobile' => (!empty($row_data['telmobile']))?$row_data['telmobile']:$row_profile['MAST_MOBILE']
					,	'member_address' => sprintf('%s %s', $row_profile['MAST_ADDR'], $row_profile['MAST_ZIP'])
					,	'member_group' => $row_profile['DEPT_NAME']					// หน่วยงาน
					,	'member_type_id' => $row_profile['STS_TYPE_ID']			// รหัสประเภทสมาชิก
					,	'member_type' => $row_profile['STS_TYPE_DESC']			// ประเภทสมาชิก
					,	'member_bd' => ConvertToThaiDate(ConvertToSQLDate($row_profile['MAST_BIRTH_YMD'])) // วันเกิด
					,	'card_person' => $row_profile['MAST_CARD_ID']			// เลขบัตรประชาชน
					,	'salary_amount' => $row_profile['MAST_SALARY']
					];
					
			 		
		}
		*/
		/*if($document_link == 'loan_petition.pdf'){
			if($pageNo == '1'){
				$y_point = 37.5;
				$pdf->SetXY( 148, $y_point );
				$pdf->MultiCell(65, 5, U2T($row_loan['member_id']), $border, 1); //เลขที่สมาชิก
				
				$y_point = 63;
				$pdf->SetXY( 44, $y_point );
				$pdf->MultiCell(65, 5, U2T($loan_application_type[$row_loan['loan_application_type']]), $border, 1); //ประเภทเงินกู้
				
				
				$application_date = $row_loan["application_date"];
				$loan_day = date('d', strtotime($application_date));
				$loan_month = MonthName((int)date('m', strtotime($application_date)));
				$loan_year = date('Y', strtotime($application_date))+543;
				
				$y_point = 85.5;
				$pdf->SetXY( 123, $y_point );
				$pdf->MultiCell(10, 5, U2T($loan_day), $border, 'C');//วันที่
				$pdf->SetXY( 138, $y_point );
				$pdf->MultiCell(25, 5, U2T($loan_month), $border, 'C');//เดือน
				$pdf->SetXY( 167, $y_point );
				$pdf->MultiCell(15, 5, U2T($loan_year), $border, 'C');//พ.ศ.
				
				$y_point = 96.5;
				$pdf->SetXY( 75, $y_point );
				$pdf->MultiCell(65, 5, U2T($row_loan['member_name']), $border, 1); //ชื่อผู้กู้
				$pdf->SetXY( 127, $y_point );
				$pdf->MultiCell(65, 5, U2T($row_loan['member_name']), $border, 1); //นามสกุลผู้กู้				
				$pdf->SetXY( 184, $y_point );
				$pdf->MultiCell(14, 5, U2T($row_member['member_age']), $border, 'C'); //อายุ
				
				$y_point = 104;
				$pdf->SetXY( 75, $y_point );
				$pdf->MultiCell(65, 5, U2T($row_loan['member_use']), $border, 1); //โดยนำไปใช้เพื่อ (เหตุผลการกู้)
				
				$y_point = 111.5;
				$pdf->SetXY( 29, $y_point );
				$pdf->MultiCell(32, 5, U2T(num_format($row_loan['loan_money'])), $border, 'C');	//จำนวนเงิน			
				$pdf->SetXY( 70, $y_point );
				$pdf->MultiCell(58, 5, U2T($row_loan['loan_money_text']), $border,  'C'); //จำนวนเงิน ตัวอักษร
				
				$pdf->SetFont('THSarabunNew', '', 9 );
				$y_point = 120;
				$pdf->SetXY( 26, $y_point );
				$pdf->MultiCell(45, 5, U2T($row_loan['member_position']), $border, 1);//ตำแหน่งผู้กู้
				
				$y_point = 119.5;
				$pdf->SetFont('THSarabunNew', '', 13 );
				$pdf->SetXY( 68, $y_point );
				$pdf->MultiCell(40, 5, U2T(num_format($row_loan['member_salary'])), $border, 'C');//เงินเดือน
				
				
				//$y_point = 127;
				//$pdf->SetXY( 28, $y_point );
				//$pdf->MultiCell(40, 5, U2T($row_member['member_address']), $border, 1);//ที่อยุ่
				
				$y_point = 127;
				$pdf->SetXY( 28, $y_point );
				$pdf->MultiCell(40, 5, U2T('หมู่บ้าน'), $border, 1);//ห้องเลขที่
				$pdf->SetXY( 49, $y_point );
				$pdf->MultiCell(40, 5, U2T('หมู่บ้าน'), $border, 1);//ชั้นที่
				$pdf->SetXY( 73, $y_point );
				$pdf->MultiCell(40, 5, U2T('หมู่บ้าน'), $border, 1);//หมู่บ้าน
				$pdf->SetXY( 106, $y_point );
				$pdf->MultiCell(15, 5, U2T('เลขที่'), $border, 'C');//เลขที่
				$pdf->SetXY( 125, $y_point );
				$pdf->MultiCell(15, 5, U2T('หมู่ที่'), $border, 'C');//หมู่ที่
				
			}else if($pageNo == '2'){
				$y_point = 29;
				if($row_loan['guarantee_type']=='4'){
					$pdf->SetXY( 41, $y_point );
					$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
				}
				$y_point = 28;
				$pdf->SetXY( 112, $y_point );
				$pdf->MultiCell(46, 5, U2T(num_format($row_loan['guarantee_share_value'])), $border, 'R');
				
				
				
				$y_point = 36;
				if($row_loan['guarantee_type']=='2'){
					$pdf->SetXY( 41, $y_point );
					$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
				}
				$pdf->SetXY( 102, $y_point );
				$pdf->MultiCell(37, 5, U2T($row_loan['account_bank_type']), $border, 1);
				$pdf->SetXY( 147, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_loan['account_bank_number']), $border, 1);
				
				$y_point = 43;
				$pdf->SetXY( 64, $y_point );
				$pdf->MultiCell(46, 5, U2T(num_format($row_loan['deposit_amount'])), $border, 'R');
				
				$y_point = 52;
				if($row_loan['guarantee_type']=='3'){
					$pdf->SetXY( 41, $y_point );
					$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
				}
				$y_point = 51;
				$pdf->SetXY( 64, $y_point );
				$pdf->MultiCell(120, 5, U2T(num_format($row_loan['guarantee_position'])), $border, 'R');
				
				$y_point = 67;
				$pdf->SetXY( 103, $y_point );
				//$pdf->MultiCell(40, 5, U2T($row_loan['daposit_issue']), $border, 'R');
				$pdf->MultiCell(40, 5, U2T(''), $border, 'R');
			}
			
		}
		
		*/
		
	//}
	
	$pdf->Output();