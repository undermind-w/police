<?php
	include "config.inc.php";
	//require PATH."/class/connect_odbc.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}	
	
	foreach($_GET as $key => $value){
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	//echo '<pre>'; print_r($_GET); echo '</pre>'; exit;
	$pdf = new FPDI();	
		
		$loan_application_type = array();
		$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
		$rs_type = $mysqli->query($sql_type);
		while($row_type = $rs_type->fetch_assoc()){			
			$loan_application_type[$row_type['id']] = $row_type['loan_name'];
		}
	
		$sql_loan = "SELECT * ,t3.loan_reason as member_use
						FROM 
							cmp_admin_loan_money t1
							INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
							LEFT JOIN loan_reason t3 ON t2.member_use = t3.id
						WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		//echo $sql_loan.'<hr>';
		
		$member_no = $row_loan['member_id'];
		$application_date = $row_loan["application_date"];
		$loan_id = $row_loan["loan_id"];
		
		$sql = "SELECT
				t1.*,
				CONCAT(
					t2.prename_full,
					t1.firstname_th,
					' ',
					t1.lastname_th
				) AS memname,
				t4.district_name,
				t5.amphur_name,
				t6.province_name,
				t7.mem_group_name
			FROM
				coop_mem_apply AS t1
			LEFT JOIN coop_prename AS t2 ON t1.prename_id = t2.prename_id
			LEFT JOIN coop_district as t4 ON t1.c_district_id = t4.district_id
			LEFT JOIN coop_amphur as t5 ON t1.c_amphur_id = t5.amphur_id
			LEFT JOIN coop_province as t6 ON t1.c_province_id = t6.province_id
			LEFT JOIN coop_mem_group as t7 ON t1.level = t7.id
			WHERE
				t1.member_id = '{$member_no}'";					
		$rs = $mysqli_app->query($sql);
		$row_member = $rs->fetch_assoc();	
		
		//ข้อมูลผู้ค้ำประกัน
		//marry_status 1=โสด,2= สมรส
		$sql_bondsman = "SELECT 
							t1.bondsman_num,
							t1.loan_application_id, 
							t1.bondsman_id,
							t1.bondsman_tel,
							t1.bondsman_number,
							t1.bondsman_name
						FROM 
							loan_application_bondsman as t1
						WHERE 
							t1.loan_id = '".$loan_id."'
							AND t1.bondsman_number != ''
						ORDER BY t1.bondsman_num";	
		$rs_bondsman = $mysqli->query($sql_bondsman);
		$check_bondsman = 0;
		$arr_guarantee_person = array();
		while($row_bondsman = $rs_bondsman->fetch_assoc()){	
			$sql_guarantee = "SELECT
						t1.*,
						CONCAT(
							t2.prename_full,
							t1.firstname_th,
							' ',
							t1.lastname_th
						) AS memname,
						t4.district_name,
						t5.amphur_name,
						t6.province_name,
						t7.mem_group_name
					FROM
						coop_mem_apply AS t1
					LEFT JOIN coop_prename AS t2 ON t1.prename_id = t2.prename_id
					LEFT JOIN coop_district as t4 ON t1.c_district_id = t4.district_id
					LEFT JOIN coop_amphur as t5 ON t1.c_amphur_id = t5.amphur_id
					LEFT JOIN coop_province as t6 ON t1.c_province_id = t6.province_id
					LEFT JOIN coop_mem_group as t7 ON t1.level = t7.id
					WHERE
						t1.member_id = '{$row_bondsman['bondsman_number']}'";		
				//echo $sql.'<hr>';			
				$rs_guarantee = $mysqli_app->query($sql_guarantee);
				$row_guarantee = $rs_guarantee->fetch_assoc();
				$arr_guarantee_person[$check_bondsman] = $row_guarantee;
				$check_bondsman++;
		}

		$document_link = 'normal_guarantee_person_pdf.pdf';
		$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/".$document_link;
		$pageCount = $pdf->setSourceFile($filename);
		
		for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
			$pdf->AddPage();
			$tplIdx = $pdf->importPage($pageNo); 
			$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
			
			$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
			$pdf->SetFont('THSarabunNew', '', 11 );
			
			$border = 0;
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetAutoPageBreak(true,0);
			
			$pay_type = array('cash'=>'เงินสด', 'cheque'=>'เช็คธนาคาร', 'transfer'=>'เงินโอน');
			if($pageNo == '1'){
				$y_point = 77;
				$pdf->SetXY( 73, $y_point );
				$pdf->MultiCell(65, 5, U2T($row_member['memname']), $border, 1);
				$pdf->SetXY( 160, $y_point );
				$pdf->MultiCell(30, 5, U2T($row_member['member_id']), $border, 'C');

				$y_point = 84;
				$pdf->SetXY( 22, $y_point );
				$pdf->MultiCell(40, 5, U2T($row_member['mem_group_name']), $border, 1);
				
				$pdf->SetXY( 86, $y_point );
				$pdf->MultiCell(29, 5, U2T(num_format($row_loan['loan_money'])), $border, 'R');
				$pdf->SetXY( 122, $y_point );
				$pdf->MultiCell(70, 5, U2T($row_loan['loan_money_text']), $border, 'C');
				
				$y_point = 91;
				$pdf->SetXY( 65, $y_point );
				$pdf->MultiCell(30, 5, U2T($row_loan['loan_number']), $border, 'C');//เลขที่สัญญา
				$pdf->SetXY( 102, $y_point );
				$pdf->MultiCell(40, 5, U2T(mysqldate2thaidate($application_date,0,0)), $border, 'C');//วันที่

			}else if($pageNo == '2'){
				$y_plus = 0;
				foreach($arr_guarantee_person AS $key=>$row_guarantee_person){
					$y_point = 26+$y_plus;
					$pdf->SetXY( 30, $y_point );
					$pdf->MultiCell(50, 5, U2T($row_guarantee_person['memname']), $border, 1);
					$pdf->SetXY( 88, $y_point );
					$pdf->MultiCell(13, 5, U2T(cal_age($row_guarantee_person['birthday'])), $border, 'C');
					$pdf->SetXY( 120, $y_point );
					$pdf->MultiCell(25, 5, U2T($row_guarantee_person['member_id']), $border, 'C');
					
					$y_point = 32+$y_plus;
					$pdf->SetXY( 45, $y_point );
					$pdf->MultiCell(30, 5, U2T($row_guarantee_person['id_card']), $border, 1);	
					$pdf->SetXY( 86, $y_point );
					$pdf->MultiCell(24, 5, U2T($row_guarantee_person['position']), $border, 1);
					$pdf->SetXY( 116, $y_point );
					$pdf->MultiCell(27, 5, U2T($row_guarantee_person['mem_group_name']), $border, 1);
					
					//ประเภทสมาชิก
					if($row_guarantee_person['mem_type_id'] == '1'){
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 16,39+$y_plus,4,0, '', '', true);
					}else if($row_guarantee_person['mem_type_id'] == '9'){
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 39,39+$y_plus,4,0, '', '', true);
					}else if($row_guarantee_person['mem_type_id'] == '3'){
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 61,39+$y_plus,4,0, '', '', true);
					}else if($row_guarantee_person['mem_type_id'] == '4'){
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 82,39+$y_plus,4,0, '', '', true);
					}else{
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 100,39+$y_plus,4,0, '', '', true);
					}
					
					$y_point = 44+$y_plus;
					$pdf->SetXY( 43, $y_point );
					$pdf->MultiCell(25, 5, U2T(num_format($row_guarantee_person['salary'])), $border, 'R');
					
					$y_point = 51+$y_plus;
					$pdf->SetXY( 27, $y_point );
					$pdf->MultiCell(15, 5, U2T($row_guarantee_person['c_address_no']), $border, 1);				
					$pdf->SetXY( 49, $y_point );
					$pdf->MultiCell(13, 5, U2T($row_guarantee_person['c_address_moo']), $border, 1);
					$pdf->SetXY( 68, $y_point );
					$pdf->MultiCell(22, 5, U2T($row_guarantee_person['district_name']), $border, 1);
					$pdf->SetXY(98, $y_point );
					$pdf->MultiCell(22, 5, U2T($row_guarantee_person['amphur_name']), $border, 1);
					$pdf->SetXY( 128, $y_point );
					$pdf->MultiCell(20, 5, U2T($row_guarantee_person['province_name']), $border, 1);
					
					$y_point = 57+$y_plus;
					$pdf->SetXY( 33, $y_point );
					$pdf->MultiCell(14, 5, U2T($row_guarantee_person['c_zipcode']), $border, 'C');
					$pdf->SetXY( 66, $y_point );
					$pdf->MultiCell(30, 5, U2T($row_guarantee_person['mobile']), $border, 1);
					$pdf->SetXY( 108, $y_point );
					$pdf->MultiCell(37, 5, U2T($row_guarantee_person['email']), $border, 1);							
					
					$y_point = 33+$y_plus;
					$pdf->SetXY( 153, $y_point );
					$pdf->MultiCell(43, 5, U2T($row_guarantee_person['memname']), $border, 'C');
					
					//marry_status 1=โสด,2= สมรส
					if($row_guarantee_person['marry_status'] == '1'){
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 150,40+$y_plus,4,0, '', '', true);
					}
					
					if($row_guarantee_person['marry_status'] == '2'){
						$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 161,40+$y_plus,4,0, '', '', true);
					}
					//$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 174,40+$y_plus,4,0, '', '', true);
					//$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/check_ok.png", 185,40+$y_plus,4,0, '', '', true);
					$y_plus += 45.5;
				}
			}else if($pageNo == '3'){
				$y_plus = 0;
				foreach($arr_guarantee_person AS $key=>$row_guarantee_person){
					if($key%2 == 0){
						$y_point = 220+$y_plus;
						$pdf->SetXY( 33, $y_point );
						$pdf->MultiCell(47, 5, U2T($row_guarantee_person['memname']), $border, 'C');
						$y_plus += 24;
					}else{
						$y_plus -= 24;
						$y_point = 220+$y_plus;
						$pdf->SetXY( 122, $y_point );
						$pdf->MultiCell(47, 5, U2T($row_guarantee_person['memname']), $border, 'C');
						$y_plus += 24;						
					}
					
				}
				
			}
		}	
		
	$pdf->Output();