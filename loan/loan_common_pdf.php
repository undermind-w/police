<?php
	require_once('mpdf/mpdf.php'); //ที่อยู่ของไฟล์ mpdf.php ในเครื่องเรานะครับ
	ob_start(); // ทำการเก็บค่า html นะครับ
	include "config.inc.php";
	$smarty->assign("title", "คำขอกู้เงินสามัญ" );
?>

<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />

<style type="text/css">
	body{
		/*margin-bottom: 3em !important;*/
	}
	.navbar{
		display: none;
	}
	.add-on-container{
		display: none;
	}
	.footer{
		display: none;
	}
	.border1 {
    border: solid 1px #ccc;
    padding: 0 15px;
}
</style>

<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scrip_include", $scrip_include);
	ob_start();
?>
<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	ob_start();
?>
<div class="row">
	
<?php 
	$sql = "SELECT * FROM loan_application WHERE loan_id = '{$_GET['loan'] }' AND loan_application_id =  '{$_GET['application']}' ";
	$rs = $mysqli->query($sql);
    echo $mysqli->error;
    $row = $rs->fetch_assoc();
	
    $sql_province = "SELECT PROVINCE_NAME FROM province WHERE PROVINCE_ID = '{$row['province_id']}' ";
	$rs_province = $mysqli->query($sql_province);
	$row_province = $rs_province->fetch_assoc();
	$sql_amphur = "SELECT AMPHUR_NAME FROM amphur WHERE AMPHUR_ID = '{$row['amphur_id']}' ";
	$rs_amphur = $mysqli->query($sql_amphur);
	$row_amphur = $rs_amphur->fetch_assoc();

	$sql_district = "SELECT DISTRICT_NAME FROM district WHERE DISTRICT_ID = '{$row['district_id']}' ";
	$rs_district = $mysqli->query($sql_district);
	$row_district = $rs_district->fetch_assoc();

	$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_id = '{$_GET['loan'] }' AND loan_application_id =  '{$_GET['application']}' ORDER BY bondsman_id ASC ";
	$rs_bondsman = $mysqli->query($sql_bondsman);
	$count = $rs_bondsman->num_rows;
 ?>

<div >
						 <table style="width: 100%;border-collapse: collapse;">
						  <thead>
						   <tr> 
						    <th  class="text-center">
	<img style="margin:0; vertical-align: middle;" width="100" height="100" src="/timthumb.php?a=c&zc=2&w=170&h=140&src=/images/logo.png" >
						    </th>
						   </tr> 
						  </thead> 
						    <tbody> 
						    </tbody>
						    </table>

<h3 class="text-center" style="text-align: center;">คำขอกู้เงินสามัญ</h3>

						<div style="text-align: right;margin-top: 0.5em;margin-right: 1em;width:100%;"> เขียนที่ <?=$row['write_at']?> </div>
							<div style="text-align: right;margin-top: 0.5em;margin-right: 1em;width:100%;">
								<?php
									if ($row['application_date']) {
									$time = strtotime($row['application_date']);
									$myFormatForView = date("d/m/Y", $time);
									}else{
									$time = strtotime(date("d-m-y"));
									$myFormatForView = date("d/m/Y", $time);
									}
								?>
								วันที่ <?=$myFormatForView ?>
							</div>
							
						<div>
							<p >เรียน คณะกรรมการดำเนินการ<?php echo COOP_NAME_TH;?></p>
						</div>
							
							<div class="row show-grid">

							 <div class="col-md-4" style="width: 100%;float: left;">
							 	<div style="width: 7%; float: left;">
							 		 ข้าพเจ้า 
							 	</div>
							 	<div  style="width: 32%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_name']?>
							 	</div>
							 	<div style="width: 11%; float: left;">
							 		 สมาชิกเลขที่ 
							 	</div>
							 	<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_id']?>
							 	</div>
							 	<div style="width: 8%; float: left;">
							 		 ตำแหน่ง 
							 	</div>
							 	<div  style="width: 27%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_position']?>
							 	</div>

							 </div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
								<div style="width: 5%; float: left;">
									ที่อยู่
								</div>
								<div  style="width: 95%; float: left;border-bottom: 1px solid #ddd;text-align: left;">
										<?=$row['address']?>
								</div>
							 </div>
							</div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
							 	<div  style="width: 16%; float: left;;text-align: center;">
							 		เลขที่บัตรประชาชน
							   </div>
							   <div  style="width: 25%; float: left;border-bottom: 1px solid #ddd;text-align: center;margin-left:1%;">
							 		<?=$row['card_id']?>
							 	</div>
							 	<!-- <div style="width: 5%; float: left;">
							 		รหัส
							 	</div>
							 	<div  style="width: 13%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_code']?>
							 	</div> -->
							 	<div style="width: 8%; float: left;">
							 		สังกัด
							 	</div>
								 <div style="width: 20%; float: left;">
							 		<?= ($row['member_branch'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 กลุ่มงาน 

									 <?= ($row['member_branch'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 สาขา 
							 	</div>
								<div  style="width: 30%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		 <?= ($row['member_branch_name'] == "") ? "-" : $row['member_branch_name'] ; ?>
							 	</div>
							 </div>

							</div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
								 <div style="width: 15%; float: left;">
							 		<?= ($row['member_sector'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 ส่วน 

									 <?= ($row['member_sector'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 เขต 
							 	</div>
								<div  style="width: 22%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		 <?= ($row['member_sector_name'] == "") ? "-" : $row['member_sector_name'] ; ?>
							 	</div>

								 <div style="width: 15%; float: left;">
							 		<?= ($row['member_department'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 ฝ่าย 

									 <?= ($row['member_department'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 ภาค 
							 	</div>
								<div  style="width: 22%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		 <?= ($row['member_department_name'] == "") ? "-" : $row['member_department_name'] ; ?>
							 	</div>

							 	<div style="width: 6%; float: left;text-align: center;">
							 		โทร
							 	</div>
								<div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_tel']?>
							 	</div>
							 </div>
							</div>


 						<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div class="col-md-6" style="width: 100%;float: left;">
							 	<div  style="width: 20%; float: left;">
							 		 ที่อยู่ตามบัตรประชาชน
							 	</div>
							 	<div style="width: 80%;float: left;border-bottom: 1px solid #ddd;text-align:left;">
							 		<?=$row['address_card_id']?>
							 	</div>
							 </div>
							</div>


						   <div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
							 	<div style="width: 12%; float: left;">
							 		ได้รับเงินเดือน
							 	</div>
							 	<div  style="width: 26%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['member_salary'],2)?>
							 	</div>

								<div style="width: 12%; float: left;">
							 		บาท
							 	</div>
							   <div  style="width: 45%; float: left;text-align: left;">
							 		ขอเสนอคำขอกู้สามัญเพื่อโปรดพิจารณาดังต่อไปนี้
							  </div>
							 </div>
							</div>

							<div class="row show-grid" style="margin-top:3.5%;display: block;">

							 <div  style="width: 100%;float: left;">
							 	<div style="width: 40%; float: left;text-indent: 1.5em;">
							 		ข้อ 1. ข้าพเจ้าขอกู้เงินของสหกรณ์ฯ จำนวน
							 	</div>

							 	<div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['loan_money'],2)?>
							 	</div>

								<div style="width: 10%; float: left;">
							 		บาท
							 	</div>

							   <div  style="width: 23%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?="(".$row['loan_money_text'].")"?>
							  </div>

							 </div>
							</div>

							<div class="row show-grid" style="margin-top:2%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;">
										โดยจะนำไปใช้เพื่อการดังต่อไปนี้ ( ชี้แจงความมุ่งหมายและเหตุผลของการกู้โดยละเอียด )
									</div>
								</div>
							</div>

							<div class="row show-grid" style="margin-top: 1.5%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;border-bottom: 1px solid #ddd;">
										<?=$row['member_use']?>
									</div>
								</div>
							</div>

							<div class="row show-grid" style="margin-top: 3.5%;">

							 <div  style="width: 100%;float: left;">
							 	<div style="width: 45%; float: left;text-indent: 1.5em;">
							 		ข้อ 2. ในเวลานี้ ข้าพเจ้ามีหุ้นอยู่ในสหกรณ์ รวม
							 	</div>

							 	<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_share']?>
							 	</div>

								<div style="width: 6%; float: left;">
							 		หุ้น
							 	</div>

								<div  style="width: 8%; float: left;text-align: center;">
							 		เป็นเงิน
							   </div>

							   <div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['member_money'],2)?>
							  </div>

							  	<div style="width: 6%; float: left;">
							 		บาท
							 	</div>

							 </div>
							</div>

							
							<div class="row show-grid" style="margin-top: 2%;">

							 <div  style="width: 100%;float: left;">
							 	<div style="width: 45%; float: left;">
							 		และข้าพเจ้าส่งค่าหุ้นรายเดือนอยู่ในอัตรา เดือนละ
							 	</div>

							 	<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['share_rate'],2)?>
							 	</div>

								<div style="width: 6%; float: left;">
							 		บาท
							 	</div>
							 </div>
							</div>


							<div class="row show-grid" style="margin-top: 3.5%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;text-indent: 1.5em;">
										ข้อ 3. ข้าพเจ้ามีหนี้สินอยู่ต่อสหกรณ์ในฐานะผู้กู้ ดังต่อไปนี้
									</div>
								</div>
							</div>

							<div class="row show-grid" style="margin-top: 2%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 20%; float: left;text-indent: 3em;">
							 				3.1. หนังสือกู้ที่
									</div>
									<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
										<?=$row['book_loan']?>
									</div>
									<div style="width: 5%; float: left;text-align:center;">
										/
									</div>
									<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
										<?=$row['book_two']?>
									</div>
									<div style="width: 10%; float: left;text-indent: 1.5em;">
							 				วันที่
									</div>
									<div  style="width: 23%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
										<?php
											if ($row['loan_date']) {
											$time = strtotime($row['loan_date']);
											$myFormatForView = date("d/m/Y", $time);
											}else{
											$time = strtotime(date("d-m-y"));
											$myFormatForView = date("d/m/Y", $time);
											}
										?>
									     <?=$myFormatForView ?>
								    </div>
								</div>
							</div>

							
							<div class="row show-grid" style="margin-top: 2%;">
							 <div  style="width: 100%;float: left;">
							 	<div style="width: 15%; float: left;text-indent: 5em;">
							 		เพื่อ
							 	</div>

							 	<div  style="width: 30%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['loan_for']?>
							 	</div>

								<div  style="width: 14%; float: left;text-align: center;">
							 		ต้นเงินคงเหลือ
							   </div>

							   <div  style="width: 25%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['loan_remain'],2)?>
							  </div>

							  	<div style="width: 6%; float: left;">
							 		บาท
							 	</div>
							 </div>
							</div>

					 <?php if ($count < 5) { ?>
	
							<div class="row show-grid" style="display: block;margin-top: 11em;">
							<div  style="width: 100%;float: left;">

							<div style="width: 100%;text-align: right;margin-top: -2.8em;">
							<!-- <div style="text-align: right;margin-top: -2.8em;page-break-after: always"> -->
							(ลงชื่อ)...................................ผู้ขอกู้
							</div>

							</div>
							</div>

					   <div class="row show-grid" style="display: block;margin-top: 2em;">
						<div  style="width: 100%;float: left;">

						<div style="width: 100%;text-align: right;margin-top: -2.8em;">
						(&nbsp;&nbsp;&nbsp;<?=$row['member_name']?>&nbsp;&nbsp;&nbsp;)
						</div>

						</div>
						</div>

					   <div class="row show-grid" style="display: block;margin-top: 1.9em;">
						<div  style="width: 100%;float: left;">

						<div style="width: 100%;text-align: right;margin-top: -2.8em;page-break-after: always">
						/ข้อ 4. นอกจากค่าหุ้น ..............
						</div>

						</div>
						</div>

						<?php } ?>

							<div class="row show-grid" style="margin-top: 2%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;text-indent: 1.5em;">
										ข้อ 4. นอกจากค่าหุ้น ซึ่งข้าพเจ้ามีอยู่ในสหกรณ์ ข้าพเจ้าขอเสนอหลักประกัน ดังต่อไปนี้ คือ	
									</div>
								</div>
							</div>
						
						<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;text-indent: 3em;">
							   <?= ($row['insurance_type'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
								4.1. สมาชิกสหกรณ์
							 </div>
						</div>

						<?php  if ($count == 0) { ?>

							<div style="border: solid 1px #000; padding: 15px;text-align: center;">
							 	<p>ไม่ได้กรอกข้อมูลผู้ค้ำประกัน</p>
							<div>

						<?php }else{ 
	
						$i = 1;
                        while($row_bondsman = $rs_bondsman->fetch_assoc()){ ?>

							 <div style="border: solid 1px #000; padding: 15px;margin-top: 5px;">
					
						      <div class="row show-grid">
							
							 <div class="col-md-4" style="width: 100%;float: left;">
							 	<div style="width: 5%; float: left;">
							 		<?=$i?>. ชื่อ 
							 	</div>
							 	<div  style="width: 30%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?= ($row_bondsman['bondsman_name'] == "") ? "-" : $row_bondsman['bondsman_name'] ; ?>
							 	</div>
							 	<div style="width: 20%; float: left;">
							 		สมาชิกเลขทะเบียนที่
							 	</div>
							 	<div  style="width: 12%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?= ($row_bondsman['bondsman_number'] == "") ? "-" : $row_bondsman['bondsman_number'] ; ?>
							 	</div>
							 	<div style="width: 18%; float: left;">
							 		 พนักงานตำแหน่ง 
							 	</div>
							 	<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?= ($row_bondsman['bondsman_position'] == "") ? "-" : $row_bondsman['bondsman_position'] ; ?>
							 	</div>

							 </div>

							</div>

							 <div class="row show-grid" style="margin-top: 10px;">
	 
							 <div class="col-md-4" style="width: 100%;float: left;">
							 	<div style="width: 5%; float: left;">
							 		สังกัด
							 	</div>
							 	<div  style="width: 22%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?= ($row_bondsman['bondsman_affiliation'] == "") ? "-" : $row_bondsman['bondsman_affiliation'] ; ?>
							 	</div>
							 	<div style="width: 14%; float: left;">
							 		 เงินได้รายเดือน 
							 	</div>
							 	<div  style="width: 13%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?= ($row_bondsman['bondsman_salary'] == "") ? "-" : $row_bondsman['bondsman_salary'] ; ?>
							 	</div>
								 <div style="width: 8%; float: left;">
							 		 บาท 
							 	</div>
								 <div style="width: 18%; float: left;">
							 		 เบอร์โทรผู้ค้ำประกัน 
							 	</div>
							 	<div  style="width: 18%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?= ($row_bondsman['bondsman_tel'] == "") ? "-" : $row_bondsman['bondsman_tel'] ; ?>
							 	</div>
							 </div>

							 <div class="row show-grid" style="margin-top: 10px;">
	 
							 <div class="col-md-4" style="width: 100%;float: left;">
							 <div style="width: 50%; float: left;">
							 		 เป็นผู้ค้ำประกันรายอื่นตามหนังสือ ค้ำประกันที่และชื่อผู้กู้ 
							 	</div>
							 </div>
							 </div>	

							</div>
							</div>

							 <?php $i++; } ?>
							<?php } ?>

							 </div>
							 </div>
							
							<div class="row show-grid" style="margin-top: 10px;display: block;">
								<div  style="width: 40%;float: left;text-indent: 3em;">
								<?= ($row['insurance_type'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									4.2. ทุนเรือนหุ้นที่มีอยู่ในสหกรณ์  
								</div>
								<div  style="width: 14%; float: left;text-align: center;">
							 		จำนวนเงิน
							   </div>
								<div  style="width: 25%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
										<?=($row['share_money'] == "") ? "-" : number_format($row['share_money'],2) ;?>
								</div>
							  	<div style="width: 6%; float: left;">
							 		บาท
							 	</div>
							</div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
								<div  style="width: 40%;float: left;text-indent: 3em;">
								<?= ($row['insurance_type'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									4.3. สมุดเงินฝากของสหกรณ์    
								</div>
								<div  style="width: 12%; float: left;text-align: center;">
							 		ประเภท
							   </div>
								<div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
										<?=($row['deposit_type'] == "") ? "-" : $row['deposit_type']?>
								</div>
							  	<div  style="width: 5%; float: left;text-align: center;">
							 		เลขที่
							   </div>
								<div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
									   <?=($row['deposit_no'] == "") ? "-" : $row['deposit_no']?>
								</div>
								<div  style="width: 14%; float: left;text-align: center;margin-left:5em;margin-top:1%;">
							 		จำนวนเงิน
							   </div>

							   <div  style="width: 25%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 	   <?=($row['deposit_amount'] == "") ? "-" : $row['deposit_amount']?>
							  </div>

							  	<div style="width: 6%; float: left;">
							 		บาท
							 	</div>
							</div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							
							 <div  style="width: 100%;float: left;">
							 	<div style="width: 100%; float: left;text-indent: 1.5em;">
							 		ข้าพเจ้าขอเสนอทุนเรือนหุ้นหรือสมุดเงินฝากสหกรณ์ ที่จะจำนำเป็นประกันซึ่งข้าพเจ้า
									ได้รับรองว่าเป็นสำเนาอันถูกต้อง มาพร้อมคำกู้นี้ รวม <span style="border-bottom: 1px solid #ddd;margin-left:5px;margin-right:5px;"> <?=$row['daposit_issue']?> </span> ฉบับ
							 	</div>
							</div>

							</div>

							<div class="row show-grid" style="margin-top: 3.5%;">

							 <div  style="width: 100%;float: left;">
							 	<div style="width: 64%; float: left;text-indent: 1.5em;">
							 		ข้อ 5. ถ้าข้าพเจ้าได้รับเงินกู้ข้าพเจ้าขอส่งต้นเงินกู้เป็นงวดรายเดือน  งวดละ
							 	</div>

							 	<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['period'],2)?>
							 	</div>

								<div style="width: 6%; float: left;">
							 		บาท
							 	</div>

								<div  style="width: 25%; float: left;text-align: center;margin-top: 1%;">
							 		(พร้อมดอกเบี้ยอัตราร้อยละ
							   </div>

							   <div  style="width: 5%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['interest_rate']?>
							  </div>

							  	<div style="width: 45%; float: left;">
							 		ต่อปี หรือตามประกาศอัตราดอกเบี้ยเงินกู้ของสหกรณ์)
							 	</div>

								<div  style="width: 10%; float: left;text-align: left;">
							 		เป็นจำนวน
							   </div>
							   <div  style="width: 7%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['interest_period']?>
							 	</div>
								<div  style="width: 5%; float: left;text-align: center;">
							 		งวด
							   </div>

								<div style="width: 45%; float: left;">
							 			ตั้งแต่เดือนถัดจากเดือนที่ สหกรณ์ จ่ายเงินกู้ให้
							 	</div>


							 </div>
							</div>

							
							<div class="row show-grid" style="margin-top: 2%;">

							 <div  style="width: 100%;float: left;">
							 	<div style="width: 100%; float: left;text-indent: 1.5em;">
							 		ข้อ 6.ในการรับเงินกู้ ข้าพเจ้าจะได้ทำหนังสือกู้สำหรับเงินกู้สามัญให้ไว้ต่อสหกรณ์ ตามแบบที่สหกรณ์กำหนด โดยโอนเงินเข้าบัญชีเงินเดือน เลขที่ <span style="border-bottom: 1px solid #ddd;margin-left:5px;margin-right:5px;"> <?=$row['account_bank']?> </span> 
							 	</div>
							 </div>
							</div>

						    <?php if ($count < 5) { ?>
	
							<div class="row show-grid" style="display: block;margin-top: 11em;">
							<div  style="width: 100%;float: left;">

							<div style="width: 100%;text-align: right;margin-top: -2.8em;">
							<!-- <div style="text-align: right;margin-top: -2.8em;page-break-after: always"> -->
							(ลงชื่อ)...................................ผู้ขอกู้
							</div>

							</div>
							</div>

					   <div class="row show-grid" style="display: block;margin-top: 2em;">
						<div  style="width: 100%;float: left;">

						<div style="width: 100%;text-align: right;margin-top: -2.8em;">
						(&nbsp;&nbsp;&nbsp;<?=$row['member_name']?>&nbsp;&nbsp;&nbsp;)
						</div>

						</div>
						</div>

						<?php } ?>



</div>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="scripts/jqueryui/jquery.ui.datepicker-th.js"></script>
    <script src="scripts/jqueryui/jquery-ui-1.8.10.offset.datepicker.min.js"></script>

<?php
$html = ob_get_contents();        //เก็บค่า html ไว้ใน $html 
// ob_end_clean();
$pdf = new mPDF('th', 'A4', '0', '');   //การตั้งค่ากระดาษถ้าต้องการแนวตั้ง ก็ A4 เฉยๆครับ ถ้าต้องการแนวนอนเท่ากับ A4-L
$pdf->SetAutoFont();
$pdf->SetDisplayMode('fullpage');
$pdf->WriteHTML($html, 2);
$pdf->Output("MyPDF/PDF_".$_GET['loan']."_".$_GET['application'].".pdf","F");         // เก็บไฟล์ html ที่แปลงแล้วไว้ใน MyPDF/MyPDF.pdf ถ้าต้องการให้แสด
?>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
