<?php
	require_once('mpdf/mpdf.php'); //ที่อยู่ของไฟล์ mpdf.php ในเครื่องเรานะครับ
	ob_start(); // ทำการเก็บค่า html นะครับ
	include "config.inc.php";
	$smarty->assign("title", "คำขอกู้เงินสามัญ" );
?>

<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />

<style type="text/css">
	body{
		/*margin-bottom: 3em !important;*/
	}
	.navbar{
		display: none;
	}
	.add-on-container{
		display: none;
	}
	.footer{
		display: none;
	}
	.border1 {
    border: solid 1px #ccc;
    padding: 0 15px;
}
</style>

<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scrip_include", $scrip_include);
	ob_start();
?>
<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	ob_start();
?>
<div class="row">
	
<?php 
	$sql = "SELECT * FROM loan_application WHERE loan_id = '{$_GET['loan'] }' AND loan_application_id =  '{$_GET['application']}' ";
	$rs = $mysqli->query($sql);
    $row = $rs->fetch_assoc();

    $sql_province = "SELECT PROVINCE_NAME FROM province WHERE PROVINCE_ID = '{$row['province_id']}' ";
	$rs_province = $mysqli->query($sql_province);
	$row_province = $rs_province->fetch_assoc();

	$sql_amphur = "SELECT AMPHUR_NAME FROM amphur WHERE AMPHUR_ID = '{$row['amphur_id']}' ";
	$rs_amphur = $mysqli->query($sql_amphur);
	$row_amphur = $rs_amphur->fetch_assoc();

	$sql_district = "SELECT DISTRICT_NAME FROM district WHERE DISTRICT_ID = '{$row['district_id']}' ";
	$rs_district = $mysqli->query($sql_district);
	$row_district = $rs_district->fetch_assoc();

	$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_id = '{$_GET['loan'] }' AND loan_application_id =  '{$_GET['application']}' ORDER BY bondsman_id ASC ";
	$rs_bondsman = $mysqli->query($sql_bondsman);
	$count = $rs_bondsman->num_rows;
 ?>

<div >
	<div class="row show-grid" style="display: block;">
							 <!-- <div  style="width:100%;float: left;">
							 	<div style="width: 35%; float: left;">
							 		<?= ($row['money_type'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									  ขอรับเป็นเงินสด 
								<br>
									<div style="margin-top: 5px;">
									<?= ($row['money_type'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									  ขอโอนเข้าบัญชี 
									 </div>
							 	</div>
							</div>
							</div> -->

							<!-- <div style="text-align: right;margin-top: -2.8em;margin-right: 1em;width:100%;"> สัญญาเลขที่ <?=$row['promise_no']?></div>
							<div style="text-align: right;margin-top: -1.5em;margin-right: 1em;width:100%;">
								<?php
									if ($row['promise_date']) {
									$time = strtotime($row['promise_date']);
									$myFormatForView = date("d/m/Y", $time);
									}else{
									$time = strtotime(date("d-m-y"));
									$myFormatForView = date("d/m/Y", $time);
									}
								?>
								วันที่ <?=$myFormatForView ?>
							</div>

							<div class="row show-grid" style="display: block;width:45%;float: right;margin-top:3%;">
								<div style="width: 50%; float: right; padding:2% 0% 1% 2%;border: 1px solid #a1a1a1;">
							 		<span style="width: 100%;border-bottom: 1px solid #ddd;">  &nbsp;กู้ ฉ. <?=$row['loan_a']?> </span> <br>
									<span style="width: 100%;border-bottom: 1px solid #ddd;">  ส่งหักงวดละ <?=$row['loan_period']?> </span> <br>
									<span style="width: 100%;border-bottom: 1px solid #ddd;">  กู้ พ.  <?=$row['loan_b']?> </span><br>
									<span style="width: 100%;border-bottom: 1px solid #ddd;">  กู้ ส.  <?=$row['loan_c']?> </span><br>
									<span style="width: 100%;border-bottom: 1px solid #ddd;">  กู้ สห. <?=$row['loan_d']?> </span><br>
									<span style="width: 100%;border-bottom: 1px solid #ddd;">  กู้ สก. <?=$row['loan_e']?> </span><br>
							 	</div>
							</div> -->

						 <table style="width: 100%;border-collapse: collapse;">
						  <thead>
						   <tr>
						   <!-- <th style="width:43%;"></th> -->
						    <th  class="text-center">
								<img style="margin:0; vertical-align: middle;" width="100" height="100" src="/timthumb.php?a=c&zc=2&w=170&h=140&src=/images/logo.png" >
						    </th>
						   </tr> 
						  </thead> 
						    <tbody> 
						    </tbody>
						    </table>

							<div style="text-align: center;margin-top: 1em;margin-right: 1em;width:100%;"> <h3> คำขอและหนังสือกู้เงินเพื่อเหตุฉุกเฉิน 12 เดือน </h3></div>

							
							<div  style="width: 100%;float: left;">

							 	<div style="width: 20%; float: left;">

							 		<?= ($row['money_type'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									  ขอรับเป็นเงินสด 
								<br>
									<div style="margin-top: 5px;">
									<?= ($row['money_type'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									  ขอโอนเข้าบัญชี 
									 </div>
							</div>
								 

							<div style="text-align: right;margin-top: -2.8em;margin-right: 1em;width:100%;"> เขียนที่ <?=$row['write_at']?> </div>
							<div style="text-align: right;margin-top: -1.0em;margin-right: 1em;width:100%;">
								<?php
									if ($row['application_date']) {
									$time = strtotime($row['application_date']);
									$myFormatForView = date("d/m/Y", $time);
									}else{
									$time = strtotime(date("d-m-y"));
									$myFormatForView = date("d/m/Y", $time);
									}
								?>
								วันที่ <?=$myFormatForView ?>
							</div>

				
						<div class="row show-grid" style="margin-top: 1%;display: block;">
						    <div  style="width: 100%;">
								<div style="width: 100%;text-align: left;">
									เรียน คณะกรรมการดำเนินการ<?php echo COOP_NAME_TH;?>
								</div>
							</div>
						</div>

						<div class="row show-grid" style="margin-top: 2%;">
							 <div class="col-md-4" style="width: 100%;float: left;">
							 	<div style="width: 7%; float: left;">
							 		 ข้าพเจ้า 
							 	</div>
							 	<div  style="width: 32%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_name']?>
							 	</div>
							 	<div style="width: 11%; float: left;">
							 		 สมาชิกเลขที่ 
							 	</div>
							 	<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_id']?>
							 	</div>
							 	<div style="width: 8%; float: left;">
							 		 ตำแหน่ง 
							 	</div>
							 	<div  style="width: 27%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_position']?>
							 	</div>

							 </div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
								<div style="width: 5%; float: left;">
									ที่อยู่
								</div>
								<div  style="width: 95%; float: left;border-bottom: 1px solid #ddd;text-align: left;">
										<?=$row['address']?>
								</div>
							 </div>
							</div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
							 	<div  style="width: 16%; float: left;;text-align: center;">
							 		เลขที่บัตรประชาชน
							   </div>
							   <div  style="width: 25%; float: left;border-bottom: 1px solid #ddd;text-align: center;margin-left:1%;">
							 		<?=$row['card_id']?>
							 	</div>
							 	<!-- <div style="width: 5%; float: left;">
							 		รหัส
							 	</div>
							 	<div  style="width: 13%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_code']?>
							 	</div> -->
							 	<div style="width: 8%; float: left;">
							 		สังกัด
							 	</div>
								 <div style="width: 20%; float: left;">
							 		<?= ($row['member_branch'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 กลุ่มงาน 

									 <?= ($row['member_branch'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 สาขา 
							 	</div>
								<div  style="width: 30%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		 <?= ($row['member_branch_name'] == "") ? "-" : $row['member_branch_name'] ; ?>
							 	</div>
							 </div>

							</div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
								 <div style="width: 15%; float: left;">
							 		<?= ($row['member_sector'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 ส่วน 

									 <?= ($row['member_sector'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 เขต 
							 	</div>
								<div  style="width: 22%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		 <?= ($row['member_sector_name'] == "") ? "-" : $row['member_sector_name'] ; ?>
							 	</div>

								 <div style="width: 15%; float: left;">
							 		<?= ($row['member_department'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 ฝ่าย 

									 <?= ($row['member_department'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									 ภาค 
							 	</div>
								<div  style="width: 22%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		 <?= ($row['member_department_name'] == "") ? "-" : $row['member_department_name'] ; ?>
							 	</div>

							 	<div style="width: 6%; float: left;text-align: center;">
							 		โทร
							 	</div>
								<div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_tel']?>
							 	</div>
							 </div>
							</div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
							 	<div style="width: 12%; float: left;">
							 		ได้รับเงินเดือน
							 	</div>
							 	<div  style="width: 26%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['member_salary'],2)?>
							 	</div>

								<div style="width: 12%; float: left;">
							 		บาท
							 	</div>

							 	<div  style="width: 12%; float: left;text-align: center;">
							 		ค่าครองชีพ
							   </div>
							   <div  style="width: 26%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['cost_live'],2)?>
							  </div>
							  	<div style="width: 12%; float: left;">
							 		บาท
							 	</div>
							 </div>
							</div>

							<div class="row show-grid" style="margin-top: 3%;display: block;margin-bottom: 2%;">
								 <div  style="width: 100%;">
									<div style="width: 100%;text-align: center;">
										 <b> ขอเสนอคำขอกู้เพื่อเหตุฉุกเฉิน 12 เดือน ดังต่อไปนี้ </b>
									</div>
								</div>
							</div>

							<div class="row show-grid" style="margin-top:2%;display: block;">

							 <div  style="width: 100%;float: left;">
							 	<div style="width: 40%; float: left;text-indent: 1.5em;">
							 		ข้อ 1. ข้าพเจ้าขอกู้เงินของสหกรณ์ฯ จำนวน
							 	</div>

							 	<div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['loan_money'],2)?>
							 	</div>

								<div style="width: 10%; float: left;">
							 		บาท
							 	</div>

							   <div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?="(".$row['loan_money_text'].")"?>
							  </div>

							 </div>
							</div>

							<div class="row show-grid" style="margin-top: 1%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;">
										โดยจะนำไปใช้เพื่อการดังต่อไปนี้ ( ชี้แจงความมุ่งหมายและเหตุผลของการกู้โดยละเอียด )
									</div>
								</div>
							</div>

							<div class="row show-grid" style="margin-top: 1%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;border-bottom: 1px solid #ddd;">
										<?=$row['member_use']?>
									</div>
								</div>
							</div>

							<div class="row show-grid" style="margin-top: 2%;">

							 <div  style="width: 100%;float: left;">
							 	<div style="width: 45%; float: left;text-indent: 1.5em;">
							 		ข้อ 2. ในเวลานี้ ข้าพเจ้ามีหุ้นอยู่ในสหกรณ์ รวม
							 	</div>

							 	<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['member_share']?>
							 	</div>

								<div style="width: 6%; float: left;">
							 		หุ้น
							 	</div>

								<div  style="width: 8%; float: left;text-align: center;">
							 		เป็นเงิน
							   </div>

							   <div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['member_money'],2)?>
							  </div>

							  	<div style="width: 6%; float: left;">
							 		บาท
							 	</div>

							 </div>
							</div>

							<div class="row show-grid" style="margin-top: 2%;">

							 <div  style="width: 100%;float: left;">
							 	<div style="width: 45%; float: left;">
							 		และข้าพเจ้าส่งค่าหุ้นรายเดือนอยู่ในอัตรา เดือนละ
							 	</div>

							 	<div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=number_format($row['share_rate'],2)?>
							 	</div>

								<div style="width: 6%; float: left;">
							 		บาท
							 	</div>
							 </div>
							</div>

							<div class="row show-grid" style="margin-top: 2%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;text-indent: 1.5em;">
										ข้อ 3. ถ้าข้าพเจ้าได้รับเงินกู้ ข้าพเจ้าขอส่งเงินกู้คืน ตามระเบียบว่าด้วยเงินสามัญและเงินกู้เพื่อเหตุฉุกเฉิน หมวด 5
									</div>
								</div>
							</div>
							

							<div class="row show-grid" style="margin-top: 2%;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;text-indent: 1.5em;">
										ข้อ 4. เมื่อข้าพเจ้าได้รับเงินกู้แล้ว ข้าพเจ้ายอมรับผูกพันตามข้อบังคับของสหกรณ์ตามข้อบังคับของสหกรณ์ ดังนี้
									</div>
								</div>
							</div>

							<div class="row show-grid">
								 <div  style="width: 100%;float: left;margin-top: 1%;">
									<div style="width: 100%; float: left;text-indent: 1.5em;line-height:2.7em;">
										ยินยอมให้ธนาคารออมสิน เมื่อได้รับมอบจากสหกรณ์ฯ หักเงินได้รายเดือนของข้าพเจ้าตามจำนวนงวดชำระหนี้ ข้อ 3. 
										เพื่อส่งต่อสหกรณ์ ยอมให้ถือว่าในกรณีใดๆ ดังกล่าวในข้อบังคับข้อ 18 ให้เงินกู้ที่ขอไปจากสหกรณ์ เป็นอันถึงกำหนด 
										ส่งคืนโดยสิ้นเชิงพร้อมทั้งดอกเบี้ยในทันที  โดยมิคำนึงถึงกำหนดเวลาที่ตกลงไว้ ในกรณีที่ข้าพเจ้าประสงค์ จะลาออก 
										จากธนาคารออมสิน ตามข้อบังคับ 33 (3) ข้าพเจ้าจะแจ้งให้สหกรณ์ฯ ทราบเป็นลายลักษณ์อักษรและจัดการชำระหนี้ 
										ซึ่งมีอยู่กับสหกรณ์ฯ ให้เสร็จสิ้น ถ้าข้าพเจ้าไม่จัดการชำระหนี้ให้เสร็จสิ้นตามที่กล่าวมาแล้ว ข้าพเจ้ายินยอมให้ธนาคาร 
										ออมสิน หรือเจ้าหน้าที่ผู้จ่ายเงินเดือน เงินค่าจ้าง โบนัส  บำเหน็จ บำนาญ เงินทุนเลี้ยงชีพ หรือเงินอื่นใดที่เป็นของข้าพเจ้า 
										หักไว้ส่งชำระหนี้พร้อมดอกเบี้ยให้แก่สหกรณ์ฯ เสียก่อนได้
									</div>
								</div>
							</div>

							<div class="row show-grid" style="margin-top:6em;">
								 <div  style="width: 100%;float: left;">
									<div style="width: 100%; float: left;text-indent: 1.5em;line-height:2.7em;">
										ข้อ5. กรณีที่ข้าพเจ้าไม่สามารถชำระหนี้เงินกู้ฉุกเฉินได้ภายในเวลาที่กำหนด ข้าพเจ้ายินยอมให้สหกรณ์
										โอนหนี้เงินกู้ฉุกเฉินคงเหลือเป็นเงินกู้สามัญได้
									</div>
								</div>
							</div>

							<div class="row show-grid" style="display: block;margin-top: 1.5%;">
							 <div  style="width:100%;float: left;">
							 	<div style="width: 100%; float: left;">
							 		<?= ($row['send_money'] == 1) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									  ส่งเงินต้นขั้นต่ำตามที่สหกรณ์กำหนด 
								</div>
								<div style="width: 35%; float: left;margin-top: 1.5%;">
									<?= ($row['send_money'] == 2) ? "<img width='16' height='16' src='/images/Checkbox.png' >" : "<img width='16' height='16' src='/images/Unchecked.png' >" ;?>
									  ส่งเงินต้นและดอกเบี้ยเป็นรายเดือน  
							 	</div>
								<div  style="width: 8%; float: left;text-align: center;">
								    รวม
								</div>
								<div  style="width: 20%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
									<?=$row['month']?>
								</div>
							  	<div style="width: 6%; float: left;">
							 		เดือน
							 	</div>

							</div>
							</div>

							<div class="row show-grid" style="margin-top: 10px;display: block;">
							 <div  style="width: 100%;float: left;">
							 	<div style="width: 15%; float: left;text-indent: 1.5em;">
							 		ผู้ค้ำประกันชื่อ 
							 	</div>
							 	<div  style="width: 25%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['guarantor']?>
							 	</div>
							 	<div  style="width: 12%; float: left;text-align: center;">
							 		สมาชิกเลขที่
							   </div>
							   <div  style="width: 25%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['guarantor_id']?>
							   </div>
							   <div  style="width: 8%; float: left;text-align: center;">
							 		เบอร์โทร
							   </div>
							   <div  style="width: 15%; float: left;border-bottom: 1px solid #ddd;text-align: center;">
							 		<?=$row['guarantor_tel']?>
							   </div>
							 </div>
							</div>


						<div class="row show-grid" style="margin-top: 10px;display: block;">
							<div class="row show-grid" style="display: block;margin-top: 10px;">
							 <div  style="width: 100%;float: left;">
							 	<div  style="width: 100%; float: left;margin-top: 5px;text-indent: 1.5em;line-height:2.7em;">
									ในกรณีที่ผู้ขอกู้ผิดนัด ไม่ชำระเงินหนี้กู้ดังกล่าวข้างต้นให้แก่สหกรณ์ฯ ไม่ว่าด้วยเหตุใดๆก็ดี ข้าพเจ้ายอมรับผิดชำระหนี้
									ให้แก่สหกรณ์ฯ แทนผู้กู้ โดยข้าพเจ้ายอมสละสิทธิ์ขอต่อสู่ที่จะให้สหกรณ์ฯ ทำการเรียกร้องเอาจากผู้ขอกู้ก่อนด้วย
									และในการค้ำประกันนี้ ข้าพเจ้ายินยอมให้นำ ข้อ 3. แห่งคำขอและหนังสือกู้เงินเพื่อเหตุฉุกเฉินมาบังคับใช้กับข้าพเจ้าด้วย
							 	</div>
							 </div>
							</div>
						<br>

						<div class="row show-grid" style="display: block;margin-top: 12em;">
						<div  style="width: 100%;float: left;">

						<div style="width: 100%;text-align: right;margin-top: -2.8em;">
						<!-- <div style="text-align: right;margin-top: -2.8em;page-break-after: always"> -->
						(ลงชื่อ)...................................ผู้ขอกู้
						</div>

						</div>
						</div>

					   <div class="row show-grid" style="display: block;margin-top: 2em;">
						<div  style="width: 100%;float: left;">

						<div style="width: 100%;text-align: right;margin-top: -2.8em;">
						(&nbsp;&nbsp;&nbsp;<?=$row['member_name']?>&nbsp;&nbsp;&nbsp;)
						</div>

						</div>
						</div>



</div>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="scripts/jqueryui/jquery.ui.datepicker-th.js"></script>
    <script src="scripts/jqueryui/jquery-ui-1.8.10.offset.datepicker.min.js"></script>

<?php
$html = ob_get_contents();        //เก็บค่า html ไว้ใน $html 
// ob_end_clean();
$pdf = new mPDF('th', 'A4', '0', '');   //การตั้งค่ากระดาษถ้าต้องการแนวตั้ง ก็ A4 เฉยๆครับ ถ้าต้องการแนวนอนเท่ากับ A4-L
$pdf->SetAutoFont();
$pdf->SetDisplayMode('fullpage');
$pdf->WriteHTML($html, 2);
$pdf->Output("MyPDF/PDF_".$_GET['loan']."_".$_GET['application'].".pdf","F");         // เก็บไฟล์ html ที่แปลงแล้วไว้ใน MyPDF/MyPDF.pdf ถ้าต้องการให้แสด
?>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
