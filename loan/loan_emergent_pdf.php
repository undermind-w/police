<?php
	include "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	//function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}
	$long_month_arr = array('1'=>'มกราคม','2'=>'กุุมภาพันธ์','3'=>'มีนาคม','4'=>'เมษายน','5'=>'พฤษภาคม','6'=>'มิถุนายน','7'=>'กรกฎาคม','8'=>'สิงหาคม','9'=>'กันยายน','10'=>'ตุลาคม','11'=>'พฤศจิกายน','12'=>'ธันวาคม');
	
	
	
	
	
	//$_GET['loan_id'] = base64_decode($_GET['loan_id']);
	//$_GET['loan_application_id'] = base64_decode($_GET['loan_application_id']);
	foreach($_GET as $key => $value){
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	$pdf = new FPDI();
	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_special.pdf";
	$pageCount_3 = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount_3; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
	}
	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/book_agree_account.pdf" ;
	$pageCount = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* ,
			t1.loan_date,
			t3.loan_reason as member_use
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_reason t3 ON t2.member_use = t3.id
		WHERE t1.loan_id = '".$_GET['loan_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 20;
			$pdf->SetXY( 14, $y_point );
			$pdf->MultiCell(65, 5, U2T('เลขที่คำขอกู้เงินออนไลน์ '.$row_loan['loan_number']), $border, 1);
			/*if($row_loan['money_receive_type']=='1'){
			$y_point = 24;
			$pdf->SetXY( 17, $y_point );
			$pdf->SetFont('THSarabunNew', '', 30 );
			$pdf->MultiCell(5, 5, U2T('/'), $border, 'C');
			}else{
			$y_point = 32;
			$pdf->SetXY( 17, $y_point );
			$pdf->SetFont('THSarabunNew', '', 30 );
			$pdf->MultiCell(5, 5, U2T('/'), $border, 'C');
			}*/
			$pdf->SetFont('THSarabunNew', '', 13 );
			$y_point = 47.5;
			$pdf->SetXY( 149, $y_point );
			$pdf->MultiCell(65, 5, U2T($row_loan['write_at']), $border, 1);
			
			$loan_date = explode('-',$row_loan['loan_date']);
			$loan_day = (int)$loan_date[2];
			$loan_month = $long_month_arr[((int)$loan_date[1])];
			$loan_year = $loan_date[0]+543;
			
			$y_point = 54.5;
			$pdf->SetXY( 117, $y_point );
			$pdf->MultiCell(15, 5, U2T($loan_day), $border, 'C');
			$pdf->SetXY( 140, $y_point );
			$pdf->MultiCell(28, 5, U2T($loan_month), $border, 'C');
			$pdf->SetXY( 175, $y_point );
			$pdf->MultiCell(20, 5, U2T($loan_year), $border, 'C');
			
			$y_point = 75;
			$pdf->SetXY( 38, $y_point );
			$pdf->MultiCell(65, 5, U2T($row_loan['member_name']), $border, 1);
			$pdf->SetXY( 115, $y_point );
			$pdf->MultiCell(32, 5, U2T($row_loan['member_position']), $border, 1);
			//$pdf->SetXY( 166, $y_point );
			//$pdf->MultiCell(25, 5, U2T($row_loan['member_id']), $border, 1);
			
			$y_point = 82;
			$pdf->SetXY( 33, $y_point );
			$pdf->MultiCell(28, 5, U2T($row_loan['member_id']), $border, 1);
			$pdf->SetXY( 103, $y_point );
			$pdf->MultiCell(35, 5, U2T($row_loan['card_id']), $border, 1);
			
			$y_point = 88.5;
			$pdf->SetXY( 36, $y_point );
			$pdf->MultiCell(150, 5, U2T($row_loan['member_branch_name']), $border, 1);

			$y_point = 95;
			$pdf->SetXY( 99, $y_point );
			$pdf->MultiCell(38, 5, U2T($row_loan['member_sector_name']), $border, 1);
			$pdf->SetXY( 163, $y_point );
			$pdf->MultiCell(38, 5, U2T($row_loan['member_department_name']), $border, 1);
			//$pdf->SetXY( 164, $y_point );
			//$pdf->MultiCell(38, 5, U2T($row_loan['member_tel']), $border, 1);
			
			$y_point = 102;
			$pdf->SetXY( 164, $y_point );
			$pdf->MultiCell(38, 5, U2T($row_loan['member_tel']), $border, 1);
			
			$y_point = 109;
			$pdf->SetXY( 41, $y_point );
			$pdf->MultiCell(32, 5, U2T(num_format($row_loan['member_salary'])), $border, 'R');
			$pdf->SetXY( 105, $y_point );
			$pdf->MultiCell(48, 5, U2T(num_format($row_loan['cost_of_living'])), $border, 'R');
			
			$y_point = 130;
			$pdf->SetXY(87, $y_point );
			$pdf->MultiCell(33, 5, U2T(num_format($row_loan['loan_money'])), $border, 'R');
			$pdf->SetXY(130, $y_point );
			if($row_loan['loan_money']>0){
				$loan_money_txt = convert($row_loan['loan_money']);
			}else{
				$loan_money_txt = '';
			}
			$pdf->MultiCell(58, 5, U2T($loan_money_txt), $border, 'C');
			
			$y_point = 143;
			$pdf->SetXY(16, $y_point );
			$pdf->MultiCell(185, 5, U2T($row_loan['member_use']), $border, 1);
			
			
			
			/*$y_point = 191;
			$pdf->SetXY( 98, $y_point );
			$pdf->MultiCell(24, 5, U2T(num_format($row_loan['member_share'])), $border, 'R');
			$pdf->SetXY( 142, $y_point );
			$pdf->MultiCell(47, 5, U2T(num_format($row_loan['member_money'])), $border, 'R');*/
			
			$y_point = 200;
			$pdf->SetXY( 93, $y_point );
			//$pdf->MultiCell(57, 5, U2T(num_format($row_loan['share_rate'])), $border, 'R');
		}else if($pageNo == '2'){
			if($row_loan['guarantee_type']=='1'){
				$y_point = 34;
				$pdf->SetXY( 43.5, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			
			$y_point = 69;
			
			$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_application_id = '".$row_loan['loan_application_id']."' ORDER BY bondsman_num ASC";
			$rs_bondsman = $mysqli->query($sql_bondsman);
			
			while($row_bondsman = $rs_bondsman->fetch_assoc()){
				if($row_bondsman['bondsman_name'] != ''){
				$pdf->SetXY( 13, $y_point );
				$pdf->MultiCell(14, 5, U2T($row_bondsman['bondsman_num']), $border, 'C');
				$pdf->SetXY( 28, $y_point );
				$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_name']), $border, 1);
				$pdf->SetXY( 28, $y_point+6 );
				$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_position']." ".$row_bondsman['bondsman_affiliation']), $border, 1);
				$pdf->SetXY( 85, $y_point );
				$pdf->MultiCell(22, 5, U2T($row_bondsman['bondsman_number']), $border, 'C');
				$pdf->SetXY( 108, $y_point );
				$pdf->MultiCell(25, 5, U2T(num_format($row_bondsman['bondsman_salary'])), $border, 'R');
				$pdf->SetXY( 133, $y_point );
				$pdf->MultiCell(30, 5, '', $border, 1);
				$pdf->SetXY( 163, $y_point );
				$pdf->MultiCell(34, 5, '', $border, 1);
				$y_point += 15;
				}
			}
			
			$y_point = 115;
			if($row_loan['guarantee_type']=='4'){
				$pdf->SetXY( 43.5, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			$y_point = 114;
			$pdf->SetXY( 113, $y_point );
			$pdf->MultiCell(46, 5, U2T(num_format($row_loan['guarantee_share_value'])), $border, 'R');
			
			$y_point = 124;
			if($row_loan['guarantee_type']=='3'){
				$pdf->SetXY( 43.5, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			$y_point = 123;
			$pdf->SetXY( 66, $y_point );
			$pdf->MultiCell(120, 5, U2T(num_format($row_loan['guarantee_position'])), $border, 'R');
			
			$y_point = 253;
			$pdf->SetXY( 125, $y_point );
			$pdf->MultiCell(41, 5, U2T($row_loan['member_name']), $border, 'C');
			$y_point = 260;
			$pdf->SetXY( 134, $y_point );
			$pdf->MultiCell(30, 5, U2T($row_loan['member_id']), $border, 'C');
			
		}else if($pageNo == '3'){
			/*if($row_loan['payment_method']=='2'){
			$y_point = 89;
			$pdf->SetXY( 15, $y_point );
			$pdf->SetFont('THSarabunNew', '', 30 );
			$pdf->MultiCell(5, 5, U2T('/'), $border, 'C');
			}else if($row_loan['payment_method']=='1'){
			$y_point = 101;
			$pdf->SetXY( 15, $y_point );
			$pdf->SetFont('THSarabunNew', '', 30 );
			$pdf->MultiCell(5, 5, U2T('/'), $border, 'C');
			}*/
			//$pdf->SetFont('THSarabunNew', '', 13 );
			$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_id = '".$_GET['loan_id']."' order by bondsman_num ASC";
			$rs_bondsman = $mysqli->query($sql_bondsman);
			$bondsman = array();
			while($row_bondsman = $rs_bondsman->fetch_assoc()){
				if($row_bondsman['bondsman_number']==''){
					continue;
				}
				$bondsman[$row_bondsman['bondsman_num']] = $row_bondsman;
			}
			$y_point = 14;
			$pdf->SetXY( 40, $y_point );
			$pdf->MultiCell(58, 5, U2T($bondsman[1]['bondsman_name']), $border, 1);
			$pdf->SetXY( 146, $y_point );
			$pdf->MultiCell(30, 5, U2T($bondsman[1]['bondsman_number']), $border, 1);
			if($bondsman[1]['bondsman_number']!=''){
			$y_point = 23;
			$pdf->SetXY( 51, $y_point );
			$pdf->MultiCell(48, 5, U2T($row_loan['member_name']), $border, 1);
			}
			
			$y_point = 32;
			$pdf->SetXY( 40, $y_point );
			$pdf->MultiCell(58, 5, U2T($bondsman[2]['bondsman_name']), $border, 1);
			$pdf->SetXY( 147, $y_point );
			$pdf->MultiCell(30, 5, U2T($bondsman[2]['bondsman_number']), $border, 1);
			if($bondsman[2]['bondsman_number']!=''){
				$y_point = 41;
				$pdf->SetXY( 51, $y_point );
				$pdf->MultiCell(48, 5, U2T($row_loan['member_name']), $border, 1);
			}
			
			$y_point = 50;
			$pdf->SetXY( 40, $y_point );
			$pdf->MultiCell(58, 5, U2T($bondsman[3]['bondsman_name']), $border, 1);
			$pdf->SetXY( 147, $y_point );
			$pdf->MultiCell(30, 5, U2T($bondsman[3]['bondsman_number']), $border, 1);
			if($bondsman[3]['bondsman_number']!=''){
				$y_point = 58;
				$pdf->SetXY( 51, $y_point );
				$pdf->MultiCell(48, 5, U2T($row_loan['member_name']), $border, 1);
			}
			
			$y_point = 107;
			$pdf->SetXY( 19, $y_point );
			$pdf->MultiCell(38, 5, U2T($bondsman[1]['bondsman_name']), $border, 'C');
			$pdf->SetXY( 81, $y_point );
			$pdf->MultiCell(35, 5, U2T($bondsman[2]['bondsman_name']), $border, 'C');
			$pdf->SetXY( 140, $y_point );
			$pdf->MultiCell(38, 5, U2T($bondsman[3]['bondsman_name']), $border, 'C');
			
			$y_point = 210;
			$pdf->SetXY( 28, $y_point );
			$pdf->MultiCell(80, 5, U2T($row_loan['member_name']), $border, 1);
			$pdf->SetXY( 145, $y_point );
			$pdf->MultiCell(42, 5, U2T(num_format($row_loan['real_money'])), $border, 'R');
			if($row_loan['real_money']!=''){
				$y_point = 217;
				$pdf->SetXY( 16, $y_point );
				if($row_loan['real_money']>0){
					$approve_money_txt = convert($row_loan['real_money']);
				}else{
					$approve_money_txt = '';
				}
				$pdf->MultiCell(75, 5, U2T($approve_money_txt), $border, 'C');
			}
			
			$y_point = 241;
			$pdf->SetXY( 125, $y_point );
			$pdf->MultiCell(59, 5, U2T($row_loan['member_name']), $border, 'C');
		}
		
	}
	
	$pdf->Output();