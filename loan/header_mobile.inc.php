
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		/* display: none; <- Crashes Chrome on hover */
		-webkit-appearance: none;
		margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
	}

	.modal-header-loan {
		padding:9px 15px;
		border:1px solid #ff4081;
		background-color: #ff4081;
		color: #FFF;
		-webkit-border-top-left-radius: 5px;
		-webkit-border-top-right-radius: 5px;
		-moz-border-radius-topleft: 5px;
		-moz-border-radius-topright: 5px;
		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
	}

	.modal-dialog-loan {
		margin:0 auto;
		margin-top: 10%;
	}

</style>

<link href="/html/frontend/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/html/frontend/font/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="/html/frontend/font/font-thaisansneue/thaisansneue.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/scripts/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href="/html/frontend/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="/html/frontend/css/margin.css" rel="stylesheet" type="text/css"/>
<link href="/html/css/sweetalert.css" rel="stylesheet" type="text/css"/>
<script src="/scripts/jquery-1.10.2.min.js"></script>
<script src="/scripts/jquery-migrate-1.2.1.js"></script>
<script src="/html/frontend/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.cookies.2.2.0.min.js"></script>
<script type="text/javascript" src="/scripts/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="/scripts/utility.js"></script>
<script type="text/javascript" src="/scripts/sweetalert.min.js"></script>
<script>

$(function(){
	$(".fancybox-contact").fancybox({
	maxWidth	: 800 ,
	maxHeight	: 555 ,
	fitToView	: false,
	autoSize	: false,
	closeClick	: false,
	openEffect	: 'none',
	closeEffect	: 'none'
	});
});

</script>
<style>

	.fancybox-skin{ background:transparent!important; padding:0 !important; }
	body {
		background-color:#FFF;
	}

</style>
	
