<?php
	include "config.inc.php";
	require PATH . "/class/pagination.class.php"  ;
?>
<HTML>
<head>
<?php
	include "header_mobile.inc.php"
 ?>
	<style>
		body {
			background-color:#FFF;
		}
		.panel-heading {
			border-bottom:1px solid;
		}
		.menu_gruop{
			margin:5px;
		}
		.icon_color{
			color: #EB068D;
		}
		.menu_link:active{
			 background-color:rgb(212,212,212);;
		} 
		h3{
			margin-top:12px;
			margin-bottom:12px;
		}
		.tr_border{
			border-bottom:1px solid rgb(212,212,212);
		}
		.td_style_icon{
			padding: 10px;
		}
		.td_style{
			padding-top: 10px;
			padding-bottom: 10px;
		}
		.text_show{
			font-size: 16px;
		}
		.date_show{
			
			color: #AAA;
			font-size: 16px;
		}
		body{
			font-family:"Sukhumvit Set",sans-serif !important;
		}
	</style>
</head>
<body>
<?php echo "<center><br><br><br><br><h3>เปิดให้บริการเร็วๆนี้</h3></center>";exit;?>
<?php
	$icon = 'fa fa-chevron-circle-right fa-stack-2x text-material-light-blue-300 icon_color'; 
	$param = '';
	if($_GET['member_no']!=''){
		$param = "member_no=".$_GET['member_no'];
	}else{
		foreach($_GET as $key => $value){
			$param .= $key;
			$decode = base64_decode($key);
			$decode = explode('=',$decode);
			$_GET[$decode[0]] = $decode[1];
		}
	}
?>
<table width="100%" border='0'>
	<tr class="tr_border"  onclick="link_click('loan_suggestion_mobile.php?<?php echo $param; ?>')">
		<td align="center" valign="middle" class="td_style_icon">
			<span class="icon_color fa-stack fa-1x">
					<i class="<?php echo $icon; ?>"></i>
				</span>
		</td>
		<td valign="middle" class="td_style" width="90%">
			<span class="text_show">ยื่นคำร้องขอกู้เงินออนไลน์</span>
		</td>
	</tr>
	<tr class="tr_border"  onclick="link_click('loan_check_status_menu_mobile.php?<?php echo $param; ?>')">
		<td align="center" valign="middle" class="td_style_icon">
			<span class="icon_color fa-stack fa-1x">
					<i class="<?php echo $icon; ?>"></i>
				</span>
		</td>
		<td valign="middle" class="td_style">
			<span class="text_show">ตรวจสอบสถานะคำร้อง</span>
		</td>
	</tr>
	<tr class="tr_border"  onclick="link_click('loan_doc_downloan_mobile.php?<?php echo $param; ?>')">
		<td align="center" valign="middle" class="td_style_icon">
			<span class="icon_color fa-stack fa-1x">
					<i class="<?php echo $icon; ?>"></i>
				</span>
		</td>
		<td valign="middle" class="td_style">
			<span class="text_show">ดาวน์โหลดเอกสาร</span>
		</td>
	</tr>
	
</table>
<script>
function link_click(link){
	location.href = link;
}
</script>
</body>
</HTML>