<?php
	session_start();
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	define("PATH",$_SERVER["DOCUMENT_ROOT"]) ;
	define("WS_BASEURL","http://".$_SERVER["HTTP_HOST"]."/ws") ;
	define("BASEPATH","http://".$_SERVER["HTTP_HOST"]."/loan") ;
	
	function curPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}

	define("TITLE"," ") ;
	define("KEYWORD","") ;
	define("DESC","") ;
	define("AUTHOR","") ;

	define("THEME","main");
	// define("DB_MEMBER","dbo");


	require PATH . "/class/connect.inc.php" ;
	require PATH . "/class/function.inc.php" ;
	require PATH . "/class/smarty.inc.php";

	$sql_coop_profile = "SELECT coop_name_th,coop_name_en FROM coop_profile LIMIT 1";
	$rs_coop_profile = $mysqli_app->query($sql_coop_profile);
	$row_coop_profile = $rs_coop_profile->fetch_assoc();	
	define("COOP_NAME_TH",$row_coop_profile['coop_name_th']);
	define("COOP_NAME_EN",$row_coop_profile['coop_name_en']);
	
	$check_http = (@$_SERVER["HTTPS"] ? "https://" : "http://");
	$smarty->assign("HTTP" , $check_http) ;
	define("HTTP",$check_http);

	$base_url = "/" ;
	$smarty->assign("base_url" , $base_url) ;

	$sql = "SELECT * FROM cmp_link WHERE link_status = 1 ORDER BY createdate DESC " ;
	$rs = $mysqli->query($sql);
	$mysqli->error ;
	$elinks = array();
	while($item = $rs->fetch_assoc())
	{
		$elinks[] = $item;
	}

	$smarty->assign("elinks" , $elinks) ;

	$sql = "SELECT * FROM cmp_menu WHERE menu_status = 1  AND menu_position = 1 AND menu_root = 0 ORDER BY menu_seq ASC " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

	while($item = $rs->fetch_assoc()){
		$sql  = "SELECT * FROM cmp_menu WHERE menu_status = 1 AND menu_root = '{$item["menu_id"]}' ORDER BY menu_seq ASC  " ;
		$rs2 = $mysqli->query($sql);
		$items = array();
		echo $mysqli->error ;
		while($item2 = $rs2->fetch_assoc()){
			$items[] = $item2 ;
		}
		$item["items"] = $items ;
		$menu1["header"][] = $item ;
	}


	$sql = "SELECT * FROM cmp_menu WHERE menu_status = 1  AND menu_position = 2 AND menu_root = 0 ORDER BY menu_seq ASC " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

	while($item = $rs->fetch_assoc()){
		$menu1["footer"][] = $item ;

	}

	$sql = "SELECT * FROM cmp_menu WHERE menu_status = 1  AND menu_position = 3 AND menu_root = 0 ORDER BY menu_seq ASC " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

	while($item = $rs->fetch_assoc()){
		$menu1["about"][] = $item ;

	}

	$smarty->assign("menu1" , $menu1) ;
