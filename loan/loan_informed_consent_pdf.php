<?php
	include "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}
	function cal_age($birthday,$type = 'y'){     //รูปแบบการเก็บค่าข้อมูลวันเกิด
		$birthday = date("Y-m-d",strtotime($birthday)); 
		$today = date("Y-m-d");   //จุดต้องเปลี่ยน
		list($byear, $bmonth, $bday)= explode("-",$birthday);       //จุดต้องเปลี่ยน
		list($tyear, $tmonth, $tday)= explode("-",$today);                //จุดต้องเปลี่ยน
		$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear);
		$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
		$mage = ($mnow - $mbirthday);
		//echo "วันเกิด $birthday"."<br>\n";
		//echo "วันที่ปัจจุบัน $today"."<br>\n";
		//echo "รับค่า $mage"."<br>\n";
		$u_y=date("Y", $mage)-1970;
		$u_m=date("m",$mage)-1;
		$u_d=date("d",$mage)-1;
		if($type=='y'){
			return $u_y;
		}else if($type=='m'){
			return $u_m;
		}else{
			return $u_d;
		}
	}

	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/informed_consent.pdf" ;
	
	
	$pdf = new FPDI();
	
	$pageCount = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* 
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_result t3 ON t1.loan_id = t3.loan_id
			WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE bondsman_id = '".$_GET['bondsman_id']."'";
		$rs_bondsman = $mysqli->query($sql_bondsman);
		$row_bondsman = $rs_bondsman->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member t1
		LEFT JOIN cmp_imp_address t2 ON t1.member_no = t2.member_no
		WHERE t1.member_no = '".$row_bondsman['bondsman_number']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 53;
			$pdf->SetXY( 124, $y_point );
			$pdf->MultiCell(80, 5, U2T($row_loan['write_at']), $border, 1);
			
			$y_point = 63;
			$pdf->SetXY( 120, $y_point );
			$pdf->MultiCell(80, 5, U2T(format_date($row_loan['application_date'])), $border, 1);
			
			$y_point = 74;
			$pdf->SetXY( 36, $y_point );
			$pdf->MultiCell(95, 5, U2T($row_bondsman['bondsman_name']), $border, 1);
			
			$pdf->SetXY( 150, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_bondsman['bondsman_number']), $border, 1);
			
			$y_point = 80;
			$pdf->SetXY( 31, $y_point );
			$pdf->MultiCell(36, 5, U2T($row_bondsman['employee_id']), $border, 1);//รหัสพนักงาน
			
			$pdf->SetXY( 75, $y_point );
			$pdf->MultiCell(43, 5, U2T($row_bondsman['bondsman_affiliation']), $border, 1);
			
			$pdf->SetXY( 158, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_member['card_person']), $border, 1);
			
			$y_point = 87;
			$pdf->SetXY( 30, $y_point );
			$pdf->MultiCell(20, 5, U2T($row_member['memb_addr']), $border, 1);
			$pdf->SetXY( 58, $y_point );
			$pdf->MultiCell(37, 5, U2T($row_member['tambol']), $border, 1);
			$pdf->SetXY( 104, $y_point );
			$pdf->MultiCell(43, 5, U2T($row_member['district_desc']), $border, 1);
			$pdf->SetXY( 157, $y_point );
			$pdf->MultiCell(43, 5, U2T($row_member['province_desc']), $border, 1);
			
			$y_point = 94;
			$pdf->SetXY( 26, $y_point );
			$pdf->MultiCell(35, 5, U2T($row_member['mem_telmobile']), $border, 1);
			$pdf->SetXY( 99, $y_point );
			$pdf->MultiCell(100, 5, U2T($row_loan['member_name']), $border, 1);
			
			$y_point = 100;
			$pdf->SetXY( 30, $y_point );
			$pdf->MultiCell(35, 5, U2T($row_loan['member_id']), $border, 1);
			$pdf->SetXY( 86, $y_point );
			$pdf->MultiCell(48, 5, U2T($row_loan['employee_id']), $border, 1);//รหัสพนักงาน
			$pdf->SetXY( 143, $y_point );
			$pdf->MultiCell(55, 5, U2T($row_loan['member_branch_name']), $border, 1);
			
			$y_point = 107;
			$pdf->SetXY( 35, $y_point );
			$pdf->MultiCell(40, 5, U2T(''), $border, 1);//เลขที่สัญญา
			$pdf->SetXY( 86, $y_point );
			$pdf->MultiCell(40, 5, U2T(''), $border, 1);
			$pdf->SetXY( 144, $y_point );
			$pdf->MultiCell(53, 5, U2T(num_format($row_loan['loan_approve_money'])), $border, 'R');
			
			$y_point = 113;
			$pdf->SetXY( 14, $y_point );
			$pdf->MultiCell(83, 5, U2T(convert($row_loan['loan_approve_money'])), $border, 'C');
			
			$y_point = 157;
			$pdf->SetXY( 83, $y_point );
			$pdf->MultiCell(52, 5, U2T($row_bondsman['bondsman_name']), $border, 'C');
		}
		
	}
	
	$pdf->Output();