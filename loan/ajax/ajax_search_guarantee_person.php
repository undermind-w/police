<?php 
include "../config.inc.php";
//require PATH."/class/connect_odbc.inc.php";
		//$member_no = $_POST["member_id"];
		$member_no = sprintf("%06d", $_POST["member_id"]);

		$sql = "SELECT
					t1.member_id AS member_no,
					t1.apply_date ,
					t1.mobile AS telmobile,
					t1.id_card AS card_person,
					CONCAT(
						t2.prename_full,
						t1.firstname_th,
						' ',
						t1.lastname_th
					) AS memname
				FROM
					coop_mem_apply AS t1
				LEFT JOIN coop_prename AS t2 ON t1.prename_id = t2.prename_id
				WHERE
					t1.member_id = '{$member_no}'";		
		//echo $sql.'<hr>';			
		$rs = $mysqli_app->query($sql);
		$row = $rs->fetch_assoc();
		$data = $row;			
		//echo '<pre>'; print_r($data); echo '</pre>';
		$member_date = $data['apply_date'];      //รูปแบบการเก็บค่าข้อมูลวันที่เป็นสมาชิก
		$today = date("Y-m-d");   //จุดต้องเปลี่ยน

		//start นับเดือน
		$datetime1 = date_create($member_date);
		$datetime2 = date_create($today);				
		$diff =  $datetime1->diff($datetime2);
		$month_count = $diff->y * 12 + $diff->m + $diff->d / 30;
		//end นับเดือน

		if($_POST['loan_type']=='1'){
			if($month_count >= 6){
				$data['can_guarantee'] = 'yes';
			}else{
				$data['can_guarantee'] = 'no';
			}
		}else if($_POST['loan_type']=='3'){
			if($month_count >= 6){
				$data['can_guarantee'] = 'yes';
			}else{
				$data['can_guarantee'] = 'no';
			}
		}
		if($row['member_no']!=''){
			$result = json_encode($data);
		}else{
			$result = "error";
		}
	echo $result;
 ?>