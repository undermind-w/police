<?php
	include "config.inc.php";
	
	if ($_GET['do'] == "approve") { 
		$loan_id = $_GET['loan_id'];
		$bondsman_id = $_GET['bondsman_id'];
		$loan_application_type = $_GET['type_id'];
		$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE bondsman_id = '{$bondsman_id}' ";
		$rs_bondsman =  $mysqli->query($sql_bondsman);
		$row_bondsman = $rs_bondsman->fetch_assoc();
		$status_name =  $row_bondsman['bondsman_name']." ผู้ค้ำประกันได้กดยอมรับแล้ว";
		$now = date("Y-m-d H:i:s");

		/*$send_status = "insert cmp_admin_loan_money_status set
							loan_id='{$loan_id}',
							status_name= '{$status_name}',
							status_date='{$now}',
							loan_application_type ='{$loan_application_type}'
							"; 
		$mysqli->query($send_status);
		*/
		$sql = "update cmp_admin_loan_money_status set
								status_name= '{$status_name}',
								status_date='{$now}'
								WHERE
								loan_id='{$loan_id}' AND bondsman_id='{$bondsman_id}'
		";
		$mysqli->query($sql); 
		
		$sql = "update loan_application_bondsman set
								bondsman_approve ='1',
								approve_time='{$now}'
								WHERE
								bondsman_id='{$bondsman_id}'
		";
		$mysqli->query($sql); 
		
		if ($mysqli) {
			header("Location: ".BASEPATH."/loan_bondsman_confirm.php?loan_id=".$_GET['loan_id']."&bondsman_id=".$bondsman_id."");

		}else{
			header("Location: ".BASEPATH."/loan_bondsman_confirm.php?loan_id=".$_GET['loan_id']."&bondsman_id=".$bondsman_id."");
		}
		
	}
?>
<HTML>
<head>
<?php
	include "header_mobile.inc.php";
	$param = '';
	foreach($_GET as $key => $value){
		$param .= $key."&";
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	$param = substr($param,0,-1);
?>
<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
<script src="/scripts/jquery.validate_loan.js"></script>
<style>
	.input-group[class*=col-]{
		padding-right: 15px;
		padding-left: 15px;
	}
	.icon_color{
		background-color: rgb(235,6,141);
		color: #FFF;
	}
</style>
</head>
<body>
	<div class="panel-body">
		<div class="row">
		<div class="col-md-12">
		<div class="text-center" style="margin-top: 1em;">
			<img style="margin:auto;" width="100" height="100" src="/timthumb.php?a=c&zc=2&w=170&h=140&src=/images/logo.png" class="img-responsive">
		</div>

	<?php 
	$loan_application_type = array();
	$sql_type = "SELECT * FROM loan_setting_detail ORDER BY id ASC";
	$rs_type = $mysqli->query($sql_type);
	while($row_type = $rs_type->fetch_assoc()){
		$loan_application_type[$row_type['id']] = $row_type['loan_name'];
	}
					
	  $sql = "SELECT * FROM loan_application WHERE loan_id = '{$_GET['loan_id']}'"; 
	  $rs = $mysqli->query($sql);
	  echo $mysqli->error;
	  $row = $rs->fetch_assoc();
	  
	  $doc_name = $loan_application_type[$row['loan_application_type']];

	  $sql_bondsman = "SELECT bondsman_approve FROM loan_application_bondsman WHERE loan_id = '{$_GET['loan_id'] }' AND bondsman_id = '{$_GET['bondsman_id']}' "; 
	  $rs_bondsman = $mysqli->query($sql_bondsman);
	  echo $mysqli->error;
	  $row_bondsman = $rs_bondsman->fetch_assoc();

	 ?>


	<h1 class="text-center"><?php echo $doc_name?></h1>
	<h3 class="text-center">คุณ <?php echo $row['member_name']?> ได้ทำการขอกู้เงินประเภท <?php echo $doc_name?> กับ<?php echo COOP_NAME_TH;?> โดยให้ท่านเป็นผู้ค้ำประกัน 
	<br>
	<br>

	<?php  if ($row_bondsman['bondsman_approve'] == 0) { ?>
	กดปุ่มด้านล่างเพื่อยืนยันการเป็นผู้ค้ำประกัน
	<?php } ?>

	</h3>

	<div class="text-center" style="margin-top:1em;">

	 <?php  if ($row_bondsman['bondsman_approve'] == 1) { ?>
		<button style="font-size: 16px" type="button" class="btn btn-success">ยืนยันการเป็นผู้ค้ำประกันเรียบร้อยแล้ว</button>
	 <?php }else{ ?>
		<a onclick="return confirm('ยืนยันการเป็นผู้ค้ำประกัน?')" href="<?php echo BASEPATH."/loan_bondsman_confirm.php?loan_id=".$_GET['loan_id']."&bondsman_id=".$_GET['bondsman_id']."&type_id=".$row['loan_application_type']."&do=approve";?>">
		<button style="font-size: 16px" type="button" class="btn btn-material-light-blue-300 btn-lg">ยืนยันการเป็นผู้ค้ำประกัน</button>
		</a>
	 <?php } ?>
	
	 <div class="text-center" style="margin-top: 1em;">
		<a target="_blank" href="<?php echo BASEPATH."/loan_all_pdf.php?".base64_encode("loan_application_id=".$row['loan_application_id']); ?>">
		<button style="font-size: 16px" type="button" class="btn btn-material-light-blue-300 btn-lg">ดูเอกสารการสมัคร</button>
		</a>
	</div>
		
	</div>
	</div>
	</div>
</div>
</body>
</HTML>

