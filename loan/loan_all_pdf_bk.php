<?php
	include "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	require "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/pdf.php";
	require "{$_SERVER["DOCUMENT_ROOT"]}/class/FPDI-1.6.2/fpdi.php";
	//function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", ($text)); }
	function format_date($text) { return date('d/m/Y',strtotime('+543 year',strtotime($text))); }
	function num_format($text) { 
		if($text!=''){
			return number_format($text,2);
		}else{
			return '';
		}
	}
	function cal_age($birthday,$type = 'y'){     //รูปแบบการเก็บค่าข้อมูลวันเกิด
		$birthday = date("Y-m-d",strtotime($birthday)); 
		$today = date("Y-m-d");   //จุดต้องเปลี่ยน
		list($byear, $bmonth, $bday)= explode("-",$birthday);       //จุดต้องเปลี่ยน
		list($tyear, $tmonth, $tday)= explode("-",$today);                //จุดต้องเปลี่ยน
		$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear);
		$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
		$mage = ($mnow - $mbirthday);
		//echo "วันเกิด $birthday"."<br>\n";
		//echo "วันที่ปัจจุบัน $today"."<br>\n";
		//echo "รับค่า $mage"."<br>\n";
		$u_y=date("Y", $mage)-1970;
		$u_m=date("m",$mage)-1;
		$u_d=date("d",$mage)-1;
		if($type=='y'){
			return $u_y;
		}else if($type=='m'){
			return $u_m;
		}else{
			return $u_d;
		}
	}
	foreach($_GET as $key => $value){
		$decode = base64_decode($key);
		$decode = explode('=',$decode);
		$_GET[$decode[0]] = $decode[1];
	}
	
	$pdf = new FPDI();
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_petition.pdf" ;
	$pageCount = $pdf->setSourceFile($filename);
	
	for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);

		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* ,t3.loan_reason as member_use
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_reason t3 ON t2.member_use = t3.id
		WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		if($pageNo == '1'){
			$y_point = 40;
			$pdf->SetXY( 14, $y_point );
			$pdf->MultiCell(65, 5, U2T('เลขที่คำขอกู้เงินออนไลน์ '.$row_loan['loan_number']), $border, 1);
			
			$y_point = 52;
			$pdf->SetXY( 146, $y_point );
			$pdf->MultiCell(65, 5, U2T($row_loan['write_at']), $border, 1);//เขียนที่
			
			$y_point = 60;
			$pdf->SetXY( 146, $y_point );
			$pdf->MultiCell(80, 5, format_date(U2T($row_loan['application_date']), $border, 1));//วันที่
			
			$y_point = 79;
			$pdf->SetXY( 36, $y_point );
			$pdf->MultiCell(60, 5, U2T($row_loan['member_name']), $border, 1);//ชื่อผู้กู้
			$pdf->SetXY( 116, $y_point );
			$pdf->MultiCell(30, 5, U2T($row_loan['member_id']), $border, 1);//เลขที่สมาชิกผู้กู้
			$pdf->SetXY( 160, $y_point );
			$pdf->MultiCell(45, 5, U2T($row_loan['member_position']), $border, 1);//ตำแหน่งผู้กู้
			
			$y_point = 87;
			$pdf->SetXY( 57, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_loan['card_id']), $border, 1);//เลขประจำตัว ปชช
			
			$pdf->SetXY( 105, $y_point );
			$pdf->MultiCell(20, 5, U2T($row_loan['employee_id']), $border, 1);
			
			$y_point = 95;
			if(U2T($row_loan['member_branch']) == '1' && U2T($row_loan['member_branch_name']) != ''){
				$pdf->SetXY( 23, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}else if(U2T($row_loan['member_branch']) == '2' && U2T($row_loan['member_branch_name']) != ''){
				$pdf->SetXY( 44, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}
			
			$pdf->SetXY( 58, $y_point );
			$pdf->MultiCell(150, 5, U2T($row_loan['member_branch_name']), $border, 1);
			
			$y_point = 103;
			if(U2T($row_loan['member_sector']) == '1' && U2T($row_loan['member_sector_name']) != ''){
				$pdf->SetXY( 14, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}else if(U2T($row_loan['member_sector']) == '2' && U2T($row_loan['member_sector_name']) != ''){
				$pdf->SetXY( 29, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}
			$pdf->SetXY( 41, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_loan['member_sector_name']), $border, 1);
			
			if(U2T($row_loan['member_department']) == '1' && U2T($row_loan['member_department_name']) != ''){
				$pdf->SetXY( 84, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}else if(U2T($row_loan['member_department']) == '2' && U2T($row_loan['member_department_name']) != ''){
				$pdf->SetXY( 102, $y_point+1 );
				$pdf->MultiCell(5, 5, '/', $border, 1);
			}
			$pdf->SetXY( 114, $y_point );
			$pdf->MultiCell(35, 5, U2T($row_loan['member_department_name']), $border, 1);
			$pdf->SetXY( 160, $y_point );
			$pdf->MultiCell(35, 5, U2T($row_loan['member_tel']), $border, 1);
			
			$y_point = 111;
			$pdf->SetXY( 50, $y_point );
			$pdf->MultiCell(35, 5, U2T(num_format($row_loan['member_salary'])), $border, 'R');
			
			$y_point = 118;
			$pdf->SetXY( 102, $y_point );
			$pdf->MultiCell(35, 5, U2T(num_format($row_loan['loan_money'])), $border, 'R');
			$pdf->SetXY( 144, $y_point );
			$pdf->MultiCell(53, 5, U2T($row_loan['loan_money_text']), $border, 'C');
			$y_point = 133;
			$pdf->SetXY( 12, $y_point );
			$pdf->MultiCell(185, 5, U2T($row_loan['member_use']), $border, 1);
			
			$y_point = 141;
			$pdf->SetXY( 98, $y_point );
			$pdf->MultiCell(25, 5, U2T(num_format(($row_loan['member_money']/10))), $border, 'R');
			$pdf->SetXY( 143, $y_point );
			$pdf->MultiCell(47, 5, U2T(num_format($row_loan['member_money'])), $border, 'R');
			
			$y_point = 151;
			$pdf->SetXY( 80, $y_point );
			//$pdf->MultiCell(54, 5, U2T(num_format($row_loan['share_rate'])), $border, 'R');
			
		if($row_loan['guarantee_type']=='1'){
			$y_point = 203;
			$pdf->SetXY( 41, $y_point );
			$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
		}
			
			$y_point = 237;
			
			$sql_bondsman = "SELECT * FROM loan_application_bondsman WHERE loan_application_id = '".$_GET['loan_application_id']."' ORDER BY bondsman_num ASC";
			$rs_bondsman = $mysqli->query($sql_bondsman);
			
			while($row_bondsman = $rs_bondsman->fetch_assoc()){
				if($row_bondsman['bondsman_name'] != ''){
				$pdf->SetXY( 11, $y_point );
				$pdf->MultiCell(14, 5, U2T($row_bondsman['bondsman_num']), $border, 'C');
				$pdf->SetXY( 25, $y_point );
				$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_name']), $border, 1);
				$pdf->SetXY( 25, $y_point+6 );
				//$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_position']." ".$row_bondsman['bondsman_affiliation']), $border, 1);
				$pdf->MultiCell(60, 5, U2T($row_bondsman['bondsman_tel']), $border, 1);
				$pdf->SetXY( 85, $y_point );
				$pdf->MultiCell(22, 5, U2T($row_bondsman['bondsman_number']), $border, 'C');
				$pdf->SetXY( 108, $y_point );
				$pdf->MultiCell(25, 5, U2T(num_format($row_bondsman['bondsman_salary'])), $border, 'R');
				$pdf->SetXY( 133, $y_point );
				$pdf->MultiCell(30, 5, '', $border, 1);
				$pdf->SetXY( 163, $y_point );
				$pdf->MultiCell(34, 5, '', $border, 1);
				$y_point += 15;
				}
			}
		}else if($pageNo == '2'){
			$y_point = 29;
			if($row_loan['guarantee_type']=='4'){
				$pdf->SetXY( 41, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			$y_point = 28;
			$pdf->SetXY( 112, $y_point );
			$pdf->MultiCell(46, 5, U2T(num_format($row_loan['guarantee_share_value'])), $border, 'R');
			
			
			
			$y_point = 36;
			if($row_loan['guarantee_type']=='2'){
				$pdf->SetXY( 41, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			$pdf->SetXY( 102, $y_point );
			$pdf->MultiCell(37, 5, U2T($row_loan['account_bank_type']), $border, 1);
			$pdf->SetXY( 147, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_loan['account_bank_number']), $border, 1);
			
			$y_point = 43;
			$pdf->SetXY( 64, $y_point );
			$pdf->MultiCell(46, 5, U2T(num_format($row_loan['deposit_amount'])), $border, 'R');
			
			$y_point = 52;
			if($row_loan['guarantee_type']=='3'){
				$pdf->SetXY( 41, $y_point );
				$pdf->MultiCell(10, 5, U2T('/'), $border, 'C');
			}
			$y_point = 51;
			$pdf->SetXY( 64, $y_point );
			$pdf->MultiCell(120, 5, U2T(num_format($row_loan['guarantee_position'])), $border, 'R');
			
			$y_point = 67;
			$pdf->SetXY( 103, $y_point );
			//$pdf->MultiCell(40, 5, U2T($row_loan['daposit_issue']), $border, 'R');
			$pdf->MultiCell(40, 5, U2T(''), $border, 'R');
		}
		
	}
	
	$sql_loan_special = "SELECT 
		* 
	FROM 
		loan_special_beg t1
	WHERE t1.loan_id = '".$_GET['loan_id']."'";
	$rs_loan_special = $mysqli->query($sql_loan_special);
	$row_loan_special = $rs_loan_special->fetch_assoc();
	if($row_loan_special['id']!=''){
		$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_special_beg.pdf" ;
		
		$pageCount = $pdf->setSourceFile($filename);
		for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {	
		$pdf->AddPage();
			$tplIdx = $pdf->importPage($pageNo); 
			$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
			
			$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
			$pdf->SetFont('THSarabunNew', '', 13 );
			
			$border = 0;
			$pdf->SetTextColor(0, 0, 0);
			$sql_loan = "SELECT 
				* 
			FROM 
				loan_special_beg t1
				INNER JOIN cmp_admin_loan_money t2 ON t1.loan_id = t2.loan_id 
				INNER JOIN loan_application t3 ON t1.loan_id = t3.loan_id 
			WHERE t1.id = '".$row_loan_special['id']."'";
			$rs_loan = $mysqli->query($sql_loan);
			$row_loan = $rs_loan->fetch_assoc();
			
			$sql_member = "SELECT * FROM cmp_imp_member WHERE member_no = '".$row_loan['member_id']."'";
			$rs_member = $mysqli->query($sql_member);
			$row_member = $rs_member->fetch_assoc();
			if($pageNo == '1'){
				$y_point = 26;
				$pdf->SetXY( 145, $y_point );
				$pdf->MultiCell(65, 5, U2T($row_loan['write_at']), $border, 1);//เขียนที่
				
				$y_point = 34;
				$pdf->SetXY( 145, $y_point );
				$pdf->MultiCell(80, 5, format_date(U2T($row_loan['application_date']), $border, 1));//วันที่
				
				$y_point = 56;
				$pdf->SetXY( 48, $y_point );
				$pdf->MultiCell(95, 5, U2T($row_loan['member_name']), $border, 1);//ชื่อผู้กู้
				$pdf->SetXY( 146, $y_point );
				$pdf->MultiCell(15, 5, U2T(cal_age($row_member['birthdtm'])), $border, 'R');
				
				$y_point = 64;
				$pdf->SetXY( 86, $y_point );
				$pdf->MultiCell(30, 5, U2T($row_loan['member_id']), $border, 1);
				$pdf->SetXY( 136, $y_point );
				$pdf->MultiCell(21, 5, U2T($row_loan['employee_id']), $border, 1);
				$pdf->SetXY( 169, $y_point );
				$pdf->MultiCell(35, 5, U2T($row_loan['member_position']), $border, 1);
				
				$y_point = 72;
				$pdf->SetXY( 43, $y_point );
				$pdf->MultiCell(29, 5, U2T($row_loan['member_salary']), $border, 'R');
				$pdf->SetXY( 106, $y_point );
				$pdf->MultiCell(37, 5, U2T($row_loan['member_branch_name']), $border, 1);
				$pdf->SetXY( 156, $y_point );
				$pdf->MultiCell(37, 5, U2T($row_loan['member_sector_name']), $border, 1);
				
				$y_point = 80;
				$pdf->SetXY( 37, $y_point );
				$pdf->MultiCell(26, 5, U2T($row_loan['member_department_name']), $border, 1);
				$pdf->SetXY( 75, $y_point );
				$pdf->MultiCell(24, 5, U2T($row_loan['member_tel']), $border, 1);
				$pdf->SetXY( 128, $y_point );
				$pdf->MultiCell(13, 5, U2T(cal_age($row_member['memdtm'])), $border, 'R');
				$pdf->SetXY( 145, $y_point );
				$pdf->MultiCell(19, 5, U2T(cal_age($row_member['memdtm'],'m')), $border, 'R');
				
				$y_point = 87;
				$pdf->SetXY( 28, $y_point );
				$pdf->MultiCell(50, 5, U2T(number_format($row_loan['loan_money'],2)), $border, 'R');
				$pdf->SetXY( 85, $y_point );
				$pdf->MultiCell(53, 5, U2T($row_loan['member_use']), $border, 1);
				
				if($row_loan['condition_1']=='1'){
					$y_point = 96;
					$pdf->SetXY( 32, $y_point );
					$pdf->MultiCell(5, 5, '/', $border, 1);
					
					$y_point = 102;
					$pdf->SetXY( 68, $y_point );
					$pdf->MultiCell(35, 5, U2T($row_loan['condition_1_extra_income']), $border, 1);
					$pdf->SetXY( 113, $y_point );
					$pdf->MultiCell(24, 5, U2T(number_format($row_loan['condition_1_extra_income_per_month'],2)), $border, 'R');
					$pdf->SetXY( 162, $y_point );
					$pdf->MultiCell(24, 5, U2T(number_format($row_loan['condition_1_mariage_income'],2)), $border, 'R');
				}
				
				if($row_loan['condition_2']=='1'){
					$y_point = 111;
					$pdf->SetXY( 32, $y_point );
					$pdf->MultiCell(5, 5, '/', $border, 1);
				}
				
				if($row_loan['condition_3']=='1'){
					$y_point = 119;
					$pdf->SetXY( 32, $y_point );
					$pdf->MultiCell(5, 5, '/', $border, 1);
				}
				
				if($row_loan['condition_4']=='1'){
					$y_point = 126;
					$pdf->SetXY( 32, $y_point );
					$pdf->MultiCell(5, 5, '/', $border, 1);
					$pdf->SetXY( 119, $y_point );
					$pdf->MultiCell(7, 5, $row_loan['condition_4_year'], $border, 'R');
					$pdf->SetXY( 150, $y_point );
					$pdf->MultiCell(10, 5, $row_loan['condition_4_period'], $border, 'R');
				}
				
				if($row_loan['condition_5']=='1'){
					$y_point = 134;
					$pdf->SetXY( 32, $y_point );
					$pdf->MultiCell(5, 5, '/', $border, 1);
					$pdf->SetXY( 70, $y_point-1 );
					$pdf->MultiCell(25, 5, $row_loan['condition_5_member_month'], $border, 'R');
				}
				
				if($row_loan['condition_6']=='1'){
					$y_point = 142;
					$pdf->SetXY( 32, $y_point );
					$pdf->MultiCell(5, 5, '/', $border, 1);
					$pdf->SetXY( 44, $y_point-1 );
					$pdf->MultiCell(160, 5, $row_loan['condition_6_detail'], $border, 1);
				}
				
				$y_point = 179;
				$pdf->SetXY( 95, $y_point );
				$pdf->MultiCell(55, 5, U2T($row_loan['member_name']), $border, 'C');//ชื่อผู้กู้
				
				$y_point = 195;
				$pdf->SetXY( 43, $y_point );
				$pdf->MultiCell(44, 5, U2T($row_loan['member_name']), $border, 1);//ชื่อผู้กู้
				$pdf->SetXY( 103, $y_point );
				$pdf->MultiCell(7, 5, U2T(cal_age($row_member['memdtm'])), $border, 'R');
				$pdf->SetXY( 112, $y_point );
				$pdf->MultiCell(10, 5, U2T(cal_age($row_member['memdtm'],'m')), $border, 'R');
				$pdf->SetXY( 147, $y_point );
				$pdf->MultiCell(33, 5, U2T(number_format($row_loan['loan_money'],2)), $border, 'R');
				
			}
		}
	}
	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/cover_normal.pdf" ;
	$pageCount_3 = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount_3; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
	}
	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_ageement_1.pdf" ;
	$pageCount_1 = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount_1; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* ,t3.loan_reason as member_use
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_reason t3 ON t2.member_use = t3.id
		WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member t1
		LEFT JOIN cmp_imp_address t2 ON t1.member_no = t2.member_no
		WHERE t1.member_no = '".$row_loan['member_id']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 67;
			$pdf->SetXY( 94, $y_point );
			$pdf->MultiCell(40, 5, U2T(format_date($row_loan['application_date'])), $border, 1);
			
			$y_point = 75;
			$pdf->SetXY( 48, $y_point );
			$pdf->MultiCell(49, 5, U2T($row_loan['member_name']), $border, 1);
			$pdf->SetXY( 104, $y_point );
			$pdf->MultiCell(9, 5, U2T(cal_age($row_member['birthdtm'])), $border, 'R');
			
			$y_point = 83;
			$pdf->SetXY( 31, $y_point );
			$pdf->MultiCell(49, 5, U2T($row_loan['member_id']), $border, 1);
			$pdf->SetXY( 113, $y_point );
			$pdf->MultiCell(49, 5, U2T($row_loan['member_position']), $border, 1);
			
			$y_point = 90;
			$pdf->SetXY( 39, $y_point );
			$pdf->MultiCell(63, 5, U2T($row_loan['member_branch_name']), $border, 1);
			$pdf->SetXY( 120, $y_point );
			$pdf->MultiCell(28, 5, U2T($row_member['memb_addr']), $border, 1);
			$pdf->SetXY( 156, $y_point );
			$pdf->MultiCell(40, 5, U2T($row_member['road']), $border, 1);
			
			$y_point = 98;
			$pdf->SetXY( 28, $y_point );
			$pdf->MultiCell(47, 5, U2T($row_member['soi']), $border, 1);
			$pdf->SetXY( 93, $y_point );
			$pdf->MultiCell(44, 5, U2T($row_member['tambol']), $border, 1);
			$pdf->SetXY( 153, $y_point );
			$pdf->MultiCell(44, 5, U2T($row_member['district_desc']), $border, 1);
			
			$y_point = 106;
			$pdf->SetXY( 23, $y_point );
			$pdf->MultiCell(52, 5, U2T($row_member['province_desc']), $border, 1);
			$pdf->SetXY( 88, $y_point );
			$pdf->MultiCell(46, 5, U2T($row_loan['member_tel']), $border, 1);
			
			$y_point = 136;
			$pdf->SetXY( 96, $y_point );
			$pdf->MultiCell(94, 5, U2T(num_format($row_loan['real_money'])), $border, 'R');
			
			$y_point = 144;
			$pdf->SetXY( 15, $y_point );
				if($row_loan['real_money']>0){
					$approve_money_txt = convert($row_loan['real_money']);
				}else{
					$approve_money_txt = '';
				}
			$pdf->MultiCell(96, 5, U2T($approve_money_txt), $border, 'C');
			
			$y_point = 152;
			$pdf->SetXY( 13, $y_point );
			$pdf->MultiCell(73, 5, U2T($row_loan['member_use']), $border, 1);
			
			$y_point = 160;
			$pdf->SetXY( 107, $y_point );
			$pdf->MultiCell(80, 5, U2T(num_format($row_loan['estimate_money_period'])), $border, 'R');
			
			$y_point = 167;
			$pdf->SetXY( 14, $y_point );
				if($row_loan['estimate_money_period']>0){
					$estimate_money_period_txt = convert($row_loan['estimate_money_period']);
				}else{
					$estimate_money_period_txt = '';
				}
			$pdf->MultiCell(81, 5, U2T($estimate_money_period_txt), $border, 'C');
			
			$y_point = 175;
			$pdf->SetXY( 112, $y_point );
			$pdf->MultiCell(41, 5, U2T($row_loan['interest_rate']), $border, 'R');
			
			$y_point = 182;
			$pdf->SetXY( 23, $y_point );
			$pdf->MultiCell(38, 5, U2T(num_format($row_loan['pay_last_month'])), $border, 'R');
			$pdf->SetXY( 154, $y_point );
			$pdf->MultiCell(17, 5, U2T($row_loan['period']), $border, 'R');
			
			$y_point = 190;
			$pdf->SetXY( 38, $y_point );
			$pdf->MultiCell(38, 5, U2T(''), $border, 1);
			
			$y_point = 205;
			$pdf->SetXY( 102, $y_point );
			$pdf->MultiCell(20, 5, U2T(''), $border, 1);
		}
		
	}
	
	$filename = "{$_SERVER["DOCUMENT_ROOT"]}/Document/loan_ageement_2.pdf" ;
	$pageCount_2 = $pdf->setSourceFile($filename);
	for ($pageNo = 1; $pageNo <= $pageCount_2; $pageNo++) {	
	$pdf->AddPage();
		$tplIdx = $pdf->importPage($pageNo); 
		$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
		
		$pdf->AddFont('THSarabunNew', '', 'THSarabunNew.php');
		$pdf->SetFont('THSarabunNew', '', 13 );
		
		$border = 0;
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetAutoPageBreak(true,0);
		$sql_loan = "SELECT 
			* 
		FROM 
			cmp_admin_loan_money t1
			INNER JOIN loan_application t2 ON t1.loan_id = t2.loan_id 
			LEFT JOIN loan_result t3 ON t1.loan_id = t3.loan_id
			WHERE t2.loan_application_id = '".$_GET['loan_application_id']."'";
		$rs_loan = $mysqli->query($sql_loan);
		$row_loan = $rs_loan->fetch_assoc();
		
		$sql_member = "SELECT * FROM cmp_imp_member t1
		LEFT JOIN cmp_imp_address t2 ON t1.member_no = t2.member_no
		WHERE t1.member_no = '".$row_loan['member_id']."'";
		$rs_member = $mysqli->query($sql_member);
		$row_member = $rs_member->fetch_assoc();
		if($pageNo == '1'){
			$y_point = 12;
			$pdf->SetXY( 119, $y_point );
			$pdf->MultiCell(13, 5, U2T(''), $border, 1);
			
			$y_point = 167;
			$pdf->SetXY( 85, $y_point );
			$pdf->MultiCell(61, 5, U2T($row_loan['member_name']), $border, 'C');
			
		}
		
	}
	$pdf->Output();