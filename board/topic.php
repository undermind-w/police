<?php
require "./config.inc.php";
require PATH."/class/Board.php";
require PATH."/class/Page.php";

$smarty->assign("is_newwindows", TRUE);

$db = new MysqlHelper();
$board = new Board($db);
$page = new Page();

$topic = $board->getTopic(@$_GET["id"]);

/**** Head Tag ****/
$headtag = "";
$headtag .= $smarty->fetch("board/views/headtag.tpl");
$headtag .= $smarty->fetch("board/views/topic_headtag.tpl");
$smarty->assign("headtag", $headtag);
/**** End Head Tag ****/

if($topic["topic_id"]) {
	/**** Content ****/
	$content = "";
	
	// Stat
	$cookie_id = "topic_stat_".@$_GET["id"];
	$time = 86400; // expire 24 hour
	$domain = $_SERVER["HTTP_HOST"];
	$domain =  substr_count($domain, ".") > 1 ? substr($domain, strpos($domain, ".")) : $domain;
	if(!isset($_COOKIE[$cookie_id])) {
		setcookie($cookie_id, "Y", time() + $time, "/", $domain);
		$board->updateStatTopic(@$_GET["id"]);
	}
	
	// Breadcrumb
	$smarty->assign("breadcrumb", array("กระดานถามตอบ" => "/board"));
	
	// Sub Head
	//$smarty->assign("_head", $smarty->fetch("board/views/head.tpl"));
	
	// Sub Content
	# Member
	$c_user_id = isset($_COOKIE["wmem_id"]) ? $_COOKIE["wmem_id"] : "";
	$c_picture = isset($_COOKIE["wmem_picture"]) ? $_COOKIE["wmem_picture"] : "";
	
	$member_pic = "/images/board/member/".$c_picture;
	$smarty->assign("member_pic", $member_pic);
	
	# Description
	$search = array(
		"@<\?[^>]*?>@si",
		"@([\r\n])[\s]+@",
		"@&(quot|#34);@i",
		"@&(nbsp|#160);@i",
		"@\"@");
	$replace = array(
		"",
		"",
		"",
		"",
		"");
	$description = mb_substr(preg_replace($search, $replace, strip_tags($topic["topic_detail"])), 0, 160, "UTF-8");
	
	# Topic
	$topic["is_own"] = $topic["topic_wmem_id"] == $c_user_id || @$_COOKIE["wmem_type_id"] == 99 ? TRUE : FALSE;
	$smarty->assign("topic", $topic);
	
	# All Post
	$page = new Page();
	$_param = array(
		"pagenum" => 10,
		"showlist" => 20,
		"page" => 1,
		"row_count" => $board->getPostCountByTopicID(@$_GET["id"])
	);
	$_pages = $page->getPage($_param);
	$smarty->assign("canload3", $_pages["page_count"] > 1 ? "1" : "0");
	$smarty->assign("posts", $board->getPostEntriesByTopicID($_pages["offset"], $_pages["showlist"], @$_GET["id"]));
	$smarty->assign("posts_all", $smarty->fetch("board/views/post_list.tpl"));
	
	$smarty->assign("_content", $smarty->fetch("board/views/topic.tpl"));
	
	// Sidebar
	$_sidebar = "";
	require "./sidebar.inc.php";
	
	$content .= $smarty->fetch("board/layout_main.tpl");
	$smarty->assign("content", $content);
	/**** End Content ****/
	
	/**** Scripts ****/
	$smarty->assign("scripts", $smarty->fetch("board/views/topic_js.tpl"));
	/**** End Scripts ****/
}
else {
	$description = "ไม่พบกระทู้";
	$smarty->assign("content", $smarty->fetch("board/views/topic_not_found.tpl"));
}

$smarty->assign("title", $topic["topic_title"]);
$smarty->assign("keywords", $topic["topic_title"]);
$smarty->assign("description", $description);
$smarty->assign("subject", "กระดานถามตอบ");
$smarty->assign("ogimage", "");

$smarty->display(THEME.".tpl");

/**** End of file ****/