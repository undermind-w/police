<?php
require "./authen.inc.php";
require "./config.inc.php";
require PATH."/class/Board.php";
require PATH."/class/MemberBoard.php";

$smarty->assign("is_newwindows", TRUE);

$db = new MysqlHelper();
$board = new Board($db);
$member = new MemberBoard($db);

$forum_id = "";
$do = isset($_REQUEST["do"]) ? $_REQUEST["do"] : "";
switch($do) {
	case "edit":
        if($member->is_login()) {
            $_data = $_POST;
            $_data["wmem_username"] = $_COOKIE["wmem_username"];
            $member->updateUser($_data);
            header("location: ?msg=complete");
        }
        break;
}

/**** Head Tag ****/
$headtag = "";
$headtag .= $smarty->fetch("board/views/headtag.tpl");
$headtag .= $smarty->fetch("board/views/profile_headtag.tpl");
$smarty->assign("headtag", $headtag);
/**** End Head Tag ****/

/**** Content ****/
$content = "";

// Breadcrumb
$smarty->assign("breadcrumb", array("กระดานถามตอบ" => "/board"));

// Sub Head
//$smarty->assign("_head", $smarty->fetch("board/views/head.tpl"));

// Sub Content
# Profile
$smarty->assign("do", "edit");
if(@$_GET["msg"] == "complete") {
    $smarty->assign("msg", "บันทึกข้อมูลเรียบร้อยแล้ว");
}
$smarty->assign("profile", $member->getDetail($_COOKIE["wmem_username"]));
$smarty->assign("_content", $smarty->fetch("board/views/profile.tpl"));

// Sidebar
$_sidebar = "";
$_sidebar .= '<div class="sidebar-panel">'.$smarty->fetch("board/views/login.tpl").'</div>';
require "./sidebar.inc.php";

$content .= $smarty->fetch("board/layout_main.tpl");
$smarty->assign("content", $content);
/**** End Content ****/

/**** Scripts ****/
$smarty->assign("scripts", $smarty->fetch("board/views/profile_js.tpl"));
/**** End Scripts ****/

$smarty->assign("title", "แก้ไขข้อมูลส่วนตัว");
$smarty->assign("subject", "แก้ไขข้อมูลส่วนตัว");
$smarty->assign("keywords", "");
$smarty->assign("description", "");

$smarty->display(THEME.".tpl");

/**** End of file ****/