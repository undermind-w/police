<?php
require "./config.inc.php";
require PATH."/class/Board.php";
require PATH."/class/MemberBoard.php";

$db = new MysqlHelper();
$board = new Board($db);
$member = new MemberBoard($db);

$forum_id = isset($_GET["id"]) ? $_GET["id"] : "";
$do = isset($_REQUEST["do"]) ? $_REQUEST["do"] : "";
switch($do) {
	case "insert":
        if($member->is_login()) {
            $insert_id = $board->insertTopic($_POST);
            header("location: ?id=".$forum_id."&topic_id=".$insert_id."&do=complete");
        }
        break;
}

/**** Head Tag ****/
$headtag = "";
$headtag .= $smarty->fetch("board/views/headtag.tpl");
$headtag .= $smarty->fetch("board/views/topic_post_headtag.tpl");
$smarty->assign("headtag", $headtag);
/**** End Head Tag ****/

/**** Content ****/
$content = "";

// Breadcrumb
$smarty->assign("breadcrumb", array(
    "กระดานถามตอบ" => "/board",
    "ตั้งกระทู้ใหม่" => "/board/topic/new"
));

$do = isset($_GET["do"]) ? $_GET["do"] : "";
switch($do) {
	case "complete":
        $smarty->assign("topic_url", "/board/topic/".$_GET["topic_id"]);
		$content .= $smarty->fetch("board/views/topic_post_complete.tpl");
		break;
	default:
        $smarty->assign("do", "insert");
        $smarty->assign("forums", $board->getForumEntries());
        $smarty->assign("topic", array(
            "forum_id" => $forum_id,
            "wmem_name" => isset($_COOKIE["wmem_name"]) ? $_COOKIE["wmem_name"] : "",
            "wmem_picture" => "/images/board/member/".(isset($_COOKIE["wmem_picture"]) ? $_COOKIE["wmem_picture"] : "")
        ));
		$content .= $smarty->fetch("board/views/topic_post.tpl");
}

$smarty->assign("content", $content);
/**** End Content ****/

/**** Scripts ****/
$smarty->assign("scripts", $smarty->fetch("board/views/topic_post_js.tpl"));
/**** End Scripts ****/

$smarty->assign("title", "ตั้งกระทู้ใหม่");
$smarty->assign("subject", "กระดานถามตอบ");
$smarty->assign("keywords", "");
$smarty->assign("description", "");

$smarty->display(THEME.".tpl");

/**** End of file ****/