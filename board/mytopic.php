<?php
require "./authen.inc.php";
require "./config.inc.php";
require PATH."/class/Board.php";
require PATH."/class/Page.php";

$smarty->assign("is_newwindows", TRUE);

$db = new MysqlHelper();
$board = new Board($db);
$page = new Page();

$forum_id = "";

/**** Head Tag ****/
$smarty->assign("headtag", $smarty->fetch("board/views/headtag.tpl"));
/**** End Head Tag ****/

/**** Content ****/
$content = "";

// Breadcrumb
$smarty->assign("breadcrumb", array("กระดานถามตอบ" => "/board"));

// Sub Head
//$smarty->assign("_head", $smarty->fetch("board/views/head.tpl"));

// Sub Content
# New Topic
$_param = array(
	"pagenum" => 10,
	"showlist" => 50,
	"link_param" => "",
	"page" => isset($_GET["page"]) ? $_GET["page"] : 1,
	"row_count" => $board->getTopicCountByUserID($_COOKIE["wmem_id"])
);
$_pages = $page->getPage($_param);
$smarty->assign("pages", $_pages);
$smarty->assign("entries", $board->getTopicEntriesByUserID($_pages["offset"], $_pages["showlist"], $_COOKIE["wmem_id"]));
$smarty->assign("_topic_list", $smarty->fetch("board/views/topic_list1_page.tpl"));
$smarty->assign("_content", $smarty->fetch("board/views/index.tpl"));

// Sidebar
$_sidebar = "";
$_sidebar .= '<div class="sidebar-panel">'.$smarty->fetch("board/views/login.tpl").'</div>';
require "./sidebar.inc.php";

$content .= $smarty->fetch("board/layout_main.tpl");
$smarty->assign("content", $content);
/**** End Content ****/

/**** Scripts ****/
$smarty->assign("scripts", $smarty->fetch("board/views/index_js.tpl"));
/**** End Scripts ****/

$smarty->assign("title", "กระทู้ของฉัน");
$smarty->assign("subject", "กระทู้ของฉัน");
$smarty->assign("keywords", "");
$smarty->assign("description", "");

$smarty->display(THEME.".tpl");

/**** End of file ****/