<?php
require "../config.inc.php";
require PATH."/class/Board.php";
require PATH."/class/MemberBoard.php";
require PATH."/class/Page.php";

$db = new MysqlHelper();
$board = new Board($db);
$member = new MemberBoard($db);
$page = new Page();

header("Content-Type: text/html; charset=UTF-8");

switch($_REQUEST["do"]) {
	case "get_comment":
		$page = new Page();
		$_param = array(
			"pagenum" => 10,
			"showlist" => 20,
			"page" => $_POST["page"],
			"row_count" => $board->getPostCountByTopicID($_POST["topic_id"])
		);
		$_pages = $page->getPage($_param);
		$smarty->assign("posts", $board->getPostEntriesByTopicID($_pages["offset"] + $_POST["offset"], $_pages["showlist"], $_POST["topic_id"]));
		echo json_encode(array(
			"status" => "TRUE",
			"html" => $smarty->fetch("board/views/post_list.tpl"),
			"is_last" => ($_POST["page"] >= $_pages["page_count"] ? TRUE : FALSE)
		));
		break;	################################ END get_comment ################################
	case "add_comment":
		if($member->is_login()) {
			$post_detail = $_POST["txt_comment"];
			
			$sql = "SELECT rep_id, rep_detail
						  FROM cmp_webboard_reply
						  WHERE topic_id = '".$_POST["topic_id"]."' AND rep_wmem_id = '".$_COOKIE["wmem_id"]."'
						  ORDER BY rep_id DESC
						  LIMIT 1";
			$_row = $db->getRow($sql);
			if($_row["rep_detail"] == $post_detail) {
				echo json_encode(array(
					"status" => "FALSE"
				));
				exit();
			}
			
			$is_show = 1;
			
			$_data = $_POST;
			$_data["rep_detail"] = $post_detail;
			$_data["rep_status"] = $is_show;
			$post_id = $board->insertPost($_data);
			$board->updateStatTopicReply($_POST["topic_id"]);
			
			$smarty->assign("posts", $board->getPostEntriesByTopicID(0, 1, $_POST["topic_id"], $post_id, TRUE));
			
			echo json_encode(array(
				"status" => "TRUE",
				"post_list" => $smarty->fetch("board/views/post_list.tpl"),
				"is_show" => $is_show
			));
		}
		else {
			echo json_encode(array(
				"status" => "FALSE"
			));
		}
		break;	################################ END add_comment ################################
	case "show_post_content":
		$smarty->assign("do", "edit_content");
		$topic = $board->getTopic($_POST["id"]);
		$smarty->assign("topic", $topic);
		$smarty->assign("forums", $board->getForumEntries());
		
		echo json_encode(array(
			"status" => "TRUE",
			"html" => $smarty->fetch("board/views/topic_post.tpl")
		));
		break;	################################ END show_post_content ################################
	case "edit_content":
		$sql = "SELECT topic_id
					  FROM cmp_webboard_topic
					  WHERE topic_id = '".$_POST["edit_id"]."' AND topic_wmem_id = '".$_COOKIE["wmem_id"]."'";
		$_rs = $db->getResultSet($sql);
		if($_row = $_rs->fetch_assoc() || $member->is_admin()) {
			$_data = $_POST;
			$_data["id"] = $_POST["edit_id"];
			$board->updateTopic($_data);
			
			$smarty->assign("topic", $board->getTopic($_POST["edit_id"]));
			
			echo "<textarea>".json_encode(array(
				"status" => "TRUE",
				"html" => $smarty->fetch("board/views/topic_detail.tpl")
			))."</textarea>";
		}
		else {
			echo json_encode(array(
				"status" => "FALSE"
			));
		}
		break;	################################ END edit_content ################################
	case "cancel_edit_content":
		$smarty->assign("topic", $board->getTopic($_POST["edit_id"]));
		
		echo json_encode(array(
			"status" => "TRUE",
			"html" => $smarty->fetch("board/views/topic_detail.tpl")
		));
		break;	################################ END cancel_edit_content ################################
	case "delete_content":
		$sql = "SELECT topic_id
					  FROM cmp_webboard_topic
					  WHERE topic_id = '".$_POST["id"]."' AND topic_wmem_id = '".$_COOKIE["wmem_id"]."'";
		$_rs = $db->getResultSet($sql);
		if($_row = $_rs->fetch_assoc() || $member->is_admin()) {
			$board->hideTopic($_POST["id"]);
			
			echo json_encode(array(
				"status" => "TRUE"
			));
		}
		else {
			echo json_encode(array(
				"status" => "FALSE"
			));
		}
		break;	################################ END delete_content ################################
	case "delete_comment":
		$sql = "SELECT rep_id
					  FROM cmp_webboard_reply
					  WHERE rep_id = '".$_POST["id"]."' AND rep_wmem_id = '".$_COOKIE["wmem_id"]."'";
		$_rs = $db->getResultSet($sql);
		if($_row = $_rs->fetch_assoc() || $member->is_admin()) {
			$board->hidePost($_POST["id"]);
			$board->updateStatTopicReplyDelete($_POST["topic_id"]);
			
			echo json_encode(array(
				"status" => "TRUE",
				"id" => $_POST["id"]
			));
		}
		else {
			echo json_encode(array(
				"status" => "FALSE"
			));
		}
		break;	################################ END delete_comment ################################
}

/* End of File */