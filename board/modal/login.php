<?php
session_start();
require "../config.inc.php";
require PATH."/class/MemberBoard.php";

$db = new MysqlHelper();
$member = new MemberBoard($db);

$type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "";

if($_POST) {
	$status_msg = array(
		-2 => "ท่านยังไม่ได้ยืนยันการสมัครสมาชิกในอีเมล์",
		-1 => "ท่านได้ทำผิดเงื่อนไขการใช้บริการเว็บไซต์",
		0 => "อีเมล์/รหัสผ่าน ไม่ถูกต้อง กรุณาลองอีกครั้ง",
		1 => ""
	);
	
	$status = $member->loginWeb($_POST["user"], $_POST["password"]);
	$msg = $status_msg[$status];
	
	if($status == 1) {
		if($type == "fancybox") {
			echo('<script>parent.postMessage("close", "*");</script>');
		}
		else {
			echo("<script>top.location.href='".$_SERVER["REQUEST_URI"]."'</script>");
		}
		exit();
	}
}

if(!empty($_COOKIE["wmem_username"])) {
	if($type == "fancybox") {
		echo('<script>parent.postMessage("close", "*");</script>');
		exit();
	}
	
	if(empty($_REQUEST["ReturnUrl"])) {
		echo("<script>top.location.href='/'</script>");
	}
	else {
		echo("<script>top.location.href='".urldecode($_REQUEST["ReturnUrl"])."'</script>");
	}
	exit();
}

if(!empty($_COOKIE["facebook_email"])) {
	echo("<script>location.href='register.php?type=facebook'</script>");
}
else if(!empty($_COOKIE["google_email"])) {
	echo("<script>location.href='register.php?type=google'</script>");
}

$smarty->assign("title", "เข้าสู่ระบบ");
$smarty->assign("subtitle", "ถามมา-ตอบไป สหกรณ์ข้าราชการสหกรณ์ จำกัด");
$smarty->assign("keywords", "เข้าสู่ระบบ ถามมา-ตอบไป สหกรณ์ข้าราชการสหกรณ์ จำกัด");

ob_start(); ?>
<link type="text/css" rel="stylesheet" href="login.css" />
<script type="text/javascript" src="/scripts/utility.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("input, textarea").placeholder();
		
		$(document).on("click", ".login_facebook", function() {
			var callbackFunction = $(this).attr("href").substring($(this).attr("href").indexOf("#") + 1);
			facebookLogin(callbackFunction);
			return false;
		});
		
		var facebookLoginWindow;
		var facebookLoginWindowTimer;
		var facebookLoginFunctionCallback;
		function facebookLogin(callbackFunction) {
			facebookLoginFunctionCallback = callbackFunction;
			var popupWidth = 700;
			var popupHeight = 380;
			var xPosition = ($(window).width() - popupWidth)/2;
			var yPosition = ($(window).height() - popupHeight)/2;
			var loginUrl = '/board/modal/login_facebook.php';
			
			facebookLoginWindow = window.open(loginUrl, "facebookLoginWindow", 
				"location=1,scrollbars=1," +
				"width=" + popupWidth + ",height=" + popupHeight + "," +
				"left=" + xPosition + ",top=" + yPosition);
			facebookLoginWindowTimer = setInterval(facebookLoginWindowOnTimerCallback, 1000);
		}
		
		function facebookLoginWindowOnTimerCallback() {
			if(facebookLoginWindow.closed) {
				clearInterval(facebookLoginWindowTimer);
				if(getCookie("wmem_username") != null) {
					if(facebookLoginFunctionCallback != "") {
						window[facebookLoginFunctionCallback]();
					}
					else {
						location.reload(true);
					}
				}
				else if(getCookie("facebook_email") != null) {
					if(facebookLoginFunctionCallback != "") {
						window[facebookLoginFunctionCallback]();
					}
					else {
						location.reload(true);
					}
				}
			}
		}
		
		$(document).on("click", ".login_google", function() {
			var callbackFunction = $(this).attr("href").substring($(this).attr("href").indexOf("#") + 1);
			googleLogin(callbackFunction);
			return false;
		});
		
		var googleLoginWindow;
		var googleLoginWindowTimer;
		var googleLoginFunctionCallback;
		function googleLogin(callbackFunction) {
			googleLoginFunctionCallback = callbackFunction;
			var popupWidth = 700;
			var popupHeight = 450;
			var xPosition = ($(window).width() - popupWidth)/2;
			var yPosition = ($(window).height() - popupHeight)/2;
			var loginUrl = '/board/modal/login_google.php';
			
			googleLoginWindow = window.open(loginUrl, "googleLoginWindow", 
				"location=1,scrollbars=1," +
				"width=" + popupWidth + ",height=" + popupHeight + "," +
				"left=" + xPosition + ",top=" + yPosition);
			googleLoginWindowTimer = setInterval(googleLoginWindowOnTimerCallback, 1000);
		}
		
		function googleLoginWindowOnTimerCallback() {
			if(googleLoginWindow.closed) {
				clearInterval(googleLoginWindowTimer);
				if(getCookie("wmem_username") != null) {
					if(googleLoginFunctionCallback != "") {
						window[googleLoginFunctionCallback]();
					}
					else {
						location.reload(true);
					}
				}
				else if(getCookie("google_email") != null) {
					if(googleLoginFunctionCallback != "") {
						window[googleLoginFunctionCallback]();
					}
					else {
						location.reload(true);
					}
				}
			}
		}
	});
</script>
<?php
$taghead = ob_get_contents();
ob_end_clean();
$smarty->assign("taghead", $taghead);

ob_start(); ?>
<div id="wrap">
	<div id="social">
		<a id="login_facebook" class="login_facebook button1" href="#"><i class="fa fa-facebook"></i> Login โดยใช้บัญชี Facebook</a>
		<a id="login_google" class="login_google button2" href="#"><i class="fa fa-google-plus"></i> Login โดยใช้บัญชี Google</a>
	</div>
	
	<div id="login">
		<h3 class="title1">กรณีสมัครด้วยการกรอกข้อมูล Login ที่นี่</h3>
		<form id="frmLogin" method="post" action="" class="form-horizontal">
			<div class="form-group">
				<div class="col-sm-12">
					<input type="text" id="user" name="user" value="" class="form-control required" title="อีเมล์" placeholder="อีเมล์" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<input type="password" id="password" name="password" value="" class="form-control required" title="รหัสผ่าน" placeholder="รหัสผ่าน" />
				</div>
			</div>
			<div class="text-right"><a href="forgotpass.php">ลืมรหัสผ่านคลิกที่นี่</a></div>
			<?php if(!empty($msg)) : ?><div class="msg1"><?php echo $msg; ?></div><?php endif; ?>
			<input type="submit" id="btnLogin" name="btnLogin" value="เข้าสู่ระบบ" class="button3" />
			<div class="text-right"><a href="register.php">หากยังไม่ได้สมัครสมาชิกคลิกที่นี่</a></div>
		</form>
	</div>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);

$smarty->display("board/modal1.tpl");
?>