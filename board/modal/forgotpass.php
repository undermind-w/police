<?php
session_start();
require "../config.inc.php";
require PATH."/class/MemberBoard.php";

$db = new MysqlHelper();
$member = new MemberBoard($db);

if($_POST) {
	// Mail Start
	$msg = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr> 
						<td colspan=\"3\" height=\"100\" bgcolor=\"#FFFFFF\">
							<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">
								<tr>
									<td><strong>Username และ Password ของคุณคือ</strong></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>";
	
	$sql = "SELECT * FROM cmp_webboard_member WHERE wmem_email = '".htmlentities($_POST["email"], ENT_QUOTES)."'";
	$rsForget = $db->getResultSet($sql);
	$is_true = false;
	while($rowForget = $rsForget->fetch_assoc()) {
		$is_true = true;
		$msg .= "			<tr>
										<td><strong>Username : ".$rowForget["wmem_username"]."</strong></td>
									</tr>
									<tr>
										<td><strong>Password : ".$rowForget["wmem_password"]."</strong></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>";
	}
	
	$msg .= "	</table>
						</td>
					  </tr>
					</table>";
	
	$subject = "รหัสผ่านสำหรับกระดานถามตอบ cmcoop.or.th";
	if($is_true) {
		@send_mj_mail($subject, $msg, $_POST["email"]);
	}
	else {
		$msg = "ไม่พบอีเมล์นี้ในระบบ";
	}
	// Mail End
}

$smarty->assign("title", "ลืมรหัสผ่าน Eduzones");
$smarty->assign("keywords", "ลืมรหัสผ่าน Eduzones");

ob_start(); ?>
<link type="text/css" rel="stylesheet" href="forgotpass.css" />
<script type="text/javascript" src="/scripts/utility.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("input, textarea").placeholder();
	});
</script>
<?php
$taghead = ob_get_contents();
ob_end_clean();
$smarty->assign("taghead", $taghead);

ob_start();
if(@$is_true) : ?>
	<div class="msg2">
		ระบบได้ส่งรหัสผ่านไปยังอีเมล์ของท่านแล้ว<br />
		ซึ่งอาจอยู่ใน junk mail หากท่านไม่ได้รับอีเมล์ภายใน 24 ชั่วโมง<br />
		ให้แจ้งมาที่เมล์ coopcgd@hotmail.com 
	</div>
<?php else : ?>
	<div id="wrap">
		<form id="frmLogin" method="post" action="" class="form-horizontal">
			<h3 class="title1">ระบบจะจัดส่งรหัสไปยังอีเมล์ที่ท่านสมัครไว้</h3>
			<div class="form-group">
				<div class="col-sm-12">
					<input type="text" id="email" name="email" value="" class="form-control required" title="อีเมล์" placeholder="อีเมล์" />
				</div>
			</div>
			<?php if(@$msg) : ?><div class="msg1"><?php echo $msg; ?></div><?php endif; ?>
			<input type="submit" id="btnSubmit" name="btnSubmit" value="ส่งรหัสผ่านไปยังอีเมล์" class="button3" />
		</form>
	</div>
<?php endif;

$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);

$smarty->display("board/modal1.tpl");
?>