<?php
session_start();
require "../config.inc.php";
require PATH."/class/MemberBoard.php";

$db = new MysqlHelper();
$member = new MemberBoard($db);

if($_POST) {
	$type = isset($_POST["type"]) ? $_POST["type"] : "";
	$password = isset($_POST["password"]) ? $_POST["password"] : "";
	
	$wmem_confirm = 0;
	if(($type == "facebook" || $type == "google") && (isset($_COOKIE["facebook_email"]) || isset($_COOKIE["google_email"]))) {
		$no_password = FALSE;
		$wmem_confirm = 1;
	}
	else if(trim($password) == "") {
		$no_password = TRUE;
	}
	else {
		$no_password = FALSE;
	}
	
	if(trim($_POST["fullname"]) == ""
		|| trim($_POST["name"]) == ""
		|| trim($_POST["email"]) == ""
		|| $no_password
		|| trim($_POST["member_picture"]) == "")
	{
		echo '<script type="text/javascript">alert("กรุณาป้อนข้อมูลให้ครบถ้วน"); location.href="register.php?type='.$type.'";</script>';
		exit();
	}
	
	$data = array(
		"wmem_type_id" => "1",
		"wmem_username" => $_POST["email"],
		"wmem_password" => $password,
		"wmem_fullname" => $_POST["fullname"],
		"wmem_name" => $_POST["name"],
		"wmem_email" => $_POST["email"],
		"wmem_picture" => $_POST["member_picture"],
		"wmem_confirm" => $wmem_confirm,
		"wmem_status" => "1"
	);
	$member->addUser($data);
	
	if($type == "facebook" || $type == "google") {
		$domain = $_SERVER["HTTP_HOST"];
		$domain =  substr_count($domain, ".") > 1 ? substr($domain, strpos($domain, ".")) : $domain;
		setcookie("facebook_email", "", time() - 3600, "/", $domain);
		setcookie("google_email", "", time() - 3600, "/", $domain);
		
		header("location: register_complete.php?type=".$type."&email=".$_POST["email"]);
	}
	else {
		$member->sendConfirmMail($data);
		header("location: register_precomplete.php");
	}
}

$smarty->assign("title", "สมัครสมาชิก");
$smarty->assign("subtitle", "สหกรณ์ออมทรัพย์ครูเชียงใหม่");
$smarty->assign("keywords", "สมัครสมาชิก");

ob_start(); ?>
<link type="text/css" rel="stylesheet" href="register_precomplete.css" />
<?php
$taghead = ob_get_contents();
ob_end_clean();
$smarty->assign("taghead", $taghead);

ob_start(); ?>
<div class="msg">
	<h2>การสมัครใกล้เสร็จสมบูรณ์แล้ว เหลือเพียงขั้นตอนเดียวเท่านั้น</h2>
	กรุณาตรวจสอบอีเมล์ของท่าน แล้วทำตามขั้นตอนที่ระบุในอีเมล์เพื่อยืนยันการสมัครสมาชิก<br />
	หากไม่ได้รับอีเมล์รบกวนตรวจสอบใน junk mail (จดหมายขยะ) และหากไม่ได้รับอีเมล์ภายใน 24 ชั่วโมง<br />
	สามารถติดต่อทีมงาน cpd.upbean.co.th เพื่อขอเปิดใช้งานได้ที่ coopcgd@hotmail.com 
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);

$smarty->display("board/modal1.tpl");
?>