<?php
session_start();
require "../config.inc.php";
require PATH."/class/MemberBoard.php";

$db = new MysqlHelper();
$member = new MemberBoard($db);

$error = false;
$type = isset($_GET["type"]) ? $_GET["type"] : "";
if($type == "facebook" || $type == "google") {
	if(!$member->is_block($_GET["email"])) {
		$member->login($_GET["email"]);
		$rowMember = $member->getDetail($_GET["email"]);
		$rowMember["type"] = $type;
		$member->sendCompleteMail($rowMember);
	}
}
else {
	if($_GET["code"] == md5($_GET["email"]."active#r8x1k5s1v8a5txz4")) {
		$member->activeUser($_GET["email"]);
		
		if(!$member->is_block($_GET["email"])) {
			$member->login($_GET["email"]);
			$rowMember = $member->getDetail($_GET["email"]);
			$rowMember["type"] = "web";
			$member->sendCompleteMail($rowMember);
		}
	}
	else {
		$error = true;
	}
}

$smarty->assign("title", "สมัครสมาชิก");
$smarty->assign("subtitle", "สหกรณ์ออมทรัพย์ครูเชียงใหม่");
$smarty->assign("keywords", "สมัครสมาชิก");

ob_start(); ?>
<link type="text/css" rel="stylesheet" href="register_complete.css" />
<?php
$taghead = ob_get_contents();
ob_end_clean();
$smarty->assign("taghead", $taghead);

ob_start(); ?>
<div class="msg">
	<?php if($error) : ?>
		การยืนยันการสมัครสมาชิกไม่ถูกต้องกรุณาตรวจสอบ
	<?php else : ?>
		ขอบคุณค่ะ ท่านได้สมัครสมาชิก กระดานถามตอบ cmcoop.or.th เรียบร้อยแล้ว<br />
		<a class="button1" href="/board" target="_top">คลิกที่นี่เพื่อใช้งานกระดานถามตอบ</a>
	<?php endif; ?>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);

$smarty->display("board/modal1.tpl");
?>