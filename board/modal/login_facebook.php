<?php
session_start();
require "../config.inc.php";
require PATH."/class/MemberBoard.php";

// require PATH.'/class/facebook-php-sdk/src/facebook.php';
require PATH . "/class/php-graph-sdk-5.5/src/Facebook/autoload.php" ; 

$db = new MysqlHelper();
$member = new MemberBoard($db);

$app_id = $fbConfig["app_id"];
$app_secret = $fbConfig["app_secret"];
$my_url = 'http://'.$_SERVER["HTTP_HOST"].'/board/modal/login_facebook.php';

/*
$facebook = new Facebook(array(
	'appId'  => $app_id,
	'secret' => $app_secret,
	'cookie' => true,
));
*/
$facebook = new Facebook\Facebook( array(
	'app_id' => $app_id ,
	'app_secret' => $app_secret 
));


$error_reason = isset($_REQUEST["error_reason"]) ? $_REQUEST["error_reason"] : "";
if($error_reason == "user_denied") {
	echo('<script>window.close();</script>');
	exit();
}

// $user = $facebook->getUser();
 
$helper = $helper = $facebook->getRedirectLoginHelper();
$user = $helper->getAccessToken() ; 

if($user) {
	try {
		$response = $facebook->get('/me?fields=id,name,email' , $user );
		$user_profile = $response->getGraphUser();
	 
		$member->loginFacebook($user_profile);
		echo('<script>window.close();</script>');
		exit();
	}
	catch(FacebookApiException $e) {
		error_log($e);
		$user = null;
		sleep(1);
		echo('<script>location.reload();</script>');
	}
}
else {
	/*
	$params = array(
		'scope' => 'email',
		'redirect_uri' => $my_url,
		'display' => 'popup'
	);
	
	$loginUrl = $facebook->getLoginUrl($params);
	*/
	
	
	$loginUrl = $helper->getLoginUrl( $my_url , array( 'email'));
	echo('<script>location.href="'.$loginUrl.'";</script>');
	
	exit();
}
?>