<?php
session_start();
require "../config.inc.php";

$type = isset($_GET["type"]) ? $_GET["type"] : "";
if($type == "facebook") {
	if(isset($_COOKIE["facebook_email"])) {
		$fullname = $_COOKIE["facebook_name"];
		$email = $_COOKIE["facebook_email"];
	}
	else {
		header("location: login.php?type=fancybox");
	}
}
else if($type == "google") {
	if(isset($_COOKIE["google_email"])) {
		$fullname = $_COOKIE["google_name"];
		$email = $_COOKIE["google_email"];
	}
	else {
		header("location: login.php?type=fancybox");
	}
}
else {
	$fullname = "";
}

$smarty->assign("title", "สมัครสมาชิก");
$smarty->assign("subtitle", "กรุณากรอกข้อมูลเพิ่มเติม");
$smarty->assign("keywords", "สมัครสมาชิก");

ob_start(); ?>
<link href="/scripts/jquery-validation/css/validated2.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="register.css" />
<script type="text/javascript" src="/scripts/jquery-validation/jquery.validate.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("input, textarea").placeholder();
		
		$(".img").click(function() {
			$("#member_picture").val($(this).attr("alt"));
			$(".img").removeClass("alt");
			$(this).addClass("alt");
		});
		
		$.validator.addMethod('notPlaceholder', function(val, el) {
			return this.optional(el) || ( val !== $(el).attr('placeholder') );
		}, $.validator.messages.required);
		
		var container = $("div.vcontainer");
		var validator = $("#frmRegister").validate({
			ignore: [],
			invalidHandler: function(e, validator) {
				var errors = validator.numberOfInvalids();
				if(errors) {
					var message = "กรุณาป้อนข้อมูลให้ถูกต้องครบถ้วน";
					$("div.vcontainer").html(message);
					$("div.vcontainer").show();
				}
				else {
					$("div.vcontainer").hide();
				}
			},
			errorContainer: container,
			errorLabelContainer: $("ol", container),
			wrapper: "li",
			meta: "validate",
			showErrors: function(errorMap, errorList) {
				this.defaultShowErrors();
				//Placeholders.enable();
			},
			rules: {
				email: {
					notPlaceholder: true,
					email: true,
					remote: {
						type: "GET",
						url: "/board/ajax/chk_email.php",
						data: {
							email: function() {
								return $("#email").val();
							}
						},
						beforeSend: function(data) {
							$("#wait_email").css("display", "block");
							$("#email_ok").css("display", "none");
						},
						complete: function(data) {
							$("#wait_email").css("display", "none");
							if(data.responseText == "true") {
								$("#email_ok").css("display", "block");
								var validator = $("#frmRegister").validate();
								//validator.resetForm();
							}
						}
					}
				},
				password: {
					notPlaceholder: true,
					minlength: 4
				},
				repassword: {
					notPlaceholder: true,
					equalTo: "#password"
				}
			},
			messages: {
				email: {
					email: function() { $("div.vcontainer").html("อีเมล์ไม่ถูกต้อง"); },
					remote: function() { $("div.vcontainer").html("อีเมล์ซ้ำ"); }
				},
				password: {
					minlength: function() { $("div.vcontainer").html("รหัสผ่านต้องมีอย่างน้อย 4 ตัวอักษร"); }
				},
				repassword: {
					equalTo: function() { $("div.vcontainer").html("ยืนยันรหัสผ่านไม่เหมือนกับรหัสผ่าน"); }
				},
				member_picture: {
					required: function() { $("div.vcontainer").html("เลือกภาพประจำตัว"); }
				}
			}
		});
	});
</script>
<?php
$taghead = ob_get_contents();
ob_end_clean();
$smarty->assign("taghead", $taghead);

ob_start(); ?>
<div id="register">
	<form id="frmRegister" method="post" action="register_precomplete.php" class="form-horizontal">
		<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" />
		
		<div class="vcontainer">
			<ol></ol>
		</div>
		
		<div class="form-group">
			<div class="col-sm-12">
				<input type="text" id="fullname" name="fullname" value="<?php echo $fullname; ?>" class="form-control required" title="ชื่อ - สกุล" placeholder="ชื่อ - สกุล" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<input type="text" id="name" name="name" value="" class="form-control required" title="ชื่อที่ใช้แสดงผล" placeholder="ชื่อที่ใช้แสดงผล" />
			</div>
		</div>
		<?php if(!$type) : ?>
			<div id="wait_email" style="display: none;">
				<div style="border: solid 1px #81e765; background: #d2ffad; text-align: center; margin-bottom: 4px;">
					กำลังตรวจสอบอีเมล์...
					<img src="/images/board/wait2.gif" alt="wait" />
				</div>
			</div>
			<div id="email_ok" style="display: none;">
				<div style="border: solid 1px #81e765; background: #d2ffad; text-align: center; margin-bottom: 4px;">อีเมล์นี้สามารถใช้งานได้</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<input type="text" id="email" name="email" value="" class="form-control required" title="อีเมล์" placeholder="อีเมล์" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<input type="password" id="password" name="password" value="" class="form-control required" title="กำหนดรหัสผ่านเข้าสู่ระบบ" placeholder="กำหนดรหัสผ่านเข้าสู่ระบบ" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<input type="password" id="repassword" name="repassword" value="" class="form-control required" title="ป้อนรหัสผ่านอีกครั้ง" placeholder="ป้อนรหัสผ่านอีกครั้ง" />
				</div>
			</div>
		<?php else : ?>
			<input type="hidden" id="email" name="email" value="<?php echo $email; ?>" />
		<?php endif; ?>
		<div class="form-group">
			<div class="col-sm-12">
				เลือกภาพประจำตัว
				<input type="hidden" id="member_picture" name="member_picture" value="" class="required" />
				<ul id="img_list">
					<?php for($i = 1; $i <= 5; $i++) : ?>
						<li><img class="img" src="/images/board/member/<?php echo $i; ?>.jpg" alt="<?php echo $i; ?>.jpg" width="55" height="55" /></li>
					<?php endfor; ?>
				</ul>
			</div>
		</div>
		
		<div id="cond" class="box1">
			เงื่อนไขการใช้กระดานถามตอบ
			
			1. ห้ามใช้ข้อความอันเป็นการวิพากษ์วิจารณ์ สถาบันพระมหากษัตริย์ และราชวงศ์ <br />
			2. ห้ามใช้ข้อความ คำหยาบคาย ลามก อนาจาร ทะลึ่ง เกินที่สุภาพชนทั่วไปจะรับได้ (ถ้าข้อคิดเห็นของท่านมีเหตุผล น่าสนใจ แต่มีบางคำไม่เหมาะสมแฝงอยู่ ก็จะโดนลบทิ้ง) <br />
			3. ห้ามใช้ข้อความ ที่มีเจตนา ใส่ร้าย-ใส่ความ บุคคลอื่น อย่างชัดแจ้ง และ ไร้เหตุผลรองรับ <br />
			4. ห้ามใช้ข้อความ ท้าทาย ชักชวน โดยมีเจตนาก่อให้เกิดการทะเลาะวิวาท หรือความวุ่นวายขึ้น <br />
			5. ห้ามใช้ข้อความกล่าวโจมตี หรือวิพากษ์วิจารณ์ในทางเสียหายต่อ ศาสนา หรือคำสอนต่างๆ <br />
			6. ห้ามใช้ชื่อ / นามแฝงอันเป็นชื่อจริงของผู้อื่น โดยมีเจตนาทำให้สาธารณะชนเข้าใจผิด และเจ้าของชื่อผู้นั้นได้รับความเสียหาย หรือเสื่อมเสียชื่อเสียง <br />
			7. ห้ามใช้ข้อความที่พิมพ์ซ้ำๆ ติดกันจำนวนมาก ทำให้หน้าเว็บไม่เป็นระเบียบ <br />
			8. ห้ามใช้ข้อความที่พิมพ์ ข้อมูลส่วนตัวของผู้อื่น เช่น ชื่อจริง เบอร์โทร อีเมล์หรือเบอร์โทรศัพท์ โดยมีเจตนากลั่นแกล้ง ให้เกิดความเสียหาย <br />
			9. ห้ามใช้ข้อความโฆษณาชวนเชื่อทุกกรณี ไม่ว่าจะเป็น โฆษณาเว็บ, สินค้า, บริการ, กลวิธีการหารายได้ทั้งหลาย <br />

			<br />
			ประกาศการใช้งานถามมา - ตอบไป <br />
			เนื่องจาก พระราชบัญญัติว่าด้วยการกระทำผิดเกี่ยวกับคอมพิวเตอร์ พ.ศ. ๒๕๕๐ ได้มีผลบังคับใช้เมื่อวันที่ 18 กรกฎาคม 2550 สหกรณ์ข้าราชการสหกรณ์ จำกัด จึงได้ปรับปรุงระบบ ถามมา-ตอบไป เพื่อให้เป็นไปตามพระราชบัญญัติดังกล่าว ดังนั้นเมื่อมีการตั้งคำถามหรือแสดงความคิดเห็น คำถามและความคิดเห็นทั้งหมดจะถูกกรองก่อนทุกครั้งก่อนขึ้นเว็บไซต์ จึงเรียนมาเพื่อโปรดทราบ สหกรณ์ข้าราชการสหกรณ์ จำกัด จะทำการ update คำถามและความคิดเห็นให้เร็วที่สุด จึงขออภัยมา ณ โอกาสนี้ด้วย หากเกิดความไม่สะดวกในการใช้งาน
			สหกรณ์ขอสงวนสิทธิ์ในการที่จะย้าย/ลบ กระทู้หรือคำตอบ หากกระทู้ดังกล่าวตั้งหรือตอบผิดหมวดที่ตั้งไว้ โดยไม่ต้องแจ้งล่วงหน้า และขอความกรุณาตั้งกระทู้หรือตอบกระทู้ให้ตรงตามหมวดที่จัดไว้

		</div>
		
		<input type="submit" id="btnRegister" name="btnRegister" value="บันทึก" class="button3" />
	</form>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);

$smarty->display("board/modal1.tpl");
?>