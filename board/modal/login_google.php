<?php
session_start();
require "../config.inc.php";
require PATH."/class/MemberBoard.php";
require PATH.'/class/google-api-php-client/src/Google/autoload.php';

$db = new MysqlHelper();
$member = new MemberBoard($db);

$client = new Google_Client();
$client->setApplicationName($glConfig["application_name"]);
$client->setClientId($glConfig["oauth2_client_id"]);
$client->setClientSecret($glConfig["oauth2_client_secret"]);
$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
$client->setScopes(array(
	'https://www.googleapis.com/auth/userinfo.profile',
	'https://www.googleapis.com/auth/userinfo.email'
));
$oauth2 = new Google_Service_Oauth2($client);

if(isset($_GET['code'])) {
	$client->authenticate($_GET['code']);
	$_SESSION['access_token'] = $client->getAccessToken();
	$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
	header('location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

if(isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	$client->setAccessToken($_SESSION['access_token']);
}
else {
	$authUrl = $client->createAuthUrl();
}

if($client->getAccessToken()) {
	try {
		$user = $oauth2->userinfo->get();
		$_SESSION['access_token'] = $client->getAccessToken();
		$member->loginGoogle($user);
		echo('<script>window.close();</script>');
	}
	catch(Exception $ex) {
		$authUrl = $client->createAuthUrl();
		header("location: ".$authUrl);
	}
}
else {
	header("location: ".$authUrl);
}
?>