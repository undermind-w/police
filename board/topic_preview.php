<?php
require "./config.inc.php";
require PATH."/class/Board.php";

$smarty->assign("is_newwindows", TRUE);

$db = new MysqlHelper();
$board = new Board($db);

// Topic Preview
$topic = $_POST;
$rowForum = $board->getForum($topic["forum_id"]);
$topic["forum_title"] = $rowForum["forum_title"];
$topic["wmem_picture"] = "/images/board/member/".$_COOKIE["wmem_picture"];
$topic["wmem_name"] = $_COOKIE["wmem_name"];
$topic["craetedate"] = date("Y-m-d h:i:s");
$topic["ip_cen"] = substr($_SERVER["REMOTE_ADDR"], 0, strrpos($_SERVER["REMOTE_ADDR"], ".")).".***";
$smarty->assign("topic", $topic);

$smarty->display("board/views/topic_preview.tpl");

/**** End of file ****/