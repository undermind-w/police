<?php
 

require "./config.inc.php";
require PATH."/class/Board.php";
require PATH."/class/Page.php";

$smarty->assign("is_newwindows", TRUE);

$db = new MysqlHelper();
$board = new Board($db);
$page = new Page();

$forum_id = isset($_GET["id"]) ? $_GET["id"] : "";

/**** Head Tag ****/
$smarty->assign("headtag", $smarty->fetch("board/views/headtag.tpl"));
/**** End Head Tag ****/

/**** Content ****/
$content = "";

// Breadcrumb
$smarty->assign("breadcrumb", array("กระดานถามตอบ" => "/board"));

// Sub Head
$smarty->assign("_head", $smarty->fetch("board/views/head.tpl"));

// Sub Content
# New Topic
$_param = array(
	"pagenum" => 10,
	"showlist" => 50,
	"link_param" => "&keyword=".@$_GET["keyword"],
	"page" => isset($_GET["page"]) ? $_GET["page"] : 1,
	"row_count" => $board->getTopicCountByForum($forum_id, @$_GET["keyword"])
);
$_pages = $page->getPage($_param);
$smarty->assign("pages", $_pages);
$smarty->assign("entries", $board->getTopicEntriesByForum($_pages["offset"], $_pages["showlist"], $forum_id, TRUE, @$_GET["keyword"]));
$smarty->assign("_topic_list", $smarty->fetch("board/views/topic_list1_page.tpl"));
$smarty->assign("_content", $smarty->fetch("board/views/index.tpl"));

// Sidebar
$_sidebar = "";
require "./sidebar.inc.php";

$content .= $smarty->fetch("board/layout_main.tpl");
$smarty->assign("content", $content);
/**** End Content ****/

/**** Scripts ****/
$smarty->assign("scripts", $smarty->fetch("board/views/index_js.tpl"));
/**** End Scripts ****/

$smarty->assign("title", "กระดานถามตอบ");
$smarty->assign("subject", "กระดานถามตอบ");
$smarty->assign("keywords", "");
$smarty->assign("description", "");

$smarty->display(THEME.".tpl");

/**** End of file ****/