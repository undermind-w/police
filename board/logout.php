<?php
session_start();
require "./config.inc.php";
require PATH."/class/MemberBoard.php";

$db = new MysqlHelper();
$member = new MemberBoard($db);

$member->logout();

if(empty($_REQUEST["ReturnUrl"])) {
	header("location: /");
}
else {
	header("location: ".urldecode($_REQUEST["ReturnUrl"]));
}
?>