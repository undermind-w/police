<?php
require_once "config.inc.php";
require_once "class/function.inc.php";
//echo $_POST["do"].'<hr>';
//echo $_POST["from"].'<hr>';
if($_POST["do"] == "poll-content") {
    ?>
    <form id="frm_poll" class="form-horizontal">
        <div class="text-center box-head"><h4>แบบสอบถามความคิดเห็น</h4></div>
        <div class="col-sm-12" style="padding-left: 0px;">
            แบบสำรวจความพึงพอใจเว็บไซต์
        </div>
        <div class="show_from">
            <div class="col-sm-12">&nbsp;</div>
            <div class="col-sm-12">
                <input type="radio" id="question1" name="question" value="1"> พึงพอใจมาก
            </div>
            <div class="col-sm-12">
                <input type="radio" id="question2" name="question" value="2"> พึงพอใจปานกลาง
            </div>
            <div class="col-sm-12">
                <input type="radio" id="question3" name="question" value="3"> พึงพอใจน้อย
            </div>
            <div class="col-sm-12 m-t-xxl m-b-xs text-center">
                <button type="button" id="btn-poll" class="btn btn-material-light-blue-300" style="background-color: #2AA70C;">แสดงความเห็น</button>
                <button type="button" id="btn-result" class="btn btn-material-light-blue-300" style="background-color: #2AA70C;">&nbsp;&nbsp;&nbsp;ผลลัพธ์&nbsp;&nbsp;&nbsp;</button>
            </div>
        </div>
        <div class="show_alert"></div>
    </form>
    <?php
    $response = ob_get_contents();
    ob_end_clean();
    echo $response;
    exit();
}else if($_POST["do"] == "cal_result") {
?>
    <form id="frm_result" class="form-horizontal">
        <div class="text-center box-head"><h4>แบบสอบถามความคิดเห็น</h4></div>
        <div class="col-sm-12" style="padding-left: 0px;">
            แบบสำรวจความพึงพอใจเว็บไซต์
        </div>
    <?php
        $sql_poll= "SELECT poll_question,COUNT(poll_question) AS count_question FROM cmp_poll GROUP BY poll_question" ;
        $rs_poll = $mysqli->query($sql_poll);
        echo $mysqli->error;
        $arr_type_question = array('1'=>'พึงพอใจมาก','2'=>'พึงพอใจปานกลาง','3'=>'พึงพอใจน้อย');
        $arr_class_color = array('1'=>'progress-bar-success','2'=>'progress-bar-warning','3'=>'progress-bar-danger');
        $data_poll = array();
        $sum_all = 0;
        while($row = $rs_poll->fetch_assoc()){
            $sum_all+=$row['count_question'];
            $data_poll[$row['poll_question']]['title'] = $arr_type_question[$row['poll_question']];
            $data_poll[$row['poll_question']]['value'] = $row['count_question'];
            $data_poll[$row['poll_question']]['class_color'] = $arr_class_color[$row['poll_question']];
        }



        foreach ($data_poll AS $key=>$value) {
            $percent = number_format($value['value']*100/$sum_all,0);
        ?>
            <div class="sp-poll-resul-item">
                <span class="poll-info">
                    <span class="poll-question"><?php echo $value['title'];?></span>
                    <span class="poll-votes"><?php echo $value['value'];?> โหวต</span>
                </span>
                <div class="progress">
                    <div class="progress-bar <?php echo $value['class_color'];?>" role="progressbar" aria-valuenow="<?php echo $percent;?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo $percent;?>%;"><?php echo $percent.'%';?></div>
                </div>
            </div>
        <?php
        }
        ?>
        </form>

    <?php
    $response = ob_get_contents();
    ob_end_clean();
    echo $response;
    exit();
}
?>

<script>
    $(document).ready(function() {
        function load_poll() {
            var url_location = 'poll.ajax.php';
            $.ajax({
                type: "POST"
                , url: url_location
                , data: {
                     "do" : "poll-content"
                    , "from" : "frm_poll"
                }
                , async: true
                , success: function(response) {
                    $(".poll-content").html(response);
                }
            });

        }

        function load_result() {
            var url_location = 'poll.ajax.php';
            $.ajax({
                type: "POST"
                , url: url_location
                , data: {
                    "do" : "cal_result"
                    , "from" : "frm_result"
                }
                , async: true
                , success: function(response) {
                    $(".poll-content").html(response);
                }
            });

        }

        $("body").on("click", "#btn-poll", function() {
            var question = $('input[name=question]:checked').val();
            var url_location = 'poll.ajax.save.php';
            $.ajax({
                type: "POST"
                , url: url_location
                , data: {
                    "do": "cal_result"
                    , "from": "frm_result"
                    , "question" : question
                }
                , async: true
                , success: function (response) {
                    console.log(response);
                    if(response == 'ok'){
                        load_result();
                    }else if(response == 'error'){
                        $(".show_alert").append( "<h6 class='text-danger text-center' style='margin-top: 100px;'>ไม่สามารถตอบสอบถามความคิดเห็นได้ <br>เนื่องจากเกิดความผิดพลาดการบันทึกข้อมูล</h6>" );
                        $(".show_from").hide();
                    }else{
                        $(".show_alert").append( "<h6 class='text-danger text-center' style='margin-top: 100px;'>ไม่สามารถตอบสอบถามความคิดเห็นได้ <br>เนื่องจากท่านสอบถามความคิดเห็นแล้ว</h6>" );
                        $(".show_from").hide();
                    }

                }
            });
        });

        $("body").on("click", "#btn-result", function() {
            var url_location = 'poll.ajax.php';
            $.ajax({
                type: "POST"
                , url: url_location
                , data: {
                    "do": "cal_result"
                    , "from": "frm_result"
                }
                , async: true
                , success: function (response) {
                    $(".poll-content").html(response);
                    console.log(response);
                }
            });
        });

        load_poll();
    });
</script>
<div class="poll-content"></div>