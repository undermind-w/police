<?php
	include "config.inc.php";
	header('Content-type: application/xml');

	$xmlns = '';

	$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";

    $xml .= '<rss version="2.0" ' . $xmlns . '>' . "\n";

    // channel required properties
    $xml .= '<channel>' . "\n";
    $xml .= '<title>สหกรณ์ออมทรัพย์โรงพยาบาลตำรวจ จำกัด</title>' . "\n";
    $xml .= '<link>http://' . $_SERVER["SERVER_NAME"] . '/news.php</link>' . "\n";
    $xml .= '<description>ข่าวสาร</description>' . "\n";

    // channel optional properties
	 $xml .= '<language>th</language>' . "\n";
	/*
	$xml .= '<image>' . "\n";
      $xml .= '<title>' . $this->channel_properties["image_title"] . '</title>' . "\n";
      $xml .= '<link>' . $this->channel_properties["image_link"] . '</link>' . "\n";
      $xml .= '<url>' . $this->channel_properties["image_url"] . '</url>' . "\n";
      $xml .= '</image>' . "\n";
	*/

    // get RSS channel items
    $now =  date("YmdHis"); // get current time  // configure appropriately to your environment
    //$rss_items = $this->get_feed_items($now);
	$sql = "SELECT * FROM cmp_news WHERE news_status = 1 AND publicdate < NOW() ORDER BY publicdate DESC LIMIT 0 , 8  " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

    while( $item = $rs->fetch_assoc() ) {
		$title = htmlspecialchars($item["news_title"]) ;
		$link = "http://{$_SERVER["SERVER_NAME"]}/news.php?nid={$item["news_id"]}" ;
		$news_detail = htmlspecialchars_decode($item["news_detail"] , ENT_QUOTES ) ;
		$news_detail = strip_tags($news_detail);
		$description = html2text($news_detail , 100 ) ;
 		$pubDate = gmdate(DATE_RSS, strtotime($item["publicdate"]));

		$xml .= '<item>' . "\n";
		$xml .= '<title><![CDATA[' . $title  . ']]></title>' . "\n";
		$xml .= '<link>' . $link . '</link>' . "\n";
		$xml .= '<description><![CDATA[' . $description . ']]></description>' . "\n";
		$xml .= '<pubDate>' . $pubDate . '</pubDate>' . "\n";

      $xml .= '</item>' . "\n";
    }

    $xml .= '</channel>';

    $xml .= '</rss>';

    echo $xml;
