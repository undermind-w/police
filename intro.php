<?php
	include "config.inc.php";
	
?><!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="user-scalable = yes" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
	<title>สหกรณ์ออมทรัพย์โรงพยาบาลตำรวจ จำกัด</title>
    
    <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/html/frontend.police/assets/bootstrap-4.4.1/css/bootstrap.min.css">
 	<link rel="stylesheet" type="text/css" href="/html/frontend.police/assets/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/html/frontend.police/css/css.css">
	<link rel="stylesheet" type="text/css" href="/html/frontend.police/assets/sweetalert2/dist/sweetalert2.min.css">

	
</head>
<?php 
	
	$month = (int) date("m") ; 
	$day = (int) date("d") ; 
	
	$sql = "SELECT * FROM cmp_importantday WHERE MONTH(publicdate) = {$month} AND DAY(publicdate) = {$day} LIMIT 1 " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	
	$row = $rs->fetch_assoc();
 ?>
<body class="body-intro" style="background-color:#000000" >
	<section>
		<div class="container" >
			<div class="row" style="margin-bottom:20px;margin-top:20px" >
				<div class="col-md-12 text-center" >
					<?php if(!empty($row["day_picture"])){ ?>
					<img class="img-fluid" src="/uploads/contents/<?php echo $row["day_picture"] ?>" style="max-height:500px;" />
					<?php } ?>
				</div>
			</div>
			
			<div class="row" >
				<div class="col-md-12 text-center" >
					<a href="/index.php?first=1" class="btn btn-default btn-custom-1"  >เข้าสู่เว็บไซต์</a>
				</div>
			</div>
		</div>
	</section>
	 
	<script src="/html/frontend.police/assets/jquery/jquery-3.4.1.min.js" ></script>
	<script src="/html/frontend.police/assets/jquery/jquery.form.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="/html/frontend.police/assets/bootstrap-4.4.1/js/bootstrap.min.js" ></script>
	
 	 <script src="/html/frontend.police/assets/sweetalert2/dist/sweetalert2.all.min.js"></script>


	
</body>
</html>
