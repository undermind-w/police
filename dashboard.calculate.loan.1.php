<?php
	include "config.inc.php";

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($_POST) exit();

	define("THEME","main");
	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	<script>
			$(document).ready(function() {

			});
	</script>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	//include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.subject.loan.inc.php";
	$smarty->assign("subject", "คำนวณสินเชื่อ");
	$smarty->assign("title", "คำนวณสินเชื่อ");
	$smarty->assign("header_title","");
	// $smarty->assign("breadcrumb", array("คำนวณสินเชื่อ" => null ));
	ob_start();
?>
	<div class="row m-t-md">

			<div class="col-md-12">
					<?php include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.include.calculate2.v2.php"; ?>
			</div>
	</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
