<?php
	include "config.inc.php";
	
	if(@$_POST)
	{
		if(@$_POST["do"] == "count") 
		{
			$jol_id = (int) @$_POST["jid"] ; 
			$sql = "UPDATE cmp_journal SET jol_count = jol_count + 1   WHERE jol_id = '{$jol_id}' LIMIT 1 " ;
			$mysqli->query($sql) ; 
			echo $mysqli->error;
			echo json_encode(array("msg" => "" , "status" => 1 )) ; 
			exit();
		}
		
	}
	ob_start();
	
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<style>
		.journal-list .item{padding-bottom:35px;     }
		.journal-list .detail {font-size:16px ;font-family: "Kanit", sans-serif ; margin-top:40px; line-height: 18px  }
		.journal-list .download {font-size:16px ; line-height:18px; ;font-family: "Kanit", sans-serif ;  }
		 @media (min-width: 768px){
			 .journal-list .detail {width:100%; }
			 .journal-list .download {width:100%; }
		 }
	</style>
<?php
	
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	
	<script>
		$(function(){
			$(".btnDownload").click(function(){
				var jid = $(this).data("id") ; 
				$.ajax({
					async  : false , 
					method: "POST",
					url: "?",
					data: { "do": "count" , "jid" : jid  }
				})
				.done(function( msg ) {
					console.log(msg)
 				});
 			})
		});
	</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();
?>
	
	
<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	 
	ob_start();
	$jol_type = $_GET["type"]; 
	$type_mane = array('journal'=>'วารสารสหกรณ์','letter'=>'จดหมายข่าวสหกรณ์');	
	
	$sql = "SELECT * FROM cmp_journal WHERE jol_status = 1 AND jol_type = '{$jol_type}' AND publicdate < NOW()  ORDER BY publicdate DESC " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	$smarty->assign("title", $type_mane[$jol_type]);
	$smarty->assign("subject", $type_mane[$jol_type]);
	$smarty->assign("breadcrumb", array($type_mane[$jol_type] => "#" ));

?>
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row journal-list">
<?php while($row = $rs->fetch_assoc()){ ?>
		<div class="col-md-6" >
			<div class="item">
				<div class="row">
					<div class="col-sm-6">
						<?php if(!empty($row["jol_picture"])){ ?>
						<img style="margin:auto;" class="img-fluid" src="/uploads/contents/<?php echo @$row["jol_picture"] ?>" />
						<?php }else{ ?>
						&nbsp;
						<?php } ?>
					</div>
					<div class="col-sm-6 padding-left-none ">
						<div class="detail" style=";">
							<div><?php echo $row["jol_title"] ?></div>
						</div>
						<div class="download">
							<a class="btnDownload" data-id="<?php echo @$row["jol_id"] ?>" href="/uploads/journal/<?php echo @$row["jol_pdf"] ?>" ><i class="fa fa-download"></i>  ดาวน์โหลด</a>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php } ?>
		</div>
	</div>
</section>
<?php
 	
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");