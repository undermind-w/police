<?php
class CmpActionLogUser {

	public function __construct() {
		global $mysqli ;
		$this->mysqli = $mysqli;
		$this->admin_id = !empty($_SESSION["ADMIN_ID"]) ? $_SESSION["ADMIN_ID"] : 0 ;  ;
 	}

	public function add_log($code = "" , $detail = "" , $refid = ""  )
	{
		$log_id = date("YmdHis") . @random_char(7);
		$log_ip = @$_SERVER["REMOTE_ADDR"] ;


		$sql = "INSERT INTO `cmp_userlog`
									(
									log_id
								  , createdate
								  , log_ip
									)
									VALUES
									(
									'{$log_id}'
									, NOW()
									, '{$log_ip}'
									)" ;
		$this->mysqli->query($sql);
		$this->mysqli->error ;
 	}

  public function get_stats()
	{
		$sql = "SELECT count(`id`) AS num_stats,MIN(`createdate`) AS createdate  FROM `cmp_userlog`" ;
		$rs = $this->mysqli->query($sql);
		$item2 = $rs->fetch_assoc();
			
		$num = substr("0000000".$item2['num_stats'],-7,7);
		$result['num_stats'] = $num;
		$result['your_ip'] = $_SERVER['REMOTE_ADDR'];
		$result['date_start'] = $item2['createdate'];

		$arr_num = array();
		for( $i = 0; $i < strlen($num); ++$i ) {
			$arr_num[$i] = substr($num, $i, 1);
		}
		$result['num_stats_arr'] = $arr_num;
	
     
		return $result ;
	}

}
