<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<style type="text/css">
			body {
				margin: 0px;
				padding: 0px;
				padding-top: 10px;
				min-width: 100%;
				mso-line-height-rule: exactly;
			}
			body, table {
				font-family: Verdana, Georgia, Tahoma, "Lucida Grande", Arial, sans-serif;
				font-size: 14px;
			}
			a { color: #00c6ff; }
		</style>
	</head>
	<body>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td align="center" style="padding: 0px;">
					<table width="600" border="0" cellpadding="0" cellspacing="0" style="background-color: #f9f9f9;">
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td>
								<a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/" style="text-decoration: none;">
									<img src="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/html/images/logo1.png" height="80" style="vertical-align: middle;" />
									<span style="vertical-align: middle; color: #000; font-size: 24px;">สหกรณ์ข้าราชการสหกรณ์ จำกัด</span>
								</a>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
					</table>
					<table width="600" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff; border-top: solid 3px #2aa70e;">
						<tr>
							<td>
								<div style="padding: 20px 10px;"><?php echo $mail_detail; ?></div>
							</td>
						</tr>
					</table>
					<table width="600" border="0" cellpadding="0" cellspacing="0" style="background-color: #f9f9f9;">
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td align="center" style="color: #666;">
								<div style="font-size: 12px;">สหกรณ์ข้าราชการสหกรณ์ จำกัด</div>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>