<?php
class Page {
	/**
	 * get page
	 *
	 * @access public
	 * @param array(
	 *		int pagenum
	 *		int showlist
	 *		string link_param
	 *		int page
	 *		int row_count
	 * )
	 * @return array 
	 */ 
	public function getPage($param) {
		$PageNum = $param["pagenum"];
		$ShowList = $param["showlist"];
		$link_param = isset($param["link_param"]) ? $param["link_param"] : "";
		
		$sPage = 0;
		$cPage = 0;
		$ePage = 0;
		
		$page = empty($param["page"]) ? "1" : $param["page"];
		$pagecount = ceil($param["row_count"] / $ShowList);
		
		$linkpage = "";
		$cPage = $page;
		if (($cPage - ($PageNum / 2)) < 1) {
			$sPage = 1;
			if ($cPage > $sPage) {
				if ($cPage > ($sPage + 1)) {
					//back 
					$linkpage = "<a href=\"?page=1".$link_param."\">First</a> ";
				}
				//first
				$linkpage .= "<a href=\"?page=".($cPage - 1).$link_param."\">Prev</a> ";
			}
		}
		else {
			$sPage = $cPage - ($PageNum / 2);
			//first & back
			$linkpage = "<a href=\"?page=1".$link_param."\">First</a> <a href=\"?page=".($cPage - 1).$link_param."\">Prev</a> ";
		}
		if (($sPage + $PageNum) > $pagecount) {
			$ePage = $pagecount;
		}
		else {
			$ePage = $sPage + $PageNum - 1;
		}
		
		for ($i = $sPage; $i <= $ePage; $i++) {
			//number
			$linkpage .= $i == $cPage ? "<span class=\"page_link\">".$i."</span> " : "<a href=\"?page=".$i.$link_param."\">".$i."</a> ";
		}
		
		if (($sPage + $PageNum) < $pagecount) {
			//next & last
			$linkpage .= " <a href=\"?page=".($cPage + 1).$link_param."\">Next</a> <a href=\"?page=".$pagecount.$link_param."\">Last</a>";
		}
		else {
			if ($cPage < $ePage) {
				//next
				$linkpage .= " <a href=\"?page=".($cPage + 1).$link_param."\">Next</a>";
				if ($cPage < ($ePage - 1))
				{
					//last
					$linkpage .= " <a href=\"?page=".$pagecount.$link_param."\">Last</a>";
				}
			}
		}
		
		$_pages = array(
			"page" => $cPage,
			"showlist" => $ShowList,
			"page_count" => $pagecount,
			"row_count" => $param["row_count"],
			"offset" => ($cPage - 1) * $ShowList,
			"index_asc" => (($cPage - 1) * $ShowList) + 1,
			"index_desc" => $param["row_count"] - (($cPage - 1) * $ShowList),
			"linkpage" => $linkpage	
		);
		
		return $_pages;
	}
	
	/**
	 * get page ajax
	 *
	 * @access public
	 * @param array(
	 *		int pagenum
	 *		int showlist
	 *		int page
	 *		int row_count
	 *		string class
	 * )
	 * @return array 
	 */ 
	public function getPageAJAX($param) {
		$PageNum = $param["pagenum"];
		$ShowList = $param["showlist"];
		$class = $param["class"];
		
		$sPage = 0;
		$cPage = 0;
		$ePage = 0;
		
		$page = empty($param["page"]) ? "1" : $param["page"];
		$pagecount = ceil($param["row_count"] / $ShowList);
		
		$linkpage = "";
		$cPage = $page;
		if (($cPage - ($PageNum / 2)) < 1) {
			$sPage = 1;
			if ($cPage > $sPage) {
				if ($cPage > ($sPage + 1)) {
					//back 
					$linkpage = "<a href=\"#1\" class=\"".$class."\">First</a> ";
				}
				//first
				$linkpage .= "<a href=\"#".($cPage - 1)."\" class=\"".$class."\">Prev</a> ";
			}
		}
		else {
			$sPage = $cPage - ($PageNum / 2);
			//first & back
			$linkpage = "<a href=\"#1\" class=\"".$class."\">First</a> <a href=\"#".($cPage - 1)."\" class=\"".$class."\">Prev</a> ";
		}
		if (($sPage + $PageNum) > $pagecount) {
			$ePage = $pagecount;
		}
		else {
			$ePage = $sPage + $PageNum - 1;
		}
		
		for ($i = $sPage; $i <= $ePage; $i++) {
			//number
			$linkpage .= $i == $cPage ? "<span class=\"page_link\">".$i."</span> " : "<a href=\"#".$i."\" class=\"".$class."\">".$i."</a> ";
		}
		
		if (($sPage + $PageNum) < $pagecount) {
			//next & last
			$linkpage .= " <a href=\"#".($cPage + 1)."\" class=\"".$class."\">Next</a> <a href=\"#".$pagecount."\" class=\"".$class."\">Last</a>";
		}
		else {
			if ($cPage < $ePage) {
				//next
				$linkpage .= " <a href=\"#".($cPage + 1)."\" class=\"".$class."\">Next</a>";
				if ($cPage < ($ePage - 1))
				{
					//last
					$linkpage .= " <a href=\"#".$pagecount."\" class=\"".$class."\">Last</a>";
				}
			}
		}
		
		$_pages = array(
			"page" => $cPage,
			"showlist" => $ShowList,
			"page_count" => $pagecount,
			"row_count" => $param["row_count"],
			"offset" => ($cPage - 1) * $ShowList,
			"index_asc" => (($cPage - 1) * $ShowList) + 1,
			"index_desc" => $param["row_count"] - (($cPage - 1) * $ShowList),
			"linkpage" => $linkpage	
		);
		
		return $_pages;
	}
}

/* End of file Consult.php */