<?php
require_once("Common.php");

class MemberBoard {
	private $db;
	private $common;
	
	public function __construct($db) {
		$this->db = $db;
		$this->common = new Common();
	}
	
	public function loginFacebook($user_profile) {
		$domain = $_SERVER["HTTP_HOST"];
		$domain =  substr_count($domain, ".") > 2 ? substr($domain, strpos($domain, ".")) : $domain;
		setcookie("facebook_email", "", time() - 3600, "/", $domain);
		setcookie("facebook_name", "", time() - 3600, "/", $domain);
		
		if(empty($user_profile["email"])) {
			return -1;	// Error
		}
		
		$row = $this->getDetail($user_profile["email"]);
		if($row) {
			$this->login($user_profile["email"]);
			return 2;	// Registered
		}
		else {
			$time = 60 * 60; // 1 Hour
			setcookie("facebook_email", $user_profile["email"], time() + $time, "/", $domain);
			setcookie("facebook_name", $user_profile["name"], time() + $time, "/", $domain);
			return 1;	// Register
		}
	}
	
	public function loginGoogle($user_profile) {
		$domain = $_SERVER["HTTP_HOST"];
		$domain =  substr_count($domain, ".") > 2 ? substr($domain, strpos($domain, ".")) : $domain;
		setcookie("google_email", "", time() - 3600, "/", $domain);
		setcookie("google_name", "", time() - 3600, "/", $domain);
		
		if(empty($user_profile->email)) {
			return -1;	// Error
		}
		
		$row = $this->getDetail($user_profile->email);
		if($row) {
			$this->login($user_profile->email);
			return 2;	// Registered
		}
		else {
			$time = 60 * 60; // 1 Hour
			setcookie("google_email", $user_profile->email, time() + $time, "/", $domain);
			setcookie("google_name", $user_profile->name, time() + $time, "/", $domain);
			return 1;	// Register
		}
	}
	
	public function loginWeb($user, $password) {
		$sql = "SELECT * FROM cmp_webboard_member WHERE wmem_username = '".$user."' AND wmem_password <> '' AND wmem_password = '".$password."'";
		$row = $this->db->getRow($sql);
		if($row) {
			if(!$row["wmem_confirm"]) {
				return -2; // no confirm
			}
			else if($this->is_block($user)) {
				return -1; // block
			}
			else {
				$this->login($user);
				return 1; // success
			}
		}
		else {
			return 0; // miss match
		}
	}
	
	public function login($user) {
		if(!$this->is_block($user)) {
			$row = $this->getDetail($user);
			
			$time = 60 * 60 * 24 * 365 * 10; // 10 Year
			$domain = $_SERVER["HTTP_HOST"];
			$domain =  substr_count($domain, ".") > 2 ? substr($domain, strpos($domain, ".")) : $domain;
			setcookie("wmem_id", $row["wmem_id"], time() + $time, "/", $domain);
			setcookie("wmem_username", $row["wmem_username"], time() + $time, "/", $domain);
			setcookie("wmem_name", $row["wmem_name"], time() + $time, "/", $domain);
			setcookie("wmem_type_id", $row["wmem_type_id"], time() + $time, "/", $domain);
			setcookie("wmem_picture", $row["wmem_picture"], time() + $time, "/", $domain);
		}
	}
	
	public function logout() {
		$domain = $_SERVER["HTTP_HOST"];
		$domain =  substr_count($domain, ".") > 2 ? substr($domain, strpos($domain, ".")) : $domain;
		setcookie("wmem_id", "", time() - 3600, "/", $domain);
		setcookie("wmem_username", "", time() - 3600, "/", $domain);
		setcookie("wmem_name", "", time() - 3600, "/", $domain);
		setcookie("wmem_type_id", "", time() - 3600, "/", $domain);
		setcookie("wmem_picture", "", time() - 3600, "/", $domain);
	}
	
	public function is_login() {
		if(empty($_COOKIE["wmem_username"])) {
			return FALSE;
		}
		else if($this->is_block($_COOKIE["wmem_username"])) {
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	public function is_admin() {
		if($this->is_login()) {
			$row = $this->getDetail($_COOKIE["wmem_username"]);
			if($row["wmem_type_id"] == 99) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
	
	public function is_block($member_id) {
		$sql = "SELECT wmem_username FROM cmp_webboard_member WHERE wmem_confirm = 1 AND wmem_status = 1 AND wmem_username = '".$member_id."'";
		$row = $this->db->getRow($sql);
		if($row["wmem_username"]) {
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	public function getDetail($user) {
		$sql = "SELECT *
					  FROM cmp_webboard_member 
					  WHERE wmem_username = '".$user."'";
		return $this->db->getRow($sql);
	}
	
	public function count() {
		$sql = "SELECT COUNT(wmem_id) as row_count 
					  FROM cmp_webboard_member 
					  WHERE wmem_confirm = 1 AND wmem_status = 1";
		$row = $this->db->getRow($sql);
		
		return $row["row_count"];
	}
	
	public function getCountByCondition($where) {
		$sql = "SELECT COUNT(wmem_id) AS row_count 
					  FROM cmp_webboard_member 
					  WHERE ".$where;
		$row = $this->db->getRow($sql);
		return $row["row_count"];
	}
	
	public function getEntriesByCondition($offset, $top, $where, $order) {
		$sql = "SELECT * 
				  	  FROM cmp_webboard_member 
					  WHERE ".$where."
					  ORDER BY ".$order."
					  LIMIT ".$offset.", ".$top;
		return $this->db->getEntries($sql);
	}
	
	public function addUser($data) {
		$sql = "INSERT INTO cmp_webboard_member (
						  wmem_type_id,
						  wmem_username,
						  wmem_password,
						  wmem_fullname,
						  wmem_name,
						  wmem_email,
						  wmem_tel,
						  wmem_detail,
						  wmem_picture,
						  createdate,
						  wmem_confirm,
						  wmem_status)
					  VALUES (
						  '".$data["wmem_type_id"]."',
						  '".$data["wmem_username"]."',
						  '".$data["wmem_password"]."',
						  '".$data["wmem_fullname"]."',
						  '".$data["wmem_name"]."',
						  '".$data["wmem_email"]."',
						  '".$data["wmem_tel"]."',
						  '".$data["wmem_detail"]."',
						  '".$data["wmem_picture"]."',
						  NOW(),
						  '".$data["wmem_confirm"]."',
						  '".$data["wmem_status"]."')";
		$this->db->execCommand($sql);
	}
	
	public function updateUser($data) {
		$sql = "UPDATE cmp_webboard_member SET
						  updatedate = NOW()".
						  (isset($data["wmem_type_id"]) ? ", wmem_type_id = '".$data["wmem_type_id"]."'" : "").
						  (isset($data["wmem_password"]) ? ", wmem_password = '".$data["wmem_password"]."'" : "").
						  (isset($data["wmem_fullname"]) ? ", wmem_fullname = '".$data["wmem_fullname"]."'" : "").
						  (isset($data["wmem_name"]) ? ", wmem_name = '".$data["wmem_name"]."'" : "").
						  (isset($data["wmem_email"]) ? ", wmem_email = '".$data["wmem_email"]."'" : "").
						  (isset($data["wmem_tel"]) ? ", wmem_tel = '".$data["wmem_tel"]."'" : "").
						  (isset($data["wmem_detail"]) ? ", wmem_detail = '".$data["wmem_detail"]."'" : "").
						  (isset($data["wmem_picture"]) ? ", wmem_picture = '".$data["wmem_picture"]."'" : "").
						  (isset($data["wmem_confirm"]) ? ", wmem_confirm = '".$data["wmem_confirm"]."'" : "").
						  (isset($data["wmem_status"]) ? ", wmem_status = '".$data["wmem_status"]."'" : "")."
					  WHERE wmem_username = '".$data["wmem_username"]."'";
		$this->db->execCommand($sql);
	}
	
	public function deleteUser($user) {
		$sql = "DELETE FROM cmp_webboard_member WHERE wmem_username = '".$user."'";
		$this->db->execCommand($sql);
	}
	
	public function blockUser($user) {
		$sql = "UPDATE cmp_webboard_member SET wmem_status = 0 WHERE wmem_username = '".$user."'";
		$this->db->execCommand($sql);
	}
	
	public function unblockUser($user) {
		$sql = "UPDATE cmp_webboard_member SET wmem_status = 1 WHERE wmem_username = '".$user."'";
		$this->db->execCommand($sql);
	}
	
	public function activeUser($user) {
		$sql = "UPDATE cmp_webboard_member SET wmem_confirm = 1 WHERE wmem_username = '".$user."'";
		$this->db->execCommand($sql);
	}
	
	public function setAdmin($user) {
		$this->unblockUser($user);
		$this->activeUser($user);
		$sql = "UPDATE cmp_webboard_member SET wmem_type_id = 99 WHERE wmem_username = '".$user."'";
		$this->db->execCommand($sql);
	}
	
	public function unsetAdmin($user) {
		$sql = "UPDATE cmp_webboard_member SET wmem_type_id = 1 WHERE wmem_username = '".$user."'";
		$this->db->execCommand($sql);
	}
	
	public function sendConfirmMail($data) {
		require_once("function.inc.php");
		
		$msg = 'สวัสดี คุณ '.$data["wmem_fullname"].'  ,<br />
						<br />
						ขอต้อนรับเข้าสู่ สหกรณ์ข้าราชการสหกรณ์ จำกัด<br />
						กรุณาคลิกลิงค์ด้านล่างหรือคัดลอกไปเปิดที่เว็บเบราเซอร์เพื่อยืนยันการสมัครสมาชิก<br />
						<a href="http://'.$_SERVER["HTTP_HOST"].'/board/modal/register_complete.php?email='.$data["wmem_email"].'&code='.md5($data["wmem_email"].'active#r8x1k5s1v8a5txz4').'" target="_blank">http://'.$_SERVER["HTTP_HOST"].'/board/modal/register_complete.php?email='.$data["wmem_email"].'&code='.md5($data["wmem_email"].'active#r8x1k5s1v8a5txz4').'</a>
						<br />
						ขอขอบคุณ<br />
						ทีมงาน <strong>cpd online</strong>';
		$subject = "ยืนยันการสมัครสมาชิกกระดานถามตอบ cpd online";
		@send_mj_mail($subject, $msg, $data["wmem_email"]);
	}
	
	public function sendCompleteMail($data) {
		require_once("function.inc.php");
		
		$type = isset($data["type"]) ? $data["type"] : "";
		if($type == "facebook") {
			$msg = 'สวัสดี คุณ '.$data["wmem_fullname"].'  ,<br />
							<br />
							&nbsp; &nbsp; &nbsp; &nbsp; ขอต้อนรับเข้าสู่ สหกรณ์ข้าราชการสหกรณ์ จำกัด<br />
							ท่านได้ใช้ Facebook ในการสมัครสมาชิกกระดานถามตอบ cpd online ดังนั้นการ Login เข้าสู่ระบบเว็บไซต์ cpd online<br />
							เพื่อใช้บริการกระดานถามตอบ ให้ท่านเข้าสู่ระบบด้วย Account Facebook ของท่าน<br />
							<br />
							ขอขอบคุณค่ะ<br />
							ทีมงาน <strong>cpd online</strong>';
		}
		else if($type == "google") {
			$msg = 'สวัสดี คุณ '.$data["wmem_fullname"].'  ,<br />
							<br />
							&nbsp; &nbsp; &nbsp; &nbsp; ขอต้อนรับเข้าสู่ สหกรณ์ข้าราชการสหกรณ์ จำกัด<br />
							ท่านได้ใช้ Google ในการสมัครสมาชิกกระดานถามตอบ cpd online ดังนั้นการ Login เข้าสู่ระบบเว็บไซต์ cpd online<br />
							เพื่อใช้บริการกระดานถามตอบ ให้ท่านเข้าสู่ระบบด้วย Account Google ของท่าน<br />
							<br />
							ขอขอบคุณค่ะ<br />
							ทีมงาน <strong>cpd online</strong>';
		}
		else {
			$msg = 'สวัสดี คุณ '.$data["wmem_fullname"].'  ,<br />
							<br />
							&nbsp; &nbsp; &nbsp; &nbsp; ขอต้อนรับเข้าสู่ สหกรณ์ข้าราชการสหกรณ์ จำกัด<br />
							ข้อมูลสำหรับเข้าสู่ระบบของท่านคือ<br />
							&nbsp; &nbsp; &nbsp; &nbsp; Username : '.$data["wmem_username"].'<br />
							&nbsp; &nbsp; &nbsp; &nbsp; Password : '.$data["wmem_password"].'<br />
							<br />
							ขอขอบคุณค่ะ<br />
							ทีมงาน <strong>cpd online</strong>';
		}
		$subject = "ยินดีต้อนรับเข้าสู่ cpd online";
		@send_mj_mail($subject, $msg, $data["wmem_email"]);
	}
}

/**** End of file ****/