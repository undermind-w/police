<?php
class MysqlHelper {
	private $mysqli;
	
	function __construct() {
		global $mysqli;
		$this->mysqli = $mysqli;
	}
	
	function getEntries($sql) {
		$rs = $this->mysqli->query($sql) or die($this->mysqli->error. " Error no:".$this->mysqli->errno);
		
		$data = array();
		while($row = $rs->fetch_assoc()) {
			array_push($data, $row);
		}
		
		return $data;
	}
	
	function getRow($sql) {
		$rs = $this->mysqli->query($sql) or die($this->mysqli->error. " Error no:".$this->mysqli->errno);
		$row = $rs->fetch_assoc();
		
		return $row;
	}
	
	function getResultSet($sql) {
		return $this->mysqli->query($sql);
	}
	
	function execCommand($sql) {
		$this->mysqli->query($sql) or die($this->mysqli->error. " Error no:".$this->mysqli->errno);
	}
	
	function getInsertID() {
		return $this->mysqli->insert_id;
	}
}

/* End of file */