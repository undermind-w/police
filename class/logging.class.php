<?php 
class CmpActionLogging {
  	private $admin_id ;
	
 	
	public function __construct() {
		global $mysqli ; 
		$this->mysqli = $mysqli;
		$this->admin_id = !empty($_SESSION["ADMIN_ID"]) ? $_SESSION["ADMIN_ID"] : 0 ;  ; 
 	} 
	
	/**
	 * set admin id 
	 *
	 * @access public
	 * @param  int admin_id
	 * @return    
	 */ 
	public function set_admin_id($admin_id = 0 )
	{
		$this->admin_id = $admin_id ; 
	}
	
	/**
	 * get and create log table 
	 *
	 * @access public
	 * @param   
	 * @return string  
	 */
	public function check_table_log()
	{
		$yy = date("Y") ;
		$mm = sprintf("%02d", date("m"));

		
		$now_table = "cmp_action_log_{$yy}{$mm}" ; 
		$sql = "SHOW TABLES LIKE '{$now_table}' ";
		$rs = $this->mysqli->query($sql);
		echo $this->mysqli->error ;
		if($rs->num_rows == 0) {
			$sql = "CREATE TABLE `{$now_table}` LIKE `cmp_action_log`" ;
			$this->mysqli->query($sql);
			echo $this->mysqli->error ;
		}
		
		return $now_table ; 
	}
	
	/**
	 * get now log table 
	 *
	 * @access public
	 * @param   
	 * @return string  
	 */
	function get_now_table_log()
	{
		$yy = date("Y") ;
		$mm = sprintf("%02d", date("m"));


		return "cmp_action_log_{$yy}{$mm}" ; 
		
	}
	
	/**
	 * add log
	 *
	 * @access public
	 * @param string code , string detail , string refid 
	 * @return array 
	 */ 
	public function add_log($code = "" , $detail = "" , $refid = ""  )
	{
		$basename = basename($_SERVER["SCRIPT_FILENAME"] ) ; 
		$now_table = $this->check_table_log() ; 
		$log_id = date("YmdHis") . @random_char(7); 
		
		$admin_id = $this->admin_id ; 
		$log_code = @$this->mysqli->real_escape_string(@$code) ; 
		$log_file = @$this->mysqli->real_escape_string(@$basename)  ; 
		$log_ref_id = @$this->mysqli->real_escape_string(@$refid) ; 
		$log_detail = @$this->mysqli->real_escape_string(@$detail) ; 
		$log_ip = @$_SERVER["REMOTE_ADDR"] ;
		
		
		$sql = "INSERT INTO `{$now_table}` 
									( 
									log_id 
									, admin_id 
									, log_code 
									, log_file 
									, log_ref_id	
									, log_detail
									, createdate
									, log_ip
									)
									VALUES 
									(
									'{$log_id}'
									, '{$admin_id}'
									, '{$log_code}'
									, '{$log_file}'
									, '{$log_ref_id}'
									, '{$log_detail}'
									, NOW() 
									, '{$log_ip}'
									)" ;
		$this->mysqli->query($sql);
		echo $this->mysqli->error ;
 	}
	
	public function get_log()
	{
		
		
	}
}
 