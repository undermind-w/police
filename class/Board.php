<?php
require_once("Common.php");

class Board {
	private $db;
	private $common;
	private $user_id;
	
	public $baseurl = "/";
	
	public function __construct($db) {
		$this->db = $db;
		$this->common = new Common();
		$this->user_id = isset($_COOKIE["wmem_id"]) ? $_COOKIE["wmem_id"] : "";
	}
	
	/**
	 * get forum entries
	 *
	 * @access public
	 * @param 
	 * @return array 
	 */ 
	public function getForumEntries() {
		$sql = "SELECT *
				  	  FROM cmp_webboard_forum
					  WHERE forum_status = 1
					  ORDER BY 	forum_seq";
		return $this->db->getEntries($sql);
	}
	
	/**
	 * get forum
	 *
	 * @access public
	 * @param string forum_id
	 * @return array 
	 */ 
	public function getForum($forum_id) {
		$sql = "SELECT *
				  	  FROM cmp_webboard_forum
					  WHERE forum_id = '".$forum_id."'";
		return $this->db->getRow($sql);
	}
	
	/**
	 * get topic count by forum
	 *
	 * @access public
	 * @param string forum_id, string keyword
	 * @return int 
	 */ 
	public function getTopicCountByForum($forum_id = "", $keyword = "") {
		$sql = "SELECT COUNT(cmp_webboard_topic.topic_id) AS row_count 
					  FROM cmp_webboard_topic 
					  		INNER JOIN cmp_webboard_forum ON (cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id)
							INNER JOIN cmp_webboard_member ON (cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id)
					  WHERE cmp_webboard_topic.topic_status = 1 AND cmp_webboard_forum.forum_status = 1".
							(empty($forum_id) ? "" : " AND cmp_webboard_topic.forum_id = '".$forum_id."'").
							(empty($keyword) ? "" : " AND (cmp_webboard_topic.topic_title LIKE '%".$keyword."%' OR cmp_webboard_topic.topic_detail LIKE '%".$keyword."%')");
		$row = $this->db->getRow($sql);
		return $row["row_count"];
	}
	
	/**
	 * get topic entries by forum
	 *
	 * @access public
	 * @param int offset, int top, string forum_id, string keyword
	 * @return array 
	 */ 
	public function getTopicEntriesByForum($offset, $top, $forum_id = "", $is_pin_up = FALSE, $keyword = "") {
		$sql = "SELECT cmp_webboard_topic.*, cmp_webboard_forum.forum_title, cmp_webboard_member.wmem_name, cmp_webboard_member.wmem_picture
				  	  FROM cmp_webboard_topic 
					  		INNER JOIN cmp_webboard_forum ON (cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id)
							INNER JOIN cmp_webboard_member ON (cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id)
					  WHERE cmp_webboard_topic.topic_status = 1 AND cmp_webboard_forum.forum_status = 1".
							(empty($forum_id) ? "" : " AND cmp_webboard_topic.forum_id = '".$forum_id."'").
							(empty($keyword) ? "" : " AND (cmp_webboard_topic.topic_title LIKE '%".$keyword."%' OR cmp_webboard_topic.topic_detail LIKE '%".$keyword."%')")."
					  ORDER BY ".($is_pin_up ? "cmp_webboard_topic.topic_pin DESC, " : "")."cmp_webboard_topic.topic_id DESC
					  LIMIT ".$offset.", ".$top;
		$rs = $this->db->getResultSet($sql);
		$data = array();
		while($row = $rs->fetch_assoc()) {
			$row["topic_title"] = str_replace("“", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("”", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("‘", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("’", "", $row["topic_title"]);
			$row["url"] = $this->baseurl."board/topic/".$row["topic_id"];
			array_push($data, $row);
		}
		return $data;
	}
	
	/**
	 * get top topic entries by forum
	 *
	 * @access public
	 * @param int offset, int top, string forum_id
	 * @return array 
	 */ 
	public function getTopTopicEntriesByForum($offset, $top, $forum_id = "") {
		$sql = "SELECT cmp_webboard_topic.*, cmp_webboard_forum.forum_title, cmp_webboard_member.wmem_name, cmp_webboard_member.wmem_picture
				  	  FROM cmp_webboard_topic 
					  		INNER JOIN cmp_webboard_forum ON (cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id)
							INNER JOIN cmp_webboard_member ON (cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id)
					  WHERE cmp_webboard_topic.topic_status = 1 AND cmp_webboard_forum.forum_status = 1".(empty($forum_id) ? "" : " AND cmp_webboard_topic.forum_id = '".$forum_id."'")."
					  ORDER BY cmp_webboard_topic.topic_viewcount DESC
					  LIMIT ".$offset.", ".$top;
		$rs = $this->db->getResultSet($sql);
		$data = array();
		while($row = $rs->fetch_assoc()) {
			$row["topic_title"] = str_replace("“", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("”", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("‘", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("’", "", $row["topic_title"]);
			$row["url"] = $this->baseurl."board/topic/".$row["topic_id"];
			array_push($data, $row);
		}
		return $data;
	}
	
	/**
	 * get last post topic entries by forum
	 *
	 * @access public
	 * @param int offset, int top, string forum_id
	 * @return array 
	 */ 
	public function getLastPostTopicEntriesByForum($offset, $top, $forum_id = "") {
		$sql = "SELECT cmp_webboard_topic.*, cmp_webboard_forum.forum_title, cmp_webboard_member.wmem_name, cmp_webboard_member.wmem_picture
				  	  FROM cmp_webboard_topic 
					  		INNER JOIN cmp_webboard_forum ON (cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id)
							INNER JOIN cmp_webboard_member ON (cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id)
					  WHERE cmp_webboard_topic.topic_status = 1 AND cmp_webboard_forum.forum_status = 1".(empty($forum_id) ? "" : " AND cmp_webboard_topic.forum_id = '".$forum_id."'")."
					  ORDER BY cmp_webboard_topic.topic_last_post_date DESC, cmp_webboard_topic.topic_id DESC
					  LIMIT ".$offset.", ".$top;
		$rs = $this->db->getResultSet($sql);
		$data = array();
		while($row = $rs->fetch_assoc()) {
			$row["topic_title"] = str_replace("“", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("”", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("‘", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("’", "", $row["topic_title"]);
			$row["url"] = $this->baseurl."board/topic/".$row["topic_id"];
			array_push($data, $row);
		}
		return $data;
	}
	
	/**
	 * get topic count by user id
	 *
	 * @access public
	 * @param string wmem_id
	 * @return int 
	 */ 
	public function getTopicCountByUserID($wmem_id) {
		$sql = "SELECT COUNT(cmp_webboard_topic.topic_id) AS row_count 
					  FROM cmp_webboard_topic 
					  		INNER JOIN cmp_webboard_forum ON (cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id)
							INNER JOIN cmp_webboard_member ON (cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id)
					  WHERE cmp_webboard_topic.topic_status = 1 AND cmp_webboard_forum.forum_status = 1 AND cmp_webboard_topic.topic_wmem_id = '".$wmem_id."'";
		$row = $this->db->getRow($sql);
		return $row["row_count"];
	}
	
	/**
	 * get topic entries by user id
	 *
	 * @access public
	 * @param int offset, int top, string forum_id
	 * @return array 
	 */ 
	public function getTopicEntriesByUserID($offset, $top, $wmem_id) {
		$sql = "SELECT cmp_webboard_topic.*, cmp_webboard_forum.forum_title, cmp_webboard_member.wmem_name, cmp_webboard_member.wmem_picture
				  	  FROM cmp_webboard_topic 
					  		INNER JOIN cmp_webboard_forum ON (cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id)
							INNER JOIN cmp_webboard_member ON (cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id)
					  WHERE cmp_webboard_topic.topic_status = 1 AND cmp_webboard_forum.forum_status = 1 AND cmp_webboard_topic.topic_wmem_id = '".$wmem_id."'
					  ORDER BY cmp_webboard_topic.topic_id DESC
					  LIMIT ".$offset.", ".$top;
		$rs = $this->db->getResultSet($sql);
		$data = array();
		while($row = $rs->fetch_assoc()) {
			$row["topic_title"] = str_replace("“", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("”", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("‘", "", $row["topic_title"]);
			$row["topic_title"] = str_replace("’", "", $row["topic_title"]);
			$row["url"] = $this->baseurl."board/topic/".$row["topic_id"];
			array_push($data, $row);
		}
		return $data;
	}
	
	/**
	 * get topic
	 *
	 * @access public
	 * @param string topic_id
	 * @return array 
	 */ 
	public function getTopic($topic_id) {
		$sql = "SELECT cmp_webboard_topic.*, cmp_webboard_forum.forum_title, cmp_webboard_member.wmem_name, cmp_webboard_member.wmem_picture
				  	  FROM cmp_webboard_topic 
					  		INNER JOIN cmp_webboard_forum ON (cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id)
							INNER JOIN cmp_webboard_member ON (cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id)
					  WHERE cmp_webboard_topic.topic_status = 1 AND cmp_webboard_forum.forum_status = 1 AND cmp_webboard_topic.topic_id = '".$topic_id."'";
		if($row = $this->db->getRow($sql)) {
			$row["url"] = $this->baseurl."board/topic/".$row["topic_id"];
			$row["wmem_picture"] = "/images/board/member/".$row["wmem_picture"];
			$row["ip_cen"] = substr($row["ip"], 0, strrpos($row["ip"], ".")).".***";
		}
		
		return $row;
	}
	
	/**
	 * insert topic
	 *
	 * @access public
	 * @param array data
	 * @return int
	 */ 
	public function insertTopic($data) {
		$sql = "INSERT INTO cmp_webboard_topic (forum_id, topic_wmem_id, topic_title, topic_detail, craetedate, ip, topic_status)
					  VALUES ('".$data["forum_id"]."', '".$this->user_id."', '".$data["topic_title"]."', '".$data["topic_detail"]."', NOW(), '".$_SERVER["REMOTE_ADDR"]."', 1)";
		$this->db->execCommand($sql);
		
		return $this->db->getInsertID();
	}
	
	/**
	 * update topic
	 *
	 * @access public
	 * @param array data
	 * @return  
	 */ 
	public function updateTopic($data) {
		$sql = "UPDATE cmp_webboard_topic SET
						forum_id = '".$data["forum_id"]."',
						topic_title = '".$data["topic_title"]."',
						topic_detail = '".$data["topic_detail"]."',
						updatedate = NOW(),
						ip = '".$_SERVER["REMOTE_ADDR"]."'
					  WHERE topic_id = '".$data["id"]."'";
		$this->db->execCommand($sql);
	}
	
	/**
	 * hide topic
	 *
	 * @access public
	 * @param int id
	 * @return  
	 */ 
	public function hideTopic($id) {
		$sql = "UPDATE cmp_webboard_topic SET topic_status = 0
					  WHERE topic_id = '".$id."'";
		$this->db->execCommand($sql);
	}
	
	/**
	 * show topic
	 *
	 * @access public
	 * @param int id
	 * @return  
	 */ 
	public function showTopic($id) {
		$sql = "UPDATE cmp_webboard_topic SET topic_status = 1
					  WHERE topic_id = '".$id."'";
		$this->db->execCommand($sql);
	}
	
	/**
	 * delete topic
	 *
	 * @access public
	 * @param int id
	 * @return  
	 */ 
	public function deleteTopic($id) {
		$sql = "DELETE FROM cmp_webboard_topic WHERE topic_id = '".$id."'";
		$this->db->execCommand($sql);
	}
	
	/**
	 * update stat topic
	 *
	 * @access public
	 * @param int id
	 * @return  
	 */ 
	public function updateStatTopic($id) {
		$sql = "UPDATE cmp_webboard_topic SET topic_viewcount = topic_viewcount + 1
					  WHERE topic_id = '".$id."'";
		$this->db->execCommand($sql);
	}
	
	/**
	 * update stat topic reply
	 *
	 * @access public
	 * @param int topic_id, int post_id
	 * @return  
	 */ 
	public function updateStatTopicReply($topic_id) {
		$sql = "UPDATE cmp_webboard_topic SET topic_replycount = (SELECT COUNT(rep_id) AS topic_replycount FROM cmp_webboard_reply WHERE topic_id = '".$topic_id."' AND rep_status = 1), topic_last_post_date = NOW()
					  WHERE topic_id = '".$topic_id."'";
		$this->db->execCommand($sql);
	}
	
	/**
	 * update stat topic reply delete
	 *
	 * @access public
	 * @param int topic_id
	 * @return  
	 */ 
	public function updateStatTopicReplyDelete($topic_id) {
		$sql = "UPDATE cmp_webboard_topic SET topic_replycount = (SELECT COUNT(rep_id) AS topic_replycount FROM cmp_webboard_reply WHERE topic_id = '".$topic_id."' AND rep_status = 1)
					  WHERE topic_id = '".$topic_id."'";
		$this->db->execCommand($sql);
	}
	
	/**
	 * get post count by topic_id
	 *
	 * @access public
	 * @param int topic_id
	 * @return int 
	 */ 
	public function getPostCountByTopicID($topic_id) {
		$sql = "SELECT COUNT(cmp_webboard_reply.rep_id) AS row_count
					   FROM cmp_webboard_reply
							INNER JOIN cmp_webboard_member ON (cmp_webboard_reply.rep_wmem_id = cmp_webboard_member.wmem_id)
					   WHERE cmp_webboard_reply.rep_status = 1 AND cmp_webboard_reply.topic_id = '".$topic_id."'";
		$row = $this->db->getRow($sql);
		return $row["row_count"];
	}
	
	/**
	 * get post entries by topic_id
	 *
	 * @access public
	 * @param int offset, int top, int topic_id, string post_id, bool is_master
	 * @return array 
	 */ 
	public function getPostEntriesByTopicID($offset, $top, $topic_id, $post_id = "") {
		$sql = "SELECT cmp_webboard_reply.*, cmp_webboard_topic.topic_wmem_id, cmp_webboard_member.wmem_id, cmp_webboard_member.wmem_name, cmp_webboard_member.wmem_picture
					  FROM cmp_webboard_reply
							INNER JOIN cmp_webboard_topic ON (cmp_webboard_reply.topic_id = cmp_webboard_topic.topic_id)
							INNER JOIN cmp_webboard_member ON (cmp_webboard_reply.rep_wmem_id = cmp_webboard_member.wmem_id)
					  WHERE cmp_webboard_reply.rep_status = 1 AND cmp_webboard_reply.topic_id = '".$topic_id."'".(empty($post_id) ? "" : " AND cmp_webboard_reply.rep_id = '".$post_id."'")."
					  ORDER BY cmp_webboard_reply.rep_id
					  LIMIT ".$offset.", ".$top;
		$rs = $this->db->getResultSet($sql);
		$entries = array();
		while($row = $rs->fetch_assoc()) {
			$row["is_own"] = $row["wmem_id"] == $this->user_id || @$_COOKIE["wmem_type_id"] == 99 ? 1 : 0;
			$row["ip_cen"] = substr($row["ip"], 0, strrpos($row["ip"], ".")).".***";
			$row["wmem_picture"] = "/images/board/member/".$row["wmem_picture"];
			array_push($entries, $row);
		}
		
		return $entries;
	}
	
	/**
	 * insert post
	 *
	 * @access public
	 * @param array data
	 * @return int
	 */ 
	public function insertPost($data) {
		$sql = "INSERT INTO cmp_webboard_reply (topic_id, rep_wmem_id, rep_detail, createdate, ip, rep_status)
					  VALUES ('".$data["topic_id"]."', '".$this->user_id."', '".$data["rep_detail"]."', NOW(), '".$_SERVER["REMOTE_ADDR"]."', '".$data["rep_status"]."')";
		$this->db->execCommand($sql);
		
		return $this->db->getInsertID();
	}
	
	/**
	 * hide post
	 *
	 * @access public
	 * @param int id
	 * @return  
	 */ 
	public function hidePost($id) {
		$sql = "UPDATE cmp_webboard_reply SET rep_status = 0
					  WHERE rep_id = '".$id."'";
		$this->db->execCommand($sql);
	}
}

/**** End of file ****/