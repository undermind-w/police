<?php
	define("AES_256_CBC", "aes-256-cbc");
	function encrypt_mobile($plaintext) {
    $encryption_key = "3hJQ*hg-&XWN_ajJ+jVvr92LKdE7%8yp";
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(AES_256_CBC));
    // echo "Before encryption: $plaintext\n";
    $encrypted = openssl_encrypt($plaintext, AES_256_CBC, $encryption_key, 0, $iv);
    // echo "Encrypted: $encrypted\n";
    $encrypted = $encrypted . ':' . base64_encode($iv);
    // echo $encrypted;
	return $encrypted;
	}
	function decrypt_mobile($plaintext,$iv) {
		$encryption_key = "3hJQ*hg-&XWN_ajJ+jVvr92LKdE7%8yp";
		$decrypted = openssl_decrypt($plaintext, AES_256_CBC, $encryption_key, 0, base64_decode($iv));
		return $decrypted;
	}




	define("ENCRYPT_KEY", "FEHnuRkGMH4SQ8LEfUymYg9XGRVBWBLf");
	function encrypt_text($plaintext) {
		$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
		$iv = openssl_random_pseudo_bytes($ivlen);
		$ciphertext_raw = openssl_encrypt($plaintext, $cipher, ENCRYPT_KEY, $options=OPENSSL_RAW_DATA, $iv);
		$hmac = hash_hmac('sha256', $ciphertext_raw, ENCRYPT_KEY, $as_binary=true);
		$ciphertext = base64_encode($iv.$hmac.$ciphertext_raw);

		return $ciphertext;
	}

	function decrypt_text($ciphertext) {
		$c = base64_decode($ciphertext);
		$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
		$iv = substr($c, 0, $ivlen);
		$hmac = substr($c, $ivlen, $sha2len=32);
		$ciphertext_raw = substr($c, $ivlen+$sha2len);
		$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, ENCRYPT_KEY, $options=OPENSSL_RAW_DATA, $iv);
		$calcmac = hash_hmac('sha256', $ciphertext_raw, ENCRYPT_KEY, $as_binary=true);

		return @hash_equals($hmac, $calcmac) ? $original_plaintext : "";
	}

	function add_quote($text, $quote = '#') { return sprintf("%s%s%s", $quote, $str, $quote); }

	function array_push_key(&$array, $key, $value) { $array[$key] = $value; }

	function U2T($text) { return iconv("UTF-8", "TIS-620", $text); }
	function T2U($text) { return iconv("TIS-620", "UTF-8", $text); }

	function random_char($max = 8) {
		unset($pass,$i);
		$salt = 'abchefghjkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		srand((double)microtime()*1000000);
		$i = 0;
		$pass = '';
		while ($i++ < $max)
			$pass .= substr($salt, rand() % strlen($salt), 1);
		return $pass;
	}

	function random_num($max = 8) {
		unset($pass,$i);
		$salt = "0123456789";
		srand((double)microtime()*1000000);
		$i = 0;
		$pass = '';
		while ($i++ < $max)
			$pass .= substr($salt, rand() % strlen($salt), 1);
		return $pass;
	}

	function genid() { return date("YmdHis").random_char(7); }

	function custom_number_format($n, $precision = 0) {
			if ($n < 1000) $n_format = number_format($n);
			elseif ($n < 1000000) $n_format = number_format(floor($n / 1000), $precision)."K";
			elseif ($n < 1000000000) $n_format = number_format(floor($n / 1000000), $precision) . "M";
			else $n_format = number_format(floor($n / 1000000000), $precision) . "B";
			return $n_format;
	}
	
	 

	function mysqldate2thaidate($value , $type = "long" , $time = false , $lang = "th"  ){
		if($lang == "th") {
			$mLong = array(1=>"มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
			$mShort = array(1=>"ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$str = "" ;

			$tmp = explode(" ",$value);
			if( $tmp[0] != "0000-00-00" ) {
				$d = explode( "-" , $tmp[0]);
				$month = array() ;
				if($type == "long" ){
					$month = $mLong ;
				}else{
					$month = $mShort ;
				}

				$str = (int)$d[2] . " " . $month[(int)$d[1]].  " ".($d[0]>2300?$d[0]:$d[0]+543);

				if($time)
				{
					$t = strtotime($value);
					$str  = $str . " " . date("H:i" , $t ) . " น." ;
				}
			}
		}
		else {
			$tmp = explode(" ",$value);
			if( $tmp[0] != "0000-00-00" ) {
				$t = strtotime($value);
				if($type == "long" ){
					$str = date("j F Y" , $t );
				}else{
					$str = date("j M Y" , $t );
				}

				if($time)
				{
					$t = strtotime($value);
					$str  = $str . " " . date("H:i" , $t );
				}
			}
		}

		return $str ;
	}

	function ConvertToThaiDate($value, $short = true, $year_short = false) {
		if($value!="0000-00-00") {
			$x = explode("-",$value);
			if($short == false)
				$arrMM=array(1=>"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
			else
				$arrMM=array(1=>"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
			$year = ( $x[0] > 2300 ?$x[0] : $x[0] +543 );
			$year = $year_short ? substr($year, 2, 2) : $year ;
			return $x[2]." ".$arrMM[(int)$x[1]]." ".$year;
		} else
		return "&nbsp;";
	}

	function send_sms($mobile, $msg) {

        $Username	= "upbean";
		$Password	= "up69Bean";
		$Sender		= "Upbean";
        $Message	= urlencode(iconv("UTF-8", "TIS-620", $msg));
        $Parameter  = "User={$Username}&Password={$Password}&Msnlist={$mobile}&Msg={$Message}&Sender={$Sender}";
		$API_URL	= "http://member.smsmkt.com/SMSLink/SendMsg/index.php";
		// $Username	= "0922246374";
		// $Password	= "013580";
		// $Sender		= "CMCOOP";
        // $Message	= urlencode(iconv("UTF-8", "TIS-620", $msg));
        // $Parameter  = "User={$Username}&Password={$Password}&Msnlist={$mobile}&Msg={$Message}&Sender={$Sender}";
		// $API_URL	= "http://www.thaibulksms.com/sms_api.php";

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$API_URL);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

		$result = curl_exec($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		return $result;
	}
	
	function send_sms_2($mobile, $msg) {
		if (!class_exists('SendMessageService')) {
			require $_SERVER["DOCUMENT_ROOT"] . '/class/sendMessageService.php';
		}
		
 		// $account = 'post@test';
		// $password = '8F0E6910C309E660B9EB768EB01E122A1F2B4770864D7988B3572E4ED92047C6';
		
		$account = 'post01@pghc';
		$password = 'D51288E198AAB1324587442964C4FBC4424C2ED872CA4C0A6D69B0B501B3433A';


		$mobile_no = $mobile ;
		// or $mobile_no = '0830000000,0831111111';
		$message = $msg ;
		$category = 'General';
		$sender_name = '';

		$results = SendMessageService::sendMessage($account, $password, $mobile_no, $message, '', $category, $sender_name);

		// use http proxy
		//$proxy = 'localhost:8888';
		//$proxy_userpwd = 'username:password';
		//$results = SendMessageService::sendMessage($account, $password, $mobile_no, $message, '', $category, $sender_name, $proxy, $proxy_userpwd);

		if ($results['result']) {
			// echo 'Send Success.'.' Task ID='.$results['task_id'].', Message ID='.$results['message_id'];
		} else {
			// echo $results['error'];
		}	
	
	}
	
	function get_token($member_id) {
        return md5($member_id."-".date("Ymdhis"));
    }

	function C2TDate($value, $short = true, $year_short = false) {
		if($value!="0000-00-00") {
			$x = explode("-",$value);
			if($short == false)
				$arrMM=array(1=>"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
			else
				$arrMM=array(1=>"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
			$year = ( $x[0] > 2300 ?$x[0] : $x[0] +543 );
			$year = $year_short ? substr($year, 2, 2) : $year ;
			return $x[2]." ".$arrMM[(int)$x[1]]." ".$year;
		} else
		return "ไม่ระบุ";
	}

	// ISO-8601
	$ub_dow = array(
		"en" => array(
			"s" => array(1 => "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"),
			"l" => array(1 => "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
		),
		"th" => array(
			"s" => array(1 => "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส.", "อา."),
			"l" => array(1 => "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์", "อาทิตย์")
		)
	);

	function DOW($val, $language = "th") {
		global $ub_dow;
		return $ub_dow[$language]["s"][$val];
	}

	$ub_dow_my = array(1 => "อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์");

	function C2D($value, $short = true) {
		if($value!="0000-00-00 00:00:00") {
			$x = explode(" ", $value);
			$date = explode("-", $x[0]);
			$time = explode(":", $x[1]);
			if($short == false)
				$arrMM = array(1=>"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
			else
				$arrMM=array(1=>"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
			return "{$date[2]} ".$arrMM[(int)$date[1]]." ".substr(( $date[0] > 2300 ?$date[0] : $date[0] +543 ), 2)." {$time[0]}:{$time[1]} น.";
		} else
		return "&nbsp;";
	}

	function MonthName($mm, $is_short = false) {
			$mm_long  = array(1 => "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
			$mm_short = array(1 => "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
			return $is_short ? $mm_short[(int)$mm] : $mm_long[(int)$mm];
	}

	function C2Pickdate($value) {
		if($value == "0000-00-00" || empty($value)) return "";
		else {
			$x = explode("-", $value);
			$yy = $x[0] + 543;
			return "{$x[2]}/{$x[1]}/{$yy}";
		}
	}

	function ConvertToSQLDate($date) {
		if(!empty($date)) {
			if(strpos($date, "/")!==false) {
				$x = explode("/", $date);
				$x[2] = ($x[2] > 2300 ? $x[2] - 543 : $x[2]);
				$x[1] = sprintf("%02d", (int)$x[1]);
				$return = "{$x[2]}-{$x[1]}-{$x[0]}";
			} elseif(strpos($date, "-")!==false) {
				$x = explode("-", $date);
				$x[0] = ($x[0] > 2300 ? $x[0] - 543 : $x[0]);
				$x[1] = sprintf("%02d", (int)$x[1]);
				$return = "{$x[0]}-{$x[1]}-{$x[2]}";
			} elseif(strpos($date, " ")!==false) {
				$arrMM = array("ม.ค." => "01", "ก.พ." => "02", "มี.ค." => "03", "เม.ย." => "04", "พ.ค." => "05", "มิ.ย." => "06", "ก.ค." => "07", "ส.ค." => "08", "ก.ย." => "09", "ต.ค." => "10", "พ.ย." => "11", "ธ.ค." => "12");
				$x = explode(" ", $date);
				$x[2] = ($x[2] > 2300 ? $x[2] - 543 : $x[2]);
				$x[1] = $arrMM[$x[1]];
				$return = "{$x[2]}-{$x[1]}-{$x[0]}";
			} else $return = "0000-00-00";
		} else $return = "0000-00-00";
		return $return;
	}

		function getBrowser() {
		$useragent = $_SERVER["HTTP_USER_AGENT"];
		$matched = "";
		if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		    $browser_version=$matched[1];
		    $browser = "IE";
		} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		    $browser_version=$matched[1];
		    $browser = "Opera";
		} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
		        $browser_version=$matched[1];
		        $browser = "Firefox";
		} elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
		        $browser_version=$matched[1];
		        $browser = "Chrome";
		} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
		        $browser_version=$matched[1];
		        //$browser_version="";
		        $browser = "Safari";
		} elseif(preg_match('|MessengerForiOS|',$useragent,$matched)) {
		        $browser_version=$matched[1];
		        //$browser_version="";
		        $browser = "Messenger";
		} else {
		    $browser_version = "";
		    $browser= "Other";
		}

		return trim("{$browser} {$browser_version}");
	}

	function getOS() {
		/*
		$OSList = array(
											'Windows 3.11' => 'Win16',
											'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
											'Windows 98' => '(Windows 98)|(Win98)',
											'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
											'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
											'Windows Server 2003' => '(Windows NT 5.2)',
											'Windows Vista' => '(Windows NT 6.0)',
											'Windows 7' => '(Windows NT 6.1)',
											'Windows 8' => '(Windows NT 6.2)',
											'Windows 8.1' => '(Windows NT 6.3)',
											'Windows 10' => '(Windows NT 10.0)',
											'Windows Phone 10' => '(Windows Phone 10.0)',
											'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
											'Windows ME' => 'Windows ME',
											'Android' => '(Android)',
											'iPhone' => '(iPhone)',
											'iPad' => '(iPad)',
											'Black Berry' => '(BlackBerry)',
											'Symbian' => '(Symbian)',
											'Mobile' => '(J2ME/MIDP)|(Profile/MIDP)|(SAMSUNG)',
											'Mac OS' => '(Mac_PowerPC)|(Macintosh)|(Mac)',
											'Open BSD' => 'OpenBSD',
											'Sun OS' => 'SunOS',
											'Linux' => '(Linux)|(X11)',
											'QNX' => 'QNX',
											'BeOS' => 'BeOS',
											'OS/2' => 'OS/2',
											'Search Bot'=>'(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
										);

		foreach($OSList as $CurrOS=>$Match) {
		        if (eregi($Match, $_SERVER['HTTP_USER_AGENT'])) break;
		}
		*/
		/*
    foreach ($OSList as $os => $pattern) {
    		if (preg_match('/' . $pattern . '/i', $uagent))
        		return $os;
    }

		return $CurrOS;
    */
    $oses   = array(
        'Windows 3.11' => 'Win16',
											'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
											'Windows 98' => '(Windows 98)|(Win98)',
											'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
											'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
											'Windows Server 2003' => '(Windows NT 5.2)',
											'Windows Vista' => '(Windows NT 6.0)',
											'Windows 7' => '(Windows NT 6.1)',
											'Windows 8' => '(Windows NT 6.2)',
											'Windows 8.1' => '(Windows NT 6.3)',
											'Windows 10' => '(Windows NT 10.0)',
											'Windows Phone' => '(Windows Phone)',
											'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
											'Windows ME' => '(Windows ME)',
											'Android' => '(Android)',
											'iPhone' => '(iPhone)',
											'iPad' => '(iPad)',
											'Black Berry' => '(BlackBerry)',
											'Symbian' => '(Symbian)',
											'Mobile' => '(J2ME\/MIDP)|(Profile\/MIDP)|(SAMSUNG)',
											'Mac OS' => '(Mac_PowerPC)|(Macintosh)|(Mac)',
											'Open BSD' => '(OpenBSD)',
											'Sun OS' => '(SunOS)',
											'Linux' => '(Linux)|(X11)',
											'QNX' => '(QNX)',
											'BeOS' => '(BeOS)',
											'OS/2' => '(OS\/2)',
											'Search Bot'=>'(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves\/Teoma)|(ia_archiver)|(bingbot)|(TweetmemeBot)|(Twitterbot)|(AhrefsBot)|(facebookexternalhit)|(Google-HTTP-Java-Client)|(SafeSearch)|(YandexBot)|(robot)|(MetaURI)|(LivelapBot)|(bot)|(jack)|(infegy)|(crawler)'
    );
    $uagent = $_SERVER['HTTP_USER_AGENT'];
    foreach ($oses as $os => $pattern) {
        if (preg_match('/' . strtolower($pattern) . '/i', strtolower($uagent))) {
            return $os;
				}
		}
    return 'Unknown';
	}
	
	function getOSbyStr($uagent = "" ) {
		 $oses   = array(
			'Windows 3.11' => 'Win16',
											'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
											'Windows 98' => '(Windows 98)|(Win98)',
											'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
											'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
											'Windows Server 2003' => '(Windows NT 5.2)',
											'Windows Vista' => '(Windows NT 6.0)',
											'Windows 7' => '(Windows NT 6.1)',
											'Windows 8' => '(Windows NT 6.2)',
											'Windows 8.1' => '(Windows NT 6.3)',
											'Windows 10' => '(Windows NT 10.0)',
											'Windows Phone' => '(Windows Phone)',
											'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
											'Windows ME' => '(Windows ME)',
											'Android' => '(Android)',
											'iPhone' => '(iPhone)',
											'iPad' => '(iPad)',
											'Black Berry' => '(BlackBerry)',
											'Symbian' => '(Symbian)',
											'Mobile' => '(J2ME\/MIDP)|(Profile\/MIDP)|(SAMSUNG)',
											'Mac OS' => '(Mac_PowerPC)|(Macintosh)|(Mac)',
											'Open BSD' => '(OpenBSD)',
											'Sun OS' => '(SunOS)',
											'Linux' => '(Linux)|(X11)',
											'QNX' => '(QNX)',
											'BeOS' => '(BeOS)',
											'OS/2' => '(OS\/2)',
											'Search Bot'=>'(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves\/Teoma)|(ia_archiver)|(bingbot)|(TweetmemeBot)|(Twitterbot)|(AhrefsBot)|(facebookexternalhit)|(Google-HTTP-Java-Client)|(SafeSearch)|(YandexBot)|(robot)|(MetaURI)|(LivelapBot)|(bot)|(jack)|(infegy)|(crawler)'
		);
		
		foreach ($oses as $os => $pattern) {
			if (preg_match('/' . strtolower($pattern) . '/i', strtolower($uagent))) {
				return $os;
			}
		}
		return 'Unknown';
	
	}
	
	function isMobileByuAgent($uagent = "" ) {
		return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $uagent  );
	}
	
	function encode($string, $key = "upbean") {
		$j = null ;
		$hash = null;
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}

function decode($string, $key = "upbean") {
		$j = null;
		$hash = null;
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    for ($i = 0; $i < $strLen; $i+=2) {
        $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= chr($ordStr - $ordKey);
    }
    return $hash;
}

function get_timeago( $ptime )
{
    $estimate_time = time() - $ptime;

    if( $estimate_time < 1 )
    {
        return 'สักครู่นี้';
    }

    $condition = array(
                12 * 30 * 24 * 60 * 60  =>  'ปี',
                30 * 24 * 60 * 60       =>  'เดือน',
                24 * 60 * 60            =>  'วัน',
                60 * 60                 =>  'ชั่วโมง',
                60                      =>  'นาที',
                1                       =>  'วินาที'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;

        if( $d >= 1 )
        {
            $r = round( $d );
            return '' . $r . ' ' . $str . ( $r > 1 ? '' : '' ) . 'ที่แล้ว';
        }
    }
}

	function getRealUrl($url) {
		if(substr($url, 0, 7) == 'http://'
		   || substr($url, 0, 8) == 'https://'
		   || substr($url, 0, 2) == '//')
		{
			$url = substr($url, 0, 2) == '//' ? 'http:'.$url : $url;
			stream_context_set_default(array(
				'http' => array(
					'method' => 'HEAD'
				)
			));
			$headers = get_headers($url, 1);
			if($headers !== false && isset($headers['Location'])) {
				return $headers['Location'];
			}
		}

		return $url;
	}

	function html2text($text, $pos) {

		$text = strip_tags ($text) ;
		$text = str_replace("&nbsp;", " ", trim(strip_tags($text)));
		$text = str_replace("\n", " ", $text);
		$text = str_replace("\r", " ", $text);

		if(mb_strlen($text, "UTF-8") > $pos) {
			$tmp = mb_substr($text, 0, $pos, "UTF-8");
			$p = mb_strrpos($tmp, " ", "UTF-8");
			if($p === FALSE) {
				return $tmp;
			}
			else {
				return mb_substr($tmp, 0, $pos, "UTF-8")."...";
			}
		}
		else {
			return $text;
		}

	}

	function send_mail($subject, $detail, $to) {
		return send_mj($subject, $detail, $to);
	}

	$key_api = "c231136b9cd1c878da45e0aad4c3e5c8";
	$key_secret = "cf918d2d3787c15fbe436824d20339ad";

	function send_mj($subject, $detail, $to) {
		global $key_api;
		global $key_secret;

		$email = array();
		if(is_array($to)) {
				foreach($to as $key => $val) {
						$email[]["Email"] = $val;
				}
		} else {
				$email[]["Email"] = $to;
		}

		$fields = array(
				'FromEmail' 	=> "steam4i@nia.or.th"
			, 'FromName' 		=> "STEAM4INNOVATION"
			, 'Subject' 		=> $subject
			, 'Html-part' 	=> $detail
			, 'Recipients' 	=> $email
		);
		$fields_string = json_encode($fields);
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL						, "https://api.mailjet.com/v3/send");
		curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST	, "POST");
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS			, $fields_string);
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER			, array(
																																'Content-Type: application/json',
																																'Content-Length: ' . strlen($fields_string)
																															)
								);

		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER	, TRUE);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER	, FALSE);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST	, 2);
		curl_setopt($curl_handle, CURLOPT_USERPWD				, "{$key_api}:{$key_secret}");
		$exec = curl_exec($curl_handle);
		$return = curl_getinfo($curl_handle);
		$json = json_decode($exec, TRUE);

		curl_close($curl_handle);
		return true;
	}

	function filetype_icon($file) {
		$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
		$icon = "icon-file-text-o";
		switch($ext) {
			case "pdf": $icon = "icon-file-pdf-o"; break;
			case "doc": $icon = "icon-file-word-o"; break;
			case "docx": $icon = "icon-file-word-o"; break;
			case "xls": $icon = "icon-file-excel-o"; break;
			case "xlsx": $icon = "icon-file-excel-o"; break;
			case "ppt": $icon = "icon-file-powerpoint-o"; break;
			case "pptx": $icon = "icon-file-powerpoint-o"; break;
			case "zip": $icon = "icon-file-archive-o"; break;
		}
		return $icon;
	}

	function writeToLog($message, $logfile, $split_file = false) {
		if($split_file && date("d") == "01") {
			$path_parts = pathinfo($logfile);
			$new_file = $path_parts["dirname"]."/".$path_parts["filename"]."-".date("Ymd").".".$path_parts["extension"];
			if(!file_exists($new_file)) {
				rename($logfile, $new_file);
			}
		}

		if($fp = fopen($logfile, 'a+')) {
			fwrite($fp, date('c') . ' ' . $message . PHP_EOL);
			fclose($fp);
		}
	}

	function ShowPeriodDate($date_s, $date_e) {
	  $startdate = strrev(explode('-', strrev($date_s), 2)[1]);
	  $enddate = strrev(explode('-', strrev($date_e), 2)[1]);
	  $period = '';
  
	  if( $date_s == $date_e ) {
		$period = ConvertToThaiDate($date_s, false, false);
	  } elseif( $startdate == $enddate ) {
		$period = sprintf('%s-%s', explode('-', $date_s)[2], ConvertToThaiDate($date_e, false, false));
	  } else {
		$period = sprintf('%s-%s', ConvertToThaiDate($date_s, true, true), ConvertToThaiDate($date_e, true, true));
	  }
	  return $period;
	}

	function base64ToImage($base64_string, $output_file) {
		$file = fopen($output_file, "wb");
	
		$data = explode(',', $base64_string);
	
		fwrite($file, base64_decode($data[1]));
		fclose($file);
	
		return $output_file;
	}
	
	function date_format_thai($str = "" ){
		return "" ; 
	}
	
	function substrrich($str = "" ){
		return "" ; 
	}