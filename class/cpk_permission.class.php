<?php 
class CmpPermission {
  	private $admin_id ;
 
	public function __construct() {
		global $mysqli ; 
		$this->mysqli = $mysqli;
		$this->admin_id = !empty($_SESSION["CPK_ADMIN_ID"]) ? $_SESSION["CPK_ADMIN_ID"] : 0 ;  ; 
 	} 
	
	/**
	 * set admin id 
	 *
	 * @access public
	 * @param  int admin_id
	 * @return    
	 */ 
	public function set_admin_id($admin_id = 0 )
	{
		$this->admin_id = $admin_id ; 
	}
	
	/**
	 * check permission and redirect  
	 *
	 * @access public
	 * @param  string code 
	 * @return string  
	 */
	public function check($code = "" , $redirect = "" )
	{
		if($this->admin_id != 1 ){
			if(!empty($code)){
				$code = $this->mysqli->real_escape_string($code) ; 
				$sql = "SELECT * FROM cmp_cpk_permission WHERE per_code = '{$code}' LIMIT 0 , 1  " ;
				$rs = $this->mysqli->query($sql);
				$permission = $rs->fetch_assoc(); 
				if(!empty($permission)){
					$sql = "SELECT * FROM cmp_cpk_admin_permission WHERE per_id = '{$permission["per_id"]}' AND admin_id = '{$this->admin_id}'  LIMIT 0 , 1 " ;
					$rs = $this->mysqli->query($sql);
					$check = $rs->fetch_assoc();
					if(empty($check)){
						header('Location: index.php');
						exit() ; 
					}
				}
				
			}
		}
	}
	
	public function get_login_permission_code()
	{
		if($this->admin_id != 1  ){
			$sql = "SELECT cmp_cpk_permission.per_code FROM cmp_cpk_admin_permission 
													LEFT JOIN cmp_cpk_permission ON cmp_cpk_admin_permission.per_id = cmp_cpk_permission.per_id 
												WHERE cmp_cpk_admin_permission.admin_id = '{$this->admin_id}' " ;
		}else{
			$sql = "SELECT cmp_cpk_permission.per_code FROM cmp_cpk_permission " ;
		}
		$rs = $this->mysqli->query($sql);
		$codes = array(); 
		while($item = $rs->fetch_assoc())
		{
			 $codes[] = $item["per_code"] ; 
		}
		 return $codes ; 
		 
	}
	
	
	 
}
 