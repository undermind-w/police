<?php
require("{$_SERVER['DOCUMENT_ROOT']}/class/smarty/Smarty.class.php");

$smarty = new Smarty();
$smarty->setTemplateDir("{$_SERVER['DOCUMENT_ROOT']}/smarty/templates");
$smarty->setCompileDir("{$_SERVER['DOCUMENT_ROOT']}/smarty/templates_c");
$smarty->setConfigDir("{$_SERVER['DOCUMENT_ROOT']}/smarty/configs");
$smarty->setCacheDir("{$_SERVER['DOCUMENT_ROOT']}/smarty/cache");

/*
$smarty->debugging = false;
$smarty->caching = true;
$smarty->cache_lifetime = 120;
*/