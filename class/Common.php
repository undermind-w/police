<?php
	class Common {
		// format MySQL DateTime (YYYY-MM-DD hh:mm:ss) using date()
		function FormatDate($format, $datetime) {
			$year = substr($datetime, 0, 4);
			$month = substr($datetime, 5, 2);
			$day = substr($datetime, 8, 2);
			$hour = substr($datetime, 11, 2);
			$min = substr($datetime, 14, 2);
			$sec = substr($datetime, 17, 2);
			return date($format, mktime($hour, $min, $sec, $month, $day, $year));
		}
		
		private $day = array("อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์");
		private $month = array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
		
		// format MySQL DateTime (YYYY-MM-DD hh:mm:ss) using date()
		function FormatDateThai($datetime, $style = 1) {
			if($datetime) {
				$year = substr($datetime, 0, 4) + 543;
				$month = substr($datetime, 5, 2);
				$day = substr($datetime, 8, 2);
				$hour = substr($datetime, 11, 2);
				$min = substr($datetime, 14, 2);
				$sec = substr($datetime, 17, 2);
				
				switch($style) {
					case 1:
						return $day."/".$month."/".$year." ".$hour.":".$min;
						break;
					case 2:
						return $day."/".$month."/".$year;
						break;
					case 3:
						return (int)$day." ".$this->month[$month - 1]." ".$year;
						break;
					case 4:
						return $hour.":".$min;
						break;
					case 5:
						return $this->month[$month - 1]." ".$year;
						break;
				}
			}
			else {
				return "";
			}
		}
		
		// format Thai DateTime (DD/MM/YYYY hh:mm) using date()
		function t2eDate($datetime, $is_mdy = "") {
			if($is_mdy) {
				$day = substr($datetime, 3, 2);
				$month = substr($datetime, 0, 2);
			}
			else {
				$day = substr($datetime, 0, 2);
				$month = substr($datetime, 3, 2);
			}
			$year = substr($datetime, 6, 4) - (substr($datetime, 6, 4) > 2500 ? 543 : 0);
			if(strlen($datetime) > 10) {
				$hour = substr($datetime, 11, 2);
				$min = substr($datetime, 14, 2);
				return date("Y-m-d H:i:s", mktime($hour, $min, 0, $month, $day, $year));
			}
			else {
				return date("Y-m-d H:i:s", mktime(0, 0, 0, $month, $day, $year));
			}
		}
		
		function fb_date($timestamp) {
			$difference = time() - $timestamp;
			$periods = array("second", "minute", "hour");
			$ending = " ago";
			if($difference < 60) {
				$j = 0;
				$periods[$j] .= ($difference != 1) ? "s" : "";
				$difference = ($difference == 3 || $difference == 4) ? " a few " : $difference;
				$text = "$difference $periods[$j] $ending";
			}
			elseif($difference < 3600) {
				$j = 1;
				$difference = round($difference / 60);
				$periods[$j] .= ($difference != 1) ? "s" : "";
				$difference = ($difference == 3 || $difference == 4) ? "a few " : $difference;
				$text = "$difference $periods[$j] $ending";
			}
			elseif($difference < 86400) {
				$j = 2;
				$difference = round($difference / 3600);
				$periods[$j] .= ($difference != 1) ? "s" : "";
				$difference = ($difference != 1) ? $difference : "about an ";
				$text = "$difference $periods[$j] $ending";
			}
			elseif($difference < 172800) {
				$difference = round($difference / 86400);
				$periods[$j] .= ($difference != 1) ? "s" : "";
				$text = "Yesterday at ".date("g:ia", $timestamp);
			}
			else {
				if($timestamp<strtotime(date("Y-01-01 00:00:00"))) {
					$text = date("l j, Y",$timestamp)." at ".date("g:ia", $timestamp);
				}
				else {
					$text = date("l j",$timestamp)." at ".date("g:ia", $timestamp);
				}
			}
			return $text;
		}
		
		function SQLText($text) {
			$text = str_replace("\'", "'", $text);
			return mysql_escape_string($text);
		}
		
		function UniqueRands($min, $max, $keys) {
			static $retval = array();
			$x = rand($min,$max);
			if(!in_array($x,$retval)){
				array_push($retval, $x);
			}
			if($keys > count($retval)){
				$this->UniqueRands($min, $max, $keys);
			}
			return $retval;
		}
		
		function isImage($filename) {
			$types = array(".gif", ".jpg", ".bmp", ".png");
			$ext = substr($filename, strrpos($filename, "."));
			foreach ($types as $type) {
				if($ext == $type) {
					return true;
				}
			}
			return false;
		}
		
		function html_remove($text) {
			$text = htmlspecialchars_decode($text, ENT_QUOTES);
			$text = strip_tags($text);
			$text = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $text);
			$text = trim($text);
			$text = preg_replace('/\s\s+/', ' ', $text);
			
			return $text;
		}
		
		function substrrich($string, $start, $length) {
			if(mb_strlen($string, "UTF-8") > $length) {	
				$tmp = mb_substr($string, $start, $length, "UTF-8");
				$pos = mb_strrpos($tmp, " ", "UTF-8");
				if($pos === FALSE) {
					return $tmp;
				}
				else {
					return mb_substr($tmp, 0, $pos, "UTF-8");
				}
			}
			else {
				return $string;
			}
		}
	}
?>