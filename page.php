<?php
	include "config.inc.php";

	if(empty($_GET["new"])){
		$pid = (int) $_GET["pid"] ;  
		echo "<script>window.location.href = '/page/{$pid}' </script>" ;
		exit();
	}
	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<style>

</style>
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scrip_include", $scrip_include);
	ob_start();
?>
<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	$content = "" ;
	

	if(!empty($_GET["pid"]))
	{
		$page_id = (int) @$_GET["pid"] ;

		$sql = "UPDATE cmp_page SET page_viewcount = page_viewcount + 1 WHERE page_id = '{$page_id}' " ;
		$mysqli->query($sql);
		echo $mysqli->error ;

		$sql = "SELECT * , cmp_admin.admin_name FROM cmp_page LEFT JOIN cmp_admin ON cmp_page.admin_id = cmp_admin.admin_id
						WHERE cmp_page.page_status = 1
 							AND cmp_page.page_id = '{$page_id}'
							LIMIT 0 , 1  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$page = $rs->fetch_assoc();
		if(empty($page))
		{
			header('Location: index.php');
			exit() ;
		}
		$smarty->assign("title",  @htmlspecialchars(@$page["page_title"]) );
 		$smarty->assign("subject", $page["page_title"]);
		$smarty->assign("breadcrumb", array("เนื้อหา" => "#" ));


		ob_start();
	?>
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row" >
			<div class="col-md-12">
				<h2 class="page-title"><?php echo @htmlspecialchars(@$page["page_title"]) ?></h2>
				<hr />
				<div class="page-detail" style="">
				<?php echo htmlspecialchars_decode(@$page["page_detail"] , ENT_QUOTES ) ;  ?>
				<br />
				</div>
				<div class="page-footer">
				</div>
 			</div>

		</div>
	</div>
</section>
	<?php
		$content = ob_get_contents();
		ob_end_clean();
	}

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
