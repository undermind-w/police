<?php

	//session_start();
	require "config.inc.php";

	header("Content-Type:text/html;charset=utf-8");
    date_default_timezone_set('Asia/Bangkok');
    
    if(@$_POST){
        $member_no = @$mysqli->real_escape_string(@$_POST["code"]) ;
   

		// if(empty($member_no)){
		// 	echo "<script>alert('กรุณากรอกหมายเลขสมาชิก'); window.location.href = 'index.php' ; </script>" ;
		// 	exit();
		// }

		$sql = "SELECT member_no FROM cmp_imp_member WHERE member_no = '{$member_no}' LIMIT 0 , 1  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
        $m1 = $rs->fetch_assoc();

		// if(empty($m1)){
		// 	echo "<script>alert('กรุณากรอกหมายเลขสมาชิก ที่ถูกต้อง '); window.location.href = 'index.php' ; </script>" ;
		// 	exit();
		// }

		$contact_title = "ร้องเรียน/แจ้งข้อเสนอแนะ" ;
		$contact_detail = @$mysqli->real_escape_string(@$_POST["detail"]);
		$contact_name = @$mysqli->real_escape_string(@$_POST["name"]);
		$contact_tel = @$mysqli->real_escape_string(@$_POST["tel"]);

		$contact_email  = @$mysqli->real_escape_string(@$_POST["email"]);
		$ip =  @$mysqli->real_escape_string(@$_SERVER["REMOTE_ADDR"]);

		$sql = "INSERT INTO cmp_contact (
										member_no
										, contact_title
										, contact_name
										, contact_tel
										, contact_email
										, contact_detail
										, createdate
										, updatedate
										, ip
										)
										VALUES
										(
										'{$member_no}'
										, '{$contact_title}'
										, '{$contact_name}'
										, '{$contact_tel}'
										, '{$contact_email}'
										, '{$contact_detail}'
										, NOW()
										, NOW()
										, '{$ip}'
                                        )" ;
                                        // echo $sql;
		$mysqli->query($sql);
		echo $mysqli->error ;

        // echo  $member_no;
        // exit;
		$sql = "SELECT * FROM cmp_website WHERE st_id = 1 LIMIT 0 , 1 " ;
		$rs = $mysqli->query($sql);
		$email = $rs->fetch_assoc();

		$smarty->assign("server_name", @$_SERVER["SERVER_NAME"] );
		$smarty->assign("contact_name", @htmlspecialchars($contact_name) );
		$smarty->assign("contact_detail", @htmlspecialchars( @nl2br($contact_detail) ));
		$html = @$smarty->fetch("email.tpl");

		// @send_mj_mail("มีผู้ ร้องเรียน/แจ้งข้อเสนอแนะ เข้ามาในระบบ" , $html , @$email["st_email_send"] );


		echo "<script>alert('ได้ทำการส่งข้อมูลไปแล้ว'); window.location.href = 'index.php' ; </script>" ;
		exit();

	}

?><!DOCTYPE html>
<html>
    <head>
        <title>-</title>
        <link rel="icon" href="/images/favicon.ico" sizes="32x32">
       <!-- <meta content="width=device-width, initial-scale=1" name="viewport"/> -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta charset="UTF-8">
        <link href="/html/frontend/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/html/frontend/font/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="/html/frontend/font/font-thaisansneue/thaisansneue.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="/scripts/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

        <link href="/html/frontend/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="/html/frontend/css/margin.css" rel="stylesheet" type="text/css"/>


        <script src="/scripts/jquery-1.10.2.min.js"></script>
        <script src="/scripts/jquery-migrate-1.2.1.js"></script>
        <script src="/html/frontend/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 		<script type="text/javascript" src="/scripts/utility.js"></script>

         <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

		<style type="text/css">
		body {background:none transparent;
		}
		</style>
    </head>
    <body>
			<div class="panel panel-primary" style="margin-bottom: 0;">
			  <div class="panel-heading" style="background:#167C3B; border-color:#167C3B;" >แจ้งปัญหาการใช้งาน/ร้องเรียน</div>
			  <div class="panel-body">
					<form id="frm_contact" action="contact.fancybox.php" method="post" >
						<div class="form-group">
							<label for="detail">เรื่อง</label>
							<textarea id="detail" name="detail" class="form-control" rows="5" ></textarea>
						</div>
						<div class="form-group">
							<label for="name">ชื่อ</label>
							<input type="text" class="form-control" id="name" name="name" placeholder="">
						</div>
						<div class="form-group">
							<label for="tel">เบอร์โทร</label>
							<input type="text" class="form-control" id="tel" name="tel" placeholder="">
						</div>
						<div class="form-group">
							<label for="email">อีเมล์</label>
							<input type="text" class="form-control" id="email" name="email" placeholder="">
						</div>

<!--						<button type="submit" class="btn btn-primary btn-block" style="background:#2aa70e" >ส่งข้อมูล </button>-->
                        <button id="bt_send" type="button" class="btn btn-primary btn-block" style="background:#167C3B;border-color:#167C3B;" >ส่งข้อมูล </button>
					</form>

			  </div>
			</div>
    </body>
</html>

<div class="modal fade" id="alertDetail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <h4>แจ้งเตือน</h4>
                </h4>
            </div>
            <div class="modal-body">
                <p style="font-size:18px;">กรุณาใส่เรื่องด้วยค่ะ</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="alertName" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <h4>แจ้งเตือน</h4>
                </h4>
            </div>
            <div class="modal-body">
                <p style="font-size:18px;">กรุณาใส่ชื่อด้วยค่ะ</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="alertTel" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <h4>แจ้งเตือน</h4>
                </h4>
            </div>
            <div class="modal-body">
                <p style="font-size:18px;">กรุณาใส่เบอร์โทรด้วยค่ะ</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="alertEmail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <h4>แจ้งเตือน</h4>
                </h4>
            </div>
            <div class="modal-body">
                <p style="font-size:18px;">กรุณาใส่อีเมล์ด้วยค่ะ</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="alertSuccess" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <h4>แจ้งเตือน</h4>
                </h4>
            </div>
            <div class="modal-body">
                <p style="font-size:18px;">ได้ทำการส่งข้อมูลไปแล้ว</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_success">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('document').ready(function() {
        $("#bt_send").on("click", function (e) {
            if($.trim($('#detail').val()) == ''){
                $('#alertDetail').modal("show");
            } else if($.trim($('#name').val()) == '') {
                $('#alertName').modal("show");
            } else if($.trim($('#tel').val()) == '') {
                $('#alertTel').modal("show");
            } else if($.trim($('#email').val()) == '') {
                $('#alertEmail').modal("show");
            }else{
                //save_contact();
                document.getElementById("frm_contact").submit();

            }
        });
    });

    function save_contact(){
        $.ajax({
            type: "POST"
            , url: "contact.ajax.php"
            , data: {
                "ajax" : 1
                , "detail" : $("#detail").val()
                , "name" : $("#name").val()
                , "tel" : $("#tel").val()
                , "email" : $("#email").val()
            }
            , async: true
            , success: function(msg) {
                $('#alertSuccess').modal("show");
                $("#close_success").on("click", function (e) {
                    window.location.href = '?' ;
                });
            }
        });
    }

</script>
<style>
    .form-loan {
        width: 120px !important;
        text-align: center;
    }
    .modal-header-info {
        color:#fff;
        padding:9px 15px;
        border-bottom:1px solid #eee;
        background-color: #2aa70e;
        -webkit-border-top-left-radius: 5px;
        -webkit-border-top-right-radius: 5px;
        -moz-border-radius-topleft: 5px;
        -moz-border-radius-topright: 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }
    .modal-dialog {
        margin-top: 250px;
        width: 300px;
    }
</style>
