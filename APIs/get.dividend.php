<?php
    require_once("config.inc.php");

    $data = [ 'status' => 0, 'responseText' => '' ];

    require_once("parameter.inc.php");
    require_once("token.validate.php");

    // $sql = "SELECT year FROM coop_dividend_average WHERE trim(member_id) = '{$member_id}' ORDER BY year DESC";
    // $rs = $mysqli->query($sql);
    // if ( $rs->num_rows ) {
    //     $data['status'] = 1;
    //     while(( $row = $rs->fetch_assoc() )) {
    //         $data['diviend'][] = [
    //             'year'  =>  (string)($row['year'] + 543),
    //             'yearText'  =>  'ปี '.(string)($row['year'] + 543)
    //         ];
    //     }
    // } 

    $sql = "SELECT `year`, dividend_value, average_return_value
    FROM coop_dividend_average
    WHERE trim(member_id) = trim('{$member_id}')
    ORDER BY `year` DESC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $data['status'] = 1;
        while(( $row = $rs->fetch_assoc() )) {
            $data['data'][] = [
                'year' 	=> $row['year'], // ปีที่คำนวณเงินปันผล (ปี พ.ศ.)
                'dividend' 	=> number_format($row['dividend_value'], 2), // เงินปันผล
                'dividend_avg' => number_format($row['average_return_value'], 2), // เงินเฉลี่ยคืน
                'dividend_sum' => number_format($row['dividend_value'] + $row['average_return_value'], 2) // รวม
            ];
        }
    }
    
    echo json_encode($data);
    exit();
?>