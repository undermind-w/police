<?php
    require_once("config.inc.php");

    $data = [ 'status' => 0, 'responseText' => '' ];

    require_once("parameter.inc.php");
    require_once("token.validate.php");

    $sql = "SELECT tb1.account_id,tb1.account_name,tb2.type_name FROM coop_maco_account AS tb1
    INNER JOIN coop_deposit_type_setting AS tb2 ON tb1.type_id = tb2.type_id
    WHERE trim(tb1.mem_id) = trim('{$member_id}') AND trim(tb1.account_status) = '0' ORDER BY tb1.created ASC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $data['status'] = 1;
        while( ($row = $rs->fetch_assoc()) ){
            $account_id = $row['account_id'];
            $sqlAcc = "SELECT transaction_balance FROM coop_account_transaction WHERE trim(account_id) = trim('$account_id') ORDER BY transaction_time DESC, transaction_id DESC";
            $rsAcc = $mysqli->query($sqlAcc);
            $rowAcc = $rsAcc->fetch_assoc();
            $data['data'][] = [
                'account_id' => $row['account_id'],
                'account_name' => $row['account_name'],
                'account_type' => $row['type_name'],
                'balance' => ( $rowAcc['transaction_balance'] > 0) ? number_format($rowAcc['transaction_balance'], 2) : '0.00'
            ];
        }
    } else {
        $data['responseText'] = 'ไม่พบบัญชีเงินฝาก';
    }

    echo json_encode($data); 
    exit();
?>