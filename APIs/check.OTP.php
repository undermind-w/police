<?php
    require_once("config.inc.php");

    $data = [ 'status' => 0, 'error_code' => 0, 'responseText' => '' ];

    $member = isset($_POST['member']) ? $mysqli->real_escape_string($_POST['member']) : null;
    $OTP = isset($_POST['OTP']) ? $mysqli->real_escape_string($_POST['OTP']) : null;
    $via = isset($_POST['via']) ? $mysqli->real_escape_string($_POST['via']) : null;

    $member_id = '';
    if ( strlen(trim($member)) == 1 ) {
        $member_id = '00000'.$member;
    } else if ( strlen(trim($member)) == 2 ) {
        $member_id = '0000'.$member;
    } else if ( strlen(trim($member)) == 3 ) {
        $member_id = '000'.$member;
    } else if ( strlen(trim($member)) == 4 ) {
        $member_id = '00'.$member;
    } else if ( strlen(trim($member)) == 5 ) {
        $member_id = '0'.$member;
    } else if ( strlen(trim($member)) == 6 ) {
        $member_id = $member;
    }

    if ( $via == "mobile" ) {

        $mobile_tokenId = isset($_POST['mobile_tokenId']) ? $mysqli->real_escape_string($_POST['mobile_tokenId']) : null;

        // $sql = "SELECT OTP,OTP_Start,OTP_End FROM mobile_token 
        // WHERE trim(member_id) = trim('{$member_id}') AND trim(mobile_uid) = trim('{$mobile_uid}') AND is_delete = 0
        // ORDER BY create_date DESC";
        $sql = "SELECT OTP,OTP_Start,OTP_End FROM mobile_token 
        WHERE trim(member_id) = trim('{$member_id}') AND mobile_id = {$mobile_tokenId} AND is_delete = 0
        ORDER BY create_date DESC";

        $rs = $mysqli->query($sql);
        if ( $rs->num_rows ) {
            $row = $rs->fetch_assoc();
            if ( trim($row['OTP']) == trim($OTP) ) {
                $end_date = date("Y-m-d H:i:s", strtotime($row['OTP_End']));
                if ( $end_date >= date("Y-m-d H:i:s") ) {
                    $data['status'] = 1;
                } else {    
                    $text = "Error: 101 (OTP หมดอายุ) / OTP Code : ".$OTP;
                    $sql = "INSERT INTO mobile_log (member_id,page,create_date,log_text)
                    VALUES('{$member_id}','OTP_mobile',NOW(),'{$text}')";
                    $mysqli->query($sql);

                    $data['error_code'] = 101;          
                    $data['responseText'] =  'OTP หมดอายุตั้งแต่ '.date("d-m-Y H:i:s",strtotime($end_date)).' น.';
                }
            } else {
                $text = "Error: 102 (OTP ไม่ตรงกัน) / OTP Code : ".$OTP." / Real OTP : ".trim($row['OTP']);
                $sql = "INSERT INTO mobile_log (member_id,page,create_date,log_text)
                VALUES('{$member_id}','OTP_mobile',NOW(),'{$text}')";
                $mysqli->query($sql);

                $data['error_code'] = 102;
                $data['responseText'] =  'OTP ไม่ตรงกัน';
            }
        } else {
            $text = "Error: 103 (ไม่พบ OTP นี้ใน Database) / OTP Code : ".$OTP;
            $sql = "INSERT INTO mobile_log (member_id,page,create_date,log_text)
            VALUES('{$member_id}','OTP_mobile',NOW(),'{$text}')";
            $mysqli->query($sql);

            $data['error_code'] = 103;
            $data['responseText'] =  'ไม่พบข้อมูลรหัส OTP นี้ในระบบ';
        }
        
    } else {

    }

    echo json_encode($data); 
    exit();
?>