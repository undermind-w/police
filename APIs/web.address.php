<?php
    require_once("config.inc.php");

    $data = [ 'province' => [], 'amphur' => [] ];

    $sql = "SELECT province_id, province_name FROM coop_province ORDER BY province_id ASC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        while( ($row = $rs->fetch_assoc()) ){
            $data['province'][] = [
                'province_id' => $row['province_id'],
                'province_name' => trim($row['province_name'])
            ];
        }
    }

    $sql = "SELECT province_id, amphur_id, amphur_name FROM coop_amphur ORDER BY amphur_id ASC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        while( ($row = $rs->fetch_assoc()) ){
            $data['amphur'][] = [
                'province_id' => $row['province_id'],
                'amphur_id' => $row['amphur_id'],
                'amphur_name' => trim($row['amphur_name'])
            ];
        }
    }

    $sql = "SELECT province_id, amphur_id, district_id, district_name FROM coop_district ORDER BY district_id ASC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        while( ($row = $rs->fetch_assoc()) ){
            $data['district'][] = [
                'province_id' => $row['province_id'],
                'amphur_id' => $row['amphur_id'],
                'district_id' => $row['district_id'],
                'district_name' => trim($row['district_name'])
            ];
        }
    }

    echo json_encode($data); 
    exit();
?>