<?php
    require_once("config.inc.php");

    $data = [ 'status' => 1, 'responseText' => '', 'is_data' => 0 ];

    require_once("parameter.inc.php");
    require_once("token.validate.php");

    $getYearLimit = (string)(date("Y") - 1).'-12-31';
    $sql = "SELECT share_collect FROM coop_mem_share WHERE trim(member_id) = trim('{$member_id}') 
    AND share_status IN('1','2') AND share_date <= '{$getYearLimit}'
    ORDER BY  share_date DESC LIMIT 1";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $row = $rs->fetch_assoc();
        $data['start_year_share'] = ( $row['share_collect'] > 0 ) ? number_format(($row['share_collect'] * 10), 2)." บาท" : number_format(0, 2)." บาท";
    } else {
        $data['start_year_share'] = number_format(0, 2)." บาท";
    }

    $sql = "SELECT share_collect FROM coop_mem_share WHERE trim(member_id) = trim('{$member_id}') AND share_status IN('1','2') ORDER BY  share_date DESC LIMIT 1";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $row = $rs->fetch_assoc();
        $data['share_collect'] = ( $row['share_collect'] > 0 ) ? number_format(($row['share_collect'] * 10), 2)." บาท" : number_format(0, 2)." บาท";
    } else {
        $data['share_collect'] = number_format(0, 2)." บาท";
    }
    
    $sql = "SELECT share_month FROM coop_mem_apply WHERE trim(member_id) = trim('{$member_id}')";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $row = $rs->fetch_assoc();
        $data['share_per_month'] = ( $row['share_month'] > 0 ) ? number_format($row['share_month'], 2)." บาท" : number_format(0, 2)." บาท";
    } else {
        $data['share_per_month'] = number_format(0, 2)." บาท";
    }

    $getYearLimit = (string)(date("Y") - 1).'-01-01';
    $sql = "SELECT tb1.payment_date,tb1.period_count,tb1.loan_amount_balance,tb1.receipt_id,tb1.total_amount,tb1.account_list_id,tb2.account_list
    FROM coop_finance_transaction AS tb1
    INNER JOIN coop_account_list AS tb2 ON tb1.account_list_id = tb2.account_id
    WHERE trim(member_id) = trim('{$member_id}') AND payment_date >= '{$getYearLimit}' AND tb1.account_list_id = 14
    OR trim(member_id) = trim('{$member_id}') AND payment_date >= '{$getYearLimit}' AND tb1.account_list_id = 16
    ORDER BY payment_date DESC, period_count DESC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $data['is_data'] = 1;
        while( ($row = $rs->fetch_assoc()) ){
            $data['transection'][] = [
                'payment_date' => date2thaiformat($row['payment_date']),
                'date_thai' => dateDB2thaidate($row['payment_date'],true,false,false),
                'account_list_id' => $row['account_list_id'],
                'account_list' => $row['account_list'],
                'period_count' => ( $row['period_count'] == null) ? 'N/A' : $row['period_count'],
                'loan_amount_balance' => ( $row['loan_amount_balance'] > 0 ) ? number_format($row['loan_amount_balance'], 2)." บาท" : number_format(0, 2)." บาท",
                'receipt_id' => ( $row['receipt_id'] == null) ? 'N/A' : $row['receipt_id'],
                'total_amount' => ( $row['total_amount'] > 0 ) ? number_format($row['total_amount'], 2)." บาท" : number_format(0, 2)." บาท"
            ];
        }
    }

    echo json_encode($data); 
    exit();
?>