<?php
    require_once("config.inc.php");

    $data = [ 'status' => 0, 'responseText' => '' ];

    require_once("parameter.inc.php");
    require_once("token.validate.php");

    $msg_id = isset($_POST['msg_id']) ? $mysqli->real_escape_string($_POST['msg_id']) : null;

    $sql = "SELECT msg_title, msg_message, create_date FROM mobile_message WHERE msg_id = {$msg_id}";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $row = $rs->fetch_assoc();
        $data['status'] = 1;
        $data['data'] = [
            'msg_title' => $row['msg_title'],
            'msg_message' => $row['msg_message'],
            'create_date' => dateDB2thaidate($row['create_date'],false,false)
        ];
    }

    echo json_encode($data);
    exit();
?>