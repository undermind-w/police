<?php
    require_once("config.inc.php");

    $data = [ 'status' => 1, 'responseText' => '', 'is_icon' => 0, 'background_img' => ONLINE_URL.'APIs/img/background_img.png' ];

    require_once("parameter.inc.php");
    require_once("token.validate.php");

    $sql = "SELECT firstname_th,lastname_th,sex,member_pic FROM coop_mem_apply WHERE trim(member_id) = trim('{$member_id}')";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $row = $rs->fetch_assoc();
        $user_img = '';
        if ( trim($row['member_pic']) == '' ) {
            $user_img = ( $row['sex'] == 'F' ) ? ONLINE_URL.'APIs/img/female.png' : ONLINE_URL.'APIs/img/male.png';
        } else {
            $user_img = SYSTEM_URL.'assets/uploads/members/'.$row['member_pic'];
        }
        $data['member_name'] = 'คุณ'.$row['firstname_th'].' '.$row['lastname_th'];
        $data['user_img'] = $user_img;

        $sql = "SELECT share_collect_value FROM coop_mem_share WHERE trim(member_id) = trim('{$member_id}') AND share_status IN('1','2') ORDER BY  share_date DESC LIMIT 1";
        $rs = $mysqli->query($sql);
        if ( $rs->num_rows ) {
            $row = $rs->fetch_assoc();
            $data['share_collect'] = ( $row['share_collect_value'] > 0 ) ? number_format($row['share_collect_value'], 2) : number_format(0, 2);
        } else {
            $data['share_collect'] = number_format(0, 2);
        }

        $deposit_balance = 0;
        $sql = "SELECT account_id FROM coop_maco_account WHERE trim(mem_id) = trim('{$member_id}') AND trim(account_status) = '0'";
        $rs = $mysqli->query($sql);
        if ( $rs->num_rows ) {
            while( ($row = $rs->fetch_assoc()) ){
                $account_id = $row['account_id'];
                $sqlAcc = "SELECT transaction_balance FROM coop_account_transaction WHERE trim(account_id) = trim('$account_id') ORDER BY transaction_time DESC, transaction_id DESC";
                $rsAcc = $mysqli->query($sqlAcc);
                $rowAcc = $rsAcc->fetch_assoc();
                $deposit_balance += $rowAcc['transaction_balance'];
            }
        }
        
        $data['deposit_sum'] = ( $deposit_balance > 0 ) ? number_format($deposit_balance, 2) : number_format(0, 2);

        // $sql = "SELECT tb2.transaction_balance FROM coop_maco_account AS tb1 
        // INNER JOIN coop_account_transaction AS tb2 ON tb1.account_id = tb2.account_id
        // WHERE trim(tb1.mem_id) = trim('{$member_id}')
        // ORDER BY tb2.transaction_id DESC LIMIT 1";
        // $rs = $mysqli->query($sql);
        // if ( $rs->num_rows ) {
        //     $row = $rs->fetch_assoc();
        //     $data['deposit_sum'] = ( $row['transaction_balance'] > 0 ) ? number_format($row['transaction_balance'], 2) : number_format(0, 2);
        // } else {
        //     $data['deposit_sum'] = number_format(0, 2);
        // }
    }

    // ICON
    $sql = "SELECT * FROM mobile_icon WHERE icon_active = 1 ORDER BY icon_sort ASC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $data['is_icon'] = 1;
        $data['count'] = $rs->num_rows;
        while( ($row = $rs->fetch_assoc()) ){
            $data['icon'][] = [
                'icon_img' => trim($row['icon_img']),
                'icon_name' => trim($row['icon_name']),
                'icon_shortname' => trim($row['icon_shortname'])
            ];
        }
    }
    // ICON

    echo json_encode($data); 
    exit();
?>