<?php
    require_once("config.inc.php");

    $data = [ 'status' => 0, 'responseText' => '' ];

    require_once("parameter.inc.php");
    require_once("token.validate.php");

    $data['data']['loan'] = [];
    $sql = "SELECT 
    tb1.id, 
    tb1.approve_date,
    IF(tb1.contract_number = '', 'N/A', tb1.contract_number) AS contract_number, 
    FORMAT(tb1.loan_amount, 2) AS loan_amount, 
    FORMAT(tb1.loan_amount_balance, 2) AS loan_amount_balance, 
    tb1.period_amount,
    tb1.loan_status,
    IF(tb1.loan_status = 1, 'ปกติ', 'เบี้ยวหนี้') AS loan_status_desc,
    tb1.pay_type,
    tb1.createdatetime,
    IF(tb1.pay_type = '1', 'N/A', (SELECT FORMAT(total_paid_per_month, 2) FROM coop_loan_period WHERE loan_id = tb1.id AND period_count = 2 LIMIT 1)) AS pay_per_month,
    tb3.loan_type
    FROM coop_loan AS tb1
    INNER JOIN coop_loan_name AS tb2 ON trim(tb1.loan_type) = trim(tb2.loan_name_id)
    INNER JOIN coop_loan_type AS tb3 ON tb2.loan_type_id = tb3.id
    WHERE trim(tb1.member_id) = trim('{$member_id}') AND trim(loan_status) = '1'
    OR trim(tb1.member_id) = trim('{$member_id}') AND trim(loan_status) = '6'
    ORDER BY createdatetime DESC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $data['status'] = 1;
        while( ($row = $rs->fetch_assoc()) ){
            $data['data']['loan'][] = [
                'id' => $row['id'],
                'approve_date' => ($row['approve_date'] == null) ? 'N/A' : dateDB2thaidate($row['approve_date'],true,false,false),
                'contract_number' => $row['contract_number'],
                'loan_amount' => $row['loan_amount'],
                'loan_amount_balance' => $row['loan_amount_balance'],
                'period_amount' => ($row['period_amount'] == null OR $row['period_amount'] == 0) ? 'N/A' : $row['period_amount'],
                'loan_status' => $row['loan_status'],
                'loan_status_desc' => $row['loan_status_desc'],
                'pay_type' => $row['pay_type'],
                'pay_per_month' => ( $row['pay_per_month'] ) ? $row['pay_per_month'] : 'N/A',
                'loan_type' => $row['loan_type'],
                'loan' => 'loan',
                'create_date' => $row['createdatetime']
            ];
        }
    }

    $data['data']['loan_atm'] = [];
    $atm_period = 'N/A';
    $sqlPeriod = "SELECT max_period FROM coop_loan_atm_setting LIMIT 1";
    $rsPeriod = $mysqli->query($sqlPeriod);
    if ( $rs->num_rows ) {
        $rowPeriod = $rsPeriod->fetch_assoc();
        $atm_period = $rowPeriod['max_period'];
    }

    $sql = "SELECT 
    tb1.loan_atm_id, 
    tb1.contract_number, 
    tb1.approve_date, 
    tb1.createdatetime,
    FORMAT(tb1.total_amount_approve, 2) AS loan_amount, 
    FORMAT(tb1.total_amount_balance, 2) AS loan_amount_balance,
    tb1.loan_atm_status,
    -- FORMAT(tb2.principal_per_month, 2) AS pay_per_month,
    IF(tb1.loan_atm_status = 1, 'ปกติ', 'เบี้ยวหนี้') AS loan_status_desc
    FROM coop_loan_atm AS tb1
    -- LEFT JOIN coop_loan_atm_detail AS tb2 ON tb1.loan_atm_id = tb2.loan_atm_id
    WHERE trim(tb1.member_id) = trim('{$member_id}') AND trim(tb1.loan_atm_status) = '1'
    OR trim(tb1.member_id) = trim('{$member_id}') AND trim(tb1.loan_atm_status) = '6'
    ORDER BY createdatetime DESC";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $data['status'] = 1;
        while( ($row = $rs->fetch_assoc()) ){

            $atm_id = $row['loan_atm_id'];

            $balance = '0.00';
            $sqlBalance = "SELECT FORMAT(loan_amount_balance, 2) AS balance FROM coop_loan_atm_transaction WHERE loan_atm_id = {$atm_id} ORDER BY transaction_datetime DESC LIMIT 1";
            $rsBalance = $mysqli->query($sqlBalance);
            if ( $rsBalance->num_rows ) {
                $rowBalance = $rsBalance->fetch_assoc();
                $balance = ( $rowBalance['balance'] ) ? $rowBalance['balance'] : '0.00';
            }

            $data['data']['loan_atm'][] = [
                'id' => $row['loan_atm_id'],
                'approve_date' => ($row['approve_date'] == null) ? 'N/A' : dateDB2thaidate($row['approve_date'],true,false,false),
                'contract_number' => $row['contract_number'],
                'loan_amount' => $row['loan_amount'],
                'loan_amount_balance' => $row['loan_amount_balance'],
                'period_amount' => $atm_period,
                'loan_status' => $row['loan_atm_status'],
                'loan_status_desc' => $row['loan_status_desc'],
                'pay_type' => 'N/A',
                'pay_per_month' => 'N/A',
                // 'pay_per_month' => $row['pay_per_month'],
                'loan_balance' => $balance,
                'loan_type' => 'เงินกู้ ATM',
                'loan' => 'loan_atm',
                'create_date' => $row['createdatetime']
            ];
        }
    }

    //array_push($data->data, []);

    echo json_encode($data);
    exit();
?>