<?php
    require_once("config.inc.php");

    $data = [ 'status' => 0, 'responseText' => '', 'is_response_gain' => '', 'is_response_diviend' => '' ];

    require_once("parameter.inc.php");
    require_once("token.validate.php");

    $sql = "SELECT 
    tb1.member_id,tb1.firstname_th,tb1.lastname_th,tb1.sex,tb1.member_pic,tb1.birthday,tb1.member_date,tb1.mobile,tb1.email,tb1.id_card,tb1.position,tb1.salary,tb1.other_income,tb1.tel,
    tb1.c_address_no,tb1.c_address_moo,tb1.c_address_village,tb1.c_address_road,tb1.c_address_soi,tb1.c_district_id,tb1.c_amphur_id,tb1.c_province_id,tb1.c_zipcode,
    tb1.address_no,tb1.address_moo,tb1.address_village,tb1.address_road,tb1.address_soi,tb1.district_id,tb1.amphur_id,tb1.province_id,tb1.zipcode,
    tb2.prename_short,
    tb3.district_name,tb7.district_name AS i_district_name,
    tb4.amphur_name,tb8.amphur_name AS i_amphur_name,
    tb5.province_name,tb9.province_name AS i_province_name,
    tb6.mem_type_name,
    tb10.mem_group_name AS Affiliation_1,
    tb11.mem_group_name AS Affiliation_2,
    tb12.mem_group_name AS Affiliation_3
    FROM coop_mem_apply AS tb1
    LEFT JOIN coop_prename AS tb2 ON tb2.prename_id = tb1.prename_id
    LEFT JOIN coop_district AS tb3 ON tb3.district_id = tb1.c_district_id
    LEFT JOIN coop_amphur AS tb4 ON tb4.amphur_id = tb1.c_amphur_id
    LEFT JOIN coop_province AS tb5 ON tb5.province_id = tb1.c_province_id
    LEFT JOIN coop_mem_type AS tb6 ON tb6.mem_type_id = tb1.mem_type_id
    LEFT JOIN coop_district AS tb7 ON tb7.district_id = tb1.district_id
    LEFT JOIN coop_amphur AS tb8 ON tb8.amphur_id = tb1.amphur_id
    LEFT JOIN coop_province AS tb9 ON tb9.province_id = tb1.province_id
    LEFT JOIN coop_mem_group AS tb10 ON tb10.id = tb1.department
    LEFT JOIN coop_mem_group AS tb11 ON tb11.id = tb1.faction
    LEFT JOIN coop_mem_group AS tb12 ON tb12.id = tb1.level
    WHERE trim(member_id) = trim('{$member_id}')";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $data['status'] = 1;
        $row = $rs->fetch_assoc();
        $user_img = '';
        if ( trim($row['member_pic']) == '' ) {
            //$user_img = ( $row['sex'] == 'F' ) ? ONLINE_URL.'APIs/img/user_female.jpg' : ONLINE_URL.'APIs/img/user_male.jpg';
            $user_img = ( $row['sex'] == 'F' ) ? ONLINE_URL.'APIs/img/female.png' : ONLINE_URL.'APIs/img/male.png';
        } else {
            $user_img = ONLINE_URL.'assets/uploads/members/'.$row['member_pic'];
        }

        $no = trim($row['c_address_no']);
        $village = ' '.trim($row['c_address_village']);
        $moo = ( $row['c_address_moo'] == '' ) ? '' : ' '.trim($row['c_address_moo']);
        $road = ( $row['c_address_road'] == '' ) ? '' : ' '.trim($row['c_address_road']);
        $soi = ( $row['c_address_soi'] == '' ) ? '' : ' '.trim($row['c_address_soi']);
        $district = '';
        if ( $row['c_province_id'] == 1 ) {
            $district = ' '.trim($row['district_name']);
            $amphur = ' '.trim($row['amphur_name']);
            $province = ' '.trim($row['province_name']);
        } else {
            $district = ( $row['district_name'] == '' ) ? '' : ' ต.'.trim($row['district_name']);
            $amphur = ( $row['amphur_name'] == '' ) ? '' : ' อ.'.trim($row['amphur_name']);
            $province = ( $row['province_name'] == '' ) ? '' : ' จ.'.trim($row['province_name']);
        }
        $zipcode = ' '.trim($row['c_zipcode']);

        // --------------------------------------------

        $i_no = trim($row['address_no']);
        $i_village = ' '.trim($row['address_village']);
        $i_moo = ( $row['address_moo'] == '' ) ? '' : ' '.trim($row['address_moo']);
        $i_road = ( $row['address_road'] == '' ) ? '' : ' '.trim($row['address_road']);
        $i_soi = ( $row['address_soi'] == '' ) ? '' : ' '.trim($row['address_soi']);
        $i_district = '';
        if ( $row['province_id'] == 1 ) {
            $i_district = ' '.trim($row['i_district_name']);
            $i_amphur = ' '.trim($row['i_amphur_name']);
            $i_province = ' '.trim($row['i_province_name']);
        } else {
            $i_district = ( $row['i_district_name'] == '' ) ? '' : ' ต.'.trim($row['i_district_name']);
            $i_amphur = ( $row['i_amphur_name'] == '' ) ? '' : ' อ.'.trim($row['i_amphur_name']);
            $i_province = ( $row['i_province_name'] == '' ) ? '' : ' จ.'.trim($row['i_province_name']);
        }
        $i_zipcode = ' '.trim($row['zipcode']);

        $affiliation_1 = (trim($row['Affiliation_1']) == '' || trim($row['Affiliation_1']) == 'ไม่ระบุ' || trim($row['Affiliation_1']) == null) ? '' : trim($row['Affiliation_1']);
        $affiliation_2 = (trim($row['Affiliation_2']) == '' || trim($row['Affiliation_2']) == 'ไม่ระบุ' || trim($row['Affiliation_2']) == null) ? '' : trim($row['Affiliation_2']);
        $affiliation_3 = (trim($row['Affiliation_3']) == '' || trim($row['Affiliation_3']) == 'ไม่ระบุ' || trim($row['Affiliation_3']) == null) ? '' : trim($row['Affiliation_3']);

        $comma_1 =  '';
        if ( $affiliation_1 != '' && $affiliation_2 != '' ) $comma_1 = ', ';

        $comma_2 =  '';
        if ( $affiliation_2 != '' && $affiliation_3 != '' ) $comma_2 = ', ';
        if ( $affiliation_1 != '' && $affiliation_2 == '' && $affiliation_3 != '' ) $comma_2 = ', ';

        $data['data'] = [
            'user_img' => $user_img,
            'member_id' => $row['member_id'],
            'member_name' => $row['prename_short']." ".$row['firstname_th']." ".$row['lastname_th'],
            'mem_type_name' => $row['mem_type_name'],
            'position' => $row['position'],
            'salary' => ( $row['salary'] > 0 ) ? number_format($row['salary'], 2) : "0.00",
            'other_income' => ( $row['other_income'] > 0 ) ? number_format($row['other_income'], 2) : "0.00",
            'id_card' => $row['id_card'],
            'birthday' => dateDB2thaidate($row['birthday'],true,false,false),
            'age' => calcDate($row['birthday']),
            'member_date' => dateDB2thaidate($row['member_date'],true,false,false),
            'registerDate' => calcDate($row['member_date']),
            'email' => $row['email'],
            'tel' => $row['tel'],
            'mobile' => $row['mobile'],
            'address' => trim($no.$moo.$village.$road.$soi.$district.$amphur.$province.$zipcode),
            'i_address' => trim($i_no.$i_moo.$i_village.$i_road.$i_soi.$i_district.$i_amphur.$i_province.$i_zipcode),
            'affiliation' => $affiliation_1.$comma_1.$affiliation_2.$comma_2.$affiliation_3,
            'edit_data' => [
                'address_no' => $row['c_address_no'],
                'address_moo' => $row['c_address_moo'],
                'address_village' => $row['c_address_village'],
                'address_road' => $row['c_address_road'],
                'address_soi' => $row['c_address_soi'],
                'district_id' => $row['c_district_id'],
                'amphur_id' => $row['c_amphur_id'],
                'province_id' => $row['c_province_id'],
                'zipcode' => $row['c_zipcode']
            ]
        ];

        $sql = "SELECT CONCAT( tb3.prename_short,' ',tb1.g_firstname,' ',tb1.g_lastname ) AS gain_name,tb2.relation_name,
        CASE 
            WHEN tb1.g_mobile = '' THEN IF(tb1.g_tel = '',tb1.g_office_tel,tb1.g_tel)
            ELSE tb1.g_mobile
        END AS gain_mobile,
        CONCAT( tb1.g_share_rate, '%' ) AS gain_percent
        FROM coop_mem_gain_detail AS tb1
        LEFT JOIN coop_mem_relation AS tb2 ON tb1.g_relation_id = tb2.relation_id
        LEFT JOIN coop_prename AS tb3 ON tb1.g_prename_id = tb3.prename_id
        WHERE tb1.member_id = '{$member_id}' ORDER BY g_create ASC";
        $rs = $mysqli->query($sql);
        if ( $rs->num_rows ) {
            while( ($row = $rs->fetch_assoc()) ){
                $data['gain'][] = $row;
            }
        } else {
            $data['is_response_gain'] = 'ไม่พบผู้รับผลประโยชน์';
        }

        $sql = "SELECT year FROM coop_dividend_average WHERE trim(member_id) = '{$member_id}' ORDER BY year DESC";
        $rs = $mysqli->query($sql);
        if ( $rs->num_rows ) {
            while(( $row = $rs->fetch_assoc() )) {
                $data['diviend'][] = [
                    'year'  =>  (string)($row['year'] + 543),
                    'yearText'  =>  'ปี '.(string)($row['year'] + 543)
                ];
            }
        } else {
            $data['is_response_diviend'] = 'ไม่พบเงินปันผลเฉลี่ยคืน';
        }

    } else {
        $data['responseText'] = 'ไม่พบข้อมูลสมาชิก';
    }

    echo json_encode($data); 
    exit();
?>