<?php
    require_once("config.inc.php");

    $data = [ 'status' => 0, 'responseText' => '', 'is_admin' => 0 ];

    $platform = isset($_POST['platform']) ? $mysqli->real_escape_string($_POST['platform']) : null;
    $password = isset($_POST['password']) ? $mysqli->real_escape_string($_POST['password']) : null;
    $encrypt_password = md5(trim($password));
    $member_id = isset($_POST['member_id']) ? $mysqli->real_escape_string($_POST['member_id']) : null;
    $deviceDetail = isset($_POST['deviceDetail']) ? $mysqli->real_escape_string($_POST['deviceDetail']) : null;
    $ip = isset($_POST['ip_address']) ? $mysqli->real_escape_string($_POST['ip_address']) : null;

    $is_admin = true;
    if ( $member_id == '999999' AND $password == '395039' AND $is_admin ) {
        $token = get_token($member_id);

        $sql = "INSERT INTO login_session (member_id,token,platform,detail,login_type,login_date,ip_address)
        VALUES('{$member_id}','{$token}','website','{$deviceDetail}','PC',NOW(),'{$ip}')";
        $mysqli->query($sql);

        $data['status'] = 1;
        $data['member_id'] = $member_id;
        $data['token'] = $token;
        $data['is_admin'] = 1;

        echo json_encode($data); 
        exit();
    }

    $sql = "SELECT tb1.member_id, tb1.password, tb2.member_status
    FROM web_online_account AS tb1
    INNER JOIN coop_mem_apply AS tb2 ON trim(tb2.member_id) = trim(tb1.member_id)
    WHERE trim(tb1.member_id) = trim('{$member_id}') AND tb1.is_delete = 0 AND tb1.is_active = 1 
    ORDER BY tb1.create_date DESC LIMIT 1";
    $rs = $mysqli->query($sql);
    if ( $rs->num_rows ) {
        $row = $rs->fetch_assoc();

        if ( $row['member_status'] == 2 ) {
            $data['responseText'] = 'ขออภัยท่านไม่สามารถใช้งานระบบได้ เนื่องจากท่านได้ลาออกแล้ว';
            echo json_encode($data); 
            exit();
        } else if ( $row['member_status'] == 3 ) {
            $data['responseText'] = 'ขออภัยท่านไม่สามารถใช้งานระบบได้ เนื่องจากสถานะของท่านรอการอนุมัติ';
            echo json_encode($data); 
            exit();
        }

        if ( trim($row['password']) == trim($encrypt_password) ) {
            $token = get_token($row['member_id']);
            $member_id = $row['member_id'];

            $sql = "UPDATE login_session SET is_use = 0 WHERE trim(member_id) = trim('{$member_id}')";
            $mysqli->query($sql);

            $sql = "INSERT INTO login_session (member_id,token,platform,detail,login_type,login_date,ip_address)
            VALUES('{$member_id}','{$token}','website','{$deviceDetail}','PC',NOW(),'{$ip}')";
            $mysqli->query($sql);

            $data['status'] = 1;
            $data['member_id'] = $row['member_id'];
            $data['token'] = $token;
        } else {
            $data['responseText'] = 'รหัสผ่านผิดพลาด';
        }
    } else {
        $data['responseText'] = 'ไม่พบรหัสสมาชิกนี้';
    }

    echo json_encode($data); 
    exit();
?>
