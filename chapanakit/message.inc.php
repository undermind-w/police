<?php 
		$id = isset($_GET["mid"]) ? $_GET["mid"] : 0 ;
		$sql = "INSERT INTO cmp_imp_memdetail_message(crmem_id, msg_id, is_read, update_time, update_ip)
							VALUES('{$_COOKIE["crmem_id"]}', {$id}, 1, NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
		$rs = $mysqli->query($sql);
		if(!$rs) {
				$sql = "UPDATE cmp_imp_memdetail_message SET
									is_read 			= 1
									, update_time = NOW()
									, update_ip 	= '{$_SERVER["REMOTE_ADDR"]}'
								WHERE crmem_id = '{$_COOKIE["crmem_id"]}'
									AND msg_id = {$id}";
				@$mysqli->query($sql);
		}
		$sql = "SELECT * 
						FROM cmp_cpk_message
						WHERE msg_id = {$id}";
		$rs = $mysqli->query($sql);
		$msg = $rs->fetch_assoc();
?>
<div class="panel panel-material-light-blue-300">
		<div class="panel-heading">
				<div class="">
						<div class="pull-left">
								<span class="fa-stack fa-2x">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-envelope fa-stack-1x text-material-light-blue-300"></i>
								</span>
						</div>
						<div class="pull-left m-l-xxs">
								<h3>ข้อความถึงท่าน</h3>
						</div>
						<div class="clearfix"></div>
				</div>
		</div>
		<div class="panel-body line-h-26">
				<h4><?php echo $msg["msg_title"]; ?></h4>
				<p><?php echo $msg["msg_detail"]; ?></p>
				<?php
						if($msg["msg_url"]) {
							echo "<div class='text-center m-t-xxl'><a class='btn btn-material-light-blue-300' href='./?do=msg&id={$msg["msg_id"]}' target='_blank'>ดูรายละเอียดเพิ่มเติม</a></div>";
						}
				?>
		</div>
</div>
<?php unset($sql, $rs, $msg); ?>