<?php
		include "config.inc.php";

		$crmem_id 	= isset($_COOKIE["crmem_id"]) ? $_COOKIE["crmem_id"] : null;
		$token 			= isset($_SESSION["cpk_token"]) ? $_SESSION["cpk_token"] : null;
		$mysqli->query("UPDATE cmp_imp_memdetail_login_session SET is_logout = 1, logout_time = NOW(), logout_ip = '{$_SERVER["REMOTE_ADDR"]}' WHERE crmem_id = '{$crmem_id}' AND token = '{$token}'");

		setcookie("crmem_id", ""	, time() - 3600, "/", ".cmcoop.or.th");
		setcookie("token", ""			, time() - 3600, "/", ".cmcoop.or.th");

		header("location: login.php");