<?php
	include "config.inc.php";
	if(!isset($_GET["id"])) exit();
	
	include "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/fpdf.php";
	
	function U2T($text) { return iconv("UTF-8", "TIS-620", trim($text)); }
	function T2U($text) { return iconv("TIS-620", "UTF-8", trim($text)); }
	function set_color_cation() { global $pdf; $pdf->SetTextColor(28, 147, 83); }
	function set_color_text() { global $pdf; $pdf->SetTextColor(130, 119, 135); }
	
	$sql = "SELECT * FROM cmp_cpk_website WHERE st_id = 1 " ;
	$rs = $mysqli->query($sql);
	$setting =  $rs->fetch_assoc();
	
	$receiptdoc_id = $_GET["id"];
	
	$margin_left 		= 3;
	$margin_right 	= 3;
	$page_width 		= 127;
	$page_height 		= 178;
	$line_height 		=	6;
	
	$pdf = new FPDF('P', 'mm', array($page_width, $page_height));
	$pdf->AddPage();
	$pdf->SetDisplayMode("real", "single");
	$pdf->SetAutoPageBreak(false);
	$pdf->SetLeftMargin(0);
	$pdf->SetRightMargin(0);
	
	//$pdf->SetLineWidth(0.5);
	$pdf->Rect(0, 0, $page_width, $page_height);
	
	$pdf->AddFont('angsa'	, null, 'angsa.php');
	$pdf->AddFont('angsab', null, 'angsab.php');
	
	$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/receipt.wf.png", 10, 4, 15);
	
	$wt_width = 60;
	$wt_x = ( $page_width / 2 ) - ( $wt_width / 2 );
	$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/receipt_bg.wf.png", $wt_x, 55,$wt_width,0, 'PNG');
	
	$pdf->SetFont('angsab', null, 18);
	$pdf->SetXY(0, 5);
	$text = U2T("สมาคมฌาปนกิจสงเคราะห์");
	$pdf->Cell($page_width, 8, $text, 0, 1, "C", false);
	$text = U2T("สหกรณ์ออมทรัพย์ครูเชียงใหม่ จำกัด");
	$pdf->Cell($page_width, 8, $text, 0, 1, "C", false);
	
	$pdf->SetFont('angsab', 'U', 15);
	$pdf->SetX(0);
	$text = U2T("ใบเสร็จรับเงิน");
	$pdf->Cell($page_width, 6, $text, 0, 1, "C", false);
	
	$sql = "SELECT cmp_imp_receipt.*, cmp_imp_memdetail.mem_id, cmp_imp_memdetail.fullname_desc, cmp_imp_memdetail.memtyp_desc
				FROM cmp_imp_receipt
					INNER JOIN cmp_imp_memdetail ON cmp_imp_receipt.crmem_id = cmp_imp_memdetail.crmem_id
				WHERE cmp_imp_receipt.receiptdoc_id = '{$receiptdoc_id}'";
	$rs_receipt = $mysqli->query($sql);
	$receipt = $rs_receipt->fetch_assoc();
	
	$operdtm = explode(" ", $receipt["receipt_dt"]);
	$operdtm = explode("-", $operdtm[0]);
	$operdtm[0] += 543;
	$year = $operdtm[0];
	$month = $operdtm[1];
	$month_name = MonthName((int)$month);
	$total = $receipt["item_val"];
	
	unset($sql, $rs);
	
	$pdf->SetY($pdf->GetY() - 3);
	
	/*
	 *		Header Config
	 */
	$caption_right	=	43;
	$content_right	=	23;
	
	$pdf->SetFont('angsa', null, 13);
	$pdf->SetX($margin_left);
	$pdf->write($line_height, U2T("เลขที่"));
	$pdf->SetX($pdf->GetX() + 5);
	$pdf->write($line_height, U2T($receipt["receiptdoc_id"]));
	$pdf->SetX($page_width - $caption_right);
	$pdf->write($line_height, U2T("วันที่"));
	$pdf->SetX($page_width - $content_right);
	$pdf->Cell($content_right, $line_height, U2T("{$operdtm[2]}/{$operdtm[1]}/{$operdtm[0]}"), 0, 1, "L", false);
	
	$pdf->SetX($margin_left);
	$pdf->write($line_height, U2T("ได้รับเงินจาก"));
	$pdf->SetX($pdf->GetX() + 5);
	$pdf->write($line_height, U2T($receipt["fullname_desc"]));
	$pdf->SetX($page_width - $caption_right);
	$pdf->write($line_height, U2T("เลขทะเบียน"));
	$pdf->SetX($page_width - $content_right);
	$pdf->Cell($content_right, $line_height, U2T($receipt["crmem_id"]), 0, 1, "L", false);
	
	$pdf->SetX($margin_left);
	$pdf->write($line_height, U2T("สังกัด"));
	$pdf->SetX($pdf->GetX() + 5);
	$pdf->write($line_height, U2T($receipt["memgrp_id"]." ".$receipt["memgrp_desc"]));
	$pdf->Ln();
	
	$txt_payname = "";
	if($receipt["memtyp_desc"] != "สมาชิกปกติ") {
		$sql = "SELECT * FROM cmp_imp_memdetail WHERE mem_id = '{$receipt["mem_id"]}' AND memtyp_desc = 'สมาชิกปกติ'";
		$rs = $mysqli->query($sql);
		$row = $rs->fetch_assoc();
		$txt_payname = "หักเงินจาก ".$row["fullname_desc"];
		$pdf->SetX($margin_left);
		$pdf->write($line_height, U2T($txt_payname));
		$pdf->Ln();
	}
	
	$sql = "SELECT *
				FROM cmp_imp_death
				WHERE death_prd = '".$receipt["invoices_prd"]."'
				ORDER BY death_no";
	$rs = $mysqli->query($sql);
	
	$pdf->SetY($pdf->GetY() + 5);
	$pdf->SetX($margin_left);
	$pdf->Cell($content_right, $line_height, U2T("รายชื่อผู้เสียชีวิตดังต่อไปนี้".str_repeat(" ", 8).$month_name." ".$year), 0, 1, "L", false);
	
	$pdf->SetFont('angsa', null, 12);
	$tmp_y2 = $pdf->GetY();
	$i = 1;
	while(($row = $rs->fetch_assoc())) {
		$tmp_y = $pdf->GetY();
		$pdf->SetX($margin_left + ($i % 2 == 0 ? 60 : 5));
		$pdf->MultiCell(($i % 2 == 0 ? 60 : 55), $line_height - 1, U2T("{$row["death_no"]}. {$row["fullname_desc"]} ({$row["memgrp_desc"]})"), 0, "L", false);
		$tmp_y2 = $pdf->GetY() > $tmp_y2 ? $pdf->GetY() : $tmp_y2;
		if($i % 2 == 0) {
			$pdf->SetY($tmp_y2);
		}
		else {
			$pdf->SetY($tmp_y);
		}
		$i++;
	}
	$pdf->SetY($tmp_y2);
	
	$pdf->SetFont('angsa', null, 13);
	$pdf->SetX($margin_left);
	$pdf->write($line_height, U2T($receipt["inv_desc"]));
	$pdf->SetX($page_width - $content_right);
	$pdf->Cell($content_right, $line_height, number_format($receipt["item_val"], 2), 0, 1, "L", false);
	
	while($receipt = $rs_receipt->fetch_assoc()) {
		$total += $receipt["item_val"];
		$pdf->SetX($margin_left);
		$pdf->write($line_height, U2T($receipt["inv_desc"]));
		$pdf->SetX($page_width - $content_right);
		$pdf->Cell($content_right, $line_height, number_format($receipt["item_val"], 2), 0, 1, "L", false);
	}
	
	$y_table_end = 140;
	$pdf->SetY($y_table_end);
	$pdf->SetX($margin_left);
	$pdf->Cell(81, $line_height * 2, U2T(" ".convert($total)), 1, 0, "L", false);
	$pdf->Cell(20, $line_height * 2, U2T("รวมเงิน"), 1, 0, "C", false);
	$pdf->Cell(20, $line_height * 2, number_format($total, 2)." ", 1, 0, "R", false);
	$pdf->Ln();
	$pdf->SetY($page_height - 18);
	$pdf->SetX($margin_left);
	$pdf->Cell(($page_width - 10) / 2, $line_height, U2T("เหรัญญิก"), 0, 0, "R", false);
	$pdf->Cell(($page_width - 0) / 2, $line_height, U2T("นายกสมาคมฯ"), 0, 0, "R", false);
	$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/uploads/".$setting["st_sign_02"], 20, $page_height - 25, 15);
	$pdf->Image("{$_SERVER["DOCUMENT_ROOT"]}/uploads/".$setting["st_sign_01"], 80, $page_height - 25, 15);
	
	$pdf->Text(18, $page_height - 6, U2T("(".$setting["st_signname_02"].")"));
	$pdf->Text(78, $page_height - 6, U2T("(".$setting["st_signname_01"].")"));
	
	//$pdf->SetXY($margin_left, $page_height - 8);
	//$pdf->Cell($page_width / 2, $line_height, U2T("(นายนพพร อารีย์)"), 0, 0, "C", false);
	//$pdf->Cell($page_width / 2, $line_height, U2T("(นายธนู มีสัตย์)"), 0, 0, "C", false);
	
	
	$pdf->SetFont('angsa', null, 18);
	$pdf->SetTextColor(130, 119, 135);
	$pdf->SetXY(0, $y_table_end - 22);
	$pdf->Cell($page_width, $line_height + 0.5, U2T('เพื่อความสะดวกและรวดเร็ว'), 0, 1, "C", false);
	$pdf->Cell($page_width, $line_height + 0.5, U2T('โปรดตรวจสอบรายการให้ถูกต้อง'), 0, 1, "C", false);
	$pdf->Cell($page_width, $line_height + 0.5, U2T('หากไม่ถูกต้องกรุณาแจ้งภายใน 5 วัน ทำการ'), 0, 1, "C", false);
	
	if(isset($_GET["member_no"])) {
			$pdf->Output("{$receiptdoc_id}.pdf" , 'D');
	} else {
			$pdf->Output();
	}