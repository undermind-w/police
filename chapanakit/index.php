<?php
	require "config.inc.php";
	require "session.inc.php";
	
	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	if($do == "msg") {
			$id = isset($_GET["id"]) ? $_GET["id"] : 0 ;
			
			$sql = "INSERT INTO cmp_imp_memdetail_message(crmem_id, msg_id, is_read, update_time, update_ip)
								VALUES('{$_COOKIE["crmem_id"]}', {$id}, 1, NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
			$rs = $mysqli->query($sql);
			if(!$rs) {
					$sql = "UPDATE cmp_imp_memdetail_message SET
										is_read 			= 1
										, update_time = NOW()
										, update_ip 	= '{$_SERVER["REMOTE_ADDR"]}'
									WHERE crmem_id = '{$_COOKIE["crmem_id"]}'
										AND msg_id = {$id}";
					@$mysqli->query($sql);
			}
			
			$sql = "SELECT msg_url
							FROM cmp_cpk_message
							WHERE msg_id = {$id}";
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc();
			if(substr($row["msg_url"], 0, 7) != "http://") $row["msg_url"] = "http://{$row["msg_url"]}";
			header("location:{$row["msg_url"]}");
			exit();
	}
	
	$smarty->assign("title", "งาน สคช.");
	
	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
	<script>
			$(document).ready(function() {

			});
	</script>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	require "subject.inc.php";
	//$smarty->assign("subject", "CMCOOP Online");
	//$smarty->assign("breadcrumb", array("เข้าสู่ระบบ" => null ));
	ob_start();
?>
	<div class="row m-t-md">
		<div class="col-md-3">
			<h2>ยินดีต้อนรับ</h2>
			<h4>
					<?php
						$sql = "SELECT *
										FROM cmp_imp_cpk_stat
										ORDER BY update_time DESC
										LIMIT 1";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
					?>
					อัพเดท <?php echo mysqldate2thaidate($row["update_time"], false, true); ?>
			</h4>
			<?php require "sidebar.inc.php"; ?>
		</div>
		<div class="col-md-9">
			<h2>&nbsp;</h2>
			<h4>&nbsp;</h4>
			<?php include "profile.inc.php"; ?>
		</div>
	</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");