<?php
	require "config.inc.php";

	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	if($do == "chk-email") {
			$email = $mysqli->real_escape_string($_POST["email"]);
			$sql = "SELECT email FROM cmp_imp_memdetail_data WHERE email = '{$email}'";
			$rs = $mysqli->query($sql);
			if(!$rs->num_rows) $response = array("valid" => true);
			else {
				$row = $rs->fetch_assoc();
				$response = array("valid" => false, "message" => "email ซ้ำ");
			}
			echo json_encode($response);
			exit();
	}

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($do == "load-content") {
			ob_start();
			
			$num_rows = 0;
			$ref = isset($_POST["ref"]) ? $_POST["ref"] : null;
			if(isset($_COOKIE["crmem_id"]) || isset($_SESSION["cpk_token"])) {
				$crmem_id 	= isset($_COOKIE["crmem_id"]) ? $mysqli->real_escape_string($_COOKIE["crmem_id"]) : null;
				$token 			= isset($_SESSION["cpk_token"]) ? $mysqli->real_escape_string($_SESSION["cpk_token"]) : null;
				$sql = "SELECT crmem_id FROM cmp_imp_memdetail_login_session WHERE crmem_id = '{$crmem_id}' AND token = '{$token}'";
				$rs = $mysqli->query($sql);
				$num_rows = $rs->num_rows;
			}
			
			//if(!isset($_COOKIE["crmem_id"]) || !isset($_COOKIE["token"])) {
			if(!$num_rows) {
?>
<h2 class="text-center f-bold">เข้าสู่ระบบ งาน สคช.</h2>
<h4 class="text-center">กรุณากรอกเลขที่สมาชิกสมาคม 6 หลัก และรหัสผ่าน</h4>
<h4 class="text-center">ในกรณีเข้าสู่ระบบครั้งแรกรหัสผ่านให้กรอกเลขที่บัตรประชาชน 13 หลัก</h4>
<form id="frm" class="form-horizontal">
		<div class="form-group">
				<label for="crmem_id" class="col-sm-1 col-sm-offset-2 control-label"><i class="fa fa-user"></i></label>
				<div class="col-sm-7">
						<input type="text" class="form-control" id="crmem_id" name="crmem_id" maxlength="6" value="" placeholder="เลขที่สมาชิกสมาคม 6 หลัก" />
				</div>
		</div>
		<div class="form-group">
				<label for="password" class="col-sm-1 col-sm-offset-2 control-label"><i class="fa fa-key"></i></label>
				<div class="col-sm-7">
						<input type="password" class="form-control" id="password" name="password" value="" placeholder="รหัสผ่าน" />
						<h6 class="help-block text-right text-material-light-blue-300"><a href="fwdpwd.php">ลืมหรือไม่ทราบรหัสผ่านคลิกที่นี่</a></h6>
				</div>
		</div>
		 <div class="form-group">
				<div class="col-sm-12 text-center">
						<button type="button" id="btn-submit" class="btn btn-material-light-blue-300">เข้าสู่ระบบ</button>
						<input type="hidden" name="ref" value="<?php echo $ref; ?>" />
				</div>
		</div>
		<div id="login-msg"></div>
</form>
<?php
			} else {
					$sql = "SELECT fullname_desc
									FROM cmp_imp_memdetail
									WHERE crmem_id = '{$_COOKIE["crmem_id"]}'";
					$rs = $mysqli->query($sql);
					$row = $rs->fetch_assoc();
					$fullname_desc = str_replace(array('นาย ', 'นางสาว ', 'นาง '), array('', '', ''), $row["fullname_desc"]);
					unset($sql, $rs, $row);
?>
<h2 class="text-center f-bold">ตรวจสอบข้อมูลของท่านอีกครั้ง</h2>
<h4 class="text-center">ยินดีต้อนรับคุณ <?php echo $fullname_desc; ?></h4>
<h4 class="text-center">ขั้นตอนสุดท้ายนี้ระบบจะให้ท่านกำหนดรหัสผ่านและอีเมล์</h4>
<form id="frm-confirm" class="form-horizontal" method="post">
		<div class="form-group">
				<label for="crmem_id" class="col-sm-2 control-label"><i class="fa fa-user"></i></label>
				<div class="col-sm-10 control-label">
						<div class="text-left"><?php echo $_COOKIE["crmem_id"]; ?></div>
				</div>
		</div>
		<div class="form-group">
				<label for="password" class="col-sm-2 control-label"><i class="fa fa-asterisk"></i></label>
				<div class="col-sm-10">
						<input type="password" class="form-control" id="password" name="password" value="" placeholder="กำหนดรหัสผ่าน" />
				</div>
		</div>
		<div class="form-group">
				<label for="password" class="col-sm-2 control-label"><i class="fa fa-asterisk"></i></label>
				<div class="col-sm-10">
						<input type="password" class="form-control" id="password_cf" name="password_cf" value="" placeholder="ป้อนรหัสผ่านอีกครั้ง" />
				</div>
		</div>
		<div class="form-group">
				<label for="password" class="col-sm-2 control-label"><i class="fa fa-envelope"></i></label>
				<div class="col-sm-10">
						<input type="email" class="form-control" id="email" name="email" value="" placeholder="อีเมล์" />
						* หากไม่มี อีเมล์ ไม่จำเป็นต้องกรอก
				</div>
		</div>
		 <div class="form-group">
				<div class="col-sm-12 text-center">
						<input type="hidden" name="do" value="confirm" />
						<input type="hidden" name="ref" value="<?php echo $ref; ?>" />
						<button type="submit" id="btn-confirm" class="btn btn-material-light-blue-300">บันทึกข้อมูล</button>
				</div>
		</div>
</form>
<script type="text/javascript">
		$(document).ready(function() {
				$('#frm-confirm').formValidation({
						fields: {
												email: {
														validators: {
																/*notEmpty		: { message: '<p>กรุณาระบุอีเมล์.</p>' },*/
																regexp			: { message: 'ภาษาอังกฤษเท่านั้น', regexp: /^[a-zA-Z0-9_.@-]+$/i },
																emailAddress: { message: '<p>รูปแบบอีเมล์ไม่ถูกต้อง.</p>' },
																remote			: { url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1] + "?do=chk-email" , type: 'POST' }
														}
												},
												password: {
														validators: {
																notEmpty		: { message: '<p>กรุณาระบุรหัสผ่าน.</p>' },
																stringLength: { message: '<p>กรุณาระบุรหัสผ่านอย่างน้อย 6 ตัวขึ้นไป.</p>', min: 6 },
																identical		: { message: 'รหัสผ่าน และยืนยันรหัสผ่านไม่ตรงกัน', field: 'password_cf' }
														}
												},
												password_cf: {
														validators: {
																notEmpty		: { message: '<p>กรุณาระบุรหัสผ่านอีกครั้ง.</p>' },
																stringLength: { message: '<p>กรุณาระบุรหัสผ่านอย่างน้อย 6 ตัวขึ้นไป.</p>', min: 6,
																								identical: { message: 'รหัสผ่าน และยืนยันรหัสผ่านไม่ตรงกัน', field: 'password' }
																							}
														}
												}
						}

				});
				/*
				$("body").on("click", "#btn-confirm", function() {
						//formValidation('validate');
						$("#frm-confirm").data('formValidation').validate();
				});
				*/
		});
</script>
<?php
			}
			$response = ob_get_contents();
			ob_end_clean();
			echo $response;
			exit();
	}

	/*
	 *	@ table cmp_login_session
	 *		@ crmem_id 						varchar(10)
	 *		@ token 								varchar(50)
	 *		@ create_time						datetime
	 *		@ create_ip							varchar(15)
	 *		@ last_access_time			datetime
	 *		@ last_access_ip				varchar(15)
	 *
	 */
	$crmem_id = isset($_POST["crmem_id"]) ? $mysqli->real_escape_string($_POST["crmem_id"]) : null ;
	if($crmem_id) {
			$ref 			= $mysqli->real_escape_string($_POST["ref"]);
			$password = $mysqli->real_escape_string($_POST["password"]);
			$response["data"] = null;
			$response["is_first"] = null;
			$sql = "SELECT tb1.crmem_id
								, CASE WHEN tb2.password IS NULL THEN 1 ELSE 0 END AS is_first
							FROM cmp_imp_memdetail AS tb1
							LEFT OUTER JOIN cmp_imp_memdetail_data AS tb2 ON tb1.crmem_id = tb2.crmem_id
							WHERE tb1.crmem_id = '{$crmem_id}'
								AND CASE WHEN tb2.password IS NULL THEN tb1.card_id ELSE tb2.password END = '{$password}'
							LIMIT 1";
			$rs = $mysqli->query($sql);
			if($rs !==false && $rs->num_rows > 0) {
					$row = $rs->fetch_assoc();
					$token = random_char(16);
					$sql = "INSERT INTO cmp_imp_memdetail_login_session(crmem_id, token, create_time, create_ip, last_access_time, last_access_ip)
										VALUES('{$crmem_id}', '{$token}', NOW(), '{$_SERVER["REMOTE_ADDR"]}', NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
					$mysqli->query($sql);
					setcookie("crmem_id"		, $crmem_id			, time() + ( 60 * 60 * 8 ), "/", ".cmcoop.or.th");
					//setcookie("token"				, $token					, time() + ( 60 * 60 * 8 ), "/", ".upbean.co.th");
					$_SESSION["cpk_token"]	= $token;
					$response["data"] 		= $ref ? $ref : "./";
					$response["is_first"] = (int)$row["is_first"];
			}
			echo json_encode($response);
			exit();
	}


	if($do == "confirm") {
			if(isset($_COOKIE["crmem_id"])) {
					$ref		= $mysqli->real_escape_string($_POST["ref"]);
					$email		= $mysqli->real_escape_string($_POST["email"]);
					$password	= $mysqli->real_escape_string($_POST["password"]);
					$mysqli->query("DELETE FROM cmp_imp_memdetail_data WHERE crmem_id = '{$_COOKIE["crmem_id"]}'");
					$mysqli->query("INSERT INTO cmp_imp_memdetail_data(crmem_id, password, email) VALUES('{$_COOKIE["crmem_id"]}','{$password}', '{$email}')");
					$mysqli->query("OPTIMIZE table cmp_imp_memdetail_data");
					if($ref) echo "<script type='text/javascript'>document.location.href = '{$ref}';</script>";
					else echo '<script type="text/javascript">document.location.href = "./";</script>';
					exit();
			}
	}

	if($_POST) exit();

	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	<script src="/scripts/validation/js/formValidation.js"></script>
	<link rel="stylesheet" href="/scripts/validation/css/formValidation.css"/>
	<script src="/scripts/validation/js/bootstrap.js"></script>
	<script>
			$(document).ready(function() {
					$("body").on("keydown", "#crmem_id1", function (e) {
							if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||// Allow: backspace, delete, tab, escape, enter and .
									(e.keyCode == 65 && e.ctrlKey === true) ||// Allow: Ctrl+A
									(e.keyCode == 67 && e.ctrlKey === true) ||// Allow: Ctrl+C
									(e.keyCode == 88 && e.ctrlKey === true) ||// Allow: Ctrl+X
									(e.keyCode >= 35 && e.keyCode <= 39)) {// Allow: home, end, left, right
											 return;// let it happen, don't do anything
							}
							if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) { // Ensure that it is a number and stop the keypress
									e.preventDefault();
							}
					});

					$("body").on("click", "#btn-submit", function() {
							var is_error = false;
							$(".form-group").removeClass("has-error");
							if($("#crmem_id").val() == "") {
									$("#crmem_id").parents(".form-group").addClass("has-error");
									is_error = true;
							}
							if($("#password").val() == "") {
									$("#password").parents(".form-group").addClass("has-error");
									is_error = true;
							}
							if(!is_error) { check_login(); }
					});

					function check_login() {
							$("#login-msg").html("");
							$.ajax({
									type: "POST"
									, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
									, data: $("#frm").serialize()
									, async: true
									, success: function(response) {
											response = $.parseJSON(response);
											//console.log(response.data);
											if(!response.data) {
													$("#login-msg").html("<div class='help-block text-center'><h6 class='text-danger'>กรุณาตรวจสอบเลขที่สมาชิก และรหัสผ่าน</span></h6>");
											} else {
													if(response.is_first == 1) load_content();
													else document.location.href = response.data;
											}
									}
							});
					}

					function load_content() {
							$.ajax({
									type: "POST"
									, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
									, data: {
											"do" 			: "load-content",
											"ref" 		: $("#ref").val(),
											"_time" 	: Math.random()
									}
									, async: true
									, success: function(response) {
											$(".login-content").html(response);
									}
							});
					}

					if($(".login-content").length) load_content();
			});
	</script>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	$smarty->assign("title", "งาน สคช.");
	$smarty->assign("subject", "งาน สคช.");
	$smarty->assign("breadcrumb", array("งาน สคช." => null ));
	ob_start();
	$ref = isset($_GET["ref"]) ? $_GET["ref"] : null;
?>
	<div class="row">
			<div class="col-md-7">
				<?php
				$sql = "SELECT * FROM cmp_page WHERE page_id = '4'" ;
				$rs = $mysqli->query($sql);
				echo $mysqli->error ;
				$page = $rs->fetch_assoc();
				echo htmlspecialchars_decode(@$page["page_detail"], ENT_QUOTES);
				?>
			</div>
			<div class="col-md-5">
				<div class="panel p login-content"></div>
			</div>
	</div>
	
	<input type="hidden" id="ref" value="<?php echo $ref; ?>" />
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");