<?php
	include "config.inc.php";

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($do == "load-content") {
?>
<h2 class="text-center f-bold">ลืมรหัสผ่าน งาน สคช.</h2>
<form id="frm" class="form-horizontal">
		<div class="form-group">
				<label for="crmem_id" class="col-sm-offset-2 col-sm-4 control-label"><h4 class="no-m">เลขที่สมาชิกสมาคม 6 หลัก</h4></label>
				<div class="col-sm-4">
						<input class="form-control" id="crmem_id" name="crmem_id" type="text" maxlength="6" autocomplete="off" />
				</div>
		</div>
		<div class="form-group">
				<label for="crmem_id" class="col-sm-offset-2 col-sm-4 control-label"><h4 class="no-m">เลขที่บัตรประชาชน 13 หลัก</h4></label>
				<div class="col-sm-4">
						<input class="form-control" id="card_id" name="card_id" type="text" maxlength="13" autocomplete="off" />
				</div>
		</div>
		 <div class="form-group">
				<div class="col-sm-12 text-center">
						<button type="button" id="btn-submit" class="btn btn-material-light-blue-300">แสดงรหัสผ่านของฉัน</button>
				</div>
		</div>
</form>
<?php
			$response = ob_get_contents();
			ob_end_clean();
			echo $response;
			exit();
	}

	$crmem_id = isset($_POST["crmem_id"]) ? $mysqli->real_escape_string($_POST["crmem_id"]) : null ;
	if($crmem_id) {

			$card_id = $mysqli->real_escape_string($_POST["card_id"]);
			$response["data"] = null;
			$response["is_first"] = null;
			$sql = "SELECT tb1.crmem_id
								, CASE WHEN tb2.password IS NULL THEN 1 ELSE 0 END AS is_first
								, tb2.password
							FROM cmp_imp_memdetail AS tb1
							LEFT OUTER JOIN cmp_imp_memdetail_data AS tb2 ON tb1.crmem_id = tb2.crmem_id
							WHERE tb1.crmem_id = '{$crmem_id}'
								AND tb1.card_id = '{$card_id}'
							LIMIT 1";
			$rs = $mysqli->query($sql);
			ob_start();
			echo "<h2 class='text-center f-bold'>ลืมรหัสผ่าน</h2>";
			if($rs !==false && $rs->num_rows > 0) {
					$row = $rs->fetch_assoc();
					if((int)$row["is_first"]) echo "<h3 class='m-v-lg text-center'>ยังไม่เคยเข้าใช้งานระบบ สามารถใช้เลขที่บัตรประชาชน 13 หลัก เพื่อเข้าสู่ระบบครั้งแรก</h3>";
					else echo "<h3 class='m-v-lg text-center'>รหัสผ่านของท่าน {$row["password"]}</h3>";
					echo "<div class='text-center'><button class='btn btn-material-light-blue-300' id='btn-next' name='btn-next' type='button'>เข้าสู่ระบบ งาน สคช.</button></div>";
			} else {
					echo "<h3 class='m-v-lg text-center'>ไม่พบข้อมูล กรุณาตรวจสอบเลขที่สมาชิกและเลขที่บัตรประชาชนอีกครั้ง</h3>";
					echo "<div class='text-center'><button class='btn btn-material-light-blue-300' id='btn-reload' name='btn-reload' type='button'>ย้อนกลับ</button></div>";
			}
			$response = ob_get_contents();
			ob_end_clean();
			echo $response;
			exit();
	}

	if($_POST) exit();

	$smarty->assign("title", "ลืมรหัสผ่าน - งาน สคช.");
	
	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	<script>
			$(document).ready(function() {
					$("body").on("keydown", ".number-only", function (e) {
							if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||// Allow: backspace, delete, tab, escape, enter and .
									((e.keyCode == 65 || e.keyCode == 97) && (e.ctrlKey === true || e.keyCode == 91)) ||// Allow: Ctrl+A
									((e.keyCode == 67 || e.keyCode == 99) && e.ctrlKey === true) ||// Allow: Ctrl+C
									((e.keyCode == 86 || e.keyCode == 118) && (e.ctrlKey === true || e.keyCode == 91)) ||// Allow: Ctrl+V
									((e.keyCode == 88 || e.keyCode == 120) && e.ctrlKey === true) ||// Allow: Ctrl+X
									(e.keyCode >= 35 && e.keyCode <= 39)) {// Allow: home, end, left, right
											 return;// let it happen, don't do anything
							}
							if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) { // Ensure that it is a number and stop the keypress
									e.preventDefault();
							}
					});

					$("body").on("click", "#btn-submit", function() {
							var is_error = false;
							$(".form-group").removeClass("has-error");
							if($("#crmem_id").val() == "") {
									$("#crmem_id").parents(".form-group").addClass("has-error");
									is_error = true;
							}
							if($("#card_id").val() == "") {
									$("#card_id").parents(".form-group").addClass("has-error");
									is_error = true;
							}
							if(!is_error) { check_login(); }
					});

					$("body").on("click", "#btn-next", function() { document.location.href='./'; });
					$("body").on("click", "#btn-reload", load_content);

					function check_login() {
							$("#btn-submit").parent("div").find(".help-block").remove();
							$.ajax({
									type: "POST"
									, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
									, data: $("#frm").serialize()
									, async: true
									, success: function(response) {
											$(".panel-content").html(response);
									}
							});
					}

					function load_content() {
							$.ajax({
									type: "POST"
									, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
									, data: {
											"do" 			: "load-content",
											"_time" 	: Math.random()
									}
									, async: true
									, success: function(response) {
											$(".panel-content").html(response);
									}
							});
					}

					if($(".panel-content").length) load_content();
			});
	</script>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	$smarty->assign("subject", "ลืมรหัสผ่าน งาน สคช.");
	$smarty->assign("breadcrumb", array("ลืมรหัสผ่าน งาน สคช." => null ));
	ob_start();
?>
	<div class="panel p">
			<div class="row">
					<div class="col-md-8 col-md-offset-2 panel-content">

					</div>
			</div>
	</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");