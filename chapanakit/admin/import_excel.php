<?php
	session_start();
	require "config.inc.php" ;
 	require "session.inc.php" ;
	require "menu.inc.php" ;
		
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year = date("Y")>2500?date("Y")-543:date("Y");
	
	function U2T($text) { return mb_convert_encoding(trim($text), "TIS-620", "auto"); }
	function T2U($text) { return mb_convert_encoding(trim($text), "UTF-8", "auto"); }
	
	function excel2mysqlDate($date) {
		if(is_numeric($date)) {
			$tmp_date = gmdate("Y-m-d", ($date - 25569) * 86400);
			return ConvertToSQLDate($tmp_date);
		}
		else {
			$dates = explode(" ", $date);
			return ConvertToSQLDate($dates[0]);
		}
	}

	//$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
	
	function process_memdetail($data) {
			global $mysqli;
			$fh = fopen("{$_SERVER["DOCUMENT_ROOT"]}/uploads/memdetail.txt", 'w') or die("can't open file");
			$xxx = "";
			foreach ($data as $row) {
					if(trim($row[0]) != "crmem_id") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 14; $i++) {
									if(in_array($i, array(7, 8, 9))) { $tmp[] = "'".excel2mysqlDate($row[$i])."'"; }
									else { $tmp[] = "'".T2U($row[$i])."'"; }
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_memdetail(crmem_id, fullname_desc, mem_id, memtyp_desc, card_id, memgrp_id, memgrp_desc, birth_dt, mem_dt, protect_dt, currfull_desc, currtel_desc, methpay_desc, invpayist_sts) VALUES({$value})";
							$mysqli->query($sql) or die($mysqli->error);
							$xxx.="{$sql}\n";
					}
			}
			fwrite($fh, "{$xxx}\n");
			fclose($fh);
	}
	
	function process_death($data) {
			global $mysqli;
			$fh = fopen("{$_SERVER["DOCUMENT_ROOT"]}/uploads/death.txt", 'w') or die("can't open file");
			$xxx = "";
			foreach ($data as $row) {
					if(trim($row[0]) != "memgrp_id") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 7; $i++) {
									if(in_array($i, array(4))) { $tmp[] = "'".excel2mysqlDate($row[$i])."'"; }
									else if(in_array($i, array(5))) { $tmp[] = (int)$row[$i]; }
									else { $tmp[] = "'".T2U($row[$i])."'"; }
									//else { $tmp[] = "'".iconv("TIS-620", "UTF-8", $row[$i])."'"; }
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_death(memgrp_id, memgrp_desc, death_prd, crmem_id, death_dt, death_no, fullname_desc) VALUES({$value})";
							$mysqli->query($sql) or die($mysqli->error);
							$xxx.="-{$sql}\n";
					}
			}
			fwrite($fh, "{$xxx}\n");
			fclose($fh);
	}
	
	function process_receipt($data) {
			global $mysqli;
			$fh = fopen("{$_SERVER["DOCUMENT_ROOT"]}/uploads/receipt.txt", 'w') or die("can't open file");
			$xxx = "";
			foreach ($data as $row) {
					if(trim($row[0]) != "memgrpctrl_id") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 11; $i++) {
									if(in_array($i, array(5))) { $tmp[] = "'".excel2mysqlDate($row[$i])."'"; }
									else if(in_array($i, array(8, 10))) { $tmp[] = (int)$row[$i]; }
									else { $tmp[] = "'".T2U($row[$i])."'"; }
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_receipt(memgrpctrl_id, memgrp_id, memgrp_desc, invoices_prd, receiptdoc_id, receipt_dt, crmem_id, invpayist_id, seq_no, inv_desc, item_val) VALUES({$value})";
							$mysqli->query($sql) or die($mysqli->error);
							$xxx.="{$sql}\n";
					}
			}
			fwrite($fh, "{$xxx}\n");
			fclose($fh);
	}
	
	function process_memstm($data) {
			global $mysqli;
			$fh = fopen("{$_SERVER["DOCUMENT_ROOT"]}/uploads/memstm.txt", 'w') or die("can't open file");
			$xxx = "";
			foreach ($data as $row) {
					if(trim($row[0]) != "crmem_id") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 6; $i++) {
									if(in_array($i, array(2))) { $tmp[] = "'".excel2mysqlDate($row[$i])."'"; }
									else if(in_array($i, array(1, 5))) { $tmp[] = (int)$row[$i]; }
									else { $tmp[] = "'".T2U($row[$i])."'"; }
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_memstm(crmem_id, seq_no, opr_dt, refdoc_no, memstm_desc, item_val) VALUES({$value})";
							$mysqli->query($sql) or die($mysqli->error);
							$xxx.="{$sql}\n";
					}
			}
			fwrite($fh, "{$xxx}\n");
			fclose($fh);
	}
	
	function process_thaiftsc($data) {
			global $mysqli;
			$fh = fopen("{$_SERVER["DOCUMENT_ROOT"]}/uploads/thaiftsc.txt", 'w') or die("can't open file");
			$xxx = "";
			foreach ($data as $row) {
					if(trim($row[0]) != "thaiftsc_id") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 9; $i++) {
									if(in_array($i, array(7, 8))) { $tmp[] = "'".excel2mysqlDate($row[$i])."'"; }
									else if(in_array($i, array(2))) { $tmp[] = "'".(is_numeric($row[$i]) ? sprintf("%06d", $row[$i]) : T2U($row[$i]))."'"; }
									else if(in_array($i, array(6))) { $tmp[] = "'".str_replace(" ", "", $row[$i])."'"; }
									else { $tmp[] = "'".T2U($row[$i])."'"; }
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_thaiftsc(thaiftsc_id, thaiftsc_memtpy, mem_id, prename, name, sname, id, thaiftsc_dtmem, thaiftsc_dtprotect) VALUES({$value})";
							$mysqli->query($sql) or die($mysqli->error);
							$xxx.="{$sql}\n";
					}
			}
			fwrite($fh, "{$xxx}\n");
			fclose($fh);
	}
	
	function process_fscct($data) {
			global $mysqli;
			$fh = fopen("{$_SERVER["DOCUMENT_ROOT"]}/uploads/fscct.txt", 'w') or die("can't open file");
			$xxx = "";
			foreach ($data as $row) {
					if(trim($row[0]) != "fscct_id") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 9; $i++) {
									if(in_array($i, array(7, 8))) { $tmp[] = "'".excel2mysqlDate($row[$i])."'"; }
									else if(in_array($i, array(2))) { $tmp[] = "'".(is_numeric($row[$i]) ? sprintf("%06d", $row[$i]) : T2U($row[$i]))."'"; }
									else if(in_array($i, array(6))) { $tmp[] = "'".str_replace(" ", "", $row[$i])."'"; }
									else { $tmp[] = "'".T2U($row[$i])."'"; }
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_fscct(fscct_id, fscct_memtyp, mem_id, prename, name, sname, id, fscct_dtmem, fscct_dtprotect) VALUES({$value})";
							$mysqli->query($sql) or die($mysqli->error);
							$xxx.="{$sql}\n";
					}
			}
			fwrite($fh, "{$xxx}\n");
			fclose($fh);
	}
	
	function process_pay($data) {
			global $mysqli;
			$fh = fopen("{$_SERVER["DOCUMENT_ROOT"]}/uploads/pay.txt", 'w') or die("can't open file");
			$xxx = "";
			foreach ($data as $row) {
					if(trim($row[0]) != "memgrpctrl_id") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 11; $i++) {
									if(in_array($i, array(5))) { $tmp[] = "'".excel2mysqlDate($row[$i])."'"; }
									else if(in_array($i, array(8, 10))) { $tmp[] = (int)$row[$i]; }
									else { $tmp[] = "'".T2U($row[$i])."'"; }
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_pay(memgrpctrl_id, memgrp_id, memgrp_desc, invoices_prd, receiptdoc_id, receipt_dt, crmem_id, invpayist_id, seq_no, inv_desc, item_val) VALUES({$value})";
							$mysqli->query($sql) or die($mysqli->error);
							$xxx.="{$sql}\n";
					}
			}
			fwrite($fh, "{$xxx}\n");
			fclose($fh);
	}
	
	$do = isset($_POST["do"]) ? $_POST["do"] : null ;
	if($do == "process-check") {
			if(file_exists(PATH."/uploads/tmp/import_process.inc.php")) { 
					$data = unserialize(file_get_contents(PATH."/uploads/tmp/import_process.inc.php"));
					$process = (int)$data["process"];
					$total = (int)$data["total"];
					echo "<i class='fa fa-spin fa-refresh'></i>&nbsp;ประมวลผล {$process} / {$total}";
			} else echo "<i class='fa fa-spin fa-refresh'></i>&nbsp;ประมวลผลไฟล์";
			exit();
	}
	
	if($do == "process") {
			$tmp = pathinfo($_POST["fname"]);
			$ext = $tmp['extension'];
			if($ext == "xlsx") {
					set_include_path(get_include_path() . PATH_SEPARATOR . "{$_SERVER["DOCUMENT_ROOT"]}/class/PHPExcel-1.8/Classes/");
					require_once "{$_SERVER["DOCUMENT_ROOT"]}/class/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php";
					
					$file_process = "{$_SERVER["DOCUMENT_ROOT"]}/uploads/tmp/{$_POST["fname"]}";
					$inputFileType = PHPExcel_IOFactory::identify($file_process);
					if($ext == "xlsx")
							$excelReader = PHPExcel_IOFactory::createReader("Excel2007");
					else
							$excelReader = PHPExcel_IOFactory::createReader($inputFileType);

					$excelReader->setReadDataOnly(true);
					$chunkSize = 1024;
					
					$excelObj = $excelReader->load($file_process);
					
					$objWorksheet = $excelObj->setActiveSheetIndex(0);
					$highest_row = $objWorksheet->getHighestRow();
					$highest_col = $objWorksheet->getHighestColumn();
					$_SESSION["total"] = $highest_row;
					
					$row_e = 0;
					//$mysqli->query("TRUNCATE TABLE cmp_imp_{$_POST["ref"]}");
					for ($startRow = 0; $startRow <= $highest_row; $startRow += $chunkSize) {
							$row_e = ($startRow + $chunkSize) > $highest_row ? $highest_row : ($startRow + $chunkSize);
							$data = $objWorksheet->rangeToArray('A'.($startRow + 1).':'.$highest_col.$row_e);
							switch($_POST["ref"]) {
									case "memdetail" 		: process_memdetail($data); 	break;
									case "death" 				: process_death($data); 			break;
									case "receipt" 			: process_receipt($data); 			break;
									case "memstm" 			: process_memstm($data); 		break;
									case "thaiftsc" 			: process_thaiftsc($data); 			break;
									case "fscct" 				: process_fscct($data); 				break;
									case "pay" 					: process_pay($data); 				break;
									default : break;
							}
							ob_flush();
							flush();
							unset($data);
					}
			} else {
					include PATH.'/class/spreadsheet-reader/php-excel-reader/excel_reader2.php';
					include PATH.'/class/spreadsheet-reader/SpreadsheetReader.php';
					$data = new SpreadsheetReader("{$_SERVER["DOCUMENT_ROOT"]}/uploads/tmp/{$_POST["fname"]}");
					switch($_POST["ref"]) {
							case "memdetail" 		: process_memdetail($data); 	break;
							case "death" 				: process_death($data); 			break;
							case "receipt" 			: process_receipt($data); 			break;
							case "memstm" 			: process_memstm($data); 		break;
							case "thaiftsc" 			: process_thaiftsc($data); 			break;
							case "fscct" 				: process_fscct($data); 				break;
							case "pay" 					: process_pay($data); 				break;
							default : break;
					}
					unset($data);
			}
			$id = genid();
			$sql = "INSERT INTO cmp_imp_cpk_stat(stat_id, admin_id, import_file , update_time, update_ip) VALUES('{$id}', {$_SESSION["ADMIN_ID"]}, '{$_POST["ref"]}' , NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
			$mysqli->query($sql);
			exit();
	}
	
	if($do == "unlink") {
			if($_POST["fname"]) @unlink($_SERVER["DOCUMENT_ROOT"]."/uploads/tmp/{$_POST["fname"]}");
			exit();
	}
	
	if($do == "record_count") {
			$sql = "SELECT COUNT(*) AS _c FROM cmp_imp_{$_POST["ref"]}";
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc();
			$c = number_format($row["_c"], 0);
			echo "ข้อมูลปัจจุบัน {$c} Rec.";
			exit();
	}
	
	if($do == "truncate") {
			$mysqli->query("TRUNCATE TABLE cmp_imp_{$_POST["ref"]}");
			exit();
	}
		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script src="/scripts/blueimp.fileupload/9.9.3/js/vendor/jquery.ui.widget.js"></script>
<script src="/scripts/blueimp.fileupload/9.9.3/js/jquery.iframe-transport.js"></script>
<script src="/scripts/blueimp.fileupload/9.9.3/js/jquery.fileupload.js"></script>
<script>
		$(document).ready(function() { 
	/*
	 *	@ File upload
	 *****************************************************/
				var url = "import_excel.upload.php";
				var obj;
				
				$('#fileupload').fileupload({
						url: url,
						dataType: 'json',
						done: function (e, data) {
								//obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;กำลังประมวลผลไฟล์");
								$.each(data.result.files, function (index, file) {
									//console.log(file.name);
									process_file(file.name, obj.attr("ref"));
									
								});
								//obj.parents(".form-group").find(".help-block").html("");
								//$(".btn-import").removeAttr("disabled");
								//obj = null;
						},
						progressall: function (e, data) {
								$(".help-block").html("");
								$(".btn-import").attr("disabled", "disabled");
								var progress = parseInt(data.loaded / data.total * 100, 10);
								obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;อัพโหลด " + progress + "%");
						}
				}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
				

	/*
	*			End File upload
	*****************************************************/
		
				$("body").on("click", ".btn-import", function() {
						obj = $(this);
						$('#fileupload').click();
				});
				
				//var timer_process;
				
				function process_file(filename, ref) {
						obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;กำลังประมวลผลไฟล์");
						console.log("process_file");
						//timer_process = setInterval(function(){ check_process() }, 2000);
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "process"
											, "fname" : filename
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: true
							, success: function(data) {
									console.log(data);
									console.log("process " + filename + " complete")
									var obj_count = obj.parents(".form-group").find(".record-count");
									get_rec_count(obj_count, ref);
									
									//check_process_stop();
									obj.parents(".form-group").find(".help-block").html("");
									$(".btn-import").removeAttr("disabled");
									obj = null;
									unlink(filename);
							}
						});
				}
				
				function check_process() {
						console.log("process-check");
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "process-check"
											, "_time" : Math.random()
										}
							, async: true
							, success: function(data) {
									if(obj) {
										console.log(data);
										obj.parents(".form-group").find(".help-block").html(data);
									}
							}
						});
				}

				function check_process_stop() {
						clearInterval(timer_process);
				}
				
				if($(".record-count").length) {
						$(".record-count").each(function() { 
								get_rec_count($(this), $(this).attr("ref"));
						});
				}
				
				function unlink(filename) {
					$.ajax({
						type: "POST"
						, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
						, data: {
										"do" : "unlink"
										, "fname" : filename
										, "_time" : Math.random()
									}
						, async: false
						, success: function(data) {
								console.log("del " + filename + " complete")
						}
					});
				}
				
				function get_rec_count(obj_count, ref) {
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "record_count"
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: false
							, success: function(data) {
									obj_count.html(data);
							}
						});
				}
				
				$("body").on("click", ".btn-truncate-confirm", function() {
						var ref = $(this).attr("ref");
						var obj_count = obj.parents(".form-group").find(".record-count");
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "truncate"
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: false
							, success: function(data) {
									$("#modal").modal("hide");
									obj_count.html("");
									get_rec_count(obj_count, ref);
									obj = null;
							}
						});
				});
				$("body").on("click", ".btn-truncate", function() {
						obj = $(this);
						$("#modal").find(".modal-title").html("<h3>ยืนยันการล้างข้อมูล</h3>");
						$("#modal").find(".modal-body").html("<div class='text-center'><h3>ต้องการล้างข้อมูล</h3><q>" + $(this).attr("title") + "</q></div>");
						$("#modal").find(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>ปิดหน้าต่าง</button><button type='button' class='btn btn-warning btn-truncate-confirm' ref='" + $(this).attr("ref") + "'><i class='fa fa-trash'></i>&nbsp;ต้องการล้างข้อมูล</button>");
						$("#modal").modal("toggle");
				});
				
		});
</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();
?>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 	
	$smarty->assign("title", "นำเข้าข้อมูล" );
	$smarty->assign("breadcrumb", array("นำเข้าข้อมูล" => "import_excel.php") );
	 
	 ob_start();
?>
		<div class="row">
				<div class="col-md-12">
						<div class="panel panel-white">
								<div class="panel-heading clearfix">
										<h4 class="panel-title">&nbsp;</h4>
								</div>
								<div class="panel-body">
										<?php echo $msg->display() ;  ?>
										<?php
												$import_file = array(
																								array("caption" => "Memdetail"				, "ref" => "memdetail"),
																								array("caption" => "Death"		, "ref" => "death"),
																								array("caption" => "Receipt"	, "ref" => "receipt"),
																								array("caption" => "Memstm"	, "ref" => "memstm"),
																								array("caption" => "Thaiftsc (สสอค.)"	, "ref" => "thaiftsc"),
																								array("caption" => "Fscct (สส.ชสอ.)"	, "ref" => "fscct"),
																								array("caption" => "Pay"	, "ref" => "pay"),
																						);
										?>
										<form class="form-horizontal" action="" method="post" >
												<input id="fileupload" type="file" name="files[]" class="hide" accept=".xls, .xlsx" />
												<?php foreach($import_file as $key => $val) { ?>
												<div class="form-group">
														<label class="col-sm-3 control-label">ไฟล์ <?php echo $val["caption"]; ?></label>
														<div class="col-sm-3">
																<button type="button" class="btn btn-info btn-block btn-import" ref="<?php echo $val["ref"]; ?>">นำเข้าข้อมูล</button>
																<div class="help-block text-center text-info"></div>
														</div>
														<div class="col-sm-3 form-control-static record-count" ref="<?php echo $val["ref"]; ?>"></div>
														<div class="col-sm-3">
																<button type="button" class="btn btn-danger btn-block btn-truncate" ref="<?php echo $val["ref"]; ?>" title="<?php echo $val["caption"]; ?>">ล้างข้อมูล</button>
														</div>
												</div>
												<?php } ?>
										</form>
								</div>
						</div>
				</div>
		</div>
		
		<div class="modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
						<div class="modal-content">
								<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h2 class="modal-title" id="myModalLabel"></h2>
								</div>
								<div class="modal-body"></div>
								<div class="modal-footer"></div>
						</div>
				</div>
		</div>
<?php 
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 