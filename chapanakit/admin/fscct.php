<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 	
	$cmpper->check("message") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
   		$fscct_id = @$mysqli->real_escape_string(@$_POST["fscct_id"]) ;
		$fscct_memtyp = @$mysqli->real_escape_string(@$_POST["fscct_memtyp"]) ;
		$mem_id = @$mysqli->real_escape_string(@$_POST["mem_id"]) ;
		$prename = @$mysqli->real_escape_string(@$_POST["prename"]) ;
		$name = @$mysqli->real_escape_string(@$_POST["name"]) ;
		$sname = @$mysqli->real_escape_string(@$_POST["sname"]) ;
		$id = @$mysqli->real_escape_string(@$_POST["id"]) ;
		$fscct_dtmem = @$mysqli->real_escape_string(@$_POST["fscct_dtmem"]) ;
		$fscct_dtprotect = @$mysqli->real_escape_string(@$_POST["fscct_dtprotect"]) ;
  		
		$sql = "SELECT * FROM`cmp_imp_fscct` WHERE fscct_id = '{$fscct_id}' " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$row = $rs->fetch_assoc();
	 
		if(empty($row))
		{
			$sql = "INSERT INTO `cmp_imp_fscct`(`fscct_id`
												, `fscct_memtyp`
												, `mem_id`
												, `prename`
												, `name`
												, `sname`
												, `id`
												, `fscct_dtmem`
												, `fscct_dtprotect`)
												VALUES (
												'{$fscct_id}'
												,'{$fscct_memtyp}'
												,'{$mem_id}'
												,'{$prename}'
												,'{$name}'
												,'{$sname}'
												,'{$id}'
												,'{$fscct_dtmem}'
												,'{$fscct_dtprotect}' )" ;
			$mysqli->query($sql);
			
			echo $mysqli->error ;
 			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
 			
		}
		else
		{	
 			$sql = "UPDATE `cmp_imp_fscct` 
							SET `fscct_id` = '{$fscct_id}'
							,`fscct_memtyp` = '{$fscct_memtyp}'
							,`mem_id` = '{$mem_id}'
							,`prename` = '{$prename}'
							,`name` = '{$name}'
							,`sname` = '{$sname}'
							,`id` = '{$id}'
							,`fscct_dtmem` = '{$fscct_dtmem}'
							,`fscct_dtprotect` = '{$fscct_dtprotect}'
							WHERE 
								`fscct_id` = '{$fscct_id}' 
							LIMIT 1 
							" ;
			$mysqli->query($sql);
			echo $mysqli->error ;						
			
 			
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array($_GET["do"] , array("delete")) ){
		
		$fscct_id = @$mysqli->real_escape_string(@$_GET["fid"]) ;
		
 		$sql = "DELETE FROM `cmp_imp_fscct`  WHERE fscct_id = '{$fscct_id}' LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;	
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
 		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	

		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
	<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
	<script>
		$(document).ready(function() {
			$('.date-picker').datepicker({
				orientation: "top auto" ,
				autoclose: true , 
				format : "yyyy-mm-dd"
			});
			 
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$fscct_id = (int) $_GET["fid"] ; 
 			
			$sql = "SELECT * FROM `cmp_imp_fscct` WHERE fscct_id = '{$fscct_id}' LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "ข้อมูลตาราง fscct" );
		$smarty->assign("page_title", "ข้อมูลตาราง fscct" );
 		$smarty->assign("breadcrumb", array("ข้อมูลตาราง fscct" => "?" ,  "{$btitle}" => "#"  ) );
 
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ข้อมูลตาราง fscct</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
   						
						<div class="form-group">
							<label for="fscct_id" class="col-sm-3 control-label">fscct_id</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="fscct_id" name="fscct_id" placeholder="" value="<?php echo htmlspecialchars(@$row["fscct_id"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="fscct_memtyp" class="col-sm-3 control-label">fscct_memtyp</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="fscct_memtyp" name="fscct_memtyp" placeholder="" value="<?php echo htmlspecialchars(@$row["fscct_memtyp"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="mem_id" class="col-sm-3 control-label">mem_id</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="mem_id" name="mem_id" placeholder="" value="<?php echo htmlspecialchars(@$row["mem_id"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="prename" class="col-sm-3 control-label">prename</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="prename" name="prename" placeholder="" value="<?php echo htmlspecialchars(@$row["prename"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-sm-3 control-label">name</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="name" name="name" placeholder="" value="<?php echo htmlspecialchars(@$row["name"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="sname" class="col-sm-3 control-label">sname</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="sname" name="sname" placeholder="" value="<?php echo htmlspecialchars(@$row["sname"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="id" class="col-sm-3 control-label">id</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="id" name="id" placeholder="" value="<?php echo htmlspecialchars(@$row["id"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="fscct_dtmem" class="col-sm-3 control-label">fscct_dtmem</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="fscct_dtmem" name="fscct_dtmem" placeholder="" value="<?php echo htmlspecialchars(@$row["fscct_dtmem"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="fscct_dtprotect" class="col-sm-3 control-label">fscct_dtprotect</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="fscct_dtprotect" name="fscct_dtprotect" placeholder="" value="<?php echo htmlspecialchars(@$row["fscct_dtprotect"]) ?>" >
 							</div>
						</div>
 						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ;
		$smarty->assign("title", "ข้อมูลตาราง fscct" );
		$smarty->assign("page_title", "ข้อมูลตาราง fscct" );
		$smarty->assign("breadcrumb", array("ข้อมูลตาราง fscct" => "#") );
		
		ob_start();
		$where = "" ;
		if(!empty($_GET["search"])){
			$search = $mysqli->real_escape_string($_GET["search"]);
			$where = "WHERE `fscct_id` like '%{$search}%'
							OR `fscct_memtyp` like '%{$search}%'
							OR `mem_id` like '%{$search}%'
							OR `prename` like '%{$search}%'
							OR `name` like '%{$search}%'
							OR `sname` like '%{$search}%'
							OR `id` like '%{$search}%'
							OR `fscct_dtmem` like '%{$search}%'
							OR `fscct_dtprotect` like '%{$search}%'
						  " ;
		}
		
		$sql = "SELECT COUNT(fscct_id) as _c FROM `cmp_imp_fscct` {$where}" ;
		$rs = $mysqli->query($sql);
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM `cmp_imp_fscct` {$where} ORDER BY fscct_id DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
 		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows  - ($per_page * ($page - 1 ));
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">ข้อมูลตาราง fscct</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			<form class="form-inline" style="margin-bottom:10px;" action="?" method="get">
				<div class="form-group">
					<input type="form-control" name="search" value="<?php echo  htmlspecialchars($_GET["search"]) ?>" style="height: 32px;width: 320px;" /> 
				</div>
				<button type="submit" class="btn btn-primary">ค้นหา</button>
			</form>
			<table class="table table-bordered">
				<thead>
					<tr>
 						<th style="text-align:center" >fscct_id</th>
						<th style="text-align:center" >fscct_memtyp</th>
                        <th style="text-align:center" >mem_id</th>
						<th style="text-align:center" >prename</th>
						<th style="text-align:center" >name</th>
						<th style="text-align:center" >sname</th>
						<th style="text-align:center" >id</th>
						<th style="text-align:center" >fscct_dtmem</th>
						<th style="text-align:center" >fscct_dtprotect</th>
 						<th style="text-align:center" ></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
						
 						<td><?php echo @$row["fscct_id"] ?></td>
						<td><?php echo @$row["fscct_memtyp"] ?></td>
						<td><?php echo @$row["mem_id"] ?></td>
						<td><?php echo @$row["prename"] ?></td>
						<td><?php echo @$row["name"] ?></td>
						<td><?php echo @$row["sname"] ?></td>
						<td><?php echo @$row["id"] ?></td>
						<td><?php echo @$row["fscct_dtmem"] ?></td>
						<td><?php echo @$row["fscct_dtprotect"] ?></td>
  
						<td style="text-align:center" >
							<a href="?in=pay&do=edit&fid=<?php echo @$row["fscct_id"] ?>" title="แก้ไขข้อมูล" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=pay&do=delete&fid=<?php echo @$row["fscct_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " title="ลบข้อมูล"  ><i class="fa fa-trash-o"></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 