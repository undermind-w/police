<?php
	//error_reporting(0);
	//ini_set('display_errors', 0);

	define("PATH",$_SERVER["DOCUMENT_ROOT"]) ;
	 
	function curPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}
 	
	define("TITLE"," ") ;
	define("KEYWORD","") ;
	define("DESC","") ;
	
	define("THEME","admin_cpk");
	
	
	require PATH . "/class/connect.inc.php" ;
	require PATH . "/class/function.inc.php" ;
	require PATH . "/class/smarty.inc.php";
 	require PATH . "/class/messages.class.php"  ;
	require PATH . "/class/pagination.class.php"  ;
	require PATH . "/class/cpk_logging.class.php"  ;
	require PATH . "/class/cpk_permission.class.php"  ;
	
	$admin_id = (int) !empty($_SESSION["ADMIN_ID"]) ? $_SESSION["ADMIN_ID"] : 0   ; 
	
	$msg = new Messages();
	$cmplog = new CmpActionLogging();
	$cmpper = new CmpPermission();
 
	function get_breadcrumb($data = array()){
		$nav = "" ; 
		$nav .= "<div class=\"row\">" ; 
        $nav .= "<div class=\"col-md-12\">" ; 
		$nav .= "<!--breadcrumbs start -->" ; 
		$nav .= "<ul class=\"breadcrumb\">" ; 
		
		$nav .= "<li><a href=\"#\"><i class=\"fa fa-home\"></i> หน้าแรก </a></li>" ; 
		foreach($data as $key => $value){
			$nav .= "<li><a href=\"{$value}\">{$key}</a></li> " ;  
		}
		$nav .= "</ul>" ;  
		$nav .= "</div>" ; 
		$nav .= "</div>" ; 
                     
        return $nav ;
	}
	
	$base_url = "/chapanakit/admin/" ; 
	$smarty->assign("base_url" , $base_url) ; 
	
	