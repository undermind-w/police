<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 	 
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	 
	
	  
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
	 
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		 
	}
	else
	{
		/*
		if(empty($_GET["year"]) || empty($_GET["month"])  )
		{
			$year = date("Y") ; 
			$month = date("m") ; 
			
			echo "<script> window.location.href = \"?year={$year}&month={$month}\"</script>" ;
			exit();
		}
		$year = (int) @$_GET["year"] ; 
		$month =(int) @$_GET["month"] ; 
 		$page = (int) @$_GET["page"]  ; 
		*/
		
		$smarty->assign("title", "อัพเดทฐานข้อมูลเมื่อ" );
		$smarty->assign("page_title", "อัพเดทฐานข้อมูลเมื่อ" );
		$smarty->assign("breadcrumb", array("อัพเดทฐานข้อมูลเมื่อ" => "#") );
		
		ob_start();
		
 		
		
		$sql = "SELECT COUNT(cmp_imp_cpk_stat.update_time) as _c FROM cmp_imp_cpk_stat " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 25 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT cmp_admin.admin_name , cmp_imp_cpk_stat.update_time 
									FROM cmp_imp_cpk_stat 
										LEFT JOIN cmp_admin ON  cmp_imp_cpk_stat.admin_id = cmp_admin.admin_id
									WHERE cmp_imp_cpk_stat.update_time <> '0000-00-00 00:00:00'
									ORDER BY  cmp_imp_cpk_stat.update_time DESC 
									LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		  
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">การเข้าใช้งานระบบ</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
 			 <div style="clear: both;margin-bottom:15px"></div>			 
			<table class="table table-bordered">
				<thead>
					<tr>
 						<th width="225" style="text-align:center" > วันที่ </th>
						<th width="150" style="text-align:center" > ผู้ใช้ระบบ </th>
 						
					</tr>
				</thead>
				<tbody>
					<?php 
					$i = 1 ; 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
 						<td><?php echo @mysqldate2thaidate(@$row["update_time"] , "long" , true )  ?></td>
						<td><?php echo @$row["admin_name"] ?></td>
                       </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 