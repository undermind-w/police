<?php
	require_once "{$_SERVER["DOCUMENT_ROOT"]}/class/PHPExcel-1.8/Classes/PHPExcel.php";
	require_once "{$_SERVER["DOCUMENT_ROOT"]}/class/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php";
	require_once "{$_SERVER["DOCUMENT_ROOT"]}/class/PHPExcel-1.8/Classes/PHPExcel/Reader/IReader.php";
	require_once "{$_SERVER["DOCUMENT_ROOT"]}/class/PHPExcel-1.8/Classes/PHPExcel/Reader/IReadFilter.php";
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
class chunkReadFilter implements PHPExcel_Reader_IReadFilter
{
private $_startRow = 0;
private $_endRow = 0;

public function setRows($startRow, $chunkSize) {
    $this->_startRow    = $startRow;
    $this->_endRow      = $startRow + $chunkSize;
}

public function readCell($column, $row, $worksheetName = '') {
    if (($row == 1) || ($row >= $this->_startRow &&$row < $this->_endRow)) {
        return true;
    }
    return false;
}
}

session_start();

if ($_SESSION['startRow']) $startRow = $_SESSION['startRow'];
else $startRow = 2;

		$inputFileType = 'Excel2007';
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$chunkSize = 100;
		$chunkFilter = new chunkReadFilter();
		$objReader->setReadFilter($chunkFilter);

    while ($startRow <= 1000) {
				$chunkFilter->setRows($startRow,$chunkSize);
				$objReader->setReadDataOnly(true);
				$objPHPExcel = $objReader->load("{$_SERVER["DOCUMENT_ROOT"]}/uploads/tmp/SS1.xlsx");
				$startRow += $chunkSize;
				$_SESSION['startRow'] = $startRow; 

				unset($objPHPExcel);
    }
				unset($objReader); 

    echo "The End";
    unset($_SESSION['startRow']);
?>