<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 	
	$cmpper->check("message") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
   		$memgrpctrl_id = @$mysqli->real_escape_string(@$_POST["memgrpctrl_id"]) ;
		$memgrp_id = @$mysqli->real_escape_string(@$_POST["memgrp_id"]) ;
		$memgrp_desc = @$mysqli->real_escape_string(@$_POST["memgrp_desc"]) ;
		$invoices_prd = @$mysqli->real_escape_string(@$_POST["invoices_prd"]) ;
		$receiptdoc_id = @$mysqli->real_escape_string(@$_POST["receiptdoc_id"]) ;
		$receipt_dt = @$mysqli->real_escape_string(@$_POST["receipt_dt"]) ;
		$crmem_id = @$mysqli->real_escape_string(@$_POST["crmem_id"]) ;
		$seq_no = @$mysqli->real_escape_string(@$_POST["seq_no"]) ;
		$inv_desc = @$mysqli->real_escape_string(@$_POST["inv_desc"]) ;
 		
		$sql = "SELECT * FROM `cmp_imp_pay` WHERE  seq_no = '{$seq_no}' AND receiptdoc_id = '{$receiptdoc_id}' " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$row = $rs->fetch_assoc();
	 
		if(empty($row))
		{
			$sql = "INSERT INTO `cmp_imp_pay` (`memgrpctrl_id`
											, `memgrp_id`
											, `memgrp_desc`
											, `invoices_prd`
											, `receiptdoc_id`
											, `receipt_dt`
											, `crmem_id`
											, `seq_no`
											, `inv_desc`
											, `item_val`) 
											VALUES (
											'{$memgrpctrl_id}'
											,'{$memgrp_id}'
											,'{$memgrp_desc}'
											,'{$invoices_prd}'
											,'{$receiptdoc_id}'
											,'{$receipt_dt}'
											,'{$crmem_id}'
											,'{$seq_no}'
											,'{$inv_desc}'
 											,'{$item_val}')" ;
			$mysqli->query($sql);
			echo $mysqli->error ;
 			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
 			
		}
		else
		{	
 			$sql = "UPDATE `cmp_imp_pay` 
							SET `memgrpctrl_id`='{$memgrpctrl_id}'
							,`memgrp_id`='{$memgrp_id}'
							,`memgrp_desc`='{$memgrp_desc}'
							,`invoices_prd`='{$invoices_prd}'
							,`receiptdoc_id`='{$receiptdoc_id}'
							,`receipt_dt`='{$receipt_dt}'
							,`crmem_id`='{$crmem_id}'
							,`seq_no`='{$seq_no}'
							,`inv_desc`='{$inv_desc}'
							,`item_val`='{$item_val}' 
						WHERE receiptdoc_id = '{$receiptdoc_id}' 
							AND seq_no ='{$seq_no}'  
						LIMIT 1 
						" ;
			$mysqli->query($sql);
			echo $mysqli->error ;						
			
 			
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array($_GET["do"] , array("delete")) ){
		
		$receiptdoc_id = @$_GET["rid"] ;  
		$seq_no = @$_GET["no"] ;  
		
 		$sql = "DELETE FROM cmp_imp_pay WHERE receiptdoc_id = '{$receiptdoc_id}' AND seq_no = '{$seq_no}' LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;	
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
 		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	

		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
	<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
	<script>
		$(document).ready(function() {
			$('.date-picker').datepicker({
				orientation: "top auto" ,
				autoclose: true , 
				format : "yyyy-mm-dd"
			});
			 
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$receiptdoc_id = (int) $_GET["rid"] ; 
			$seq_no = (int) $_GET["no"] ; 
			
			$sql = "SELECT * FROM cmp_imp_pay WHERE receiptdoc_id = '{$receiptdoc_id}' AND seq_no = '{$seq_no}'  LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "ข้อมูลตาราง pay" );
		$smarty->assign("page_title", "ข้อมูลตาราง pay" );
 		$smarty->assign("breadcrumb", array("ข้อมูลตาราง pay" => "?" ,  "{$btitle}" => "#"  ) );

		/*
		$msg_id = (int) @$row["msg_id"] ; 
 		$msg_title =  @$row["msg_title"];  
		$msg_detail =  htmlspecialchars(@$row["msg_detail"]); 
		$msg_url = @$row["msg_url"];  
		
		$msg_senddate = @$row["msg_senddate"];  
 		$msg_senddate = empty($msg_senddate) ? date("Y-m-d H:i:s") : $msg_senddate ; 
		
		$msg_senddate_time = strtotime($msg_senddate);
		
		$msg_senddate_time_date =  date("Y-m-d" , $msg_senddate_time ) ; 
		$msg_senddate_time_hour = (int) date("H" , $msg_senddate_time ) ; 
		$msg_senddate_time_min = (int) date("i" , $msg_senddate_time ) ; 
		
 		$msg_status = (int) @$row["msg_status"]  ;  
		*/
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ข้อมูลตาราง pay</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
   						
						<div class="form-group">
							<label for="memgrpctrl_id" class="col-sm-3 control-label">memgrpctrl_id</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="memgrpctrl_id" name="memgrpctrl_id" placeholder="" value="<?php echo htmlspecialchars(@$row["memgrpctrl_id"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="memgrp_id" class="col-sm-3 control-label">memgrp_id</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="memgrp_id" name="memgrp_id" placeholder="" value="<?php echo htmlspecialchars(@$row["memgrp_id"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="memgrp_desc" class="col-sm-3 control-label">memgrp_desc</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="memgrp_desc" name="memgrp_desc" placeholder="" value="<?php echo htmlspecialchars(@$row["memgrp_desc"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="invoices_prd" class="col-sm-3 control-label">invoices_prd</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="invoices_prd" name="invoices_prd" placeholder="" value="<?php echo htmlspecialchars(@$row["invoices_prd"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="receiptdoc_id" class="col-sm-3 control-label">receiptdoc_id</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="receiptdoc_id" name="receiptdoc_id" placeholder="" value="<?php echo htmlspecialchars(@$row["receiptdoc_id"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="receipt_dt" class="col-sm-3 control-label">receipt_dt</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="receipt_dt" name="receipt_dt" placeholder="" value="<?php echo htmlspecialchars(@$row["receipt_dt"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="crmem_id" class="col-sm-3 control-label">crmem_id</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="crmem_id" name="crmem_id" placeholder="" value="<?php echo htmlspecialchars(@$row["crmem_id"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="seq_no" class="col-sm-3 control-label">seq_no</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="seq_no" name="seq_no" placeholder="" value="<?php echo htmlspecialchars(@$row["seq_no"]) ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="inv_desc" class="col-sm-3 control-label">inv_desc</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="inv_desc" name="inv_desc" placeholder="" value="<?php echo htmlspecialchars(@$row["inv_desc"]) ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="item_val" class="col-sm-3 control-label">item_val</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="item_val" name="item_val" placeholder="" value="<?php echo htmlspecialchars(@$row["item_val"]) ?>" >
 							</div>
						</div>
						 
						 
						 
						 <!-- 
						<div class="form-group">
							<label for="news_detail" class="col-sm-3 control-label">กำหนดส่ง</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control date-picker" id="msg_senddate" name="msg_senddate" placeholder="" value="<?php echo $msg_senddate_time_date  ?>" style="width:150px;float:left;"  >
								 
							</div>
						</div>
						 -->
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ;
		$smarty->assign("title", "ข้อมูลตาราง pay" );
		$smarty->assign("page_title", "ข้อมูลตาราง pay" );
		$smarty->assign("breadcrumb", array("ข้อมูลตาราง pay" => "#") );
		
		ob_start();
		$where = "" ;
		if(!empty($_GET["search"])){
			$search = $mysqli->real_escape_string($_GET["search"]);
			$where = "WHERE `memgrpctrl_id` like '%{$search}%'
							OR `memgrp_id` like '%{$search}%'
							OR `memgrp_desc` like '%{$search}%'
							OR `invoices_prd` like '%{$search}%'
							OR `receiptdoc_id` like '%{$search}%'
							OR `receipt_dt` like '%{$search}%'
							OR `crmem_id` like '%{$search}%'
							OR `seq_no` like '%{$search}%'
							OR `inv_desc` like '%{$search}%'
							OR `item_val` like '%{$search}%'   " ;
		}
		
		$sql = "SELECT COUNT(crmem_id) as _c FROM cmp_imp_pay {$where}" ;
		$rs = $mysqli->query($sql);
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM cmp_imp_pay {$where} ORDER BY memgrpctrl_id DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
 		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows  - ($per_page * ($page - 1 ));
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">ข้อมูลตาราง pay</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			<form class="form-inline" style="margin-bottom:10px;" action="?" method="get">
				<div class="form-group">
					<input type="form-control" name="search" value="<?php echo  htmlspecialchars($_GET["search"]) ?>" style="height: 32px;width: 320px;" /> 
				</div>
				<button type="submit" class="btn btn-primary">ค้นหา</button>
			</form>
			<table class="table table-bordered">
				<thead>
					<tr>
 						<th style="text-align:center" >memgrpctrl_id</th>
						<th style="text-align:center" >memgrp_id</th>
                        <th style="text-align:center" >memgrp_desc</th>
						<th style="text-align:center" >invoices_prd</th>
						<th style="text-align:center" >receiptdoc_id</th>
						<th style="text-align:center" >receipt_dt</th>
						<th style="text-align:center" >crmem_id</th>
						<th style="text-align:center" >seq_no</th>
						<th style="text-align:center" >inv_desc</th>
						<th style="text-align:center" >item_val</th>
						<th style="text-align:center" ></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
						
 						<td><?php echo @$row["memgrpctrl_id"] ?></td>
						<td><?php echo @$row["memgrp_id"] ?></td>
						<td><?php echo @$row["memgrp_desc"] ?></td>
						<td><?php echo @$row["invoices_prd"] ?></td>
						<td><?php echo @$row["receiptdoc_id"] ?></td>
						<td><?php echo @$row["receipt_dt"] ?></td>
						<td><?php echo @$row["crmem_id"] ?></td>
						<td><?php echo @$row["seq_no"] ?></td>
						<td><?php echo @$row["inv_desc"] ?></td>
						<td><?php echo @$row["item_val"] ?></td>
 
						<td style="text-align:center" >
							<a href="?in=pay&do=edit&rid=<?php echo @$row["receiptdoc_id"] ?>&no=<?php echo @$row["seq_no"] ?>" title="แก้ไขข้อมูล" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=pay&do=delete&rid=<?php echo @$row["receiptdoc_id"] ?>&no=<?php echo @$row["seq_no"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " title="ลบข้อมูล"  ><i class="fa fa-trash-o"></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 