<?php
	session_start();
	require "config.inc.php" ;
	require "menu.inc.php" ;
 	require "session.inc.php" ;
	

 
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");
		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 
	ob_start();
	
	$sql = "SELECT DATE(create_time) as _date 
					, DAY(create_time) as _day 
					, MONTH(create_time) as _month 
					, COUNT(token) as _c 
				FROM  cmp_imp_memdetail_login_session 
				WHERE create_time > DATE_ADD( NOW(),INTERVAL -30 DAY) GROUP BY DATE(create_time) " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;
	$i = 0 ; 
	$label = array(); 
	$data = array() ; 
	while($item = $rs->fetch_assoc()){
		$i ++ ; 
		
		$label[] = "[ {$i} , '{$item["_day"]}/{$item["_month"]}' ]" ; 
		$data[] = "[ {$i} , {$item["_c"]} ]" ; 
	}
?>
	<script>
		var flot1 = function () {
			
			var data = [<?php  echo implode( ",", $data) ?>];
 			
			var dataset =  [
				{
					data: data ,
					color: "rgba(220,220,220,1)",
					lines: {
						show: true,
						fill: 0.2,
					},
					shadowSize: 0,
				}, {
					data: data,
					color: "#fff",
					lines: {
						show: false,
					},
					points: {
						show: true,
						fill: true,
						radius: 4,
						fillColor: "rgba(220,220,220,1)",
						lineWidth: 2
					},
					curvedLines: {
						apply: false,
					},
					shadowSize: 0
				} 
			];
			
			var ticks = [<?php echo implode("," ,  $label ) ?>];

			var plot1 = $.plot( "#flotchart1" , dataset, {
				series: {
					color: "#14D1BD",
					lines: {
						show: true,
						fill: 0.2
					},
					shadowSize: 0,
					curvedLines: {
						apply: true,
						active: true
					}
				},
				xaxis: {
					ticks: ticks,
				},
				legend: {
					show: false
				},
				grid: {
					color: "#AFAFAF",
					hoverable: true,
					borderWidth: 0,
					backgroundColor: '#FFF'
				}
			});
		} ; 
		
		var set_hegiht_panel_1 = function(){
			var h1 = $(".panel-stats-member-login").height();
			var h2 = $(".panel-stats-member-addr").height();
			var h3 = (h2 +  h1) - 46 ; 
			$(".panel-stats-admin .panel-body").height(h3) ; 
		} ; 
		
		$(function(){
			flot1() ; 
			set_hegiht_panel_1();
			
			$('#model_profile_status').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) ; 
				var modal = $(this) ;
				$.ajax({
					method: "POST",
					url: "?",
					data: { 'do' : 'stats1' , id :  $(button).data("id")  }
				}).done(function( data ) {
					var json = JSON.parse(data) 
					modal.find("#txt_type").val(json.type) ; 
 					modal.find("#txt_new").val(json.detail)  ;

				 });
 			});
		}) ; 
		
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 
 
	
 	
	$smarty->assign("title", "หน้าแรก" );
	$smarty->assign("breadcrumb", array("Dashboard" => "index.php") );
	 
	 ob_start();
?>
	<div class="row">
		<div class="col-md-9">
			<div class="panel panel-white">
				<div class="row">
					<div class="col-sm-12">
						<div class="visitors-chart">
							<div class="panel-heading">
								<h4 class="panel-title">สถิติผู้ใช้งานระบบ</h4>
							</div>
							<div class="panel-body">
								<div id="flotchart1"></div>
							</div>
						</div>
					</div>
					 
				</div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">อัพเดทฐานข้อมูลเมื่อ</h3>
				</div>
				<div class="panel-body" style="    padding-bottom: 5px;">
					<div class="table-responsive" >
					<table class="table" style="    margin-bottom: 0;">
						<tbody>
						<?php 
							$sql = "SELECT cmp_admin.admin_name  
											, cmp_imp_cpk_stat.update_time 
											FROM cmp_imp_cpk_stat 
												LEFT JOIN cmp_admin ON  cmp_imp_cpk_stat.admin_id = cmp_admin.admin_id
											WHERE cmp_imp_cpk_stat.update_time <> '0000-00-00 00:00:00'
  											ORDER BY  cmp_imp_cpk_stat.update_time DESC 
											LIMIT  0 , 1 " ; 
							$rs = $mysqli->query($sql);
							echo $mysqli->error ; 
							while($item = $rs->fetch_assoc()){
						?>
						
							<tr>
								<td><?php echo @mysqldate2thaidate( $item["update_time"]  , "long" ,  true  ) ?> <br /> <strong> โดย <?php echo $item["admin_name"] ?>  </strong>  </td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
					<a class="pull-right" href="stats.import.php" >ดูทั้งหมด</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php 
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 