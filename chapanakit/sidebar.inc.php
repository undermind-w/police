<?php
		$sql = "SELECT tb1.fullname_desc
						FROM cmp_imp_memdetail AS tb1
						WHERE crmem_id = '{$_COOKIE["crmem_id"]}'";
		$rs = $mysqli->query($sql);
		$row = $rs->fetch_assoc();
		$fullname_desc = str_replace(array('นาย ', 'นางสาว ', 'นาง '), array('', '', ''), $row["fullname_desc"]);
		unset($sql, $rs, $row);

		$sql = "SELECT create_time
						FROM cmp_imp_memdetail_login_session
						WHERE crmem_id = '{$_COOKIE["crmem_id"]}'
							AND token <> '{$_SESSION["cpk_token"]}'
						ORDER BY create_time DESC";
		$rs = $mysqli->query($sql);
		$row = $rs->fetch_assoc();
		$last_access = $row["create_time"];
?>
<div class="panel panel-material-light-blue-300 hidden-xs hidden-sm">
		<div class="panel-heading">
				<div class="">
						<div class="pull-left">
								<span class="fa-stack fa-2x">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-bookmark fa-stack-1x text-material-light-blue-300"></i>
								</span>
						</div>
						<div class="pull-left">
								<h5>ชื่อสกุล</h5>
								<h5>คุณ <?php echo $fullname_desc; ?></h5>
						</div>
						<div class="clearfix"></div>
				</div>
		</div>
		<div class="panel-body">
				<h5 class="no-m m-v-xxs">วันที่</h5>
				<h5 class="no-m m-v-xxs p-l-md"><?php echo mysqldate2thaidate(date("Y-m-d H:i:00"), "", true); ?></h5>
				<h5 class="no-m m-v-xxs">ครั้งสุดท้ายที่เข้ามา</h5>
				<h5 class="no-m m-v-xxs p-l-md"><?php echo empty($last_access) ? "เข้าใช่้งานครั้งแรก" : mysqldate2thaidate($row["create_time"], "", true) ; ?></h5>
		</div>
		<div class="panel-heading">
				<div class="">
						<div class="pull-left">
								<span class="fa-stack fa-2x">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-envelope fa-stack-1x text-material-light-blue-300"></i>
								</span>
						</div>
						<div class="pull-left">
								<h3>ข้อความถึงท่าน</h3>
						</div>
						<div class="clearfix"></div>
				</div>
		</div>
		<div class="panel-body">
				<?php
						$sql = "SELECT tb1.msg_id, tb1.msg_title, tb1.msg_senddate
										, CASE WHEN tb2.is_read IS NULL THEN 0 ELSE tb2.is_read END AS is_read
										FROM cmp_cpk_message AS tb1
										LEFT OUTER JOIN (
											SELECT msg_id, is_read
												FROM cmp_imp_memdetail_message
												WHERE crmem_id = '{$_COOKIE["crmem_id"]}'
										) AS tb2 ON tb1.msg_id = tb2.msg_id
										WHERE tb1.msg_status = 1
										AND tb1.msg_senddate < NOW()
										ORDER BY tb1.msg_senddate DESC
										LIMIT 3";
						$rs = $mysqli->query($sql);
						if(!$rs->num_rows) echo '<h5 class="text-center">ไม่พบข้อความถึงท่าน</h5>';
						else {
								while(($row = $rs->fetch_assoc())) {
										$icon = $row["is_read"] ? "fa-envelope-o" : "fa-envelope" ;
										$time_ago = get_timeago(strtotime($row["msg_senddate"]));
										echo "<div class='row'>
															<div class='col-xs-2'><i class='fa {$icon}' style='font-size: 22px;color: grey;'></i></div>
															<div class='col-xs-10'>
																	<div><a href='message.php?mid={$row["msg_id"]}'>{$row["msg_title"]}</a></div>
																	<div class='text-grey-400 f-s-11'>เมื่อ {$time_ago}</div>
															</div>
													</div>";
								}
						}
				?>
				<div class="text-right"><a href="message.php">ดูทั้งหมด</a></div>
		</div>
</div>
<?php
		unset($sql, $rs, $row); 
?>