<?php
	session_start(); 
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	define("PATH",$_SERVER["DOCUMENT_ROOT"]) ;
	 
	function curPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}
 	
	define("TITLE"," ") ;
	define("KEYWORD","") ;
	define("DESC","") ;
	
	define("THEME","main");
	
	
	require PATH . "/class/connect.inc.php" ;
	require PATH . "/class/function.inc.php" ;
	require PATH . "/class/smarty.inc.php";
	
	$base_url = "/" ; 
	$smarty->assign("base_url" , $base_url) ; 
	
	
	$sql = "SELECT * FROM cmp_link WHERE link_status = 1 ORDER BY createdate DESC " ;
	$rs = $mysqli->query($sql);
	$mysqli->error ;
	$elinks = array();
	while($item = $rs->fetch_assoc())
	{
		$elinks[] = $item; 
	}
	
	$smarty->assign("elinks" , $elinks) ; 
	