<?php
define(PATH, $_SERVER["DOCUMENT_ROOT"]);
define(BASEURL, 'http://'.$_SERVER["SERVER_NAME"].'/');
require(PATH."/class/connect.inc.php");
require(PATH."/class/function.inc.php");

header("Content-type: application/json; charset=UTF-8");

class Service {
	private $mysqli;
	
	public function __construct($mysqli) {
		$this->mysqli = $mysqli;
	}
	
	private function cut_text($text) {
		$text = strip_tags($text);
		$text = trim($text);
		$text = str_replace("&nbsp;", "", $text);
		$text = str_replace("\n", "", $text);
		$text = str_replace("\s", "", $text);
		$text = str_replace("\t", "", $text);
		$text = str_replace("\r", "", $text);
		return $text;
	}
	
	public function loginApp() {
		$sql = "SELECT tb1.crmem_id, tb1.fullname_desc, tb1.mem_id
					FROM cmp_imp_memdetail AS tb1
						LEFT OUTER JOIN cmp_imp_memdetail_data AS tb2 ON tb1.crmem_id = tb2.crmem_id
					WHERE tb1.crmem_id = '".$_GET["crmem_id"]."' AND CASE WHEN tb2.password IS NULL THEN tb1.card_id ELSE tb2.password END = '".$_GET["password"]."'";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		if($row = $rs->fetch_assoc()) {
			echo json_encode(array(
				"status" => "TRUE",
				"member" => $row
			));
		}
		else {
			echo json_encode(array(
				"status" => "FALSE"
			));
		}
	}
	
	public function getMember() {
		$sql = "SELECT *
					FROM cmp_imp_memdetail
					WHERE crmem_id = '".$_GET["crmem_id"]."'";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		if($row = $rs->fetch_assoc()) {
			$interval = date_diff(date_create($row["birth_dt"]), date_create(date("Y-m-d")));
			$row["age"] = $interval->format("%y ปี");
			$interval = date_diff(date_create($row["mem_dt"]), date_create(date("Y-m-d")));
			$row["mem_age"] = $interval->format("%y ปี %m เดือน");
			$row["birth_dt"] = mysqldate2thaidate($row["birth_dt"]);
			$row["mem_dt"] = mysqldate2thaidate($row["mem_dt"]);
			$row["protect_dt"] = mysqldate2thaidate($row["protect_dt"]);
			
			$sql = "SELECT * FROM cmp_imp_memdetail WHERE mem_id = '{$row["mem_id"]}' AND memtyp_desc <> 'สมาชิกปกติ'";
			$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
			$members2 = array();
			while($row2 = $rs->fetch_assoc()) {
				array_push($members2, $row2);
			}
			
			$sql = "SELECT * FROM cmp_imp_thaiftsc WHERE id = '{$row["card_id"]}'";
			$rs_thaiftsc = $this->mysqli->query($sql);
			$row_thaiftsc = $rs_thaiftsc->fetch_assoc();
			$row_thaiftsc["thaiftsc_dtmem"] = mysqldate2thaidate($row_thaiftsc["thaiftsc_dtmem"]);
			$row_thaiftsc["thaiftsc_dtprotect"] = mysqldate2thaidate($row_thaiftsc["thaiftsc_dtprotect"]);
			
			$sql = "SELECT * FROM cmp_imp_fscct WHERE id = '{$row["card_id"]}'";
			$rs_fscct = $this->mysqli->query($sql);
			$row_fscct = $rs_fscct->fetch_assoc();
			$row_fscct["fscct_dtmem"] = mysqldate2thaidate($row_fscct["fscct_dtmem"]);
			$row_fscct["fscct_dtprotect"] = mysqldate2thaidate($row_fscct["fscct_dtprotect"]);
			
			echo json_encode(array(
				"status" => "TRUE",
				"member" => $row,
				"members2" => $members2,
				"thaiftsc" => $row_thaiftsc,
				"fscct" => $row_fscct
			));
		}
	}
	
	public function getReceiptEntries() {
		$sql = "SELECT cmp_imp_receipt.*, cmp_imp_memdetail.fullname_desc
					FROM cmp_imp_receipt
						INNER JOIN cmp_imp_memdetail ON cmp_imp_receipt.crmem_id = cmp_imp_memdetail.crmem_id
					WHERE cmp_imp_receipt.crmem_id = '".$_GET["crmem_id"]."' AND cmp_imp_receipt.seq_no = 1
					ORDER BY cmp_imp_receipt.receiptdoc_id DESC";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		$entries = array();
		while($row = $rs->fetch_assoc()) {
			$operdtm = explode("-", $row["receipt_dt"]);
			$year = $operdtm[0] + 543;
			$month = $operdtm[1];
			$month_name = MonthName((int)$month);
			$row["receipt_title"] = "ใบเสร็จ {$month_name} {$year}";
			array_push($entries, $row);
		}
		
		$sql = "SELECT *
					FROM cmp_imp_pay
					WHERE crmem_id = '".$_GET["crmem_id"]."'
					ORDER BY seq_no";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		$entries2 = array();
		$pay_total = 0;
		while($row = $rs->fetch_assoc()) {
			array_push($entries2, $row);
			$pay_total += $row["item_val"];
		}
		if(empty($entries2)) {
			array_push($entries2, array(
				"inv_desc" => "ไม่พบข้อมูลการเรียกเก็บเงิน",
				"item_val" => ""
			));
		}
		else {
			array_push($entries2, array(
				"inv_desc" => "รวม",
				"item_val" => "{$pay_total}"
			));
		}
		
		echo json_encode(array(
			"status" => "TRUE",
			"receipts" => $entries,
			"pays" => $entries2
		));
	}
	
	public function getMessageEntries() {
		$sql = "SELECT tb1.msg_id, tb1.msg_title, tb1.msg_senddate
						, CASE WHEN tb2.msg_id IS NULL THEN 0 ELSE 1 END AS is_fetch
						, CASE WHEN tb2.is_read IS NULL THEN 0 ELSE tb2.is_read END AS is_read
					FROM cmp_cpk_message AS tb1
					LEFT OUTER JOIN (
						SELECT msg_id, is_read
							FROM cmp_imp_memdetail_message
							WHERE crmem_id = '".$_GET["crmem_id"]."'
					) AS tb2 ON tb1.msg_id = tb2.msg_id
					WHERE tb1.msg_status = 1 AND tb1.msg_senddate < NOW()
					ORDER BY tb1.msg_senddate DESC";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		$entries = array();
		while($row = $rs->fetch_assoc()) {
			if(!$row["is_fetch"]) {
				$sql = "INSERT INTO cmp_imp_memdetail_message (
								crmem_id
								, msg_id
								, is_read
								, update_time
								, update_ip)
							VALUES (
								'".$_GET["crmem_id"]."'
								, '".$row["msg_id"]."'
								, 0
								, NOW()
								, '{$_SERVER["REMOTE_ADDR"]}')";
				@$this->mysqli->query($sql);
			}
			
			$row["msg_senddate"] = get_timeago(strtotime($row["msg_senddate"]));
			array_push($entries, $row);
		}
		
		echo json_encode(array(
			"status" => "TRUE",
			"messages" => $entries
		));
	}
	
	public function getMessage() {
		if($_GET["set_read"]) {
			$this->setReadMessage();
		}
		
		$sql = "SELECT COUNT(msg_id) AS notify_count
					FROM cmp_cpk_message
					WHERE msg_status = 1 AND msg_senddate < NOW() AND msg_id NOT IN (
						SELECT msg_id
						FROM cmp_imp_memdetail_message
						WHERE is_read = 1 AND crmem_id = '".$_GET["crmem_id"]."')";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		$rowCount = $rs->fetch_assoc();
		
		$sql = "SELECT *
					FROM cmp_cpk_message
					WHERE msg_id = '".$_GET["msg_id"]."'";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		if($row = $rs->fetch_assoc()) {
			echo json_encode(array(
				"status" => "TRUE",
				"message" => $row,
				"notify_count" => $rowCount["notify_count"]
			));
		}
	}
	
	public function setReadMessage() {
		$sql = "SELECT *
					FROM cmp_imp_memdetail_message
					WHERE crmem_id = '".$_GET["crmem_id"]."' AND msg_id = '".$_GET["msg_id"]."'";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		if($row = $rs->fetch_assoc()) {
			$sql = "UPDATE cmp_imp_memdetail_message SET is_read = 1, update_time = NOW(), update_ip = '".$_SERVER["REMOTE_ADDR"]."'
						WHERE crmem_id = '".$_GET["crmem_id"]."' AND msg_id = '".$_GET["msg_id"]."'";
		}
		else {
			$sql = "INSERT INTO cmp_imp_memdetail_message (crmem_id, msg_id, is_read, update_time, update_ip)
						VALUES ('".$_GET["crmem_id"]."', '".$_GET["msg_id"]."', 1, NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
		}
		$this->mysqli->query($sql) or die ($this->mysqli->error);
	}
	
	public function getNotify() {
		if(!empty($_GET["token"])) {
			$sql = "SELECT *
						FROM cmp_cpk_app_token
						WHERE device_token = '".$_GET["token"]."'";
			$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
			if($row = $rs->fetch_assoc()) {
				$sql = "UPDATE cmp_cpk_app_token SET crmem_id = '".$_GET["crmem_id"]."'
							WHERE device_token = '".$_GET["token"]."'";
				$this->mysqli->query($sql) or die ($this->mysqli->error);
			}
			else {
				$sql = "INSERT INTO cmp_cpk_app_token (device_token, crmem_id)
							VALUES ('".$_GET["token"]."', '".$_GET["crmem_id"]."')";
				$this->mysqli->query($sql) or die ($this->mysqli->error);
			}
		}
		
		$sql = "SELECT COUNT(msg_id) AS notify_count
					FROM cmp_cpk_message
					WHERE msg_status = 1 AND msg_senddate < NOW() AND msg_id NOT IN (
						SELECT msg_id
						FROM cmp_imp_memdetail_message
						WHERE is_read = 1 AND crmem_id = '".$_GET["crmem_id"]."')";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		$rowCount = $rs->fetch_assoc();
		
		$sql = "SELECT msg_senddate
					FROM cmp_cpk_message
					WHERE msg_status = 1 AND msg_senddate < NOW()
					ORDER BY msg_senddate DESC
					LIMIT 1";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		$rowMsg = $rs->fetch_assoc();
		
		$sql = "SELECT *
					FROM cmp_imp_cpk_stat
					ORDER BY update_time DESC
					LIMIT 1";
		$rs = $this->mysqli->query($sql) or die ($this->mysqli->error);
		$rowImp = $rs->fetch_assoc();
		
		echo json_encode(array(
			"status" => "TRUE",
			"notify_count" => $rowCount["notify_count"],
			"last_date" => "เมื่อ ".mysqldate2thaidate($rowMsg["msg_senddate"], "long", TRUE),
			"data_update_time" => "อัพเดทข้อมูลเมื่อ ".mysqldate2thaidate($rowImp["update_time"], "long", TRUE)
		));
	}
}

if($_GET["authencode"] == "c4p4bf8hrfgtkuy5gmqlckgt6a7kx") {
	$service = new Service($mysqli);
	call_user_func_array(array($service, $_REQUEST["do"]), array());
}
?>