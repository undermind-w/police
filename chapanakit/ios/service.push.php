<?php
define(PATH, $_SERVER["DOCUMENT_ROOT"]);
require(PATH."/class/connect.inc.php");

#### Config ####
if($_GET["mode"] == "development") {
    $app_config = array(
        "server" => "ssl://gateway.sandbox.push.apple.com:2195",
        "certificate" => "CMCoopCPKDev.pem",
        "passphrase" => "up69Bean"
    );
}
else if($_GET["mode"] == "production") {
    $app_config = array(
        "server" => "ssl://gateway.push.apple.com:2195",
        "certificate" => "CMCoopCPKDev.pem",
        "passphrase" => "up69Bean"
    );
}
else {
    exit;
}
#### End Config ####

function writeToLog($message) {
	if($fp = fopen(PATH.'/log/service.push.log', 'at')){
		fwrite($fp, date('c') . ' ' . $message . PHP_EOL);
		fclose($fp);
	}
}

$sql = "SELECT * FROM cmp_cpk_app_token";
$rs = $mysqli->query($sql) or die ($mysqli->error);
while($row = $rs->fetch_assoc()) {
    $sql = "INSERT INTO cmp_cpk_app_notify_queue (device_token, msg_id)
					SELECT device_token, msg_id
					FROM (
						SELECT '".$row["device_token"]."' AS device_token, msg_id, msg_senddate
						FROM cmp_cpk_message
						WHERE msg_status = 1 AND msg_senddate < NOW()
						ORDER BY msg_senddate DESC
						LIMIT 5
					) tmp
					WHERE msg_id NOT IN (SELECT msg_id FROM cmp_cpk_app_notify_queue WHERE device_token = '".$row["device_token"]."')
					ORDER BY msg_senddate";
    $mysqli->query($sql) or die ($mysqli->error);
}

echo 'Created Queue for delivery' . "<br />";

sleep(2);

$sql = "SELECT cmp_cpk_message.*, cmp_cpk_app_notify_queue.queue_id, cmp_cpk_app_notify_queue.device_token, cmp_cpk_app_token.crmem_id
			FROM cmp_cpk_app_notify_queue
				INNER JOIN cmp_cpk_message ON cmp_cpk_app_notify_queue.msg_id = cmp_cpk_message.msg_id
				INNER JOIN cmp_cpk_app_token ON cmp_cpk_app_notify_queue.device_token = cmp_cpk_app_token.device_token
			WHERE cmp_cpk_app_notify_queue.is_send = 0
			ORDER BY cmp_cpk_app_notify_queue.queue_id
			LIMIT 20";
$rs = $mysqli->query($sql) or die ($mysqli->error);

if($rs->num_rows < 1) {
    exit;
}

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', $app_config["certificate"]);
stream_context_set_option($ctx, 'ssl', 'passphrase', $app_config["passphrase"]);

// Open a connection to the APNS server
$fp = stream_socket_client(
	$app_config["server"], $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp) {
	writeToLog("Failed to connect: $err $errstr");
    exit("Failed to connect: $err $errstr" . "<br />");
}

writeToLog("Connected to APNS");
echo 'Connected to APNS' . "<br />";

while($row = $rs->fetch_assoc()) {
    $sql = "SELECT COUNT(msg_id) AS unread_count
				FROM cmp_cpk_message
				WHERE msg_status = 1 AND msg_senddate < NOW() AND msg_id NOT IN (
					SELECT msg_id
					FROM cmp_imp_memdetail_message
					WHERE is_read = 1 AND crmem_id = '".$row["crmem_id"]."')";
	$rsCount = $mysqli->query($sql) or die ($mysqli->error);
    $rowCount = $rsCount->fetch_assoc();
    
    // Create the payload body
    $body = array(
        'aps' => array(
            'alert' => $row["msg_title"],
            'badge' => (int)$rowCount["unread_count"],
            'sound' => 'default'
        ),
        'notify_id' => $row["msg_id"]
    );
    
    // Encode the payload as JSON
    $payload = json_encode($body);
    
    if (!$fp) {
        writeToLog("Failed to connect: $err $errstr");
        exit("Failed to connect: $err $errstr" . "<br />");
    }
    
    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $row["device_token"]) . pack('n', strlen($payload)) . $payload;
    
    // Send it to the server
    $result = @fwrite($fp, $msg, strlen($msg));
    
    if(!$result) {
        writeToLog("Message id ".$row["msg_id"]." not delivered to ".$row["device_token"]);
        echo 'Message not delivered' . "<br />";
        exit;
    }
    else {
        $sql = "UPDATE cmp_cpk_app_notify_queue SET is_send = 1, time_send = NOW()
                      WHERE queue_id = '".$row["queue_id"]."'";
        $mysqli->query($sql) or die ($mysqli->error);
        
        writeToLog("Message id ".$row["msg_id"]." successfully delivered to ".$row["device_token"]);
        echo 'Message successfully delivered' . "<br />";
    }
}

// Close the connection to the server
fclose($fp);

writeToLog("Complete");
echo 'Complete';

/*** End of File ***/