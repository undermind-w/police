<div class="panel panel-material-light-blue-300">
		<div class="panel-heading">
				<div class="">
						<div class="pull-left">
								<span class="fa-stack fa-2x">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-envelope fa-stack-1x text-material-light-blue-300"></i>
								</span>
						</div>
						<div class="pull-left m-l-xxs">
								<h3>ข้อความถึงท่าน</h3>
						</div>
						<div class="clearfix"></div>
				</div>
		</div>
		<div class="panel-body line-h-26">
				<?php
						$sql = "SELECT tb1.msg_id, tb1.msg_title, tb1.msg_senddate
										, CASE WHEN tb2.is_read IS NULL THEN 0 ELSE tb2.is_read END AS is_read
										FROM cmp_cpk_message AS tb1
										LEFT OUTER JOIN (
											SELECT msg_id, is_read
												FROM cmp_imp_memdetail_message
												WHERE crmem_id = '{$_COOKIE["crmem_id"]}'
										) AS tb2 ON tb1.msg_id = tb2.msg_id
										WHERE tb1.msg_status = 1
										AND tb1.msg_senddate < NOW()
										ORDER BY tb1.msg_senddate DESC";
						$rs = $mysqli->query($sql);
						if(!$rs->num_rows) echo '<h5 class="text-center">ไม่พบข้อความถึงท่าน</h5>';
						else {
								while(($row = $rs->fetch_assoc())) {
										$icon = $row["is_read"] ? "fa-envelope-o" : "fa-envelope" ;
										$time_ago = get_timeago(strtotime($row["msg_senddate"]));
										echo "<div class='row p-l-md'>
															<div class='pull-left m-r-xs'><i class='fa {$icon}' style='font-size: 22px;color: grey;'></i></div>
															<div class='pull-left'>
																	<div><a href='message.php?mid={$row["msg_id"]}'>{$row["msg_title"]}</a></div>
																	<div class='text-grey-400 f-s-11'>เมื่อ {$time_ago}</div>
															</div>
															<div class='clearfix'></div>
													</div>
													<hr class='m-v-xs' />";
								}
						}
				?>
		</div>
</div>
<?php unset($sql, $rs, $msg); ?>