<?php
	include "config.inc.php";
	require "session.inc.php";
	
	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($do == "chgpwd") {
			$val = $mysqli->real_escape_string($_POST["val"]);
			$sql = "UPDATE cmp_imp_memdetail_data SET password = '{$val}' WHERE crmem_id = '{$_COOKIE["crmem_id"]}'";
			$mysqli->query($sql);
			exit();
	}
	if($_POST) exit();

	$smarty->assign("title", "เปลี่ยนรหัสผ่าน - งาน สคช.");
	
	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
	<script>
			$(document).ready(function() {
					$("body").on("click", "#btn-submit", function() { 
							var is_error = false;
							$(".form-group").removeClass("has-error");
							$("#password_confirm").parents(".form-group").find(".help-block").html("");
							if($("#password").val() == "") {
									$("#password").parents(".form-group").addClass("has-error");
									is_error = true;
							}
							if($("#password_confirm").val() == "") {
									$("#password_confirm").parents(".form-group").addClass("has-error");
									is_error = true;
							}
							if($("#password").val() != $("#password_confirm").val()) {
									$("#password_confirm").parents(".form-group").addClass("has-error");
									$("#password_confirm").parents(".form-group").find(".help-block").html("รหัสผ่านใหม่ และยืนยันรหัสผ่านใหม่ ต้องเหมือนกันค่ะ");
									is_error = true;
							}
							if(!is_error) {
							$.ajax({
									type: "POST"
									, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
									, data: {
											"do" 	: "chgpwd" ,
											"val"	: $("#password").val() ,
											"_time" 	: Math.random()
									}
									, async: true
									, success: function(response) {
											$("#modal-password").modal("show");
									}
							});
							}
					});
			});
	</script>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	require "subject.inc.php";
	//$smarty->assign("subject", "CMCOOP Online");
	//$smarty->assign("breadcrumb", array("เข้าสู่ระบบ" => null ));
	ob_start();
?>
	<div class="row m-t-md">
			<div class="col-md-3">
					<h2>ยินดีต้อนรับ</h2>
					<h4>
							<?php
								$sql = "SELECT *
												FROM cmp_imp_cpk_stat
												ORDER BY update_time DESC
												LIMIT 1";
								$rs = $mysqli->query($sql);
								$row = $rs->fetch_assoc();
							?>
							อัพเดท <?php echo mysqldate2thaidate($row["update_time"], false, true); ?>
					</h4>
					<?php require "sidebar.inc.php"; ?>
			</div>
			<div class="col-md-9">
					<h2>&nbsp;</h2>
					<h4>&nbsp;</h4>
					<div class="panel panel-material-light-blue-300">
							<div class="panel-heading">
									<div class="">
											<div class="pull-left">
													<span class="fa-stack fa-2x">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-key fa-stack-1x text-material-light-blue-300"></i>
													</span>
											</div>
											<div class="pull-left m-l-xxs">
													<h3>เปลี่ยนรหัสผ่าน</h3>
											</div>
											<div class="pull-right badget">
											</div>
											<div class="clearfix"></div>
									</div>
							</div>
							<div class="panel-body">
									<form id="frm" class="form-horizontal">
											<div class="form-group">
													<label for="member_no" class="col-sm-offset-1 col-sm-4 control-label"><h4 class="no-m">รหัสผ่านใหม่</h4></label>
													<div class="col-sm-4">
															<input class="form-control" id="password" name="password" type="password" autocomplete="off" />
													</div>
											</div>
											<div class="form-group">
													<label for="member_no" class="col-sm-offset-1 col-sm-4 control-label"><h4 class="no-m">ยืนยันรหัสผ่านใหม่</h4></label>
													<div class="col-sm-4">
															<input class="form-control" id="password_confirm" name="password_confirm" type="password" autocomplete="off" />
															<div class="help-block"></div>
													</div>
											</div>
											 <div class="form-group">
													<div class="col-sm-12 text-center">
															<button class="btn btn-lg btn-material-light-blue-300" type="button" id="btn-submit">เปลี่ยนรหัสผ่าน</button>
													</div>
											</div>
									</form>
							
							</div>
					</div>
			</div>
	</div>
	
<div class="modal" id="modal-password" tabindex="-1" role="dialog" aria-labelledby="modal-password-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body text-center">
					<h1>เปลี่ยนรหัสผ่านเรียบร้อยแล้วค่ะ</h1>
					<button type="button" class="btn btn-lg btn-default" data-dismiss="modal">ปิดหน้าต่าง</button>
      </div>
    </div>
  </div>
</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");