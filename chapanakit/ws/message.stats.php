<?php

	session_start();
	include "config.inc.php";
 	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
		 
	$crmem_id = isset($_REQUEST["crmem_id"]) ? $_REQUEST["crmem_id"] : 0 ;

	$sql = "SELECT tb1.msg_id
					, tb1.msg_title
					, tb1.msg_url
					, tb1.msg_senddate
					, CASE WHEN tb2.msg_id IS NULL THEN 0 ELSE 1 END AS is_fetch
					, CASE WHEN tb2.is_read IS NULL THEN 0 ELSE tb2.is_read END AS is_read
					
					FROM cmp_cpk_message AS tb1
					LEFT OUTER JOIN (
						SELECT msg_id, is_read
							FROM cmp_imp_memdetail_message
							WHERE crmem_id = '{$crmem_id}'
					) AS tb2 ON tb1.msg_id = tb2.msg_id
					WHERE tb1.msg_status = 1
					AND tb1.msg_senddate < NOW()
					ORDER BY tb1.msg_senddate DESC";

	$rs = $mysqli->query($sql);
	echo $mysqli->error; 
	
	 /*
	$sql = "SELECT date_upload FROM history_upload ORDER BY date_upload  DESC LIMIT 0 , 1 " ;
	$rs1 = $mysqli->query($sql);
	$update_time = $rs1->fetch_assoc();
	 */
	$update_time = array();
	$update_time["date_upload"] = date("Y-m-d H:i:s");
	if(!empty($rs)) {
			
			$new = 0;
			$unread = 0;
			$data = array();
			$time = "";
			$x = 0 ;
			 
			
			while(($row = $rs->fetch_assoc())) {
					$new += !$row["is_fetch"];
					$unread	+= !$row["is_read"];
					if(empty($time)) $time = "เมื่อ ".mysqldate2thaidate($row["msg_senddate"]);
					$x++ ;

			}
			
			

			$json["status"] = 1;
			$json["data"] = $data;
			$json["count"] = $rs->num_rows;
			$json["new"] = $new;
			$json["unread"] = $unread;
			$json["time"] = $time ;
			$json["update_db_time"] = "อัพเดทฐานข้อมูลเมิ่อ " . @mysqldate2thaidate($update_time["date_upload"] , "short" , true ) ;
			
	} else {
			$json["status"] = 0;
			$json["data"] = array();
			$json["count"] = 0;
			$json["new"] 	= 0;
			$json["unread"] = 0;
			$json["time"] = "" ; 
			$json["update_db_time"] = "อัพเดทฐานข้อมูลเมิ่อ " . @mysqldate2thaidate($update_time["date_upload"] , "short" , true ) ;
	}

	echo json_encode($json);
 