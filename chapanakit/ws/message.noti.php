<?php
	include "config.inc.php";
	//db_connect();
	 
	$crmem_id = isset($_REQUEST["crmem_id"]) ? $_REQUEST["crmem_id"] : 0 ;
	$debug = isset($_REQUEST["debug"]) ? 1 : 0 ;
	$crmem_id = $mysqli->real_escape_string($crmem_id) ; 
	
	
	
	if(empty($crmem_id)){
		echo json_encode(array("status" => "0" , "text" => "ไม่มีรหัสผู้ใช่งาน" ));
		exit();
	}
	 
	 
	$sql = "SELECT tb1.msg_id 
					,  CASE WHEN tb2.msg_id IS NULL THEN 0 ELSE 1 END AS is_fetch
					FROM cmp_cpk_message AS tb1
					LEFT OUTER JOIN (
						SELECT msg_id, is_read
							FROM cmp_imp_memdetail_message
							WHERE crmem_id = '{$crmem_id}'
					) AS tb2 ON tb1.msg_id = tb2.msg_id
					WHERE tb1.msg_status = 1
					AND tb1.msg_senddate < NOW()
 					ORDER BY tb1.msg_senddate DESC " ; 
	$rs = $mysqli->query($sql);
	echo $mysqli->error; 
	$noti = 0 ; 
	
	while($row  = $rs->fetch_assoc()){
		if($row["is_fetch"] == 0 ){
			$noti ++ ; 
		} 
	}
	 
	if( $noti == 0 ){
		echo json_encode(array("status" => "0" , "text" => "ไม่มีข้อความใหม่" ));
		exit();
	}
	
	
	$sql = "SELECT tb1.msg_id
					, tb1.msg_title
					, tb1.msg_url
					, tb1.msg_senddate
					, CASE WHEN tb2.msg_id IS NULL THEN 0 ELSE 1 END AS is_fetch
					, CASE WHEN tb2.is_read IS NULL THEN 0 ELSE tb2.is_read END AS is_read
					
					FROM cmp_cpk_message AS tb1
					LEFT OUTER JOIN (
						SELECT msg_id, is_read
							FROM cmp_imp_memdetail_message
							WHERE crmem_id = '{$crmem_id}'
					) AS tb2 ON tb1.msg_id = tb2.msg_id
					
					WHERE tb1.msg_status = 1
					AND tb1.msg_senddate < NOW()
 					ORDER BY tb1.msg_senddate DESC";

	$rs = $mysqli->query($sql);
	echo $mysqli->error; 
	
	
	if(!empty($rs)) {
			$new = 0;
			$unread = 0;
			$data = array();
			$time = "";
			$x = 0 ;
			
			while(($row = $rs->fetch_assoc())) {
					$new += !$row["is_fetch"];
					$unread	+= !$row["is_read"];
					
					if(empty($time)) $time = "เมื่อ ".mysqldate2thaidate($row["msg_senddate"]);
					
					$row["msg_timeago"] = get_timeago(strtotime($row["msg_senddate"]));
					
					if($row["is_fetch"] == 0 ){
						$data[] = $row;
					}
					
					$x++ ;
					if($debug == 0) {
					if(!$row["is_fetch"]) {
							$id = $row["msg_id"];
							$sql = "INSERT INTO cmp_imp_memdetail_message ( 
															crmem_id
															, msg_id 
															, is_read
															, update_time 
															, update_ip)
														VALUES('{$crmem_id}'
																, {$id} 
																, 0
																, NOW()
																, '{$_SERVER["REMOTE_ADDR"]}'
																)";
							//if($member_no != "010025") {
							@$mysqli->query($sql);
							//}
					}
				}
			}

			$json["status"] = 1;
			$json["data"] = $data;
			$json["count"] = $rs->num_rows;
			$json["new"] = $new;
			$json["unread"] = $unread;
			
	} else {
			$json["status"] = 0;
			$json["data"] = array();
			$json["count"] = 0;
			$json["new"] 	= 0;
			$json["unread"] = 0;
	}
 

	echo json_encode($json);
	
	//db_close();