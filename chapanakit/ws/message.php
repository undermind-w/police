<?php
 
	session_start();
	include "config.inc.php";
 	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
	$crmem_id = isset($_REQUEST["crmem_id"]) ? $_REQUEST["crmem_id"] : 0 ;
	$type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "" ;

	$sql = "SELECT tb1.msg_id
					, tb1.msg_title
					, tb1.msg_url
					, tb1.msg_senddate
					, CASE WHEN tb2.msg_id IS NULL THEN 0 ELSE 1 END AS is_fetch
					, CASE WHEN tb2.is_read IS NULL THEN 0 ELSE tb2.is_read END AS is_read
					FROM cmp_cpk_message AS tb1
					LEFT OUTER JOIN (
						SELECT msg_id , is_read
							FROM cmp_imp_memdetail_message
							WHERE crmem_id = '{$crmem_id}'
					) AS tb2 ON tb1.msg_id = tb2.msg_id
					WHERE tb1.msg_status = 1
					AND tb1.msg_senddate < NOW()
					ORDER BY tb1.msg_senddate DESC";
	$rs = $mysqli->query($sql);

	/*
	$sql = "SELECT update_time FROM cmp_imp_stat ORDER BY update_time DESC " ;
	$rs1 = $mysqli->query($sql);
	$update_time = $rs1->fetch_assoc();
	*/
	$update_time = date("Y-m-d");

	if($rs) {
			$new = 0;
			$unread = 0;
			$data = array();
			$time = "";
			$x = 0 ;
			while(($row = $rs->fetch_assoc())) {
					$new 		+= !$row["is_fetch"];
					$unread	+= !$row["is_read"];
					if(empty($time)) $time = "เมื่อ ".mysqldate2thaidate($row["msg_senddate"]);
					$row["msg_timeago"] = get_timeago(strtotime($row["msg_senddate"]));
					//$data[] = $row;

					if($type == "noti" ){
						if($x == 0 ){
							$data[] = $row;
						}
					}else{
						$data[] = $row;
					}

					$x++ ;
					if(!$row["is_fetch"]) {
							$id = $row["msg_id"];
							$sql = "INSERT INTO cmp_imp_memdetail_message(crmem_id
																		, msg_id
																		, is_read
																		, update_time
																		, update_ip)
																	VALUES( '{$crmem_id}'
																			, {$id}
																			, 0
																			, NOW()
																			, '{$_SERVER["REMOTE_ADDR"]}')";
							@$mysqli->query($sql);
					}
			}

			$json["status"] 		= 1;
			$json["data"] 			= $data;
			$json["data_count"] = $rs->num_rows;
			$json["data_new"] 	= $new;
			$json["unread"] 		= $unread;
			$json["msg_time"] 	= $time;
			$json["update_db_time"] = "อัพเดทฐานข้อมูลเมิ่อ " . @mysqldate2thaidate($update_time["update_time"] , "short" , true ) ;
	} else {
			$json["status"] 		= 0;
			$json["data"] 			= array();
			$json["data_count"] = 0;
			$json["data_new"] 	= 0;
			$json["unread"] 		= 0;
			$json["msg_time"] 	= "";
			$json["update_db_time"]  = "อัพเดทฐานข้อมูลเมิ่อ " . @mysqldate2thaidate($update_time["update_time"] , "short" , true ) ;
	}

	echo json_encode($json);