<?php
	session_start();
	include "config.inc.php";
 	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
 	$crmem_id = $mysqli->real_escape_string(@$_REQUEST["crmem_id"])  ; 
	$sql = "SELECT  
				cmp_imp_memdetail.crmem_id 
				, cmp_imp_memdetail.fullname_desc as name
				, cmp_imp_memdetail.memtyp_desc
				, cmp_imp_memdetail_data.email 
				, cmp_imp_memdetail.birth_dt 
				, cmp_imp_memdetail.mem_dt 
 				, TIMESTAMPDIFF(MONTH, cmp_imp_memdetail.birth_dt, CURDATE()) AS birthdtm_age
				, TIMESTAMPDIFF(MONTH, cmp_imp_memdetail.mem_dt, CURDATE()) AS memdtm_age
				, cmp_imp_fscct.fscct_id 
				, cmp_imp_fscct.prename as fscct_prename
				, cmp_imp_fscct.name as fscct_name
				, cmp_imp_fscct.sname as fscct_sname
				, cmp_imp_fscct.fscct_memtyp 
				, cmp_imp_fscct.fscct_dtmem
				, cmp_imp_fscct.fscct_dtprotect
 				, TIMESTAMPDIFF(MONTH, cmp_imp_fscct.fscct_dtmem, CURDATE()) AS fscct_dtmem_age
				, TIMESTAMPDIFF(MONTH, cmp_imp_fscct.fscct_dtprotect, CURDATE()) AS fscct_dtprotect_age
				, cmp_imp_thaiftsc.thaiftsc_id 
				, cmp_imp_thaiftsc.prename as thaiftsc_prename
				, cmp_imp_thaiftsc.name as thaiftsc_name
				, cmp_imp_thaiftsc.sname as thaiftsc_sname
				, cmp_imp_thaiftsc.thaiftsc_memtpy 
				, cmp_imp_thaiftsc.thaiftsc_dtmem
				, cmp_imp_thaiftsc.thaiftsc_dtprotect
 				, TIMESTAMPDIFF(MONTH, cmp_imp_thaiftsc.thaiftsc_dtmem, CURDATE()) AS thaiftsc_dtmem_age
				, TIMESTAMPDIFF(MONTH, cmp_imp_thaiftsc.thaiftsc_dtprotect, CURDATE()) AS thaiftsc_dtprotect_age
				FROM cmp_imp_memdetail 
				LEFT JOIN cmp_imp_memdetail_data ON cmp_imp_memdetail.crmem_id = cmp_imp_memdetail_data.crmem_id 
				LEFT JOIN cmp_imp_fscct ON cmp_imp_memdetail.card_id = cmp_imp_fscct.id 
				LEFT JOIN cmp_imp_thaiftsc ON cmp_imp_memdetail.card_id = cmp_imp_thaiftsc.id 
				WHERE cmp_imp_memdetail.crmem_id = '{$crmem_id}' " ;
		
	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	$member = $rs->fetch_assoc();
	
	$birthdtm_age = array();
	$birthdtm_age["year"] = floor($member["birthdtm_age"] / 12);
	$birthdtm_age["month"] = $member["birthdtm_age"] % 12;
	$memdtm_age = array();
	$memdtm_age["year"] = floor($member["memdtm_age"] / 12);
	$memdtm_age["month"] = $member["memdtm_age"] % 12;
	
	$b1 = strtotime(@$member["birth_dt"]) ; 
	$b2 = strtotime(date("Y-m-d")) ; 
	$b3  = (int)abs(($b2 - $b1)/(60*60*24*30));
	$b3year = floor( $b3 / 12 )  ; 
	$age = "" . $b3year . " ปี " . ( $b3 % 12 ) . " เดือน" ; 
	$age = "{$birthdtm_age["year"]}  ปี {$birthdtm_age["month"]} เดือน" ; 
	
	$member["age"] = $age ; 
	
	$b1 = strtotime(@$member["mem_dt"]) ; 
	$b2 = strtotime(date("Y-m-d")) ; 
	$b3  = (int)abs(($b2 - $b1)/(60*60*24*30));
	$b3year = floor( $b3 / 12 )  ; 
	$memage = "" . $b3year . " ปี " . ( $b3 % 12 ) . " เดือน" ; 
	$memage = "{$memdtm_age["year"]} ปี {$memdtm_age["month"]} เดือน" ; 
	
	$member["memage"] = $memage ; 
	
	$d1 = explode(" " , $member["birth_dt"] ) ;
	$d2 = explode(" " , $member["mem_dt"] ) ;
	$member["birthdtm"] = ConvertToThaiDate($d1[0] , true);
	$member["memdtm"] = ConvertToThaiDate($d2[0], true);
	
	//fscct
	if(!empty($member["fscct_id"] )){
		$member["fscct_fullname"] = $member["fscct_prename"] . " " . $member["fscct_name"] . " " . $member["thaiftsc_sname"] ; 
		$d1 = explode(" " , $member["fscct_dtmem"] ) ;
		$d2 = explode(" " , $member["fscct_dtprotect"] ) ;
		$member["fscct_memdtm"] = ConvertToThaiDate($d1[0] , true);
		$member["fscct_protectdtm"] = ConvertToThaiDate($d2[0], true);
	}else{
		$member["fscct_id"] = "" ;
		$member["fscct_memtyp"] = "" ;
		$member["fscct_fullname"] = "" ; 
		$member["fscct_memdtm"] = "" ;
		$member["fscct_protectdtm"] = "" ;
	}
	
	  
	//thaiftsc
	if(!empty($member["thaiftsc_id"] )){
		$member["thaiftsc_fullname"] = $member["thaiftsc_prename"] . " " . $member["thaiftsc_name"] . " " . $member["thaiftsc_sname"] ; 
		$d1 = explode(" " , $member["thaiftsc_dtmem"] ) ;
		$d2 = explode(" " , $member["thaiftsc_dtprotect"] ) ;
		$member["thaiftsc_memdtm"] = ConvertToThaiDate($d1[0] , true);
		$member["thaiftsc_protectdtm"] = ConvertToThaiDate($d2[0], true);
	}else{
		$member["thaiftsc_id"] = "" ;
		$member["thaiftsc_memtpy"] = "" ;
		$member["thaiftsc_fullname"] = "" ; 
		$member["thaiftsc_memdtm"] = "";
		$member["thaiftsc_protectdtm"] = "";
	}
	 
	
	
	
	
	$member["status"] = 1;
	echo json_encode( array( "status" => 1 , "data" => $member )) ; 
 
	exit();