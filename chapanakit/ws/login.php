<?php
	session_start();
	include "config.inc.php";
 	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
 	$crmem_id 	= isset($_REQUEST["crmem_id"]) ? $mysqli->real_escape_string($_REQUEST["crmem_id"]) : null ;
	$password 	= $mysqli->real_escape_string($_REQUEST["password"]);
	
	
	$data = array();
	$sql = "SELECT tb1.crmem_id
					FROM cmp_imp_memdetail AS tb1
					LEFT OUTER JOIN cmp_imp_memdetail_data AS tb2 ON tb1.crmem_id = tb2.crmem_id
					WHERE tb1.crmem_id = '{$crmem_id}'
						AND CASE WHEN tb2.password IS NULL THEN tb1.card_id ELSE tb2.password END = '{$password}'
					LIMIT 1";
	$rs = $mysqli->query($sql);
	if($rs->num_rows) {
			$data["status"] = 1;
			$data["text"] = 1;
	} else {
			$data["status"] = 0;
			$data["text"] = "Username or Password ไม่ถูกต้อง";
	}
	echo json_encode($data);