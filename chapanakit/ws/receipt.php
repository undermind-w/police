<?php
	session_start();
	include "config.inc.php";
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
 	$crmem_id = $mysqli->real_escape_string(@$_REQUEST["crmem_id"])  ; 
	
	//echo $_COOKIE["crmem_id"] ; 
	
	$sql = "SELECT cmp_imp_receipt.*, cmp_imp_memdetail.fullname_desc
				FROM cmp_imp_receipt
					INNER JOIN cmp_imp_memdetail ON cmp_imp_receipt.crmem_id = cmp_imp_memdetail.crmem_id
				WHERE cmp_imp_receipt.crmem_id = '{$crmem_id}' AND cmp_imp_receipt.seq_no = 1
				ORDER BY cmp_imp_receipt.receiptdoc_id DESC";
	$rs = $mysqli->query($sql);
 	echo $mysqli->error ; 
 	$data = array();
	while(($row = $rs->fetch_assoc())) {
 		$operdtm 		= explode("-", $row["receipt_dt"]);
		$year 			= $operdtm[0] + 543;
		$month 			= $operdtm[1];
		$month_name	= MonthName((int)$month);
		
		$data[] = array("id" =>  $row["receiptdoc_id"]  , "title" => "{$month_name} {$year}" );
 
	}
	
	echo json_encode( array( "status" => 1 , "data" => $data) ) ; 