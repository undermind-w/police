<div class="panel panel-material-light-blue-300">
		<div class="panel-heading">
				<div class="">
						<div class="pull-left">
								<span class="fa-stack fa-2x">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-user fa-stack-1x text-material-light-blue-300"></i>
								</span>
						</div>
						<div class="pull-left m-l-xxs">
								<h3>ประวัติสมาชิก</h3>
						</div>
						<div class="clearfix"></div>
				</div>
		</div>
		<?php
				$sql = "SELECT tb1.*
									, TIMESTAMPDIFF(MONTH, tb1.birth_dt, CURDATE()) AS birth_dt_age
									, TIMESTAMPDIFF(MONTH, tb1.mem_dt, CURDATE()) AS mem_dt_age
									, tb2.email
								FROM cmp_imp_memdetail AS tb1
									LEFT OUTER JOIN cmp_imp_memdetail_data AS tb2 ON tb1.crmem_id = tb2.crmem_id
								WHERE tb1.crmem_id = '{$_COOKIE["crmem_id"]}'";
				$rs = $mysqli->query($sql);
				$row = $rs->fetch_assoc();
				$birth_dt_age = array();
				$birth_dt_age["year"] = floor($row["birth_dt_age"] / 12);
				$birth_dt_age["month"] = $row["birth_dt_age"] % 12;
				$mem_dt_age = array();
				$mem_dt_age["year"] = floor($row["mem_dt_age"] / 12);
				$mem_dt_age["month"] = $row["mem_dt_age"] % 12;
				
				$sql = "SELECT * FROM cmp_imp_member WHERE card_person = '{$row["card_id"]}'";
				$rs_member = $mysqli->query($sql);
				$row_member = $rs_member->fetch_assoc();
		?>
		<div class="panel-body line-h-26">
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">เลขที่สมาชิก</div>
					<div class="col-xs-8 col-md-3"><?php echo $row["crmem_id"]; ?></div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">ชื่อ - สกุล</div>
					<div class="col-xs-8 col-md-4"><?php echo $row["fullname_desc"]; ?></div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">เลขที่สมาชิกสหกรณ์ฯ</div>
					<div class="col-xs-8 col-md-3"><?php echo $row["mem_id"]; ?></div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">ประเภทสมาชิก</div>
					<div class="col-xs-8 col-md-4"><?php echo $row["memtyp_desc"]; ?></div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">เลขที่บัตรประชาชน</div>
					<div class="col-xs-8 col-md-3"><?php echo $row["card_id"]; ?></div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">สังกัด</div>
					<div class="col-xs-8 col-md-4"><?php echo "{$row["memgrp_id"]} - {$row["memgrp_desc"]}"; ?></div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">วันเดือนปีเกิด</div>
					<div class="col-xs-8 col-md-9 p-r-xxs">
							<?php
							$dt = explode(" ",$row["birth_dt"]);
							echo empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true);
							?>
							<span class="f-bold m-l-md">วันที่เป็นสมาชิก</span>
							<span class="m-l-xs">
									<?php
									$dt = explode(" ",$row["mem_dt"]);
									echo empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true);
									?>
							</span>
							<span class="f-bold m-l-md">วันที่คุ้มครอง</span>
							<span class="m-l-xs">
									<?php
									$dt = explode(" ",$row["protect_dt"]);
									echo empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true);
									?>
							</span>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">ที่อยู่</div>
					<div class="col-xs-8 col-md-3">
							<span class="span-addr">
							<?php echo $row["currfull_desc"]; ?>
							</span>
					</div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">โทรศัพท์</div>
					<div class="col-xs-8 col-md-4"><?php echo $row["currtel_desc"]; ?></div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">การชำระเงิน</div>
					<div class="col-xs-8 col-md-3"><?php echo $row["methpay_desc"]; ?></div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">เรียกเก็บเงินจาก</div>
					<div class="col-xs-8 col-md-4"><?php echo $row["invpayist_sts"]; ?></div>
				</div>
				
				<div class="row">
					<div class="col-md-12 f-bold m-t-md">สมาชิกสมทบ</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ol>
							<?php
							$sql = "SELECT * FROM cmp_imp_memdetail WHERE mem_id = '{$row["mem_id"]}' AND memtyp_desc <> 'สมาชิกปกติ'";
							$rs2 = $mysqli->query($sql);
							while($row2 = $rs2->fetch_assoc()) { ?>
								<li>
									<div style="display: inline-block; width: 32%;"><?php echo $row2["fullname_desc"]; ?></div>
									<div style="display: inline-block; width: 32%;">เลขที่สมาชิก <span class="m-l-xs"><?php echo $row2["crmem_id"]; ?></span></div>
									<div style="display: inline-block; width: 32%;">ประเภทสมาชิก <span class="m-l-xs"><?php echo $row2["memtyp_desc"]; ?></span></div>
								</li>
							<?php } ?>
						</ol>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 f-bold m-t-md">รายการเคลื่อนไหว</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th class="text-center" width="100">ลำดับที่</th>
									<th class="text-center" width="120">วันที่</th>
									<th class="text-center" width="150">เลขที่ใบเสร็จ</th>
									<th class="text-center">รายการ</th>
									<th class="text-right" width="100">จำนวนเงิน</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$sql = "SELECT * FROM cmp_imp_memstm WHERE crmem_id = '{$_COOKIE["crmem_id"]}' ORDER BY seq_no DESC";
								$rs2 = $mysqli->query($sql);
								while($row2 = $rs2->fetch_assoc()) { ?>
									<tr>
										<td class="text-center"><?php echo $row2["seq_no"]; ?></td>
										<td class="text-center">
											<?php
											$dt = explode(" ",$row2["opr_dt"]);
											echo empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true);
											?>
										</td>
										<td class="text-center"><?php echo $row2["refdoc_no"]; ?></td>
										<td><?php echo $row2["memstm_desc"]; ?></td>
										<td class="text-right"><?php echo $row2["item_val"]; ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				
				<?php
				$sql = "SELECT * FROM cmp_imp_thaiftsc WHERE id = '{$row["card_id"]}'";
				$rs_thaiftsc = $mysqli->query($sql);
				$row_thaiftsc = $rs_thaiftsc->fetch_assoc();
				?>
				<div class="row">
					<div class="col-md-12 f-bold m-t-md">สมาคมฌาปนกิจสงเคราะห์สมาชิกชุมนุมสหกรณ์ออมทรัพย์ครูไทย (สสอค.)</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">เลขที่ฌาปนกิจ</div>
					<div class="col-xs-8 col-md-3"><?php echo $row_thaiftsc["thaiftsc_id"]; ?></div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">ชื่อ - สกุล</div>
					<div class="col-xs-8 col-md-4"><?php echo $row_thaiftsc["prename"].$row_thaiftsc["name"]." ".$row_thaiftsc["sname"]; ?></div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">ประเภทสมาชิก</div>
					<div class="col-xs-8 col-md-9"><?php echo $row_thaiftsc["thaiftsc_memtpy"]; ?></div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">วันที่สมัคร</div>
					<div class="col-xs-8 col-md-3">
						<?php
						$dt = explode(" ",$row_thaiftsc["thaiftsc_dtmem"]);
						echo empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true);
						?>
					</div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">วันที่เป็นสมาชิก</div>
					<div class="col-xs-8 col-md-4">
						<?php
						$dt = explode(" ",$row_thaiftsc["thaiftsc_dtprotect"]);
						echo empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true);
						?>
					</div>
				</div>
				
				<?php
				$sql = "SELECT * FROM cmp_imp_fscct WHERE id = '{$row["card_id"]}'";
				$rs_fscct = $mysqli->query($sql);
				$row_fscct = $rs_fscct->fetch_assoc();
				?>
				<div class="row">
					<div class="col-md-12 f-bold m-t-md">สมาคมฌาปนกิจสงเคราะห์สหกรณ์สมาชิกของชุมนุมสหกรณ์ออมทรัพย์แห่งประเทศไทย (สส.ชสอ.)</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">เลขที่ฌาปนกิจ</div>
					<div class="col-xs-8 col-md-3"><?php echo $row_fscct["fscct_id"]; ?></div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">ชื่อ - สกุล</div>
					<div class="col-xs-8 col-md-4"><?php echo $row_fscct["prename"].$row_fscct["name"]." ".$row_fscct["sname"]; ?></div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">ประเภทสมาชิก</div>
					<div class="col-xs-8 col-md-9"><?php echo $row_fscct["fscct_memtyp"]; ?></div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-3 p-r-xxs text-right f-bold">วันที่สมัคร</div>
					<div class="col-xs-8 col-md-3">
						<?php
						$dt = explode(" ",$row_fscct["fscct_dtmem"]);
						echo empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true);
						?>
					</div>
					<div class="col-xs-4 col-md-2 p-r-xxs text-right f-bold">วันที่เป็นสมาชิก</div>
					<div class="col-xs-8 col-md-4">
						<?php
						$dt = explode(" ",$row_fscct["fscct_dtprotect"]);
						echo empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true);
						?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 text-center m-t-xxl m-b-md"><a class="btn btn-material-light-blue-300 btn-lg p-h-xxl" href="profile.pdf.php" target="_blank"><i class="fa fa-print"></i>&nbsp;พิมพ์ทะเบียนสมาชิก</a></div>
				</div>
		</div>
</div>
<?php unset($sql, $rs, $row); ?>
<div class="modal modal-vcenter" id="modal-profile" tabindex="-1" role="dialog" aria-labelledby="modal-profile-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title" id="modal-profile-label"></h2>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>