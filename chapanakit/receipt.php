<?php
	include "config.inc.php";
	include "session.inc.php";
	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	$smarty->assign("title", "พิมพ์ใบเสร็จ - งาน สคช.");

	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
	<script>
			$(document).ready(function() {

			});
	</script>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	include "subject.inc.php";
	//$smarty->assign("subject", "CMCOOP Online");
	//$smarty->assign("breadcrumb", array("เข้าสู่ระบบ" => null ));
	ob_start();
?>
	<div class="row m-t-md">
		<div class="col-md-3">
			<h2>ยินดีต้อนรับ</h2>
			<h4>
					<?php
						$sql = "SELECT *
										FROM cmp_imp_cpk_stat
										ORDER BY update_time DESC
										LIMIT 1";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
					?>
					อัพเดท <?php echo mysqldate2thaidate($row["update_time"], false, true); ?>
			</h4>
			<?php include "sidebar.inc.php"; ?>
		</div>
		<div class="col-md-9">
			<h2>&nbsp;</h2>
			<h4>&nbsp;</h4>
			<?php if(!isset($_GET["id"])) { ?>
				<div class="panel panel-material-light-blue-300">
					<div class="panel-heading">
							<div class="">
									<div class="pull-left">
											<span class="fa-stack fa-2x">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-print fa-stack-1x text-material-light-blue-300"></i>
											</span>
									</div>
									<div class="pull-left m-l-xxs">
											<h3>รายการเรียกเก็บเงินประจำเดือน</h3>
									</div>
									<div class="pull-right badget">
									</div>
									<div class="clearfix"></div>
							</div>
					</div>
					<div class="panel-body">
						<?php
										$sql = "SELECT *
													FROM cmp_imp_pay
													WHERE crmem_id = '{$_COOKIE["crmem_id"]}'
													ORDER BY seq_no";
										$rs = $mysqli->query($sql);
										if($rs->num_rows > 0) {
							?>
							<table class="table">
									<thead>
											<tr>
													<th class="text-left">รายการ</th>
													<th class="text-right" width="150">จำนวนเงิน</th>
											</tr>
									</thead>
									<tbody>
											<?php
													$total = 0;
													while(($row = $rs->fetch_assoc())) {
											?>
											<tr>
													<td class="text-left"><?php echo $row["inv_desc"]; ?></td>
													<td class="text-right"><?php echo $row["item_val"]; ?></td>
											</tr>
											<?php
														$total += $row["item_val"];
													}
											?>
									</tbody>
									<tfoot>
											<tr style="font-weight: bold;">
													<td class="text-left">รวม</td>
													<td class="text-right"><?php echo $total; ?></td>
											</tr>
									</tfoot>
							</table>
							<?php
										} else {
							?>
							<h5 class="text-center">ไม่พบข้อมูลการเรียกเก็บเงิน</h5>
							<?php
										}
										unset($sql, $rs, $row);
							?>
					</div>
				</div>
			<?php } ?>
			
			<div class="panel panel-material-light-blue-300">
				<div class="panel-heading">
						<div class="">
								<div class="pull-left">
										<span class="fa-stack fa-2x">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-print fa-stack-1x text-material-light-blue-300"></i>
										</span>
								</div>
								<div class="pull-left m-l-xxs">
										<h3>พิมพ์ใบเสร็จ</h3>
								</div>
								<div class="pull-right badget">
								</div>
								<div class="clearfix"></div>
						</div>
				</div>
				<div class="panel-body">
						<?php
								if(!isset($_GET["id"])) {
									$sql = "SELECT cmp_imp_receipt.*, cmp_imp_memdetail.fullname_desc
												FROM cmp_imp_receipt
													INNER JOIN cmp_imp_memdetail ON cmp_imp_receipt.crmem_id = cmp_imp_memdetail.crmem_id
												WHERE cmp_imp_receipt.crmem_id = '{$_COOKIE["crmem_id"]}' AND cmp_imp_receipt.seq_no = 1
												ORDER BY cmp_imp_receipt.receiptdoc_id DESC";
									$rs = $mysqli->query($sql);
									if($rs->num_rows > 0) {
						?>
						<table class="table">
								<thead>
										<tr>
												<th class="text-left" width="120">เดือน / ปี</th>
												<th class="text-center" width="150">เลขที่</th>
												<th class="text-center" width="150">ชื่อ - นามสกุล</th>
												<th class="text-center" width="150">เลขที่สมาคม</th>
												<th class="text-center">&nbsp;</th>
										</tr>
								</thead>
								<tbody>
										<?php
												while(($row = $rs->fetch_assoc())) {
														$operdtm 		= explode("-", $row["receipt_dt"]);
														$year 			= $operdtm[0] + 543;
														$month 			= $operdtm[1];
														$month_name	= MonthName((int)$month);
										?>
										<tr>
												<td class="text-left"><?php echo "{$month_name} {$year}"; ?></td>
												<td class="text-center"><?php echo $row["receiptdoc_id"]; ?></td>
												<td class="text-left"><?php echo $row["fullname_desc"]; ?></td>
												<td class="text-center"><?php echo $row["crmem_id"]; ?></td>
												<td class="text-right"><a class="text-info" href="?id=<?php echo $row["receiptdoc_id"]; ?>">รายละเอียด</span></td>
										</tr>
										<?php
												}
										?>
								</tbody>
						</table>
						<?php
									} else {
						?>
						<h5 class="text-center">ไม่พบข้อมูลการชำระเงิน</h5>
						<?php
									}
									unset($sql, $rs, $row);
								} else {
										$receiptdoc_id = $_GET["id"];
						?>
						<?php
								$sql = "SELECT cmp_imp_receipt.*, cmp_imp_memdetail.mem_id, cmp_imp_memdetail.fullname_desc, cmp_imp_memdetail.memtyp_desc
											FROM cmp_imp_receipt
												INNER JOIN cmp_imp_memdetail ON cmp_imp_receipt.crmem_id = cmp_imp_memdetail.crmem_id
											WHERE cmp_imp_receipt.receiptdoc_id = '{$receiptdoc_id}'
											ORDER BY cmp_imp_receipt.seq_no";
								$rs_receipt = $mysqli->query($sql);
								$receipt = $rs_receipt->fetch_assoc();
								$operdtm = explode(" ", $receipt["receipt_dt"]);
								$operdtm = explode("-", $operdtm[0]);
								$operdtm[0] += 543;
								$year = $operdtm[0];
								$month = $operdtm[1];
								$month_name = MonthName((int)$month);
								$total = $receipt["item_val"];
		
								unset($sql, $rs);
								$sql = "SELECT *
											FROM cmp_imp_death
											WHERE death_prd = '".$receipt["invoices_prd"]."'
											ORDER BY death_no";
								$rs = $mysqli->query($sql);
						?>
						<div class="row">
								<div class="col-xs-6">เลขที่ <?php echo $receipt["receiptdoc_id"]; ?></div>
								<div class="col-xs-4 text-right">วันที่</div>
								<div class="col-xs-2"><?php echo "{$operdtm[2]}/{$operdtm[1]}/{$operdtm[0]}"; ?></div>
						</div>
						<div class="row m-t-xxs">
								<div class="col-xs-6">ได้รับเงินจาก <?php echo $receipt["fullname_desc"]; ?></div>
								<div class="col-xs-4 text-right">เลขทะเบียน</div>
								<div class="col-xs-2"><?php echo $receipt["crmem_id"]; ?></div>
						</div>
						<div class="row m-t-xxs">
								<div class="col-xs-12">สังกัด <?php echo $receipt["memgrp_id"]." ".$receipt["memgrp_desc"]; ?></div>
						</div>
						
						<?php
						if($receipt["memtyp_desc"] != "สมาชิกปกติ") {
							$sql = "SELECT * FROM cmp_imp_memdetail WHERE mem_id = '{$receipt["mem_id"]}' AND memtyp_desc = 'สมาชิกปกติ'";
							$rs2 = $mysqli->query($sql);
							$row2 = $rs2->fetch_assoc();
							?>
							<div class="row m-t-xxs">
									<div class="col-xs-12">หักเงินจาก <?php echo $row2["fullname_desc"]; ?></div>
							</div>
							<?php
						}
						?>
						
						<div class="row m-t-xxs">
								<div class="col-xs-12">รายชื่อผู้เสียชีวิตดังต่อไปนี้ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?php echo $month_name." ".$year; ?></div>
						</div>
						<?php while(($row = $rs->fetch_assoc())) { ?>
						<div class="row m-t-xxs">
								<div class="col-xs-offset-1 col-xs-11"><?php echo "{$row["death_no"]}. {$row["fullname_desc"]} ({$row["memgrp_desc"]})"; ?></div>
						</div>
						<?php } ?>
						
						<div class="row m-t-xxs">
								<div class="col-xs-10"><?php echo $receipt["inv_desc"]; ?></div><div class="col-xs-2"><?php echo number_format($receipt["item_val"], 2); ?></div>
						</div>
						<?php while($receipt = $rs_receipt->fetch_assoc()) {
							$total += $receipt["item_val"]; ?>
							<div class="row m-t-xxs">
									<div class="col-xs-10"><?php echo $receipt["inv_desc"]; ?></div><div class="col-xs-2"><?php echo number_format($receipt["item_val"], 2); ?></div>
							</div>
						<?php } ?>
						
						<div class="row m-t-xxs" style="font-weight: bold;">
								<div class="col-xs-10">รวมเป็นเงิน</div><div class="col-xs-2"><?php echo number_format($total, 2); ?></div>
						</div>
						
						<div class="row m-t-lg">
								<div class="col-xs-offset-3 col-xs-3"><a class="btn btn-lg btn-block btn-default btn-back" href="?"><i class="fa fa-arrow-left"></i>&nbsp;ย้อนกลับ</a></div>
								<div class="col-xs-3"><a class="btn btn-lg btn-block btn-success btn-print" href="receipt.pdf.php?id=<?php echo $receiptdoc_id; ?>" target="_blank"><i class="fa fa-print"></i>&nbsp;พิมพ์ใบเสร็จ</a></div>
						</div>
						<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");