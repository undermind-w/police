<?php
		include "config.inc.php";
		define('FPDF_FONTPATH',"{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/font/");
		include "{$_SERVER["DOCUMENT_ROOT"]}/class/fpdf/fpdf.php";

		function U2T($text) { return @iconv("UTF-8", "TIS-620//IGNORE", trim($text)); }
		function T2U($text) { return iconv("TIS-620", "UTF-8", trim($text)); }
		function set_color_cation() { global $pdf; $pdf->SetTextColor(28, 147, 83); }
		function set_color_text() { global $pdf; $pdf->SetTextColor(130, 119, 135); }

		$crmem_id 	= isset($_GET["crmem_id"]) ? $_GET["crmem_id"] : $_COOKIE["crmem_id"];

		$page_width 		= 210;
		$page_height 		= 297;
		$line_height 		=	7;
		$margin_left 		= 5;
		$margin_right 	= 5;
		$content_width	=	$page_width - 10;
		
		class PDF extends FPDF {
				function Header() {
						/*
						$this->SetFont('Arial','B',15);
						$this->Cell( 0, 9, 'Page Heading', 1, 1, 'C' );
						$this->Ln(20);
						*/
						$page_width 		= 210;
						$line_height 		=	7;
						$margin_left 		= 5;
						$content_width	=	$page_width - 10;
						
						$curdate = ConvertToThaiDate(date("Y-m-d"), true);
						$curtime = date("H:i");
						$this->AddFont('angsa','','angsa.php');
						$this->SetFont('angsa','',15);
						$this->Image("{$_SERVER["DOCUMENT_ROOT"]}/images/receipt.wf.png", 10, 4, 20, 0, '', '', true);
						$this->SetY(5);
						$this->Cell($content_width, $line_height, U2T("สมาคมฌาปนกิจสงเคราะห์"), 0, 0, "C", false);
						$this->SetX(0);
						$this->Cell($content_width, $line_height, U2T("วันที่พิมพ์   {$curdate} เวลา {$curtime} น."), 0, 0, "R", false);
						$this->SetY(12);
						$this->Cell($content_width, $line_height, U2T("สหกรณ์ออมทรัพย์ครูเชียงใหม่ จำกัด"), 0, 0, "C", false);
						$this->SetX(0);
						$this->Cell($content_width, $line_height, U2T("หน้าที่ ".$this->PageNo()." / {nb}"), 0, 0, "R", false);
						$this->Ln();
						$this->Cell($content_width, $line_height, U2T("รายงานรายละเอียดสมาชิก"), 0, 0, "C", false);
						$this->Ln();
						$pos_y = $this->GetY() + ($line_height / 2);
						$this->SetLineWidth(0.05);
						$this->SetDrawColor(238, 238, 238);
						$this->Line($margin_left, $pos_y, $content_width + 5, $pos_y);
						$this->Ln();
				}
		}
		
		$pdf = new PDF('P', 'mm', array($page_width, $page_height));
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(true, 10);
		$pdf->AddPage();
		$pdf->AddFont('angsa'	, null, 'angsa.php');
		$pdf->AddFont('angsab', null, 'angsab.php');
		$pdf->SetFont('angsab'	, '', 14);
		//$pdf->SetDisplayMode("real", "default");
		//$pdf->SetAutoPageBreak("true", 2);
		$pdf->SetLeftMargin(5);
		$pdf->SetRightMargin(5);
		/*
		$curdate = ConvertToThaiDate(date("Y-m-d"), true);
		$curtime = date("H:i");
		
		$pdf->SetFont('angsab', null, 14);
		$pdf->SetY(10);
		$pdf->Cell($content_width, $line_height, U2T("สหกรณ์ออมทรัพย์ครูเชียงใหม่ จำกัด"), 0, 0, "C", false);
		$pdf->SetX(0);
		$pdf->Cell($content_width, $line_height, U2T("วันที่พิมพ์ {$curdate} เวลา {$curtime} น."), 0, 0, "R", false);
		$pdf->Ln();
		$pdf->Cell($content_width, $line_height, U2T("รายงานรายละเอียดสมาชิก"), 0, 0, "C", false);
		$pdf->SetX(0);
		$pdf->Cell($content_width, $line_height, U2T("หน้าที่ ..../.........."), 0, 0, "R", false);
		$pdf->Ln();
		$pos_y = $pdf->GetY() + ($line_height / 2);
		$pdf->SetLineWidth(0.05);
		$pdf->SetDrawColor(238, 238, 238);
		$pdf->Line($margin_left, $pos_y, $content_width + 5, $pos_y);
		$pdf->Ln();
		*/
		
		$sql = "SELECT tb1.*
						, TIMESTAMPDIFF(MONTH, tb1.birth_dt, CURDATE()) AS birth_dt_age
						, TIMESTAMPDIFF(MONTH, tb1.mem_dt, CURDATE()) AS mem_dt_age
						, tb2.email
					FROM cmp_imp_memdetail AS tb1
						LEFT OUTER JOIN cmp_imp_memdetail_data AS tb2 ON tb1.crmem_id = tb2.crmem_id
					WHERE tb1.crmem_id = '{$crmem_id}'";
		$rs = $mysqli->query($sql);
		$member = $rs->fetch_assoc();
		unset($sql, $rs);
		
		$addr_curr = $member["currfull_desc"];
		
		$birth_dt_age 					= array();
		$birth_dt_age["year"] 	= floor($member["birth_dt_age"] / 12);
		$birth_dt_age["month"] 	= $member["birth_dt_age"] % 12;
		$mem_dt_age 						= array();
		$mem_dt_age["year"] 		= floor($member["mem_dt_age"] / 12);
		$mem_dt_age["month"] 		= $member["mem_dt_age"] % 12;
		
		$sql = "SELECT * FROM cmp_imp_member WHERE card_person = '{$member["card_id"]}'";
		$rs = $mysqli->query($sql);
		$member_main = $rs->fetch_assoc();
		
		$pdf->SetFont('angsab', "U", 14);
		$pdf->SetX($margin_left);
		$pdf->Cell($content_width, $line_height, U2T("ประวัติสมาชิก"), 0, 0, "L", false);
		$pdf->SetFont('angsa', "", 14);
		$pdf->Ln();
		$pdf->Cell(80, $line_height, U2T("เลขที่สมาชิก   {$member["crmem_id"]}"), 0, 0, "L", false);
		$pdf->Cell(120, $line_height, U2T("ชื่อ - สกุล   {$member["fullname_desc"]}"), 0, 0, "L", false);
		$pdf->Ln();
		$pdf->Cell(80, $line_height, U2T("เลขที่สมาชิกสหกรณ์ฯ   {$member["mem_id"]}"), 0, 0, "L", false);
		$pdf->Cell(120, $line_height, U2T("ประเภทสมาชิก   {$member["memtyp_desc"]}"), 0, 0, "L", false);
		$pdf->Ln();
		$pdf->Cell(80, $line_height, U2T("เลขที่บัตรประจำตัวประชาชน   {$member["card_id"]}"), 0, 0, "L", false);
		$pdf->Cell(120, $line_height, U2T("สังกัด   {$member["memgrp_id"]} - {$member["memgrp_desc"]}"), 0, 0, "L", false);
		$pdf->Ln();
		$dt = explode(" ",$member["birth_dt"]);
		$pdf->Cell(55, $line_height, U2T("วันเดือนปีเกิด   ".(empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true))), 0, 0, "L", false);
		$dt = explode(" ",$member["mem_dt"]);
		$pdf->Cell(55, $line_height, U2T("วันที่เป็นสมาชิก   ".(empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true))), 0, 0, "L", false);
		$dt = explode(" ",$member["protect_dt"]);
		$pdf->Cell(55, $line_height, U2T("วันที่คุ้มครอง   ".(empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true))), 0, 0, "L", false);
		$pdf->Ln();
		$pdf->Cell(110, $line_height, U2T("ที่อยู่   {$addr_curr}"), 0, 0, "L", false);
		$pdf->Cell(120, $line_height, U2T("โทรศัพท์   {$member["currtel_desc"]}"), 0, 0, "L", false);
		$pdf->Ln();
		$pdf->Cell(80, $line_height, U2T("การชำระเงิน   {$member["methpay_desc"]}"), 0, 0, "L", false);
		$pdf->Cell(120, $line_height, U2T("เรียกเก็บเงินจาก   {$member["invpayist_sts"]}"), 0, 0, "L", false);
		$pdf->Ln();
		
		$pdf->Ln();
		$pdf->SetFont('angsab', "U", 14);
		$pdf->SetX($margin_left);
		$pdf->Cell($content_width, $line_height, U2T("สมาชิกสมทบ"), 0, 0, "L", false);
		$pdf->SetFont('angsa', "", 14);
		$pdf->Ln();
		
		$sql = "SELECT * FROM cmp_imp_memdetail WHERE mem_id = '{$member["mem_id"]}' AND memtyp_desc <> 'สมาชิกปกติ'";
		$rs = $mysqli->query($sql);
		$i = 0;
		while($row = $rs->fetch_assoc()) {
			$pdf->Cell(80, $line_height, U2T(++$i.". {$row["fullname_desc"]}"), 0, 0, "L", false);
			$pdf->Cell(50, $line_height, U2T("เลขที่สมาชิก   {$row["crmem_id"]}"), 0, 0, "L", false);
			$pdf->Cell(70, $line_height, U2T("ประเภทสมาชิก   {$row["memtyp_desc"]}"), 0, 0, "L", false);
			$pdf->Ln();
		}
		
		/*$pdf->Ln();
		$pdf->SetFont('angsab', "U", 14);
		$pdf->SetX($margin_left);
		$pdf->Cell($content_width, $line_height, U2T("รายการเคลื่อนไหว"), 0, 0, "L", false);
		$pdf->SetFont('angsa', "", 14);
		$pdf->Ln();
		$pdf->Cell(20, $line_height, U2T("ลำดับที่"), 0, 0, "C", false);
		$pdf->Cell(30, $line_height, U2T("วันที่"), 0, 0, "C", false);
		$pdf->Cell(30, $line_height, U2T("เลขที่ใบเสร็จ"), 0, 0, "C", false);
		$pdf->Cell(90, $line_height, U2T("รายการ"), 0, 0, "C", false);
		$pdf->Cell(30, $line_height, U2T("จำนวนเงิน"), 0, 0, "R", false);
		$pdf->Ln();
		
		$sql = "SELECT * FROM cmp_imp_memstm WHERE crmem_id = '{$_COOKIE["crmem_id"]}' ORDER BY seq_no DESC";
		$rs = $mysqli->query($sql);
		$i = 0;
		while($row = $rs->fetch_assoc()) {
			$pdf->Cell(20, $line_height, U2T("{$row["seq_no"]}"), 0, 0, "C", false);
			$dt = explode(" ",$row["opr_dt"]);
			$pdf->Cell(30, $line_height, U2T((empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true))), 0, 0, "C", false);
			$pdf->Cell(30, $line_height, U2T("{$row["refdoc_no"]}"), 0, 0, "C", false);
			$pdf->Cell(90, $line_height, U2T("{$row["memstm_desc"]}"), 0, 0, "L", false);
			$pdf->Cell(30, $line_height, U2T("{$row["item_val"]}"), 0, 0, "R", false);
			$pdf->Ln();
		}*/
		
		$pdf->Ln();
		$pdf->SetFont('angsab', "U", 14);
		$pdf->SetX($margin_left);
		$pdf->Cell($content_width, $line_height, U2T("สมาคมฌาปนกิจสงเคราะห์สมาชิกชุมนุมสหกรณ์ออมทรัพย์ครูไทย (สสอค.)"), 0, 0, "L", false);
		$pdf->SetFont('angsa', "", 14);
		$pdf->Ln();
		
		$sql = "SELECT * FROM cmp_imp_thaiftsc WHERE id = '{$member["card_id"]}'";
		$rs_thaiftsc = $mysqli->query($sql);
		$row_thaiftsc = $rs_thaiftsc->fetch_assoc();
		$pdf->Cell(80, $line_height, U2T("เลขที่ฌาปนกิจ   {$row_thaiftsc["thaiftsc_id"]}"), 0, 0, "L", false);
		$pdf->Cell(120, $line_height, U2T("ชื่อ - สกุล   {$row_thaiftsc["prename"]}{$row_thaiftsc["name"]} {$row_thaiftsc["sname"]}"), 0, 0, "L", false);
		$pdf->Ln();
		$pdf->Cell(80, $line_height, U2T("ประเภทสมาชิก   {$row_thaiftsc["thaiftsc_memtpy"]}"), 0, 0, "L", false);
		$pdf->Ln();
		$dt = explode(" ",$row_thaiftsc["thaiftsc_dtmem"]);
		$pdf->Cell(80, $line_height, U2T("วันที่สมัคร   ".(empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true))), 0, 0, "L", false);
		$dt = explode(" ",$row_thaiftsc["thaiftsc_dtprotect"]);
		$pdf->Cell(120, $line_height, U2T("วันที่เป็นสมาชิก   ".(empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true))), 0, 0, "L", false);
		$pdf->Ln();
		
		$pdf->Ln();
		$pdf->SetFont('angsab', "U", 14);
		$pdf->SetX($margin_left);
		$pdf->Cell($content_width, $line_height, U2T("สมาคมฌาปนกิจสงเคราะห์สหกรณ์สมาชิกของชุมนุมสหกรณ์ออมทรัพย์แห่งประเทศไทย (สส.ชสอ.)"), 0, 0, "L", false);
		$pdf->SetFont('angsa', "", 14);
		$pdf->Ln();
		
		$sql = "SELECT * FROM cmp_imp_fscct WHERE id = '{$member["card_id"]}'";
		$rs_thaiftsc = $mysqli->query($sql);
		$row_thaiftsc = $rs_thaiftsc->fetch_assoc();
		$pdf->Cell(80, $line_height, U2T("เลขที่ฌาปนกิจ   {$row_thaiftsc["fscct_id"]}"), 0, 0, "L", false);
		$pdf->Cell(120, $line_height, U2T("ชื่อ - สกุล   {$row_thaiftsc["prename"]}{$row_thaiftsc["name"]} {$row_thaiftsc["sname"]}"), 0, 0, "L", false);
		$pdf->Ln();
		$pdf->Cell(80, $line_height, U2T("ประเภทสมาชิก   {$row_thaiftsc["fscct_memtyp"]}"), 0, 0, "L", false);
		$pdf->Ln();
		$dt = explode(" ",$row_thaiftsc["fscct_dtmem"]);
		$pdf->Cell(80, $line_height, U2T("วันที่สมัคร   ".(empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true))), 0, 0, "L", false);
		$dt = explode(" ",$row_thaiftsc["fscct_dtprotect"]);
		$pdf->Cell(120, $line_height, U2T("วันที่เป็นสมาชิก   ".(empty($dt[0]) ? "" : ConvertToThaiDate($dt[0], true))), 0, 0, "L", false);
		$pdf->Ln();

		$pdf->Output();