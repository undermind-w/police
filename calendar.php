<?php
	include "config.inc.php";
	
	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<style>	
 
</style>
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scrip_include", $scrip_include);
	ob_start();
?>
<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	$content = "" ; 
	
	if(!empty($_GET["cid"]))
	{
		$cal_id = (int) @$_GET["cid"] ; 
		 
		$sql = "SELECT * , cmp_admin.admin_name FROM cmp_calendar LEFT JOIN cmp_admin ON cmp_calendar.admin_id = cmp_admin.admin_id  
						WHERE cmp_calendar.cal_status = 1 
 							AND cmp_calendar.cal_id = '{$cal_id}'  
							LIMIT 0 , 1  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$page = $rs->fetch_assoc();
		if(empty($page))
		{
			header('Location: index.php');
			exit() ; 
		}
		$smarty->assign("title",  @htmlspecialchars(@$page["cal_title"]) );
 		$smarty->assign("subject", $page["cal_title"]);
		$smarty->assign("breadcrumb", array("เนื้อหา" => "#" ));
		
		
		ob_start();
	?>
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row">
			<div class="col-md-12">
				<h2 class="page-title"><?php echo @htmlspecialchars(@$page["cal_title"]) ?></h2>
				<hr />
				<div class="page-detail">
				<?php echo htmlspecialchars_decode(@$page["cal_detail"] , ENT_QUOTES ) ;  ?>
				<br />
				</div>
				<div class="page-footer">
				</div>
 			</div>
			 
		</div>
	</div>
</section>
	<?php
		$content = ob_get_contents();
		ob_end_clean();
	}
	 
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");