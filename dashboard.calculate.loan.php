<?php
	include "config.inc.php";

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($_POST) exit();

	ob_start();
?>
	<meta name="description" content="<?php echo DESC; ?>" />
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	
	<script src="/scripts/jquery.number_format.js"></script>
	<script>
	Number.prototype.format = function(n, x, s, c) {
	    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
		    num = this.toFixed(Math.max(0, ~~n));
	    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	};

	$('document').ready(function() {
		$("body").on("click", ".btn-calculate", function(e){
         if($.trim($('#loan').val()) == ''){
            $('#alertLaon').modal("show");
         } else if($.trim($('#interest').val()) == '') {
            $('#alertInterest').modal("show");
         } else if($.trim($('#period').val()) == '') {
            $('#alertPeriod').modal("show");
         } else if($.trim($('#day').val()) == '') {
            $('#alertDay').modal("show");
         } else if($.trim($('#month').val()) == '') {
            $('#alertMonth').modal("show");
         }else if($.trim($('#year').val()) == '') {
            $('#alertYear').modal("show");
         } else {
            cal();
         }
    });

		function cal(){
      $.ajax({
				type: "POST"
				, url: "dashboard.include.calculate3.v2.ajax.php"
				, data: {
					"ajax" : 1
					, "do" : "cal"
					, "loan" : $("#loan").val().replace(/,/g, "")
					, "pay_period" : $("#pay_period").val()
					, "pay_type" : $("#pay_type").val()
					, "day" : $("#day").val()
          , "month" : $("#month").val()
          , "year" : $("#year").val()
          , "period_type" : $("#period_type").val()
          , "period" : $("#period").val()
					, "interest" : $("#interest").val()
					, "_time" : Math.random()
				}
				, async: true
				, success: function(msg) {
					$("#result_wrap").html(msg);
				}
			});
		}

    $("#select_interest option").filter(function() {
      return $(this).val() == $("#interest").val();
    }).attr('selected', true);

    $(this).on("change" , "#select_interest" , function() {
        $("#interest").val($(this).find("option:selected").attr("value"));
    });

    $('#loan').keyup(function(event) {
      if(event.which >= 37 && event.which <= 40) return;
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });

    $( "#period_type" )
    .change(function () {
      var str = " ";
      $( "#period_type option:selected" ).each(function() {
        if ($(this).val() == 1) {
          str += "งวด";
        } else {
          str += "บาท";
        }
      });
      $( "#type_period" ).text( str );
    })
    .change();

	});
	</script>

	 
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	//include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.subject.loan.inc.php";
	$smarty->assign("title", "คำนวณสินเชื่อ");
	$smarty->assign("subject", "คำนวณสินเชื่อ");
	// $smarty->assign("breadcrumb", array("คำนวณสินเชื่อ" => null ));
	ob_start();
	
?>
<section class="section-main content-page"  >	 
	<div class="container" >
		<div class="row m-t-md">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header subject hidden-print">
						<div class="">
							<div class="pull-left">
								<span class="fa-stack fa-2x">
									<i class="fa fa-circle fa-stack-2x" style="color: #167C3B;" ></i>
									<i class="fa fa-calculator fa-stack-1x subject" style="color: #FFF;" ></i>
								</span>
							</div>
							<div class="pull-left m-l-xxs">
								<h2 style="margin-top: 10px;" >คำนวณสินเชื่อ</h2>
							</div>
							<div class="pull-right badget"></div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="card-body">
						<form class="calculate-loan-form hidden-print">

							<div class="row">
								<span class="col-sm-4 control-label">วงเงินกู้</span>
								<div class="col-sm-8">
									<input type="text" id="loan" name="loan" class="form-control form-loan number-only inline-block"/>
									<span class="inline-block control-label m-l-xs"> บาท </span>
								</div>
							</div>

							<div class="row">
								<span class="col-sm-4 control-label">อัตราดอกเบี้ย</span>
								<div class="col-sm-8">
							<input type="nuumber" id="interest" name="interest"  class="form-control form-loan number-only inline-block" step="0.01" />
									<span class="inline-block control-label m-l-xs"> % </span>
								</div>
							</div>

							<div class="row">
								<span class="col-sm-4 control-label"> จำนวน </span>
									<div class="col-sm-8">
									<select id="period_type" name="period_type" class="form-control  inline-block" style="width:242px;">
										<option value="1"> งวดที่ต้องการผ่อน </option>
										<option value="2"> เงินที่ต้องการผ่อนต่องวด </option>
									</select>
									<input type="nuumber" id="period" name="period"  class="form-control form-loan number-only inline-block" />
									<span class="inline-block control-label m-l-xs" id="type_period" > % </span>
								</div>
							</div>

							<div class="row">
								<span class="col-sm-4 control-label">วันที่เริ่มชำระ</span>
								<div class="col-sm-8">
									<input type="text" id="day"   name="day"    class="form-control form-loan number-only inline-block" value="<?php echo empty($day) ? 1 : $day  ;?>" />
										<?php 
											$month = (int) empty($month) ? date("m") : $month ; 
											$month = $month + 1 > 12 ? 1 : $month + 1 ;  
										?>
										<select name="month" id="month" class="form-control inline-block" style="width:120px;">
										<?php $months = array(1=>"ม.ค.",2=>"ก.พ.",3=>"มี.ค.",4=>"เม.ย.",5=>"พ.ค.",6=>"มิ.ย.",7=>"ก.ค.",8=>"ส.ค.",9=>"ก.ย.",10=>"ต.ค.",11=>"พ.ย.",12=>"ธ.ค.");?>
										<?php foreach ($months as $key => $value) {
												if ($key == $month) { ?>
													<option value="<?php echo $key?>" selected><?php echo $value?></option>
												<?php } else { ?>
													<option value="<?php echo $key?>"><?php echo $value?></option>
												<?php } ?>
									   <?php } ?>
									</select>
									<?php 
										$year = (int) empty($year) ? date("Y") : $year ; 
										$year = $month == 1 ? $year + 1 : $year ; 
									?>
									<input type="text" id="year"  name="year"   class="form-control form-loan number-only inline-block" value="<?php echo $year ;?>"/>
								</div>
							</div>

							<div class="row">
								<span class="col-sm-4 control-label">ประเภทการชำระเงิน</span>
								<div class="col-sm-8">
								  <select id="pay_type" name="pay_type" class="form-control inline-block" style="width:220px;">
									<option value="1">ชำระต้นเท่ากันทุกงวด</option>
									<option value="2">ชำระยอดเท่ากันทุกงวด</option>
								  </select>
								</div>
							</div>

							<div class="row hidden-print">
								<div class="col-md-12 text-center p-v-xxl">
									<button type="button" class="btn btn-calculate btn-custom-1">คำนวณ</button>
								</div>
							</div>
						</form>
						<div id="result_wrap"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="calculate-modal-group" >
<div class="modal fade" id="alertLaon" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header subject">
			<h4 class="modal-title">
				แจ้งเตือน
			</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>

		</div>
		<div class="modal-body text-center">
			 กรุณาใส่จำนวนวงเงินกู้
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" style="margin:auto" data-dismiss="modal">ปิด</button>
		</div>
		</div>
	</div>
</div>

<div class="modal fade" id="alertInterest" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header subject">
	<h4 class="modal-title">
	  แจ้งเตือน
	</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>

  </div>
  <div class="modal-body text-center">
	 กรุณาใส่อัตราดอกเบี้ย	
  </div>
  <div class="modal-footer ">
	<button type="button" class="btn btn-primary" style="margin:auto" data-dismiss="modal">ปิด</button>
  </div>
</div>
</div>
</div>

<div class="modal fade" id="alertPeriod" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header subject">
	  <h4 class="modal-title">
		แจ้งเตือน
	  </h4>
	  	  <button type="button" class="close" data-dismiss="modal">&times;</button>

	</div>
	<div class="modal-body text-center">
		กรุณาใส่จำนวนงวด หรือ จำนวนเงิน 
	</div>
	<div class="modal-footer text-center">
	  <button type="button" class="btn btn-primary" style="margin:auto" data-dismiss="modal">ปิด</button>
	</div>
  </div>
</div>
</div>

<div class="modal fade" id="alertPeriod" role="dialog">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header subject">
		<h4 class="modal-title">
		  แจ้งเตือน
		</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>

	  </div>
	  <div class="modal-body text-center">
		กรุณาใส่จำนวนงวด หรือ จำนวนเงิน 
	  </div>
	  <div class="modal-footer text-center">
		<button type="button" class="btn btn-primary" style="margin:auto" data-dismiss="modal">ปิด</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="alertDay" role="dialog">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header subject">
		<h4 class="modal-title">
		  แจ้งเตือน
		</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>

	  </div>
	  <div class="modal-body text-center">
		กรุณาใส่ วัน ที่ต้องการคำนวณ 
	  </div>
	  <div class="modal-foote text-centerr">
		<button type="button" class="btn btn-primary" style="margin:auto" data-dismiss="modal">ปิด</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="alertMonth" role="dialog">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header subject">
		<h4 class="modal-title">
		  แจ้งเตือน
		</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>

	  </div>
	  <div class="modal-body text-center">
		 กรุณาเลือก เดือน ที่ต้องการคำนวณ 
	  </div>
	  <div class="modal-footer text-center">
		<button type="button" class="btn btn-primary" style="margin:auto" data-dismiss="modal">ปิด</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="alertYear" role="dialog">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header subject">
		<h4 class="modal-title">
		  แจ้งเตือน
		</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>

	  </div>
	  <div class="modal-body text-center">
		 กรุณาใส่ ปี ที่ต้องการคำนวณ 
	  </div>
	  <div class="modal-footer text-center">
		<button type="button" class="btn btn-primary" style="margin:auto" data-dismiss="modal">ปิด</button>
	  </div>
	</div>
  </div>
</div>
</div >
<style>
	@media print
	{    
		.hidden-print, .hidden-print *
		{
			display: none !important;
		}
		.section-header {
			display: none !important;
		} 
		.section-footer {
			display: none !important;
		}
	}
	.calculate-modal-group .modal-body{
		font-size: 18px ;
		font-family : "Kanit", sans-serif ; 
	}
	.calculate-modal-group .btn {
		font-size: 18px ;
		font-family : "Kanit", sans-serif ; 
	}
	.form-loan {
		width: 120px !important;
		text-align: center;
	}
    .modal-header-info {
        color:#fff;
        padding:9px 15px;
        border-bottom:1px solid #eee;
        background-color: #66cc00;
        -webkit-border-top-left-radius: 5px;
        -webkit-border-top-right-radius: 5px;
        -moz-border-radius-topleft: 5px;
        -moz-border-radius-topright: 5px;
         border-top-left-radius: 5px;
         border-top-right-radius: 5px;
    }
    .modal-dialog {
      margin-top: 250px;
      width: 300px;
    }
</style>
<?php
 
	//include "{$_SERVER["DOCUMENT_ROOT"]}/dashboard.include.calculate3.v2.php";
		 
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
