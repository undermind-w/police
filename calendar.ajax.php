<?php 
 
	include "config.inc.php";
	$start = $mysqli->real_escape_string($_GET["start"]);
	$end = $mysqli->real_escape_string($_GET["end"]);
	
	$sql = "SELECT * FROM cmp_calendar WHERE DATE(publicdate) >= DATE('{$start}') AND  DATE(publicdate) <= DATE('{$end}') ORDER BY publicdate DESC LIMIT 0 , 100 " ;
 	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	$data = array();
	$i = 0 ; 
	while($row = $rs->fetch_assoc()){
		$data[$i]["title"] =  "<i class=\"fa fa-eye\"></i>"  ;  
		$data[$i]["start"] =  date("Y-m-d" , strtotime($row["publicdate"]) )   ;  
		$data[$i]["url"] = "/calendar/{$row["cal_id"]}" ;
		$i ++ ; 
	}
	
	echo json_encode($data);