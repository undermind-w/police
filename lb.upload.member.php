<?php
	require "config.inc.php";
	require "dashboard.session.inc.php";
	require PATH."/class/Image.php";
	
	if($_POST["do"]=="upload") {
		setcookie("IMG".$_GET["index"], "", time() - 3600, "/");
		setcookie("IMG_W".$_GET["index"], "", time() - 3600, "/");
		setcookie("IMG_H".$_GET["index"], "", time() - 3600, "/");
		ini_set('memory_limit', '64M');
		
		if(!empty($_FILES["request_file"]["name"])) {
			$tmp = pathinfo($_FILES["request_file"]["name"]);
			$ext = $tmp['extension'];
			//if(in_array(strtolower($ext), array("jpg", "gif", "png"))) {
			if(in_array(strtolower($ext), array("jpg"))) {
				
				$ints = date('YmdGis').random_char(4);
				$prefix = "{$employee["employee_id"]}_{$ints}";
				$filename= "{$prefix}.{$ext}";
				copy($_FILES["request_file"]["tmp_name"], PATH."/uploads/tmp/{$filename}");
				list($width, $height, $type, $attr) = getimagesize(PATH."/uploads/tmp/{$filename}");
				if($width>$height) {
					$set_width = 9999;
					$set_height = 600;
				} else {
					$set_width = 600;
					$set_height = 9999;
				}
				
				if($_COOKIE["emp_id"] != "") {
					$path = glob(PATH."/uploads/tmp/*"); // get all file names
					foreach($path as $file){ // iterate files
						$info = pathinfo($file);
						if(is_file($file) && (substr($info["filename"], 0, strlen($_COOKIE["emp_id"])) == $_COOKIE["emp_id"]) && $info["filename"] != $filename) {
							unlink($file);
						}
					}
				}
				
				setcookie("is_upload".$_GET["index"], "", time() - 3600, "/");
				
				//----------------------------------------- Start
				$srcFile = $_FILES["request_file"]["tmp_name"];
				$destPath = PATH."/uploads/tmp/";
				$image = new Image();
				$image->sizelimit_x = $set_width;
				$image->sizelimit_y = $set_height;
				$image->keep_proportions = true;
				if($image->resize_image($srcFile) === true) {
					$image->save_resizedimage($destPath, $prefix);
				}
				$image->destroy_resizedimage();
				//----------------------------------------- End
				
				chmod(PATH."/uploads/tmp/{$filename}", 0777);
				
				list($width, $height, $type, $attr) = getimagesize(PATH."/uploads/tmp/{$filename}");
				setcookie("IMG".$_GET["index"], $filename, time() + (60 * 60 * 2), "/");
				setcookie("IMG_W".$_GET["index"], $width, time() + (60 * 60 * 2), "/");
				setcookie("IMG_H".$_GET["index"], $height, time() + (60 * 60 * 2), "/");
			} else {
				echo "<script type='text/javascript'>alert('ไฟล์แนบ มีชนิดไฟล์เป็น jpg เท่านั้น');</script>";
			}
		}
		//echo "<script type='text/javascript'>parent.jQuery.fancybox.close();</script>";
		echo "<script type='text/javascript'>document.location.href='?index=".$_GET["index"]."';</script>";
		exit();
	}
	
	if($_POST["do"]=="crop") {
		if(!empty($_POST["img"])) {
			$targ_w = $targ_h = 600;
			$jpeg_quality = 100;

			$src = $_SERVER["DOCUMENT_ROOT"] . "/uploads/tmp/{$_POST["img"]}";
			$ext = pathinfo($src, PATHINFO_EXTENSION);
			
			$img_r = imagecreatefromjpeg($src);
			$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

			$ints = date('YmdGis').random_char(4);
			$prefix = "m_{$ints}";
			$filename = "{$prefix}.{$ext}";
			
			$x = (int) $_POST["x"] ;
			$y = (int) $_POST["y"] ;
			$w = (int) $_POST["w"] ;
			$h = (int) $_POST["h"] ;
			
			//echo $x . " " . $y . " " . $w . " " . $h ;

			imagecopyresampled($dst_r, $img_r, 0, 0, $x , $y  , $targ_w, $targ_h, $w , $h );

			@imagejpeg($dst_r , PATH."/uploads/members/{$filename}",$jpeg_quality);
			setcookie("IMG".$_GET["index"], $filename, time() - 3600, "/");
			setcookie("IMG_W".$_GET["index"], $w, time() - 3600, "/");
			setcookie("IMG_H".$_GET["index"], $h, time() - 3600, "/");
			setcookie("is_upload".$_GET["index"], 1, time() - 3600, "/");
			@unlink($src);
			
			$sql = "UPDATE cmp_imp_member_data SET mem_image = '{$filename}' WHERE member_no = '{$_COOKIE["member_no"]}'";
			$mysqli->query($sql);
			
			echo "<script type='text/javascript'>parent.jQuery('#mem_image').attr('src', '/uploads/members/{$filename}');</script>";
		}
		echo "<script type='text/javascript'>parent.jQuery.fancybox.close();</script>";
		exit();
	}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Upload</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<link href="http://cpd.upbean.co.th/html/frontend/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="/scripts/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/scripts/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="/scripts/jquery.cookies.2.2.0.min.js"></script>
	
	<script src="/scripts/jcrop2/jquery.Jcrop.js"></script>
	<link href="/scripts/jcrop2/jquery.Jcrop.css" type="text/css" rel="stylesheet" />
<style type="text/css">
	body {
		overflow: hidden;
		padding: 15px;
		position: relative;
	}
	.jcrop-holder {
		margin: 0 auto !important;
	}
	.btn-approve {
		margin-top: 10px;
		display: inline-block;
	}
	.div-button {
		margin: 5px auto;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() { 
		function updateCoords(c) {
			$('#x').val(c.x);
			$('#img_x').val(c.x);
			
			$('#y').val(c.y);
			$('#img_y').val(c.y);
			
			$('#w').val(c.w);
			$('#h').val(c.h);
		};
		
		if($("#img").size()>0) {
			var min = $.cookies.get('IMG_W')>$.cookies.get('IMG_H')?$.cookies.get('IMG_H'):$.cookies.get('IMG_W');
			$("#min").val(min);
			$('#cropbox').Jcrop({
				aspectRatio: 1
				, onSelect: updateCoords
				, minSize: [ 300, 300 ]
				, setSelect:   [ 0, 0, min, min ]
				, bgOpacity: 0.3
				, boxWidth: 600
				, boxHeight: 600
			});
		}
		
		$("body").on("click", ".btn-approve", function() {
			$("#frmCrop").submit();
			return true;
		});
		
		$("body").on("click", ".btn-close", function() {
			parent.jQuery.fancybox.close();
			return true;
		});
		
		$("body").on("click", ".btn-submit", function() {
			$('.wait').css('display', 'block');
			$("#frm").submit();
			return true;
		});
		
	});
</script>
</head>
<body>
<div class="align-left" style="margin-bottom: 10px;">
	<span class="btn btn-warning" onclick="parent.jQuery.fancybox.close();"><i class="icon-remove-sign"></i>ปิดหน้าต่าง</span>
</div>
<div style="text-align: center;">
	<form id="frm" name="frm" action="" method="post" enctype="multipart/form-data">
		<div style="margin-top: 5px;">
			<input type="file" id="request_file" name="request_file" style="margin: 3px auto;" />
			<input type="hidden" name="do" value="upload" />
			<span class="btn-submit btn btn-info"><i class="icon-upload-alt"></i>Upload</span>
			<span class="btn-close btn btn-default"><i class="icon-remove"></i>ยกเลิก</span>
		</div>
		<div class="align-center" style="margin: 3px auto;"><span style="font-weight: bold;">* <span style="text-decoration: underline;">หมายเหตุ</span></span> รูปที่อัพโหลดเป็นไฟล์นามสกุล  jpg เท่านั้นค่ะ</div>
		<div class="wait" style="display: none;"><img src="/images/wait.gif" /></div>
	</form>
	<?php if(!empty($_COOKIE["IMG".$_GET["index"]])) { ?>
		<form id="frmCrop" name="frmCrop" action="" method="post" enctype="multipart/form-data">
			<div class="div-button"><span class="btn-approve btn btn-success"><i class="icon-ok"></i>ตกลง</span></div>
			<img src="/uploads/tmp/<?php echo $_COOKIE["IMG".$_GET["index"]]; ?>" id="cropbox" width="<?php echo $_COOKIE["IMG_W".$_GET["index"]]; ?>" height="<?php echo $_COOKIE["IMG_H".$_GET["index"]]; ?>" />
			<input type="hidden" id="img" name="img" value="<?php echo $_COOKIE["IMG".$_GET["index"]]; ?>" />
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
			<input type="hidden" id="min" name="min" />
			<input type="hidden" name="do" value="crop" />
			<div style="clear: both;"></div>
		</form>
	<?php } ?>
</div>
</body>
</html>