<?php
    include "config.inc.php";
    
    ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<style> 
    /**/
/* timeline */
.timeline h4, .timeline h5, .timeline h6 {
    margin-top: 0px;
    margin-bottom: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
}
.timeline {
    list-style: none;
    padding: 0px 0 0px;
    position: relative;
}
.timeline:before {
    top: 0;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 3px;
    background-color: #CCCCCC;
    left: 98px;
    margin-left: -1.5px;
}
.timeline > li {
    position: relative;
}
.timeline > li:before,
.timeline > li:after {
    content: " ";
    display: table;
}
.timeline > li:after {
    clear: both;
}
.timeline > li:before,
.timeline > li:after {
    content: " ";
    display: table;
}
.timeline > li:after {
    clear: both;
}
.timeline > li > .timeline-panel {
    width: calc( 100% - 113px );
    float: right;
    padding: 18px 0px 0px 0px;
    position: relative;
}
.timeline > li > .timeline-panel:before {
    position: absolute;
    top: 26px;
    left: -15px;
    display: inline-block;
}
.timeline > li > .timeline-panel:after {
    position: absolute;
    top: 27px;
    left: -14px;
    display: inline-block;
}
.timeline > li .timeline-time {
    position: absolute;
    top: 18px;
    color: #9e9e9e;
}
.timeline > li > .timeline-badge {
    color: #ccc;
    width: 25px;
    height: 25px;
    line-height: 25px;
    font-size: 1.4em;
    text-align: center;
    position: absolute;
    top: 16px;
    left: 85px;

    background-color: #FFF;
    z-index: 100;
    border-top-right-radius: 50%;
    border-top-left-radius: 50%;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
}
.timeline > li.timeline-inverted > .timeline-panel {
    float: left;
}
.timeline > li.timeline-inverted > .timeline-panel:before {
    border-left-width: 0;
    border-right-width: 15px;
    right: -15px;
    right: auto;
}
.timeline > li.timeline-inverted > .timeline-panel:after {
    border-left-width: 0;
    border-right-width: 14px;
    left: -14px;
    right: auto;
}
.timeline-title {
    margin-top: 0;
    /* color: inherit; */
}
.timeline-title > a { color: #222; }
.timeline-title > a:hover { color: #ccc; }
.timeline-body > p,
.timeline-body > ul {
    margin-bottom: 0;
}
.timeline-body > p + p {
    margin-top: 5px;
}
/* timeline */
</style>
<?php
    $headtag = ob_get_contents();
    ob_end_clean();
    $smarty->assign("headtag", $headtag);
    ob_start();
?>
<?php
    $scrip_include = ob_get_contents();
    ob_end_clean();
    $smarty->assign("scrip_include", $scrip_include);
    ob_start();
?>
<?php
    $jquery = ob_get_contents();
    ob_end_clean();
    $smarty->assign("jquery", $jquery);
    $content = "" ; 
    
    if(!empty($_GET["cid"]))
    {
        $cal_id = (int) @$_GET["cid"] ;
        $sql = "SELECT cmp_calendar.* , cmp_admin.admin_name FROM cmp_calendar
                LEFT JOIN cmp_admin ON cmp_calendar.admin_id= cmp_admin.admin_id
                WHERE cmp_calendar.cal_status = 1 AND cmp_calendar.cal_id = '{$cal_id}'" ;
        $rs = $mysqli->query($sql);
        echo $mysqli->error;
        $row = $rs->fetch_assoc();
        if(empty($row))
        {
            header('Location: index.php');
            exit() ;
        }

        $smarty->assign("title",  @htmlspecialchars(@$row["cal_title"]) );
        $smarty->assign("subject", $row["cal_title"]);
        $smarty->assign("breadcrumb", array("เนื้อหา" => "#" ));
        
        
        ob_start();
    ?>
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title"><?php echo @htmlspecialchars(@$row["cal_title"]) ?></h2>
                <hr />
                <div class="detail">
                    <?php echo htmlspecialchars_decode(empty($row["cal_detail"]) ? "" : $row["cal_detail"] , ENT_QUOTES ) ;  ?>
                <br />
                </div>
                <div class="page-footer">
                </div>
            </div>
             
        </div>
    <?php
        $content = ob_get_contents();
        ob_end_clean();
    }else{

        $smarty->assign("title",  'ปฏิทินกิจกรรม');
        $smarty->assign("subject", 'ปฏิทินกิจกรรม');
        $smarty->assign("breadcrumb", array("ปฏิทินกิจกรรม" => "#" ));


        ob_start();
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="detail">
                    <!--start TimeLine-->
                    <div class="right-panel" >
                        <ul class="timeline">
                            <?php
                            $sql = "SELECT * FROM cmp_calendar WHERE cal_status = 1 ORDER BY publicdate ASC ";
                            $rs = $mysqli->query($sql);
                            echo $mysqli->error;

                            while($row = $rs->fetch_assoc()) {?>
                                                            <li ><!--Time Line Element -->
                                    <div class="timeline-badge"><span class="fa fa-clock-o"></span></div>
                                    <div class="timeline-time f-s-13">
                                        <?php echo mysqldate2thaidate($row["publicdate"] , "short" , false ) ?>   </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <div class="timeline-title f-s-12 f-green">
                                                 <h3><a href="/calendarTimeline.php?cid=<?php echo @$row["cal_id"] ?>"><?php echo $row["cal_title"];?></a>
                                                </a></h3>
                                                 <p class="steps-description" style="word-wrap: break-word;"><?php echo html2text(htmlspecialchars_decode(empty($row["cal_detail"]) ? "" : $row["cal_detail"], ENT_QUOTES),100) ;  ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                    <!--end TimeLine-->
                </div>
                <div class="page-footer">
                </div>
            </div>

        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
    }
     
    $smarty->assign("content", $content);
    $smarty->display(THEME.".tpl");