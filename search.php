<?php
	include "config.inc.php";
	$fullurl = "http://{$_SERVER["SERVER_NAME"]}{$_SERVER["REQUEST_URI"]}";

	if(@$_POST["do"] == "get-social-count") {
		echo @getFacebooks(@$_POST["url"]) + @getTweets(@$_POST["url"]) + @getPlus1(@$_POST["url"]) ;
		exit();
	}

	ob_start();
?>

<meta name="description" content="<?php echo @DESC; ?>" />
<meta name="keywords" content="<?php echo @KEYWORD; ?>" />
<meta name="author" content="<?php echo @AUTHOR; ?>" />
	<style>
		.img-responsive{margin:auto}
		.slide2-pager {
			text-align: center;
			font-size: 40px;
			color: #CCC;
			line-height: 20px;
		}


		.slide2-pager span { cursor: pointer; }
		.slide2-pager .cycle-pager-active{
			color : #43b7f6 ;
		}
		.slide-full{background-color: #FFF !important;}
		.owl-wrapper-outer{}
		/* .owl-controls{    display: none!important;} */
        .gsc-wrapper .gsc-adBlock {
            display: none;
        }
	</style>
	<link href="/scripts/owl-carousel/owl.carousel.css"  rel="stylesheet">
	<link href="/scripts/owl-carousel/owl.theme.css"  rel="stylesheet">
	<link href="/scripts/owl-carousel/owl.transitions.css"  rel="stylesheet" >
    <link href="/scripts/fullcalendar/fullcalendar.min.css"  rel="stylesheet" >
    <style>
        .fc-basic-view .fc-body .fc-row {    min-height: 35px;}
        .fc-event, .fc-event-dot {text-align:center ;  background-color : #7a52b5 }
    </style>
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
<script src="/scripts/jquery.cycle2.min.js"></script>
<script src="/scripts/owl-carousel/owl.carousel.js"></script>
<script src="/scripts/fullcalendar/lib/moment.min.js"></script>
<script src="/scripts/fullcalendar/fullcalendar.min.js"></script>
<script src="/scripts/fullcalendar/locale/th-shot.js"></script>
<script >
	$(function(){
		$("#owl-slide").owlCarousel({
			  navigation : false , // Show next and prev buttons
			  slideSpeed : 300,
			  paginationSpeed : 400,
			  singleItem:true ,
			  autoPlay: 7500,

			  // "singleItem:true" is a shortcut for:
			  // items : 1,
			  // itemsDesktop : false,
			  // itemsDesktopSmall : false,
			  // itemsTablet: false,
			  // itemsMobile : false

		  });

		$(".item-footer .shard").hover(function() {
			$(this).closest(".item-footer").find(".footer1").hide() ;
			$(this).closest(".item-footer").find(".addthis-show").show() ;
		}, function(){


		} ) ;
		$(".item-footer .addthis-show").hover(function() {

		}, function(){
			$(this).closest(".item-footer").find(".footer1").show () ;
			$(this).closest(".item-footer").find(".addthis-show").hide() ;

		} )

		if($(".count-social").length) {
				$(".count-social").each(function() {
					var obj = $(this) ;
					obj.html("<i class='fa fa-refresh fa-spin'></i>");
					$.ajax({
						type: "POST"
						, url: "?"
						, data: {
							"do" : "get-social-count"
							, "url" : encodeURIComponent(obj.data("url"))
							, "_time" : Math.random()
						}
						, async: true
						, success: function(data) {
							obj.html(data);

 						}
					});
				});
			}


	});
</script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	$smarty->assign("title", "สหกรณ์ออมทรัพย์โรงพยาบาลตำรวจ จำกัด");

	ob_start();
?>

<?php
	$slide = ob_get_contents();
	ob_end_clean();
	$smarty->assign("slide", $slide);


	ob_start();

?>

	<style>
		.news-tab .nav-tabs li a{ font-family: 'thaisans_neueregular'; padding : 10px 20px ;font-size : 25px; border: 0;color:#333 ;  background-image: url('/images/line.png'); background-repeat: no-repeat; background-position: right;}
 		.news-tab .nav-tabs li:first-child a{padding-left: 0 ;}
		.news-tab .nav-tabs li:last-child a{background-image: none ;}
		.news-tab .nav-tabs li.active a{color : #5ac7f8 ; border: 0; border-bottom : 1px solid #ddd }

        .gsc-search-box{
            display:none !important;
        }
	</style>

    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <div id="show_search"></div>
    <script>
//        var myCallback = function() {
//            if (document.readyState == 'complete') {
//                // Document is ready when CSE element is initialized.
//                // Render an element with both search box and search results in div with id 'test'.
//                google.search.cse.element.render(
//                    {
//                        div: "show_search",
//                        tag: 'search'
//                    });
//            } else {
//                // Document is not ready yet, when CSE element is initialized.
//                google.setOnLoadCallback(function() {
//                    // Render an element with both search box and search results in div with id 'test'.
//                    google.search.cse.element.render(
//                        {
//                            div: "show_search",
//                            tag: 'search'
//                        });
//                }, true);
//            }
//        };

        // Insert it before the CSE code snippet so that cse.js can take the script
        // parameters, like parsetags, callbacks.
//        window.__gcse = {
//            parsetags: 'explicit',
//            callback: myCallback
//        };

        /*(function() {
            var cx = '005453680162568750241:g1bedayj7j0';
            var gcse = document.createElement('script');
            gcse.type = 'text/javascript';
            gcse.async = true;
            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gcse, s);
        })();
        */
		/*
        $(document).ready(function() {
            $("#test").click(function () {
                var key_search = '<?php echo $_GET['p'];?>';
                $("#gsc-i-id1").val(key_search);
            })
        });
		*/
    </script>

<!-- 
<script>
  (function() {
    var cx = '016290203170195860349:zkxknbqirh1';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
-->

<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row" >
			<div class="col-md-12">
				<h2 class="page-title">ค้นหา</h2>
				<hr />
				<div class="gcse-searchresults-only"></div>   


			</div>
		</div>
	</div>
</section>
<script async src="https://cse.google.com/cse.js?cx=016290203170195860349:zkxknbqirh1"></script>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
