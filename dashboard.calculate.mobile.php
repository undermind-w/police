<?php
include "config.inc.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link rel="apple-touch-icon" sizes="57x57" href="/html/images/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/html/images/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/html/images/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/html/images/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/html/images/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/html/images/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/html/images/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/html/images/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/html/images/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/html/images/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/html/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/html/images/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/html/images/favicon/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/html/images/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
       <!-- <meta content="width=device-width, initial-scale=1" name="viewport"/> -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta charset="UTF-8">
        <meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />

        <link href="/html/frontend/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/html/frontend/font/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="/html/frontend/font/font-thaisansneue/thaisansneue.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="/scripts/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
		
		<link href="/html/frontend/css/margin.css" rel="stylesheet" type="text/css"/>
        <link href="/html/frontend/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="/html/frontend/css/custom-cpd.css" rel="stylesheet" type="text/css"/>
				

        <script src="/scripts/jquery-1.10.2.min.js"></script>
        <script src="/scripts/jquery-migrate-1.2.1.js"></script>
        <script src="/html/frontend/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/scripts/jquery.cookies.2.2.0.min.js"></script>
		<script type="text/javascript" src="/scripts/fancybox/jquery.fancybox.js?v=2.1.5"></script>
		<script type="text/javascript" src="/scripts/utility.js"></script>
		
		<script>
		
			$(function(){
				$(".fancybox-contact").fancybox({
					maxWidth	: 800 ,
					maxHeight	: 555 ,
					fitToView	: false,
 					autoSize	: false,
					closeClick	: false,
					openEffect	: 'none',
					closeEffect	: 'none'
				});
			});
		
		</script>
		<style>
			
			.fancybox-skin{ background:transparent!important; padding:0 !important; }
			
		</style>
			<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
	<script>
		$('document').ready(function(){
			$("body").on("keydown", ".number-only", function (e) {
	            var charCode = (e.charCode) ? e.charCode : ((e.which) ? e.which : e.keyCode);
	            if($.inArray(charCode, [110, 190]) != -1 && $(this).val().indexOf(".") != -1) return false;
	            if( $.inArray(charCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||  // Allow: backspace, delete, tab, escape, enter and .
	                (charCode == 65 && e.ctrlKey === true) ||		             // Allow: Ctrl+A
	                (charCode == 67 && e.ctrlKey === true) ||		             // Allow: Ctrl+C
	                (charCode == 88 && e.ctrlKey === true) ||		             // Allow: Ctrl+X
	                (charCode == 86 && e.ctrlKey === true) ||		             // Allow: Ctrl+V
	                (charCode == 82 && e.ctrlKey === true) ||		             // Allow: Ctrl+R
	                (charCode == 116 && e.ctrlKey === true) ||	                 // Allow: Ctrl+F5
	                (charCode >= 35 && charCode <= 39) 					         // Allow: home, end, left, right
	            ){ return; }

	            // Ensure that it is a number and stop the keypress
	            if((e.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)){ e.preventDefault(); }
	        });
		});

		function returnInt(num){
			return (isNaN(num) ? 0 : (isFinite(num) ? num : 0));
		}
	</script>
	<script src="/scripts/inputmask/jquery.inputmask.bundle.min.js"></script>

		
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
		
		
    </head>
    <body>
	 
			
			
						
						<div class="content-inner container">
					<div class="row m-t-md">
			<div class="col-md-12">
					<span>ประเภทสัญญา</span>&nbsp;
					<div class="dropdown inline-block">
						<?php
							$index = (isset($_GET['type'])) ? $mysqli->real_escape_string($_GET['type']) : 1;
							$types = array(1 => 'เงินกู้สามัญ','เงินกู้พิเศษ');
						?>
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    <?php echo $types[$index]; ?>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    	<?php foreach($types as $i => $type){ ?>
								<li><a href="?member_no=<?php echo $_GET["member_no"]; ?>&type=<?php echo $i; ?>"><?php echo $type; ?></a></li>
							<?php } ?>
						</ul>
					</div>
					<br><br>
					<?php
						$member_no = $_GET["member_no"];
						$type = isset($_GET['type']) ? $_GET['type'] : '';
						switch ($type) {
							case '2': include "dashboard.include.calculate2.php"; break; // เงินกู้พิเศษ
							default:  include "dashboard.include.calculate.php";  break; // เงินกู้สามัญ
						}
					?>
			</div>
	</div>

			</div>
			 
    </body>
</html>