<?php
    // เงินกู้สามัญ
    $sql = "SELECT *, TIMESTAMPDIFF(YEAR, birthdtm, CURDATE()) AS age, TIMESTAMPDIFF(MONTH, birthdtm, CURDATE()) AS age_m, TIMESTAMPDIFF(MONTH, memdtm, CURDATE()) AS memdtm_age
            FROM cmp_imp_member
            WHERE member_no = '{$member_no}'";
    $rs = $mysqli->query($sql);
    $member = $rs->fetch_assoc();
	
    $sql = "SELECT *
            FROM cmp_imp_share_master
            WHERE member_no = '{$member_no}'";
    $rs = $mysqli->query($sql);
    $share = $rs->fetch_assoc();
    $share["sharestkval"] = $share["sharestkval"];
	
	$sql = "SELECT *
            FROM cmp_imp_loan_master
            WHERE member_no = '{$member_no}' AND loansts != 'จบ'";
    $rs = $mysqli->query($sql);
	$loan_month_pay = 0;
    while($loan = $rs->fetch_assoc()) {
		if($loan["period_paypercent"] == 0) {
			$loan_month_pay += $loan["period_payment"];
		}
	}
	
	$loan_types = array(
		array("name" => "เงินกู้สามัญทั่วไป", "interest_percent" => 5.5, "loan_limit" => 3000000, "period_limit" => 120, "share_percent" => 25, "salary_percent" => 60),
		array("name" => "เงินกู้สามัญเพื่อการศึกษา", "interest_percent" => 4.25, "loan_limit" => 30000, "period_limit" => 120, "share_percent" => 25, "salary_percent" => 60),
		array("name" => "เงินกู้สามัญเพื่อชำระเบี้ยประกัน", "interest_percent" => 5.5, "loan_limit" => 1000000, "period_limit" => 120, "salary_percent" => 60),
		array("name" => "เงินกู้สามัญเพื่อการอันจำเป็น", "interest_percent" => 5.5, "loan_limit" => 1000000, "period_limit" => 120, "share_percent" => 25, "salary_percent" => 60)
	);
?>
<div class="card"> 
	<div class="card-header">
		<div class="">
			<div class="pull-left">
				<span class="fa-stack fa-2x">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-calculator fa-stack-1x text-material-light-blue-300"></i>
				</span>
			</div>
			<div class="pull-left m-l-xxs">
				<h3>คำนวณสินเชื่อ</h3>
			</div>
			<div class="pull-right badget"></div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="card-body">
		<form class="">
			<div class="row">
				<span class="col-sm-6 control-label">ประเภท</span>
				<div class="col-sm-6">
                    <select id="loan_type" class="form-control" style="width: auto;">
						<?php foreach($loan_types as $key => $value) { ?>
							<option value="<?php echo $key; ?>"><?php echo $value["name"]; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="row">
				<span class="col-sm-6 control-label">อายุ</span>
				<div class="col-sm-6">
					<input type="text" class="form-control form-loan number-only inline-block age" value="<?php echo $member["age"]; ?>" />
					<span class="inline-block control-label m-l-xs">ปี</span>
				</div>
			</div>
			<div class="row">
				<span class="col-sm-6 control-label">เงินเดือน</span>
				<div class="col-sm-6">
					<input type="text" class="form-control form-loan number-only inline-block salary_amount" value="<?php echo (double)$member["salary_amount"]; ?>" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'rightAlign': false" />
					<span class="inline-block control-label m-l-xs">บาท</span>
				</div>
			</div>
			<div id="salary_times_wrap" class="row">
				<span class="col-sm-6 control-label"><span id="salary_times"></span> เท่าของเงินเดือน</span>
				<div class="col-sm-6">
					<span class="inline-block control-label salary_times_loan"></span> บาท
				</div>
			</div>
			<div id="sharestkval_wrap" class="row">
				<span class="col-sm-6 control-label">หุ้นสะสม</span>
				<div class="col-sm-6">
					<span class="inline-block control-label sharestkval"><?php echo number_format($share["sharestkval"], 2); ?></span> บาท
				</div>
			</div>
			<div id="share_times_wrap" class="row">
				<span class="col-sm-6 control-label"><span id="share_times"></span> เท่าของหุ้นสะสม</span>
				<div class="col-sm-6">
					<span class="inline-block control-label share_times_loan"></span> บาท
				</div>
			</div>
			<div id="share_percent_wrap" class="row">
				<span class="col-sm-6 control-label"><span id="share_percent"></span>% ของหุ้นสะสม</span>
				<div class="col-sm-6">
					<span class="inline-block control-label sharestkval-percent"></span> บาท
				</div>
			</div>
			<div class="row">
				<span class="col-sm-6 control-label">วงเงินกู้</span>
				<div class="col-sm-6">
                    <input type="text" class="form-control form-loan number-only inline-block num-loan" value="" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'rightAlign': false" />
					<span class="inline-block control-label m-l-xs">บาท</span>
				</div>
			</div>
			<div class="row ">
				<span class="col-sm-6 control-label">ส่งชำระ</span>
				<div class="col-sm-6">
					<input type="text" class="form-control form-loan number-only inline-block num-period" value="120" maxlength="3" />
					<span class="inline-block control-label m-l-xs">งวด</span>
					<p class="num-year"></p>
				</div>
			</div>
			<div class="row">
				<span class="col-sm-6 control-label">ประเภทการชำระเงิน</span>
				<div class="col-sm-6">
                    <select id="loan_cal_type" class="form-control" style="width: auto;">
						<option value="1">แบบสหกรณ์</option>
						<option value="2">แบบธนาคาร</option>
					</select>
				</div>
			</div>
            <div class="row">
				<span class="col-sm-6 control-label">เงินต้นเฉลี่ยต่อเดือน</span>
				<div class="col-sm-6">
					<span class="inline-block control-label pay-permonth"></span> บาท
				</div>
			</div>
			<div class="row">
				<span class="col-sm-6 control-label">ดอกเบี้ยเฉลี่ยต่อเดือน</span>
				<div class="col-sm-6">
					<span class="inline-block control-label interest"></span> บาท
				</div>
			</div>
			<h2 class="text-center remain-status"></h2>
		</form>
		<h1 class="text-center">จำนวนเงินที่ต้องผ่อนชำระเฉลี่ย<span class="p-h-lg calculate-result"></span>บาทต่อเดือน</h1>
		<h3 class="text-center loan-limit-bottom alert-danger"></h3>
		
		<div class="text-center">
			*<u>หมายเหตุ</u> เป็นการคำนวณเบื้องต้น หากต้องการยื่นกู้กรุณาติดต่อเจ้าหน้าที่เพื่อคำนวณข้อมูลที่ถูกต้องอีกครั้ง
		</div>
	</div>
</div>
<style>
	.form-loan {
		width: 120px !important;
		text-align: center;
	}
</style>

<script src="/scripts/jquery.number_format.js"></script>
<script>
	
	Number.prototype.format = function(n, x, s, c) {
	    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
		    num = this.toFixed(Math.max(0, ~~n));
	    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	};
	
	var loan_types = <?php echo json_encode($loan_types); ?>;
	var loan_limit = 0;
	var interest_percent = 0;
	var period_limit = 0;
	var share_times = 0;
	var share_percent = 0;
	var salary_times = 0
	var salary_percent = 0;
	var loan_month_pay = <?php echo $loan_month_pay; ?>;
	
	window.onload = function() {

		$('document').ready(function() {
			$(":input").inputmask();
			
			$("#loan_type").change(function() {
				loan_limit = loan_types[$(this).val()].loan_limit;
				interest_percent = loan_types[$(this).val()].interest_percent;
				period_limit = loan_types[$(this).val()].period_limit;
				share_times = typeof loan_types[$(this).val()].share_times == "undefined" ? 0 : loan_types[$(this).val()].share_times;
				share_percent = typeof loan_types[$(this).val()].share_percent == "undefined" ? 0 : loan_types[$(this).val()].share_percent;
				salary_times = typeof loan_types[$(this).val()].salary_times == "undefined" ? 0 : loan_types[$(this).val()].salary_times;
				salary_percent = typeof loan_types[$(this).val()].salary_percent == "undefined" ? 0 : loan_types[$(this).val()].salary_percent;
				var sharestkval = isNaN(parseFloat($(".sharestkval").text().replace(/,/g, ""))) ? 0 : parseFloat($(".sharestkval").text().replace(/,/g, ""));
				$(".num-loan").val(loan_limit);
				
				$("#salary_times_wrap").addClass("hide");
				$("#share_times_wrap").addClass("hide");
				$("#share_percent_wrap").addClass("hide");
				if($(this).val() == 2) {
					$("#sharestkval_wrap").addClass("hide");
				}
				else {
					$("#sharestkval_wrap").addClass("hide");
					//var num_loan = sharestkval * 100 / share_percent;
					//$(".num-loan").val(num_loan > loan_limit ? loan_limit : num_loan);
				}
				
				cal();
			});
			$("#loan_type").trigger("change");
			
			$("body").on("keyup", ".number-only", function(e){ cal(); });
			$("body").on("click", ".btn-calculate", function(e){ cal(); });
			$("#loan_cal_type").change(function() { cal(); });

			function cal(){
				var age    = isNaN(parseFloat($(".age").val().replace(/,/g, ""))) ? 0 : parseFloat($(".age").val().replace(/,/g, ""));
				var salary_amount    = isNaN(parseFloat($(".salary_amount").val().replace(/,/g, ""))) ? 0 : parseFloat($(".salary_amount").val().replace(/,/g, ""));
				
				var loan             = isNaN(parseFloat($(".num-loan").val().replace(/,/g, ""))) ? 0 : parseFloat($(".num-loan").val().replace(/,/g, ""));
				var sharestkval      = isNaN(parseFloat($(".sharestkval").text().replace(/,/g, ""))) ? 0 : parseFloat($(".sharestkval").text().replace(/,/g, ""));
				var period             = $(".num-period").val();
				var year           = period / 12;
				
				var salary_times_loan = salary_amount * salary_times;
				var share_times_loan = sharestkval * share_times;
				var loan_percent = loan * share_percent / 100;
				
				var interest_percent_m = 0;
				var result = 0;
				var interest = 0;
				var loan_period = 0;
				
				var cal_type = $("#loan_cal_type").val();
				if(cal_type == 1) {
					loan_period = loan / period;
					var day_period = 31;
					var p = 0;
					var loan_int = 0;
					var total_loan_int = 0;
					var loan_remain = loan;
					while(++p <= period) {
						loan_int = loan_remain * (interest_percent / (365 / day_period)) / 100;
						loan_pay = loan_period + loan_int;
						loan_remain -= loan_period;
						total_loan_int += loan_int;
					}
					interest = total_loan_int / period;
					result = loan_period + interest;
				}
				else if(cal_type == 2) {
					interest_percent_m = interest_percent / 1200;
					result = loan * interest_percent_m / (1 - (Math.pow(1 / (1 + interest_percent_m), period)));
					interest = ((result * period) - loan) / period;
					loan_period = result - interest;
				}
				
				var loan_month_pay2 = loan_month_pay + result;
				var salary_month_limit = salary_amount * salary_percent / 100;
				
				$('.num-year').text('( ชำระ ' + Math.round(year * 10) / 10 + ' ปี )');
				$("#salary_times").html(salary_times);
				$(".salary_times_loan").text(returnInt(salary_times_loan).format(2));
				$("#share_times").html(share_times);
				$(".share_times_loan").text(returnInt(share_times_loan).format(2));
				$("#share_percent").html(share_percent);
				$(".sharestkval-percent").text(returnInt(loan_percent).format(2));
				$('.calculate-result').text(returnInt(result).format(2));
				$('.interest').text(returnInt(interest).format(2));
				$('.pay-permonth').text(returnInt(loan_period).format(2));

				var obj = $('.loan-limit-bottom');
				obj.empty();
				if(period > period_limit && period_limit != 0){ obj.append('<p>จำนวนงวดส่งชำระเกิน '+period_limit+' งวด</p>'); }
				//if(sharestkval < loan_percent){ obj.append('<p>จำนวนหุ้นสะสม น้อยกว่า '+share_percent+'% ของวงเงินกู้</p>'); }
				if(loan_month_pay2 > salary_month_limit){ obj.append('<p>ยอดผ่อนชำระต่อเดือนรวมทุกสัญญาเกิน '+salary_percent+'% ของเงินเดือน</p>'); }
				//if(loan_limit == 0){ obj.append('<p>ไม่สามารถกู้ได้ เนื่องจากไม่ได้ดำรงตำแหน่งข้าราชการ ลูกจ้างประจำ หรือเจ้าหน้าที่สหกรณ์</p>'); }
				if(loan > loan_limit){ obj.append('<p>ไม่สามารถกู้ได้ เนื่องจากวงเงินเกินสิทธิกู้ สูงสุด '+returnInt(loan_limit).format(2)+' บาท</p>'); }
			}
		});
	}
</script>
