<?php
require "config.inc.php";
require "session.inc.php";

$active_menu = 4;
require "menu.inc.php";

header("Content-Type:text/html;charset=utf-8");

if( $_GET["do"] == "delete" ){
 	
	$member_id =  @$mysqli->real_escape_string(@$_GET["id"]) ;  
  	
	$sql = "DELETE FROM cmp_sapp_member WHERE member_id = '{$member_id}' LIMIT 1 " ;
	$mysqli->query($sql);
	echo $mysqli->error ;	
	
 	echo "<script> alert('ลบข้อมูลแล้ว !');window.location.href = \"?\"</script>" ;
	exit();
}

ob_start(); ?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
 <?php
$header = ob_get_contents();
ob_end_clean();
$smarty->assign("header", $header);

ob_start(); ?>
<?php
$include_css[] = ob_get_contents();
ob_end_clean();
$smarty->assign("include_css", $include_css);

$smarty->assign("title", "ข้อมูลการสมัคร");
$smarty->assign("breadcrumb", ["ข้อมูลการสมัคร" => ""]);

$startdate = empty($_GET["startdate"]) ? date("Y-m-1") : $_GET["startdate"] ;
$stopdate = empty($_GET["stopdate"]) ? date("Y-m-t") : $_GET["stopdate"] ;

ob_start(); ?>
	<div style="clear: both;margin-bottom:15px"></div>
	<div class="row" >
		<div class="col-md-6" >
			<form class="form-inline" action="?" method="get" style="margin-bottom:10px;"  >
				<div class="form-group">
					<label for="month">ค้นหา</label>
					<div>
						<input class="form-control" type="text" name="search" id="search" placeholder="รหัสประจำตัว หรือ ชื่อ-สกุล" value="<?php echo htmlspecialchars($_GET["search"]) ?>"   /> 
					</div>
				</div>
				<div class="form-group">
					<label for="month">เริ่มวันที่</label>
					<div class="input-with-icon">
						<input class="form-control" type="text" name="startdate" id="startdate" data-provide="datepicker" value="<?php echo htmlspecialchars($startdate) ?>" data-date-format="yyyy-mm-dd" >
						<span class="icon icon-calendar input-icon"></span>
					</div>
 				</div>
				<div class="form-group">
					<label for="year">ถีงวันที่</label>
					<div class="input-with-icon">
						<input class="form-control" type="text" name="stopdate" id="stopdate" data-provide="datepicker" value="<?php echo htmlspecialchars($stopdate) ?>" data-date-format="yyyy-mm-dd"  >
						<span class="icon icon-calendar input-icon"></span>
					</div>
				</div>
				<button type="submit" class="btn btn-primary" style="margin-top: 20px;"  > ดูข้อมูล </button>
			</form> 
		</div>
		<div class="col-md-6 text-right" >
			<a href="register.excel?do=export2excles&startdate=<?php echo htmlspecialchars($_GET["startdate"]) ?>&stopdate=<?php echo htmlspecialchars($_GET["stopdate"]) ?>&search=<?php echo htmlspecialchars($_GET["search"]) ?>" class="btn btn-primary"> Export To Excel </a>
		</div>
	</div>
	
	<div class="table-responsive">
		<table class="table table-hover ub_table1" id="table-data">
			<thead>
				<tr>
					<th class="text-center" width="75"  >ลำดับ</th>
					<th class="text-center"  width="200" >วันที่สมัคร</th>
					<th class="text-center" width="100"  >รหัสสมาชิก</th>
					<th class="text-center" >ชื่อสมาชิก</th>
 					<th class="text-center" width="200" >สมัครด้วย</th>
					<th class="text-center" width="200" >จัดการ</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if(!empty($_GET["search"])){
						$search = $mysqli->real_escape_string($_GET["search"]);
						$sql_search = " AND ( `cmp_sapp_member`.`name` like '%{$search}%' OR `cmp_sapp_member`.`member_id` like '%{$search}%' )" ;
					}
						
					if(!empty($_GET["startdate"])){
						
						$startdate = $mysqli->real_escape_string($_GET["startdate"]);
						$stopdate = $mysqli->real_escape_string($_GET["stopdate"]);
						
						$sql_where_date = " AND ( DATE(`cmp_sapp_member`.`createtime`) >= DATE('{$startdate}') AND  DATE(`cmp_sapp_member`.`createtime`) <= DATE('{$stopdate}') ) " ;
					}
					$sql = "SELECT COUNT(member_id) _c FROM `cmp_sapp_member` WHERE is_password = 1 {$sql_where_date} {$sql_search}  ORDER BY `cmp_sapp_member`.`createtime` DESC  ";
					$rs = $mysqli->query($sql);
					echo $mysqli->error ; 
					
					$count = $rs->fetch_assoc();
					$num_rows = $count["_c"] ; 
					$per_page = 25 ; 
					$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
					
					$pagination = (new Pagination());
					$pagination->setCurrent($page);
					$pagination->setRPP($per_page);
					$pagination->setTotal($num_rows);
					$pagination->setCrumbs(25);
					$paging = $pagination->parse();
					
					$page_start = (($per_page * $page) - $per_page ) ; 
		
					$sql = "SELECT * FROM `cmp_sapp_member` WHERE `cmp_sapp_member`.`is_password` = 1 {$sql_where_date} {$sql_search} ORDER BY `cmp_sapp_member`.`createtime` DESC  ";
					$rs = $mysqli->query($sql);
					echo $mysqli->error ; 
		
					
					$i = $num_rows - ($per_page * ($page - 1)) ; 
					while(( $row = $rs->fetch_assoc() )) {
?>
				<tr>
					<td class="text-center" ><?php echo $i  ?></td>
					<td class="text-center" ><?php echo mysqldate2thaidate($row["createtime"]  , "short" , true ) ?></td>
					<td class="text-center" ><?php echo htmlspecialchars($row["member_id"]) ?></td>
					<td><?php echo htmlspecialchars($row["name"]) ?></td>
					<td class="text-center"><?php echo isMobileByuAgent($row["user_agent"]) ? "Mobile" : "Desktop"  ?></td>
					<td class="text-center"><a href="?do=delete&id=<?php echo $row["member_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้ !');" ><span class="icon icon-trash-o"></span></a></td>
 				</tr>
<?php 
						$i -- ; 
					}
				?>
			</tbody>
		</table>
	</div>
	
<?php
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);

ob_start(); ?>
<script src="/js/ub-utility.js"></script>
<script>
	$(function() {
		
	});
</script>
<?php
$include_js[] = ob_get_contents();
ob_end_clean();
$smarty->assign("include_js", $include_js);

ob_start();
include "modal.inc.php";
$footer = ob_get_contents();
ob_end_clean();
$smarty->assign("footer", $footer);

$smarty->display(THEME.".tpl");