<div class="modal fade" id="modal_msg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"></h5>
			</div>
			<div class="modal-body text-center"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_alert" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"></h5>
			</div>
			<div class="modal-body text-center"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal_alert_btn_ok">ปิด</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"></h5>
			</div>
			<div class="modal-body text-center"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal_confirm_btn_cancel" data-text="ยกเลิก">ยกเลิก</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" id="modal_confirm_btn_ok" data-text="ตกลง">ตกลง</button>
			</div>
		</div>
	</div>
</div>