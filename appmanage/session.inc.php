<?php
global $admin;

$sql = "SELECT * FROM cmp_admin WHERE admin_status = 1 AND admin_id = '{$_SESSION["ADMIN_ID"]}'";
$rs = $mysqli->query($sql);
$admin = $rs->fetch_assoc();
if(empty($admin)) {
	if(!empty($_REQUEST["ajax"])) {
		echo json_encode([
			"status" => "-1",
			"error_msg" => "No login."
		]);
	}
	else {
		header("location: login?return_url=".urlencode($_SERVER["REQUEST_URI"]));
	}
	exit();
}

$smarty->assign("admin", $admin);