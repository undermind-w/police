<?php
require "config.inc.php";
require "session.inc.php";

header("Content-Type:text/html;charset=utf-8");

 

$do = isset($_POST['do']) ? $mysqli->real_escape_string($_POST['do']) : null ;
if( $do == 'confirm' ) {
	$confirm = (int) $_POST["confirm"] ; 
	$pay_id = (int) $_POST["pay_id"] ; 
	$is_confirm = (int) $_POST["is_confirm"] ; 
	
	if($confirm == 0 ){
		$sql = "UPDATE cmp_sapp_payment SET is_confirm = '{$is_confirm}' , confirmtime = NOW() WHERE pay_id = '{$pay_id}' LIMIT 1 " ; 
		$mysqli->query($sql);
		echo $mysqli->error ; 
	}
	
	echo "<script> alert('ยืนยันการชำระเงินแล้ว'); window.location.href = '?' </script>" ; 
	exit();
}

 

if( $_POST ) exit();

$active_menu = 2;
require "menu.inc.php";

ob_start(); ?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
 <?php
$header = ob_get_contents();
ob_end_clean();
$smarty->assign("header", $header);

ob_start(); ?>
<link rel="stylesheet" href="/js/fileuploads/css/dropify.min.css">
<style>
	.album-img div[class*='col-'] { margin-bottom: 15px; }
	.dropify-wrapper, .duplicate-wrapper {
		border: dashed 2px #ccc;
		border-radius: 4px;
		background: #f2f2f2;
	}
	.dropify-wrapper p {
		font-family: "Kanit", sans-serif;
	}
	.duplicate-wrapper {
    display: block;
    position: relative;
    cursor: pointer;
    overflow: hidden;
    width: 100%;
    max-width: 100%;
    padding: 5px 10px;
    font-size: 14px;
    line-height: 22px;
    color: #777;
    text-align: center;
    -webkit-transition: border-color .15s linear;
    transition: border-color .15s linear;
	}
	.duplicate-wrapper .duplicate-msg {
		position: relative;
    top: 50%;
    transform: translateY(-50%);
		-moz-transform: translateY(-50%);
		-webkit-transform: translateY(-50%);
	}
	.duplicate-wrapper .duplicate-msg span.fe-plus {
    font-size: 50px;
    color: #CCC;
	}

	#modal_confirm .btn { min-width: 120px; }
	#frm_data #btn_save,
	#frm_data #btn_save_public {
		min-width: 150px;
	}
	.card.card-gallery {
		border-radius: 10px;
		text-decoration: none;
	}
	.card.card-gallery p {
		margin: 0px;
	}
	#table-data .spinner {
		margin-right: 15px;
	}

	.ub_btn1 {
		width: 22px;
		height: 22px;
	}
	.form-check-inline-group {display:inline-block;padding-top:10px;}
	.form-check-inline {display:inline-block; }
	@media(max-width: 781px) {
		#frm_data #btn_save,
		#frm_data #btn_save_public {
			min-width: unset;
			width: 100%;
			display: block;
			margin-bottom: 10px;
		}
	}
</style>
<?php
$include_css = ob_get_contents();
ob_end_clean();
$smarty->append("include_css", $include_css);

$smarty->assign("title", "หลักฐานการชำระเงิน");

ob_start();
$pay_type = array( 1 => "เงินฝาก " , "เงินกู้" , "ถอนเงิน") ;
if(in_array($_GET["do"], ["add", "edit"])) {
	/*
		? ****************************************
		? Add & Edit View
		? ****************************************
	*/
 	$pay_id = (int) $_GET["payid"] ;  
	
	$sql = "SELECT cmp_sapp_payment.*
					, cmp_sapp_member.name
					, cmp_sapp_member.mobile
						FROM `cmp_sapp_payment` 
						LEFT JOIN `cmp_sapp_member` 
							ON `cmp_sapp_payment`.`member_id` =  `cmp_sapp_member`.`member_id` 
						WHERE `cmp_sapp_payment`.`pay_id` = '{$pay_id}' 
						LIMIT 0 , 1 ";
	$rs = $mysqli->query($sql); 
	echo $mysqli->error ; 
	$row = $rs->fetch_assoc(); 
	$smarty->assign("breadcrumb", ["หลักฐานการชำระเงิน" => "?", "รับรองการชำระเงิน" => "#" ]);
	ob_start(); 
	
?>
	 
	<form id="frm_data" class="form" action="?" method="post" enctype="multipart/form-data" data-toggle="validator">
		<input type="hidden" id="id" name="id" value="<?php echo $admin_id; ?>">
		<input type="hidden" id="do" name="do" value="confirm" />
		<input type="hidden" id="confirm" name="confirm" value="<?php echo $row["is_confirm"] ?>" />
		<input type="hidden" id="pay_id" name="pay_id" value="<?php echo $row["pay_id"] ?>" />
		
		<div class="panel panel-body">
			<h4 class="margin_0"><?php echo $btitle; ?></h4>
		</div>
		<div class="panel panel-body">
			<div class="row gutter-xs">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="act_title">ชื่อ-สกุล</label>
						<input type="text" id="name" name="name" value="<?php echo htmlspecialchars($row["name"]) ; ?>" class="form-control input-contrast" />
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="act_title">มือถือ</label>
						<input type="text" id="mobile" name="mobile" value="<?php echo htmlspecialchars($row["mobile"]) ; ?>" class="form-control input-contrast" />
					</div>
				</div>
			</div>
			<div class="row gutter-xs">
				<div class="col-xs-12 col-sm-6 ">
					<div class="form-group">
						<label for="act_title">วันที่</label>
						<input type="text" id="name" name="name" value="<?php echo htmlspecialchars(mysqldate2thaidate($row["createtime"] , true , true )) ?>" class="form-control input-contrast" />
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="act_title">รายละอียด</label>
						<input type="text" id="name" name="name" value="<?php echo htmlspecialchars( "ประเภท "  . $pay_type[$row["pay_type"]] . " บัญชี  " . $row["account_no"] . " จำนวนเงิน " . $row["amount"] . " บาท" ) ?>" class="form-control input-contrast" />
					</div>
				</div>
			</div>
			<div class="row gutter-xs">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="act_title">สถานะ </label>
						<div>
							<input type="radio" name="is_confirm" value="1" <?php echo $row["is_confirm"] == 1 ? "checked" : "" ; ?> <?php echo $row["is_confirm"] != 0 ? "disabled" :"" ; ?> /> ยืนยันการชำระเงิน
							<input type="radio" name="is_confirm" value="99" <?php echo $row["is_confirm"] == 99 ? "checked" : "" ; ?> <?php echo $row["is_confirm"] != 0 ? "disabled" :"" ; ?> /> ไม่อนุมัติ
							<input type="radio" name="is_confirm" value="0" <?php echo $row["is_confirm"] == 0 ? "checked" : "" ; ?> <?php echo $row["is_confirm"] != 0 ? "disabled" :"" ; ?> /> รอการชำระเงิน
						</div>
 					</div>
				</div>
			</div>
			<hr />
			<div class="row gutter-xs">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="act_title">รูปหลักฐานการชำระเงิน </label>
						<?php if(!empty($row["pay_file"])){ ?>
						<div class="row" >
							<div class="col-xs-4">
								<img class="img-fluld" style="display:block;max-width:100%" src="/uploads/app/payments/<?php echo $row["pay_file"] ?>"   />
							</div>
						</div>
						<?php } ?>
 					</div>
				</div>
			</div>
			
		</div> <!-- End .panel.panel-body -->
		<div class="text-right">
			<button type="submit" id="btn_save" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
			<a href="?" class="btn btn-default btn-lg">กลับ</a>
		</div>
	</form>
	<?php
		} else {
			
			
			$smarty->assign("breadcrumb", ["หลักฐานการชำระเงิน" => ""]);
			ob_start();
		?>
			<script>
				$(function() {
					$(this).on('click', '.view-pic', function() {
						
						
						$('#modal_msg .modal-header').addClass('hide');
						$('#modal_msg .modal-dialog').removeClass('modal-sm');
						$('#modal_msg .modal-body').html("<img style=\"max-width:100%;\" src=\"" + $(this).attr("href") + "\" />");
						$('#modal_msg').modal();
						
						return false ; 
					});

				 
				});
			</script>
		<?php
			$include_js = ob_get_contents();
			ob_end_clean();
			$smarty->append("include_js", $include_js);
			
			$search = htmlspecialchars($_GET["search"]) ; 
			$status = (int) htmlspecialchars($_GET["status"]) ; 

			$startdate = empty($_GET["startdate"]) ? date("Y-m-1") : $_GET["startdate"] ;
			$stopdate = empty($_GET["stopdate"]) ? date("Y-m-t") : $_GET["stopdate"] ;			
?>
	 
	<form class="form-inline" >
		<div class="form-group">
			<label for="month">ค้นหา</label>
			<div>
				<input class="form-control" type="text" name="search" id="search" placeholder="รหัสประจำตัว หรือ ชื่อ-สกุล" value="<?php echo htmlspecialchars($_GET["search"]) ?>"   /> 
			</div>
		</div>
		<div class="form-group">
			<label for="month">เริ่มวันที่</label>
			<div class="input-with-icon">
				<input class="form-control" type="text" name="startdate" id="startdate" data-provide="datepicker" value="<?php echo htmlspecialchars($startdate) ?>" data-date-format="yyyy-mm-dd" >
				<span class="icon icon-calendar input-icon"></span>
			</div>
		</div>
		<div class="form-group">
			<label for="year">ถีงวันที่</label>
			<div class="input-with-icon">
				<input class="form-control" type="text" name="stopdate" id="stopdate" data-provide="datepicker" value="<?php echo htmlspecialchars($stopdate) ?>" data-date-format="yyyy-mm-dd"  >
				<span class="icon icon-calendar input-icon"></span>
			</div>
		</div>
		<div class="form-group" style="margin-right:15px;margin-left:15px;" >
			<label for="year">สถานะ</label>
			<div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" id="status_0" name="status" value="0"  <?php echo $status == 0 ? "checked" : "" ; ?>  >
					<label class="form-check-label" for="inlineCheckbox0">ทั้งหมด</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" id="status_1" name="status" value="1"  <?php echo $status == 1 ? "checked" : "" ; ?> >
					<label class="form-check-label" for="inlineCheckbox1">อนุมัติ</label>
				</div>
				<!-- 
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" id="status_2" name="status" value="2"  >
					<label class="form-check-label" for="inlineCheckbox2">ไม่อนุมัติ</label>
				</div>
				-->
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" id="status_3" name="status" value="3" <?php echo $status == 3 ? "checked" : "" ; ?>  >
					<label class="form-check-label" for="inlineCheckbox3">รออนุมัติ</label>
				</div>
			</div>
		</div>
		 
		<!-- 
		<select class="form-control" name="status" id="status" >
			<option value="0" <?php echo $status == 0 ? "selected" : "" ; ?> >ทั้งหมด</option>
			<option value="1" <?php echo $status == 1 ? "selected" : "" ; ?> >อนุมัติ</option>
			<option value="2" <?php echo $status == 2 ? "selected" : "" ; ?> >ไม่อนุมัติ</option>
			<option value="3" <?php echo $status == 3 ? "selected" : "" ; ?> >รออนุมัติ</option>
		</select>
		-->
		<div class="form-group">
			<label for="year">&nbsp;</label>
			<button type="submit" class="btn btn-primary btn-block" >แสดงผล</button>
		</div>
<?php 
		$status = (int) htmlspecialchars($_GET["status"]);
		$startdate = htmlspecialchars ($startdate); 
		$stopdate = htmlspecialchars ($stopdate); 
?>
		<a class="btn btn-primary pull-right" href="payment.excel.php?search=<?php echo htmlspecialchars($_GET["search"]) ?>&status=<?php echo $status ?>&startdate=<?php echo $startdate ?>&stopdate=<?php echo $stopdate ?>" >Export Excle</a>

	</form>
	<div class="table-responsive">
			<table class="table table-hover ub_table1" id="table-data">
				<thead>
					<tr>
						<th class="text-center" width="75"  >ลำดับ</th>
						<th class="text-center"  width="200" >วันที่เวลา</th>
						<th class="text-center" width="100"  >รหัสสมาชิก</th>
						<th class="text-center" >ชื่อสมาชิก</th>
						<th class="text-center" width="200"  >เบอร์โทร</th>
						<th class="text-center" >รายละเอียด</th>
						<th class="text-center" width="75" >จำนวน</th>
						<th class="text-center" width="75" >ภาพประกอบ</th>
						<th class="text-center" width="75" >สถานะ</th>
						<th class="text-center" width="75" >&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if(!empty($search)){
							$sql_search = " AND ( `cmp_sapp_member`.`name` like '%{$search}%' OR `cmp_sapp_member`.`member_id` like '%{$search}%' )" ;
						}
						if(!empty($status)){
							if($status == 1 ){
								$sql_status = "AND `cmp_sapp_payment`.`is_confirm` = 1 " ; 
							}elseif($status == 2){
								$sql_status = "AND `cmp_sapp_payment`.`is_confirm` = 99 " ; 
							}elseif($status == 3){
								$sql_status = "AND `cmp_sapp_payment`.`is_confirm` = 0 " ; 
							}
						}
						
						if(!empty($_GET["startdate"])){
						
							$startdate = $mysqli->real_escape_string($_GET["startdate"]);
							$stopdate = $mysqli->real_escape_string($_GET["stopdate"]);
						
							$sql_where_date = " AND ( DATE(`cmp_sapp_payment`.`createtime`) >= DATE('{$startdate}') AND  DATE(`cmp_sapp_payment`.`createtime`) <= DATE('{$stopdate}') ) " ;
						}
					
						$sql = "SELECT cmp_sapp_payment.* 
									, `cmp_sapp_member`.`name` 
									, `cmp_sapp_member`.`mobile`
									FROM `cmp_sapp_payment` 
									LEFT JOIN `cmp_sapp_member` 
										ON `cmp_sapp_payment`.`member_id` =  `cmp_sapp_member`.`member_id` 
									WHERE 
										`cmp_sapp_payment`.`member_id` IS NOT NULL 
										{$sql_search}
										{$sql_where_date}
										{$sql_status}
										
									ORDER BY `cmp_sapp_payment`.`createtime` DESC 
											";
 						$rs = $mysqli->query($sql);
						echo $mysqli->error ; 
						
						$statuslabel = array(0 =>  "<span style=\"color:#E28110\">รออนุมัติ</span>" , 1 => "<span style=\"color:#108E37\">อนุมัติแล้ว</span>"  , 99 =>  "<span style=\"color:#FF0000\">ไม่อนุมัติ</span>"  ) ;

						while(( $row = $rs->fetch_assoc() )) {
?>
 					<tr>
						<td class="text-center" ><?php echo $row["pay_id"] ?></td>
						<td  class="text-center" ><?php echo mysqldate2thaidate($row["createtime"] , "short" , true ) ?></td>
						<td  class="text-center" ><?php echo $row["member_id"] ?></td>
						<td><?php echo $row["name"] ?></td>
						<td><?php echo $row["mobile"] ?></td>
						<td><?php echo $pay_type[$row["pay_type"]] . " " . $row["account_no"]  ?></td>
						<td><?php echo $row["pay_type"] == 1 || $row["pay_type"] == 2 ? number_format($row["amount"] , 2 , "." , "," ) . "บาท" : "-" ; ?></td>
						<td  class="text-center">
							<?php if(!empty($row["pay_file"])){ ?>
							<a class="view-pic" target="blank" href="/uploads/app/payments/<?php echo $row["pay_file"] ?>" ><span class="icon icon-file-picture-o"></span></a>
							<?php } ?>
						</td>
						<td  class="text-center" ><?php echo  $statuslabel[$row["is_confirm"]]; ?></td>
						<td  class="text-center"><a href="?do=edit&payid=<?php echo $row["pay_id"] ?>" ><span class="icon icon-pencil-square-o"></span></a></td>
					</tr>
<?php 
							 
						}
					?>
				</tbody>
			</table>
		</div>
	<?php
}
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);
ob_start();
include "modal.inc.php";
$footer = ob_get_contents();
ob_end_clean();
$smarty->assign("footer", $footer);

$smarty->display(THEME.".tpl");