<?php
require "config.inc.php";
require "session.inc.php";

header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$active_menu = 4;
require "menu.inc.php";
 	 
	if($_POST)
	{
 		$msg_id = (int) @$_POST["msg_id"] ; 
  		$msg_title = @$mysqli->real_escape_string(@$_POST["msg_title"]) ;
		$msg_detail = @$mysqli->real_escape_string(@$_POST["msg_detail"]) ;
		$msg_url = @$mysqli->real_escape_string(@$_POST["msg_url"]) ;
		$msg_senddate_date = @$mysqli->real_escape_string(@$_POST["msg_senddate"]) ;
		$msg_senddate_time_hour = (int) @$_POST["msg_senddate_time_hour"] ; 
		$msg_senddate_time_min = (int) @$_POST["msg_senddate_time_min"] ; 
		
		$msg_senddate_time_hour = sprintf("%02d",  $msg_senddate_time_hour  ) ; 
		$msg_senddate_time_min = sprintf("%02d",  $msg_senddate_time_min  ) ; 
		$msg_senddate  = $msg_senddate_date . " " . $msg_senddate_time_hour . ":" . $msg_senddate_time_min . ":00" ; 
		
  		$msg_status = (int) @$_POST["msg_status"] ; 
	 
		if(empty($msg_id))
		{
			$sql = "INSERT INTO cmp_message 
											( 
											msg_title 
											, msg_detail
											, msg_url 
											, msg_senddate
 											, createdate
											, updatedate
											, msg_status 
											) 
											VALUES  
											(
											'{$msg_title}'
											, '{$msg_detail}'
											, '{$msg_url}'
											, '{$msg_senddate}'
 											, NOW() 
											, NOW() 
											, {$msg_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$msg_id =  (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
			$cmplog->add_log("message-add" , "เพิ่มส่งข้อความถึงสมาชิก  " , $msg_id ) ;
			
		}
		else
		{	
 			$sql = "UPDATE cmp_message SET msg_title = '{$msg_title}' 
												, msg_detail = '{$msg_detail}'  
												, msg_url = '{$msg_url}'  
												, msg_senddate = '{$msg_senddate}'  
												, updatedate = NOW() 
												, msg_status = {$msg_status}
												WHERE 
												msg_id = {$msg_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;						
			
			$cmplog->add_log("message-edit" , "แก้ไขส่งข้อความถึงสมาชิก  " , $msg_id ) ;
			
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array($_GET["do"] , array("delete")) ){
		$msg_id = @$_GET["mid"] ;  
 		$sql = "DELETE FROM cmp_message WHERE msg_id = {$msg_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;	
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("message-delete" , "ลบส่งข้อความถึงสมาชิก  " , $msg_id ) ;
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	

		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
	<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
	<script>
		$(document).ready(function() {
			$('.date-picker').datepicker({
				orientation: "top auto" ,
				autoclose: true , 
				format : "yyyy-mm-dd"
			});
			 
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$msg_id = (int) $_GET["mid"] ; 
			$sql = "SELECT * FROM cmp_message WHERE msg_id = {$msg_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "ส่งข้อความถึงสมาชิก" );
		$smarty->assign("page_title", "ส่งข้อความถึงสมาชิก" );
 		$smarty->assign("breadcrumb", array("ส่งข้อความถึงสมาชิก" => "?" ,  "{$btitle}" => "#"  ) );

		$msg_id = (int) @$row["msg_id"] ; 
 		$msg_title =  @$row["msg_title"];  
		$msg_detail =  htmlspecialchars(@$row["msg_detail"]); 
		$msg_url = @$row["msg_url"];  
		$msg_senddate = @$row["msg_senddate"];  
 		$msg_senddate = empty($msg_senddate) ? date("Y-m-d H:i:s") : $msg_senddate ; 
		
		$msg_senddate_time = strtotime($msg_senddate);
		
		$msg_senddate_time_date =  date("Y-m-d" , $msg_senddate_time ) ; 
		$msg_senddate_time_hour = (int) date("H" , $msg_senddate_time ) ; 
		$msg_senddate_time_min = (int) date("i" , $msg_senddate_time ) ; 
		
 		$msg_status = (int) @$row["msg_status"]  ;  
		
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title"><?php echo @$type["type_title"] ?></h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="msg_id" value="<?php echo $msg_id ;  ?>"/>
  						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">หัวข้อ</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="msg_title" name="msg_title" placeholder="" value="<?php echo @$msg_title ?>" >
 							</div>
						</div>
						<div class="form-group">
							<label for="msg_detail" class="col-sm-3 control-label">ข้อความ</label>
							<div class="col-sm-9">
								<textarea name="msg_detail" id="msg_detail" class="form-control" rows="4" ><?php echo $msg_detail ?></textarea>
 							</div>
						</div>
						<div class="form-group">
							<label for="msg_url" class="col-sm-3 control-label">ลิงค์เนื้อหา</label>
  							<div class="col-sm-9">
								<input type="text"  class="form-control" id="msg_url" name="msg_url" placeholder="" value="<?php echo @$msg_url ?>" >
  							</div>
						</div>
						<div class="form-group">
							<label for="news_detail" class="col-sm-3 control-label">กำหนดส่ง</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control date-picker" id="msg_senddate" name="msg_senddate" placeholder="" value="<?php echo $msg_senddate_time_date  ?>" style="width:150px;float:left;"  >
 								<select class="form-control" id="msg_senddate_time_hour" name="msg_senddate_time_hour" style="width:75px;float:left;margin-left:10px;" >
								   <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 24 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$msg_senddate_time_hour == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
								<!-- 
								<div style="float:left; padding: 4px;" > : </div>
								<select class="form-control" id="msg_senddate_time_min" name="msg_senddate_time_min" style="width:75px;float:left"  >
								   <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 60 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$msg_senddate_time_min == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
								-->
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="msg_status" name="msg_status" <?php echo $msg_status == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1"> แสดงข้อมูล
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		$smarty->assign("title", "ส่งข้อความถึงสมาชิก" );
		$smarty->assign("page_title", "ส่งข้อความถึงสมาชิก" );
		$smarty->assign("breadcrumb", array("ส่งข้อความถึงสมาชิก" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(msg_id) as _c FROM cmp_message " ;
		$rs = $mysqli->query($sql);
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_message ORDER BY createdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">ส่งข้อความถึงสมาชิก</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="100" style="text-align:center" >ลำดับ</th>
						<th width="250" style="text-align:center" >วันที่ส่ง</th>
                        <th style="text-align:center" >ข้อความ</th>
                        <th width="150" style="text-align:center" >จัดการ</th>
						
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
						<td style="text-align:center" ><?php echo $i ++  ?></td>
						<td style="text-align:center" ><?php echo mysqldate2thaidate(@$row["msg_senddate"] , "short" , true ) ?></td>
                        <td><?php echo @$row["msg_title"] ?></td>
						<td style="text-align:center" >
							<a href="?in=message&do=edit&mid=<?php echo @$row["msg_id"] ?>" title="แก้ไขข้อมูล" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=message&do=delete&mid=<?php echo @$row["msg_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " title="ลบข้อมูล"  ><i class="fa fa-trash-o"></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 