<?php
require "config.inc.php";
require "session.inc.php";

$active_menu = 1;
require "menu.inc.php";

header("Content-Type:text/html;charset=utf-8");

ob_start(); ?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
 <?php
$header = ob_get_contents();
ob_end_clean();
$smarty->assign("header", $header);

ob_start(); ?>
<?php
$include_css[] = ob_get_contents();
ob_end_clean();
$smarty->assign("include_css", $include_css);

$smarty->assign("title", "Home");
$smarty->assign("breadcrumb", ["Home" => ""]);

ob_start(); ?>
<div class="panel panel-body">
	<h4 class="color1">Home</h4>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);

ob_start(); ?>
<script src="/js/ub-utility.js"></script>
<script>
	$(function() {
		
	});
</script>
<?php
$include_js[] = ob_get_contents();
ob_end_clean();
$smarty->assign("include_js", $include_js);

ob_start();
include "modal.inc.php";
$footer = ob_get_contents();
ob_end_clean();
$smarty->assign("footer", $footer);

$smarty->display(THEME.".tpl");