<?php
	ob_start();
	$data_menu = [
		'main' => [
			'title' => 'เมนูหลัก',
			'child' => [
									 1 => [ 'title' => 'ภาพรวม', 'icon' => 'fe-home icon', 'url' => "{$base_url}" ], 
									 2 => [ 'title' => 'หลักฐานการชำระเงิน', 'icon' => 'fe-settings icon', 'url' => "{$base_url}payment.php" ] ,
									 3 => [ 'title' => 'นำเข้าข้อมูลเงินฝาก', 'icon' => 'fe-settings icon', 'url' => "{$base_url}deposit.import.php" ] ,
									 4 => [ 'title' => 'ข้อมูลการสมัคร', 'icon' => 'fe-settings icon', 'url' => "{$base_url}register.php" ] , 
									  
									  
			]
		],
		'other' => [
			'title' => 'เมนูอื่นๆ',
		 	'child' => [
		 							999 => [ 'title' => 'ตั้งค่า', 'icon' => 'fe-settings icon', 'url' => "{$base_url}configadmin.php" ]
		 	]
		]
	];
?>
<ul class="sidenav level-1">
	<?php
		$config_menu = $data_menu;
		// echo "<pre>"; print_r($admin);
		// exit;
		foreach($config_menu as $main_key => $main_val) {
			echo sprintf('<li class="sidenav-heading">%s</li>', $main_val['title']);
			foreach($main_val['child'] as $child_key => $child_val) {
				// echo in_array($child_key, explode(',', $admin['admin_role']));
				// exit;
				if( empty( $admin['admin_role'] ) || in_array($child_key, explode(',', $admin['admin_role'])) || $child_key == 1 || $admin['admin_status'] ==1 ) {
					$active_text = $active_menu == $child_key ? ' class="active"' : '' ;
					$target_text = $child_val['url_target'] ? ' target="_blank"' : '' ;
					echo sprintf('
						<li class="sidenav-item">
							<a href="%s"%s%s>
								<span class="%s"></span>
								<span class="sidenav-label">%s</span>
							</a>
						</li>',
						$child_val['url'],
						$active_text,
						$target_text,
						$child_val['icon'],
						$child_val['title']
					);
				}
			}
		}
	?>

	<?php /*<li class="sidenav-item"><a href="<?php echo $base_url; ?>program"<?php if($active_menu == "3") { ?> class="active"<?php } ?>><span class="fe-grid icon"></span> <span class="sidenav-label">Program</span></a></li>*/ ?>
	<?php
	/*
	<li class="sidenav-heading">เมนูหลัก</li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>"<?php if($active_menu == "1") { ?> class="active"<?php } ?>><span class="fe-home icon"></span> <span class="sidenav-label">หน้าแรก</span></a></li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>registerprogram"<?php if($active_menu == "2") { ?> class="active"<?php } ?>><span class="fe-edit icon"></span> <span class="sidenav-label">ค่าย</span></a></li>

	<li class="sidenav-item"><a href="<?php echo $base_url; ?>course"<?php if($active_menu == "4") { ?> class="active"<?php } ?>><span class="fe-book icon"></span> <span class="sidenav-label">หลักสูตร</span></a></li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>gallery"<?php if($active_menu == "5") { ?> class="active"<?php } ?>><span class="fe-image icon"></span> <span class="sidenav-label">คลังภาพ</span></a></li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>portfolio"<?php if($active_menu == "6") { ?> class="active"<?php } ?>><span class="fe-folder icon"></span> <span class="sidenav-label">ผลงาน</span></a></li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>activities"<?php if($active_menu == "7") { ?> class="active"<?php } ?>><span class="fe-rss icon"></span> <span class="sidenav-label">ข่าวและกิจกรรม</span></a></li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>calendar"<?php if($active_menu == "8") { ?> class="active"<?php } ?>><span class="fe-calendar icon"></span> <span class="sidenav-label">ปฏิทิน</span></a></li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>contactus"<?php if($active_menu == "12") { ?> class="active"<?php } ?>><span class="fe-message-square icon"></span> <span class="sidenav-label">ติดต่อเรา</span></a></li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>enews"<?php if($active_menu == "13") { ?> class="active"<?php } ?>><span class="fe-mail icon"></span> <span class="sidenav-label">E-Newsletter</span></a></li>

	<li class="sidenav-heading">เมนูอื่นๆ</li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>student"<?php if($active_menu == "9") { ?> class="active"<?php } ?>><span class="fe-users icon"></span> <span class="sidenav-label">นักเรียน</span></a></li>
	<li class="sidenav-item"><a href="http://google.com/analytics/" target="_blank"<?php if($active_menu == "10") { ?> class="active"<?php } ?>><span class="fe-printer icon"></span> <span class="sidenav-label">รายงาน</span></a></li>
	<li class="sidenav-item"><a href="<?php echo $base_url; ?>configadmin"<?php if($active_menu == "11") { ?> class="active"<?php } ?>><span class="fe-settings icon"></span> <span class="sidenav-label">ตั้งค่า</span></a></li>
	*/ ?>
</ul>
<?php
$menu = ob_get_contents();
ob_end_clean();
$smarty->assign("menu", $menu);

ob_start();
?>
<ul class="nav navbar-nav navbar-right">
	<li class="dropdown hidden-xs">
	  <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
		<img class="circle" width="36" height="36" src="/images/user1.jpg" alt=""> Admin
		<span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu dropdown-menu-right">
		<li><a href="<?php echo $base_url; ?>logout">ออกจากระบบ</a></li>
	  </ul>
	</li>
	<li class="visible-xs-block">
	  <a href="<?php echo $base_url; ?>logout">
		<span class="fe-power icon-lg icon-fw"></span>
		ออกจากระบบ
	  </a>
	</li>
</ul>
<?php
$topmenu = ob_get_contents();
ob_end_clean();
$smarty->assign("topmenu", $topmenu);

ob_start();
?>
<button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
	<span class="sr-only">Toggle navigation</span>
	<span class="arrow-up"></span>
	<span class="ellipsis ellipsis-vertical">
	  <img class="ellipsis-object" width="32" height="32" src="/images/user1.jpg" alt="">
	</span>
</button>
<?php
$topmenu_mobile = ob_get_contents();
ob_end_clean();
$smarty->assign("topmenu_mobile", $topmenu_mobile);