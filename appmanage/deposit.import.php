<?php
require "config.inc.php";
require "session.inc.php";


require PATH."/class/PHPExcel-1.8/Classes/PHPExcel.php" ; 
require PATH."/class/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php"  ; 


header("Content-Type:text/html;charset=utf-8");
 

$do = isset($_POST['do']) ? $mysqli->real_escape_string($_POST['do']) : null ;
if( $do == 'submit' ) {
	 
	$admin_id = (int)$_POST['id'];
	
	
	// EXCLE
	$ucount = 0 ; 
	$file_1 = false ; 
	if(!empty($_FILES["file_1"]["name"] )) {
		$file_1 = true ; 
		$imgsn = "" ;
		$ints = date("YmdGis") . @random_char(7) ;
		if(!empty($_FILES["file_1"]["name"] )){
			$info = pathinfo($_FILES["file_1"]["name"]) ; 
			$imgsn = $ints . "." . $info["extension"] ; 
		}
		
		if(!empty($imgsn)) { 
			$filename = $imgsn ;
			@copy($_FILES["file_1"]["tmp_name"] , PATH . "/uploads/import/{$filename}") ;
			$path1 = PATH . "/uploads/import/{$filename}"  ;
			$tmppath1 = $path1  ;
		}
		$ucount ++ ; 
	}
	
	$file_2 = false ; 
	if(!empty($_FILES["file_2"]["name"] )) {
		
		$file_2 = true ; 
		$imgsn = "" ;
		$ints = date("YmdGis") . @random_char(7) ;
		if(!empty($_FILES["file_2"]["name"] )){
			$info = pathinfo($_FILES["file_2"]["name"]) ; 
			$imgsn = $ints . "." . $info["extension"] ; 
		}
		
		if(!empty($imgsn)) { 
			$filename = $imgsn ;
			@copy($_FILES["file_2"]["tmp_name"] , PATH . "/uploads/import/{$filename}") ;
			$path1 = PATH . "/uploads/import/{$filename}"  ;
			$tmppath2 = $path1  ;
			
		}
		$ucount ++ ; 
	}
	
	
	$file_3 = false ; 
	if(!empty($_FILES["file_3"]["name"] )) {
		
		$file_3 = true ; 
		$imgsn = "" ;
		$ints = date("YmdGis") . @random_char(7) ;
		if(!empty($_FILES["file_3"]["name"] )){
			$info = pathinfo($_FILES["file_3"]["name"]) ; 
			$imgsn = $ints . "." . $info["extension"] ; 
		}
		
		if(!empty($imgsn)) { 
			$filename = $imgsn ;
			@copy($_FILES["file_3"]["tmp_name"] , PATH . "/uploads/import/{$filename}") ;
			$path1 = PATH . "/uploads/import/{$filename}"  ;
			$tmppath3 = $path1  ;
			
		}
		$ucount ++ ; 
	}
	
	// echo $path1 ; 
	
 	if($ucount > 1 ){
		
		@unlink($tmppath1) ; 
		@unlink($tmppath2) ; 
		@unlink($tmppath3) ; 
		
		echo "<script> alert('อัพโหลดไพล์ได้ทีละประเภท');window.location.href = '?' </script>" ; 
		exit();
	}
	
	// Clear Data 
	if($file_1){
		// 55,56,57,58,59,63  
		$sql = "DELETE FROM `cmp_sapp_deposit` WHERE ( account_no NOT LIKE '54%' 
														AND account_no NOT LIKE '55%'  
														AND account_no NOT LIKE '56%' 
														AND account_no NOT LIKE '57%' 
														AND account_no NOT LIKE '58%' 
														AND account_no NOT LIKE '59%'
														AND account_no NOT LIKE '62%'
														AND account_no NOT LIKE '63%'
													) " ;
		$mysqli->query($sql);
		echo $mysqli->error ; 
	}
	
	if($file_2){
		$sql = "TRUNCATE TABLE `cmp_sapp_deposit` " ;
		$mysqli->query($sql);
		echo $mysqli->error ; 
		
		$sql = "ALTER TABLE `cmp_sapp_deposit` AUTO_INCREMENT = 1 " ; 
		$mysqli->query($sql);
		echo $mysqli->error ; 
	}
	
	if($file_3){
		// DELETE 62  
		$sql = "DELETE FROM `cmp_sapp_deposit` WHERE ( account_no LIKE '62%' ) " ;
		$mysqli->query($sql);
		echo $mysqli->error ; 
	}
	
	
	//echo $path1 ; 
	$inputFileName = $path1 ;
	
	try {
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($inputFileName);
		
	} catch(Exception $e) {
		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
	}
	 
	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); 
	$highestColumn = $sheet->getHighestColumn();
	
	 
	for ($row = 5 ; $row <= $highestRow; $row++){
		
		$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
		$data = $rowData[0] ; 
		
		if($file_1){
			// EVEY DAY 
			$member_id = $mysqli->real_escape_string($data[3]);
			$account_no = $mysqli->real_escape_string($data[1]);
			$account_title = $mysqli->real_escape_string($data[2]);
			$amount = (double) $mysqli->real_escape_string($data[4]);
			
			$member_id = sprintf("%06d", $member_id ); 
		}elseif($file_2){
			// MONTH 
			
			$member_id = $mysqli->real_escape_string($data[3]);
			$account_no = $mysqli->real_escape_string($data[1]);
			$account_title = $mysqli->real_escape_string($data[2]);
			$amount = (double) $mysqli->real_escape_string($data[4]);
			
			$member_id = sprintf("%06d", $member_id ); 
			
		}elseif($file_3){
			
			// MONTH 
			
			$member_id = $mysqli->real_escape_string($data[3]);
			$account_no = $mysqli->real_escape_string($data[1]);
			$account_title = $mysqli->real_escape_string($data[2]);
			$amount = (double) $mysqli->real_escape_string($data[4]);
			
			$member_id = sprintf("%06d", $member_id ); 
		}
		
		
		if($member_id != "000000" ){
			 
			$sql = "INSERT INTO `cmp_sapp_deposit`(`member_id`
											, `account_no`
											, `account_title`
											, `amount`
											, `createtime`) 
										VALUES ( '{$member_id}' 
											, '{$account_no}' 
											, '{$account_title}' 
											, '{$amount}'
											, NOW() )" ; 
			$mysqli->query($sql);
			echo $mysqli->error ; 
			  
		}
		 
		
	}
	
	@unlink($path1) ; 
	echo "<script> window.location.href = '?' </script>" ; 
	exit();
}
 

if( $_POST ) exit();

$do = isset($_GET['do']) ? $mysqli->real_escape_string($_GET['do']) : null ;
if($do == "delete" ){
	$sql = "DELETE FROM cmp_sapp_deposit WHERE 1 " ;
	$mysqli->query($sql);
	echo $mysqli->error ; 
	echo "<script> window.location.href = '?' </script>" ; 
	exit();
}


$active_menu = 3 ;
require "menu.inc.php";

ob_start(); ?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
 <?php
$header = ob_get_contents();
ob_end_clean();
$smarty->assign("header", $header);

ob_start(); ?>
<link rel="stylesheet" href="/js/fileuploads/css/dropify.min.css">
<style>
	.album-img div[class*='col-'] { margin-bottom: 15px; }
	.dropify-wrapper, .duplicate-wrapper {
		border: dashed 2px #ccc;
		border-radius: 4px;
		background: #f2f2f2;
	}
	.dropify-wrapper p {
		font-family: "Kanit", sans-serif;
	}
	.duplicate-wrapper {
    display: block;
    position: relative;
    cursor: pointer;
    overflow: hidden;
    width: 100%;
    max-width: 100%;
    padding: 5px 10px;
    font-size: 14px;
    line-height: 22px;
    color: #777;
    text-align: center;
    -webkit-transition: border-color .15s linear;
    transition: border-color .15s linear;
	}
	.duplicate-wrapper .duplicate-msg {
		position: relative;
    top: 50%;
    transform: translateY(-50%);
		-moz-transform: translateY(-50%);
		-webkit-transform: translateY(-50%);
	}
	.duplicate-wrapper .duplicate-msg span.fe-plus {
    font-size: 50px;
    color: #CCC;
	}

	#modal_confirm .btn { min-width: 120px; }
	#frm_data #btn_save,
	#frm_data #btn_save_public {
		min-width: 150px;
	}
	.card.card-gallery {
		border-radius: 10px;
		text-decoration: none;
	}
	.card.card-gallery p {
		margin: 0px;
	}
	#table-data .spinner {
		margin-right: 15px;
	}

	.ub_btn1 {
		width: 22px;
		height: 22px;
	}

	@media(max-width: 781px) {
		#frm_data #btn_save,
		#frm_data #btn_save_public {
			min-width: unset;
			width: 100%;
			display: block;
			margin-bottom: 10px;
		}
	}
</style>
<?php
$include_css = ob_get_contents();
ob_end_clean();
$smarty->append("include_css", $include_css);

$smarty->assign("title", "นำเข้าข้อมูลเงินฝาก");

ob_start();

if(in_array($_GET["do"], ["add", "edit"])) {
	
} else {
	/*
	? ****************************************
	? Add & Edit View
	? ****************************************
	*/
  

	$smarty->assign("breadcrumb", ["นำเข้าข้อมูลเงินฝาก" => "?" ]);
	ob_start(); ?>
	<script src="/js/ub-utility.js"></script>
	<script src="/js/ub-form-validator.js"></script>
	<script>
		$(function() {



			function save_data() {
				if(ub_form_valid($("#frm_data"))) {
					var $params = new FormData($("#frm_data")[0]);
					$params.append("ajax", "1");
					$params.append("do", "set_data");

					$.ajax({
						type: "POST",
						url: "?",
						data: $params,
						dataType: 'json',
						contentType: false,
						cache: false,
						processData: false,
						success: function( $json ) { console.log( $json );
							if($json["err_no"] == 0) {
								$("#id").val($json['data']['id']);
								$("#modal_confirm .modal-dialog").addClass('modal-sm');
								$("#modal_confirm #modal_confirm_btn_cancel").addClass('btn-default');
								ub_confirm($(this), '', `<div style="font-size: 50px;"><span class="fe-check-circle"></span></div><div>${ $json.msg }</div>`, function() {
									location.href = "?";
								}, "กลับหน้าหลัก", "ปิด");
							} else {
								$('#modal_alert .modal-body').html(`<h4 class="text-center">${$json.err_msg}</h4>`);
								$('#modal_alert').modal('show');
							}
						}
					});
				}
			}

			$("#btn_save").click(function() {
				save_data(1);
			});
		});
	</script>
	<?php
		$include_js = ob_get_contents();
		ob_end_clean();
		$smarty->append("include_js", $include_js);
		
		
		$sql = "SELECT COUNT(id) _c FROM cmp_sapp_deposit " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		$sum = $rs->fetch_assoc();
		
		
		$sql = "SELECT * FROM cmp_sapp_deposit ORDER BY createtime DESC LIMIT 0 ,1  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		$last = $rs->fetch_assoc();
	?>
	<form id="frm_data" class="form" action="?" method="post" enctype="multipart/form-data" data-toggle="validator">
		
		<input type="hidden" id="id" name="id" value="<?php echo $admin_id; ?>">
		<input type="hidden" id="do" name="do" value="submit">
		
		<div class="panel panel-body">
			<h4 class="margin_0"><?php echo $btitle; ?></h4>
		</div>
		<div class="panel panel-body">
			<div class="row" >
				<div class="col-md-4 "  ></div>
				<div class="col-md-4 "  >
					<div class="row gutter-xs">
						<div class="col-xs-12 text-center" style="font-size:20px;" >
							<div>จำนวนข้อมูล <?php echo number_format($sum["_c"])?> รายการ</div>
							<?php if(!empty($last)){ ?>
							<div>นำเข้าล่าสุดเมื่อ <?php echo mysqldate2thaidate($last["createtime"] , true , true )?></div>
							<?php } ?>
						</div>
					</div>
					<div class="row gutter-xs">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="file" >บัญชี ฝากประจำ  : อัปโหลดทุกสิ้นเดือน</label>
								<input type="file" id="file_2" name="file_2" style=";" />
							</div>
						</div>
					</div> 
					<div class="row gutter-xs">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="file" >บัญชี ฝากประจำ ประเภท 62 </label>
								<input type="file" id="file_3" name="file_3" style=";" />
							</div>
						</div>
					</div> 
					<div class="row gutter-xs">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="file" >บัญชี ออมทรัพย์ : อัปโหลดทุกวัน     </label>
								<input type="file" id="file_1" name="file_1" style=";" />
							</div>
						</div>
					</div>
					
					
					<div class="row gutter-xs">
						<div class="col-xs-12">
							<button type="submit" id="" class="btn btn-primary btn-lg btn-block">บันทึกข้อมูล</button>
							<a href="?do=delete"  class="btn btn-danger btn-lg btn-block" onclick="return confirm('ต้องการลบข้อมูล !! ')" >ลบข้อมูล</a>
						</div>
					</div>
				</div>
			</div>

		</div> <!-- End .panel.panel-body -->
		 
	</form>
<?php
}
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);
ob_start();
include "modal.inc.php";
$footer = ob_get_contents();
ob_end_clean();
$smarty->assign("footer", $footer);

$smarty->display(THEME.".tpl");