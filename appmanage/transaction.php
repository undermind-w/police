<?php
require "config.inc.php";
require "session.inc.php";

header("Content-Type:text/html;charset=utf-8");

function check_duplicate($mysqli, $admin_id, $field, $value) {
	$admin_id = (int)$admin_id;
	$sql = "SELECT admin_id
					FROM `admin`
					WHERE {$field} = '{$value}'
						AND admin_id <> {$admin_id}
						AND admin_status = 1
					LIMIT 1";
	$rs = $mysqli->query($sql);
	return $rs->num_rows; // 1 => ซ้ำ
}

	$do = isset($_POST['do']) ? $mysqli->real_escape_string($_POST['do']) : null ;
	if( $do == 'set_data' ) {
		$json = [
			'msg' => '',
			'err_no' => 0,
			'err_msg' => '',
			'data' => []
		];
		$admin_id = (int)$_POST['id'];
		$admin_name = $mysqli->real_escape_string( $_POST['admin_name'] );
		$admin_email = $mysqli->real_escape_string( $_POST['admin_email'] );
		$admin_username = $mysqli->real_escape_string( $_POST['admin_username'] );
		$admin_password = encrypt_text($mysqli->real_escape_string( $_POST['admin_password'] ));
		$admin_role = implode(',', $_POST['admin_role']);

		if( check_duplicate($mysqli, $admin_id, 'admin_username', $admin_username) ) {
			$json['err_no'] = 1;
			$json['err_msg'] = 'รหัสผู้ใช้ซ้ำ';
		} else if( check_duplicate($mysqli, $admin_id, 'admin_email', $admin_email) ) {
			$json['err_no'] = 2;
			$json['err_msg'] = 'อีเมล์ซ้ำ';
		}

		if( $json['err_no'] == 0 ) {
			if( empty($admin_id) ) {
				$sql = "INSERT INTO `admin`(`admin_username`, `admin_password`, `admin_name`, `admin_email`, `admin_role`, `createdate`, `admin_status`)
				VALUES('{$admin_username}', '{$admin_password}', '{$admin_name}', '{$admin_email}', '{$admin_role}', NOW(), 1)";
				$mysqli->query($sql);
				$admin_id = $mysqli->insert_id;
			} else {
				$sql = "UPDATE `admin` SET
									`admin_username` = '{$admin_username}',
									`admin_password` = '{$admin_password}',
									`admin_name` = '{$admin_name}',
									`admin_email` = '{$admin_email}',
									`admin_role` = '{$admin_role}',
									updatedate = NOW()
								WHERE admin_id = {$admin_id}";
				$mysqli->query($sql);
			}
			$json['data']['id'] = $admin_id;
			$json['msg'] = 'อัพเดทบัญชีผู้ใช้งานสำเร็จ' ;
		}
		echo json_encode( $json );
		exit();
	}

if( $do == 'del_data' ) {
	$json = [
		'err_no' => 0,
		'err_msg' => '',
		'redirect' => '?'
	];
	$admin_id = (int)$_POST['id'];
	$sql = "UPDATE `admin` SET
					admin_status = 0,
					updatedate = NOW()
					WHERE admin_id = {$admin_id}";
	$mysqli->query($sql);
	echo json_encode( $json );
	exit();
}

if( $_POST ) exit();

$active_menu = 6;
require "menu.inc.php";

ob_start(); ?>
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="description" content="<?php echo DESC; ?>" />
 <?php
$header = ob_get_contents();
ob_end_clean();
$smarty->assign("header", $header);

ob_start(); ?>
<link rel="stylesheet" href="/js/fileuploads/css/dropify.min.css">
<style>
	.album-img div[class*='col-'] { margin-bottom: 15px; }
	.dropify-wrapper, .duplicate-wrapper {
		border: dashed 2px #ccc;
		border-radius: 4px;
		background: #f2f2f2;
	}
	.dropify-wrapper p {
		font-family: "Kanit", sans-serif;
	}
	.duplicate-wrapper {
    display: block;
    position: relative;
    cursor: pointer;
    overflow: hidden;
    width: 100%;
    max-width: 100%;
    padding: 5px 10px;
    font-size: 14px;
    line-height: 22px;
    color: #777;
    text-align: center;
    -webkit-transition: border-color .15s linear;
    transition: border-color .15s linear;
	}
	.duplicate-wrapper .duplicate-msg {
		position: relative;
    top: 50%;
    transform: translateY(-50%);
		-moz-transform: translateY(-50%);
		-webkit-transform: translateY(-50%);
	}
	.duplicate-wrapper .duplicate-msg span.fe-plus {
    font-size: 50px;
    color: #CCC;
	}

	#modal_confirm .btn { min-width: 120px; }
	#frm_data #btn_save,
	#frm_data #btn_save_public {
		min-width: 150px;
	}
	.card.card-gallery {
		border-radius: 10px;
		text-decoration: none;
	}
	.card.card-gallery p {
		margin: 0px;
	}
	#table-data .spinner {
		margin-right: 15px;
	}

	.ub_btn1 {
		width: 22px;
		height: 22px;
	}

	@media(max-width: 781px) {
		#frm_data #btn_save,
		#frm_data #btn_save_public {
			min-width: unset;
			width: 100%;
			display: block;
			margin-bottom: 10px;
		}
	}
</style>
<?php
$include_css = ob_get_contents();
ob_end_clean();
$smarty->append("include_css", $include_css);

$smarty->assign("title", "ตั้งค่า");

ob_start();

if(in_array($_GET["do"], ["add", "edit"])) {
	/*
		? ****************************************
		? Add & Edit View
		? ****************************************
	*/
	$admin_id = $mysqli->real_escape_string($_GET["id"]);
	if(empty($admin_id)) {
		$btitle = "สร้างบัญชีผู้ใช้งานใหม่";
	} else {
		$btitle = "อัพเดทบัญชีผู้ใช้งาน";

		$sql = "SELECT *
					FROM admin
					WHERE admin_id = '{$admin_id}'
						AND admin_status = 1";
		$rs = $mysqli->query($sql);
		if( !$rs->num_rows ) {
			header('location:/admin/configadmin');
			exit();
		}
		$row = $rs->fetch_assoc();
	}

	$smarty->assign("breadcrumb", ["ตั้งค่า" => "?", "{$btitle}" => ""]);
	ob_start(); ?>
	<script src="/js/ub-utility.js"></script>
	<script src="/js/ub-form-validator.js"></script>
	<script>
		$(function() {



			function save_data() {
				if(ub_form_valid($("#frm_data"))) {
					var $params = new FormData($("#frm_data")[0]);
					$params.append("ajax", "1");
					$params.append("do", "set_data");

					$.ajax({
						type: "POST",
						url: "?",
						data: $params,
						dataType: 'json',
						contentType: false,
						cache: false,
						processData: false,
						success: function( $json ) { console.log( $json );
							if($json["err_no"] == 0) {
								$("#id").val($json['data']['id']);
								$("#modal_confirm .modal-dialog").addClass('modal-sm');
								$("#modal_confirm #modal_confirm_btn_cancel").addClass('btn-default');
								ub_confirm($(this), '', `<div style="font-size: 50px;"><span class="fe-check-circle"></span></div><div>${ $json.msg }</div>`, function() {
									location.href = "?";
								}, "กลับหน้าหลัก", "ปิด");
							} else {
								$('#modal_alert .modal-body').html(`<h4 class="text-center">${$json.err_msg}</h4>`);
								$('#modal_alert').modal('show');
							}
						}
					});
				}
			}

			$("#btn_save").click(function() {
				save_data(1);
			});
		});
	</script>
	<?php
		$include_js = ob_get_contents();
		ob_end_clean();
		$smarty->append("include_js", $include_js);
	?>
	<form id="frm_data" class="form" action="?" method="post" enctype="multipart/form-data" data-toggle="validator">
		<input type="hidden" id="id" name="id" value="<?php echo $admin_id; ?>">

		<div class="panel panel-body">
			<h4 class="margin_0"><?php echo $btitle; ?></h4>
		</div>
		<div class="panel panel-body">
			<div class="row gutter-xs">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="act_title">ชื่อสกุล</label>
						<input type="text" id="admin_name" name="admin_name" value="<?php echo $row["admin_name"]; ?>" class="form-control input-contrast" data-rule-required="true" data-msg-required="กรุณาป้อน ชื่อสกุล" />
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="act_title">อีเมล์</label>
						<input type="text" id="admin_email" name="admin_email" value="<?php echo $row["admin_email"]; ?>" class="form-control input-contrast" data-rule-required="true" data-msg-required="กรุณาป้อน อีเมล์" />
					</div>
				</div>
			</div>
			<div class="row gutter-xs">
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="act_title">รหัสผู้ใช้</label>
						<input type="text" id="admin_username" name="admin_username" value="<?php echo $row["admin_username"]; ?>" class="form-control input-contrast" data-rule-required="true" data-msg-required="กรุณาป้อน รหัสผู้ใช้" />
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="form-group">
						<label for="act_title">รหัสผ่าน</label>
						<input type="text" id="admin_password" name="admin_password" value="<?php echo decrypt_text($row["admin_password"]); ?>" class="form-control input-contrast" data-rule-required="true" data-msg-required="กรุณาป้อน รหัสผ่าน" />
					</div>
				</div>
			</div>
			<div class="row gutter-xs">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="act_title">สิทธิ์การใช้งาน</label>
					</div>

					<div class="form-group custom-controls-stacked">
						<?php
							$menu_role = $data_menu;
							unset($menu_role['main']['child'][1]);
							foreach($menu_role as $main_key => $main_val) {
								foreach($main_val['child'] as $child_key => $child_val) {
									$checked = empty($row['admin_role']) || in_array($child_key, explode(',', $row['admin_role'])) ? ' checked' : '';
									echo sprintf(
										'
										<label class="custom-control custom-control-primary custom-checkbox">
											<input class="custom-control-input" type="checkbox" name="admin_role[]" value="%s"%s />
											<span class="custom-control-indicator"></span>
											<span class="custom-control-label">%s</span>
										</label>
										',
										$child_key,
										$checked,
										$child_val['title']
									);
								}
							}
						?>
					</div>

				</div>
			</div>



		</div> <!-- End .panel.panel-body -->
		<div class="text-right">
			<button type="button" id="btn_save" class="btn btn-default btn-lg">บันทึก</button>
		</div>
	</form>
	<?php
		} else {
			$smarty->assign("breadcrumb", ["การฝากถอน" => ""]);
			ob_start();
		?>
			<script>
				$(function() {
					$('body').on('click', '.control-del', function() {
						$('#modal_msg .modal-header').addClass('hide');
						$('#modal_msg .modal-dialog').removeClass('modal-sm');
						$('#modal_msg .modal-body').html(`\
							<div class="confirm-del">\
								<div class="text-center"><i class="fe-help-circle"></i></div>\
								<h4>ต้องการลบใช่หรือไม่</h4>\
								<div class="row">
									<div class="col-xs-6"><button type="button" class="btn btn-block btn-outline-danger" id="confirm-del" data-id="${ $(this).data('id') }">ลบ</button></div>
									<div class="col-xs-6"><button type="button" class="btn btn-block btn-primary" data-dismiss="modal">เก็บไว้</button></div>
								</div>
							</div>\
						`);
						$('#modal_msg').modal();
					});

					$('body').on('click', '#confirm-del', function() {
						var $data = Array();
						$data.push({ name: "do", value: 'del_data' });
						$data.push({ name: "id", value: $(this).data('id') });
						$data = jQuery.param($data);

						$.ajax({
							type: "POST",
							url: "?",
							data: $data,
							dataType: 'json',
							cache: false,
							processData: false,
							success: function( $json ) { console.log( $json );
								if($json["err_no"] == 0) {
									location.href = $json.redirect;
								}
							}
						});
					});
				});
			</script>
		<?php
			$include_js = ob_get_contents();
			ob_end_clean();
			$smarty->append("include_js", $include_js);
	?>
	<!-- <div class="row gutter-xs m-b-lg">
		<div class="col-md-9 col-xs-6">
			<div class="input-with-icon">
				<input type="text" id="filter" name="filter" class="form-control" placeholder="ใส่คำค้นหา…">
				<span class="icon icon-search input-icon"></span>
			  </div>
		</div>
		<div class="col-md-3 col-xs-6">
			<a href="?do=add" class="btn btn-primary btn-block"><span class="fe-plus"></span> สร้างบัญชีผู้ใช้งานใหม่</a>
		</div>
	</div> -->
	<div class="table-responsive">
			<table class="table table-hover ub_table1" id="table-data">
				<thead>
					<tr>
						<th class="text-center" width="10%">ลำดับ</th>
						<th class="text-center" width="10%">รหัสอ้างอิง</th>
						<th class="text-center" width="15%">วันที่</th>
						<th class="text-center" width="8%">รหัสสมาชิก</th>
						<th class="text-center" width="20%">ชื่อสมาชิก</th>
						<th class="text-center" width="10%">รายการ</th>
						<th class="text-center" width="20%">จำนวน</th>
						<th class="text-center" width="20%">สถานะSCB</th>
						<th class="text-center" width="20%">สถานะKU</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i=1;
					$amount_sum = 0.0;
						$sql = "SELECT *
										FROM account_transaction
										ORDER BY trans_date DESC";
						$rs = $mysqli->query($sql);
						while(( $row = $rs->fetch_assoc() )) {
							$amount_sum += $row['amount'];
							// $control_del = $row['admin_id'] == $admin['admin_id'] ? '' : sprintf('<span class="control-del" data-id="%s"><span class="ub_btn1"><i class="fe-x"></i></span></span>', $row['admin_id']) ;
							echo sprintf('
								<tr>
									<td class="text-center">%s</td>
									<td class="text-center">%s</td>
									<td class="text-center">%s</td>
									<td class="text-center">%s</td>
									<td class="text-center">%s</td>
									<td class="text-center">%s</td>
									<td class="text-center">%s</td>
									<td class="text-center">%s</td>
									<td class="text-center">%s</td>
								</tr>
								',
								$i,
								$row['transaction_id'],
								$row['trans_date'],
								$row['member_id'],
								$row['mem_account'],
								$row['trans_type'] == 'deposit' ? 'ฝากเงิน' : 'ถอนเงิน',
								number_format($row['amount'],2),
								$row['messagescb'],
								$row['messageku']

								// $control_del
							);
							$i++;
						}
					?>
				</tbody>
				<thead>
					<tr>
						<th class="text-center" width="100">&nbsp;</th>
						<th class="text-center" width="100">&nbsp;</th>
						<th class="text-center" width="100">&nbsp;</th>
						<th class="text-center" width="100">&nbsp;</th>
						<th class="text-center" width="250">&nbsp;</th>
						<th class="text-right" width="250">รวม</th>
						<th class="text-center" width="100"><?php echo number_format($amount_sum,2);?></th>
						<th  width="100">บาท</th>
					</tr>
				</thead>
			</table>
		</div>
	<?php
}
$content = ob_get_contents();
ob_end_clean();
$smarty->assign("content", $content);
ob_start();
include "modal.inc.php";
$footer = ob_get_contents();
ob_end_clean();
$smarty->assign("footer", $footer);

$smarty->display(THEME.".tpl");