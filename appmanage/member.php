<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
 	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	  
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script>
	$(function(){
		$('.date-picker').datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
				
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		 
	}
	else
	{
		if(empty($_GET["year"]) || empty($_GET["month"])  )
		{
			$year = date("Y") ; 
			$month = date("m") ; 
				echo "<script> window.location.href = \"?year={$year}&month={$month}&keyword={$_GET["keyword"]}&start={$_GET["startdate"]}&end={$_GET["enddate"]}\"</script>" ;
			exit();
		}
		
		$year = (int) @$_GET["year"] ; 
		
		$month =(int) @$_GET["month"] ; 
 		$page = (int) @$_GET["page"]  ; 
		
		 $smarty->assign("title", "สมาชิก" );
		 $smarty->assign("page_title", "สมาชิก" );
		 $smarty->assign("breadcrumb", array("สมาชิก" => "#") );
		
		ob_start();
		
		$yy = $year ; 
		$mm = sprintf("%02d", $month);
		$logtable = "cmp_action_log_{$yy}{$mm}" ; 
		
		$where = "";
		if(!empty($_GET["keyword"])) {
			$where .= " AND (cmp_imp_member.member_no = '".$_GET["keyword"]."' OR cmp_imp_member.memname LIKE '%".$_GET["keyword"]."%')";
		}	
		if(!empty($_GET["start"])) {
			// $where .= "  AND TO_CHAR(cmp_imp_member.memdtm, 'YYYY-MM-DD') >=  '{$_GET["startdate"]}' ";
			// $where .= "AND ".("cmp_imp_member.memdtm BETWEEN '{$_GET["startdate"]}' AND '{$_GET["enddate"]}'")."";
			$where .= " AND (cmp_imp_member.memdtm BETWEEN  '{$_GET["start"]}' AND '{$_GET["end"]}')";
		}
		
		if(!empty($_GET["enddate"])) {
			// $where .= "  AND TO_CHAR(cmp_imp_member.memdtm, 'YYYY-MM-DD') <=  '{$_GET["enddate"]}' ";
		}
		
		//$sql = "SELECT COUNT(log_id) as _c FROM {$logtable} " ;
		
		$sql = "SELECT  COUNT( cmp_imp_member_data.member_no ) as _c
						 FROM cmp_imp_member 
						 LEFT JOIN cmp_imp_member_data ON  cmp_imp_member.member_no = cmp_imp_member_data.member_no 
		 WHERE cmp_imp_member_data.member_no  IS NOT NULL ".$where."
		 ORDER BY  cmp_imp_member.memdtm DESC 
						 
						 "; 
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 25 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		// $sql = "SELECT cmp_imp_member_data.member_no,cmp_imp_member_data.email,cmp_imp_member_data.telmobile,cmp_imp_member.memname,cmp_imp_member.membgroup_code,
		// cmp_imp_member.membgroup_desc,cmp_imp_member.birthdtm,cmp_imp_member_data.telmobile,cmp_imp_member.memdtm
		//  FROM cmp_imp_member_data
		// 					LEFT JOIN cmp_imp_member ON  cmp_imp_member.member_no = cmp_imp_member_data.member_no 			
		// 				WHERE YEAR(cmp_imp_member_data.update_time) = '{$year}' 
		// 					AND MONTH(cmp_imp_member_data.update_time) = '{$month}'
		// 				ORDER BY  cmp_imp_member_data.update_time DESC
		// 				LIMIT {$page_start} , {$per_page}   " ;
		// $rs = $mysqli->query($sql);
		// echo $mysqli->error; 
		$sql = "SELECT cmp_imp_member_data_stat.member_no,cmp_imp_member_data_stat.stat_data,cmp_imp_member_data_stat.stat_type,cmp_imp_member_data.telmobile,
		cmp_imp_member_data_stat.update_time,cmp_imp_member.*
		 FROM cmp_imp_member_data_stat
		 LEFT JOIN cmp_imp_member ON  cmp_imp_member.member_no = cmp_imp_member_data_stat.member_no 
		 LEFT JOIN cmp_imp_member_data ON cmp_imp_member_data.member_no = cmp_imp_member_data_stat.member_no
		 WHERE cmp_imp_member_data_stat.member_no  IS NOT NULL AND cmp_imp_member_data_stat.stat_type = 'telmobile' ".$where."
		 ORDER BY  cmp_imp_member_data_stat.update_time DESC 
						LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		// echo $sql;
		// exit;
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">สมาชิก</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
 			 <div style="clear: both;margin-bottom:15px"></div>
			<form class="form-inline" action="" method="get" style="margin-bottom:10px;" >
			<div class="row">
					<div class="col-md-4">
						<input type="text" id="keyword" name="keyword" class="form-control" value="<?php echo @$_GET["keyword"]; ?>" /> 
					</div>				
					<div class="col-md-5">
						<span class="control-label" style="float:left; text-align: center;margin-top: 5px;margin-right: 5px;"> วันที่สมัคร </span>
						<input type="text"  class="form-control date-picker" id="startdate" name="startdate" placeholder="" value="<?php echo $_GET["startdate"];?>" style="width:150px;float:left;margin-right:5px;" >
						<span class="control-label" style="float:left; text-align: center;margin-top: 5px;margin-right: 5px;"> สิ้นสุด </span>
						<input type="text"  class="form-control date-picker" id="enddate" name="enddate" placeholder="" value="<?php echo $_GET["enddate"];?>" style="width:150px;float:left;margin-right:5px;" >
					</div>
					<div class="col-md-3">
						<button type="submit" class="btn btn-primary"> ค้นหา </button>
						<a href="member.stas.export.php?keyword=<?php echo @$_GET["keyword"]; ?>&register=<?php echo @$_GET["register"]; ?>&startdate=<?php echo @$_GET["startdate"]; ?>&enddate=<?php echo @$_GET["enddate"]; ?>" target="blank" class="btn btn-success" style="margin-left:10px;"> Export To Excel </a>
					</div>					
				</div>								
				<!-- <a href="member.stas.export.php" target="blank" class="btn btn-success" style="margin-left:10px;"> Export To Excel </a> -->
 			</form> 
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="20" style="text-align:center" > ลำดับ</th>
 						<th width="150" style="text-align:center" > เลขทะเบียน </th>
						 <th width="" style="text-align:center" > รูปสมาชิก </th>
						<th width="" style="text-align:center" > ชื่อ - สกุล </th>
						<th width="150" style="text-align:center" >สังกัด</th>
						<th width="150" style="text-align:center" >วันเกิด</th>
						<!-- <th width="150" style="text-align:center" >สอ.มช.</th> -->
						<th width="150" style="text-align:center" >ข้อมูลเบอร์โทรสมาชิก</th>
						<th width="150" style="text-align:center" >วันที่สมัคร</th>
						<th width="150" style="text-align:center" >วันที่แก้ไข</th>
						<!-- <th width="150" style="text-align:center" >สมัครจาก</th>
						<th width="50" style="text-align:center" > &nbsp; </th> -->
						<?php /*<th width="150" style="text-align:center" > &nbsp; </th>*/ ?>
					</tr>
				</thead>
				<tbody>
					<?php 
					$i = 1 ; 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
					 	<td><?php echo $i++?></td>
 						<td><?php echo @$row["member_no"]?></td>
						 <?php if($row["imageBase64"]){ ?>
						 <td><img style=" margin-top: 0px ; position: inherit ; max-width:100% ; " src="<?php echo "https://www.cmcoop.or.th/timthumb.php?src=/ws_mobile/".@$row["imageBase64"]."&w=150&h=150" ?>"></td>
						 <?php } else { ?>
							<!-- uploads/contents/default_new.png -->
							<td><img style=" margin-top: 0px ; position: inherit ; max-width:100% ; " src="<?php echo "https://api.coop.in.th/mla/img/female.png" ?>"></td> 
						<?php } ?>
						<td><?php echo @$row["memname"] ?></td>
  						<td><?php echo @$row["membgroup_code"]." ".@$row["membgroup_desc"] ?></td>
						<td class="text-center"><?php echo ConvertToThaiDate(@$row["birthdtm"], true); ?></td>
						<td><?php echo $row["telmobile"];?></td>
						<td class="text-center"><?php echo $row["memdtm"]; ?></td>
						<td class="text-center"><?php echo $row["update_time"]; ?></td>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 <!-- Modal -->
	<div class="modal fade" id="model_profile_status" tabindex="-1" role="dialog" aria-labelledby="model_profile_status" >
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modelAddrStatsLabel">ดูข้อมูล</h4>
			</div>
			<div class="modal-body">
				<form>
				 <div class="form-group">
					<label for="txt_old">เปลี่ยน</label>
					<input type="text" class="form-control" id="txt_type"  value="" disabled />
				 </div>
				 <div class="form-group">
					<label for="txt_new">ข้อมูลใหม่</label>
					<textarea class="form-control" id="txt_new" disabled rows="3"> </textarea>
				  </div>
				 </form>
			</div>
		   
		</div>
	  </div>
	</div>
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 