<?php
	require "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	$do = isset($_POST['do']) ? $_POST['do'] : null ;

	if( $do == 'resetpwd' ) {
		$json = [
			'err_no' => 0,
			'err_msg' => '',
			'data' => []
		];
		$password = encrypt_text(trim($mysqli->real_escape_string( $_POST['password'] )));
    $ref = decrypt_text(trim($mysqli->real_escape_string( $_POST['ref'] )));

    $sql = "SELECT *
        FROM forgot_password
        WHERE ref = '{$ref}'
          AND section = 'admin'
          AND is_use = 0";
    $rs = $mysqli->query($sql);
    if( !$rs->num_rows ) {
      $json['err_no'] = 1;
      $json['err_msg'] = 'Link ไม่ถูกต้อง';
    } else {
      $row = $rs->fetch_assoc();
      $sql = "UPDATE `admin` SET
              admin_password = '{$password}'
              WHERE admin_id = {$row['id']}
              LIMIT 1";
      $mysqli->query($sql);
      $sql = "UPDATE forgot_password SET
              is_use = 1,
              update_time = NOW(),
              update_ip = '{$_SERVER["REMOTE_ADDR"]}'
              WHERE ref = '{$ref}'
                AND section = 'admin'";
      $mysqli->query($sql);
    }
		echo json_encode($json);
		exit();
	}

	if($_POST) exit();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Login</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>

    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Kanit:400&display=swap&subset=thai" rel="stylesheet">
    <link rel="stylesheet" href="/html/elephant/css/vendor.min.css">
    <link rel="stylesheet" href="/html/elephant/css/elephant.min.css">
    <link rel="stylesheet" href="/html/elephant/css/login-2.min.css">
		<link rel="stylesheet" href="/html/elephant/css/custom.css">
		<style>
			.login { max-width: 800px; }
			.login-body { padding: 0; }
			.login-wrap { padding: 80px 30px; }
			.banner_login { background: rgb(115,84,161); background: linear-gradient(45deg, rgba(115,84,161,1) 0%, rgba(36,44,124,1) 100%); }
			.banner_login img { width: 100%; }
			.login-form { margin-top: 30px; }
			.alert { padding: 5px 15px 5px 15px; }
		</style>
  </head>
  <body>
    <div class="login">
		<div class="login-body">
			<div class="row">
				<div class="col-md-12" style="padding-right: 0;">
					<div class="login-wrap">
						<div class="login-brand">
						  <img class="img-responsive center-block" src="/images/logo1.png" alt="">
						</div>
						<h3 style="margin-top: 0; color: #000;text-align: center;">ตั้งค่ารหัสผ่าน</h3>
						<div class="row">
							<div class="col-xs-12 col-sm-8 col-sm-offset-2">
								<div class="login-form">
                  <div id="err-msg"></div>
                  <?php
                    $ref = decrypt_text($_GET['ref']);
                    $sql = "SELECT ref
                            FROM forgot_password
                            WHERE ref = '{$ref}'
                              AND section = 'admin'
                              AND is_use = 0";
                    $rs = $mysqli->query($sql);
                  ?>
									<form id="frm-reset" method="post" action="">
                    <?php if( $rs->num_rows ) { ?>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<label for="email">ป้อนรหัสผ่านใหม่</label>
												</span>
												<input type="password" class="form-control" id="password" name="password" autocomplete="false" />
											</div>
                    </div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<label for="email">ป้อนรหัสผ่านใหม่อีกครั้ง</label>
												</span>
												<input type="password" class="form-control" id="password_confirm" name="password_confirm" autocomplete="false" />
											</div>
                    </div>
                    <div class="text-center"><button type="button" class="btn btn-primary btn-block" id="btn-submit">บันทึกรหัสผ่านใหม่</button></div>
                    <input type="hidden" id="ref" name="ref" value="<?php echo $_GET['ref']; ?>" />
                    <?php } else { ?>
                    <div class="alert alert-danger text-center" role="alert">Link ไม่ถูกต้อง</div>
                    <?php } ?>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		</div>
		<?php include "{$_SERVER["DOCUMENT_ROOT"]}/admin/modal.inc.php"; ?>
    <script src="/html/elephant/js/vendor.min.js"></script>
		<script src="/html/elephant/js/elephant.min.js"></script>
		<script>
			$(function() {
				function emailIsValid (email) {
					return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
				}

				$('body').on('click', '#btn-submit', function() {
					let $is_error = false;
					$('#err-msg').html('');
					if( $.trim($('#password').val()).length < 8 ) {
						$is_error = true;
						$('#err-msg').html('<div class="alert alert-danger" role="alert">กรุณาป้อนรหัสผ่านใหม่  อย่างน้อย 8 ตัว</div>');
					} else if( $.trim($('#password_confirm').val()).length < 8 ) {
            $is_error = true;
						$('#err-msg').html('<div class="alert alert-danger" role="alert">กรุณาป้อนรหัสผ่านใหม่อีกครั้ง  อย่างน้อย 8 ตัว</div>');
					} else if( $.trim($('#password').val()) != $.trim($('#password_confirm').val()) ) {
            $is_error = true;
            $('#err-msg').html('<div class="alert alert-danger" role="alert">กรุณาป้อนรหัสผ่านให้เหมือนกัน</div>');
          }
					if($is_error == false) {
						let $data = Array();
						$data.push({ name: "do", value: 'resetpwd' });
						$data.push({ name: "password", value: $.trim($('#password').val()) });
						$data.push({ name: "ref", value: $.trim($('#ref').val()) });
						$data = jQuery.param($data);

						$.ajax({
							type: "POST",
							url: "?",
							data: $data,
							dataType: 'json',
							cache: false,
							processData: false,
							success: function( $json ) { console.log( $json );
								if( $json.err_no > 0 ) {
									$('#err-msg').html(`<div class="alert alert-danger" role="alert">${$json.err_msg}</div>`);
								} else {
                  $('#frm-reset').html(`\
                  <h4 class="text-center">บันทึกรหัสผ่านใหม่แล้ว คลิกปุ่มด้านล่างเพื่อเข้าสู่ระบบใหม่</h4>\
                  <div class="text-center" style="margin-top: 30px;"><a class="btn btn-primary btn-block" href="/admin/login">เข้าสู่ระบบ</a></div>\
                  `);
								}
							}
						});
					}
				});
			});
		</script>
  </body>
</html>