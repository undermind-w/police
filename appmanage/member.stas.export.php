<?php
		session_start();
		require "config.inc.php" ;
		require "session.inc.php" ;
		require "menu.inc.php" ;
		
		 
		header("Content-Type:text/html;charset=utf-8");
		date_default_timezone_set('Asia/Bangkok');
	
	

	$year = (int) @$_GET["year"] ; 
	$month =(int) @$_GET["month"] ; 
 	
	$d = date("YmdHis");
 	$yy = $year ; 
	$mm = sprintf("%02d", $month);
 	
	// $type = array("telmobile" => "โทรศัพท์มือถือ" , "addr" => "ที่อยู่ปัจจุบัน" , "addrc"  => "ที่อยู่ที่ติดต่อได้" , "tel"  => "โทรศัพท์บ้าน" , "email" => "อีเมล" ) ;
	
	$sql = "SELECT cmp_imp_member_data.member_no,cmp_imp_member_data.email,cmp_imp_member_data.telmobile,cmp_imp_member.memname,
	cmp_imp_member.membgroup_code,cmp_imp_member.membgroup_desc,cmp_imp_member.birthdtm,cmp_imp_member.memdtm
		 FROM cmp_imp_member_data
		 LEFT JOIN cmp_imp_member ON  cmp_imp_member.member_no = cmp_imp_member_data.member_no 
		 WHERE cmp_imp_member_data.member_no  IS NOT NULL  AND cmp_imp_member.member_no
						ORDER BY  cmp_imp_member_data.update_time DESC" ;
		$rs = $mysqli->query($sql);
	echo $mysqli->error; 
	
	header("Content-type: application/vnd.ms-excel;charset=utf-8");
	header("Content-Disposition: attachment; filename=member{$year}{$month}_{$d}.xls"); 
	date_default_timezone_set('Asia/Bangkok');
	$i = 1 ; 
?><pre>
<html>
<body>
<style> .string{ mso-number-format:\@; } </style>
<table width="600" border="1">
<tr>
						<th width="50" style="text-align:center" > ลำดับ</th>
 						<th width="150" style="text-align:center" > เลขทะเบียน </th>
						<th width="" style="text-align:center" > ชื่อ - สกุล </th>
						<th width="300" style="text-align:center" >สังกัด</th>
						<th width="150" style="text-align:center" >วันเกิด</th>
						<!-- <th width="150" style="text-align:center" >สอ.มช.</th> -->
						<th width="150" style="text-align:center" >ข้อมูลเบอร์โทรสมาชิก</th>
						<th width="150" style="text-align:center" >วันที่สมัคร</th>
    <!-- <th width="50"> <div align="center">ลำดับ </div></th>
	<th width="250"> <div align="center">วันที่ </div></th>
	<th width="100"> <div align="center">รหัส </div></th>
 	<th width="250"> <div align="center">ชื่อ-สกุล </div></th>
	<th width="100"> <div align="center">ประเภทข้อมูล</div></th>
 	<th width="500"> <div align="left">ข้อมูลใหม่ </div></th> -->
</tr> 
<?php 
 
?>
<tr>
<?php while($row = $rs->fetch_assoc()) { ?>
    <td><?php echo $i++?></td>
 	<td><?php echo @$row["member_no"]?></td>
	<td><?php echo @$row["memname"] ?></td>
  	<td><?php echo @$row["membgroup_code"]." ".@$row["membgroup_desc"] ?></td>
	<td class="text-center"><?php echo ConvertToThaiDate(@$row["birthdtm"], true); ?></td>
	<td><?php echo $row["telmobile"];?></td>
	<td class="text-center"><?php echo ConvertToThaiDate(@$row["memdtm"], true); ?></td>
 </tr>
<?php } ?>
</table>
</body>
</html></pre>
