<?php
	session_start();
	require "config.inc.php";
	require "session.inc.php";
	
	header("Content-Type:text/html;charset=utf-8");
		date_default_timezone_set('Asia/Bangkok');
		$active_menu = 7;
	require "menu.inc.php";
 	 
	if($_POST)
	{
 		 
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("document"))  )
	{
	 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	  
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		$("#frm_journal").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		 
	}
	else
	{
		if(empty($_GET["year"]) || empty($_GET["month"])  )
		{
			$year = date("Y") ; 
			$month = date("m") ; 
			
			echo "<script> window.location.href = \"?year={$year}&month={$month}\"</script>" ;
			exit();
		}
		$year = (int) @$_GET["year"] ; 
		$month =(int) @$_GET["month"] ; 
 		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "การเข้าใช้งานระบบ" );
		$smarty->assign("page_title", "การเข้าใช้งานระบบ" );
		$smarty->assign("breadcrumb", array("การเข้าใช้งานระบบ" => "#") );
		
		ob_start();
		
		$yy = $year ; 
		$mm = sprintf("%02d", $month);
		$logtable = "cmp_action_log_{$yy}{$mm}" ; 
		$rs = $mysqli->query("SHOW TABLES LIKE '$logtable'") ; 
		if($rs->num_rows == 0 ){
			$logtable = "cmp_action_log" ; 
		}
		 
		
		$sql = "SELECT COUNT(log_id) as _c FROM {$logtable} " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 25 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT tb1.*  , admin.admin_name FROM {$logtable} as tb1 LEFT JOIN admin 
														ON admin.admin_id = tb1.admin_id  
													ORDER BY tb1.createdate DESC 
													LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		  
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">การเข้าใช้งานระบบ</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
 			 <div style="clear: both;margin-bottom:15px"></div>
			<form class="form-inline" action="?" method="get" style="margin-bottom:10px;"  >
				<div class="form-group">
					<label for="month">เดือน</label>
					<select id="month" name="month" class="form-control" >
						<?php for($i = 1 ; $i  <= 12 ; $i++){ ?>
						<option value="<?php echo $i ?>" <?php echo $month == $i ? "selected" : "" ;  ?> ><?php echo MonthName($i) ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<label for="year">ปี</label>
					<select id="year" name="year" class="form-control" >
						<?php for($i = 2000 ; $i  <= 2020 ; $i++){ ?>
						<option value="<?php echo $i ?>" <?php echo $year == $i ? "selected" : "" ;  ?> ><?php echo $i ?></option>
						<?php } ?>
					</select>
				</div>
				<button type="submit" class="btn btn-primary"> ดูข้อมูล </button>
 			</form> 
			<table class="table table-bordered">
				<thead>
					<tr>
 						<th width="225" style="text-align:center" > วันที่ </th>
						<th width="150" style="text-align:center" > ผู้ใช้ระบบ </th>
						<th width="" style="text-align:center" >รายละเอียด</th>
						
					</tr>
				</thead>
				<tbody>
					<?php 
					$i = 1 ; 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
 						<td><?php echo @mysqldate2thaidate(@$row["createdate"] , "long" , true )  ?></td>
						<td><?php echo @$row["admin_name"] ?></td>
  						<td><?php echo @$row["log_detail"] ?></td>
                      </tr>
					<?php 
						} 
						
						if($rs->num_rows == 0 ){
							echo "<tr><td colspan=\"3\" class=\"text-center\"> ไม่มีข้อมูล </td></tr>"  ; 
						}
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 