<?php
	session_start();
	require "config.inc.php";
	require "session.inc.php";

header("Content-Type:text/html;charset=utf-8");
date_default_timezone_set(TIMEZONE);

unset($_SESSION["ADMIN_ID"]);

echo "<script type='text/javascript'>document.location.href='login';</script>";
exit();