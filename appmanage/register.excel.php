<?php
	
	
	 
	
	define("PATH", $_SERVER["DOCUMENT_ROOT"]);
	
	require PATH . "/class/connect.inc.php";
	require PATH . "/class/function.inc.php";
	
	require PATH . "/class/PHPExcel-1.8/Classes/PHPExcel.php" ; 
	require PATH . "/class/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php"  ; 
 	
	
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");

	if(!empty($_GET["startdate"])){		
		$startdate = $mysqli->real_escape_string($_GET["startdate"]);
		$stopdate = $mysqli->real_escape_string($_GET["stopdate"]);
		$sql_where_date = " AND ( `cmp_sapp_member`.`createtime` >= DATE('{$startdate}') AND  `cmp_sapp_member`.`createtime` <= DATE('{$stopdate}') ) " ;
	}
					
					
	// Add some data
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'ลำดับ')
				->setCellValue('B1', 'วันที่สมัคร')
				->setCellValue('C1', 'รหัสสมาชิก')
				->setCellValue('D1', 'ชื่อสมาชิก')
				->setCellValue('E1', 'สมัครด้วย')
				;

	// Miscellaneous glyphs, UTF-8
	
	$sql = "SELECT * FROM `cmp_sapp_member` WHERE `cmp_sapp_member`.`is_password` = 1 {$sql_where_date} ORDER BY `cmp_sapp_member`.`createtime` DESC  ";
	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	$i = 2 ; 

	while($row = $rs->fetch_assoc()){
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $i , ($i - 1) )
				->setCellValue('B' . $i , $row["createtime"])
				->setCellValue('C' . $i , $row["member_id"])
				->setCellValue('D' . $i , $row["name"])
				->setCellValue('E' . $i , isMobileByuAgent($row["user_agent"]) ? "Mobile" : "Desktop" ) ;
	
	}
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('สมาชิก');


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);


	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="member.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	
	exit();
