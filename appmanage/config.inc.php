<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
define("PATH", $_SERVER["DOCUMENT_ROOT"]);

define("THEME", "elephant");
define("KEYWORD", "");
define("DESC", "");

require PATH."/class/connect.inc.php";
require PATH."/class/function.inc.php";
require PATH."/class/smarty.inc.php";
require PATH."/class/messages.class.php";
require PATH."/class/pagination.class.php";

$msg = new Messages();
$msg->msgClass = 'alert';
$msg->msgWrapper = '<div class="%s alert-%s">%s</div>';
$msg->msgBefore = '';
$msg->msgAfter = '';

$base_url = "/appmanage/";
$smarty->assign("base_url", $base_url);