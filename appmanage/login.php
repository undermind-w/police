<?php
require "config.inc.php";

header("Content-Type:text/html;charset=utf-8");
date_default_timezone_set('Asia/Bangkok');

if($_POST) {
	if(@$_POST["do"] == "login") {
		$username = $mysqli->real_escape_string($_POST["username"]);
		$password = $_POST["password"];
		
		if($_POST["is_remember"]) {
			setcookie("admin_username", $username, time() + (60 * 60 * 24 * 365), "/", $_SERVER["HTTP_HOST"]);
		}
		else {
			setcookie("admin_username", "", time() - 3600, "/", $_SERVER["HTTP_HOST"]);
		}
		
		$sql = "SELECT * FROM admin WHERE admin_status = 1 AND admin_username = '{$username}'";
		$rs = $mysqli->query($sql);
		echo $mysqli->error;
		$row = $rs->fetch_assoc();
		if(!empty($row) && decrypt_text($row["admin_password"]) == $password){
			$_SESSION["ADMIN_ID"] = $row["admin_id"];
			
			echo "<script type='text/javascript'>document.location.href='".(empty($_GET["return_url"]) ? "./" : $_GET["return_url"])."';</script>";
			exit();
		}
		
		echo "<script type='text/javascript'>document.location.href='login?error=1&return_url=".urlencode(@$_GET["return_url"])."';</script>";
		exit();
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Login</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
	
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Kanit:400&display=swap&subset=thai" rel="stylesheet">
    <link rel="stylesheet" href="/html/elephant/css/vendor.min.css">
    <link rel="stylesheet" href="/html/elephant/css/elephant.min.css">
    <link rel="stylesheet" href="/html/elephant/css/login-2.min.css">
	<link rel="stylesheet" href="/html/elephant/css/custom.css">
	<style>
		.login { max-width: 800px; }
		.login-body { padding: 0; }
		.login-wrap { padding: 80px 30px; }
		.login-brand { margin-left: 0; margin-right: 0; width: 170px; }
		.banner_login { background: rgb(7,173,45); background: linear-gradient(45deg, rgba(7,173,45,1) 0%, rgba(4,87,23,1) 100%); }
		.banner_login img { width: 100%; }
	</style>
  </head>
  <body>
    <div class="login">
		<div class="login-body">
			<div class="row">
				<div class="col-md-5" style="padding-right: 0;">
					<div class="login-wrap">
						<h3 style="color: #000;">COOP Application</h3>
						<h4>ยินดีต้อนรับระบบแอดมิน,</h4>
						<h3 style="margin-top: 0; color: #000;">เข้าสู่ระบบ</h3>
						<div class="login-form">
							<?php if(@$_GET["error"] == 1) { ?><div class="alert alert-danger" role="alert">อีเมล์/รหัสผ่าน ไม่ถูกต้อง</div><?php } ?>
							<form method="post" data-toggle="validator" action="">
								<input type="hidden" name="do" value="login">
								<div class="form-group">
								  <label for="email">อีเมล์</label>
								  <input id="email" class="form-control" type="text" name="username" value="<?php echo $_COOKIE["admin_username"]; ?>" spellcheck="false" autocomplete="off" data-msg-required="กรุณาป้อนอีเมล์" required>
								</div>
								<div class="form-group">
								  <label for="password">รหัสผ่าน</label>
								  <input id="password" class="form-control" type="password" name="password" data-msg-required="กรุณาป้อนรหัสผ่าน" required>
								</div>
								<div class="form-group">
								  <label class="custom-control custom-control-primary custom-checkbox">
									<input class="custom-control-input" type="checkbox" name="is_remember" checked="checked">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-label">จำบัญชีผู้ใช้งาน</span>
								  </label>
								  <a href="forgotpwd" class="pull-right">ลืมรหัสผ่าน ?</a>
								</div>
								<button class="btn btn-primary btn-block" type="submit">เข้าสู่ระบบ</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-7" style="padding-left: 0;">
					<div class="banner_login hidden-xs hidden-sm">
						<img src="/images/login.png" alt="">
					</div>
				</div>
			</div>
		</div>
    </div>
    <script src="/html/elephant/js/vendor.min.js"></script>
    <script src="/html/elephant/js/elephant.min.js"></script>
  </body>
</html>