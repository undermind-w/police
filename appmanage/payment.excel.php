<?php
	 

	define("PATH", $_SERVER["DOCUMENT_ROOT"]);
	
	require PATH . "/class/connect.inc.php";
	require PATH . "/class/function.inc.php";
	
	require PATH . "/class/PHPExcel-1.8/Classes/PHPExcel.php" ; 
	require PATH . "/class/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php"  ; 
 	
	
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");

	
	
	$search = htmlspecialchars($_GET["search"]) ; 
	$status = (int) htmlspecialchars($_GET["status"]) ; 
	$startdate =  htmlspecialchars($_GET["startdate"]) ; 
	
	if(!empty($search)){
		$sql_search = " AND ( `cmp_sapp_member`.`name` like '%{$search}%' OR `cmp_sapp_member`.`member_id` like '%{$search}%' )" ;
	}
	if(!empty($status)){
		if($status == 1 ){
			$sql_status = "AND `cmp_sapp_payment`.`is_confirm` = 1 " ; 
		}elseif($status == 2){
			$sql_status = "AND `cmp_sapp_payment`.`is_confirm` = 99 " ; 
		}elseif($status == 3){
			$sql_status = "AND `cmp_sapp_payment`.`is_confirm` = 0 " ; 
		}
	}
	
	if(!empty($startdate)){
		
		$startdate = $mysqli->real_escape_string($_GET["startdate"]);
		$stopdate = $mysqli->real_escape_string($_GET["stopdate"]);
	
		$sql_where_date = " AND ( DATE(`cmp_sapp_payment`.`createtime`) >= DATE('{$startdate}') AND  DATE(`cmp_sapp_payment`.`createtime`) <= DATE('{$stopdate}') ) " ;
	}

	$sql = "SELECT cmp_sapp_payment.* 
				, `cmp_sapp_member`.`name` 
				, `cmp_sapp_member`.`mobile`
				FROM `cmp_sapp_payment` 
				LEFT JOIN `cmp_sapp_member` 
					ON `cmp_sapp_payment`.`member_id` =  `cmp_sapp_member`.`member_id` 
				WHERE 
					`cmp_sapp_payment`.`member_id` IS NOT NULL 
					{$sql_search}
					{$sql_where_date}
					{$sql_status}
				ORDER BY `cmp_sapp_payment`.`createtime` DESC 
						";
	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	
	$statuslabel = array(0 =>  "รออนุมัติ" , 1 => "อนุมัติแล้ว"  , 99 =>  "ไม่อนุมัติ"  ) ;

					
	// Add some data
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'ลำดับ')
				->setCellValue('B1', 'วันที่เวลา')
				->setCellValue('C1', 'รหัสสมาชิก')
				->setCellValue('D1', 'ชื่อสมาชิก')
				->setCellValue('E1', 'เบอร์โทร')
				->setCellValue('F1', 'รายละเอียด')
 				->setCellValue('G1', 'สถานะ')
 				;

	// Miscellaneous glyphs, UTF-8
	
	$sql = "SELECT cmp_sapp_payment.* 
				, `cmp_sapp_member`.`name` 
				, `cmp_sapp_member`.`mobile`
				FROM `cmp_sapp_payment` 
				LEFT JOIN `cmp_sapp_member` 
					ON `cmp_sapp_payment`.`member_id` =  `cmp_sapp_member`.`member_id` 
				WHERE 
					`cmp_sapp_payment`.`member_id` IS NOT NULL 
					{$sql_search}
					{$sql_where_date}
					{$sql_status}
				ORDER BY `cmp_sapp_payment`.`createtime` DESC 
						";
	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	$i = 2 ; 

	while($row = $rs->fetch_assoc()){
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $i , $row["pay_id"]  )
				->setCellValue('B' . $i , mysqldate2thaidate($row["createtime"])  )
				->setCellValue('C' . $i , $row["member_id"])
				->setCellValue('D' . $i , $row["name"])
				->setCellValue('E' . $i , $row["mobile"])
				->setCellValue('F' . $i , $pay_type[$row["pay_type"]] . " " . $row["account_no"] )
				->setCellValue('G' . $i , $statuslabel[$row["is_confirm"]]) ; 
		$i ++ ; 
  	
	}
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('ชำระเงิน');
	
	foreach(range('A','G') as $columnID) {
		$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
	}

	$objPHPExcel->getActiveSheet()->getStyle("A1:G" . $i )->applyFromArray(
		array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)
		)
	);
	
	  $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $objPHPExcel->getActiveSheet()->getStyle("A1:G1" )->applyFromArray(array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )));
	
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);


	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="payment.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	
	exit();
