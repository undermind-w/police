<?php
	require "config.inc.php";
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	$do = isset($_POST['do']) ? $_POST['do'] : null ;

	if( $do == 'forgotpwd' ) {
		$json = [
			'err_no' => 0,
			'err_msg' => '',
			'data' => []
		];
		$email = trim($mysqli->real_escape_string( $_POST['email'] ));
		$sql = "SELECT admin_id, admin_name
						FROM `admin`
						WHERE admin_email = '{$email}'
							AND admin_status = 1
						LIMIT 1";
		$rs = $mysqli->query($sql);
		if( !$rs->num_rows ) {
			$json['err_no'] = 1;
			$json['err_msg'] = 'ไม่พบ Email ที่ระบุ กรุณาลองใหม่อีกครั้ง';
		} else {
			$row = $rs->fetch_assoc();
			$ref = genid();
			$sql = "INSERT INTO forgot_password(`section`, `id`, `name`, `email`, `ref`, `create_time`, `create_ip`)
								VALUES('admin', {$row['admin_id']}, '{$row['admin_name']}', '{$email}', '{$ref}', NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
			$mysqli->query($sql);

			$subject = "Reset Password STEAM4 INNOVATION ADMIN";
			$link = sprintf('https://%s/admin/resetpwd?ref=%s', $_SERVER["HTTP_HOST"], urlencode(encrypt_text($ref)));
      $mail_detail = sprintf('
				เรียน คุณ %s<br />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ระบบได้ทำการส่งลิงก์รีเซตรหัสผ่านให้ทางแล้ว <a href="%s" target="_blank">%s</a>  คลิกที่ลิงก์เพื่อทำการรีเซตรหัสผ่าน
				<br /><br />
				STEAM4 INNOVATION
        ',
				$row['admin_name'],
				$link,
				$link
      );
      ob_start();
      include PATH."/mail.form.php";
      $detail = ob_get_contents();
      ob_end_clean();
      send_mail($subject, $detail, $email);

		}
		echo json_encode($json);
		exit();
	}

	if($_POST) exit();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Login</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>

    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Kanit:400&display=swap&subset=thai" rel="stylesheet">
    <link rel="stylesheet" href="/html/elephant/css/vendor.min.css">
    <link rel="stylesheet" href="/html/elephant/css/elephant.min.css">
    <link rel="stylesheet" href="/html/elephant/css/login-2.min.css">
		<link rel="stylesheet" href="/html/elephant/css/custom.css">
		<style>
			.login { max-width: 800px; }
			.login-body { padding: 0; }
			.login-wrap { padding: 80px 30px; }
			.banner_login { background: rgb(115,84,161); background: linear-gradient(45deg, rgba(115,84,161,1) 0%, rgba(36,44,124,1) 100%); }
			.banner_login img { width: 100%; }
			.login-form { margin-top: 30px; }
			.alert { padding: 5px 15px 5px 15px; }
		</style>
  </head>
  <body>
    <div class="login">
		<div class="login-body">
			<div class="row">
				<div class="col-md-12" style="padding-right: 0;">
					<div class="login-wrap">
						<div class="login-brand">
						  <img class="img-responsive center-block" src="/images/logo1.png" alt="">
						</div>
						<h3 style="margin-top: 0; color: #000;text-align: center;">ลืมรหัสผ่าน</h3>
						<div class="row">
							<div class="col-xs-12 col-sm-8 col-sm-offset-2">
								<div class="login-form">
									<div id="err-msg"></div>
									<form method="post" action="">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<label for="email">ป้อนอีเมล์ของท่าน</label>
												</span>
												<input id="email" class="form-control" type="text" name="username" autocomplete="false" />
												<span class="input-group-btn">
													<button type="button" class="btn btn-primary btn-block" id="btn-reset">รีเซตรหัสผ่าน</button>
												</span>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		</div>
		<?php include "{$_SERVER["DOCUMENT_ROOT"]}/admin/modal.inc.php"; ?>
    <script src="/html/elephant/js/vendor.min.js"></script>
		<script src="/html/elephant/js/elephant.min.js"></script>
		<script>
			$(function() {
				function emailIsValid (email) {
					return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
				}

				$('body').on('click', '#btn-reset', function() {
					let $is_error = false;
					$('#err-msg').html('');
					if( $.trim($('#email').val()).length == 0 ) {
						$is_error = true;
						$('#err-msg').html('<div class="alert alert-danger" role="alert">กรุณากรอกอีเมล์</div>');
					} else if( !emailIsValid($.trim($('#email').val())) ) {
						$is_error = true;
						$('#err-msg').html('<div class="alert alert-danger" role="alert">อีเมล์ไม่ถูกต้อง</div>');
					}
					if($is_error == false) {
						let $data = Array();
						$data.push({ name: "do", value: 'forgotpwd' });
						$data.push({ name: "email", value: $.trim($('#email').val()) });
						$data = jQuery.param($data);

						$.ajax({
							type: "POST",
							url: "?",
							data: $data,
							dataType: 'json',
							cache: false,
							processData: false,
							success: function( $json ) { console.log( $json );
								if( $json.err_no > 0 ) {
									$('#err-msg').html(`<div class="alert alert-danger" role="alert">${$json.err_msg}</div>`);
								} else {
									$('#email').val('');
									$('#modal_msg .modal-header').addClass('hide');
									$('#modal_msg .modal-body').html(`\
										<div class="text-center"><i class="icon icon-envelope-o text-success" style="font-size: 50px;"></i></div>\
										<h4 class="text-center" style="margin: 30px 0px;">ระบบได้ส่งลิงก์รีเซตรหัสผ่านให้ท่านทางอีเมล์แล้ว</h4>\
										<div class="text-center"><button type="button" class="btn btn-primary" data-dismiss="modal">ปิดหน้าต่าง</button></div>\
									`);
									$('#modal_msg').modal('show');
								}
							}
						});
					}
				});
			});
		</script>
  </body>
</html>