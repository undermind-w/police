<?php
	
	session_start();
	
	header("Content-Type:application/json;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
	include "inc/inc.config.php";
 	
	$return = array() ; 
	$return["msg"] = "" ; 
	$return["status"] = "" ; 
	$return["url"] = "?" ; 
	$return["data"] = array() ;
	
	$member_id = $mysqli->real_escape_string($_POST["member_id"]) ;
	$password = $mysqli->real_escape_string($_POST["password"]) ;
	 
	
	if(!is_numeric($member_id)){
		$return["msg"] = "กรุณากรอกรหัสสมาชิก 6 หลัก" ; 
		$return["url"] = "?" ; 
		echo json_encode($return); 
		exit();
	}
	
	$member_id = sprintf("%06d", $member_id ); 

	 
	
	$sql = "SELECT * FROM cmp_sapp_member WHERE member_id like '{$member_id}' AND password like '{$password}' LIMIT 0 , 1" ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ; 
	$member = $rs->fetch_assoc();
 	 
	if(!empty($member)){
		
		$token = md5(random_char(20));
		$sql =  "UPDATE cmp_sapp_member SET token = '{$token}' , updatetime = NOW() , lastlogintime = NOW() WHERE member_id = '{$member["member_id"]}' LIMIT 1" ; 
		$mysqli->query($sql);
		echo $mysqli->error ; 
		if(empty($member["user_agent"])){
			
			$user_agent =  $mysqli->real_escape_string($_SERVER['HTTP_USER_AGENT'])  ; 
			$sql =  "UPDATE cmp_sapp_member SET user_agent = '{$user_agent}' , updatetime = NOW()  WHERE member_id = '{$member["member_id"]}' LIMIT 1" ; 
			$mysqli->query($sql);
			echo $mysqli->error ; 
		}
 
 
		setcookie("TOKEN", $token , time() + 84600 , "/" , $_SERVER["HTTP_HOST"]);
		$_SESSION["MEMBERID"] = $member["member_id"] ; 
		
 		$return["url"] = "/app/index.php" ; 
		$return["status"] = 1 ; 
	}else{
		$return["msg"] = "รหัสสมาชิก หรือรหัสผ่าน ไม่ถูกต้อง" ; 
		$return["url"] = "?" ; 
		
	}
	echo json_encode($return); 
	exit();
	
	
	
	
	