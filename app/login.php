<?php
	
	session_start();
	
	//ini_set('display_errors', 1);
	//ini_set('display_startup_errors', 1);
	//error_reporting(E_ALL);

	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
	include "inc/inc.config.php";
	
	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	 
	
	if($_POST) exit();

	ob_start();
	
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	
	
	ob_start();
?>
	 <script>
		$(document).ready(function() { 
			$('#loginform').ajaxForm(function(data) { 
				if(data.status == 1){
					window.location.href = data.url ;  
				}else{
					Swal.fire({
							icon: 'error',
							title: 'มีข้อผิดพลาด',
							text:  data.msg ,
							confirmButtonText: 'ตกลง',
						}) ; 
				}
            }); 
			
        }); 
	</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	$smarty->assign("script", $script);
	
	$smarty->assign("subject", "เข้าสู่ระบบ");
	$smarty->assign("breadcrumb", array("เข้าสู่ระบบ" => null ));
	
	ob_start();
 	
	
?>

<section class="main-section" >
	<div class="container" >
		<div class="row" >
			<div class="col-md-6 desktop-content mobile-hide" >
				<div class="desktop-picuture-content" >
					<img src="/html/app/images/desktop-picture.png" class="img-fluid"  />
				</div>
			</div>
			<div class="col-md-6" >
				<div class="login-content" >
					
					<div class="logo-content-1" >
						<img src="/html/app/images/logo-1.png" class="img-fluid login-logo-1" />
						<div class="logo-title" >
							<div class="main" >สหกรณ์ออมทรัพย์โรงพยาบาลตำรวจ จำกัด </div>
							<div class="sub" >Police General Hospital Saving and Credit Cooperative, Limited.</div>
						</div>
					</div>
					
					<form id="loginform" class="form-login" action="login.submit.php" method="post"  autocomplete="off" >
						<input autocomplete="off" name="hidden" type="text" style="display:none;">

						<h4 class="main-title">ระบบบริการสมาชิกออนไลน์<br />เข้าสู่ระบบ</h4>
						<div class="form-group">
							<input type="text" class="form-control" id="member_id" name="member_id" placeholder="รหัสสมาชิก" autocomplete="off" />
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="password" name="password" placeholder="รหัสผ่าน" autocomplete="off" />
						</div>
						<!-- 
						<div class="form-group text-right forget-password-block">
							<a href="forget.php" >ลืมรหัสผ่าน</a>
						</div>
						 -->
						<button type="submit" class="btn btn-default btn-block btn-custom-1">เข้าสู่ระบบ</button>
					 
					</form>
					<div class="reigster-block" >
						<a href="register.php" >ลงทะเบียนใช้งานครั้งแรก</a> | <a href="forgot.php" >ลืมรหัสผ่าน</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	
 
<?php
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("title", "เข้าสู่ระบบ");
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
