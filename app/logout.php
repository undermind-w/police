<?php
	
	session_start();
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
	include "inc/inc.config.php";
	include "inc/inc.session.php";

	$member_id 	= isset($_SESSION["MEMBERID"]) ? $_SESSION["MEMBERID"] : null;
	$member_id = $mysqli->real_escape_string($member_id) ; 
	
	$_SESSION["MEMBERID"] = "" ; 
	setcookie("TOKEN", ""	, time() - 3600, "/", $_SERVER["HTTP_HOST"]);
	 
 	$sql = "UPDATE cmp_sapp_member WHERE token = '' WHERE member_id = '{$member_id}' LIMIT 1 " ;
	$mysqli->query($sql);
	echo $mysqli->error  ; 
	 

	header("location: /app/login.php");
