<?php
	
	session_start();
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
	include "inc/inc.config.php";
	include "inc/inc.connect.upfsccoop.php";


	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	 
	ob_start();
	
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	
	
	ob_start();
?>
	<script>
		$(document).ready(function() { 
			$('#step2forgot').ajaxForm(function(data) { 
				if(data.status == 1){
					window.location.href = data.url ;  
				}else{
					Swal.fire({
							icon: 'error',
							title: 'พบข้อผิดพลาด',
							text:  data.msg ,
							confirmButtonText: 'ตกลง',
						}) ; 
				}
            }); 
			
        }); 
	</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	$smarty->assign("script", $script);
	
	$smarty->assign("subject", "ลืมรหัสผ่าน");
	$smarty->assign("breadcrumb", array("ลืมรหัสผ่าน" => null ));
	
	ob_start();
 	
	
?>

<section class="main-section" >
	<div class="container" >
		<div class="row" >
			<div class="col-md-6 desktop-content mobile-hide" >
				<div class="desktop-picuture-content" >
					<img src="/html/app/images/desktop-picture.png" class="img-fluid"  />
				</div>
			</div>
			<div class="col-md-6" >
				<div class="login-content" >
					<div class="logo-content-1" >
						<img src="/html/app/images/logo-1.png" class="img-fluid login-logo-1" />
						<div class="logo-title" >
							<div class="main" >สหกรณ์ออมทรัพย์โรงพยาบาลตำรวจ จำกัด </div>
							<div class="sub" >Police General Hospital Saving and Credit Cooperative, Limited.</div>
						</div>
					</div>
					<?php if(empty($_GET["step"])){ ?> 
					<form action="forgot.submit.php" method="post" id="step2forgot" class="form-register"  >
						<input type="hidden" id="step" name="step" value="1"  /> 
						<h4 class="main-title">ลืมรหัสผ่าน กรุณากรอกข้อมูลสมาชิก</h4>
						<div class="form-group">
							<input type="text" class="form-control" id="member_no" name="member_no" placeholder="รหัสสมาชิก" value="" required >
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="personid" name="personid" placeholder="เลขประจำตัวประชาขน"  value="" required>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="mobile" name="mobile" placeholder="หมายเลขโทรศัพท์มือถือ " value="" required >
						</div>
						<button type="submit" class="btn btn-default btn-block btn-custom-1">ยืนยันข้อมูล</button>
					</form>
					<?php }elseif($_GET["step"] == 2 ){ ?>
					<form action="forgot.submit.php" method="post" id="step2forgot"  class="form-register"  >
						<input type="hidden" id="step" name="step" value="2"  /> 
						<h4 class="main-title">ป้อนรหัส OTP</h4>
						<div class="form-group">
							<input type="number" class="form-control" id="otp" name="otp" placeholder="ป้อนรหัส OTP ที่ได้รับ" maxlength="6" />
						</div>
						<button type="submit" class="btn btn-primary btn-block btn-custom-1">ต่อไป</button>
					</form>
					<?php }elseif($_GET["step"] == 3 ){ ?>
					<form action="forgot.submit.php" method="post" id="step2forgot"  class="form-register"  >
						<input type="hidden" id="step" name="step" value="<?php echo (int) $_GET["step"] ?>"  /> 
						<h4 class="main-title">ตั้งค่ารหัสผ่านใหม่</h4>
						<div class="form-group">
							<input type="password" class="form-control" id="password1" name="password1" placeholder="รหัสผ่าน a-z , 0-9 ไม่น้อยกว่า 8 ตัวอักษร">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="password2" name="password2" placeholder="ป้อนรหัสผ่านอีกครั้ง">
						</div>
						<button type="submit" class="btn btn-primary  btn-block btn-custom-1">เปลี่ยนรหัสผ่าน</button>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
	
 
<?php
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("title", "ลงทะเบียนใช้งานครั้งแรก");
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
