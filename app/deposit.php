<?php

	session_start();

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	include "inc/inc.config.php";
	include "inc/inc.session.php";

	$do = isset($_GET["do"]) ? $_GET["do"] : null ;


	if($_POST) exit();


	$smarty->assign("head", 1 );
	$smarty->assign("foot", 1 );
	$smarty->assign("member", $member );
	$smarty->assign("title", "เงินฝาก" );
	$smarty->assign("breadcrumb", array("เงินฝาก" => null ));

	ob_start();

?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);


	ob_start();
?>

<?php
	$script = ob_get_contents();
	ob_end_clean();
	$smarty->assign("script", $script);


	ob_start();

	$sql = "SELECT SUM(amount) _sum FROM cmp_sapp_deposit WHERE member_id = '{$member["member_id"]}'" ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error  ;
	$sum = $rs->fetch_assoc();


?>


<section class="main-section" >
	<div class="container" >
		<div class="row" >
			<div class="col-md-3 mobile-hide" >
				<?php include "inc/inc.profile.php" ?>
			</div>
			<div class="col-md-9" >
				<div class="right-content" >
					<div class="deposit-head" >
						<div class="row" >
							<div class="col-6 title" >
								เงินฝาก
							</div>
							<div class="col-6 detail" >
								<?php echo number_format($sum["_sum"]  , 2 , "." , "," )?>
							</div>
						</div>
						<div class="row"><div class="col-12"><h6 class="text-center text-danger">**ระบบเป็นระบบออฟไลน์ รายการเคลื่อนไหวระหว่างวันจะแสดงข้อมูลรายการในวันทำการถัดไป</h6></div></div>
					</div>
					<div class="main-list" >
						<ul class="diposit-list" >
							<?php
								$sql = "SELECT * FROM cmp_sapp_deposit WHERE member_id = '{$member_id}' ORDER BY amount DESC " ;
								$rs = $mysqli->query($sql);
								echo $mysqli->error ;

								while($item = $rs->fetch_assoc()){
									$account_no = substr_replace($item["account_no"] , 'X', 3 , 1) ;
									$account_no = substr_replace($account_no , 'XX', 5 , 2)  ;
									//$account_no = $item["account_no"] ;
							?>
							<li class="list-item" >
								<div class="left" >
									<div class="account-title" ><?php echo empty($item["account_title"]) ? "เงินฝากออมทรัพย์ ATM" : $item["account_title"] ?></div>
									<div class="account-no" ><?php echo htmlspecialchars($account_no) ?>
									 </div>
									<div class="name" ><?php echo htmlspecialchars($member["name"]) ?></div>
								</div>
								<div class="right" >
									<div class="text-1" >คงเหลือ</div>
									<div class="amount" ><?php echo number_format($item["amount"] , 2 , "." , "," ) ?></div>
								</div>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("title", "เงินฝาก");
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
