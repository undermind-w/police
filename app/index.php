<?php
	
	session_start();
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
	include "inc/inc.config.php";
	include "inc/inc.session.php";

	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	 
	
	if($_POST) exit();

	
	$smarty->assign("head", 1 );
	$smarty->assign("foot", 1 );
	
	$smarty->assign("member", $member );
	$smarty->assign("title", "หน้าแรก" );
	$smarty->assign("breadcrumb", array("หน้าแรก" => null ));

	ob_start();
	
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	
	
	ob_start();
?>
	 
<?php
	$script = ob_get_contents();
	ob_end_clean();
	$smarty->assign("script", $script);
	
	
	ob_start();
 	
	
?>

<section class="main-section" >
	<div class="container" >
		<div class="row" >
			<div class="col-md-3 mobile-hide" >
			<?php include "inc/inc.profile.php" ?>
			</div>
			<div class="col-md-9" >
				<!-- 
				<div class="row desktop-hide " >
					<div class="col-md-12" >
						<div class="logo-content-1 " >
							<img src="/html/app/images/logo-1.png" class="img-fluid login-logo-2" />
							<div class="logo-title" >
								<div class="main" >สหกรณ์ออมทรัพย์โรงพยาบาลตำรวจ จำกัด </div>
								<div class="sub" >Police General Hospital Saving and Credit Cooperative, Limited.</div>
							</div>
						</div>
					</div>
				</div>
				-->
				<div class="row dash-list" >
					<div class="col-md-2 col-6 dash-block" >
						<a class="dash-item" href="deposit.php?" >
							<img class="img" src="/html/app/images/icon1.png" />
							<div class="title" >เงินฝาก</div>
						</a>
					</div>
					<div class="col-md-2 col-6 dash-block" >
						<a class="dash-item" href="payment.php?" >
							<img class="img" src="/html/app/images/icon2.png" />
							<div class="title" >แนบหลักฐาน<br />การชำระเงิน</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	
 
<?php
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("title", "หน้าแรก");
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
