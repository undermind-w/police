<?php

	session_start();

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	include "inc/inc.config.php";
	include "inc/inc.session.php";

	$do = isset($_GET["do"]) ? $_GET["do"] : null ;



	if(!empty($_POST)){
		$do = isset($_POST["do"]) ? $_POST["do"] : null ;
		if($do == "submit" ){
			$pay_type = (int) $_POST["pay_type"] ;
			$pay_file = "" ;

			if(!empty($_FILES["file"]["tmp_name"])){
				$ints = date("YmdGis") . @random_char(7) ;
				if($_FILES["file"]["type"]  == "image/png"||$_FILES["file"]["type"] == "image/x-png"){
					$imgsn = $ints.".png";
				}elseif($_FILES["file"]["type"] =="image/gif"){
					$imgsn = $ints.".gif";
				}elseif($_FILES["file"]["type"] =="image/pjpeg" || $_FILES["file"]["type"] =="image/jpeg"){
					$imgsn = $ints.".jpg";
				}

				if(!empty($imgsn)) {
					$pay_file = $imgsn ;
					@copy( $_FILES["file"]["tmp_name"] , PATH . "/uploads/app/payments/{$imgsn}" );
				}

			}

			$account_no = "" ;
			if($pay_type == 1 ){
				$account_no = $mysqli->real_escape_string($_POST["account_no_1"]);
			}elseif($pay_type == 2){
				$account_no = $mysqli->real_escape_string($_POST["account_no_2"]);
			}else{
				$account_no = "" ;
			}

			$amount = (double) $mysqli->real_escape_string($_POST["amount"]);

			$sql = "INSERT INTO `cmp_sapp_payment`(
												`member_id`
												, `pay_type`
												, `pay_file`
												, `account_no`
												, `amount`
												, `createtime`
 												, `is_confirm`)
											VALUES (
												'{$member["member_id"]}'
												, '{$pay_type}'
												, '{$pay_file}'
												, '{$account_no}'
												, '{$amount}'
												, NOW()
												, 0 )" ;
			$mysqli->query($sql);
			echo $mysqli->error ;

			echo "<script> window.location.href = 'payment.php' </script>" ;
			exit();
		}


	}


	$smarty->assign("head", 1 );
	$smarty->assign("foot", 1 );
	$smarty->assign("member", $member );
	$smarty->assign("title", "แนบหลักฐานการชำระเงิน" );
	$smarty->assign("breadcrumb", array("แนบหลักฐานการชำระเงิน" => null ));

	ob_start();

?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);


	ob_start();
?>
	<script>
		$(function(){
			function readURL(input) {
			  if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {

					$('#blah').attr('src', e.target.result);
					$('#blah').show(true);
				}
				reader.readAsDataURL(input.files[0]); // convert to base64 string
			  }
			}

			$("#btnuploads").click(function(){
				$("#file").click();

			});
			$("#file").change(function() {
				readURL(this);
			});

			$("input[name='pay_type']").change(function(){
				var type = parseInt($("input[name='pay_type']:checked").val()) ;
 				if(type == 1 ){
					$(".form-group-account-no-1").show();
					$(".form-group-account-no-2").hide();

					$("#account_no_1").attr("required", true ) ;
					$("#account_no_2").attr("required", false ) ;
				}else if(type == 2 ){
					$(".form-group-account-no-1").hide();
					$(".form-group-account-no-2").show();

					$("#account_no_1").attr("required", false ) ;
					$("#account_no_2").attr("required", true ) ;
				}else{
					$(".form-group-account-no-1").hide();
					$(".form-group-account-no-2").hide();

					$("#account_no_1").attr("required", false ) ;
					$("#account_no_2").attr("required", false ) ;
				}
			});

		})
	</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	$smarty->assign("script", $script);


	ob_start();

	$sql = "SELECT SUM(amount) _sum FROM cmp_sapp_deposit WHERE member_id = '{$member["member_id"]}' LIMIT 0 , 1" ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error  ;
	$sum = $rs->fetch_assoc();

	if($_GET["do"] == "form" ){
?>
<section class="main-section" >
	<div class="container" >
		<div class="row" >
			<div class="col-md-3 mobile-hide" >
				<?php include "inc/inc.profile.php" ?>
			</div>
			<div class="col-md-9" >
				<div class="right-content" >
					<div class="payment-head" >
						<div class="title" >
							แนบหลักฐานการชำระเงิน
						</div>
					</div>
					<div class="row" >
						<div class="col-md-6 mx-auto" >
							<form action="?" method="post" enctype="multipart/form-data" class="payment-uploads-form" >
								<input type="hidden" id="do" name="do" value="submit" />
								<div class="form-group">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="pay_type" id="pay_type_1" value="1" checked >
										<label class="form-check-label" for="inlineRadio1">เงินฝาก</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="pay_type" id="pay_type_2" value="2">
										<label class="form-check-label" for="inlineRadio2">เงินกู้</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="pay_type" id="pay_type_3" value="3">
										<label class="form-check-label" for="inlineRadio3">ถอนเงิน</label>
									</div>
								</div>
								<div class="form-group form-group-account-no-1">
									<select class="form-control" id="account_no_1" name="account_no_1"  placeholder="เลขที่บัญชี" required >
										<option value="" diasbled >กรุณาเลือกเลขที่บัญชี</option>
										<?php
											$sql = "SELECT * FROM cmp_sapp_deposit WHERE member_id = '{$member_id}' AND (account_no like '12%' OR account_no like '20%' OR account_no like '25%' OR account_no like '26%' ) ORDER BY amount DESC" ;
											$rs = $mysqli->query($sql);
											echo $mysqli->error ;
											while($item = $rs->fetch_assoc()){
												echo "<option value=\"{$item["account_no"]}\">{$item["account_no"]}</option>" ;
											}

										?>

									</select>

								</div>
								<div class="form-group form-group-account-no-2" style="display:none;" >
									<input type="text" class="form-control" id="account_no_2" name="account_no_2" placeholder="กรุณากรอกเลขที่บัญชี"  >
								</div>

								<div class="form-group">
									<input type="number" class="form-control" id="amount" name="amount" placeholder="จำนวนเงิน" required >
								</div>
								<div class="form-group">
									<div id="btnuploads" class="btn btn-primary btn-custom-2">
										<img src="/html/app/images/camera.png" />
										แนบรูปหลักฐาน
									</div>
									<input type="file" id="file" name="file" style="display:none;" />
									<img src="#" id="blah" class="img-fluid" style="display:none;margin: auto;margin-top: 10px;" />
								</div>
								<hr />
								<button type="submit" class="btn btn-default btn-block btn-custom-1">บันทึกข้อมูล</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
	}else{
?>


<section class="main-section">
	<div class="container" >
		<div class="row" >
			<div class="col-md-3 mobile-hide" >
				<?php include "inc/inc.profile.php" ?>
			</div>
			<div class="col-md-9" >
				<div class="right-content" >
					<div class="payment-head" >
						<div class="row" >
							<div class="col-md-6 title" >
								แนบหลักฐานการชำระเงิน
							</div>
							<div class="col-md-6 detail" >
								<a href="?do=form" class="gotoform" ><img src="/html/app/images/uploads.png" /> แนบหลักฐานการชำระเงิน</a>
							</div>
						</div>
						<div class="row"><div class="col-12"><h6 class="text-center text-danger">**ระบบเป็นระบบออฟไลน์ รายการเคลื่อนไหวระหว่างวันจะแสดงข้อมูลรายการในวันทำการถัดไป</h6></div></div>
					</div>
					<div class="main-list" >
						<ul class="diposit-list" >
							<?php

								$typetitle = array(1 => "เงินฝาก" , "ชำระเงินกู้" , "ถอนเงิน" );
								$sql = "SELECT * FROM cmp_sapp_payment WHERE member_id = '{$member_id}'   ORDER BY createtime DESC " ;
								$rs = $mysqli->query($sql);
								echo $mysqli->error ;


								$statuslabel = array(0 => "<span class=\"wait\" >รอดำเนินการ</span>" , 1 => "<span class=\"success\" >ดำเนินการแล้ว</span>" , 99 => "<span class=\"danger\" style=\"color:#FF0000\"  >ไม่อนุมัติ</span>"  ) ;
								while($item = $rs->fetch_assoc()){
									$account_no = !empty($item["account_no"]) ? $item["account_no"] : "-"  ;
							?>
							<li class="list-item" >
								<div class="left" >
									<div class="pay-title" ><?php echo $typetitle[$item["pay_type"]] ?></div>
									<div class="account-no" ><?php echo htmlspecialchars($account_no) ?></div>
									<div class="date" ><?php echo mysqldate2thaidate($item["createtime"] , true , true  ) ?></div>
								</div>
								<div class="right" >
									<div class="blank" >&nbsp;</div>
									<div class="amount" ><?php echo number_format($item["amount"]  , 2 , "." , "," ) ?></div>
									<div class="status" ><?php echo $statuslabel[$item["is_confirm"]] ; ?></div>

								</div>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php
	}


	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("title", "แนบหลักฐานการชำระเงิน");
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
