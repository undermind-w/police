<?php
	
	session_start();
	 
	include "inc/inc.config.php";
	include "inc/inc.session.php";
	
	
	
	
	

	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	
	 
	 
	
	if(!empty($_POST)){
		
		header("Content-Type:application/json;charset=utf-8");
		date_default_timezone_set('Asia/Bangkok');
		
		
		$passwordold = $mysqli->real_escape_string($_POST["passwordold"]);
		
		$password1 = $mysqli->real_escape_string($_POST["passwordnew1"]);
		$password2 = $mysqli->real_escape_string($_POST["passwordnew2"]);
		
		$password1 = trim($password1);
		$password2 = trim($password2);
		
		if($member["password"] != $passwordold ){
			$return["msg"] = "รหัสผ่านเดิมไม่ถูกต้อง" ; 
			echo json_encode($return);
			exit();
		}
		
		
		if(strlen($password1) < 8 || strlen($password2) < 8 ){
			$return["status"] = 0 ; 
			$return["msg"] = "รหัสผ่าน a-z , 0-9 อย่างน้อย 8 ตัวอักษร (ไม่เกิน 16 ตัวอักษร)" ; 
			echo json_encode($return);
			exit();
		}
		
		if(strlen($password2) > 16 || strlen($password2) > 16 ){
			$return["status"] = 0 ; 
			$return["msg"] = "รหัสผ่าน a-z , 0-9 อย่างน้อย 8 ตัวอักษร (ไม่เกิน 16 ตัวอักษร)" ; 
			echo json_encode($return);
			exit();
		}
		 
		
		$dump = preg_replace('/[0-9]+/', '', $password1);
		if(!empty($dump)){
			if(!ctype_lower($dump)){
				$return["status"] = 0 ; 
				$return["msg"] = "รหัสผ่าน a-z , 0-9 อย่างน้อย 8 ตัวอักษร (ไม่เกิน 16 ตัวอักษร)" ; 
				echo json_encode($return);
				exit();
			}
		}

		
		
		if($password1 !== $password2  ){
			$return["msg"] = "รหัสผ่านไม่ตรงกัน" ; 
			echo json_encode($return);
			exit();
		}
		
		$password = trim($password1) ; 
		
		
		
		$member_id = $member["member_id"] ; 
		$sql = "UPDATE `cmp_sapp_member` SET  password = '{$password}' , updatetime = NOW() WHERE `member_id` = '{$member_id}' LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ; 
		
		$return["status"] = 1 ; 
		$return["url"] = "password.php" ; 
		echo json_encode($return);
		exit();
	}
	 
	
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	
	$smarty->assign("head", 1 );
	$smarty->assign("foot", 1 );
	
	$smarty->assign("member", $member );
	$smarty->assign("title", "หน้าแรก" );
	$smarty->assign("breadcrumb", array("หน้าแรก" => null ));

	ob_start();
	
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	
	
	ob_start();
?>
	 <script>
		$(document).ready(function() { 
			$('#passwordform1').ajaxForm(function(data) { 
				if(data.status == 1){
					Swal.fire({
							icon: 'success',
							title: 'สำเร็จ',
							text:  "ได้ทำการเปลี่ยนรหัสผ่านของท่านแล้ว" ,
							confirmButtonText: 'ตกลง',
						}).then((result) => {
							window.location.href = data.url ; 
						}) ;
 				}else{
					Swal.fire({
							icon: 'error',
							title: 'พบข้อผิดพลาด',
							text:  data.msg ,
							confirmButtonText: 'ตกลง',
						}) ; 
				}
            }); 
			
        }); 
	 </script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	$smarty->assign("script", $script);
	
	
	ob_start();
 	
	
?>

<section class="main-section" >
	<div class="container" >
		<div class="row" >
			<div class="col-md-3 mobile-hide" >
			<?php include "inc/inc.profile.php" ?>
			</div>
			<div class="col-md-9" >
				<div class="right-content" >
					<div class="password-head" >
						<div class="title" >
							เปลี่ยนรหัสผ่าน
						</div>
					</div>
					<div class="row" >
						<div class="col-md-6 mx-auto" >
							<form id="passwordform1" action="?" method="post" enctype="multipart/form-data" class="password-form" >
								<input type="hidden" id="do" name="do" value="password" />
								 
								<div class="form-group">
									<input type="password" class="form-control" id="passwordold" name="passwordold" placeholder="รหัสผ่านเดิม" required >
								</div>
								 
								<div class="form-group">
									<input type="password" class="form-control" id="passwordnew1" name="passwordnew1" placeholder="รหัสผ่านใหม่" required >
								</div>
								 
								<div class="form-group">
									<input type="password" class="form-control" id="passwordnew2" name="passwordnew2" placeholder="รหัสผ่านใหม่ (ยืนยัน)" required >
								</div>								 
								<button type="submit" class="btn btn-default btn-block btn-custom-1">บันทึกข้อมูล</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	
 
<?php
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("title", "เปลี่ยนรหัสผ่าน");
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
