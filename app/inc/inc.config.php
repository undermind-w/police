<?php
	
	define("PATH",$_SERVER["DOCUMENT_ROOT"]) ;

	function curPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}

	define("TITLE"," ") ;
	define("KEYWORD","") ;
	define("DESC","") ;
	define("AUTHOR","") ;

	define("THEME","app.mobile");


	require PATH . "/class/connect.inc.php" ;
	require PATH . "/class/function.inc.php" ;
	require PATH . "/class/smarty.inc.php";
	require PATH . "/class/loguser.class.php";
	 
