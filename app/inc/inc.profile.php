<div class="content-profile" >
	<div class="profile-picture" >
		<?php 
		if(empty($member["picture"])){ 
			$urlpic = $member["sex"] == "M" ? "/html/app/images/profile-male.png" : "/html/app/images/profile-female.jpg" ; 
		?>
		<img src="<?php echo $urlpic ?>" class="img-1" />
		<?php }else{ ?> 
		<img src="https://fsccoop.upbean.co.th/uploads/member/<?php echo htmlspecialchars($member["picture"]) ?>" class="img-1" />
		<?php } ?>
	</div>
	<div class="welcome" >ยินดีต้อนรับ</div>
	<div class="name" >คุณ <?php echo htmlspecialchars($member["name"]) ?></div>
	<div class="last-login" > เข้าสู่ระบบเมื่อ  <?php echo mysqldate2thaidate($member["lastlogintime"] , "short" , true ) ?> </div>
	<div class="change-password" ><a href="/app/password.php"> <i class="fa fa-pencil" ></i> เปลี่ยนรหัสผ่าน </a></div>
</div>