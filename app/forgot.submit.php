<?php
	
	session_start();
 	
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	
	header("Content-Type:application/json;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
		
	
	include "inc/inc.config.php";
	include "inc/inc.connect.upfsccoop.php";
	

	$return = array() ; 
	$return["msg"] = "" ; 
	$return["status"] = "" ; 
	$return["url"] = "?" ; 
	$return["data"] = array() ;
	
	$do = isset($_POST["do"]) ? (int) $_POST["do"] : null ;
	$step = isset($_POST["step"]) ? (int) $_POST["step"] : null ;
	
	if($step == 1 ){
		// CHECK MEMEBER DETAIL 
		$member_id = $fsccoopmysqli->real_escape_string($_POST["member_no"]);
		$id_card = $fsccoopmysqli->real_escape_string($_POST["personid"]);
		$mobile = $fsccoopmysqli->real_escape_string($_POST["mobile"]);
		
		$member_id = trim($member_id) ; 
		$idcard = trim($id_card) ; 
		$mobile = trim($mobile) ; 
		
		$member_id = sprintf("%06d", $member_id );  
		
		$sql = "SELECT * FROM cmp_sapp_member WHERE member_id like '{$member_id}' LIMIT 0 , 1" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		$tmp = $rs->fetch_assoc();
		if(empty($tmp)){
			$return["msg"] = "ไม่พบข้อมูลการสมัครในระบบ" ; 
			$return["url"] = "?" ; 
			echo json_encode($return);
			exit();
		}
		
		$sql = "SELECT * FROM coop_mem_apply WHERE member_id like '{$member_id}' AND id_card like '{$id_card}' AND mobile like '{$mobile}' LIMIT 0 , 1" ;
		//$sql = "SELECT * FROM cmp_sapp_member WHERE member_id like '{$member_id}' AND idcard like '{$id_card}' AND mobile like '{$mobile}' LIMIT 0 , 1" ;
		$rs = $fsccoopmysqli->query($sql);
		echo $fsccoopmysqli->error ; 
		
		$apply = $rs->fetch_assoc();
		
		// 
		if(empty($apply)){
			$return["msg"] = "ไม่พบข้อมูลการสมัครในระบบ" ; 
			$return["url"] = "?" ; 
			echo json_encode($return);
			exit();
		}
		
 		
		
		
		
		
		
		$otp = rand(1,999999) ; 
		$otp = sprintf("%06d", $otp);
		
		$sql = "SELECT * FROM `cmp_sapp_member` WHERE member_id = '{$member_id}' LIMIT 0 , 1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		$member = $rs->fetch_assoc();
		
		$token = md5(random_char(20));
		
		$sql = "UPDATE `cmp_sapp_member` SET token = '{$token}' WHERE `member_id` = '{$member_id}' LIMIT 1 " ; 
		$mysqli->query($sql);
		echo $mysqli->error ; 
		
 		$sql = "INSERT INTO `cmp_sapp_member_forgot`( `member_id`
													, `otp`
													, `createtime`
 													, `is_confirm`
												) VALUES ( 
													'{$member["member_id"]}'
													, '{$otp}'
													, NOW() 
 													, 0 ) " ; 
		$mysqli->query($sql);
		echo $mysqli->error ; 
		  
		setcookie("TOKEN", $token , time() + 84600 , "/" , $_SERVER["HTTP_HOST"]);
			
		// SEND OTP
		$time1 = date("dM H:i"); 
		send_sms_2($apply["mobile"] , "รหัส OTP ของท่านคือ {$otp} @{$time1}");
 		$return["status"] = 1 ; 
		$return["url"] = "forgot.php?step=2" ; 
		echo json_encode($return);
		exit();
		
	}elseif($step == 2){
		// CHECK OTP ; 
 		$otp = $mysqli->real_escape_string($_POST["otp"]);
		$otp = trim($otp) ; 
		
 		$token = $mysqli->real_escape_string($_COOKIE["TOKEN"]);
		
		 
		
		$sql = "SELECT * FROM `cmp_sapp_member` WHERE  token = '{$token}' LIMIT 0 ,1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		$member = $rs->fetch_assoc();
		
		$sql = "SELECT * FROM  `cmp_sapp_member_forgot` WHERE member_id = '{$member["member_id"]}' AND otp like '{$otp}' AND is_confirm = 0 LIMIT 0 , 1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		$otp = $rs->fetch_assoc();
		
		if(empty($member)){
			$return["msg"] = "ชื่อผู้ใช้งานผิดพลาด" ; 
			echo json_encode($return);
 			exit();
		}
		
		if(empty($otp)){
			$return["msg"] = "OTP ไม่ถูกต้อง กรุณากรอกใหม่อีกครั้ง !!!" ; 
			echo json_encode($return);
 			exit();
		}
		
		
		$_SESSION["MEMBERID"] = $member["member_id"]  ; 
		
		$sql = "UPDATE `cmp_sapp_member_forgot` SET is_confirm = 1 , otptime = NOW()  WHERE `fg_id` = '{$otp["fg_id"]}' LIMIT 1" ; 
		$mysqli->query($sql);
		echo $mysqli->error ; 
		
 		
		$return["status"] = 1 ; 
		$return["url"] = "forgot.php?step=3" ; 
		echo json_encode($return);
 		exit();
	}elseif($step == 3){
		
		$member_id = $mysqli->real_escape_string($_SESSION["MEMBERID"]);
		$token = $mysqli->real_escape_string($_COOKIE["TOKEN"]);
		
		$password1 = $mysqli->real_escape_string($_POST["password1"]);
		$password2 = $mysqli->real_escape_string($_POST["password2"]);
		
		$password1 = trim($password1);
		$password2 = trim($password2);
		
		if(strlen($password1) < 8 || strlen($password2) < 8 ){
			$return["status"] = 0 ; 
			$return["msg"] = "รหัสผ่าน a-z , 0-9 อย่างน้อย 8 ตัวอักษร (ไม่เกิน 16 ตัวอักษร)" ; 
			echo json_encode($return);
			exit();
		}
		
		if(strlen($password2) > 16 || strlen($password2) > 16 ){
			$return["status"] = 0 ; 
			$return["msg"] = "รหัสผ่าน a-z , 0-9 อย่างน้อย 8 ตัวอักษร (ไม่เกิน 16 ตัวอักษร)" ; 
			echo json_encode($return);
			exit();
		}
		 
		
		$dump = preg_replace('/[0-9]+/', '', $password1);
		if(!empty($dump)){
			if(!ctype_lower($dump)){
				$return["status"] = 0 ; 
				$return["msg"] = "รหัสผ่าน a-z , 0-9 อย่างน้อย 8 ตัวอักษร (ไม่เกิน 16 ตัวอักษร)" ; 
				echo json_encode($return);
				exit();
			}
		}

		
		
		if($password1 !== $password2  ){
			$return["msg"] = "รหัสผ่านไม่ตรงกัน" ; 
			echo json_encode($return);
			exit();
		}
		
		$password = trim($password1) ; 
		
		$sql = "SELECT * FROM `cmp_sapp_member` WHERE token like '{$token}' AND member_id like '{$member_id}' LIMIT 0 ,1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		$member = $rs->fetch_assoc();
		
		$member_id = $member["member_id"] ; 
		$sql = "UPDATE `cmp_sapp_member` SET  password = '{$password}' , updatetime = NOW() WHERE `member_id` = '{$member_id}' LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ; 
		
		$return["status"] = 1 ; 
		$return["url"] = "login.php" ; 
		echo json_encode($return);
		exit();
		
	}elseif($step == 4){
	
	}
	
	
	  