<?php
	include "config.inc.php";
	
	if(empty($_GET["new"])){
		if(isset($_GET["nid"])){
			$nid = (int) $_GET["nid"] ;  
			echo "<script>window.location.href = '/news/{$nid}' </script>" ;
			exit();
		}else{
			$type = (int) $_GET["type"] ; 
			echo "<script>window.location.href = '/news?type={$type}' </script>" ;
			exit();
		}
		
	}
	$url = "http://{$_SERVER["SERVER_NAME"]}{$_SERVER["REQUEST_URI"]}";
	
	if(@$_POST["do"] == "get-fb") {
  		echo @getFacebooks(@$_POST["url"]);
		exit();
	}
	if(@$_POST["do"] == "get-tw") {
		echo @getTweets(@$_POST["url"]);
		exit();
	}
	if(@$_POST["do"] == "get-gp") {
		echo @getPlus1(@$_POST["url"]);
		exit();
	}
	
	ob_start();
	 
	?>
	<meta name="description" content="<?php echo @DESC; ?>" />
	<meta name="keywords" content="<?php echo @KEYWORD; ?>" />
	<meta name="author" content="<?php echo @AUTHOR; ?>" />


<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>	
 
 
	<script>
		$.ajaxSetup({ cache: true });
 		function handleClientLoad(){
			console.log($(".btn-gp").data("url")) ; 
			var params = {
			  nolog: true,
			  id: $(".btn-gp").data("url") ,
			  source: "widget",
			  userId: "@viewer",
			  groupId: "@self"
			};
 			gapi.client.setApiKey('AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ')
			gapi.client.rpcRequest('pos.plusones.get', 'v1', params).execute(function(resp) {
				console.log('count:', resp.result.metadata.globalCounts.count)
				$(".count-social-gp").html(resp.result.metadata.globalCounts.count) ; 
				social_sum();
			});
		}
		
		$(function(){

			/** fancybox **/
			$(".fancybox").fancybox();
			
			 

			/*
			 *	@ Social Section
			 *************************************************/
			if($(".count-social").length) {
				$(".count-social").each(function() {
					get_social($(this));
				});
			}

			function get_social(obj) {
				obj.html("<i class='fa fa-refresh fa-spin'></i>");
				//console.log(obj.attr("data-url"));
				if(obj.attr("ref") == "fb"){
					$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
						FB.init({
						version: 'v2.4'  
						}); 
						var u = encodeURIComponent(obj.attr("data-url")) ; 
						FB.api({
							method: 'fql.query',
							query: 'SELECT like_count, total_count, share_count, click_count, comment_count FROM link_stat WHERE url = "' +  u  + '"'
						}, function(data) {
							//console.log(   data);
							obj.html(data[0]["share_count"]);
							social_sum();
						}
						);
					});
				}else if(obj.attr("ref") == "tw"){
					var u = encodeURIComponent(obj.attr("data-url")) ; 
					$.ajax({
						url: "http://cdn.api.twitter.com/1/urls/count.json?url=" + u ,
						method: "GET",
						dataType: "jsonp"
 					}).done(function(data) {
						console.log(data) ; 
						obj.html(data.count);
						social_sum();
					});
				}else if(obj.attr("ref") == "gp"){
					var u = encodeURIComponent(obj.attr("data-url")) ; 
					

					/*
					gapi.client.setApiKey('AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ')
					gapi.client.rpcRequest('pos.plusones.get', 'v1', params).execute(function(resp) {
					  console.log('count:', resp.result.metadata.globalCounts.count)
					});
					*/
					 	//obj.html(data[0]['result']['metadata']['globalCounts']['count']);
						//social_sum();
				}else{
					
					$.ajax({
						type: "POST"
						, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
						, data: {
							"do" : "get-" + obj.attr("ref")
							, "url" : encodeURIComponent(obj.attr("data-url"))
							, "_time" : Math.random()
						}
						, async: true
						, success: function(data) {
							obj.html(data);
							social_sum();
						}
					});
				}
			}

			

			$("body").on("click", ".gp", function() {
				var windowFeatures = "status=no,height=" + 368 + ",width=" + 700 + ",resizable=no,toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
			window.open('https://plus.google.com/share?url='+encodeURIComponent($(this).attr('data-url')),'', windowFeatures);
				return false;
			});
			$("body").on("click", ".tw", function() {
				var windowFeatures = "status=no,height=" + 368 + ",width=" + 700 + ",resizable=no,toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
				window.open('https://twitter.com/intent/tweet?url='+encodeURIComponent($(this).attr('data-url'))+'&text='+encodeURIComponent($(this).attr("data-text")),'share-tw', windowFeatures);
				return false;
			});
			$("body").on("click", ".fb", function() {
				var windowFeatures = "status=no,height=" + 368 + ",width=" + 700 + ",resizable=no,toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
				window.open('http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent($(this).attr('data-url')), 'share-fb', windowFeatures);
				return false;
			});
					
			
			
		});
		function social_sum() {
				var sum = 0;
				var val = 0;
				$(".count-social").each(function() {
					val = isNaN(parseInt($(this).html().replace(",", ""))) ? 0 : parseInt($(this).html().replace(",", ""));
					sum += val;
				});
				$(".num-share").html(sum);
			}
	</script>
	<script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();
?>

<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	$content = "" ; 
	
	if(!empty($_GET["nid"]))
	{
		$news_id = (int) @$_GET["nid"] ; 
		
		$sql = "UPDATE cmp_news SET news_viewcount = news_viewcount + 1 WHERE news_id = '{$news_id}' " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		
		$sql = "SELECT * , cmp_admin.admin_name FROM cmp_news LEFT JOIN cmp_admin ON cmp_news.admin_id = cmp_admin.admin_id  
						WHERE news_status = 1 
							AND publicdate < NOW() 
							AND news_id = '{$news_id}'  
							ORDER BY publicdate DESC 
							LIMIT 0 , 1  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$news = $rs->fetch_assoc();
		if(empty($news))
		{
			header('Location: index.php');
			exit() ; 
		}
		
		$sql = "SELECT * FROM cmp_news_type WHERE type_id = '{$news["news_type_id"]}' LIMIT 0 , 1  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$type =  $rs->fetch_assoc();
		
	
		$smarty->assign("subject", $type["type_title"]);
		$smarty->assign("breadcrumb", array("{$type["type_title"]}" => "/news.php?type={$type["type_id"]}" ));
		$smarty->assign("title", htmlspecialchars($news["news_title"])) ;
		
		
		
		ob_start();
	?>
		
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row">
			<div class="col-md-9">
				<h2 class="news-title"><?php echo @htmlspecialchars(@$news["news_title"]) ?></h2>
				<div class="news-header-line">
					<div class="item" >โดย&nbsp;<?php echo @$news["admin_name"] ?></div>
 					<div class="item" ><i class="fa fa-clock-o"></i>&nbsp;วันที่ <?php echo mysqldate2thaidate(@$news["publicdate"] , "short" , true ) ?></div>
 					<div class="item" ><i class="fa fa-eye"></i>&nbsp;<?php echo @$news["news_viewcount"] ?></div>
				</div>
				<div class="news-header-detail">
				<?php echo htmlspecialchars_decode($news["news_detail"] , ENT_QUOTES ) ;  ?>
				<br />
				<br />
				<div class="fb-like" data-href="http://<?php echo $_SERVER["SERVER_NAME"] ?>/news/<?php echo $news["news_id"] ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>

				</div>
				<div class="news-footer">
					<?php 
						$sql = "SELECT * FROM cmp_news_item WHERE nitem_code = 'document' AND nitem_status = 1 AND news_id = '{$news["news_id"]}' ORDER BY nitem_seq, nitem_id" ;
						$rs = $mysqli->query($sql);
						$mysqli->error ;
						if( $rs->num_rows > 0 ){
					?>
					<div class="row" >
						<div class="col-md-12">
							<h5>เอกสาร</h5>
							<ul>
								<?php while($item = $rs->fetch_assoc()){ ?>
								<li><a target="_blank" download="<?php echo @htmlspecialchars(@$item["nitem_title"]) ?>" href="/uploads/news/document/<?php echo $item["nitem_path"] ?>"><?php echo $item["nitem_title"] ?></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
						<?php } ?>
					<div class="row">
						<div class="col-md-12">
							<div class="news-picture-item">
							<?php 
								$sql = "SELECT * FROM cmp_news_item WHERE nitem_code = 'images' AND nitem_status = 1  AND news_id = '{$news["news_id"]}' ORDER BY createdate DESC " ; 
								$rs = $mysqli->query($sql);
								echo $mysqli->error ;
								$i = 0 ; 
								while($item = $rs->fetch_assoc()){
								if($i == 0 )
								{
									$firstitem = $item ; 
								}
								$i ++ ; 
							?>
								<div class="item">
									<a target="_blank" href="/uploads/contents/<?php echo @$item["nitem_path"]  ?>" class="fancybox" rel="group1" >
										<img class="img-fluid img-thumbnail" src="/timthumb.php?src=/uploads/contents/<?php echo @$item["nitem_path"]  ?>&w=640&h=640" /> 
									</a>
								</div>
							<?php 
								}
							
							?>
								<div style="clear:both;"></div>
							</div>
						</div>
					</div>
 					<div class="row">
						<!-- <div class="col-xs-3 f-s-13"></div> -->
						<div class="col-xs-12 text-left">
							 
						</div>
					</div>
				</div>
 			</div>
			<div class="col-md-3">
				<div class="right-panel">
					<!-- 
					<a href="/page.php?pid=16" target="_blank" >
						<img class="img-responsive" style="margin:auto" src="/images/moblieapp.png" />
					</a>
					-->
				</div>
				
				<div class="right-panel">
					<div class="right-top-news">
						<h2 class="" >ข่าวล่าสุด</h2>
						<div class="line"></div>
						<div class="list">
							<?php 
								
								$sql = "SELECT * FROM cmp_news WHERE news_status = 1 AND publicdate < NOW() ORDER BY publicdate DESC LIMIT 0 , 5  " ;
								$rs = $mysqli->query($sql);
								echo $mysqli->error; 
								while($row = $rs->fetch_assoc()){
									
							?>
							<div class="row item">
								<div class="col-sm-4 item-image">
									<div class="image">
										<?php if(empty($row["news_picture"])){  ?>
										<img class="img-fluid" style="margin:auto" src="/timthumb.php?src=/images/blank.png&w=640&h=640" />
										<?php }else{ ?>
										<img class="img-fluid" style="margin:auto" src="/timthumb.php?src=/uploads/contents/<?php echo $row["news_picture"] ?>&w=640&h=640" />
										<?php } ?>
									</div>
								</div>
								<div class="col-sm-8 item-detail">
									<h5 class="title" ><a href="/news/<?php echo $row["news_id"] ?>" ><?php echo $row["news_title"] ?></a></h5>
									<div class="view hidden-xs">
										<div class="viewcount"><i class="fa fa-clock-o"></i>&nbsp;<?php echo @$row["news_viewcount"] ?> ครั้ง</div>
									</div>
								</div>
							</div>
							<?php } ?>
							<div class="row">
								<div class="col-sm-12 text-right">
									<a href="?">ดูทั้งหมด</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<script>
		  window.fbAsyncInit = function() {
			FB.init({
			  appId      : 'your-app-id',
			  xfbml      : true,
			  version    : 'v2.5'
			});
		  };

		  (function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/en_US/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>
	<?php
		
		
		$content = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		if(empty($firstitem)){
			$sc_img_url = "http://{$_SERVER["SERVER_NAME"]}/images/FB.png" ;
		}else{
			$sc_img_url = "http://{$_SERVER["SERVER_NAME"]}/timthumb.php?src=/uploads/contents/{$firstitem["nitem_path"]}&w=470&h=246" ;
		}
		 
		
		?>
		<meta name="description" content="<?php echo @htmlspecialchars(@$news["news_title"]) ; ?>" />
		<meta name="keywords" content="<?php echo  @htmlspecialchars(@$news["news_title"]) ; ?>" />
		
		<meta name="author" content="<?php echo @AUTHOR; ?>" />
		
		<meta property="og:site_name" content="สหกรณ์ออมทรัพย์ครูเชียงใหม่ จำกัด"/>
		<meta property="og:title" content="<?php echo htmlspecialchars(@$news["news_title"]) ; ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:locale" content="th_TH">
		<meta property="og:url" content="http://<?php echo $_SERVER["SERVER_NAME"] ?>/news.php?nid=<?php echo @$_GET["nid"]; ?>" />
		
		
		<meta property="article:author" content="http://facebook.com/profile.php?id=506382382852508" />
		<meta property="article:publisher" content="http://facebook.com/profile.php?id=506382382852508" /> 
		
		<meta property="og:image" content="<?php echo $sc_img_url ;  ?>" />
		<meta name="image" content="<?php echo $sc_img_url ;  ?>"  />
		<link rel="image_src" type="image/png" href="<?php echo $sc_img_url ;  ?>" />

		
	<?php
		$headtag = ob_get_contents();
		ob_end_clean();
		$smarty->assign("headtag", $headtag);
	}
	else
	{
		$where = "" ;
		$type_id = 0 ; 
 		if(!empty($_GET["type"]))
		{
			$type_id = (int) @$_GET["type"] ; 
			$sql = "SELECT * FROM cmp_news_type WHERE type_id = {$type_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ; 
			$type = $rs->fetch_assoc();
			$where .= "AND cmp_news.news_type_id = {$type_id} " ; 
			
			$smarty->assign("subject", $type["type_title"]);
			$smarty->assign("breadcrumb", array("{$type["type_title"]}" => "/news.php?cat={$type["type_id"]}" ));
			$smarty->assign("title",$type["type_title"]) ;
		}else
		{
			$smarty->assign("subject", "ข่าวทั้งหมด");
			$smarty->assign("breadcrumb", array("ข่าวทั้งหมด" => "#" ));
			$smarty->assign("title","ข่าวทั้งหมด") ;
			
		}
			

		ob_start();
		?>
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row">
			<div class="col-md-9" style="top: 40px;">
				<div class="news-list"> 
				<?php 
					$sql = "SELECT COUNT(news_id) as _c 
											FROM cmp_news LEFT JOIN cmp_admin ON cmp_news.admin_id = cmp_admin.admin_id 
											WHERE cmp_news.news_status = 1 
												AND cmp_news.publicdate < NOW() 
												{$where} " ;
					$rs = $mysqli->query($sql);
					$count = $rs->fetch_assoc();
					$num_rows = $count["_c"] ; 
					$per_page = 10;   // Per Page
					$page = @$_GET["page"];
					if(!@$_GET["page"]){
						$page=1;
					}
					$prev_page = $page -1;
					$next_page = $page+1;
					$page_start = (($per_page*$page)-$per_page);
					if($num_rows<=$per_page){
						$num_pages =1;
					}else if(($num_rows % $per_page)==0){
						$num_pages =($num_rows/$per_page) ;
					}else{
						$num_pages =($num_rows/$per_page)+1;
						$num_pages = (int)$num_pages;
					}
					$paging = "" ; 
					for($p=1; $p <= $num_pages; $p++){
						if($p != $page){
							$paging  .=  "<li><a href=\"?type={$type_id}&page={$p}\">{$p}</a></li>";
						}else{
							$paging .= "<li class=\"active\"><a href=\"#\">{$p}</a></li>";
						}
					}
							
							
					$sql = "SELECT cmp_news.* , cmp_admin.admin_name 
									FROM cmp_news LEFT JOIN cmp_admin ON cmp_news.admin_id = cmp_admin.admin_id 
									WHERE cmp_news.news_status = 1 
										AND cmp_news.publicdate < NOW() 
										{$where} 
									ORDER BY cmp_news.publicdate DESC
									LIMIT {$page_start} , {$per_page}" ; 
					$rs = $mysqli->query($sql);
					echo $mysqli->error ;
					while($row = $rs->fetch_assoc()){
				?>		
					<div class="row item">
						<div class="col-sm-4">
							<div class="image">
								<?php if(empty($row["news_picture"])){  ?>
								<img class="img-fluid" src="/timthumb.php?src=/images/blank.png&w=640&h=480" />
								<?php }else{ ?>
								<img class="img-fluid" src="/timthumb.php?src=/uploads/contents/<?php echo $row["news_picture"] ?>&w=640&h=480" />
								<?php } ?>
							</div>
						</div>
						<div class="col-sm-8 group-detail">
							<div class="auther">โดย <?php echo $row["admin_name"] ?> - <i class="fa fa-clock-o"></i>&nbsp;วันที่ <?php echo mysqldate2thaidate(@$row["publicdate"] , "short" , true ) ?> </div>
							<h2 class="title"><a href="/news/<?php echo $row["news_id"] ?>"><?php echo $row["news_title"] ?></a></h2>
							<div class="detail">
								<p>
									<?php 	
										$news_detail = htmlspecialchars_decode($row["news_detail"] , ENT_QUOTES ) ;
										$news_detail = strip_tags($news_detail);
										echo html2text($news_detail , 100 ) ; 
									?>
								</p>
							</div>
						</div>
					</div>
				<?php 
					}
				?>
				<div class="">
					  <ul class="pagination">
					  <?php echo $paging ?>
					  </ul>
				</div>
				</div>
  			</div>
			<div class="col-md-3">
				<div class="right-panel" style="margin-bottom: 0px;">
 				</div>
				<div class="right-panel" >
					<h4 style="margin-top: 0px;">ข่าวล่าสุด</h4>
					<div class="right-album" >
						<div class="row">
						   <?php
							$sql = "SELECT * FROM cmp_news LEFT JOIN cmp_admin ON cmp_news.admin_id = cmp_admin.admin_id 
								WHERE cmp_news.news_status = 1 AND  cmp_news.publicdate < NOW()  ORDER BY cmp_news.publicdate DESC LIMIT 0 , 5 ";
							$rs = $mysqli->query($sql);
							echo $mysqli->error;
							while ($row = $rs->fetch_assoc()) {
								?>
								<div class="col-md-12" >
									<div class="item" style="" >
										<div class="item-image">
											<a target="" href="/news/<?php echo @$row["news_id"] ?>">
												<?php if(empty($row["news_picture"])){ ?>
													<img class="img-fluid" style="margin:auto" src="/timthumb.php?src=/images/blank.png&w=665&h=400" />
												<?php }else{ ?>
													<img class="img-fluid" style="margin:auto"  src="/timthumb.php?src=/uploads/contents/<?php echo $row["news_picture"]  ?>&w=665&h=400 " />
												<?php }?>
											</a>
										</div>
										<div class="item-detail" style="height: 70px;">
											<h4 class="title"><a target="" href="/news/<?php echo @$row["news_id"] ?>" ><?php echo @$row["news_title"] ?></a></h4>
										</div>
									</div>
								</div>
							<?php } ?>
							
						</div>
					</div>
				</div>
				 
			</div>
		</div>
	</div>
</section>
	<?php
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");