<?php
	include "config.inc.php";
	include "dashboard.session.inc.php";
	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	// if($_POST) exit();
	$do = (isset($_POST['do'])) ? $_POST['do'] : '';

	if($do == 'add_loan_education'){
        $member_no = $mysqli->real_escape_string($_POST['member_no']);
        $memname = $mysqli->real_escape_string($_POST['memname']);
        $position = $mysqli->real_escape_string($_POST['position']);
        $year = $mysqli->real_escape_string($_POST['year']);
        $level = $mysqli->real_escape_string($_POST['level']);
        $loan = $mysqli->real_escape_string($_POST['loan']);
        $interest = $mysqli->real_escape_string($_POST['interest']);
        $result = $mysqli->real_escape_string($_POST['result']);

        $sql = "INSERT INTO cmp_loan_education (member_no,memname,membtype_desc,year,level,loan,interest,permonth)
                    VALUES ($member_no,'$memname','$position','$year','$level','$loan','$interest','$result')";
        echo ($mysqli->query($sql)) ? 'true' : 'false'; exit();
    }
	if($do == 'add_loan_career'){
		$member_no = $mysqli->real_escape_string($_POST['member_no']);
        $memname = $mysqli->real_escape_string($_POST['memname']);
        $position = $mysqli->real_escape_string($_POST['position']);
        $year = $mysqli->real_escape_string($_POST['year']);
        $file_id = $mysqli->real_escape_string($_POST['file_id']);
        $loan = $mysqli->real_escape_string($_POST['loan']);
        $interest = $mysqli->real_escape_string($_POST['interest']);
        $result = $mysqli->real_escape_string($_POST['result']);

        $sql = "INSERT INTO cmp_loan_career (member_no,memname,membtype_desc,year,file_id,loan,interest,result)
                    VALUES ($member_no,'$memname','$position','$year','$file_id','$loan','$interest','$result')";
        echo ($mysqli->query($sql)) ? 'true' : 'false'; exit();
	}

	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
	<link rel="stylesheet" href="/html/frontend/css/dashboard.css"/>
	<script>
		$('document').ready(function(){
			$("body").on("keydown", ".number-only", function (e) {
	            var charCode = (e.charCode) ? e.charCode : ((e.which) ? e.which : e.keyCode);
	            if($.inArray(charCode, [110, 190]) != -1 && $(this).val().indexOf(".") != -1) return false;
	            if( $.inArray(charCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||  // Allow: backspace, delete, tab, escape, enter and .
	                (charCode == 65 && e.ctrlKey === true) ||		             // Allow: Ctrl+A
	                (charCode == 67 && e.ctrlKey === true) ||		             // Allow: Ctrl+C
	                (charCode == 88 && e.ctrlKey === true) ||		             // Allow: Ctrl+X
	                (charCode == 86 && e.ctrlKey === true) ||		             // Allow: Ctrl+V
	                (charCode == 82 && e.ctrlKey === true) ||		             // Allow: Ctrl+R
	                (charCode == 116 && e.ctrlKey === true) ||	                 // Allow: Ctrl+F5
	                (charCode >= 35 && charCode <= 39) 					         // Allow: home, end, left, right
	            ){ return; }

	            // Ensure that it is a number and stop the keypress
	            if((e.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)){ e.preventDefault(); }
	        });
		});

		function returnInt(num){
			return (isNaN(num) ? 0 : (isFinite(num) ? num : 0));
		}
	</script>
	<script src="/scripts/inputmask/jquery.inputmask.bundle.min.js"></script>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);

	include "dashboard.subject.inc.php";
	ob_start();
?>
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row">
				<div class="col-md-12">
					<h4 class="hidden-print">&nbsp;</h4>
					<span>ประเภทสัญญา</span>&nbsp;
					<div class="dropdown inline-block">
						<?php
							$index = (isset($_GET['type'])) ? $mysqli->real_escape_string($_GET['type']) : 1;
							$types = array(1 => 'เงินกู้สามัญ','เงินกู้พิเศษ');
						?>
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<?php echo $types[$index]; ?>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<?php foreach($types as $i => $type){ ?>
								<li><a href="?type=<?php echo $i; ?>"><?php echo $type; ?></a></li>
							<?php } ?>
						</ul>
					</div>
					<br><br>
					<?php
						$member_no = $_COOKIE["member_no"];
						$type = isset($_GET['type']) ? $_GET['type'] : '';
						switch ($type) {
							case '2': include "dashboard.include.calculate2.php"; break; // เงินกู้พิเศษ
							default:  include "dashboard.include.calculate.php";  break; // เงินกู้สามัญ
						}
					?>
				</div>
		</div>
	</div>
</section>
<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
