<?php
	include "config.inc.php";

	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />

<style>
.border_style {
    /*background-color: #e6602c;*/
    width: 100%;
    height: 80px;
    vertical-align: middle;
    margin-top: 5px;
    color: #000000;
    font-size: 18px;
    padding: 10px 5px 10px 5px;
    text-align: center;
}

.img-shadow{
    box-shadow: 2px 2px 8px 3px rgba(10, 10, 10, 0.49);
}

.img-fix {
 margin-top: 10px;
}

.img-fix-list {
 margin-top: -85px;
}

</style>

<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scrip_include", $scrip_include);
	ob_start();
?>
<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	$content = "" ;

    $cid = isset($_GET["cid"]) ? $_GET["cid"] : "" ;
    $where = !empty($cid) ? " OR department_id = '{$cid}' " : "" ;
    $where1 = !empty($cid) ? " WHERE department_id = '{$cid}' " : "" ;
    $where2 = !empty($cid) ? " WHERE department_id = '{$cid}' OR parent_id = {$cid}" : " WHERE department_id NOT IN('1','2','3','4','5') " ;
		$where2 = !empty($cid) ? " WHERE parent_id = {$cid}" : " WHERE department_id NOT IN('0') " ;

    $sql_head = "SELECT * FROM cmp_departmentoff {$where2} ORDER BY department_seq";
	$rs = $mysqli->query($sql_head);
	$row = $rs->fetch_assoc();

	//echo '<pre>'; print_r($row); echo '<pre>';
    $i =  1;
		if(!empty($cid)){
			$sql_head2 = "SELECT * FROM cmp_departmentoff WHERE department_id = '{$cid}'";
			$rs2 = $mysqli->query($sql_head2);
			$row2 = $rs2->fetch_assoc();
			$text_title = $row2['department_name'];
		}else{
	    $text_title = $row['department_name'];
		}
	$smarty->assign("title", "เจ้าหน้าที่" );
 	$smarty->assign("subject", "เจ้าหน้าที่" );
	$smarty->assign("breadcrumb", array("เจ้าหน้าที่" => "#" ));
	ob_start();

	$sql = "SELECT COUNT(staff_id) AS C FROM staff_off {$where1}" ;
	$stmt = $mysqli->query($sql);
	$count = $stmt->fetch_assoc();

	$num_rows = $count["C"] ;
	$per_page = 5;   // Per Page
	$page = @$_GET["page"];
	if(!@$_GET["page"]){
		$page=1;
	}
	$prev_page = $page -1;
	$next_page = $page+1;
	$page_start = (($per_page*$page)-$per_page);
	if($num_rows<=$per_page){
		$num_pages =1;
	}else if(($num_rows % $per_page)==0){
		$num_pages =($num_rows/$per_page) ;
	}else{
		$num_pages =($num_rows/$per_page)+1;
		$num_pages = (int)$num_pages;
	}
	$paging = "" ;
	for($p=1; $p <= $num_pages; $p++){
		if($p != $page){
			$paging  .=  "<li><a href=\"?cid={$cid}&page={$p}\">{$p}</a></li>";
		}else{
			$paging .= "<li class=\"active\"><a href=\"#\">{$p}</a></li>";
		}
	}
	$sql_head = "SELECT * FROM cmp_departmentoff {$where2} ORDER BY department_seq";
	$rs = $mysqli->query($sql_head);
	$i =  1;
	//echo $sql_head;
	?>
    <?php
    while($row = $rs->fetch_assoc()){
		$department_id=$row['department_id'];
        $sql = " SELECT * FROM staff_off WHERE department_id = {$row['department_id']} ORDER BY staff_seq ASC";
		$rs1 = $mysqli->query($sql);
        $i =  1;
        ?>
		<div class="row">
            <div class="col-md-12">
                <h2 class="page-title"><?php echo $row['department_name'] ?></h2>
                <hr style="border:1px dashed; color:#0094d9;">
            </div>
        </div>
		<div class="row">
            <div class="col-md-1"></div>
			<div class="col-md-12 text-center">
				<div class="page-detail">
                    <div class="row">
                    <?php
                        $i = 1;
                        $style = '';


                        while($r = $rs1->fetch_assoc()){
                        	// echo $department_id;
                    ?>
                            <div class="<?php echo $n_col;?> text-center">
                            	<?php
                            		if($i==1 && $department_id==1  ){

                            	?>
                            	    <div class="col-md-6 text-center "style="padding-left: 100px;">
									
										
                            	<?php
                            		}elseif ($i==2 && $department_id==1 ) {
                            	?>

									 <div class="col-md-6 text-center "style="padding-right: 100px;">
									
                            	<?php
                            		}else {
                            	?>
                            	    <div class="col-md-4 ">
                            		<?php
                            		}
                            	?>
                                    <?php if(empty($r["staff_image"] )) {  ?>
                                        <img src="/timthumb.php?src=/uploads/staff/default.png&h=150&w=100" class="img-shadow img-fix"/>
                                    <?php } else { ?>
                                        <img src="/timthumb.php?src=/uploads/staff/<?php echo $r["staff_image"] ?>&h=200&w=150" class="img-shadow img-fix" />
                                    <?php } ?>
                                    <?php if($department_id == 3 ){?>
                                    <h3 class="title border_style" align="left" style="padding: 10px 15px 10px 15px;">
                                    <?php }else{?>
                                    <h3 class="title border_style" align="left" style="<?php echo $style;?>">
                                    <?php }?>
                                        <span style="font-weight: bold;"> <?php echo $r['staff_name'];?> </span> <br>
                                        <span style="color: #000000;"> <?php echo $r['position_name'];?> </span>
                                    </h3>
                                </div>
                            </div>
                        <?php $i++; } ?>
					<?php //} ?>
				    </div>
				</div>
 			</div>
            <div class="col-md-1"></div>
		</div>
        <?php } ?>
	<?php
		$content = ob_get_contents();
		ob_end_clean();
		$smarty->assign("content", $content);
		$smarty->display(THEME.".tpl");
