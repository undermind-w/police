﻿CREATE TABLE `log_transaction` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`loan_transaction_id` INT(11) NULL DEFAULT NULL,
	`loan_id` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`loan_amount_balance` DOUBLE(22,0) NULL DEFAULT NULL COMMENT 'ยอดคงเหลือของเงินกู้',
	`receipt_id` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`createdatetime` DATETIME NULL DEFAULT NULL COMMENT 'วันที่บันทึกรายการ',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1615;