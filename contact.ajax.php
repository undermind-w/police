<?php 

	//session_start();
	require "config.inc.php";
 
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

    if(!empty($_POST["ajax"])) {
        $member_no = @$mysqli->real_escape_string(@$_POST["code"]) ;

        if(empty($member_no)){
            //echo "<script>alert('กรุณากรอกหมายเลขสมาชิก'); window.location.href = 'index.php' ; </script>" ;
            // exit();
        }

        $sql = "SELECT member_no FROM cmp_imp_member WHERE member_no = '{$member_no}' LIMIT 0 , 1  " ;
        $rs = $mysqli->query($sql);
        echo $mysqli->error ;
        $m1 = $rs->fetch_assoc();

        if(empty($m1)){
            //echo "<script>alert('กรุณากรอกหมายเลขสมาชิก ที่ถูกต้อง '); window.location.href = 'index.php' ; </script>" ;
            //exit();
        }


        $contact_title = "ร้องเรียน/แจ้งข้อเสนอแนะ" ;
        $contact_detail = @$mysqli->real_escape_string(@$_POST["detail"]);
        $contact_name = @$mysqli->real_escape_string(@$_POST["name"]);
        $contact_tel = @$mysqli->real_escape_string(@$_POST["tel"]);

        $contact_email  = @$mysqli->real_escape_string(@$_POST["email"]);
        $ip =  @$mysqli->real_escape_string(@$_SERVER["REMOTE_ADDR"]);

        $sql = "INSERT INTO cmp_contact ( 
										member_no
										, contact_title 
										, contact_name 
										, contact_tel
										, contact_email 
										, contact_detail 
										, createdate
										, updatedate 
										, ip
										) 
										VALUES 
										(
										'{$member_no}'
										, '{$contact_title}'
										, '{$contact_name}' 
										, '{$contact_tel}' 
										, '{$contact_email}'
										, '{$contact_detail}'
										, NOW() 
										, NOW() 
										, '{$ip}'
										)" ;
        $mysqli->query($sql);
        echo $mysqli->error ;

        $sql = "SELECT * FROM cmp_website WHERE st_id = 1 LIMIT 0 , 1 " ;
        $rs = $mysqli->query($sql);
        $email = $rs->fetch_assoc();

        $smarty->assign("server_name", @$_SERVER["SERVER_NAME"] );
        $smarty->assign("contact_name", @htmlspecialchars($contact_name) );
        $smarty->assign("contact_detail", @htmlspecialchars( @nl2br($contact_detail) ));
        $html = @$smarty->fetch("email.tpl");

		@send_mj_mail("มีผู้ร้องเรียน/แจ้งข้อเสนอแนะ เข้ามาในระบบ" , $html , @$email["ST_EMAIL_SEND"] );

		exit();
	}
?>