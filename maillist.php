<?php
	include "config.inc.php";
	
	ob_start();
?>
<meta name="description" content="<?php echo DESC; ?>" />
<meta name="keywords" content="<?php echo KEYWORD; ?>" />
<meta name="author" content="<?php echo AUTHOR; ?>" />
<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>
<?php
	$scrip_include = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scrip_include);
	$smarty->assign("subject", "CMCOOP Online");
	$smarty->assign("breadcrumb", array("จดหมายถึงท่าน" => null ));
	ob_start();
	
	$id = isset($_GET["id"]) ? $_GET["id"] : null ;
	
	if($id != null) {
			$sql = "SELECT * FROM cmp_mailing_list WHERE list_id = '{$id}'";
			$rs = $mysqli->query($sql);
			$row =$rs->fetch_assoc();
			echo "
							<div class='panel panel-default'>
									<div class='panel-heading'><h4>{$row["list_title"]}</h4></div>
									<div class='panel-body'>{$row["list_detail"]}</div>
							</div>
			";
	}
?>

<?php
	$content = ob_get_contents();
	ob_end_clean();
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");