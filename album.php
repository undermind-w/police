<?php
	include "config.inc.php";
	
	
	$url = "http://{$_SERVER["SERVER_NAME"]}{$_SERVER["REQUEST_URI"]}";
	
	if(@$_POST["do"] == "get-fb") {
		echo @getFacebooks(@$_POST["url"]);
		exit();
	}
	if(@$_POST["do"] == "get-tw") {
		echo @getTweets(@$_POST["url"]);
		exit();
	}
	if(@$_POST["do"] == "get-gp") {
		echo @getPlus1(@$_POST["url"]);
		exit();
	}
	
	ob_start();
?>
<meta name="description" content="<?php echo @DESC; ?>" />
<meta name="keywords" content="<?php echo @KEYWORD; ?>" />
<meta name="author" content="<?php echo @AUTHOR; ?>" />
<style>	

</style>


<?php
	$headtag = ob_get_contents();
	ob_end_clean();
	$smarty->assign("headtag", $headtag);
	ob_start();
?>	
	<script>
		$(function(){
			/** fancybox **/
			$(".fancybox").fancybox();
			$(".various").fancybox({
				maxWidth	: 800,
				maxHeight	: 600,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
 
		});
	</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();
?>
<?php
	$jquery = ob_get_contents();
	ob_end_clean();
	$smarty->assign("jquery", $jquery);
	$content = "" ; 
	
	if(!empty($_GET["aid"]))
	{
		$album_id = (int) @$_GET["aid"] ; 
	
		$sql = "UPDATE cmp_album SET album_viewcount = album_viewcount + 1 WHERE album_id = '{$album_id}' " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		 
		
		$sql = "SELECT cmp_album.* , cmp_admin.admin_name FROM cmp_album LEFT JOIN cmp_admin ON cmp_album.admin_id = cmp_admin.admin_id  
						WHERE cmp_album.album_status = 1 
							AND cmp_album.publicdate < NOW() 
							AND cmp_album.album_id = '{$album_id}'  
							ORDER BY cmp_album.publicdate DESC 
							LIMIT 0 , 1  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$album = $rs->fetch_assoc();
		if(empty($album))
		{
			header('Location: index.php');
			exit() ; 
		}
		 
	
		$smarty->assign("subject", "อัลบั้มภาพ");
		$smarty->assign("breadcrumb", array("อัลบั้มภาพ" => "album.php" ));
		$smarty->assign("title", htmlspecialchars($album["album_title"])) ;
		
		ob_start();
	?>
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row">
			<div class="col-md-12">
				<h2 class="album-title"><?php echo @htmlspecialchars(@$album["album_title"]) ?></h2>
				<div class="album-header-line">
					<div class="item" >โดย&nbsp;<?php echo @$album["admin_name"] ?></div>
 					<div class="item" ><i class="fa fa-clock-o"></i>&nbsp;วันที่ <?php echo mysqldate2thaidate(@$album["publicdate"] , "short" , true ) ?></div>
 					<div class="item" ><i class="fa fa-eye"></i>&nbsp;<?php echo @$album["album_viewcount"] ?></div>
				</div>
				<div class="album-header-detail">
				<?php echo @htmlspecialchars_decode(@$album["album_detail"] , ENT_QUOTES ) ;  ?>
				<br />
				<hr />
				</div>
				<div class="">
					<div class="row">
						<div class="col-md-12">
							<div class="album-picture-item">
							<?php 
								if($album["album_type"] == "picture"){
									$sql = "SELECT * FROM cmp_album_item WHERE aitem_status = 1  AND album_id = '{$album["album_id"]}' AND aitem_type = 'images'  ORDER BY createdate DESC " ;
									$rs = $mysqli->query($sql);
									echo $mysqli->error ;
									while($item = $rs->fetch_assoc()){
								?>
									<div class="item" >
										<div style="padding:10px">
										<a target="_blank" href="/uploads/contents/<?php echo @$item["aitem_path"]  ?>" class="fancybox" rel="group1" >
											<img class="img-responsive img-thumbnail" src="/timthumb.php?src=/uploads/contents/<?php echo @$item["aitem_path"]  ?>&w=320&h=320" /> 
										</a>
										</div>
									</div>
								<?php 
									} 									
								}elseif($album["album_type"] == "video"){
									$sql = "SELECT * FROM cmp_album_item WHERE aitem_status = 1  AND album_id = '{$album["album_id"]}' AND aitem_type = 'video' ORDER BY createdate DESC " ;
									$rs = $mysqli->query($sql);
									echo $mysqli->error ;
									
 
									while($item = $rs->fetch_assoc()){
										$code = @url2youtucode($item["aitem_path"]); 
								?>
									<div class="item" >
										<div style="padding:10px">
											<a target="_blank" href="http://www.youtube.com/embed/<?php echo $code ?>" class="fancybox.iframe various" rel="group2" >
												<img class="img-responsive img-thumbnail" src="http://img.youtube.com/vi/<?php echo $code ?>/hqdefault.jpg" /> 
											</a>
										</div>
									</div>
								<?php 
									}
									
								}elseif($album["album_type"] == "sound"){
									
									$sql = "SELECT * FROM cmp_album_item WHERE aitem_status = 1  AND album_id = '{$album["album_id"]}' AND aitem_type = 'sound' ORDER BY createdate DESC " ;
									$rs = $mysqli->query($sql);
									echo $mysqli->error ;
									
 
									while($item = $rs->fetch_assoc()){
 								?>
									<div class="item-2 text-center" >
										<?php echo @$item["aitem_title"] ?>
										<div style="padding:10px;height:60px">
											<audio controls style="height:100%;width:100%">
												<source src="/uploads/album/sound/<?php echo $item["aitem_path"] ?>" type="audio/mpeg">
												Your browser does not support the audio element.
											</audio>
										</div>
									</div>
								<?php 
									}
									
								}
								
								
						
							
							?>
								<div style="clear:both;"></div>
							</div>
						</div>
					</div>
				</div>
 			</div>
			 
		</div>
	</div>
</section>
	<?php
		
		
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$where = "" ;
		$smarty->assign("subject", "อัลบั้มภาพ");
		$smarty->assign("breadcrumb", array("อัลบั้มภาพ" => "album.php" ));
		$smarty->assign("title", "อัลบั้มภาพ" ) ;
 
		ob_start();
		?>
<section class="section-main content-page" >	 
	<div class="container" >
		<div class="row">
			<div class="col-md-12">
				<div class="album-list"> 
				<div class="row">
				<?php 
					$sql = "SELECT COUNT(album_id) as _c 
											FROM cmp_album LEFT JOIN cmp_admin ON cmp_album.admin_id = cmp_admin.admin_id 
											WHERE cmp_album.album_status = 1 
												AND cmp_album.publicdate < NOW() 
												{$where} " ;
					$rs = $mysqli->query($sql);
					$count = $rs->fetch_assoc();
					$num_rows = $count["_c"] ; 
					$per_page = 12 ;   // Per Page
					$page = @$_GET["page"];
					if(!@$_GET["page"]){
						$page=1;
					}
					$prev_page = $page -1;
					$next_page = $page+1;
					$page_start = (($per_page*$page)-$per_page);
					if($num_rows<=$per_page){
						$num_pages =1;
					}else if(($num_rows % $per_page)==0){
						$num_pages =($num_rows/$per_page) ;
					}else{
						$num_pages =($num_rows/$per_page)+1;
						$num_pages = (int)$num_pages;
					}
					$paging = "" ; 
					for($p=1; $p <= $num_pages; $p++){
						if($p != $page){
							$paging  .=  "<li><a href=\"?page={$p}\">{$p}</a></li>";
						}else{
							$paging .= "<li class=\"active\"><a href=\"#\">{$p}</a></li>";
						}
					}
							
							
					$sql = "SELECT cmp_album.* 
									, cmp_admin.admin_name 
									,  (SELECT cmp_album_item.aitem_path FROM cmp_album_item WHERE cmp_album_item.album_id = cmp_album.album_id AND cmp_album_item.aitem_type = 'images' LIMIT 0 , 1 ) as album_cover
									FROM cmp_album LEFT JOIN cmp_admin ON cmp_album.admin_id = cmp_admin.admin_id 
									WHERE cmp_album.album_status = 1 
										AND cmp_album.publicdate < NOW() 
 									ORDER BY cmp_album.publicdate DESC
									LIMIT {$page_start} , {$per_page}" ; 
					$rs = $mysqli->query($sql);
					echo $mysqli->error ;
					while($row = $rs->fetch_assoc()){
				?>		
					
						<div class="col-sm-4" style="padding:10px">
							<div class="item" >
								<div class="image">
									<a href="?aid=<?php echo $row["album_id"] ?>">
										<?php if(empty($row["album_cover"])){  ?>
										<img class="img-responsive" src="/timthumb.php?src=/images/blank.png&w=400&h=400" />
										<?php }else{ ?>
										<img class="img-responsive" src="/timthumb.php?src=/uploads/contents/<?php echo $row["album_cover"] ?>&w=400&h=400" />
										<?php } ?>
									</a>
								</div>
								<div class="title">
									<a href="?aid=<?php echo $row["album_id"] ?>"><?php echo htmlspecialchars($row["album_title"]) ?></a>
								</div>
							</div>
						</div>
				<?php 
					}
				?>
				</div>
				<div class="">
					  <ul class="pagination">
					  <?php echo $paging ?>
					  </ul>
				</div>
				</div>
  			</div>
		</div>
	</div>
</section>
	<?php
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");