var ub_confirm_obj;
var ub_confirm_callback;
var ub_confirm;

$(function(){
	ub_confirm = function(obj, title, msg, callback, ok_text, cancel_text) {
		ok_text = ok_text || $("#modal_confirm_btn_ok").data("text");
		cancel_text = cancel_text || $("#modal_confirm_btn_cancel").data("text");
		ub_confirm_obj = obj;
		ub_confirm_callback = callback;
		$("#modal_confirm_btn_ok").html(ok_text);
		$("#modal_confirm_btn_cancel").html(cancel_text);
		$("#modal_confirm").find(".modal-title").html(title);
		$("#modal_confirm").find(".modal-body").html(msg);
		$("#modal_confirm").modal("show");
		return false;
	}
	
	$("#modal_confirm_btn_ok").click(function() {
		ub_confirm_callback();
	});
});

function ub_chk_status(status) {
	if(status == -1) {
		location.reload(true);
		return false;
	}
	else {
		return true;
	}
}

function calculateAge(birthday) { // birthday is a date
	var ageDifMs = Date.now() - birthday;
	var ageDate = new Date(ageDifMs); // miliseconds from epoch
	return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}