var ub_form_valid;

$(function(){
	ub_form_valid = function(o) {
		var is_error = false;
		var old_name = "";
		
		$(".has-error").removeClass("has-error");
		$(".ub_error_msg").remove();
		o.find("[data-rule-required=true]").each(function(index) {
			if($(this).is(":visible")) {
				var is_error2 = false;
				if($(this).prop("type") == "checkbox" || $(this).prop("type") == "radio") {
					var chk_error = true;
					$("input[name='" + $(this).prop("name") + "']").each(function(index2) {
						if($(this).prop("checked")) {
							chk_error = false;
						}
					});
					is_error2 = chk_error;
				}
				else {
					if($(this).val() == "") {
						is_error2 = true;
					}
				}
				
				if(is_error2 && old_name != $(this).prop("name")) {
					is_error = true;
					var wrap = $(this).prop("type") == "checkbox" || $(this).prop("type") == "radio" ? $(this).parent().parent() : $(this).parent();
					wrap.addClass("has-error");
					$(this).next().find(".select2-selection").parent().addClass("has-error");
					wrap.append('<div class="ub_error_msg has-error help-block">' + $(this).data("msg-required") + '</div>');
				}
				
				old_name = $(this).prop("name");
			}
		});
		
		return !is_error;
	};
	
	$("form[data-toggle=validator]").submit(function(e) {
		if(!ub_form_valid($(this))) {
			e.preventDefault();
		}
	});
});