var gp_login;

$(document).ready(function() {
	(function() {
    var po = document.createElement('script');
		po.type = 'text/javascript';
		po.async = true;
    //po.src = 'https://apis.google.com/js/plusone.js?onload=render';
    //po.src = 'https://apis.google.com/js/client.js?onload=handleClientLoad';
    po.src = 'https://apis.google.com/js/client:platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();

	var clientId = '273950501410-8uekrnk4aivqb00ae2bd9ps117fds5nu.apps.googleusercontent.com';
	//var apiKey = 'AIzaSyAStIK5H7eN8YNrnozG-AFN8d0mBq8B6JE';
	var scopes = 'https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile';

	function gp_check_auth() {
		//gapi.client.setApiKey(apiKey);
		gapi.auth.authorize({
			client_id: clientId
			, scope: scopes
			, immediate: true
		} ,
		function(response) { //console.log(response);
				if (response && !response.error) { console.log("OK");
					gp_get_data();
				} else {
					gp_login();
				}
		});
	}
	function gp_get_data() {
		gapi.client.load('oauth2', 'v2', function() {
			gapi.client.oauth2.userinfo.get().execute(function(data) { //console.log(data);

				$.ajax({
					type: "POST"
					, url: "/login"
					, data: {
						"ajax" : "1"
						, "do" : "login"
						, "login_type" : "G"
						, "data" : data
						, _time : Math.random()
					}
					, async: false
					, success: function(response) { //console.log(response);
							response = $.parseJSON(response);
							document.location.href = response.url;
					}
				});


			})
		});
	}
	gp_login = function() {
		gapi.auth.signIn({
			'clientid' 				: clientId
			, 'cookiepolicy' 	: 'single_host_origin'
			, 'requestvisibleactions' : 'https://schema.org/CommentAction https://schema.org/ReviewAction'
			, 'scope' 				: 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
			//, 'approvalprompt': 'force'
			, 'callback'			: function(authResult) {
					if(authResult['status']['signed_in']) {
						gp_get_data();
					}
			}
		});
	}

	$("body").on("click", ".btn_login_google", gp_login);

});