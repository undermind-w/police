var fb_login;

$(document).ready(function() {
	var fb_app_id = "2514299952117807";
	
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
			FB.init({
				appId: fb_app_id
				, version: 'v5.0'
				, oauth: true
				, status: true
				, cookie: true
				, xfbml: true
			});
	});

	function fb_get_data() {
		FB.getLoginStatus(function(response) {
				if(response.authResponse) { //console.log(response);
						FB.api('/me?fields=id,email,first_name,last_name', function(data) { 
							console.log(data);
							$.ajax({
								type: "POST"
								, url: "/login"
								, data: {
									"ajax" : "1"
									, "do" : "login"
									, "login_type" : "F"
									, "data" : data
									, "_time" : Math.random()
								}
								, async: false
								, success: function(response) { //console.log(response);
										response = $.parseJSON(response);
										location.href = response.url;
								}
							});
						 });
				} else {
					fb_login(); //console.log('User cancelled login or did not fully authorize.');
				}
		} ,
		{
				scope: "public_profile,email"
				, auth_type: "rerequest"
				, return_scopes: true
		});
	}

	fb_login = function() {
		FB.login(function(response) {
				if (response.authResponse) {
						FB.api('/me', function(response) {
								fb_get_data();
						});
				} else {
						//console.log('User cancelled login or did not fully authorize.');
				}
		} ,
		{
				scope: "public_profile,email"
				, auth_type: "rerequest"
				, return_scopes: true
		});
	}

	$("body").on("click", ".btn_login_facebook", fb_login);

});