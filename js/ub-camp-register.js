$(function() {
	$(".btn_camp_register").click(function() {
		var id = $(this).data("id");
		
		$.ajax({
			type: "POST",
			url: "/ajaxcontrol",
			data: "do=get_camp_register&id=" + id,
			success: function(msg) {
				data = jQuery.parseJSON(msg);
				
				$("#modal_alert").find(".modal-header").css("display", "none");
				$("#modal_alert").find(".modal-body").html(data["html"]);
				$("#modal_alert").find(".modal-footer").css("display", "none");
				$("#modal_alert").modal("show");
				
				$("#btn_camp_register_ok").click(function() {
					location.href = data["url"];
				});
			}
		});
		
		return false;
	});
});