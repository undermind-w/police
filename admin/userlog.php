<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;

	$cmpper->check("userlog") ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');



	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<style>
		@media print {
		   .page-breadcrumb{display:none;}
		   .form-group-print-hide{display:none;}
		   .form-control{ border : 0 }
		   .pace {display:none}
		}

	</style>
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		function printExternal(url) {
			var printWindow = window.open( url, 'Print', 'left=200, top=200, width=950, height=500, toolbar=0, resizable=0');
			printWindow.addEventListener('load', function(){
				printWindow.print();
				printWindow.close();
			}, true);
		}

		function auto_grow() {
			var element = document.getElementById("contact_detail") ;
            element.style.height = "150px";
            element.style.height = (element.scrollHeight + 100 )+"px";
        }

		$("#frm_journal").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true ,
			format : "yyyy-mm-dd"
		});
		$(document).on("click" , "#btnPrint" , function(){
			window.print();
		});
		$(document).on("click" , ".btnPrintItem" , function(){
			//printExternal($(this).attr("href")) ;
		});
		auto_grow() ;
		<?php if(@$_GET["print"] == "1"  ){ ?>
		window.print();
		<?php } ?>

	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$sql = "SELECT COUNT(id) as _c FROM cmp_userlog " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;

	$count = $rs->fetch_assoc();
	$num_rows = $count["_c"] ;
	$per_page = 25 ;
	$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

	$pagination = (new Pagination());
	$pagination->setCurrent($page);
	$pagination->setRPP($per_page);
	$pagination->setTotal($num_rows);
	$pagination->setCrumbs(25);
	$paging = $pagination->parse();

	$page_start = (($per_page * $page) - $per_page ) ;




  $sql = "SELECT *
          FROM cmp_userlog
          ORDER BY createdate DESC
          LIMIT  {$page_start} , {$per_page}   " ;
					$rs = $mysqli->query($sql);
					echo $mysqli->error;


					$i =  1;



  while($item = $rs->fetch_assoc()){

$smarty->assign("title", "Log" );
$smarty->assign("page_title", "Log" );
$smarty->assign("breadcrumb", array("Log" => "?" ,  "{$btitle}" => "#"  ) );


$id = (int) @$row["id"]  ;


ob_start();

$i =  1;


?>

	<div class="row">
	<div class="col-md-10">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">Log</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
 			 <div style="clear: both;margin-bottom:15px"></div>

			<table class="table table-bordered">
				<thead>
					<tr>
						<!-- <th width="100" style="text-align:center" ></th> -->
						<th width="225" style="text-align:center" > วันที่ </th>
            <th width="100" style="text-align:center" >IP</th>
					</tr>
				</thead>
				<tbody>
					<?php

					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
						<!-- <td style="text-align:center" > -->
							<!-- <?php echo $i ++  ?> -->
						</td>
						<td><?php echo @mysqldate2thaidate(@$row["createdate"] , "long" , true )  ?></td>
						<td><?php echo @$row["log_ip"] ?></td>

                      </tr>
					<?php
						}



					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>

<?php
		$content = ob_get_contents();
		ob_end_clean();

	}



	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
