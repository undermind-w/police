<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
    require "menu.inc.php" ;

	$cmpper->check("setting") ;

	header("Content-Type:text/html;charset=utf-8");
    date_default_timezone_set('Asia/Bangkok');
    
    ob_start();
?>
    <meta name="keywords" content="<?php echo KEYWORD; ?>" />
    <meta name="description" content="<?php echo DESC; ?>" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

    ob_start();   
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>

<style>
</style>

<script>
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "ส่งข้อความถึงสมาชิก" );
	$smarty->assign("page_title", "ส่งข้อความถึงสมาชิก" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("ส่งข้อความถึงสมาชิก" => "#"));

	ob_start();

    require "system.connect.php";

?>

<?php
    if ($_GET['do'] == 'send'){
        require "message_send.php";
    } else if ($_GET['do'] == 'edit') {

    } else if ($_GET['do'] == 'delete') {
        $sql = "UPDATE mobile_message SET admin_delete = 1 WHERE msg_id = ".$_GET['msg'];
        if ( $mysqli_system->query($sql) === TRUE ) {
            echo "<script> window.location.href = \"/admin/mobile_message.php\"</script>";
            exit();
        } else {
            alert('ไม่สามารถดำเนินการได้ กรุณาลองใหม่อีกครั้ง');
        }
    }
?>

<?php
    $content = ob_get_contents();
    ob_end_clean();

    $smarty->assign("content", $content);
    $smarty->display(THEME.".tpl");
?>