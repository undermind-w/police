<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	
	if(@$_POST["ajax"]) {
		switch($_POST["do"]) {
			case "set_hide":
				$sql = "UPDATE cmp_webboard_topic SET topic_status = 0 WHERE topic_id = {$_POST["id"]}";
				$mysqli->query($sql);
				echo $mysqli->error;
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
			case "set_show":
				$sql = "UPDATE cmp_webboard_topic SET topic_status = 1 WHERE topic_id = {$_POST["id"]}";
				$mysqli->query($sql);
				echo $mysqli->error;
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
			case "set_unpin":
				$sql = "UPDATE cmp_webboard_topic SET topic_pin = 0 WHERE topic_id = {$_POST["id"]}";
				$mysqli->query($sql);
				echo $mysqli->error;
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
			case "set_pin":
				$sql = "UPDATE cmp_webboard_topic SET topic_pin = 1 WHERE topic_id = {$_POST["id"]}";
				$mysqli->query($sql);
				echo $mysqli->error;
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
		}
		exit;
	}
	
	if($_POST)
	{
 		$topic_id = (int) @$_POST["topic_id"] ; 
		
  		$topic_title = @$mysqli->real_escape_string(@$_POST["topic_title"]) ;
		$topic_detail = @$mysqli->real_escape_string(@$_POST["topic_detail"]) ;
		
		$topic_pin = (int) @$_POST["topic_pin"] ; 
  		$topic_status = (int) @$_POST["topic_status"] ; 
		
		
	 
		if(!empty($topic_id))
		{	
  			$sql = "UPDATE cmp_webboard_topic SET forum_id = '{$_POST["forum_id"]}'  
												, topic_title = '{$topic_title}'
												, topic_detail = '{$topic_detail}'
   												, updatedate = NOW() 
												, topic_pin = {$topic_pin}
												, topic_status = {$topic_status}
												WHERE 
												topic_id = {$topic_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("page"))  )
	{
		$topic_id = (int) @$_GET["pid"] ;  
 		$sql = "DELETE FROM cmp_webboard_topic WHERE topic_id = {$topic_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_forum").validate();
		
		$(document).on("click", ".status_show", function() {
			var status_show = $(this);
			var id = $(this).attr("data-id");
			
			$.ajax({
				type: "POST",
				url: "?",
				data: "ajax=1&do=set_hide&id=" + id,
				success: function(msg) {
					data = jQuery.parseJSON(msg);
					if(data["status"] == "TRUE") {
						status_show.removeClass("status_show");
						status_show.removeClass("btn-info");
						status_show.addClass("status_hide");
						status_show.addClass("btn-danger");
						status_show.html("ซ่อน");
					}
				}
			});
			return false;
		});
		
		$(document).on("click", ".status_hide", function() {
			var status_hide = $(this);
			var id = $(this).attr("data-id");
			
			$.ajax({
				type: "POST",
				url: "?",
				data: "ajax=1&do=set_show&id=" + id,
				success: function(msg) {
					data = jQuery.parseJSON(msg);
					if(data["status"] == "TRUE") {
						status_hide.removeClass("status_hide");
						status_hide.removeClass("btn-danger");
						status_hide.addClass("status_show");
						status_hide.addClass("btn-info");
						status_hide.html("แสดง");
					}
				}
			});
			return false;
		});
		
		$(document).on("click", ".status_pin", function() {
			var status_pin = $(this);
			var id = $(this).attr("data-id");
			
			$.ajax({
				type: "POST",
				url: "?",
				data: "ajax=1&do=set_unpin&id=" + id,
				success: function(msg) {
					data = jQuery.parseJSON(msg);
					if(data["status"] == "TRUE") {
						status_pin.removeClass("status_pin");
						status_pin.removeClass("btn-success");
						status_pin.addClass("status_unpin");
						status_pin.addClass("btn-warning");
						status_pin.html("ไม่ได้ปักหมุด");
					}
				}
			});
			return false;
		});
		
		$(document).on("click", ".status_unpin", function() {
			var status_unpin = $(this);
			var id = $(this).attr("data-id");
			
			$.ajax({
				type: "POST",
				url: "?",
				data: "ajax=1&do=set_pin&id=" + id,
				success: function(msg) {
					data = jQuery.parseJSON(msg);
					if(data["status"] == "TRUE") {
						status_unpin.removeClass("status_unpin");
						status_unpin.removeClass("btn-warning");
						status_unpin.addClass("status_pin");
						status_unpin.addClass("btn-success");
						status_unpin.html("ปักหมุด");
					}
				}
			});
			return false;
		});
	})
	
	if($("#topic_detail").length) {
		$("#topic_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
	}
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$topic_id = (int) @$_GET["pid"] ; 
			$sql = "SELECT * FROM cmp_webboard_topic WHERE topic_id = {$topic_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "กระดานถามตอบ - กระทู้ทั้งหมด" );
		$smarty->assign("forum_title", "กระดานถามตอบ - กระทู้ทั้งหมด" );
 		$smarty->assign("breadcrumb", array("กระดานถามตอบ - กระทู้ทั้งหมด" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$topic_id = (int) @$row["topic_id"]  ; 
		
		$topic_pin = (int) @$row["topic_pin"]  ;
		$topic_status = (int) @$row["topic_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">กระทู้ทั้งหมด</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_forum" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="topic_id" value="<?php echo $topic_id ;  ?>"/>
						
						<div class="form-group">
							<label for="forum_title" class="col-sm-3 control-label">หมวด</label>
							<div class="col-sm-9">
								<select class="form-control" id="forum_id" name="forum_id" required="required" style="width: auto;" >
									<option value="" disabled="" selected="">กรุณาเลือกหมวด</option>
									<?php
									$sql = "SELECT * FROM cmp_webboard_forum ORDER BY forum_seq";
									$rsType = $mysqli->query($sql);
									echo $mysqli->error;
									while($rowType = $rsType->fetch_assoc()) : ?>
										<option value="<?php echo $rowType["forum_id"] ?>" <?php echo $rowType["forum_id"] == $row["forum_id"] ? "selected" :"" ;  ?> ><?php echo $rowType["forum_title"] ?></option>
									<?php endwhile; ?>
								</select>
 							</div>
						</div>
						
						<div class="form-group">
							<label for="forum_title" class="col-sm-3 control-label">หัวข้อ</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="topic_title" name="topic_title" placeholder="" value="<?php echo @$row["topic_title"] ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="inr_percent" class="col-sm-3 control-label">รายละเอียด</label>
							<div class="col-sm-9">
								<textarea  class="form-control" id="topic_detail" name="topic_detail" required="required" ><?php echo @$row["topic_detail"] ?></textarea>
  							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="topic_pin" name="topic_pin" <?php echo $row["topic_pin"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  ปักหมุด
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="topic_status" name="topic_status" <?php echo $row["topic_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  แสดง
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "กระดานถามตอบ - กระทู้ทั้งหมด" );
		$smarty->assign("forum_title", "กระดานถามตอบ - กระทู้ทั้งหมด" );
		$smarty->assign("breadcrumb", array("กระดานถามตอบ - กระทู้ทั้งหมด" => "#") );
		
		ob_start();
		
		$where = "";
		switch(@$_GET["search_type"]) {
			case 1:
				$where = "AND cmp_webboard_topic.topic_title LIKE '%".@$_GET["keyword"]."%'";
				break;
			case 2:
				$where = "AND cmp_webboard_topic.topic_detail LIKE '%".@$_GET["keyword"]."%'";
				break;
			case 3:
				$where = "AND cmp_webboard_member.wmem_name LIKE '%".@$_GET["keyword"]."%'";
				break;
		}
		
		$sql = "SELECT COUNT(topic_id) as _c FROM cmp_webboard_topic INNER JOIN cmp_webboard_member ON cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id WHERE 1 ".$where ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT cmp_webboard_topic.*, cmp_webboard_forum.forum_title, cmp_webboard_member.wmem_name
						FROM  cmp_webboard_topic
							INNER JOIN cmp_webboard_forum ON cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id
							INNER JOIN cmp_webboard_member ON cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id
						WHERE 1 ".$where."
						ORDER BY cmp_webboard_topic.craetedate DESC
						LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows - ($per_page * ($page - 1 ) );
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">กระทู้ปักหมุด</h4>
		</div>
		<div class="panel-body">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
 						<th class="text-center" >หัวข้อ</th>
						<th class="text-center" >หมวด</th>
						<th class="text-center" >โดย</th>
						<th class="text-center" >วันที่โพส</th>
						<th class="text-center" >สถานะ</th>
  						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT cmp_webboard_topic.*, cmp_webboard_forum.forum_title, cmp_webboard_member.wmem_name
									FROM  cmp_webboard_topic
										INNER JOIN cmp_webboard_forum ON cmp_webboard_topic.forum_id = cmp_webboard_forum.forum_id
										INNER JOIN cmp_webboard_member ON cmp_webboard_topic.topic_wmem_id = cmp_webboard_member.wmem_id
									WHERE topic_pin = 1
									ORDER BY cmp_webboard_topic.craetedate DESC
									LIMIT {$page_start} , {$per_page}   " ;
					$rsPin = $mysqli->query($sql);
					echo $mysqli->error;
					$i2 = $rsPin->num_rows;
					while($rowPin = $rsPin->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i2--; ?></td>
						<td><a href="/board/topic/<?php echo @$rowPin["topic_id"] ?>" target="_blank"><?php echo @$rowPin["topic_title"] ?></a></td>
						<td><?php echo @$rowPin["forum_title"] ?></td>
						<td><?php echo @$rowPin["wmem_name"] ?></td>
						<td class="text-center"><?php echo mysqldate2thaidate(@$rowPin["craetedate"], "short", true) ?></td>
						<td class="text-center">
							<?php if(@$rowPin["topic_status"]) : ?>
								<span class="status_show btn btn-info btn-xs" data-id="<?php echo @$rowPin["topic_id"] ?>">แสดง</span>
							<?php else : ?>
								<span class="status_hide btn btn-danger btn-xs" data-id="<?php echo @$rowPin["topic_id"] ?>">ซ่อน</span>
							<?php endif; ?>
							
							<?php if(@$rowPin["topic_pin"]) : ?>
								<span class="status_pin btn btn-success btn-xs" data-id="<?php echo @$rowPin["topic_id"] ?>">ปักหมุด</span>
							<?php else : ?>
								<span class="status_unpin btn btn-warning btn-xs" data-id="<?php echo @$rowPin["topic_id"] ?>">ไม่ได้ปักหมุด</span>
							<?php endif; ?>
						</td>
  						<td style="text-align:center" >
							<a href="?in=page&do=edit&pid=<?php echo @$rowPin["topic_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=page&do=delete&pid=<?php echo @$rowPin["topic_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">กระทู้ทั้งหมด</h4>
		</div>
		<div class="panel-body">
			<?php echo $msg->display() ;  ?>
			
			<form id="frm_search" class="form-inline" action="" method="get">
				<div class="form-group">
					<select class="form-control" id="search_type" name="search_type" style="width: auto;" >
						<option value="1"<?php if(@$_GET["search_type"] == 1) : ?> selected<?php endif; ?>>หัวข้อ</option>
						<option value="2"<?php if(@$_GET["search_type"] == 2) : ?> selected<?php endif; ?>>รายละเอียด</option>
						<option value="3"<?php if(@$_GET["search_type"] == 3) : ?> selected<?php endif; ?>>ผู้โพส</option>
					</select>
				</div>
				
				<div class="form-group">
					<input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo @$_GET["keyword"]; ?>" placeholder="พิมพ์ข้อความที่ต้องการค้นหา">
				</div>
				
				<button type="submit" class="btn btn-success">ค้นหา</button>
			</form>
			
			<div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
 						<th class="text-center" >หัวข้อ</th>
						<th class="text-center" >หมวด</th>
						<th class="text-center" >โดย</th>
						<th class="text-center" >วันที่โพส</th>
						<th class="text-center" >สถานะ</th>
  						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i--; ?></td>
						<td><a href="/board/topic/<?php echo @$row["topic_id"] ?>" target="_blank"><?php echo @$row["topic_title"] ?></a></td>
						<td><?php echo @$row["forum_title"] ?></td>
						<td><?php echo @$row["wmem_name"] ?></td>
						<td class="text-center"><?php echo mysqldate2thaidate(@$row["craetedate"], "short", true) ?></td>
						<td class="text-center">
							<?php if(@$row["topic_status"]) : ?>
								<span class="status_show btn btn-info btn-xs" data-id="<?php echo @$row["topic_id"] ?>">แสดง</span>
							<?php else : ?>
								<span class="status_hide btn btn-danger btn-xs" data-id="<?php echo @$row["topic_id"] ?>">ซ่อน</span>
							<?php endif; ?>
							
							<?php if(@$row["topic_pin"]) : ?>
								<span class="status_pin btn btn-success btn-xs" data-id="<?php echo @$row["topic_id"] ?>">ปักหมุด</span>
							<?php else : ?>
								<span class="status_unpin btn btn-warning btn-xs" data-id="<?php echo @$row["topic_id"] ?>">ไม่ได้ปักหมุด</span>
							<?php endif; ?>
						</td>
  						<td style="text-align:center" >
							<a href="?in=page&do=edit&pid=<?php echo @$row["topic_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=page&do=delete&pid=<?php echo @$row["topic_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 