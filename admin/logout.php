<?php
	session_start();
	require "config.inc.php" ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
	$cmplog->add_log("logout" , "ออกจากระบบ"  ) ;
	
	$_SESSION["ADMIN_ID"] = "" ;
	
	
	
	echo "<script type='text/javascript'>document.location.href='login.php?';</script>";
	exit();