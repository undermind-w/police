<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;

	$cmpper->check("setting") ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');


	if($_POST){
		if(@$_POST["in"] == "setting" ){

			$st_title = @$mysqli->real_escape_string(@$_POST["st_title"]) ;
			$st_addr_1 = @$mysqli->real_escape_string(@$_POST["st_addr_1"]) ;
			$st_addr_2 = @$mysqli->real_escape_string(@$_POST["st_addr_2"]) ;
			$st_tel = @$mysqli->real_escape_string(@$_POST["st_tel"]) ;
			$st_fax = @$mysqli->real_escape_string(@$_POST["st_fax"]) ;
			$st_email = @$mysqli->real_escape_string(@$_POST["st_email"]) ;
			$st_facebook = @$mysqli->real_escape_string(@$_POST["st_facebook"]) ;
			$st_line = @$mysqli->real_escape_string(@$_POST["st_line"]) ;
			$st_email_send = @$mysqli->real_escape_string(@$_POST["st_email_send"]) ;
			$st_name_01 = @$mysqli->real_escape_string(@$_POST["st_name_01"]) ;
			$st_name_02 = @$mysqli->real_escape_string(@$_POST["st_name_02"]) ;
			$st_name_03 = @$mysqli->real_escape_string(@$_POST["st_name_03"]) ;

			if(!empty($_FILES["st_sign_01"]["tmp_name"])) {
					$x = pathinfo($_FILES["st_sign_01"]["name"]);
					$ext = $x["extension"];
					$filename = "st_sign_01_".date('YmdGis').".{$ext}";
					copy($_FILES["st_sign_01"]["tmp_name"], "{$_SERVER['DOCUMENT_ROOT']}/uploads/{$filename}");
					$st_sign_01 = " , st_sign_01 = '{$filename}' ";
			}
			if(!empty($_FILES["st_sign_02"]["tmp_name"])) {
					$x = pathinfo($_FILES["st_sign_02"]["name"]);
					$ext = $x["extension"];
					$filename = "st_sign_02_".date('YmdGis').".{$ext}";
					copy($_FILES["st_sign_02"]["tmp_name"], "{$_SERVER['DOCUMENT_ROOT']}/uploads/{$filename}");
					$st_sign_02 = " , st_sign_02 = '{$filename}' ";
			}
			if(!empty($_FILES["st_sign_03"]["tmp_name"])) {
					$x = pathinfo($_FILES["st_sign_03"]["name"]);
					$ext = $x["extension"];
					$filename = "st_sign_03_".date('YmdGis').".{$ext}";
					copy($_FILES["st_sign_03"]["tmp_name"], "{$_SERVER['DOCUMENT_ROOT']}/uploads/{$filename}");
					$st_sign_03 = " , st_sign_03 = '{$filename}' ";
			}
			//$st_sign_02

			$sql = "UPDATE cmp_website SET
								st_title = '{$st_title}'
								, st_addr_1 = '{$st_addr_1}'
								, st_addr_2 = '{$st_addr_2}'
								, st_tel = '{$st_tel}'
								, st_fax = '{$st_fax}'
								, st_email = '{$st_email}'
								, st_facebook = '{$st_facebook}'
								, st_email_send = '{$st_email_send}'
								, st_line = '{$st_line}'
									{$st_sign_01}
									{$st_sign_02}
									{$st_sign_03}
								, st_name_01 = '{$st_name_01}'
								, st_name_02 = '{$st_name_02}'
								, st_name_03 = '{$st_name_03}'
								, updatedate = NOW()
								WHERE st_id = 1  LIMIT 1 " ;
			$rs  = $mysqli->query($sql);
			echo $mysqli->error  ;
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;

			$cmplog->add_log("setting-edit" , "แก้ไขข้อมูลเว็บไซต์ " ) ;
			echo "<script> window.location.href = \"?\"</script>" ;
			exit();
		}
	}

	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
	<!-- <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script> -->
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>
	<script>
		$(function(){


		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "ข้อมูลเว็บไซต์" );
	$smarty->assign("page_title", "ข้อมูลเว็บไซต์" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("ข้อมูลเว็บไซต์" => "#"));

	 ob_start();
	 $sql = "SELECT * FROM cmp_website WHERE st_id = 1 " ;
	 $rs = $mysqli->query($sql);
	$setting =  $rs->fetch_assoc();
?>
	 <div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ข้อมูลเว็บไซต์</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ; ?>
					<form class="form-horizontal" action="?" method="post" enctype="multipart/form-data">
						<input type="hidden" id="in" name="in" value="setting" />
						<div class="form-group">
							<label for="st_title" class="col-sm-3 control-label">ชื่อเว็บไซต์</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="st_title" name="st_title" placeholder="" value="<?php echo @$setting["st_title"] ?>" >
							</div>
						</div>
						 <div class="form-group">
							<label for="st_addr_1" class="col-sm-3 control-label">ที่อยู่บรรทัดที่ 1</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="st_addr_1" name="st_addr_1" placeholder="" value="<?php echo @$setting["st_addr_1"] ?>" >
							</div>
						</div>
						<div class="form-group">
							<label for="st_addr_2" class="col-sm-3 control-label">ที่อยู่บรรทัดที่ 2</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="st_addr_2" name="st_addr_2" placeholder="" value="<?php echo @$setting["st_addr_2"] ?>" >
							</div>
						</div>
						<div class="form-group">
							<label for="st_tel" class="col-sm-3 control-label">เบอร์โทรศัพท์</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="st_tel" name="st_tel" placeholder="" value="<?php echo @$setting["st_tel"] ?>" >
							</div>
						</div>
						<div class="form-group">
							<label for="st_fax" class="col-sm-3 control-label">เบอร์โทรสาร</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="st_fax" name="st_fax" placeholder="" value="<?php echo @$setting["st_fax"] ?>" >
							</div>
						</div>
						<div class="form-group">
							<label for="st_email" class="col-sm-3 control-label">อีเมล์</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" id="st_email" name="st_email" placeholder="" value="<?php echo @$setting["st_email"] ?>" >
							</div>
						</div>
						<div class="form-group">
							<label for="st_facebook" class="col-sm-3 control-label">Facebook </label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="st_facebook" name="st_facebook" placeholder="" value="<?php echo @$setting["st_facebook"] ?>" >
							</div>
						</div>
						<div class="form-group">
							<label for="st_line" class="col-sm-3 control-label">Line </label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="st_line" name="st_line" placeholder="" value="<?php echo @$setting["st_line"] ?>" >
							</div>
						</div>
						<div class="form-group">
							<label for="st_email_send" class="col-sm-3 control-label">อีเมล์ส่งข้อมูล</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" id="st_email_send" name="st_email_send"  placeholder="" value="<?php echo @$setting["st_email_send"] ?>" >
							</div>
						</div>
						<?php /*<div class="form-group">
							<label for="st_sign_01" class="col-sm-3 control-label">ลายเซ็นประธาน</label>
							<div class="col-sm-9">
								<input type="file" name="st_sign_01" />
								<?php
										if(!empty($setting["st_sign_01"])) {
												echo sprintf('<img src="/uploads/%s" width="70" />', $setting["st_sign_01"]);
										}
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="st_sign_02" class="col-sm-3 control-label">ลายเซ็นเลขานุการ</label>
							<div class="col-sm-9">
								<input type="file" name="st_sign_02" />
								<?php
										if(!empty($setting["st_sign_02"])) {
												echo sprintf('<img src="/uploads/%s" width="70" />', $setting["st_sign_02"]);
										}
								?>
							</div>
						</div>*/ ?>
						<!--
						<div class="form-group">
							<label for="st_sign_03" class="col-sm-3 control-label">ลายเซ็นหัวหน้าฝ่ายการเงิน</label>
							<div class="col-sm-9">
								<input type="file" name="st_sign_03" />
								<?php
										if(!empty($setting["st_sign_03"])) {
												echo sprintf('<img src="/uploads/%s" width="70" />', $setting["st_sign_03"]);
										}
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="st_name_02" class="col-sm-3 control-label">ชื่อหัวหน้าฝ่ายการเงิน</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="st_name_03" name="st_name_03" placeholder="" value="<?php echo @$setting["st_name_03"] ?>" >
							</div>
						</div>
						-->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
