<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
		if(@$_POST["in"] == "board" ){ 
			$boa_id = (int) @$_POST["boa_id"] ; 
			$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
			$boa_name = @$mysqli->real_escape_string(@$_POST["boa_name"]) ;
			$boa_position = @$mysqli->real_escape_string(@$_POST["boa_position"]) ;
			$boa_picture = "" ; 
			if(!empty($_FILES["boa_picture"]["tmp_name"] )) {
				$ints = date("YmdGis") . @random_char(7) ; 
				
				if($_FILES["boa_picture"]["type"] =="image/png"||$_FILES["boa_picture"]["type"] =="image/x-png"){
					$imgsn = $ints.".png";
				}elseif($_FILES["boa_picture"]["type"] =="image/gif"){
					$imgsn = $ints.".gif";
				}elseif($_FILES["boa_picture"]["type"] =="image/pjpeg"||$_FILES["boa_picture"]["type"] =="image/jpeg"){
					$imgsn = $ints.".jpg";
				}
				if(!empty($imgsn)) {
					@copy($_FILES["boa_picture"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
					// CHECK SIZE IMAGE
					$size = getimagesize (PATH . "/uploads/contents/{$imgsn}" ) ; 
					if($size[0] > 350 ){
						$url = "http://{$_SERVER["SERVER_NAME"]}/timthumb.php?src=/uploads/contents/{$imgsn}&w=350&zc=0";
						$img = PATH . "/uploads/contents/{$imgsn}" ;
						file_put_contents($img, file_get_contents($url));
					}
					// END CHECK IMAGE
					$boa_picture = "{$imgsn}"  ;
				}
			}
			
			
			$boa_status = (int) @$_POST["boa_status"] ; 
			
			
		 
			if(empty($boa_id))
			{
				$sql = "SELECT MAX(boa_seq) as _m FROM cmp_board " ;
				$rs = $mysqli->query($sql);
				$max = $rs->fetch_assoc();
				$boa_seq = (int) ( $max["_m"] + 1  ) ; 
				
				$sql = "INSERT INTO cmp_board 
												( 
												admin_id 
												, boa_seq 
												, boa_name 
												, boa_position 
												, boa_picture
												, createdate
												, updatedate
												, boa_status 
												) 
												VALUES  
												(
												'{$admin_id}'
												, '{$boa_seq}'
												, '{$boa_name}'
												, '{$boa_position}'
												, '{$boa_picture}'
												, NOW() 
												, NOW() 
												, {$boa_status} ) " ;
				$mysqli->query($sql);
				echo $mysqli->error ;
				$boa_id = (int) $mysqli->insert_id ; 
				$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
				$cmplog->add_log("boards-add" , "เพิ่มคณะกรรมการ " , $boa_id ) ;
			}
			else
			{	
				$sql_boa_picture =  !empty($boa_picture) ? " , boa_picture = '{$boa_picture}' " : "" ;   
				$sql = "UPDATE cmp_board SET boa_name = '{$boa_name}' 
													, boa_position= '{$boa_position}' 
													{$sql_boa_picture}
													, updatedate = NOW() 
													, boa_status = {$boa_status}
													WHERE 
													boa_id = {$boa_id} LIMIT 1  " ;
				$mysqli->query($sql);
				echo $mysqli->error ;								
				$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
				$cmplog->add_log("boards-edit" , "แก้ไขคณะกรรมการ " , $boa_id ) ;
			}
 			
			 
			if(@$_POST["by"] == "ajax"){
				
			}else{
				echo "<script> window.location.href = \"?\"</script>" ;
			}
			exit();
		}elseif($_POST["in"] == "sort"){
			$boa_id = $_POST["boa_id"] ; 
			$i = 1  ; 
			foreach($boa_id as $value){
				$sql = "UPDATE cmp_board SET boa_seq = {$i} WHERE boa_id = {$value}  " ;
				$mysqli->query($sql);
				echo $mysqli->error; 
				$i ++ ; 
			}
			echo json_encode(array("msg"=>"" , "status" => 1)) ;
			exit();
			
		}
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("board"))  )
	{
		$boa_id = (int) @$_GET["bid"] ;  
 		$sql = "DELETE FROM cmp_board WHERE boa_id = {$boa_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("boards-delete" , "ลบคณะกรรมการ " , $boa_id ) ;
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/scripts/jquery-ui/jquery-ui.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

jquery-ui.min.js
<script>
	$(function(){
		$("#frm_board").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
		$( "#sortable" ).sortable({ 
			stop : function(event,ui){ 
				var boa_id = [] ; 
				$("#sortable input[name='boa_id[]']").each( function(){
 					boa_id.push($(this).val()) ; 
				});
				
				$.ajax({
					method: "POST",
					url: "?",
					data: { 'in' : 'sort',  boa_id : boa_id }
				})
				.done(function( msg ) {
					console.log(msg);
				});
			}
			
			
		}); 
		$( "#sortable" ).disableSelection();
 	}) ; 
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$boa_id = (int) @$_GET["bid"] ; 
			$sql = "SELECT * FROM cmp_board WHERE boa_id = {$boa_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "คณะกรรมการ" );
		$smarty->assign("page_title", "คณะกรรมการ" );
 		$smarty->assign("breadcrumb", array("คณะกรรมการ" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$boa_id = (int) @$row["boa_id"]  ; 
		
 		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">คณะกรรมการ</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_board" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="boa_id" value="<?php echo $boa_id ;  ?>"/>
  						<input type="hidden" name="in" value="board"/>
						<div class="form-group">
							<label for="boa_name" class="col-sm-3 control-label">ชื่อ - สกุล </label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="boa_name" name="boa_name" placeholder="" value="<?php echo @$row["boa_name"] ?>" required="required" >
 							</div>
						</div>
						<div class="form-group">
							<label for="boa_name" class="col-sm-3 control-label">ตำแหน่ง </label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="boa_position" name="boa_position" placeholder="" value="<?php echo @$row["boa_position"] ?>" required="required" >
 							</div>
						</div>
						 <div class="form-group">
							<label for="doc_title" class="col-sm-3 control-label">รูปกาพ </label>
							<div class="col-sm-9">
								<input type="file"  class="" id="boa_picture" name="boa_picture" placeholder="" value=""  >
								<?php if(!empty($row["boa_picture"])){ ?>
									<br />
 									<img src="/uploads/contents/<?php echo $row["boa_picture"] ?>" style="min-width:250px"  />									
								<?php } ?>
 							</div>
						</div>
						  
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="boa_status" name="boa_status" <?php echo $row["boa_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "คณะกรรมการ" );
		$smarty->assign("page_title", "คณะกรรมการ" );
		$smarty->assign("breadcrumb", array("คณะกรรมการ" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(boa_id) as _c FROM cmp_board" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 100 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_board ORDER BY boa_seq ASC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
 
		$i =  1; 
	 
?>
	<style>
		.move {
			padding : 5px;
			display:block ; 
			float:right ; 
			
		}
	</style>
	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">คณะกรรมการ</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			<div>
				<div id="sortable" class="row group-sort" >
				 <?php while($row = $rs->fetch_assoc()){ ?>
				 
				 <div class="col-sm-3 sortable" style="height:400px;  background-color: #FFF;">
					<input type="hidden" name="boa_id[]" value="<?php echo @$row["boa_id"] ?>" />
					<div class="">
						<span class="move"   ><i class="icon-cursor-move"> </i></span>
					</div>
					<div style="padding:10px;border:1px solid #CCC ">
						<form id="frm_board_<?php echo $row["boa_id"] ?>" method="post" action="?" enctype="multipart/form-data" >
						<div >
							<?php if(empty($row["boa_picture"] )){  ?>
							<div><img src="/timthumb.php?src=/images/blank.png&h=300&w=300" class="img-responsive" style="margin:auto"/></div>
							<?php }else{ ?>
							<div><img src="/timthumb.php?src=/uploads/contents/<?php echo $row["boa_picture"] ?>&h=300&w=300" class="img-responsive" style="margin:auto"/></div>
							<?php }?>
						</div>
						<div style="padding-top:10px">
							<div class="row">
								<div class="col-xs-6">
									<a href="?in=board&do=edit&bid=<?php echo @$row["boa_id"] ?>" class="btn btn-success btn-block"  >แก้ไข</a>
								</div>
								<div class="col-xs-6">
									<a href="?in=board&do=delete&bid=<?php echo @$row["boa_id"] ?>" class="btn btn-danger btn-block" >ลบข้อมูล</a>
								</div>
							</div>
						</div>
						<div class="text-center" style="padding-top:10px" >
							<strong><?php echo @$row["boa_name"] . " " . @$row["boa_position"]  ?></strong>
						</div> 
						
						</form>
					 </div>
				 </div>
				 <?php } ?>
				 </div>
			 </div>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 