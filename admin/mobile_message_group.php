<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
    require "menu.inc.php" ;

	$cmpper->check("setting") ;

	header("Content-Type:text/html;charset=utf-8");
    date_default_timezone_set('Asia/Bangkok');
    
    ob_start();
?>
    <meta name="keywords" content="<?php echo KEYWORD; ?>" />
    <meta name="description" content="<?php echo DESC; ?>" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

    ob_start();   
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>

<style>
    .group-content{
        background: #fff;
        padding: 20px;
    }
    .btn-send{
        background: #548159 !important;
        color: #fff !important;
        float: right;
    }
    .fas{
        margin: 0 5px;
        cursor: pointer;
    }
    .err-txt{
		text-align: center;
	}
	.alert-danger{
		text-align: center;
		display: none;
	}
	.close-tag-err{
		float: right;
		cursor: pointer;
	}
</style>

<script>
    function closeErr(e) {
        document.getElementById(e).style.display = "none";
	}
    function gotoInsert() {
        window.location.href = "/admin/message_group.php?do=insert";
    }
    function gotoDeleteGroup(id) {
        var param = { "action": "delete", "group_id": id };
		$.post('query.member.group.php', param, function(res) {
            var response = JSON.parse(res);
            // console.log('response', response)
            if ( response.success ) {
                window.location.href = "/admin/mobile_message_group.php";
            } else {
                $("#err-alert-server").show("fade");
            }
        })
    }
    function gotoEditGroup(id) {
        window.location.href = "/admin/message_group.php?do=edit&id=" + id;
    }
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "กลุ่มผู้รับข้อความ" );
	$smarty->assign("page_title", "กลุ่มผู้รับข้อความ" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("กลุ่มผู้รับข้อความ" => "#"));

	ob_start();

    require "system.connect.php";

    function dateDB2thaidate($date, $yearShort = true, $time = true) { //2018-01-01 or 2018-01-01 12:12:12
        $month = array( 1 => "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค." );
        $dateTime = explode(" ", $date);
        $date = explode("-", $dateTime[0]);
        $y = $date[0];
        $m = $date[1];
        $d = $date[2];
        $yConvert = ( $yearShort ) ? substr((string)((int)$y + 543), 2) : (string)((int)$y + 543);
        $dateConvert = $d." ".$month[(int)$m]." ".$yConvert;
        $convert = ( $time ) ? $dateConvert." ".$dateTime[1] : $dateConvert;
        return $convert;
    }

    $sql = "SELECT tb1.mobile_group_id, tb1.mobile_group_name, tb1.create_date, tb2.member_list
    FROM mobile_message_group AS tb1 
    INNER JOIN mobile_message_group_person AS tb2 ON tb2.mobile_group_id = tb1.mobile_group_id
    WHERE tb1.is_active = 1
    ORDER BY tb1.create_date ASC";
    $group = $mysqli_system->query($sql);
    $i = 1;
?>

<div class="group-content">
    <?php require "alert.content.php"; ?>
    <div class="btn btn-send" onClick="gotoInsert()">เพิ่มกลุ่ม</div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="10%">ลำดับ</th>
                <th width="40%">ชื่อกลุ่ม</th>
                <th width="15%" class="text-center">จำนวนสมาชิก</th>
                <th width="25%">วันที่สร้าง</th>
                <th width="10%"></th>
            </tr>
        </thead>

        <?php if ( $group->num_rows ) { ?>
            <tbody>
                <?php 
                    while( ($row = $group->fetch_assoc()) ) { 
                    $rowCount = explode(",", $row['member_list']);
                ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><div><?php echo $row['mobile_group_name']; ?></div></td>
                        <td class="text-center"><?php echo COUNT($rowCount); ?></td>
                        <td><?php echo dateDB2thaidate($row['create_date']); ?></td>
                        <td>
                            <i class="fas fa-edit" title="แก้ไข" onClick="gotoEditGroup(<?php echo $row['mobile_group_id']; ?>)"></i>  
                            |  
                            <i class="fas fa-trash-alt" title="ลบ" onClick="gotoDeleteGroup(<?php echo $row['mobile_group_id']; ?>)"></i>
                        </td>
                    </tr>
                <?php $i++; } ?>
            </tbody>
        <?php } else { ?>
            <tbody>
                <tr>
                    <td colspan="5" class="text-center">ไม่พบกลุ่มผู้รับข้อความ</td>
                </tr>
            </tbody>
        <?php } ?>

    </table>
</div>

<?php
	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
    $smarty->display(THEME.".tpl");
?>