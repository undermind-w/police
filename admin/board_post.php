<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	
	if(@$_POST["ajax"]) {
		switch($_POST["do"]) {
			case "set_hide":
				$sql = "UPDATE cmp_webboard_reply SET rep_status = 0 WHERE rep_id = {$_POST["id"]}";
				$mysqli->query($sql);
				echo $mysqli->error;
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
			case "set_show":
				$sql = "UPDATE cmp_webboard_reply SET rep_status = 1 WHERE rep_id = {$_POST["id"]}";
				$mysqli->query($sql);
				echo $mysqli->error;
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
		}
		exit;
	}
	
	if($_POST)
	{
 		$rep_id = (int) @$_POST["rep_id"] ; 
		
		$rep_detail = @$mysqli->real_escape_string(@$_POST["rep_detail"]) ;
		
  		$rep_status = (int) @$_POST["rep_status"] ; 
		
		
	 
		if(!empty($rep_id))
		{	
  			$sql = "UPDATE cmp_webboard_reply SET rep_detail = '{$rep_detail}'
   												, updatedate = NOW() 
												, rep_status = {$rep_status}
												WHERE 
												rep_id = {$rep_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("page"))  )
	{
		$rep_id = (int) @$_GET["pid"] ;  
 		$sql = "DELETE FROM cmp_webboard_reply WHERE rep_id = {$rep_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_forum").validate();
		
		if($("#rep_detail").length) {
			$("#rep_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
		}
		
		$(document).on("click", ".status_show", function() {
			var status_show = $(this);
			var id = $(this).attr("data-id");
			
			$.ajax({
				type: "POST",
				url: "?",
				data: "ajax=1&do=set_hide&id=" + id,
				success: function(msg) {
					data = jQuery.parseJSON(msg);
					if(data["status"] == "TRUE") {
						status_show.removeClass("status_show");
						status_show.removeClass("btn-info");
						status_show.addClass("status_hide");
						status_show.addClass("btn-danger");
						status_show.html("ซ่อน");
					}
				}
			});
			return false;
		});
		
		$(document).on("click", ".status_hide", function() {
			var status_hide = $(this);
			var id = $(this).attr("data-id");
			
			$.ajax({
				type: "POST",
				url: "?",
				data: "ajax=1&do=set_show&id=" + id,
				success: function(msg) {
					data = jQuery.parseJSON(msg);
					if(data["status"] == "TRUE") {
						status_hide.removeClass("status_hide");
						status_hide.removeClass("btn-danger");
						status_hide.addClass("status_show");
						status_hide.addClass("btn-info");
						status_hide.html("แสดง");
					}
				}
			});
			return false;
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$rep_id = (int) @$_GET["pid"] ; 
			$sql = "SELECT * FROM cmp_webboard_reply WHERE rep_id = {$rep_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "กระดานถามตอบ - ความคิดเห็นล่าสุด" );
		$smarty->assign("forum_title", "กระดานถามตอบ - ความคิดเห็นล่าสุด" );
 		$smarty->assign("breadcrumb", array("กระดานถามตอบ - ความคิดเห็นล่าสุด" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$rep_id = (int) @$row["rep_id"]  ; 
		
		$rep_status = (int) @$row["rep_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ความคิดเห็นล่าสุด</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_forum" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="rep_id" value="<?php echo $rep_id ;  ?>"/>
						
						<div class="form-group">
							<label for="inr_percent" class="col-sm-3 control-label">ความคิดเห็น</label>
							<div class="col-sm-9">
								<textarea  class="form-control" id="rep_detail" name="rep_detail" ><?php echo @$row["rep_detail"] ?></textarea>
  							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="rep_status" name="rep_status" <?php echo $row["rep_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  แสดง
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "กระดานถามตอบ - ความคิดเห็นล่าสุด" );
		$smarty->assign("forum_title", "กระดานถามตอบ - ความคิดเห็นล่าสุด" );
		$smarty->assign("breadcrumb", array("กระดานถามตอบ - ความคิดเห็นล่าสุด" => "#") );
		
		ob_start();
		
		$where = "";
		switch(@$_GET["search_type"]) {
			case 1:
				$where = "AND cmp_webboard_reply.rep_detail LIKE '%".@$_GET["keyword"]."%'";
				break;
			case 2:
				$where = "AND cmp_webboard_member.wmem_name LIKE '%".@$_GET["keyword"]."%'";
				break;
		}
		
		$sql = "SELECT COUNT(rep_id) as _c FROM cmp_webboard_reply INNER JOIN cmp_webboard_member ON cmp_webboard_reply.rep_wmem_id = cmp_webboard_member.wmem_id WHERE 1 ".$where ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT cmp_webboard_reply.*, cmp_webboard_member.wmem_name, cmp_webboard_topic.topic_title
						FROM  cmp_webboard_reply
							INNER JOIN cmp_webboard_topic ON cmp_webboard_reply.topic_id = cmp_webboard_topic.topic_id
							INNER JOIN cmp_webboard_member ON cmp_webboard_reply.rep_wmem_id = cmp_webboard_member.wmem_id
						WHERE 1 ".$where."
						ORDER BY createdate DESC
						LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows - ($per_page * ($page - 1 ) );
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">ความคิดเห็นล่าสุด</h4>
		</div>
		<div class="panel-body">
			<?php echo $msg->display() ;  ?>
			
			<form id="frm_search" class="form-inline" action="" method="get">
				<div class="form-group">
					<select class="form-control" id="search_type" name="search_type" style="width: auto;" >
						<option value="1"<?php if(@$_GET["search_type"] == 1) : ?> selected<?php endif; ?>>ความคิดเห็น</option>
						<option value="2"<?php if(@$_GET["search_type"] == 2) : ?> selected<?php endif; ?>>ผู้โพส</option>
					</select>
				</div>
				
				<div class="form-group">
					<input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo @$_GET["keyword"]; ?>" placeholder="พิมพ์ข้อความที่ต้องการค้นหา">
				</div>
				
				<button type="submit" class="btn btn-success">ค้นหา</button>
			</form>
			
			<div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
 						<th class="text-center" >ความคิดเห็น</th>
						<th class="text-center" >โดย</th>
						<th class="text-center" >วันที่โพส</th>
						<th class="text-center" >สถานะ</th>
  						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i--; ?></td>
						<td>
							<div><?php echo @$row["rep_detail"] ?></div>
							<div>จากกระทู้</div>
							<div><?php echo @$row["topic_title"] ?></div>
							<div class="text-right"><a href="/board/topic/<?php echo @$row["topic_id"] ?>" target="_blank">ดูรายละเอียด</a></div>
						</td>
						<td><?php echo @$row["wmem_name"] ?></td>
						<td class="text-center"><?php echo mysqldate2thaidate(@$row["createdate"], "short", true) ?></td>
						<td class="text-center">
							<?php if(@$row["rep_status"]) : ?>
								<span class="status_show btn btn-info btn-xs" data-id="<?php echo @$row["rep_id"] ?>">แสดง</span>
							<?php else : ?>
								<span class="status_hide btn btn-danger btn-xs" data-id="<?php echo @$row["rep_id"] ?>">ซ่อน</span>
							<?php endif; ?>
						</td>
  						<td style="text-align:center" >
							<a href="?in=page&do=edit&pid=<?php echo @$row["rep_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=page&do=delete&pid=<?php echo @$row["rep_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 