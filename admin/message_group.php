<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
    require "menu.inc.php" ;

	$cmpper->check("setting") ;

	header("Content-Type:text/html;charset=utf-8");
    date_default_timezone_set('Asia/Bangkok');
    
    ob_start();
?>
    <meta name="keywords" content="<?php echo KEYWORD; ?>" />
    <meta name="description" content="<?php echo DESC; ?>" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

    ob_start();   
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>

<style>
    .group-content{
        background: #fff;
        padding: 20px;
        position: relative;
    }
    .group-row{
        width: 100%;
        position: relative;
        padding: 10px 0;
    }
    .content-left{
        width: 20%;
        display: inline-block;
        text-align: right;
        padding-right: 20px;
    }
    .content-right{
        width: 79%;
        display: inline-block;
        padding-left: 20px;
    }
    .member{
        vertical-align: top;
    }
    div.input-group{
		width: 100%;
	}
	.display-none{
		display: none;
	}
    .input-group-btn button.btn{
		width: 50px !important;
	}
    .fa-spinner{
		display: none;
		font-size: 18.5px !important;
	}
    .title-search{
        padding-top: 5px;
    }
    .err-txt{
		text-align: center;
	}
	.alert-danger{
		text-align: center;
		display: none;
	}
	.close-tag-err{
		float: right;
		cursor: pointer;
	}
    .frame-people{
		width: 100%;
		height: 500px;
		background: #F1F4F9;
		border-radius: 0;
		border: 1px solid #dce1e4;
		box-shadow: none!important;
		font-size: 13px;
		padding: 7px!important;
		-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
	}
    .people-tag{
		border-radius: 4px;
		padding: 4px 5px;
		background: #F1F4F9;
		border: 0.5px solid #dce1e4;
		font-size: 11px;
		margin: 0 5px 5px 0;
		display: inline-block;
        cursor: pointer;
	}
    .fa-times{
		margin-left: 5px;
	}
    .text-center{
        text-align: center !important;
    }
    .btn-theme{
        background: #548159 !important;
        color: #fff !important;
    }
</style>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "เพิ่มกลุ่มผู้รับข้อความ" );
	$smarty->assign("page_title", "เพิ่มกลุ่มผู้รับข้อความ" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("เพิ่มกลุ่มผู้รับข้อความ" => "#"));

	ob_start();

	require "system.connect.php";
	
	$group_name = "";
	$member_list = "";

	if ( $_GET['id'] ) {
		$group_id = $_GET['id'];
		$sql = "SELECT mobile_group_name FROM mobile_message_group WHERE mobile_group_id = {$group_id}";
		$group = $mysqli_system->query($sql);
		$row = $group->fetch_assoc();
		$group_name = $row['mobile_group_name'];

		$sql = "SELECT member_list FROM mobile_message_group_person WHERE mobile_group_id = {$group_id}";
		$group = $mysqli_system->query($sql);
		$row = $group->fetch_assoc();
		$member_list = $row['member_list'];
	}
?>

<div class="group-content">
    <?php require "alert.content.php"; ?>
    <div class="group-row">
        <div class="content-left">ชื่อกลุ่ม : </div>
        <div class="content-right"><input type="text" id="group_name" class="form-control" name="group_name" value="<?php echo $group_name; ?>" /></div>
    </div>
    <div class="group-row">
        <div class="content-left member title-search">ค้นหา : </div>
        <div class="content-right">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </span>
                <input type="text" class="form-control" id="inputPeople" placeholder="ค้นหาจาก ชื่อ - สกุล หรือ รหัสสมาชิก  ( ตัวอย่าง สะอาด ใจซื่อ  หรือ  000001 )" onkeypress="runScript(event)">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onClick="AjaxPost(document.getElementById('inputPeople').value)">
                        <span class="people-go">Go !</span>
                        <i class="fa fa-spinner fa-spin people-fa" style="font-size:24px"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="group-row">
        <div class="content-left member">สมาชิก : </div>
        <div class="content-right">
            <div class="frame-people" id="frame-people">
                <input type="hidden" id="peopleList" class="hidden-people" name="peopleList" value="<?php echo $member_list; ?>">
				<?php
					if ( $_GET['id'] ) {
						$member_arr = explode(',', $member_list);
						for ( $i = 0; $i < COUNT($member_arr); $i++ ) {
							echo "<div class='people-tag'>".$member_arr[$i]."<i class='fa fa-times close-tag' aria-hidden='true' onClick='closeTag(\"".$member_arr[$i]."\")'></i></div>";
						}
					}
				?>
            </div>
        </div>
    </div>
    <div class="group-row text-center">
        <div class="content-left member"></div>
        <div class="content-right"><button class="btn btn-theme" onClick="validate('<?php echo $_GET['do']; ?>', <?php echo $_GET['id']; ?>)">บันทึก</button></div>
    </div>
</div>

<script>

    var check = [];

	setTimeout(() => {
		if ( checkQueryVariable("id") ) {
			var param = { "action": "get", "group_id": getQueryVariable("id") };
			$.post('query.member.group.php', param, function(res) {
				var response = JSON.parse(res);
				// console.log('response', response)
				for ( $i = 0; $i < response.member_list.length; $i++ ) {
					check.push(response.member_list[$i]);
				}
			})
		}
	}, 500);

	function getQueryVariable(variable) {
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
			if (pair[0] == variable) {
				return pair[1];
			}
		} 
		return "";
	}
	function checkQueryVariable(variable) {
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
			if (pair[0] == variable) return true;
		} 
		return false;
	}
    function getClass(e) { return document.getElementsByClassName(e) }
    function loadingSearch(e) {
		if ( e ) {
			var elems = getClass('people-fa');
			elems[0].style.display = "block";
			var elems = getClass('people-go');
			elems[0].style.display = "none";
		} else {
			var elems = getClass('people-fa');
			elems[0].style.display = "none";
			var elems = getClass('people-go');
			elems[0].style.display = "block";
		}
	}
	function AjaxPost(e) {
        if ( e == '' ) {
            $("#err-alert-no-data").show("fade");
            return;
        }
		loadingSearch(true);
		if (inArray(e, check)) {
			loadingSearch(false);
			$("#err-alert-no-repeat").show("fade");
			return;
		}	
		var param = { "search" : e };
		$.post('search.member.admin.php', param, function(res){
			var response = JSON.parse(res);
			if ( response.success ) {
                if ( response.success == 202 ) {
                    loadingSearch(false);
                    $("#err-alert-more-one").show("fade");
                    return;
                }
                if (inArray(response.data.member_id, check)) {
                    loadingSearch(false);
                    $("#err-alert-no-repeat").show("fade");
                    return;
                }

				check.push(response.data.member_id);

				var elems = getClass('hidden-people');
				var value = elems[0].value != "" ? elems[0].value + "," : "";
				elems[0].value = value + response.data.member_id;	

				document.getElementById('inputPeople').value = "";

				var elems = getClass('frame-people');

				var span = document.createElement("div"); 
				span.className = "people-tag";                                     
				document.getElementById('frame-people').appendChild(span);

				var elems = getClass('people-tag');
				var length = (elems.length - 1);
				elems[length].innerHTML = response.data.member_id + "<i class=\"fa fa-times close-tag\" aria-hidden=\"true\" onClick='closeTag(\""+response.data.member_id+"\")'></i>";

			} else {		
				$("#err-alert-no-user").show("fade");
			}
			loadingSearch(false);
			
		});
	}
    function closeTag(e) {	
		deleteArray(e, check);	
	}
	function inArray(string, array) {
		for(var i = 0; i < array.length; i++) {
			if(array[i] == string) return true;
		}
		return false;
	}
    function deleteArray(string, array) {
		var x = 0, text = "";
		for(var i = 0; i < array.length; i++) {
			if(array[i] == string){
				array.splice(i, 1);
				$('.people-tag')[i].remove();
				x = i;
			}
		}
		var elems = getClass('hidden-people');
		var valueArray = elems[0].value.split(",");
		var z = 0;
		for(var i = 0; i < valueArray.length; i++){
			if (i != x){
				text += (z > 0) ? "," + valueArray[i] : valueArray[i];
				z++;
			}
		}
		elems[0].value = text;		
	}
	function closeErr(e) {
        document.getElementById(e).style.display = "none";
	}
    function runScript(e) {
        if ( e.keyCode == 13 ) AjaxPost(document.getElementById('inputPeople').value)
    }
    function validate(e, id) {
        if ( document.getElementById('group_name').value == '' || document.getElementById('peopleList').value == '' ) {
            $("#err-alert-no-data").show("fade");
        } else {
            SaveToServer(e, id)
        }
    }
    function SaveToServer(e, id) {
        $member_list = document.getElementById('peopleList').value
        var param = ( e == "insert" ) ? { "action": e, "group_name": document.getElementById('group_name').value, "data": $member_list } : { "action": e, "group_id": getQueryVariable("id"), "group_name": document.getElementById('group_name').value, "data": $member_list };
		$.post('query.member.group.php', param, function(res) {
            var response = JSON.parse(res);
            // console.log('response', response)
            if ( response.success ) {
                if ( response.success == 101 ) {
                    $("#err-alert-group-duplicate").show("fade");
                } else if ( response.success == 1 ) {
					window.location.href = "/admin/message_group.php";
                    // window.location.href = "/admin/message_group.php?do=insert";
                }
            } else {
                $("#err-alert-server").show("fade");
            }
        });
    }
</script>

<?php
	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
    $smarty->display(THEME.".tpl");
?>