<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
    require "menu.inc.php" ;

	$cmpper->check("setting") ;

	header("Content-Type:text/html;charset=utf-8");
    date_default_timezone_set('Asia/Bangkok');
    
    ob_start();
?>
    <meta name="keywords" content="<?php echo KEYWORD; ?>" />
    <meta name="description" content="<?php echo DESC; ?>" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

    ob_start();   
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>

<style>
    .msg-content{
        background: #fff;
        padding: 20px;
    }
    .btn-send{
        background: #548159 !important;
        color: #fff !important;
        float: right;
    }
    .msg-desc{
        width: 500px;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .text-center{
        text-align: center;
    }
    .fas{
        cursor: pointer;
    }
    .fas:hover{
        color: #548159 !important;
    }
</style>

<script>
    function gotoSendMsg() {
        window.location.href = "/admin/message_action.php?do=send";
    }
    function gotoEditMsg(id) {
        window.location.href = "/admin/message_action.php?do=edit&msg=" + id;
    }
    function gotoDeleteMsg(id) {
        window.location.href = "/admin/message_action.php?do=delete&msg=" + id;
    }
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "ส่งข้อความถึงสมาชิก" );
	$smarty->assign("page_title", "ส่งข้อความถึงสมาชิก" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("ส่งข้อความถึงสมาชิก" => "#"));

	ob_start();

    require "system.connect.php";

    $sql = "SELECT msg_id, msg_title, create_date
    FROM mobile_message WHERE admin_delete = 0 ORDER BY create_date DESC";
    $msg = $mysqli_system->query($sql);

    function dateDB2thaidate($date, $yearShort = true, $time = true) { //2018-01-01 or 2018-01-01 12:12:12
        $month = array( 1 => "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค." );
        $dateTime = explode(" ", $date);
        $date = explode("-", $dateTime[0]);
        $y = $date[0];
        $m = $date[1];
        $d = $date[2];
        $yConvert = ( $yearShort ) ? substr((string)((int)$y + 543), 2) : (string)((int)$y + 543);
        $dateConvert = $d." ".$month[(int)$m]." ".$yConvert;
        $convert = ( $time ) ? $dateConvert." ".$dateTime[1] : $dateConvert;
        return $convert;
    }
?>

    <div class="msg-content">
        <div class="btn btn-send" onClick="gotoSendMsg()">ส่งข้อความ</div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>วัน / เวลา</th>
                    <th>หัวข้อ</th>
                    <th>จำนวนที่ส่ง</th>
                    <th>สถานะ</th>
                    <th></th>
                </tr>
            </thead>

            <?php if ( $msg->num_rows ) { ?>
                <tbody>
                    <?php while( ($row = $msg->fetch_assoc()) ) { 
                        $rows = "SELECT msg_id FROM mobile_message_validate WHERE msg_id = ".$row['msg_id'];
                        $count = $mysqli_system->query($rows);
                    ?>
                        <tr>
                            <td><?php echo dateDB2thaidate($row['create_date']); ?></td>
                            <td><div class="msg-desc"><?php echo $row['msg_title']; ?></div></td>
                            <td class="text-center"><?php echo $count->num_rows; ?></td>
                            <td><?php echo ($count->num_rows > 0) ? "ส่งแล้ว" : "ยังไม่ได้ส่ง"; ?></td>
                            <td>
                                <i class="fas fa-edit" title="แก้ไข" onClick="gotoEditMsg(<?php echo $row['msg_id']; ?>)"></i>  
                                |  
                                <i class="fas fa-trash-alt" title="ลบ" onClick="gotoDeleteMsg(<?php echo $row['msg_id']; ?>)"></i>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } else { ?>
                <tbody>
                    <tr>
                        <td colspan="5" class="text-center">ไม่พบการส่งข้อความถึงสมาชิก</td>
                    </tr>
                </tbody>
            <?php } ?>
        </table>
    </div>

<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
    $smarty->display(THEME.".tpl");
?>