<!-- ============= ERROR ============= -->
<div id="err-alert-no-data" class="alert alert-danger" role="alert" style="display:none">
    กรุณากรอกข้อมูลให้ครบถ้วน
    <i class="fa fa-times close-tag-err" aria-hidden="true" onClick="closeErr('err-alert-no-data')"></i>
</div>
<div id="err-alert-no-user" class="alert alert-danger" role="alert" style="display:none">
    ไม่พบสมาชิกคนนี้ในระบบ
    <i class="fa fa-times close-tag-err" aria-hidden="true" onClick="closeErr('err-alert-no-user')"></i>
</div>
<div id="err-alert-no-repeat" class="alert alert-danger" role="alert" style="display:none">
    คุณได้เพิ่มสมาชิกรายนี้ไปแล้ว
    <i class="fa fa-times close-tag-err" aria-hidden="true" onClick="closeErr('err-alert-no-repeat')"></i>
</div>
<div id="err-alert-more-one" class="alert alert-danger" role="alert" style="display:none">
    รายการที่คุณค้นหามีมากกว่า 1 รายการ กรุณาใส่รายละเอียดเพิ่มเติม
    <i class="fa fa-times close-tag-err" aria-hidden="true" onClick="closeErr('err-alert-more-one')"></i>
</div>
<div id="err-alert-server" class="alert alert-danger" role="alert" style="display:none">
    เกิดความผิดพลาด กรุณาลองใหม่ภายหลัง
    <i class="fa fa-times close-tag-err" aria-hidden="true" onClick="closeErr('err-alert-server')"></i>
</div>
<div id="err-alert-group-duplicate" class="alert alert-danger" role="alert" style="display:none">
    มีกลุ่มนี้อยู่แล้วในระบบ
    <i class="fa fa-times close-tag-err" aria-hidden="true" onClick="closeErr('err-alert-group-duplicate')"></i>
</div>
<!-- ============= ERROR ============= -->


<!-- ============= SUCCESS ============= -->
<!-- ============= SUCCESS ============= -->