<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$page_id = (int) @$_POST["page_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
  		$page_title = @$mysqli->real_escape_string(@$_POST["page_title"]) ;
		$page_code = @$mysqli->real_escape_string(@$_POST["page_code"]) ;
		$page_detail = @$mysqli->real_escape_string(@htmlspecialchars(@$_POST["page_detail"], ENT_QUOTES)) ;

  		$page_status = (int) @$_POST["page_status"] ; 
		
		
	 
		if(empty($page_id))
		{
			$sql = "INSERT INTO cmp_page 
											( 
											admin_id
											, page_code
											, page_title 
 											, page_detail 
   											, createdate
											, updatedate
											, page_status 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$page_code}'
											, '{$page_title}'
 											, '{$page_detail}'
 											, NOW() 
											, NOW() 
											, {$page_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$page_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("page-add" , "เพิ่มหน้าเนื้อหา "  , $page_id    ) ;
		}
		else
		{	
  			$sql = "UPDATE cmp_page SET page_code = '{$page_code}' 
												, page_title = '{$page_title}'  
												, page_detail = '{$page_detail}'  
   												, updatedate = NOW() 
												, page_status = {$page_status}
												WHERE 
												page_id = {$page_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("page-edit" , "แก้ไขหน้าเนื้อหา"  , $page_id    ) ;
		}
		 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("page"))  )
	{
		$page_id = (int) @$_GET["pid"] ;  
 		$sql = "DELETE FROM cmp_page WHERE page_id = '{$page_id}' AND page_code = 'page'  LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("page-delete" , "ลบหน้าเนื้อหา"  , $page_id    ) ;
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_page").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
		
		if($("#page_detail").length) {
			$("#page_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
		}
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$page_id = (int) @$_GET["pid"] ; 
			$sql = "SELECT * FROM cmp_page WHERE page_id = {$page_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "หน้าเนื้อหา" );
		$smarty->assign("page_title", "หน้าเนื้อหา" );
 		$smarty->assign("breadcrumb", array("หน้าเนื้อหา" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$page_id = (int) @$row["page_id"]  ; 
		$page_code = empty($row["page_code"]) ? "page" : @$row["page_code"] ;  
		$page_status = (int) @$row["page_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">หน้าเนื้อหา</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_page" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="page_id" value="<?php echo $page_id ;  ?>"/>
						<input type="hidden" name="page_code" value="<?php echo $page_code ;  ?>"/>
						
						<div class="form-group">
							<label for="page_title" class="col-sm-3 control-label">ชื่อหน้า</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="page_title" name="page_title" placeholder="" value="<?php echo htmlspecialchars(@$row["page_title"]) ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="inr_percent" class="col-sm-3 control-label">เนื้อหา</label>
							<div class="col-sm-9">
								<textarea  class="form-control" id="page_detail" name="page_detail" ><?php echo @$row["page_detail"] ?></textarea>
  							</div>
						</div>
 
  
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="page_status" name="page_status" <?php echo $row["page_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "หน้าเนื้อหา" );
		$smarty->assign("page_title", "หน้าเนื้อหา" );
		$smarty->assign("breadcrumb", array("หน้าเนื้อหา" => "#") );
		
		ob_start();

		$where_key_word = '';
		if($_GET['key_word']){
            $where_key_word = " WHERE page_title LIKE '%{$_GET['key_word']}%'";
        }
		$sql = "SELECT COUNT(page_id) as _c FROM cmp_page".$where_key_word ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;

		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_page {$where_key_word}ORDER BY createdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows - ($per_page * ($page - 1 ) )  ; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">หน้าเนื้อหา</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
            <form id="frm_search" class="form-horizontal" action="?" method="get"  enctype="multipart/form-data">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 pull-right">
                            <table>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <input type="text"  class="form-control" id="key_word" name="key_word" placeholder="" value="">
                                    </td>
                                    <td style="padding-left: 2px;">
                                        <button type="submit" class="btn btn-success">&nbsp;&nbsp;&nbsp;<i class="fa fa-search"></i> ค้นหา&nbsp;&nbsp;&nbsp;</button>
                                    </td>
                                    <td>
                                        <a href="?do=add" class="btn btn-info" style="margin-left:5px;"><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </form>

			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
 						<th class="text-center" >ชื่อหน้า</th>
  						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i --  ?></td>
						<td><a target="_blank" href="/page.php?pid=<?php echo $row["page_id"] ?>" ><?php echo @$row["page_title"] ?></a></td>
  						<td style="text-align:center" >
							<a href="?in=page&do=edit&pid=<?php echo @$row["page_id"] ?>" ><i class="fa fa-edit"></i></a> 
							<?php if($row["page_code"] == "page" ){ ?>
							<a href="?in=page&do=delete&pid=<?php echo @$row["page_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>  
							<?php } ?>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 