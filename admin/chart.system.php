<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;

	$cmpper->check("setting") ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />

<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>

<!-- script and css -->
<style>
    .canvasjs-chart-credit{
        display: none !important;
    }
    #chartContainer{
        height: 370px; width: 100%; padding: 30px 0; background: #ffffff !important;
    }
	.info{
		width: 100%;
		text-align: center;
		z-index: 999999 !important;
		padding: 40px 0;
		background: #ffffff !important;
		font-size: 1.2em;
	}
	.info span{
		padding: 0 15px;
	}
	.info div{
		margin-top: 20px;
		font-size: 1.2em;
	}
</style>


<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "ข้อมูลผู้ใช้งานระบบ" );
	$smarty->assign("page_title", "ข้อมูลผู้ใช้งานระบบ" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("ข้อมูลผู้ใช้งานระบบ" => "#"));

	ob_start();

	$sql = "SELECT mobile_uid FROM mobile_token WHERE trim(platform) = 'iOS' GROUP BY mobile_uid";
	$rs = $mysqli_app->query($sql);
	$row =  $rs->fetch_assoc();
	$ios = $rs->num_rows;
	$ios_people = number_format($ios, 0);

	$sql = "SELECT mobile_uid FROM mobile_token WHERE trim(platform) = 'Android' GROUP BY mobile_uid";
	$rs = $mysqli_app->query($sql);
	$row =  $rs->fetch_assoc();
	$android = $rs->num_rows;
	$android_people = number_format($android, 0);

	$sql = "SELECT member_id FROM web_online_account GROUP BY member_id";
	$rs = $mysqli_app->query($sql);
	$row =  $rs->fetch_assoc();
	$web = $rs->num_rows;
	$web_people = number_format($web, 0);
?>
    
<script>
	var ios_people = "<?php echo $ios_people; ?>";
	var android_people = "<?php echo $android_people; ?>";
	var web_people = "<?php echo $web_people; ?>";

	var ios = "<?php echo $ios; ?>";
	var android = "<?php echo $android; ?>";
	var web = "<?php echo $web; ?>";
	var sum = (Number(ios) + Number(android)) + Number(web);

	var ios_per = ((Number(ios) / sum) * 100).toFixed(2);
	var android_per = ((Number(android) / sum) * 100).toFixed(2);
	var web_per = ((Number(web) / sum) * 100).toFixed(2);

    window.onload = function() {

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            title: {
                text: "ข้อมูลผู้ใช้งานระบบ Online และ Mobile Application"
            },
            data: [{
                type: "pie",
                startAngle: 240,
                yValueFormatString: "##0.00\"%\"",
                indexLabel: "{label} {y} {num}",
                dataPoints: [
                    {y: android_per, label: "Android ", num: "( " + android_people + " คน )"},
                    {y: ios_per, label: "iOS ", num: "( " + ios_people + " คน )"},
                    {y: web_per, label: "Website ", num: "( " + web_people + " คน )"}
                ]
            }]
        });
        chart.render();

    }
</script>

<div id="chartContainer"></div>
<div class="info">
	<span>Android : <?php echo number_format($android, 0); ?> คน</span><span>iOS : <?php echo number_format($ios, 0); ?> คน</span><span>Website : <?php echo number_format($web, 0); ?> คน</span>
	<div><strong>รวม : <?php echo number_format((($android + $ios) + $web), 0); ?> คน</strong></div>
</div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
