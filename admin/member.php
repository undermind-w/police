<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
 	$cmpper->check("member") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	ob_start();
?>

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$(".btn_resetpass").click(function() {
			if(confirm("ต้องการรีเซ็ตรหัสผ่านเป็น เลขที่บัตรประชาชน ใช่หรือไม่?")){
				var id = $(this).data("id");

				$.ajax({
					type: "POST"
					, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
					, data: {
									"do" : "resetpass"
									, "id" : id
									, "_time" : Math.random()
								}
					, success: function(data) {
						alert("รีเซ็ตรหัสผ่านเรียบร้อยแล้ว");
					}
				});
			}
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		 
	}
	else
	{
		
		$smarty->assign("title", "สมาชิก" );
		$smarty->assign("page_title", "สมาชิก" );
		$smarty->assign("breadcrumb", array("สมาชิก" => "#") );
		
		ob_start();
		
		$where = "";
		if(!empty($_GET["search"])) {
			$where = " AND (A.member_id = '".str_pad($_GET["search"], 6, "0", STR_PAD_LEFT)."' OR C.firstname_th LIKE '%".$_GET["search"]."%' OR C.lastname_th LIKE '%".$_GET["search"]."%' )";
		}

		$sql = "SELECT COUNT(*) as `_c` FROM mobile_token A INNER JOIN (SELECT min(mobile_id) as mobile_id, member_id FROM mobile_token t1 WHERE member_id is not null AND member_id <> '' GROUP BY member_id ORDER BY member_id) B ON A.mobile_id=B.mobile_id AND A.member_id=B.member_id
INNER JOIN coop_mem_apply C ON A.member_id = C.member_id INNER JOIN coop_prename D ON C.prename_id=D.prename_id
WHERE is_delete <> 1 AND is_activate = 1".$where ;
		$rs = $mysqli_app->query($sql);
		echo $mysqli_app->error ;

		//echo $sql; exit;

		$count = $rs->fetch_assoc();

		$num_rows = $count["_c"] ;
		$per_page = 25 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ;

        $sql = "SELECT A.mobile_id, A.member_id, D.prename_full, C.firstname_th, C.lastname_th, C.birthday, C.mobile, C.apply_date, C.level, IF(A.platform <> '', A.platform, 'Web') platform, A.create_date, A.description FROM mobile_token A INNER JOIN (SELECT min(mobile_id) as mobile_id, member_id FROM mobile_token t1 WHERE member_id is not null AND member_id <> '' GROUP BY member_id ORDER BY member_id) B ON A.mobile_id=B.mobile_id AND A.member_id=B.member_id
INNER JOIN coop_mem_apply C ON A.member_id = C.member_id INNER JOIN coop_prename D ON C.prename_id=D.prename_id
WHERE is_delete <> 1 AND is_activate = 1 ".$where ." ORDER BY member_id LIMIT {$page_start} , {$per_page}   ";

		$rs = $mysqli_app->query($sql);

		echo $mysqli_app->error;

		$i =  1;
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">สมาชิก</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
 			 <div style="clear: both;margin-bottom:15px"></div>
			<form class="form-inline" action="" method="get" style="margin-bottom:10px;"  >
				<div class="form-group">
					<input type="text" id="search" name="search" class="form-control" value="<?php echo @$_GET["search"]; ?>" />
				</div>
				<button type="submit" class="btn btn-primary"> ค้นหา </button>
 			</form> 
			<table class="table table-bordered">
				<thead>
					<tr>
                        <th width="150" style="text-align:center" > เลขทะเบียน </th>
                        <th width="" style="text-align:center" > ชื่อ - สกุล </th>
                        <th width="300" style="text-align:center" >สังกัด</th>
                        <th width="150" style="text-align:center" >วันเกิด</th>
                        <th width="150" style="text-align:center" >ข้อมูลเบอร์โทรสมาชิก</th>
                        <th width="150" style="text-align:center" >วันที่สมัคร</th>
                        <th width="150" style="text-align:center" >สมัครจาก</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$i = 1 ; 
					while($row = $rs->fetch_assoc()){

					    $sql = "SELECT * FROM coop_mem_group WHERE id = '{$row['level']}'";
					    $rss = $mysqli_app->query($sql);
					    $group = $rss->fetch_assoc();

					?>
					 <tr>
 						<td><?php echo @$row["member_id"]  ?></td>
						<td><?php echo @$row["prename_full"].@$row["firstname_th"]." ".@$row["lastname_th"] ?></td>
  						<td><?php echo @$group['mem_group_name']; ?></td>
                        <td><?php echo ConvertToThaiDate(@$row["birthday"]); ?></td>
                        <td><?php echo @$row["mobile"]; ?></td>
                        <td><?php echo ConvertToThaiDate(@$row["apply_date"]); ?></td>
                        <td><?php echo @$row["platform"]; ?></td>
                      </tr>
					<?php 
						} 
						
						if($rs->num_rows == 0 ){
							echo "<tr><td colspan=\"7\" class=\"text-center\"> ไม่มีข้อมูล </td></tr>"  ;
						}
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
