<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	
	

	$year = (int) @$_GET["year"] ; 
	$month =(int) @$_GET["month"] ; 
 	
	$d = date("YmdHis");
 	$yy = $year ; 
	$mm = sprintf("%02d", $month);
 	
	$type = array("telmobile" => "โทรศัพท์มือถือ" , "addr" => "ที่อยู่ปัจจุบัน" , "addrc"  => "ที่อยู่ที่ติดต่อได้" , "tel"  => "โทรศัพท์บ้าน" , "email" => "อีเมล" ) ;
	
	$sql = "SELECT  cmp_imp_member_data_stat.stat_id  
					, cmp_imp_member.member_no  
					, cmp_imp_member.memname  
					, cmp_imp_member_data_stat.stat_type
					, cmp_imp_member_data_stat.update_time 
					, cmp_imp_member_data_stat.stat_data
					FROM cmp_imp_member_data_stat 
						LEFT JOIN cmp_imp_member ON  cmp_imp_member_data_stat.member_no = cmp_imp_member.member_no				
					WHERE YEAR(cmp_imp_member_data_stat.update_time) = '{$year}' 
						AND MONTH(cmp_imp_member_data_stat.update_time) = '{$month}'
					ORDER BY  cmp_imp_member_data_stat.update_time DESC   " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error; 
	
	header("Content-type: application/vnd.ms-excel;charset=utf-8");
	header("Content-Disposition: attachment; filename=stats{$year}{$month}_{$d}.xls"); 
	date_default_timezone_set('Asia/Bangkok');
	$i = 1 ; 
?><pre>
<html>
<body>
<style> .string{ mso-number-format:\@; } </style>
<table width="600" border="1">
<tr>
    <th width="50"> <div align="center">ลำดับ </div></th>
	<th width="250"> <div align="center">วันที่ </div></th>
	<th width="100"> <div align="center">รหัส </div></th>
 	<th width="250"> <div align="center">ชื่อ-สกุล </div></th>
	<th width="100"> <div align="center">ประเภทข้อมูล</div></th>
 	<th width="500"> <div align="left">ข้อมูลใหม่ </div></th>
</tr> 
<?php 
 
?>
<tr>
<?php while($row = $rs->fetch_assoc()) { ?>
    <td><div align="center"><?php echo $i++ ;  ?></div></td>
    <td  align="left"><?php echo @mysqldate2thaidate( $row["update_time"]  , "long" ,  true  ) ?></td>
	<td  align="left"  class="string" ><?php echo @$row["member_no"] ?></td>
	<td  align="left"><?php echo @$row["memname"] ?></td>
	<td  align="center"><?php echo @$type[$row["stat_type"]] ?></td>
	<td  align="left" class="string" ><span><?php echo @$row["stat_data"] ?></span></td>
 </tr>
<?php } ?>
</table>
</body>
</html></pre>
