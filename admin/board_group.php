<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	
	if(@$_POST["ajax"]) {
		switch($_POST["do"]) {
			case "setBoardGroupPosition":
				if($_POST["index"] > 0) {
					$sql = "SELECT forum_seq 
								  FROM cmp_webboard_forum 
								  ORDER BY forum_seq
								  LIMIT ".($_POST["index"] - 1).", 1";
					$rsTo = $mysqli->query($sql);
					echo $mysqli->error ;
					if($rowTo = $rsTo->fetch_assoc()) {
						$sql = "SELECT * FROM cmp_webboard_forum WHERE forum_id = '".$_POST["id"]."'";
						$rsFrom = $mysqli->query($sql);
						echo $mysqli->error ;
						$rowFrom = $rsFrom->fetch_assoc();
						
						if($rowFrom["forum_seq"] > $rowTo["forum_seq"]) {
							$sql = "UPDATE cmp_webboard_forum SET forum_seq = forum_seq + 1
										  WHERE forum_seq BETWEEN '".$rowTo["forum_seq"]."' AND '".$rowFrom["forum_seq"]."'";
							$mysqli->query($sql);
							echo $mysqli->error;
						}
						else {
							$sql = "UPDATE cmp_webboard_forum SET forum_seq = forum_seq - 1
										  WHERE forum_seq BETWEEN '".$rowFrom["forum_seq"]."' AND '".$rowTo["forum_seq"]."'";
							$mysqli->query($sql);
							echo $mysqli->error;
						}
						
						$sql = "UPDATE cmp_webboard_forum SET forum_seq = '".$rowTo["forum_seq"]."'
									  WHERE forum_id = '".$_POST["id"]."'";
						$mysqli->query($sql);
						echo $mysqli->error;
					}
				}
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
		}
		exit;
	}
	
	if($_POST)
	{
 		$forum_id = (int) @$_POST["forum_id"] ; 
		
  		$forum_title = @$mysqli->real_escape_string(@$_POST["forum_title"]) ;
		$forum_detail = @$mysqli->real_escape_string(@htmlspecialchars(@$_POST["forum_detail"], ENT_QUOTES)) ;

  		$forum_status = (int) @$_POST["forum_status"] ; 
		
		
	 
		if(empty($forum_id))
		{
			$sql = "SELECT forum_seq FROM cmp_webboard_forum ORDER BY forum_seq DESC LIMIT 1";
			$rs = $mysqli->query($sql);
			echo $mysqli->error;
			if($row = $rs->fetch_assoc()) {
				$forum_seq = $row["forum_seq"] + 1;
			}
			else {
				$forum_seq = 1;
			}
			
			$sql = "INSERT INTO cmp_webboard_forum 
											( 
											forum_seq
											, forum_title
											, forum_detail 
 											, createdate 
											, forum_status 
											) 
											VALUES  
											(
											'{$forum_seq}'
											, '{$forum_title}'
											, '{$forum_detail}'
 											, NOW() 
											, {$forum_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$banner_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		else
		{	
  			$sql = "UPDATE cmp_webboard_forum SET forum_title = '{$forum_title}'  
												, forum_detail = '{$forum_detail}'  
   												, updatedate = NOW() 
												, forum_status = {$forum_status}
												WHERE 
												forum_id = {$forum_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("page"))  )
	{
		$forum_id = (int) @$_GET["pid"] ;  
 		$sql = "DELETE FROM cmp_webboard_forum WHERE forum_id = {$forum_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_forum").validate();
		
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};
		
		var loadSortable = function() {
			$("#tb_board_group tbody").sortable({
				helper: fixHelper,
				disabled: true,
				update: function(event, ui) {  
					var id = ui.item.children("td.ui-move").attr("id");
					var index = 0;
					$("#tb_board_group tbody tr").each(function() {
						if($(this).children("td.ui-move").attr("id") == id) {
							index = $(this).index() + parseInt($("#pageindex").val());
						}
					});
					
					$.ajax({
						type: "POST",
						url: "?",
						data: "ajax=1&do=setBoardGroupPosition&id=" + id + "&index=" + index,
						success: function(msg) {
							$("#tb_board_group tbody tr").each(function() {
								$(this).children("td.col2").html($(this).index() + parseInt($("#pageindex").val()));
							});
						}
					});
				}
			});
			$("#tb_board_group tbody .col1").disableSelection();
		}
		loadSortable();
		
		$(document).on("mouseover", "#tb_board_group tbody .col1", function() {
			$("#tb_board_group tbody").sortable("enable");
		});
		$(document).on("mouseout", "#tb_board_group tbody .col1", function() {
			$("#tb_board_group tbody").sortable("disable");
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$forum_id = (int) @$_GET["pid"] ; 
			$sql = "SELECT * FROM cmp_webboard_forum WHERE forum_id = {$forum_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "กระดานถามตอบ - หมวด" );
		$smarty->assign("forum_title", "กระดานถามตอบ - หมวด" );
 		$smarty->assign("breadcrumb", array("กระดานถามตอบ - หมวด" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$forum_id = (int) @$row["forum_id"]  ; 
		
		$forum_status = (int) @$row["forum_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">หมวด</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_forum" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="forum_id" value="<?php echo $forum_id ;  ?>"/>
						
						<div class="form-group">
							<label for="forum_title" class="col-sm-3 control-label">ชื่อหมวด</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="forum_title" name="forum_title" placeholder="" value="<?php echo @$row["forum_title"] ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="inr_percent" class="col-sm-3 control-label">รายละเอียด</label>
							<div class="col-sm-9">
								<textarea  class="form-control" id="forum_detail" name="forum_detail" ><?php echo @$row["forum_detail"] ?></textarea>
  							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="forum_status" name="forum_status" <?php echo $row["forum_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "กระดานถามตอบ - หมวด" );
		$smarty->assign("forum_title", "กระดานถามตอบ - หมวด" );
		$smarty->assign("breadcrumb", array("กระดานถามตอบ - หมวด" => "#") );
		
		ob_start();
				
		$sql = "SELECT * FROM  cmp_webboard_forum ORDER BY forum_seq" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i = 1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">หมวด</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  ><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<input type="hidden" id="pageindex" name="pageindex" value="<?php echo $i; ?>" />
			<table id="tb_board_group" class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="30" ></th>
						<th class="text-center" width="50" >#</th>
 						<th class="text-center" >หมวด</th>
  						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
						<td id="<?php echo @$row["forum_id"] ?>" class="col1 ui-move" title="คลิกลากย้ายตำแหน่ง"><i class="fa fa-arrows-v"></i></td>
                        <td class="col2" style="text-align:center" ><?php echo $i++; ?></td>
						<td><?php echo @$row["forum_title"] ?></td>
  						<td style="text-align:center" >
							<a href="?in=page&do=edit&pid=<?php echo @$row["forum_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=page&do=delete&pid=<?php echo @$row["forum_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 