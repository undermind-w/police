<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$album_id = (int) @$_POST["album_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
		$album_type = @$mysqli->real_escape_string(@$_POST["album_type"]) ;
  		$album_title = @$mysqli->real_escape_string(@$_POST["album_title"]) ;
		$album_detail = @$mysqli->real_escape_string(@$_POST["album_detail"]) ;
		
		$publicdate_date = @$mysqli->real_escape_string(@$_POST["publicdate"]) ;

		$publicdate_time_hour = (int) @$_POST["publicdate_time_hour"] ; 
		$publicdate_time_min = (int) @$_POST["publicdate_time_min"] ; 
		
		$publicdate_time_hour = sprintf("%02d",  $publicdate_time_hour  ) ; 
		$publicdate_time_min = sprintf("%02d",  $publicdate_time_min  ) ; 
		$publicdate  = $publicdate_date . " " . $publicdate_time_hour . ":" . $publicdate_time_min . ":00" ; 

		
		
  		$album_status = (int) @$_POST["album_status"] ; 
		
		
	 
		if(empty($album_id))
		{
			$sql = "INSERT INTO cmp_album 
											( 
											admin_id 
											, album_title 
											, album_detail 
											, album_type 
											, publicdate
  											, createdate
											, updatedate
											, album_status 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$album_title}'
											, '{$album_detail}'
											, '{$album_type}'
											, '{$publicdate}'
 											, NOW() 
											, NOW() 
											, {$album_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$album_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("album-add" , "เพิ่มอัลบั้ม"  , $album_id    ) ;
		}
		else
		{	
 			$sql = "UPDATE cmp_album SET album_title = '{$album_title}' 
												, album_type = '{$album_type}'
												, album_detail = '{$album_detail}'  
												, publicdate = '{$publicdate}'  
 												, updatedate = NOW() 
												, album_status = {$album_status}
												WHERE 
												album_id = {$album_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("album-edit" , "แก้ไขอัลบั้ม"  , $album_id    ) ;
		}
		//Select Type Picture
		if($album_type == "picture" ){
			$album_item = @$_FILES["album_item"]  ; 
			for($i = 0 ; $i < count($album_item["tmp_name"])  ;$i ++ )
			{
			
				if(!empty($album_item["tmp_name"][$i])) 
				{
					$ints = date("YmdGis") . @random_char(7) ; 
					
					if(@$album_item["type"][$i] == "image/png"||@$album_item["type"][$i] =="image/x-png"){
						$imgsn = $ints.".png";
					}elseif(@$album_item["type"][$i] =="image/gif"){
						$imgsn = $ints.".gif";
					}elseif(@$album_item["type"][$i] =="image/pjpeg"||@$album_item["type"][$i] =="image/jpeg"){
						$imgsn = $ints.".jpg";
					}
					
					
					if(!empty($imgsn)) {
					
						@copy( $album_item["tmp_name"][$i] , PATH . "/uploads/contents/{$imgsn}" );
						
						// CHECK SIZE IMAGE
						$size = getimagesize (PATH . "/uploads/contents/{$imgsn}" ) ; 
						if($size[0] > 1200 ){
							$url = "http://{$_SERVER["SERVER_NAME"]}/timthumb.php?src=/uploads/contents/{$imgsn}&w=1200&zc=0";
							$img = PATH . "/uploads/contents/{$imgsn}" ;
							file_put_contents($img, file_get_contents($url));
						}
						// END CHECK IMAGE
						
						$aitem_title = $mysqli->real_escape_string($album_item["name"][$i]); 
						$aitem_path = "{$imgsn}"  ;
						
						$sql = "INSERT INTO cmp_album_item (admin_id 
																, aitem_type
																, album_id 
																, aitem_title 
																, aitem_path 
																, createdate
																, updatedate 
																, aitem_status ) 
																VALUES 
																(
																'{$admin_id}'
																, 'images'
																, '{$album_id}'
																, '{$aitem_title}'
																, '{$aitem_path}'
																, NOW() 
																, NOW() 
																, 1 ) " ;
						$mysqli->query($sql);
						echo $mysqli->error ; 
					}
				}
			}
		}
		
		//Select Type Sound
		if($album_type == "sound" ){
			$album_item_sound = @$_FILES["album_item_sound"]  ; 
			for($i = 0 ; $i < count($album_item_sound["tmp_name"])  ;$i ++ )
			{
			
				if(!empty($album_item_sound["tmp_name"][$i])) 
				{
					$ints = date("YmdGis") . @random_char(7) ; 
					
					$info = $album_item_sound["name"][$i] ;
					$ext = pathinfo($info, PATHINFO_EXTENSION);
					if(in_array($ext, array( "mp3" )))
					{
						$imgsn = $ints . "." . $ext ; 
					}
					
					unset( $info , $ext) ; 
					 
					
					if(!empty($imgsn)) {
					
						@copy( $album_item_sound["tmp_name"][$i] , PATH . "/uploads/album/sound/{$imgsn}" );
						
						$aitem_title = $mysqli->real_escape_string($album_item_sound["name"][$i]); 
						$aitem_path = "{$imgsn}"  ;
						
						$sql = "INSERT INTO cmp_album_item (admin_id 
																, aitem_type
																, album_id 
																, aitem_title 
																, aitem_path 
																, createdate
																, updatedate 
																, aitem_status ) 
																VALUES 
																(
																'{$admin_id}'
																, 'sound'
																, '{$album_id}'
																, '{$aitem_title}'
																, '{$aitem_path}'
																, NOW() 
																, NOW() 
																, 1 ) " ;
						$mysqli->query($sql);
						echo $mysqli->error ; 
					}
				}
			}
		}
		
		//Select Type Video
		if($album_type == "video")
		{
			$album_item_youtube = @$_POST["album_item_youtube"]  ;
			for($i = 0 ; $i < count($album_item_youtube)  ;$i ++ )
			{			
				if(!empty($album_item_youtube[$i])) 
				{
					
					$aitem_title = $mysqli->real_escape_string($album_item_youtube[$i]); 
					$aitem_path = $mysqli->real_escape_string($album_item_youtube[$i]); 
					
					$sql = "INSERT INTO cmp_album_item (admin_id 
															, aitem_type
															, album_id 
															, aitem_title 
															, aitem_path 
															, createdate
															, updatedate 
															, aitem_status ) 
															VALUES 
															(
															'{$admin_id}'
															, 'video'
															, '{$album_id}'
															, '{$aitem_title}'
															, '{$aitem_path}'
															, NOW() 
															, NOW() 
															, 1 ) " ;
					$mysqli->query($sql);
					echo $mysqli->error ; 
				}
			}
		}
		
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("album"))  )
	{
		$album_id = (int) @$_GET["aid"] ;  
		
		$sql = "SELECT aitem_path , aitem_type FROM cmp_album_item WHERE album_id = '{$album_id}' " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		
		while( $item = $rs->fetch_assoc()){
			if($item["aitem_type"] == "images"){
				@unlink(PATH . "/uploads/contents/{$item["aitem_path"]}" ) ; 
			}elseif($item["aitem_type"] == "sound"){
				@unlink(PATH . "/uploads/album/sound/{$item["aitem_path"]}" ) ; 
			}
		}
		
 		$sql = "DELETE FROM cmp_album WHERE album_id = {$album_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		
		$sql = "DELETE FROM cmp_album_item WHERE album_id = {$album_id} "; 
		$mysqli->query($sql);
		echo $mysqli->error ;
		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("album-delete" , "ลบอัลบั้ม"  , $album_id    ) ;
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("item"))  )
	{
 		$aitem_id = (int) @$_GET["iaid"] ;  
		$album_id = (int) @$_GET["aid"] ;  
		
		$sql = "SELECT aitem_path , aitem_type FROM cmp_album_item WHERE aitem_id = '{$aitem_id}' LIMIT 0 , 1  " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		
		while( $item = $rs->fetch_assoc()){
			if($item["aitem_type"] == "images"){
				@unlink(PATH . "/uploads/contents/{$item["aitem_path"]}" ) ; 
			}elseif($item["aitem_type"] == "sound"){
				@unlink(PATH . "/uploads/album/sound/{$item["aitem_path"]}" ) ; 
			}
		}
		
		$sql = "DELETE FROM cmp_album_item WHERE aitem_id = {$aitem_id} "; 
		$mysqli->query($sql);
		echo $mysqli->error ;
		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?in=document&do=edit&aid={$album_id}\"</script>" ;
		exit();
	}
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<style>
		#input_item_youtube_list li {margin-bottom:10px;}
		
		.video-container {
			position: relative;
			padding-bottom: 56.25%;
			padding-top: 30px; height: 0; overflow: hidden;
		}
		 
		.video-container iframe,
		.video-container object,
		.video-container embed {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
		}
	</style>
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		$("#frm_album").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
		
		var select_album_type = function (){
			var type = $("#album_type").val();
			$(".form-type").hide();
			$(".form-type-" +  type ).show();
		}
		$("#album_type").change(select_album_type) ; 
		select_album_type() ; 
		
		
		
		var add_input_item_youtube_list = function (){
			$("#input_item_youtube_list").append('<li><input type="text" class="form-control" id="album_item_youtube" name="album_item_youtube[]" ></li>') ; 
			return false ; 
 		}
		
		$("#btn_add_input_item_yotube").click(add_input_item_youtube_list) ; 
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$album_id = (int) @$_GET["aid"] ; 
			$sql = "SELECT * FROM cmp_album WHERE album_id = {$album_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "อัลบั้มภาพ" );
		$smarty->assign("page_title", "อัลบั้มภาพ" );
 		$smarty->assign("breadcrumb", array("อัลบั้มภาพ" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$album_id = (int) @$row["album_id"]  ; 
		
		
		$publicdate = @$row["publicdate"] ; 
		$publicdate = empty($publicdate) ? date("Y-m-d H:i:s") : $publicdate ; 
		$publicdatee_time = strtotime($publicdate);
		
		$publicdate_time_date =  date("Y-m-d" , $publicdatee_time ) ; 
		$publicdate_time_hour = (int) date("H" , $publicdatee_time ) ; 
		$publicdate_time_min = (int) date("i" , $publicdatee_time ) ; 
		
		$album_type_option = array("picture" => "ภาพ" , "sound" => "เสียง"  , "video" => "วีดีโอ"  );
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">อัลบั้มภาพ</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_album" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="album_id" value="<?php echo $album_id ;  ?>"/>
  						
						<div class="form-group">
							<label for="album_title" class="col-sm-3 control-label">ชื่ออัลบั้มภาพ</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="album_title" name="album_title" placeholder="" value="<?php echo htmlspecialchars(@$row["album_title"]) ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="album_detail" class="col-sm-3 control-label">รายละเอียด</label>
							<div class="col-sm-9">
								<textarea  class="form-control" id="album_detail" name="album_detail" rows="5"><?php echo  htmlspecialchars(@$row["album_detail"]) ?></textarea>
 							</div>
						</div>
 
						<div class="form-group">
							<label for="publicdate" class="col-sm-3 control-label">วันที่แผยแผ่</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control date-picker" id="publicdate" name="publicdate" placeholder="" value="<?php echo $publicdate_time_date  ?>" style="width:100px;float:left" >
 								<select class="form-control" id="publicdate_time_hour" name="publicdate_time_hour" style="width:75px;float:left;margin-left:10px;" >
								   <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 24 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_hour == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
								<div style="float:left; padding: 4px;" > : </div>
								<select class="form-control" id="publicdate_time_min" name="publicdate_time_min" style="width:75px;float:left"  >
								   <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 60 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_min == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="album_detail" class="col-sm-3 control-label">ประเภท</label>
							<div class="col-sm-5">
								<select class="form-control" id="album_type" name="album_type" >
									<?php foreach($album_type_option as  $key => $value ){ ?>
									<option value="<?php echo $key ?>" <?php echo $row["album_type"] == $key ? "selected" : "" ;    ?> ><?php echo $value ?></option>
									<?php } ?>
								</select>
 							</div>
						</div>
						
						<div class="form-group form-type form-type-picture" style="display:none;" >
							<label for="album_item" class="col-sm-3 control-label">รูปภาพประกอบ</label>
  							<div class="col-sm-9">
								<input type="file" id="album_item" name="album_item[]" multiple >
  							</div>
						</div>
						
						<div class="form-group form-type form-type-video " style="display:none;" >  
							<label for="album_item_youtube" class="col-sm-3 control-label">วิดีโอประกอบ <br /> (Youtube)</label>
  							<div class="col-sm-4">
								<ul id="input_item_youtube_list" style="margin:0;padding:0;list-style-type:none;" >
									<li><input type="text" class="form-control" id="album_item_youtube" name="album_item_youtube[]" ></li>
								</ul>
								<br />
								<a href="#" id="btn_add_input_item_yotube" class="btn btn-success">เพิ่มช่อง</a>
  							</div>
						</div>
						
						<div class="form-group form-type form-type-sound" style="display:none;" >  
							<label for="album_item_sound" class="col-sm-3 control-label">เสียง </label>
  							<div class="col-sm-9">
								<input type="file" id="album_item_sound" name="album_item_sound[]" multiple >
  							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="album_status" name="album_status" <?php echo $row["album_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
					<?php if(@$_GET["do"] == "edit"){ ?>
						<?php if($row["album_type"] == "picture" ){ ?>
						<h4>ภาพ</h4>
						<div id="sortable" class="row" >
						<?php 
						$sql = "SELECT * FROM cmp_album_item WHERE album_id = '{$album_id}' AND aitem_type = 'images' " ; 	
						$rs = $mysqli->query($sql);
						while($item = $rs->fetch_assoc()){ 
						?>
						 <div class="col-sm-2 sortable" style="height:400px;  background-color: #FFF;">
							<input type="hidden" name="album_id[]" value="<?php echo @$row["album_id"] ?>" />
							<div style="padding:10px;border:1px solid #CCC ">
								<div >
									<?php if(empty($item["aitem_path"] )){  ?>
									<div><img src="/timthumb.php?src=/images/blank.png&h=300&w=300" class="img-responsive" style="margin:auto"/></div>
									<?php }else{ ?>
									<div><img src="/timthumb.php?src=/uploads/contents/<?php echo $item["aitem_path"] ?>&h=300&w=300" class="img-responsive" style="margin:auto"/></div>
									<?php }?>
								</div>
								<div style="padding-top:10px">
									<div class="row">
										<div class="col-xs-12">
											<a href="?in=item&do=delete&aid=<?php echo @$item["album_id"] ?>&iaid=<?php echo @$item["aitem_id"] ?>" class="btn btn-danger btn-block" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " >ลบข้อมูล</a>
										</div>
									</div>
								</div>
							 </div>
						 </div>
						 <?php } ?>
						</div>
						<?php } ?>
						
						<!-- Sound -->
						<?php if($row["album_type"] == "sound" ){ ?>
						<h4>Sound</h4>
						<div id="sortable" class="row" >
						<?php 
						$sql = "SELECT * FROM cmp_album_item WHERE album_id = '{$album_id}' AND aitem_type = 'sound' " ; 	
						$rs = $mysqli->query($sql);
						while($item = $rs->fetch_assoc()){ 
						?>
						 <div class="col-sm-2 sortable" style="height:400px;  background-color: #FFF;">
 							<div style="padding:10px;border:1px solid #CCC ">
								<div style="height:30px;" >
									<audio controls style="height:100%;width:100%">
										<source src="/uploads/album/sound/<?php echo $item["aitem_path"] ?>" type="audio/mpeg">
										Your browser does not support the audio element.
									</audio>
 								</div>
								<div style="padding-top:10px">
									<div class="row">
										<div class="col-xs-12">
											<a href="?in=item&do=delete&aid=<?php echo @$item["album_id"] ?>&iaid=<?php echo @$item["aitem_id"] ?>" class="btn btn-danger btn-block" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " >ลบข้อมูล</a>
										</div>
									</div>
								</div>
							 </div>
						 </div>
						 <?php } ?>
						</div>
						<?php } ?>
						
						<!-- VDO -->
						<?php if($row["album_type"] == "video" ){ ?>
						<h4>VDO</h4>
						<div id="sortable" class="row" >
						<?php 
						$sql = "SELECT * FROM cmp_album_item WHERE album_id = '{$album_id}' AND aitem_type = 'video' " ; 	
						$rs = $mysqli->query($sql);
						while($item = $rs->fetch_assoc()){ 
						?>
						 <div class="col-sm-2 sortable" style="height:400px;  background-color: #FFF;">
 							<div style="padding:10px;border:1px solid #CCC ">
								<div style="height:155px;" >
									<div class="video-container" >
										<?php echo url2youtubeemb2($item["aitem_path"])  ?>
									</div>
 								</div>
								<div style="padding-top:10px">
									<div class="row">
										<div class="col-xs-12">
											<a href="?in=item&do=delete&aid=<?php echo @$item["album_id"] ?>&iaid=<?php echo @$item["aitem_id"] ?>" class="btn btn-danger btn-block" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " >ลบข้อมูล</a>
										</div>
									</div>
								</div>
							 </div>
						 </div>
						 <?php } ?>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "อัลบั้มภาพ" );
		$smarty->assign("page_title", "อัลบั้มภาพ" );
		$smarty->assign("breadcrumb", array("อัลบั้มภาพ" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(album_id) as _c FROM cmp_album" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 12 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_album ORDER BY publicdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">อัลบั้มภาพ</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  >อัลบั้มภาพ</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<div id="sortable" class="row" >
				 <?php while($row = $rs->fetch_assoc()){ ?>
				 <div class="col-sm-3 sortable" style="min-height: 100px;;  background-color: #FFF;margin-bottom:20px;">
					<input type="hidden" name="album_id[]" value="<?php echo @$row["album_id"] ?>" />
					 
					<div style="padding:10px;border:1px solid #CCC ">
 						<div >
							<?php 
								$sql = "SELECT * FROM cmp_album_item WHERE album_id = {$row["album_id"]} AND aitem_type = 'images' LIMIT 0 , 1 " ;
								$rs2 = $mysqli->query($sql);
								$item = $rs2->fetch_assoc();
							?>
							<?php if(empty($item["aitem_path"])){  ?>
							<div><img src="/timthumb.php?src=/images/blank.png&h=300&w=360" class="img-responsive" style="margin:auto"/></div>
							<?php }else{ ?>
							<div><img src="/timthumb.php?src=/uploads/contents/<?php echo $item["aitem_path"] ?>&h=300&w=360" class="img-responsive" style="margin:auto"/></div>
							<?php }?>
						</div>
						<div style="padding-top:10px">
							<div class="row">
								<div class="col-xs-6">
									<a href="?in=album&do=edit&aid=<?php echo @$row["album_id"] ?>" class="btn btn-success btn-block"  >แก้ไข</a>
								</div>
								<div class="col-xs-6">
									<a href="?in=album&do=delete&aid=<?php echo @$row["album_id"] ?>" class="btn btn-danger btn-block" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " >ลบข้อมูล</a>
								</div>
							</div>
						</div>
						<div class="text-center" style="padding-top:10px;height:46px;overflow:hidden;" >
							<strong><?php echo @$row["album_title"] ?></strong>
						</div>
						
 					 </div>
				 </div>
				 <?php } ?>
 			 </div>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 