<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$link_id = (int) @$_POST["link_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
  		$link_title = @$mysqli->real_escape_string(@$_POST["link_title"]) ;
		$link_url = @$mysqli->real_escape_string(@$_POST["link_url"]) ;
 
		$ban_picture = ""; 
		 
			
  		$link_status = (int) @$_POST["link_status"] ; 
		
		
	 
		if(empty($link_id))
		{
			$sql = "INSERT INTO cmp_link 
											( 
											admin_id
											, link_title 
 											, link_url 
   											, createdate
											, updatedate
											, link_status 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$link_title}'
 											, '{$link_url}'
 											, NOW() 
											, NOW() 
											, {$link_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$link_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("link-add" , "เพิ่มลิงค์ที่เกี่ยวข้อง " , $link_id ) ;
		}
		else
		{	
  			$sql = "UPDATE cmp_link SET link_title = '{$link_title}' 
												, link_url = '{$link_url}'  
   												, updatedate = NOW() 
												, link_status = {$link_status}
												WHERE 
												link_id = {$link_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("link-edit" , "แก้ไขลิงค์ที่เกี่ยวข้อง " , $link_id ) ;
			
		}
		 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("link"))  )
	{
		$link_id = (int) @$_GET["bid"] ;  
 		$sql = "DELETE FROM cmp_link WHERE link_id = {$link_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("link-delete" , "ลบลิงค์ที่เกี่ยวข้อง " , $link_id ) ;
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		$("#frm_banner").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$link_id = (int) @$_GET["bid"] ; 
			$sql = "SELECT * FROM cmp_link WHERE link_id = {$link_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "ลิงค์ที่เกี่ยวข้อง" );
		$smarty->assign("page_title", "ลิงค์ที่เกี่ยวข้อง" );
 		$smarty->assign("breadcrumb", array("ลิงค์ที่เกี่ยวข้อง" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$link_id = (int) @$row["link_id"]  ; 
		
		$banner_status = (int) @$row["banner_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ลิงค์ที่เกี่ยวข้อง</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_album" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="link_id" value="<?php echo $link_id ;  ?>"/>
  						
						<div class="form-group">
							<label for="link_title" class="col-sm-3 control-label">ชื่อลิงค์ที่เกี่ยวข้อง</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="link_title" name="link_title" placeholder="" value="<?php echo @$row["link_title"] ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="link_url" class="col-sm-3 control-label">ลิงค์ที่เกี่ยวข้อง</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="link_url" name="link_url" placeholder="" value="<?php echo @$row["link_url"] ?>" required="required" >
  							</div>
						</div>
 
  
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="link_status" name="link_status" <?php echo $row["link_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "ลิงค์ที่เกี่ยวข้อง" );
		$smarty->assign("page_title", "ลิงค์ที่เกี่ยวข้อง" );
		$smarty->assign("breadcrumb", array("ลิงค์ที่เกี่ยวข้อง" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(link_id) as _c FROM cmp_link" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_link ORDER BY createdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">ลิงค์ที่เกี่ยวข้อง</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  ><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
 						<th class="text-center" >หัวข้อ</th>
 						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i ++  ?></td>
                         <td><a target="_blank" href="<?php echo $row["link_url"] ;  ?>" ><?php echo @$row["link_title"] ?></a></td>
 						<td style="text-align:center" >
							<a href="?in=link&do=edit&bid=<?php echo @$row["link_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=link&do=delete&bid=<?php echo @$row["link_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 