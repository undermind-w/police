<?php
	session_start();
	require "config.inc.php" ;
 	require "session.inc.php" ;
	require "menu.inc.php" ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($do == "cat-form-set") {
			$cat_name = $mysqli->real_escape_string($_POST["cat_name"]);
			if(empty($_POST["cat_id"])) {
					$id = genid();
					$sql = "INSERT INTO cmp_document_cat(cat_id, cat_name, update_time, update_ip)
										VALUES('{$id}', '{$cat_name}', NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
					$mysqli->query($sql);
			} else {
					$sql = "UPDATE cmp_document_cat SET
											cat_name		= '{$cat_name}'
										, update_time = NOW()
										, update_ip		= '{$_SERVER["REMOTE_ADDR"]}'
									WHERE cat_id 		= '{$_POST["cat_id"]}'";
					$mysqli->query($sql);
			}
			echo "<script>window.location.href = '?manage_cat';</script>";
			exit();
	}

	if($do == "document-form-set") {

 		$doc_id = (int) @$_POST["doc_id"] ;
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
  	$doc_title = @$mysqli->real_escape_string(@$_POST["doc_title"]) ;
		$doc_detail = @$mysqli->real_escape_string(@$_POST["doc_detail"]) ;
		$cat_id = @$mysqli->real_escape_string(@$_POST["cat_id"]) ;

		$publicdate_date = @$mysqli->real_escape_string(@$_POST["publicdate"]) ;

		$publicdate_time_hour = (int) @$_POST["publicdate_time_hour"] ;
		$publicdate_time_min = (int) @$_POST["publicdate_time_min"] ;

		$publicdate_time_hour = sprintf("%02d",  $publicdate_time_hour  ) ;
		$publicdate_time_min = sprintf("%02d",  $publicdate_time_min  ) ;
		$publicdate  = $publicdate_date . " " . $publicdate_time_hour . ":" . $publicdate_time_min . ":00" ;



  		$doc_status = (int) @$_POST["doc_status"] ;
			$doc_black_status = (int) @$_POST["doc_black_status"] ;



		if(empty($doc_id))
		{
            $doc_seq = 0 ;
            $sql = "INSERT INTO cmp_document_group
											(
											admin_id
											, doc_seq
											, cat_id
											, doc_title
											, publicdate
  											, createdate
											, updatedate
											, doc_status
											,doc_black_status
											)
											VALUES
											(
											'{$admin_id}'
											, '{$doc_seq}'
											, '{$cat_id}'
											, '{$doc_title}'
											, '{$publicdate}'
 											, NOW()
											, NOW()
											, {$doc_status}
											,{$doc_black_status}) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$doc_id = (int) $mysqli->insert_id ;
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;

		}
		else
		{
 			$sql = "UPDATE cmp_document_group SET
													doc_title 	= '{$doc_title}'
												, cat_id			= '{$cat_id}'
												, publicdate 	= '{$publicdate}'
 												, updatedate 	= NOW()
												, doc_status 	= {$doc_status}
												, doc_black_status 	= {$doc_black_status}
												WHERE doc_id 	= {$doc_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;
		}

		if(!is_dir(PATH . "/uploads/document/{$doc_id}")){
			mkdir(PATH."/uploads/document/{$doc_id}", 0777) or die("err");
		}


		$doc_item = @$_FILES["doc_item"]  ;


		for($i = 0 ; $i < count($doc_item["tmp_name"])  ;$i ++ )
		{
			if(!empty($doc_item["tmp_name"][$i]))
			{
				$ints = date("YmdGis") . @random_char(7) ;
				$info = $doc_item["name"][$i] ;
				$ext = pathinfo($info, PATHINFO_EXTENSION);
				if(in_array($ext, array("doc" , "docx" , "pdf" , "zip" , "rar" )))
				{
					$imgsn = $ints . "." . $ext ;

					@copy( $doc_item["tmp_name"][$i] , PATH . "/uploads/document/{$doc_id}/{$imgsn}" );

					$idoc_title = $mysqli->real_escape_string($doc_item["name"][$i]);
					$idoc_path = "{$imgsn}"  ;

					$sql = "INSERT INTO cmp_document_item (admin_id
															, doc_id
 															, idoc_title
															, idoc_path
															, createdate
 															, updatedate
															, idoc_status )
															VALUES
															(
															'{$admin_id}'
															, '{$doc_id}'
															, '{$idoc_title}'
															, '{$idoc_path}'
															, NOW()
 															, NOW()
															, 1 ) " ;
					$mysqli->query($sql);
					echo $mysqli->error ;
				}
			}
		}


		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}

	if($_POST) exit();
	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	if(isset($_GET["manage_cat"]) && $do == "del") {
			$mysqli->query("DELETE FROM cmp_document_cat WHERE cat_id = '{$_GET["id"]}'");
			$mysqli->query("OPTIMIZE TABLE cmp_document_cat");
			echo "<script>window.location.href = '?manage_cat';</script>";
			exit();
	}


	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("document"))  )
	{
		$doc_id = (int) @$_GET["did"] ;
 		$sql = "DELETE FROM cmp_document_group WHERE doc_id = {$doc_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;

		$sql = "DELETE FROM cmp_document_item WHERE doc_id = {$doc_id} ";
		$mysqli->query($sql);
		echo $mysqli->error ;

		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ;
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}

	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("item"))  )
	{
 		$idoc_id = (int) @$_GET["idid"] ;
		$doc_id = (int) @$_GET["did"] ;

		$sql = "DELETE FROM cmp_document_item WHERE idoc_id = {$idoc_id} ";
		$mysqli->query($sql);
		echo $mysqli->error ;

		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ;
		echo "<script> window.location.href = \"?in=document&do=edit&did={$doc_id}\"</script>" ;
		exit();
	}

    #เรียงลำดับข้อมูล
    if(@in_array(@$_GET["do"],array("down","up"))) {
        $sql = "SELECT doc_id , doc_seq FROM cmp_document_group ORDER BY doc_seq ASC ";
        $rs = $mysqli->query($sql);
        echo $mysqli->error ;
        $i = 0;
        while(($row = $rs->fetch_assoc())) {
            if($row["doc_id"]== @$_GET["sid"]) {
                $pos = $i;
            } else{
                $array[$i++] = $row["doc_id"];
            }
        }

        if(@$_GET["do"]=="down") {
            $pos++;
        } else {
            $pos--;
        }


        $count = count($array)+1;

        for($i=0;$i<$count;$i++) {
            if($i == $pos) {
                $tmp[$i] = @$_GET["sid"];
            } else {
                $tmp[$i] = @$array[0];
                array_shift($array);
            }
        }
        for($i = 0 ; $i<count($tmp) ;$i++) {
            $index = $i + 1 ;
            $sql = "UPDATE cmp_document_group SET doc_seq = '{$index}'  WHERE doc_id = '{$tmp[$i]}'";
            $mysqli->query($sql);
            echo $mysqli->error;
        }

        $msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;
        echo "<script type=\"text/javascript\">document.location.href='?';</script>";
        exit();
    }
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/frontend/css/margin.css" rel="stylesheet" type="text/css"/>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	<style type="text/css">
			.date-picker {
					cursor: pointer;
					/* width: 250px; */
			}
			[id*="publicdate_time"] {
					cursor: pointer;
					width: 150px;
			}
 </style>
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
	$(function(){
		$("#frm_document").validate();
				$(".date-picker").datepicker({
					orientation: "top auto" ,
					autoclose: true ,
					format : "yyyy-mm-dd"
				});
	})
</script>

<script>
		$(document).ready(function() {
				$("body").on("click", ".btn-cat-submit", function() {
						$(".group-error").remove();
						if($("#cat_name").val() == "") {
								$(".group-cat").addClass("has-error");
								$("#cat_name").after("<p class='help-block text-danger group-error'>กรุณาใส่ชื่อหมวดหมู่</p>")
						} else {
								$(".group-cat").removeClass("has-error");
								$("#frm_cat").submit();
						}
				});

				$("body").on("change", "#filter", function() {
						document.location.href = "document.php?cid=" + $(this).val();
				});
		});
</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 ob_start();

 ?>
 <?php
		if(isset($_GET["manage_cat"])) {
				$smarty->assign("title", "หมวดหมู่เอกสารดาวน์โหลด" );
				$smarty->assign("page_title", "หมวดหมู่เอกสารดาวน์โหลด" );
				$breadcrumb = array();
				$breadcrumb["หมวดหมู่เอกสารดาวน์โหลด"] = "?";

				if(!isset($_GET["do"])) {

		$page = (int) @$_GET["page"]  ;

		$sql = "SELECT COUNT(cat_id) as _c FROM cmp_document_cat" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;

		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ;
		$per_page = 20 ;
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();

		$page_start = (($per_page * $page) - $per_page ) ;
		unset($sql, $rs, $row);
		$sql = "SELECT tb1.cat_id, tb1.cat_name, COUNT(tb2.doc_id) AS doc_num
							FROM  cmp_document_cat AS tb1
							LEFT OUTER JOIN cmp_document_group AS tb2 ON CONVERT(tb1.cat_id USING UTF8) = CONVERT(tb2.cat_id USING UTF8)
						GROUP BY tb1.cat_id, tb1.cat_name
						ORDER BY CONVERT(tb1.cat_name USING TIS620) ASC
						LIMIT {$page_start}, {$per_page}" ;
		$rs = $mysqli->query($sql);
		$i =  1;
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">หมวดหมู่เอกสารดาวน์โหลด</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-default pull-left" href="?">ย้อนกลับไป จัดการเอกสาร</a>
			 <a class="btn btn-info pull-right" href="?manage_cat&do=add">เพิ่มข้อมูลหมวดหมู่</a>
			 <div class="clearfix m-b-md"></div>

			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="60">ลำดับ</th>
						<th> หมวดหมู่เอกสาร </th>
						<th class="text-center" width="120">จำนวนเอกสาร</th>
						<th class="text-center" width="100">จัดการ</th>

					</tr>
				</thead>
				<tbody>
					<?php
							while(($row = $rs->fetch_assoc())) {
									if($row["cat_id"] != null) {
					?>
					<tr>
							<td class="text-center"><?php echo $i++  ?></td>
							<td><?php echo $row["cat_name"] ?></td>
							<td class="text-center"><?php echo number_format($row["doc_num"], 0); ?></td>
							<td class="text-center">
									<a href="?manage_cat&do=edit&id=<?php echo $row["cat_id"] ?>" >แก้ไข</a>
									<?php if(!$row["doc_num"]) { ?>
									|
									<a href="?manage_cat&do=del&id=<?php  echo $row["cat_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้');">ลบ</a>
									<?php } ?>
							</td>
					</tr>
					<?php
							} }
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
<?php

				} else {
						if(isset($_GET["id"])) {
								$sql = "SELECT * FROM cmp_document_cat WHERE cat_id = '{$_GET["id"]}'";
								$rs = $mysqli->query($sql);
								$row =$rs->fetch_assoc();
						}

?>
<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">หมวดหมู่เอกสารดาวน์โหลด</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_cat" class="form-horizontal" action="" method="post"  enctype="multipart/form-data" >


						<div class="form-group group-cat">
							<label for="doc_title" class="col-sm-3 control-label">ชื่อหมวดหมู่</label>
							<div class="col-sm-6">
								<input type="text"  class="form-control" id="cat_name" name="cat_name" placeholder="ชื่อหมวดหมู่" value="<?php echo isset($row["cat_name"]) ? $row["cat_name"] : "" ?>" required="required" />
 							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button class="btn btn-success btn-cat-submit" type="button">บันทึกข้อมูล</button>
								<input type="hidden" name="do" 		value="cat-form-set" />
								<input type="hidden" name="cat_id" value="<?php echo isset($row["cat_id"]) ? $row["cat_id"] : "" ?>"/>
								<a class="btn btn-danger m-l-xxs" href="?manage_cat">ยกเลิก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
<?php
				}
		} else {
			$smarty->assign("title", "เอกสารดาวน์โหลด" );
			$smarty->assign("page_title", "เอกสารดาวน์โหลด" );
			$breadcrumb = array();
			$breadcrumb["เอกสารดาวน์โหลด"] = "?";
 ?>
 <?php
	if(@in_array($_GET["do"] , array("add" , "edit") ) ) {
		$breadcrumb[$_GET["do"] == "add" ? "เพิ่มข้อมูล" : "แก้ไขข้อมูล"] = "#";
		if(@in_array($_GET["do"] , array("edit"))){
			$doc_id = (int) @$_GET["did"] ;
			$sql = "SELECT * FROM cmp_document_group WHERE doc_id = {$doc_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error;
			$row = $rs->fetch_assoc() ;
		}




		$doc_id = (int) @$row["doc_id"]  ;


		$publicdate = @$row["publicdate"] ;
		$publicdate = empty($publicdate) ? date("Y-m-d H:i:s") : $publicdate ;
		$publicdatee_time = strtotime($publicdate);

		$publicdate_time_date =  date("Y-m-d" , $publicdatee_time ) ;
		$publicdate_time_hour = (int) date("H" , $publicdatee_time ) ;
		$publicdate_time_min = (int) date("i" , $publicdatee_time ) ;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">เอกสารดาวน์โหลด</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_document" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="doc_id" value="<?php echo $doc_id ;  ?>"/>
						<div class="form-group">
							<label for="doc_title" class="col-sm-3 control-label">หมวดหมู่</label>
							<div class="col-sm-6">
									<select class="form-control" id="cat_id" name="cat_id">
									<?php
											$id = isset($row["cat_id"]) ? $row["cat_id"] : "" ;
											echo "<option value='0' selected disabled>เลือกหมวดหมู่</option>";
											$sql = "SELECT *
															FROM cmp_document_cat
															ORDER BY CONVERT(cat_name USING TIS620)";
											$rs_cat = $mysqli->query($sql);
											while(($row_cat  = $rs_cat->fetch_assoc())) {
													$selected = $row_cat["cat_id"] == $id ? " selected" : "";
													echo "<option value='{$row_cat["cat_id"]}'{$selected}>{$row_cat["cat_name"]}</option>";
											}
									?>
									</select>
 							</div>
							<div class="col-sm-3">
									<a class="btn btn-info btn-block btn-manage-cat" href="?manage_cat">คลิกที่นี่เพื่อจัดการหมวดหมู่</a>
							</div>
						</div>
						<div class="form-group">
							<label for="doc_title" class="col-sm-3 control-label">ชื่อเอกสาร</label>
							<div class="col-sm-6">
								<input type="text"  class="form-control" id="doc_title" name="doc_title" placeholder="" value="<?php echo @$row["doc_title"] ?>" required="required" >
 							</div>
						</div>
						<div class="form-group">
							<label for="publicdate" class="col-sm-3 control-label">วันที่แผยแผ่</label>
							<div class="col-sm-9">
								<input type="text" class="form-control date-picker" id="publicdate" name="publicdate" placeholder="" value="<?php echo $publicdate_time_date  ?>" style="width:150px;float:left" >
								<div class="pull-left control-label">&nbsp;เวลา&nbsp;</div>

								<select class="form-control pull-left" id="publicdate_time_hour" name="publicdate_time_hour" >
								   <option value="" disabled="disabled" selected="">กรุณาเลือกชั่วโมง</option>
									<?php for($i = 0 ; $i < 24 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_hour == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
								<div class="pull-left control-label">&nbsp;:&nbsp;</div>
								<select class="form-control pull-left" id="publicdate_time_min" name="publicdate_time_min">
								   <option value="" disabled="disabled" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 60 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_min == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="form-group">
								<label for="doc_item" class="col-sm-3 control-label">แนบเอกสาร</label>
  							<div class="col-sm-9">
										<input type="file" id="doc_item" name="doc_item[]" multiple />
										<div class="show-select-file"></div>

								<?php
									$sql = "SELECT * FROM cmp_document_item WHERE doc_id = {$doc_id} ORDER BY createdate ASC " ;
									$rs = $mysqli->query($sql);
									if($rs->num_rows > 0 ){
								?>
								<br />
								<div>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th width="100" style="text-align:center" >ลำดับ</th>
												<th width="" style="text-align:center" > เอกสาร </th>
												<th width="150" style="text-align:center" >ลบข้อมูล</th>

											</tr>
										</thead>
										<tbody>
											<?php
											$i = 1 ;
											while($item = $rs->fetch_assoc()){
											?>
											 <tr>
												<td style="text-align:center" ><?php echo $i ++  ?></td>
												<td><a href="/uploads/document/<?php echo $item["doc_id"]; ?>/<?php echo $item["idoc_path"] ?>" target="_blank"><?php echo @$item["idoc_title"] ?></a></td>
												<td style="text-align:center" >
													<a href="?in=item&do=delete&did=<?php echo @$item["doc_id"] ?>&idid=<?php echo $item["idoc_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  >ลบ</a>
												</td>
											  </tr>
											<?php
												}
											?>
										</tbody>
									</table>
								</div>
									<?php } ?>
  							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label><input type="checkbox" id="doc_status" name="doc_status" <?php echo $row["doc_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">&nbsp;อนุมัติ</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label><input type="checkbox" id="doc_black_status" name="doc_black_status" <?php echo $row["doc_black_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">&nbsp;New tab</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<input type="hidden" name="do" value="document-form-set" />
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ยกเลิก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>

<?php
	} else {
		$type = @$mysqli->real_escape_string($_GET["type"]) ;
		$page = (int) @$_GET["page"]  ;
		$cid = isset($_GET["cid"]) ? $_GET["cid"] : "" ;
		$where = !empty($cid) ? " WHERE tb4.cat_id = '{$cid}' " : "" ;

		$sql = "SELECT COUNT(tb1.doc_id) as _c
						FROM cmp_document_group AS tb1
						LEFT OUTER JOIN cmp_document_cat AS tb4 ON tb1.cat_id = tb4.cat_id
						{$where}";
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;

		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ;
		$per_page = 10 ;
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();

		$page_start = (($per_page * $page) - $per_page ) ;

		$sql = "SELECT tb1.*, tb2.admin_name, tb3.memname AS member_name, tb4.cat_name
							FROM  cmp_document_group AS tb1
							LEFT OUTER JOIN cmp_admin AS tb2 ON tb1.admin_id = tb2.admin_id
							LEFT OUTER JOIN cmp_imp_member AS tb3 ON CONVERT(tb1.member_no USING UTF8) = CONVERT(tb3.member_no USING UTF8)
							LEFT OUTER JOIN cmp_document_cat AS tb4 ON tb1.cat_id = tb4.cat_id
							{$where}
						ORDER BY tb1.doc_seq ASC, CONVERT(tb1.doc_title USING TIS620) ASC
						LIMIT {$page_start}, {$per_page}" ;
		$rs = $mysqli->query($sql);
		//echo $mysqli->error;
		$i =  1;
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">เอกสารดาวน์โหลด</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-left" href="?manage_cat">คลิกที่นี่เพื่อจัดการหมวดหมู่</a>
			 <a class="btn btn-info pull-right" href="?do=add">เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>

			<div class="form-inline" style="margin-bottom: 20px;">
				<div class="form-group">
						<select class="form-control" id="filter">
								<option value="">แสดงทุกหมวด</option>
								<?php
										$sql = "SELECT cat_id, cat_name
														FROM cmp_document_cat
														ORDER BY CONVERT(cat_name USING TIS620)";
										$rs_f = $mysqli->query($sql);
										while(($row_f = $rs_f->fetch_assoc())) {
												$cid = isset($_GET["cid"]) ? $_GET["cid"] : null ;
												$selected = $row_f["cat_id"] == $cid ? " selected" : "" ;
												echo "<option value='{$row_f["cat_id"]}'{$selected}>{$row_f["cat_name"]}</option>";
										}
								?>
						</select>
				</div>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="50" class="text-center">ลำดับ</th>
                        <th class="text-center" width="100" >เรียงลำดับ</th>
						<th width="225" class="text-center"> วันที่แสดง  </th>
						<th width="150" class="text-center">หมวดหมู่</th>
						<th class="text-center"> ชื่อเอกสาร </th>
						<th width="80" class="text-center">สถานะ</th>
						<th width="150" class="text-center">เพิ่มโดย</th>
						<th width="100" class="text-center">จัดการ</th>

					</tr>
				</thead>
				<tbody>
					<?php
					while($row = $rs->fetch_assoc()){
					?>
					<tr>
							<td class="text-center"><?php echo $i  ?></td>
                            <td style="text-align:center" >
                                <?php if($i != $rs->num_rows ){ ?>
                                    <a href="?do=down&in=slide&sid=<?php echo $row["doc_id"]; ?>"><i class="fa fa-arrow-down"></i></a>
                                <?php } ?>

                                <?php if($i != 0 ){ ?>
                                    <a href="?do=up&in=slide&sid=<?php echo $row["doc_id"]; ?>"><i class="fa fa-arrow-up"></i></a>
                                <?php }  ?>
                            </td>
							<td class="text-center"><?php echo @mysqldate2thaidate(@$row["publicdate"] , "long" , true )  ?></td>
							<td><?php echo @$row["cat_name"] ?></td>
							<td><?php echo @$row["doc_title"] ?></td>
							<td class="text-center"><?php echo $row["doc_status"] ? "<span class='text-success'>อนุมัติ</span>" : "<span class='text-danger'>ไม่อนุมัติ</span>" ; ?></td>
							<td class="text-center"><?php echo !empty($row["admin_id"]) ? "ผู้ดูแลระบบ" : $row["member_name"]; ?></td>
							<td class="text-center">
									<a href="?in=document&do=edit&did=<?php echo @$row["doc_id"] ?>" >แก้ไข</a> |
									<a href="?in=document&do=delete&did=<?php echo @$row["doc_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  >ลบ</a>
							</td>
					</tr>
					<?php
                        $i ++;
						}
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
<?php
				}
		}
		$content = ob_get_contents();
		ob_end_clean();
		$smarty->assign("breadcrumb", $breadcrumb);
		$smarty->assign("content", $content);
		$smarty->display(THEME.".tpl");
