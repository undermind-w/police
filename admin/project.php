<?php
	session_start();
	require "config.inc.php" ;
	require "menu.inc.php" ;
 	require "session.inc.php" ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	 
	if($_POST){
 		$project_id = (int) @$_POST["project_id"] ; 
		
		$project_seq = 0 ; 
 		$project_title = @$mysqli->real_escape_string($_POST["project_title"]) ;
 		$project_detail = @$mysqli->real_escape_string(@htmlspecialchars($_POST["project_detail"], ENT_QUOTES)) ;
		$project_category_id = (int)  @$_POST["project_category_id"]  ;
 		$is_show = (int) @$_POST["is_show"] ; 
		
		
		
 		
		
		if(empty($project_id))
		{
			$sql = "INSERT INTO upc_project 
											( 
											project_category_id
											, project_title 
 											, project_detail
 											, createdate
											, updatedate
 											, is_show 
											) 
											VALUES  
											(
											'{$project_category_id}'
											, '{$project_title}'
											, '{$project_detail}'
 											, NOW() 
											, NOW() 
 											, {$is_show} ) " ;
			$mysqli->query($sql);
			$project_id = (int) @$mysqli->insert_id ; 
			echo $mysqli->error ;
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		else
		{	
 			$sql = "UPDATE upc_project SET project_category_id = '{$project_category_id}' 
												, project_title = '{$project_title}'  
												, project_detail = '{$project_detail}'  
 												, updatedate = NOW() 
 												, is_show = {$is_show}
												WHERE 
												project_id = {$project_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		$icount = (int) @count(@$_FILES["upc_project_item"]["name"]) ; 
 		for($i = 0  ; $i < $icount ; $i++ ){
			if(!empty($_FILES["upc_project_item"]["name"][$i])) {
				$ints = date("YmdGis") . @random_char(7) ;
				if(@$_FILES["upc_project_item"]["type"][$i]=="image/png"||@$_FILES["upc_project_item"]["type"][$i]=="image/x-png")
					$imgsn = $ints.".png";
				if(@$_FILES["upc_project_item"]["type"][$i]=="image/gif")
					$imgsn = $ints.".gif";
				elseif(@$_FILES["upc_project_item"]["type"][$i]=="image/pjpeg"||@$_FILES["upc_project_item"]["type"][$i]=="image/jpeg")
					$imgsn = $ints.".jpg";
				if(!empty($imgsn)) {
					$pitem_path = $imgsn ; 
					$pitem_title = @$mysqli->real_escape_string(@$_FILES["upc_project_item"]["name"][$i]); 
					@copy($_FILES["upc_project_item"]["tmp_name"][$i],PATH."/uploads/contents/{$imgsn}");
					$sql = "INSERT INTO upc_project_item (
														pitem_code 
														, project_id 
														, pitem_title 
														, pitem_path 
														, createdate
														, updatedate
														, is_show
														) 
														VALUES 
														(
														'image'
														, '{$project_id}'
														, '{$pitem_title}'
														, '{$pitem_path}'
														, NOW()
														, NOW()
														, 1  
														)
														" ;
					$mysqli->query($sql);
					echo $mysqli->error ;
					
				}
			}
		}
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array($_GET["do"] , array("delete")) ){
		$project_id = @$_GET["pid"] ;  
 		$sql = "DELETE FROM upc_project WHERE project_id = {$project_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;	
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	if( @in_array($_GET["do"] , array("delimg")) ){
		$project_id = @$_GET["pid"] ;
		$pitem_id = @$_GET["piid"] ;
		
		$sql = "SELECT pitem_path FROM upc_project_item WHERE pitem_id = {$pitem_id} LIMIT 0 ,  1 " ;
		$rs = $mysqli->query($sql) ; 
		echo $mysqli->error ;
		$item = $rs->fetch_assoc() ; 
		@unlink(PATH . "/uploads/contents/" . $item["pitem_path"] );
		
 		$sql = "DELETE FROM upc_project_item WHERE pitem_id = {$pitem_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		
		
		
		$msg->add("s" , "! ลบรูปแล้ว" ) ; 
		echo "<script> window.location.href = \"?do=edit&pid={$project_id}\"</script>" ;
		exit();
	}

	
	if( @in_array($_GET["do"] , array("show")) ){
		$value =  (int) @$_GET["value"] ;
		$project_id =  (int) @$_GET["pid"] ;
		$page = (int) @$_GET["page"] ;
		
		$sql = "UPDATE upc_project_item " ;
		 
		
		
		$msg->add("s" , "! ลบรูปแล้ว" ) ; 
		echo "<script> window.location.href = \"?page={$page}&pid={$project_id}\"</script>" ;
		exit();
	}
 		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>
	<script type="text/javascript" src="/html/backend/assets/plugins/ckeditor/ckeditor.js"></script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 
 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		
		if(@in_array($_GET["do"] , array("edit"))){
			$project_id = (int) $_GET["pid"] ; 
			$sql = "SELECT * FROM upc_project WHERE project_id = {$project_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc() ;
			$btitle= "เพิ่มข้อมูล" ; 
		}else{
			$btitle= "แก้ไขข้อมูล" ; 
			
		}
		
		$smarty->assign("title", "จัดการผลงาน" );
		$smarty->assign("page", "จัดการผลงาน" );
		$smarty->assign("sub", "" );
		$smarty->assign("breadcrumb", array("จัดการผลงาน" => "?" , "{$btitle}" => "#" ) );
		
 		$project_id = (int) @$row["project_id"];  
		
		$project_title = @$row["project_title"];  
		$project_detail = @$row["project_detail"];  
		
		 
		 
		$is_show = (int) @$row["is_show"]  ;  
		
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
		<div class="portlet box green ">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i> ผลงาน
				</div>
			</div>
			<div class="portlet-body form">
				<?php echo $msg->display(); ?>
				<form class="form-horizontal" id="frm_project" name="frm_project" role="form" method="post" action="?"  enctype="multipart/form-data" >
					<input type="hidden" name="do" value="content" />
 					<input type="hidden" name="project_id" value="<?php  echo $project_id  ?>" />
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-3 control-label">ชื่อผลงาน</label>
							<div class="col-md-9">
								<input type="text" id="project_title" name="project_title" class="form-control" placeholder="" value="<?php  echo $project_title   ?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">ประเภทผลงาน</label>
							<div class="col-md-9">
								<select id="project_category_id" name="project_category_id" class="form-control" >
								<?php 
									$sql = "SELECT * FROM upc_category " ; 
									$rs = $mysqli->query($sql);
									while($item = $rs->fetch_assoc()){
								?>
									<option value="<?php echo @$item["category_id"] ?>" <?php echo @$item["category_id"]  == $row["project_category_id"] ?> ><?php echo @$item["category_title"] ?></option>
								<?php
									}
								?>
								</select>
							</div>
						</div>
					 
						
						<div class="form-group">
							<label class="col-md-3 control-label">รายละเอียด </label>
							<div class="col-md-9">
								 <textarea class="ckeditor form-control" id="project_detail" name="project_detail" rows="6"><?php echo $project_detail ?></textarea>
							</div>
						</div>
						
						 
						 <div class="form-group">
							<label class="col-md-3 control-label"> รูปโลโก้  </label>
							<div class="col-md-9">
								<input type="file" id="upc_project_item" name="upc_project_item[]" class="" placeholder="" value="" multiple="multiple" >
								<br />
								สามารถเลือกได้หลายรุป
								<br />
								<?php 
								$sql = "SELECT * FROM upc_project_item WHERE project_id = {$project_id}  AND is_show = 1 " ;
								$rs = $mysqli->query($sql);
								echo $mysqli->error ; 
								while($item = $rs->fetch_assoc()){ 
								?>
								<img src="/uploads/contents/<?php echo $item["pitem_path"] ?>" style="max-width:300px;" />
 								<a href="?do=delimg&pid=<?php echo $item["project_id"] ?>&piid=<?php echo $item["pitem_id"] ?>" ><i class="fa fa-trash-o" style="font-size:20px" ></i></a>
								<br />
								<?php
								} 
								?>
  							</div>
						</div>
						 
						<div class="form-group">
							<label class="col-md-3 control-label">แสดง</label>
							<div class="col-md-9" style="padding-top: 7px;">
								<input type="checkbox" id="is_show" name="is_show" value="1" <?php echo $is_show == 1  ? "checked" : "" ;  ?> >   
 							</div>
						</div>
						 
					</div>
					<div class="form-actions fluid">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn green">บันทึกข้อมูล</button>
							<a href="?" type="button" class="btn default">ยกเลิก</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		$smarty->assign("title", "จัดการผลงาน" );
		$smarty->assign("page", "จัดการผลงาน" );
		$smarty->assign("sub", "" );
		$smarty->assign("breadcrumb", array("จัดการผลงาน" => "#") );
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
		<div class="portlet box green ">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i> จัดการผลงาน
				</div>
			</div>
			<div class="portlet-body">
				<?php echo $msg->display() ?>
				<div style="clear:both"></div>
				<a href="?do=add&type=<?php echo $type ?>" class="btn btn-primary pull-right" style="margin-bottom: 10px;" >เพิ่มข้อมูล</a>
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th  width="100" class="text-center" >ลำดับ</th>
							<th class="text-center"> ชื่อผลงาน</th> 
							<th width="250"  class="text-center"> ประเภท </th> 
							<th width="100" class="text-center"> สถานะ </th>
							<th width="200" class="text-center" > จัดการ </th>
						</tr>
					</thead>
					<tbody>
					<?php 
						$sql = "SELECT COUNT(project_id) as _c FROM upc_project " ;
						$rs = $mysqli->query($sql);
						$count = $rs->fetch_assoc();
						$num_rows = $count["_c"] ; 
						$per_page = 10 ; 
						$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
						
						$pagination = (new Pagination());
						$pagination->setCurrent($page);
						$pagination->setRPP($per_page);
						$pagination->setTotal($num_rows);
						$pagination->setCrumbs(25);
						$paging = $pagination->parse();
						
						$page_start = (($per_page * $page) - $per_page ) ; 
					
						$sql = "SELECT upc_project.project_id 
									, upc_project.project_title 
									, upc_category.category_title 
									, upc_project.is_show 
									FROM upc_project 
										LEFT JOIN upc_category ON upc_project.project_category_id = upc_category.category_id 
									ORDER BY upc_project.createdate DESC   " ;
						$rs = $mysqli->query($sql);
						echo $mysqli->error ; 
						$i = (int) $rs->num_rows ; 
						while($row = $rs->fetch_assoc()){
						?>
						<tr>
							<td class="text-center" ><?php echo $i -- ?></td>
							<td><a href="#"><?php echo @$row["project_title"] ?></a></td>
							<td><a href="#"><?php echo @$row["category_title"] ?></a></td>
							<td class="text-center">
								<?php 
								if($row["is_show"] == 1 ){
									echo "<a href=\"?do=show&pid={$row["project_id"]}&value=0&page={$page}\" class=\"btn blue btn-xs\">แสดง</a>" ; 
								}else{ 
									echo "<a href=\"?do=show&pid={$row["project_id"]}&value=1&page={$page}\" class=\"btn red btn-xs\">ซ่อน</a>" ; 
								}
								?>
							</td>
							<td class="text-center" >
								<a href="?do=edit&pid=<?php echo @$row["project_id"] ?>" >แก้ไข</a> | 
								<a href="?do=delete&pid=<?php echo @$row["project_id"] ?>" >ลบข้อมูล</a>
							</td>
						</tr>
						<?php 
						}
						if( $rs->num_rows  == 0 ){
							echo "<tr><td colspan=\"5\" class=\"text-center\" >ไม่มีข้อมูลที่ท่านต้องการ</td></tr>" ;
							
						}
					?>
					
					</tbody>
					</table>
				</div>
			</div>
		</div>
		</div>
	</div>
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 