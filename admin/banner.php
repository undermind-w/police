<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	$cmpper->check("website") ;
 
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$ban_id = (int) @$_POST["ban_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
  		$ban_title = @$mysqli->real_escape_string(@$_POST["ban_title"]) ;
		$ban_url = @$mysqli->real_escape_string(@$_POST["ban_url"]) ;
	$imgsn = "" ; 
		$ban_picture = ""; 
		if(!empty($_FILES["ban_picture"]["tmp_name"] )) {
			$ints = date("YmdGis") . @random_char(7) ; 
			
			if($_FILES["ban_picture"]["type"] =="image/png"||$_FILES["ban_picture"]["type"] =="image/x-png"){
				$imgsn = $ints.".png";
			}elseif($_FILES["ban_picture"]["type"] =="image/gif"){
				$imgsn = $ints.".gif";
			}elseif($_FILES["ban_picture"]["type"] =="image/pjpeg"||$_FILES["ban_picture"]["type"] =="image/jpeg"){
				$imgsn = $ints.".jpg";
			}
			if(!empty($imgsn)) {
				@copy($_FILES["ban_picture"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
				$ban_picture = "{$imgsn}"  ;
			}
		}
		$imgsn = "" ; 
		$ban_background = ""; 
		if(!empty($_FILES["ban_background"]["tmp_name"] )) {
			$ints = date("YmdGis") . @random_char(7) ; 
			
			if($_FILES["ban_background"]["type"] =="image/png"||$_FILES["ban_background"]["type"] =="image/x-png"){
				$imgsn = $ints.".png";
			}elseif($_FILES["ban_background"]["type"] =="image/gif"){
				$imgsn = $ints.".gif";
			}elseif($_FILES["ban_background"]["type"] =="image/pjpeg"||$_FILES["ban_background"]["type"] =="image/jpeg"){
				$imgsn = $ints.".jpg";
			}
			if(!empty($imgsn)) {
				@copy($_FILES["ban_background"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
				$ban_background = "{$imgsn}"  ;
			}
		}
			
  		$ban_status = (int) @$_POST["ban_status"] ; 
		
		
	 
		if(empty($ban_id))
		{
			$sql = "INSERT INTO cmp_banner 
											( 
											admin_id
											, ban_title 
											, ban_picture 
											, ban_url 
   											, createdate
											, updatedate
											, ban_status 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$ban_title}'
											, '{$ban_picture}'
											, '{$ban_url}'
 											, NOW() 
											, NOW() 
											, {$ban_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$banner_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		else
		{	
			$sql_ban_picture = !empty($ban_picture) ? " ,  ban_picture = '{$ban_picture}' " : ""  ;
			$sql_ban_background = !empty($ban_background) ? " ,  ban_background = '{$ban_background}' " : ""  ;
			
 			$sql = "UPDATE cmp_banner SET ban_title = '{$ban_title}' 
												, ban_url = '{$ban_url}'  
												{$sql_ban_picture}
												{$sql_ban_background}
  												, updatedate = NOW() 
												, ban_status = {$ban_status}
												WHERE 
												ban_id = {$ban_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("banner"))  )
	{
		$ban_id = (int) @$_GET["bid"] ;  
 		$sql = "DELETE FROM cmp_banner WHERE ban_id = {$ban_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("image"))  )
	{
		$ban_id = (int) @$_GET["bid"] ;  
 		$sql = "UPDATE cmp_banner SET ban_picture = '' WHERE ban_id = {$ban_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?in=banner&do=edit&bid={$ban_id}\"</script>" ;
		exit();
	}
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("background"))  )
	{
		$ban_id = (int) @$_GET["bid"] ;  
 		$sql = "UPDATE cmp_banner SET ban_background = '' WHERE ban_id = {$ban_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?in=banner&do=edit&bid={$ban_id}\"</script>" ;
		exit();
	}
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		$("#frm_banner").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$ban_id = (int) @$_GET["bid"] ; 
			$sql = "SELECT * FROM cmp_banner WHERE ban_id = {$ban_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "แบนเนอร์" );
		$smarty->assign("page_title", "แบนเนอร์" );
 		$smarty->assign("breadcrumb", array("แบนเนอร์" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$ban_id = (int) @$row["ban_id"]  ; 
		
		$banner_status = (int) @$row["banner_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">แบนเนอร์</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_album" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="ban_id" value="<?php echo $ban_id ;  ?>"/>
  						
						<div class="form-group">
							<label for="ban_title" class="col-sm-3 control-label">ชื่อแบนเนอร์</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="ban_title" name="ban_title" placeholder="" value="<?php echo @$row["ban_title"] ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="ban_url" class="col-sm-3 control-label">แบนเนอร์ลิ้งค์</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="ban_url" name="ban_url" placeholder="" value="<?php echo @$row["ban_url"] ?>"   >
 							</div>
						</div>
 
 
						<div class="form-group">
							<label for="doc_item" class="col-sm-3 control-label">รูปภาพประกอบ</label>
  							<div class="col-sm-9">
								<input type="file" id="ban_picture" name="ban_picture"   >
								<?php if(!empty($row["ban_picture"])){ ?>
									<br />
									<img class="img-responsive" src="/uploads/contents/<?php echo $row["ban_picture"] ?>" />
									<a href="?in=image&do=delete&bid=<?php echo @$row["ban_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
								<?php } ?>
  							</div>
						</div>
						
						 
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="ban_status" name="ban_status" <?php echo $row["ban_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "แบนเนอร์" );
		$smarty->assign("page_title", "แบนเนอร์" );
		$smarty->assign("breadcrumb", array("แบนเนอร์" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(ban_id) as _c FROM cmp_banner" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_banner ORDER BY createdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">แบนเนอร์</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  ><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
 						<th class="text-center" >หัวข้อ</th>
 						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i ++  ?></td>
                         <td><?php echo @$row["ban_title"] ?></td>
 						<td style="text-align:center" >
							<a href="?in=banner&do=edit&bid=<?php echo @$row["ban_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=banner&do=delete&bid=<?php echo @$row["ban_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 