<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 	$cmpper->check("news") ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	if($_POST)
	{

 		$news_id = (int) @$_POST["news_id"] ;
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
  		$news_type_id = (int) @$_POST["news_type_id"] ;
		$news_title = @$mysqli->real_escape_string(@$_POST["news_title"]) ;
		$news_detail = @$mysqli->real_escape_string(@htmlspecialchars(@$_POST["news_detail"], ENT_QUOTES)) ;

		$news_picture = "";
		if(!empty($_FILES["news_picture"]["tmp_name"] )) {
			$ints = date("YmdGis") . @random_char(7) ;

			if($_FILES["news_picture"]["type"] =="image/png"||$_FILES["news_picture"]["type"] =="image/x-png"){
				$imgsn = $ints.".png";
			}elseif($_FILES["news_picture"]["type"] =="image/gif"){
				$imgsn = $ints.".gif";
			}elseif($_FILES["news_picture"]["type"] =="image/pjpeg"||$_FILES["news_picture"]["type"] =="image/jpeg"){
				$imgsn = $ints.".jpg";
			}
			if(!empty($imgsn)) {
				@copy($_FILES["news_picture"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
				// CHECK SIZE IMAGE
				$size = getimagesize (PATH . "/uploads/contents/{$imgsn}" ) ;
 				if($size[0] > 1200 ){
					$url = "http://{$_SERVER["SERVER_NAME"]}/timthumb.php?src=/uploads/contents/{$imgsn}&w=1200&zc=0";
					$img = PATH . "/uploads/contents/{$imgsn}" ;
					file_put_contents($img, file_get_contents($url));
 				}
				// END CHECK IMAGE
				$news_picture = "{$imgsn}"  ;
 			}
		}

		$publicdate_date = @$mysqli->real_escape_string(@$_POST["publicdate"]) ;

		$publicdate_time_hour = (int) @$_POST["publicdate_time_hour"] ;
		$publicdate_time_min = (int) @$_POST["publicdate_time_min"] ;

		$publicdate_time_hour = sprintf("%02d",  $publicdate_time_hour  ) ;
		$publicdate_time_min = sprintf("%02d",  $publicdate_time_min  ) ;
		$publicdate  = $publicdate_date . " " . $publicdate_time_hour . ":" . $publicdate_time_min . ":00" ;

  		$news_status = (int) @$_POST["news_status"] ;



		if(empty($news_id))
		{
			$sql = "INSERT INTO cmp_news
											(
											admin_id
											, news_type_id
											, news_title
											, news_detail
											, news_picture
											, publicdate
 											, createdate
											, updatedate
											, news_status
											
											)
											VALUES
											(
											{$admin_id}
											, '{$news_type_id}'
											, '{$news_title}'
											, '{$news_detail}'
											, '{$news_picture}'
											, '{$publicdate}'
 											, NOW()
											, NOW()
											, '{$news_status}'
										   ) " ;
			$mysqli->query($sql);
			$news_id = (int)$mysqli->insert_id ;
			echo $mysqli->error ;
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;
			$cmplog->add_log("news-add" , "เพิ่มช่าว "  , $news_id    ) ;
		}
		else
		{
			$news_log = date("Y-m-d H:i:s") . "|" . $admin_id .  "\n"  ;

			$sql_news_picture = !empty($news_picture) ? ", news_picture = '{$news_picture}' " : ""  ;
 			$sql = "UPDATE cmp_news SET news_title = '{$news_title}'
												, news_detail = '{$news_detail}'
												, news_log = CONCAT( news_log , '{$news_log}' )
												{$sql_news_picture}
												, publicdate = '{$publicdate}'
												, updatedate = NOW()
												, news_status = {$news_status}
												, news_type_id = {$news_type_id}
												WHERE
												news_id = {$news_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;
			$cmplog->add_log("news-edit" , "แก้ไขข่าว "  , $news_id    ) ;
		}


		// Document
		$news_document_item = @$_FILES["news_document_item"]  ;

		$sql = "SELECT COUNT(nitem_id) AS c FROM cmp_news_item WHERE news_id = '{$news_id}'  AND nitem_code = 'document' AND news_id <> 0" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$row_count = $rs->fetch_assoc();
		$nitem_seq = $row_count["c"];
		
		for($i = 0 ; $i < count($news_document_item["tmp_name"])  ;$i ++ )
		{
			if(!empty($news_document_item["tmp_name"][$i]))
			{
				$ints = date("YmdGis") . @random_char(7) ;
				$info = $news_document_item["name"][$i] ;
				$ext = pathinfo($info, PATHINFO_EXTENSION);
				if(@in_array($ext, array("doc" , "docx" , "xls" , "xlsx" ,  "ppt", "pptx" , "pdf" , "zip" , "rar" )))
				{
					$imgsn = $ints . "." . $ext ;

					@copy( $news_document_item["tmp_name"][$i] , PATH . "/uploads/news/document/{$imgsn}" );

					$nitem_title = $mysqli->real_escape_string($news_document_item["name"][$i]);
					$nitem_name = $mysqli->real_escape_string($news_document_item["name"][$i]);
					$nitem_path = "{$imgsn}"  ;
					
					$nitem_seq++;

					$sql = "INSERT INTO cmp_news_item (admin_id
															, news_id
															, nitem_seq
															, nitem_code
 															, nitem_title
															, nitem_name
															, nitem_path
															, createdate
 															, updatedate
															, nitem_status )
															VALUES
															(
															'{$admin_id}'
															, '{$news_id}'
															, '{$nitem_seq}'
															, 'document'
															, '{$nitem_title}'
															, '{$nitem_name}'
															, '{$nitem_path}'
															, NOW()
 															, NOW()
															, 1 ) " ;
					$mysqli->query($sql);
					echo $mysqli->error ;
				}
			}
		}
		
		$news_document_item_seq = $_POST["news_document_item_seq"];
		if(!empty($news_document_item_seq)) {
			foreach($news_document_item_seq as $key => $value) {
				$sql = "UPDATE cmp_news_item SET nitem_seq = '{$value}'
							WHERE nitem_id = '{$key}'" ;
				$mysqli->query($sql);
				echo $mysqli->error ;
			}
		}


		// PICTURE
		$news_picture_item = @$_FILES["news_picture_item"]  ;
		for($i = 0 ; $i < count($news_picture_item["tmp_name"])  ;$i ++ )
		{

			if(!empty($news_picture_item["tmp_name"][$i]))
			{
 				$ints = date("YmdGis") . @random_char(7) ;

				if(@$news_picture_item["type"][$i] == "image/png"||@$news_picture_item["type"][$i] =="image/x-png"){
					$imgsn = $ints.".png";
				}elseif(@$news_picture_item["type"][$i] =="image/gif"){
					$imgsn = $ints.".gif";
				}elseif(@$news_picture_item["type"][$i] =="image/pjpeg"||@$news_picture_item["type"][$i] =="image/jpeg"){
					$imgsn = $ints.".jpg";
				}
 				if(!empty($imgsn)) {

					@copy( $news_picture_item["tmp_name"][$i] , PATH . "/uploads/contents/{$imgsn}" );

					// CHECK SIZE IMAGE
					$size = getimagesize ( PATH . "/uploads/contents/{$imgsn}" ) ;
					if($size[0] > 1200 ){
						$url = "http://{$_SERVER["SERVER_NAME"]}/timthumb.php?src=/uploads/contents/{$imgsn}&w=1200&zc=0";
						$img = PATH . "/uploads/contents/{$imgsn}" ;
						@file_put_contents($img, file_get_contents($url));
					}
					// END CHECK IMAGE

					$nitem_title = $mysqli->real_escape_string($news_picture_item["name"][$i]);
					$nitem_name = $mysqli->real_escape_string($news_picture_item["name"][$i]);
					$nitem_path = "{$imgsn}"  ;

					$sql = "INSERT INTO cmp_news_item (admin_id
															, nitem_code
															, news_id
 															, nitem_title
															, nitem_name
															, nitem_path
															, createdate
 															, updatedate
															,  nitem_status )
															VALUES
															(
															'{$admin_id}'
															, 'images'
															, '{$news_id}'
															, '{$nitem_title}'
															, '{$nitem_name}'
															, '{$nitem_path}'
															, NOW()
 															, NOW()
															, 1 ) " ;
					$mysqli->query($sql);
					echo $mysqli->error ;
				}
			}
		}

		$type_code =  @$mysqli->real_escape_string(@$_POST["type_code"]) ;  ;
		$type_code = urlencode($type_code) ;

		echo "<script> window.location.href = \"?type={$type_code}\"</script>" ;
		exit();
	}

	if( @in_array($_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("news"))  ){
		$news_id = (int) @$_GET["nid"] ;

		$type_code =  @$mysqli->real_escape_string(@$_GET["type"]) ;
		$type_code = urlencode($type_code) ;
		// DELETE IMAGE
		$sql = "SELECT news_picture FROM cmp_news WHERE news_id = '{$news_id}'  LIMIT 0 , 1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$row = $rs->fetch_assoc();

		if(!empty($row)){
			@unlink(PATH . "/uploads/contents/{$row["news_picture"]}" ) ;
		}

		$sql = "SELECT nitem_code , nitem_path FROM cmp_news_item WHERE news_id = '{$news_id}' " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		while( $item = $rs->fetch_assoc()){
			if($item["nitem_code"] == "images"){
				@unlink(PATH . "/uploads/contents/{$item["nitem_path"]}" ) ;
			}elseif($item["nitem_code"] == "document"){
				@unlink(PATH . "/uploads/news/document/{$item["nitem_path"]}" ) ;
			}
		}

		//END


 		$sql = "DELETE FROM cmp_news WHERE news_id = {$news_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ;
		$cmplog->add_log("news-delete" , "ลบข่าว " , $news_id  ) ;
		echo "<script> window.location.href = \"?type={$type_code}\"</script>" ;
		exit();
	}

	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("image"))  ){
		$news_id = (int) @$_GET["nid"] ;

		$type_code =  @$mysqli->real_escape_string(@$_GET["type"]) ;
		$type_code = urlencode($type_code) ;

		// DELETE IMAGE
		$sql = "SELECT news_picture FROM cmp_news WHERE news_id = '{$news_id}'  LIMIT 0 , 1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$row = $rs->fetch_assoc();

		if(!empty($row)){
			@unlink(PATH . "/uploads/contents/{$row["news_picture"]}" ) ;
		}

 		$sql = "UPDATE cmp_news SET news_picture = '' WHERE news_id = {$news_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;

		$msg->add("s" , "! ลบรูปภาพแล้ว" ) ;
		echo "<script> window.location.href = \"?in=news&do=edit&type={$type_code}&nid={$news_id}\"</script>" ;
		exit();
	}

	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("item"))  ){
		$news_id = (int) @$_GET["nid"] ;
		$nitem_id = (int) @$_GET["iid"] ;
		$type_code =  @$mysqli->real_escape_string(@$_GET["type"]) ;
		$type_code = urlencode($type_code) ;

		$sql = "SELECT nitem_code , nitem_path FROM cmp_news_item WHERE nitem_id = '{$nitem_id}'  LIMIT 0 , 1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;

		while( $item = $rs->fetch_assoc()){
			if($item["nitem_code"] == "images"){
				@unlink(PATH . "/uploads/contents/{$item["nitem_path"]}" ) ;
			}elseif($item["nitem_code"] == "document"){
				@unlink(PATH . "/uploads/news/document/{$item["nitem_path"]}" ) ;
			}
		}

		$sql = "DELETE FROM cmp_news_item WHERE nitem_id = {$nitem_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;

		$msg->add("s" , "! ลบรูปภาพแล้ว" ) ;
		echo "<script> window.location.href = \"?in=news&do=edit&type={$type_code}&nid={$news_id}\"</script>" ;
		exit();
	}



	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>

	<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
	<script>
		$(document).ready(function() {
			$(".fancybox").fancybox();
			$('.date-picker').datepicker({
				orientation: "top auto" ,
				autoclose: true ,
				format : "yyyy-mm-dd"
			});
			$("#frm_news").validate();

			if($("#news_detail").length) {
				$("#news_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
			}
			
			$("#tb_news_doc > tbody").sortable({
				stop: function(e, ui) {
					var seq = 0;
					$("#tb_news_doc > tbody > .tb_row").each(function(index) {
						seq++;
						$(this).find(".seq").html(seq);
						$(this).find(".item_seq").val(seq);
					});
				}
			});
		});
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$type_code =  @$mysqli->real_escape_string(@$_GET["type"]) ;


	$sql = "SELECT * FROM cmp_news_type WHERE type_code = '{$type_code}' LIMIT 0 , 1  " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;
	$type = $rs->fetch_assoc() ;
 	$type_code = urlencode($type_code) ;

 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{

		if(@in_array($_GET["do"] , array("edit"))){
			$news_id = (int) $_GET["nid"] ;
			$sql = "SELECT * FROM cmp_news WHERE news_id = {$news_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;

		}

		$smarty->assign("title", @$type["type_title"] );
		$smarty->assign("page_title", @$type["type_title"] );
 		$smarty->assign("breadcrumb", array("ข่าวสารสหกรณ์" => "#" , "{$type["type_title"]}" => "?type={$type_code}" ,  "{$btitle}" => "#"   ) );

		$type_code = @$type["type_code"] ;

		$news_id = (int) @$row["news_id"] ;
		$news_type_id = (int) @$type["type_id"] ;

		$news_title =  htmlspecialchars(@$row["news_title"]);
		$news_detail = @$row["news_detail"];
		$news_picture = @$row["news_picture"] ;

		$publicdate = @$row["publicdate"] ;

  		$publicdate = empty($publicdate) ? date("Y-m-d H:i:s") : $publicdate ;
		$publicdatee_time = strtotime($publicdate);

		$publicdate_time_date =  date("Y-m-d" , $publicdatee_time ) ;
		$publicdate_time_hour = (int) date("H" , $publicdatee_time ) ;
		$publicdate_time_min = (int) date("i" , $publicdatee_time ) ;

 		$news_status = (int) @$row["news_status"]  ;


		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title"><?php echo @$type["type_title"] ?></h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" id="frm_news" action="?" method="post"  enctype="multipart/form-data" >

						<input type="hidden" name="news_id" value="<?php echo $news_id ;  ?>"/>
						<!-- 
						<input type="hidden" name="news_type_id" value="<?php echo $news_type_id ;  ?>"/>
						-->
						<input type="hidden" name="type_code" value="<?php echo $type_code ;  ?>"/>
						<div class="form-group">
							<label for="news_type_id" class="col-sm-3 control-label">หมวดหมู่</label>
							<div class="col-sm-9">
								<?php 
									$sql = "SELECT * FROM cmp_news_type WHERE type_id = 1 OR type_id = 6 OR type_id = 7 OR type_id = 8 " ;
									$rs = $mysqli->query($sql);
									echo $mysqli->error ; 
								?>
								<select class="form-control" id="news_type_id" name="news_type_id" >
 									<?php while($item = $rs->fetch_assoc()){ ?>
									<option value="<?php echo $item["type_id"] ?>" <?php echo @$item["type_id"] == $news_type_id ? "selected" :"" ;  ?> ><?php echo $item["type_title"] ?></option>
									<?php } ?>
								</select>
 							</div>
						</div>
 						<div class="form-group">
							<label for="news_title" class="col-sm-3 control-label">หัวข้อข่าว</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="news_title" name="news_title" placeholder="" value="<?php echo @$news_title ?>" required >
 							</div>
						</div>
						<div class="form-group">
							<label for="news_detail" class="col-sm-3 control-label">เนื้อหา</label>
							<div class="col-sm-9">
								<textarea id="news_detail" name="news_detail" ><?php echo @$news_detail ?></textarea>
 							</div>
						</div>

						<div class="form-group">
							<label for="news_title" class="col-sm-3 control-label">เอกสารแนบ </label>
							<div class="col-sm-9">
								<input type="file"  id="news_document_item[]" name="news_document_item[]" multiple   >
								<style>
									.news_list_file table.table tbody td , .news_list_file table.table thead th   {
										padding:5px!important
									}
								</style>
								<div class="news_list_file" style="padding-top:10px" >
									<?php
										$sql = "SELECT * FROM cmp_news_item WHERE news_id = '{$news_id}'  AND nitem_code = 'document' AND news_id <> 0 ORDER BY nitem_seq, nitem_id" ;
										$rs = $mysqli->query($sql);
										$i = 1 ;
										if(@$rs->num_rows > 0 ){
									?>
									<p>*คลิกลากเพื่อย้ายตำแหน่ง</p>
									<table id="tb_news_doc" class="table" style="">
									<thead>
										<tr>
											<th class="text-center" style="" >ลำดับ</th>
											<th class="text-center" style="">ไฟล์</th>
											<th class="text-center" style="">ลิ้งค์</th>
											<th class="text-center"  style="">ลบ</th>
										</tr>
									</thead>
									<tbody>
									<?php
										while($item = $rs->fetch_assoc()){
									?>
										<tr class="tb_row">
											<td class="tb_col1" style="text-align:center;">
												<span class="seq"><?php echo $i; ?></span>
												<input type="hidden" name="news_document_item_seq[<?php echo @$item["nitem_id"] ?>]" value="<?php echo $i++; ?>" class="item_seq">
											</td>
											<td class="tb_col2" style=""><?php echo $item["nitem_title"] ?></td>
											<td class="tb_col3" style="">
												<a target="_blank" href="/uploads/news/document/<?php echo $item["nitem_path"] ?>" >
												http://<?php echo $_SERVER["SERVER_NAME"] ?>/uploads/news/document/<?php echo $item["nitem_path"] ?></a>
											</td>
											<td class="tb_col4" style="text-align:center;" >
											<a href="?in=item&do=delete&type=<?php echo $type_code ?>&nid=<?php echo $news_id  ?>&iid=<?php echo @$item["nitem_id"] ?>"  onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  title="ลบข้อมูล" >
												<i class="fa fa-trash-o"></i>
											</a>
											</td>
										</tr>
									<?php
										}
									?>
									</tbody>
									</table>
									<?php } ?>
								</div>
 							</div>
						</div>

						<div class="form-group">
							<label for="news_title" class="col-sm-3 control-label">ภาพประกอบข่าว </label>
							<div class="col-sm-9">
								<input type="file"  id="news_picture" name="news_picture" placeholder=""  >
								<?php
									if(!empty($news_picture)){
								?>
									<div class="row" style="padding-top:10px">
										<div class="col-md-4">
											<a class="fancybox" href="/uploads/contents/<?php echo $row["news_picture"] ?>" >
												<img class="img-responsive img-thumbnail" src="/timthumb.php?src=/uploads/contents/<?php echo $row["news_picture"] ?>&w=400&h=300" />
											</a>
											<a href="?in=image&do=delete&type=<?php echo $type_code ?>&nid=<?php echo $news_id  ?>"  onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  title="ลบข้อมูล" ><i class="fa fa-trash-o"></i></a>
										</div>
									</div>
								<?php
									}
								?>
 							</div>
						</div>

						<div class="form-group">
							<label for="news_title" class="col-sm-3 control-label">ภาพประกอบอื่นๆ </label>
							<div class="col-sm-9">
								<input type="file"  id="news_picture_item[]" name="news_picture_item[]" multiple  >
								<div style="padding-top:10px" >
									<div class="row" style="">
									<?php
										$sql = "SELECT * FROM cmp_news_item WHERE news_id = '{$news_id}' AND nitem_code = 'images' ORDER BY nitem_seq ASC " ;
										$rs = $mysqli->query($sql);
										echo $mysqli->error ;
										while($item = $rs->fetch_assoc()){
									?>
											<div class="col-xs-2 text-center">
												<a class="fancybox" href="/uploads/contents/<?php echo $item["nitem_path"] ?>" target="_blank" >
													<img class="img-responsive img-thumbnail" src="/timthumb.php?src=/uploads/contents/<?php echo $item["nitem_path"] ?>&w=300&h=300" style="margin:auto" />
												</a>

												<a href="?in=item&do=delete&type=<?php echo $type_code ?>&nid=<?php echo $news_id  ?>&iid=<?php echo @$item["nitem_id"] ?>"  onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  title="ลบข้อมูล" >
													<i class="fa fa-trash-o"></i>
												</a>
											</div>

									<?php
										}
									?>
									</div>
								</div>
 							</div>
						</div>

						<div class="form-group">
							<label for="news_detail" class="col-sm-3 control-label">วันที่แผยแผ่</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control date-picker" id="publicdate" name="publicdate" placeholder="" value="<?php echo $publicdate_time_date  ?>" style="width:150px;float:left;margin-right:5px;" >
 								<select class="form-control" id="publicdate_time_hour" name="publicdate_time_hour" style="width:100px;float:left" >
								   <option value="" disabled="" selected="">กรุณาเลือกชัวโมง</option>
									<?php for($i = 0 ; $i < 24 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_hour == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
								<div style="float:left; padding-top: 7px;" > : </div>
								<select class="form-control" id="publicdate_time_min" name="publicdate_time_min" style="width:100px;float:left"  >
								   <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 60 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_min == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								   </select>
								</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="news_status" name="news_status" <?php echo $news_status == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1"> แสดงข้อมูล
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
  								<button type="submit" name="test2" class="btn btn-success" style="margin-left:5px;" value="save" >บันทึกข้อมูล</button>
								<a href="?type=<?php echo $type_code ?>" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>


<?php
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
 		$page = (int) @$_GET["page"]  ;
		$smarty->assign("title", "{$type["type_title"]}" );
		$smarty->assign("page_title", "{$type["type_title"]}" );
		$smarty->assign("breadcrumb", array( "ข่าวสารสหกรณ์" => "?" , "{$type["type_title"]}" => "#") );

		ob_start();
        $where_key_word = '';
        if($_GET['key_word']){
            $where_key_word = " AND news_title LIKE '%{$_GET['key_word']}%'";
        }

		$sql = "SELECT COUNT(news_id) as _c FROM cmp_news WHERE news_type_id = '{$type["type_id"]}' {$where_key_word}" ;
		$rs = $mysqli->query($sql);
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ;
		$per_page = 10 ;
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;

		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();

		$page_start = (($per_page * $page) - $per_page ) ;

		$sql = "SELECT * FROM cmp_news WHERE news_type_id = '{$type["type_id"]}' {$where_key_word} ORDER BY publicdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error;



		$i =  $num_rows  - ($per_page * ($page - 1 ));

?>
	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title"><?php echo $type["type_title"] ?></h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
            <form id="frm_search" class="form-horizontal" action="?" method="get"  enctype="multipart/form-data">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4 pull-right">
                            <table>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <input type="hidden"  class="form-control" id="type" name="type" value="<?php echo $_GET['type'];?>">
                                        <input type="text"  class="form-control" id="key_word" name="key_word" placeholder="" value="">
                                    </td>
                                    <td style="padding-left: 2px;">
                                        <button type="submit" class="btn btn-success">&nbsp;&nbsp;&nbsp;<i class="fa fa-search"></i> ค้นหา&nbsp;&nbsp;&nbsp;</button>
                                    </td>
                                    <td>
                                        <a href="?do=add&type=<?php echo $type["type_code"] ?>" class="btn btn-info" style="margin-left:5px;"><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
			 <!--<a class="btn btn-info pull-right" href="?do=add&type=<?php echo $type["type_code"] ?>"  >เพิ่มข้อมูล</a>-->
			 <div style="clear: both;margin-bottom:15px"></div>

			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
						<th class="text-center" width="200" >วันที่เวลา</th>
						<th class="text-center" >หัวข้อ</th>
						<th class="text-center" width="100"  >สถานะ</th>
						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i --  ?></td>
						<td style="text-align:center" ><?php echo mysqldate2thaidate(@$row["publicdate"] , "short" , true ) ?></td>
                        <td><a target="_blank" href="/news.php?nid=<?php echo $row["news_id"]  ?>" ><?php echo @$row["news_title"] ?></a></td>
						<td style="text-align:center" ><?php echo $row["news_status"] == 1 ? "<span class=\"btn btn-info btn-xs\" >แสดง</span>" : "<span class=\"btn btn-danger btn-xs\" >ไม่แสดง</span>" ; ?></td>
						<td style="text-align:center" >
							<a href="?in=news&do=edit&type=<?php echo $type_code ?>&nid=<?php echo @$row["news_id"] ?>" title="แก้ไขข้อมูล" ><i class="fa fa-edit"></i></a> |
							<a href="?in=news&do=delete&type=<?php echo $type_code ?>&nid=<?php echo @$row["news_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  title="ลบข้อมูล" ><i class="fa fa-trash-o"></i></a>
						</td>
                      </tr>
					<?php
						}
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 <?php
		$content = ob_get_contents();
		ob_end_clean();

	}



	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
