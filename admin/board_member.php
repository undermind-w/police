<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	
	if(@$_POST["ajax"]) {
		switch($_POST["do"]) {
			case "set_block":
				$sql = "UPDATE cmp_webboard_member SET wmem_status = 0 WHERE wmem_id = {$_POST["id"]}";
				$mysqli->query($sql);
				echo $mysqli->error;
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
			case "set_active":
				$sql = "UPDATE cmp_webboard_member SET wmem_status = 1, wmem_confirm = 1 WHERE wmem_id = {$_POST["id"]}";
				$mysqli->query($sql);
				echo $mysqli->error;
				
				echo json_encode(array(
					"status" => "TRUE"
				));
				break;
		}
		exit;
	}
	
	if($_POST)
	{
 		$wmem_id = (int) @$_POST["wmem_id"] ; 
		
  		$wmem_password = @$mysqli->real_escape_string(@$_POST["wmem_password"]) ;
		$wmem_fullname = @$mysqli->real_escape_string(@$_POST["wmem_fullname"]) ;
		$wmem_name = @$mysqli->real_escape_string(@$_POST["wmem_name"]) ;
		$wmem_email = @$mysqli->real_escape_string(@$_POST["wmem_email"]) ;
		$wmem_tel = @$mysqli->real_escape_string(@$_POST["wmem_tel"]) ;
		$wmem_detail = @$mysqli->real_escape_string(@htmlspecialchars(@$_POST["wmem_detail"], ENT_QUOTES)) ;
		
		$wmem_confirm = (int) @$_POST["wmem_confirm"] ; 
  		$wmem_status = (int) @$_POST["wmem_status"] ; 
		
		
	 
		if(!empty($wmem_id))
		{	
  			$sql = "UPDATE cmp_webboard_member SET wmem_type_id = '{$_POST["wmem_type_id"]}'  
												, wmem_password = '{$wmem_password}'
												, wmem_fullname = '{$wmem_fullname}'
												, wmem_name = '{$wmem_name}'
												, wmem_email = '{$wmem_email}'
												, wmem_tel = '{$wmem_tel}'
												, wmem_detail = '{$wmem_detail}'
												, wmem_confirm = '{$wmem_confirm}'
   												, updatedate = NOW() 
												, wmem_status = {$wmem_status}
												WHERE 
												wmem_id = {$wmem_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("page"))  )
	{
		$wmem_id = (int) @$_GET["pid"] ;  
 		$sql = "DELETE FROM cmp_webboard_member WHERE wmem_id = {$wmem_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_forum").validate();
		
		$(document).on("click", ".status_active", function() {
			var status_active = $(this);
			var id = $(this).attr("data-id");
			
			$.ajax({
				type: "POST",
				url: "?",
				data: "ajax=1&do=set_block&id=" + id,
				success: function(msg) {
					data = jQuery.parseJSON(msg);
					if(data["status"] == "TRUE") {
						status_active.removeClass("status_active");
						status_active.removeClass("btn-info");
						status_active.addClass("status_block");
						status_active.addClass("btn-danger");
						status_active.html("ระงับการใช้งาน");
					}
				}
			});
			return false;
		});
		
		$(document).on("click", ".status_block", function() {
			var status_block = $(this);
			var id = $(this).attr("data-id");
			
			$.ajax({
				type: "POST",
				url: "?",
				data: "ajax=1&do=set_active&id=" + id,
				success: function(msg) {
					data = jQuery.parseJSON(msg);
					if(data["status"] == "TRUE") {
						status_block.removeClass("status_block");
						status_block.removeClass("btn-danger");
						status_block.removeClass("btn-warning");
						status_block.addClass("status_active");
						status_block.addClass("btn-info");
						status_block.html("เปิดการใช้งาน");
					}
				}
			});
			return false;
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$wmem_id = (int) @$_GET["pid"] ; 
			$sql = "SELECT * FROM cmp_webboard_member WHERE wmem_id = {$wmem_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "กระดานถามตอบ - สมาชิก" );
		$smarty->assign("forum_title", "กระดานถามตอบ - สมาชิก" );
 		$smarty->assign("breadcrumb", array("กระดานถามตอบ - สมาชิก" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$wmem_id = (int) @$row["wmem_id"]  ; 
		
		$wmem_confirm = (int) @$row["wmem_confirm"]  ;
		$wmem_status = (int) @$row["wmem_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">สมาชิก</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_forum" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="wmem_id" value="<?php echo $wmem_id ;  ?>"/>
						
						<div class="form-group">
							<label for="wmem_type_id" class="col-sm-3 control-label">ประเภทสมาชิก</label>
							<div class="col-sm-9">
								<select class="form-control" id="wmem_type_id" name="wmem_type_id" required="required" style="width: auto;" >
									<option value="" disabled="" selected="">กรุณาเลือกประเภทสมาชิก</option>
									<?php
									$sql = "SELECT * FROM cmp_webboard_member_type ORDER BY type_id";
									$rsType = $mysqli->query($sql);
									echo $mysqli->error;
									while($rowType = $rsType->fetch_assoc()) : ?>
										<option value="<?php echo $rowType["type_id"] ?>" <?php echo $rowType["type_id"] == $row["wmem_type_id"] ? "selected" :"" ;  ?> ><?php echo $rowType["type_title"] ?></option>
									<?php endwhile; ?>
								</select>
 							</div>
						</div>
						
						<div class="form-group">
							<label for="wmem_username" class="col-sm-3 control-label">ผู้ใช้</label>
							<div class="col-sm-9">
								<?php echo @$row["wmem_username"] ?>
 							</div>
						</div>
						
						<div class="form-group">
							<label for="wmem_password" class="col-sm-3 control-label">รหัสผ่าน</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="wmem_password" name="wmem_password" placeholder="" value="<?php echo @$row["wmem_password"] ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="wmem_fullname" class="col-sm-3 control-label">ชื่อ - สกุล</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="wmem_fullname" name="wmem_fullname" placeholder="" value="<?php echo @$row["wmem_fullname"] ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="wmem_name" class="col-sm-3 control-label">ชื่อที่ใช้แสดงผล</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="wmem_name" name="wmem_name" placeholder="" value="<?php echo @$row["wmem_name"] ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="wmem_email" class="col-sm-3 control-label">อีเมล์</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="wmem_email" name="wmem_email" placeholder="" value="<?php echo @$row["wmem_email"] ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="wmem_tel" class="col-sm-3 control-label">โทรศัพท์</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="wmem_tel" name="wmem_tel" placeholder="" value="<?php echo @$row["wmem_tel"] ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="wmem_detail" class="col-sm-3 control-label">รายละเอียด</label>
							<div class="col-sm-9">
								<textarea  class="form-control" id="wmem_detail" name="wmem_detail" ><?php echo @$row["wmem_detail"] ?></textarea>
  							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="wmem_confirm" name="wmem_confirm" <?php echo $row["wmem_confirm"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  ยืนยันอีเมล์
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="wmem_status" name="wmem_status" <?php echo $row["wmem_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "กระดานถามตอบ - สมาชิก" );
		$smarty->assign("forum_title", "กระดานถามตอบ - สมาชิก" );
		$smarty->assign("breadcrumb", array("กระดานถามตอบ - สมาชิก" => "#") );
		
		ob_start();
		
		$where = "";
		switch(@$_GET["search_type"]) {
			case 1:
				$where = "AND cmp_webboard_member.wmem_username LIKE '%".@$_GET["keyword"]."%'";
				break;
			case 2:
				$where = "AND cmp_webboard_member.wmem_fullname LIKE '%".@$_GET["keyword"]."%'";
				break;
			case 3:
				$where = "AND cmp_webboard_member.wmem_name LIKE '%".@$_GET["keyword"]."%'";
				break;
		}
		
		$sql = "SELECT COUNT(wmem_id) as _c FROM cmp_webboard_member WHERE 1 ".$where ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT cmp_webboard_member.*, cmp_webboard_member_type.type_title
						FROM  cmp_webboard_member
							INNER JOIN cmp_webboard_member_type ON cmp_webboard_member.wmem_type_id = cmp_webboard_member_type.type_id
						WHERE 1 ".$where."
						ORDER BY createdate DESC
						LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows - ($per_page * ($page - 1 ) );
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">สมาชิก</h4>
		</div>
		<div class="panel-body">
			<?php echo $msg->display() ;  ?>
			
			<form id="frm_search" class="form-inline" action="" method="get">
				<div class="form-group">
					<select class="form-control" id="search_type" name="search_type" style="width: auto;" >
						<option value="1"<?php if(@$_GET["search_type"] == 1) : ?> selected<?php endif; ?>>ผู้ใช้</option>
						<option value="2"<?php if(@$_GET["search_type"] == 2) : ?> selected<?php endif; ?>>ชื่อ - สกุล</option>
						<option value="3"<?php if(@$_GET["search_type"] == 3) : ?> selected<?php endif; ?>>ชื่อที่ใช้แสดงผล</option>
					</select>
				</div>
				
				<div class="form-group">
					<input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo @$_GET["keyword"]; ?>" placeholder="พิมพ์ข้อความที่ต้องการค้นหา">
				</div>
				
				<button type="submit" class="btn btn-success">ค้นหา</button>
			</form>
			
			<div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
 						<th class="text-center" >ผู้ใช้</th>
						<th class="text-center" >ชื่อ - สกุล</th>
						<th class="text-center" >ชื่อที่ใช้แสดงผล</th>
						<th class="text-center" >ประเภทสมาชิก</th>
						<th class="text-center" >สถานะ</th>
  						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i--; ?></td>
						<td><?php echo @$row["wmem_username"] ?></td>
						<td><?php echo @$row["wmem_fullname"] ?></td>
						<td><?php echo @$row["wmem_name"] ?></td>
						<td><?php echo @$row["type_title"] ?></td>
						<td class="text-center">
							<?php if(@$row["wmem_confirm"] && @$row["wmem_status"]) : ?>
								<span class="status_active btn btn-info btn-xs" data-id="<?php echo @$row["wmem_id"] ?>">เปิดการใช้งาน</span>
							<?php elseif(!@$row["wmem_status"]) : ?>
								<span class="status_block btn btn-danger btn-xs" data-id="<?php echo @$row["wmem_id"] ?>">ระงับการใช้งาน</span>
							<?php else : ?>
								<span class="status_block btn btn-warning btn-xs" data-id="<?php echo @$row["wmem_id"] ?>">ไม่ได้ยืนยันอีเมล์</span>
							<?php endif; ?>
						</td>
  						<td style="text-align:center" >
							<a href="?in=page&do=edit&pid=<?php echo @$row["wmem_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=page&do=delete&pid=<?php echo @$row["wmem_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 