<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");
	
	$do = isset($_POST["do"]) ? $_POST["do"] : null ;
	
	if($do == "update") {
			$list_id			= $mysqli->real_escape_string($_POST["id"]);
			$list_title 	= $mysqli->real_escape_string($_POST["list_title"]);
			$list_detail 	= $mysqli->real_escape_string($_POST["list_detail"]);
			$sent_to 			= $mysqli->real_escape_string($_POST["sent_to"]);
			$list_time		= $mysqli->real_escape_string("{$_POST["list_time_d"]} {$_POST["list_time_h"]}:{$_POST["list_time_m"]}:00");
			if(empty($list_id)) {
					$list_id = genid();
					$sql = "INSERT INTO cmp_mailing_list(list_id, list_title, list_detail, list_time, list_status, sent_to, update_time, update_ip)
									VALUES('{$list_id}', '{$list_title}', '{$list_detail}', '{$list_time}', 1, '{$sent_to}', NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
					$mysqli->query($sql);
			} else {
					$sql = "UPDATE cmp_mailing_list SET
											list_title		= '{$list_title}'
										, list_detail		= '{$list_detail}'
										, list_time			= '{$list_time}'
										, sent_to				=	'{$sent_to}'
									WHERE list_id = '{$list_id}'
									";
					$mysqli->query($sql);
			}
			echo "<script>document.location.href = 'mailing_list.php';</script>";
			exit();
	}
	
	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	if($do == "del") {
			$sql = "DELETE FROM cmp_mailing_list WHERE list_id = '{$_GET["id"]}'";
			$mysqli->query($sql);
			$mysqli->query("OPTIMIZE TABLE cmp_mailing_list");
			echo "<script>document.location.href = 'mailing_list.php';</script>";
			exit();
	}
	
		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
	<script src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<link rel="stylesheet" href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" />
	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>
	<style>
			.table tbody tr, .table tbody td { background-color: #FFF; }
			.table tbody td:last-child span {
					border-right: 2px solid #888;
					padding: 0px 5px 0px 0px;
			}
			.table tbody td:last-child span:last-child {
					border-right: 0px;
			}
			
.bs-callout {
    padding: 20px;
    margin: 20px 0;
    border: 1px solid #eee;
    border-left-width: 5px;
    border-radius: 3px
}

.bs-callout h4 {
    margin-top: 0;
    margin-bottom: 5px
}
.bs-callout p { text-indent: 30px; }
.bs-callout i { text-indent: 0px; }
.bs-callout p:last-child { margin-bottom: 0 }

.bs-callout code {
    border-radius: 3px
}

.bs-callout+.bs-callout {
    margin-top: -5px
}

.bs-callout-danger {
    border-left-color: #ce4844
}

.bs-callout-danger h4 {
    color: #ce4844
}

.bs-callout-warning {
    border-left-color: #aa6708
}

.bs-callout-warning h4 {
    color: #aa6708
}

.bs-callout-info {
    border-left-color: #1b809e;
		background-color: #FFF;
}

.bs-callout-info h4 {
    color: #1b809e
}
	</style>
	<script>
		$(document).ready(function() {
				if($('.date-picker').length) {
						$('.date-picker').datepicker({
									orientation: "top auto"
								, autoclose: true
								, format : "yyyy-mm-dd"
						});
				}
				if($(".ckeditor").length) {
							$(".ckeditor").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js' });
				}
				
				$("body").on("click", ".btn-submit", function() { 
						$("#frm").submit();
				});
		});
	</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 
	ob_start();
 ?>
	
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 	
	$smarty->assign("title", "ส่งเมล์ถึงสมาชิก" );
	$smarty->assign("breadcrumb", array("ส่งเมล์ถึงสมาชิก" => "mailing_list.php") );
	ob_start();

	$sql = "SELECT COUNT(email) AS _c
					FROM cmp_imp_member_data
					WHERE email <> ''
					";
	$rs = $mysqli->query($sql);
	$row = $rs->fetch_assoc();
	$mail_count = (int)$row["_c"];
	
	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	
	if(!$do) {
?>
	<div class="bs-callout bs-callout-info" id="callout-type-b-i-elems">
    <h4>หมายเหตุ</h4>
    <p>หากสถานะการส่ง เป็น <span class='text-info'>กำลังส่ง</span> หรือ <span class='text-success'>ส่งสำเร็จ</span> จะไม่สามารถ <i class="fa fa-pencil m-r-xxs"></i>แก้ไข หรือ <i class="fa fa-trash m-r-xxs"></i>ลบ ได้</p>
  </div>
	
	<div class="row">
			<div class="col-xs-12">
					<div class="pull-left">จำนวนอีเมล์ที่มี&nbsp;<?php echo number_format($mail_count, 0); ?>&nbsp;อีเมล์</div>
					<div class="pull-right"><a class='btn btn-info' href="?do=add">เพิ่มการส่ง</a></div>
					<div class="clearfix"></div>
			</div>
	</div>
	<table class="table table-bordered m-t-md">
			<thead>
					<tr>
							<th class="text-center" width="60">ลำดับ</th>
							<th>หัวข้อ</th>
							<th class="text-center" width="210">กำหนดส่ง</th>
							<th class="text-center" width="210">ส่งถึง</th>
							<th class="text-center" width="150">สถานะการส่ง</th>
							<th class="text-center" width="150">จำนวนอีเมล์ที่ส่ง</th>
							<th class="text-center" width="150">&nbsp;</th>
					</tr>
			</thead>
			<tbody>
					<?php
							$list_status = array(1 => "<span class='text-warning'>รอการส่ง</span>", "<span class='text-info'>กำลังส่ง</span>", "<span class='text-success'>ส่งสำเร็จ</span>");
							$sql = "SELECT tb1.list_id, tb1.list_title, tb1.list_time, tb1.list_status, tb1.email_count, tb1.sent_to
												, tb2.group_name
											FROM cmp_mailing_list AS tb1
											LEFT OUTER JOIN cmp_mailing_list_group AS tb2 ON tb1.sent_to = tb2.group_id
											ORDER BY tb1.list_status, tb1.list_time";
							$rs = $mysqli->query($sql);
							if($rs->num_rows == 0) {
									echo "<tr><td class='text-center f-bold' colspan='7'>ยังไม่มีรายการค่ะ</td></tr>";
							} else {
									$i = 1;
									while(($row = $rs->fetch_assoc())) {
					?>
					<tr class="text-center">
							<td><?php echo number_format($i++, 0); ?>.</td>
							<td class="text-left"><?php echo $row["list_title"]; ?></td>
							<td><?php echo mysqldate2thaidate($row["list_time"], "short", true); ?></td>
							<td><?php echo empty($row["sent_to"]) ? "สมาชิก CMCOOP Online" : $row["group_name"]; ?></td>
							<td><?php echo $list_status[$row["list_status"]]; ?></td>
							<td><?php echo $row["list_status"] < 2 ? "-" : number_format($row["email_count"], 0) ; ?></td>
							<td>
									<span><a href="?do=edit&id=<?php echo $row["list_id"]; ?>"><i class="fa fa-pencil m-r-xxs"></i>แก้ไข</a></span>
									<?php if($row["list_status"] < 2) { ?>
									<span><a class="text-danger" href="?do=del&id=<?php echo $row["list_id"]; ?>"><i class="fa fa-trash m-r-xxs"></i>ลบ</a></span>
									<?php } ?>
							</td>
					</tr>
					<?php 	
									}
							}
					?>
			</tbody>	
	</table>
	<?php
			} else {
				$row = array();
				$id = isset($_GET["id"]) ? $_GET["id"] : null ;
				if($id) {
						$sql = "SELECT *
										FROM cmp_mailing_list
										WHERE list_id = '{$id}'";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
				}
				
				$list_title 	= isset($row["list_title"]) ? $row["list_title"]  : null;
				$list_detail 	= isset($row["list_detail"]) ? $row["list_detail"]  : null;
				$sent_to 			= isset($row["sent_to"]) ? $row["sent_to"]  : null;
				$list_status 	= isset($row["list_detail"]) ? (int)$row["list_status"]  : 0;
				if(isset($row["list_time"])) {
						$list_time		= explode(" ", $row["list_time"]);
						$list_time_d	= $list_time[0];
						$list_time		= explode(":", $list_time[1]);
						$list_time_h	= (int)$list_time[0];	
						$list_time_m	= (int)$list_time[1];
				} else {
						$list_time_d = date("Y-m-d");
						$list_time_h = (int)date("G");
						$list_time_m = (int)date("i");
				}
	?>
	<div class="row">
			<div class="col-xs-12">
					<div class="panel panel-white">
							<div class="panel-heading clearfix">
								<h4 class="panel-title">ส่งเมล์ถึงสมาชิก</h4>
							</div>
							<div class="panel-body">
									<?php echo $msg->display() ;  ?>
									<form class="form-horizontal" id="frm" method="post">
											<div class="form-group">
													<label for="list_title" class="col-sm-3 control-label">ส่งถึง</label>
													<div class="col-sm-9">
															<select id="sent_to" name="sent_to" class="form-control" >
																	<option selected value=''>สมาชิก CMCOOP Online</option>
																	<?php
																			$sql = "SELECT * FROM cmp_mailing_list_group ORDER BY CONVERT(group_name USING TIS620)";
																			$rs_g = $mysqli->query($sql);
																			while(($row_g = $rs_g->fetch_assoc())) {
																					$selected = $sent_to == $row_g["group_id"] ? " selected" : "" ;
																					echo "<option value='{$row_g["group_id"]}'{$selected}>{$row_g["group_name"]}</option>";
																			}
																	?>
															</select>
													</div>
											</div>
											<div class="form-group">
													<label for="list_title" class="col-sm-3 control-label">หัวข้อ</label>
													<div class="col-sm-9">
															<input class="form-control" type="text" id="list_title" name="list_title" value="<?php echo $list_title; ?>"  />
													</div>
											</div>
											<div class="form-group">
													<label for="list_detail" class="col-sm-3 control-label">เนื้อหา</label>
													<div class="col-sm-9">
															<textarea class="form-control ckeditor" id="list_detail" name="list_detail"><?php echo $list_detail ?></textarea>
													</div>
											</div>
											<div class="form-group">
												<label for="news_detail" class="col-sm-3 control-label">กำหนดส่ง</label>
												<div class="col-sm-9">
													<input type="text"  class="form-control date-picker cursor text-center display-inline" id="list_time_d" name="list_time_d" placeholder="" value="<?php echo $list_time_d  ?>" style="width:150px;float:left;"  >
													<select class="form-control display-inline m-l-xxs" id="list_time_h" name="list_time_h" style="width:75px;">
														 <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
														<?php for($i = 0 ; $i < 24 ; $i ++   ){ ?>
														<option value="<?php echo $i ?>" <?php echo $list_time_h == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
														<?php } ?>
													</select>
													<div class="display-inline p-h-xxs"> : </div>
													<select class="form-control display-inline" id="list_time_m" name="list_time_m" style="width:75px;"  >
														 <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
														<?php for($i = 0 ; $i < 60 ; $i ++   ){ ?>
														<option value="<?php echo $i ?>" <?php echo $list_time_m == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-10">
													<button class="btn btn-success btn-submit" type="button"<?php echo $list_status > 1 ? " disabled" : "" ; ?>>บันทึกข้อมูล</button>
													<a class="btn btn-danger m-l-xxs" href="?">ออก</a>
													<input type="hidden" name="id" value="<?php echo $id ;  ?>"/>
													<input type="hidden" name="do" value="update"/>
												</div>
											</div>
									</form>
							</div>
					</div>
			</div>
	</div>
	<?php } ?>
<?php 
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");