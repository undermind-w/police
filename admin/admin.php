<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	$cmpper->check("setting") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$admin_id = (int) @$_POST["admin_id"] ; 
  		$admin_name = @$mysqli->real_escape_string(@$_POST["admin_name"]) ;
		$admin_username = @$mysqli->real_escape_string(@$_POST["admin_username"]) ;
		$admin_password = @$mysqli->real_escape_string(@$_POST["admin_password"]) ;
 		
  		$admin_status = (int) @$_POST["admin_status"] ; 
		
		$sql = "SELECT COUNT(admin_id) as _c FROM cmp_admin WHERE admin_username = '{$admin_username}' AND admin_id <> '{$admin_id}' LIMIT 0 , 1  " ;
		$rs = $mysqli->query($sql);
		$valid = $rs->fetch_assoc();
		
		if(@$valid["_c"] > 0 ){
			$msg->add("s" , "! มี username นี้ในระบบแล้ว" ) ; 
			echo "<script> window.location.href = \"?\"</script>" ;
			exit();
		}
		 
		if(empty($admin_id))
		{
			$sql = "INSERT INTO cmp_admin 
											( 
											admin_name 
											, admin_username 
											, admin_password
 											, createdate
											, updatedate
											, admin_status 
											) 
											VALUES  
											(
											'{$admin_name}'
											, '{$admin_username}'
											, '{$admin_password}'
 											, NOW() 
											, NOW() 
											, {$admin_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$admin_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
		}
		else
		{	
 			$sql = "UPDATE cmp_admin SET admin_name = '{$admin_name}' 
												, admin_username = '{$admin_username}'  
												, admin_password = '{$admin_password}'  
												, updatedate = NOW() 
												, admin_status = {$admin_status}
												WHERE 
												admin_id = {$admin_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
		}
		
		$sql = "DELETE FROM cmp_admin_permission WHERE admin_id = '{$admin_id}'  " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		$admin_permission_item = @$_POST["admin_permission_item"] ; 
		foreach($admin_permission_item  as $item){
			$per_id = (int) @$item ; 
			if(!empty($item)){
				$sql = "INSERT INTO cmp_admin_permission (admin_id , per_id , updatedate ) VALUES ('{$admin_id}' , '{$per_id}' , NOW())  " ;
				$mysqli->query($sql);
				echo $mysqli->error ;
			}
		}
		
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array($_GET["do"] , array("delete")) ){
		$admin_id = @$_GET["aid"] ;  
		
 		$sql = "DELETE FROM cmp_admin WHERE admin_id = {$admin_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		
		$sql = "DELETE FROM cmp_admin_permission WHERE admin_id = {$admin_id} "; 
		$mysqli->query($sql);
		echo $mysqli->error ;
		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	

		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_admin").validate();
		
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$admin_id = (int) @$_GET["aid"] ; 
			$sql = "SELECT * FROM cmp_admin WHERE admin_id = {$admin_id} AND admin_super = 0 LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "ผู้ใช้งานระบบ" );
		$smarty->assign("page_title", "ผู้ใช้งานระบบ" );
 		$smarty->assign("breadcrumb", array("ผู้ใช้งานระบบ" => "?" ,  "{$btitle}" => "#"  ) );

		$admin_id = (int) @$row["admin_id"] ; 
 		$admin_name =  @$row["admin_name"];  
  		$admin_username = @$row["admin_username"]  ;  
		$admin_password = @$row["admin_password"]  ; 
		
		
		$admin_status = (int) @$row["admin_status"]  ; 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title"><?php echo @$type["type_title"] ?></h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_admin" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="admin_id" value="<?php echo $admin_id ;  ?>"/>
  						
						<div class="form-group">
							<label for="msg_title" class="col-sm-3 control-label">ชื่อ-สกุล</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="admin_name" name="admin_name" placeholder="" value="<?php echo @$admin_name ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="admin_username" class="col-sm-3 control-label">Username</label>
  							<div class="col-sm-9">
								<input type="text"  class="form-control" id="admin_username" name="admin_username" placeholder="" value="<?php echo @$admin_username ?>" required="required" >
  							</div>
						</div>
						
						<div class="form-group">
							<label for="admin_password" class="col-sm-3 control-label">Password</label>
  							<div class="col-sm-9">
								<input type="text"  class="form-control" id="admin_password" name="admin_password" placeholder="" value="<?php echo @$admin_password ?>" required="required" >
  							</div>
						</div>
						
						<div class="form-group">
							<label for="msg_url" class="col-sm-3 control-label">กำหนดสิทธิ์การใช้งาน</label>
  							<div class="col-sm-9">
								<?php 
									$sql = "SELECT per_id FROM cmp_admin_permission WHERE admin_id = '{$admin_id}' ORDER BY updatedate " ;
									$rs = $mysqli->query($sql);
									$admin_permission = array(); 
									while($item = $rs->fetch_assoc())
									{
										$admin_permission[] = $item["per_id"] ; 
									}
									unset($item);
									$sql = "SELECT * FROM cmp_permission WHERE per_seq <> 0 ORDER BY per_seq ASC" ;
									$rs = $mysqli->query($sql);
									echo $mysqli->error ;
									
									while($item = $rs->fetch_assoc() ){
								?>
								<div class="checkbox">
								  <label style="width:350px">
									<input type="checkbox" name="admin_permission_item[]" value="<?php echo $item["per_id"] ?>" <?php echo in_array( $item["per_id"], $admin_permission  ) ? "checked" : "" ; ?> >
									<?php echo $item["per_title"] ?>
								  </label>
								</div>
								<?php } ?>
								 
  							</div>
						</div>
						 
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="admin_status" name="admin_status" <?php echo $admin_status == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		$smarty->assign("title", "ผู้ใช้งานระบบ" );
		$smarty->assign("page_title", "ผู้ใช้งานระบบ" );
		$smarty->assign("breadcrumb", array("ผู้ใช้งานระบบ" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(admin_id) as _c FROM cmp_admin WHERE admin_super = 0 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_admin WHERE admin_super = 0 ORDER BY createdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">ผู้ใช้งานระบบ</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="100" style="text-align:center" >ลำดับ</th>
						<th width="" style="text-align:center" >ชื่อ - สกุล </th>
                        <th width="200" style="text-align:center" > Username  </th>
						<th width="200" style="text-align:center" > Password </th>
                        <th width="150" style="text-align:center" >จัดการ</th>
						
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
						<td style="text-align:center" ><?php echo $i ++  ?></td>
						<td><?php echo @$row["admin_name"] ?></td>
						<td><?php echo @$row["admin_username"] ?></td>
						<td><?php echo @$row["admin_password"] ?></td>
						<td style="text-align:center" >
							<a href="?in=message&do=edit&aid=<?php echo @$row["admin_id"] ?>" >แก้ไข</a> | 
							<a href="?in=message&do=delete&aid=<?php echo @$row["admin_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  >ลบ</a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 