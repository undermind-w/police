<?php
	session_start();
	require "config.inc.php";
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	 
	if($_POST){
		if(@$_POST["do"] == "login"){
			$username = $mysqli->real_escape_string($_POST["username"]);
			$password = $mysqli->real_escape_string($_POST["password"]);
			$sql = "SELECT * FROM bun_admin WHERE admin_status = 1 AND admin_username = '{$username}' AND admin_password = '{$password}' LIMIT 0 , 1 " ;
 			$rs = $mysqli->query($sql);
			echo $mysqli->error;
			$admin = $rs->fetch_assoc();
 			if(!empty($admin)){
				$token = @date("YmdHis") . $admin["admin_id"] .  @random_char(10);
				$admin_id = (int) @$admin["admin_id"] ; 
				$sql = "UPDATE bun_admin SET admin_token = '{$token}' WHERE admin_id = {$admin_id} LIMIT 1  " ;
				 $mysqli->query($sql);
				 echo $mysqli->error;
				 
				echo "<script type='text/javascript'>document.location.href='login.submit.php?token={$token}';</script>";
				exit();
			}
			
			echo "<script type='text/javascript'> alert('รหัสผ่านหรือชื่อผู้ใช้ผิดพลาด !!! ') ; document.location.href='login.php?';</script>";
			exit();
			
			exit();
		}
		
	}
	 
?>
<!DOCTYPE html>
<html>
    <head>
        
        <!-- Title -->
        <title>Modern | Login - Sign in</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href="/html/meter/admin2/assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="/html/meter/admin2/assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="/html/meter/admin2/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/html/meter/admin2/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="/html/meter/admin2/assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
        <link href="/html/meter/admin2/assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
        <link href="/html/meter/admin2/assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
        <link href="/html/meter/admin2/assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>	
        
        <!-- Theme Styles -->
        <link href="/html/meter/admin2/assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
        <link href="/html/meter/admin2/assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        <script src="/html/meter/admin2/assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="page-login">
        <main class="page-content">
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-3 center">
                            <div class="login-box">
                                <a href="/" class="logo-name text-lg text-center"><img src="/images/logoweb2.png" alt="" /></a>
								<h3 class="text-center" style="color: #b61918;">THE CENTER OF THAI INSPIRATION</h3>
                                <p class="text-center m-t-md">Please login into your account.</p>
                                <form method="post" class="m-t-md" action="">
									<input type="hidden" name="do" value="login" />
                                    <div class="form-group">
                                        <input type="text" id="username" name="username" class="form-control" placeholder="Username" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-block" style="background-color: #b61918;">Login</button>
                                    <?php /*<a href="forgot.html" class="display-block text-center m-t-md text-sm">Forgot Password?</a>*/ ?>
                                </form>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
	

        <!-- Javascripts -->
        <script src="/html/meter/admin2/assets/plugins/jquery/jquery-2.1.3.min.js"></script>
        <script src="/html/meter/admin2/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="/html/meter/admin2/assets/plugins/pace-master/pace.min.js"></script>
        <script src="/html/meter/admin2/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="/html/meter/admin2/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/html/meter/admin2/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="/html/meter/admin2/assets/plugins/switchery/switchery.min.js"></script>
        <script src="/html/meter/admin2/assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="/html/meter/admin2/assets/plugins/classie/classie.js"></script>
        <script src="/html/meter/admin2/assets/plugins/waves/waves.min.js"></script>
        <script src="/html/meter/admin2/assets/js/modern.min.js"></script>
        
    </body>
</html>