<?php
	session_start();
	require "config.inc.php" ;
 	require "session.inc.php" ;
	require "menu.inc.php" ;
	require PATH.'/class/spreadsheet-reader/php-excel-reader/excel_reader2.php';
	require PATH.'/class/spreadsheet-reader/SpreadsheetReader.php';
	require PATH."/class/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php";

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year = date("Y")>2500?date("Y")-543:date("Y");

	function U2T($text) { return mb_convert_encoding(trim($text), "TIS-620", "auto"); }
	function T2U($text) { return mb_convert_encoding(trim($text), "UTF-8", "auto"); }
	function ENC_T2U($txt) { return iconv("tis-620", "utf-8", $txt); }

	function excel2mysqlDate($date) {
		if(is_numeric($date)) {
			$tmp_date = gmdate("Y-m-d", ($date - 25569) * 86400);
			return ConvertToSQLDate($tmp_date);
		}
		else {
			$dates = explode(" ", $date);
			return ConvertToSQLDate($dates[0]);
		}
	}
	
	function process_address($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','memb_addr','addr_group','soi','mooban','road','tambol','district_desc',
						'province_desc','postcode','mem_tel','memb_addrc','addr_groupc','soic','moobanc','roadc','tambolc',
						'district_descc','province_descc','postcodec','phone','mem_telmobile');

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_address ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	/*
	function process_ag($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "member_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 4; $i++) {
									if($i == 1)	$tmp[] = "'".ConvertToSQLDate(T2U($row[$i]))."'";
									elseif($i == 2)	$tmp[] = (double)$row[$i];
									else	$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_ag(member_no, mutuallydtm, assit_amt, mutsts) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	function process_dept_master($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','deptaccount_no','deptaccount_name','depttype_desc','prncbal','deptmthamt');

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_dept_master ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	function process_dept_statement($file) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "deptaccount_no"){
					// delete duplicate data
					$fields = array('deptaccount_no','seq_no','oprdtm','deptitemtype_desc','deptamt','withamt','prncbal');
					$where = array();

					/*foreach($fields as $i => $index){
						if($index != "oprdtm"){
							// convert to utf-8
							$data[$i] = iconv("tis-620", "utf-8", $data[$i]);
							// add conditions
							array_push($where, "{$index} = '{$data[$i]}'");
						}else{
							// reformat date
							list($m,$d,$y) = explode('/', $data[2]);
							$data[2] = "$y-$m-$d";
						}
					}
					$where = implode(" AND ", $where);
					$sql = "DELETE FROM cmp_imp_dept_statement WHERE {$where}";
					$mysqli->query($sql);*/

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_dept_statement($fields) VALUES('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	function process_divavg($file, $ext){
		global $mysqli;
		if($ext == "xls") {
			$data = new SpreadsheetReader($file);
			$row_index = 0;
			foreach ($data as $row) {
				if($row_index++ > 0) {
					unset($tmp, $value);
					$tmp = array();
					for($i = 0; $i < 12; $i++) {
						if(in_array($i, array(6, 7, 9, 10)))		$tmp[] = (float)$row[$i];
						elseif(in_array($i, array(4, 11)))		$tmp[] = "'".T2U($row[$i])."'";
					}
					$value = implode(",", $tmp);
					$sql = "INSERT INTO cmp_imp_divavg(member_no, div_amt, avg_amt, ins_amt, bal_amt, account_year) VALUES({$value})";
					$mysqli->query($sql);
				}
			}
		}
	}

	function process_keep_from($file) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no', 'ref_membno', 'recv_period');

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_keep_from ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
	    	fclose($handle);
			unset($fields);
		}
	}

	/*
	function process_insurance1($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "member_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 3; $i++) {
									if($i == 2)													$tmp[] = "'".ConvertToSQLDate(T2U($row[$i]))."'";
									else																$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_insurance1(member_no, insdesc, insdtm) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	function process_insurance2($data) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no', 'ins_no', 'insdtm');

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_insurance2 ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
	    	fclose($handle);
			unset($fields);
		}
	}

	function process_keep($file) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// reformat date
					/*list($m,$d,$y) = explode('/', $data[2]);
					$data[2] = "$y-$m-$d";*/

					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','recv_period','operdtm','receipt_no','keepitemtype_code',
							'keepdesc','period','principal_payment','interest_payment','item_payment','itembal');

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_keep ($fields) VALUES('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	/*function process_keepdep($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// reformat date
					list($m,$d,$y) = explode('/', $data[2]);
					$data[2] = "$y-$m-$d";

					// member_no,	recv_period,	operdtm,		receipt_no,		account_no_1,	deposit_amt_1,		account_no_2,		deposit_amt_2,		account_no_3,	deposit_amt_3,	account_no_4,	deposit_amt_4,	account_no_5,	deposit_amt_5,	apply_fee
					// 04425,		201512,			12/25/2015,		590101551,		0202232,		2000,				1202736,			1000,				1202737,		1000,			,				0,				,				0,				0

					// $data = array();
					$detail = array_slice($data, 0, 4);
					$index = 4;
					$accounts = array();
					while($index < 10){
						array_push($accounts, array_slice($data, $index, 2));
						$index += 2;
					}

					foreach($accounts as $k => $account){
						if($account[1] > 0){
							$data = array_merge($detail, $account);

							// convert to utf-8
							foreach($data as $i => $row){
								$data[$i] = iconv("tis-620", "utf-8", $row);
							}

							// lists fields
							$fields = array('member_no','recv_period','operdtm','receipt_no','keepitemtype_code','item_payment');

							// insert new data
							$fields = implode(",", $fields);
							$value = implode("','", $data);
							$sql = "INSERT INTO cmp_imp_keepdep ($fields) VALUES('{$value}')";
							$mysqli->query($sql);
							unset($where, $value, $sql);
						}
					}

					// // convert to utf-8
					// foreach($data as $i => $row){
					// 	$data[$i] = iconv("tis-620", "utf-8", $row);
					// }
					//
					// // lists fields
					// $fields = array('member_no','recv_period','operdtm','receipt_no','keepitemtype_code',
					// 		'keepdesc','period','principal_payment','interest_payment','item_payment','itembal');
					//
					// // insert new data
					// $fields = implode(",", $fields);
					// $value = implode("','", $data);
					// $sql = "INSERT INTO cmp_imp_keepdep ($fields) VALUES('{$value}')";
					// $mysqli->query($sql);
					// unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}*/

	function process_keepint($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','interest_accum','sharestk_value');

					// insert new data
					$fields = implode(",", $fields);
					// $value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_keepint ($fields) VALUES ('{$data['0']}', '{$data['3']}', '{$data['4']}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_keepshare($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// delete duplicate data
					$fields = array('member_no','recv_period','sharestk_value');
					$where = array();

					/*foreach($fields as $i => $index){
						// convert to utf-8
						$data[$i] = iconv("tis-620", "utf-8", $data[$i]);
						// add conditions
						array_push($where, "{$index} = '{$data[$i]}'");
					}
					$where = implode(" AND ", $where);
					$sql = "DELETE FROM cmp_imp_keepshare WHERE {$where}";
					$mysqli->query($sql);*/

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_keepshare ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	function process_loan_coll($file) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','loancontract_no','seq_no','ref_collno','description');

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_loan_coll ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_loan_master($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','loancontract_no','lastdtm','startdtm','loanapprove_amt',
						'period_payamt','period_payment','principal_balance','loansts');

					// reformat date
					/*list($m,$d,$y) = explode('/', $data[2]);
					$data[2] = "$y-$m-$d";
					list($m,$d,$y) = explode('/', $data[3]);
					$data[3] = "$y-$m-$d";*/

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_loan_master ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_loan_statement($file) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// delete duplicate data
					$fields = array('member_no','loancontract_no','seq_no','operdtm','loanitemtype_code',
						'period','principal_payment','interest_payment','principal_balance','ref_docno');
					$where = array();

					/*foreach($fields as $i => $index){
						if($index != "operdtm"){
							// convert to utf-8
							$data[$i] = iconv("tis-620", "utf-8", $data[$i]);
							// add conditions
							array_push($where, "{$index} = '{$data[$i]}'");
						}else{
							// reformat date
							list($m,$d,$y) = explode('/', $data[3]);
							$data[3] = "$y-$m-$d";
						}
					}
					$where = implode(" AND ", $where);
					$sql = "DELETE FROM cmp_imp_loan_statement WHERE {$where}";
					$mysqli->query($sql);*/

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_loan_statement ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
		    fclose($handle);
			unset($fields);
		}
	}

	function process_member($file) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','memname','membgroup_code','membgroup_desc','membtype_desc','birthdtm',
						'card_person','position_desc','salary_amount','incomeetc_amount','memdtm');

					// reformat date
					/*list($m,$d,$y) = explode('/', $data[5]);
					$data[5] = "$y-$m-$d";
					list($m,$d,$y) = explode('/', $data[10]);
					$data[10] = "$y-$m-$d";*/

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_member ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_share_master($file) {
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// convert to utf-8
					/*foreach($data as $i => $row){
						$data[$i] = iconv("tis-620", "utf-8", $row);
					}*/

					// lists fields
					$fields = array('member_no','lastdtm','sharebegin_amt','periodshareval','last_period','sharestkval','paymentsts');

					// reformat date
					/*list($m,$d,$y) = explode('/', $data[1]);
					$data[1] = "$y-$m-$d";*/

					// insert new data
					$fields = implode(",", $fields);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_share_master ($fields) VALUES ('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
				$i++;
		    }
			fclose($handle);
			unset($fields);
		}
	}

	function process_share_statement($file){
		global $mysqli;
		if(($handle = fopen("$file", "r")) !== FALSE){
		    while(($data = fgetcsv($handle, 0, ",")) !== FALSE){
				if(trim($data[0]) != "member_no"){
					// reformat date
					/*list($m,$d,$y) = explode('/', $data[1]);
					$data[1] = "$y-$m-$d";*/

					// delete duplicate data
					$field = array('member_no','operdtm','shritemtype_desc','period','shrval','shrbalval','ref_docno');
					$where = array();

					/*foreach($field as $i => $index){
						// convert to utf-8
						$data[$i] = iconv("tis-620", "utf-8", $data[$i]);
						array_push($where, "{$index} = '{$data[$i]}'");
					}
					$where = implode(" AND ", $where);
					$sql = "DELETE FROM cmp_imp_share_statement WHERE {$where}";
					$mysqli->query($sql);*/

					// insert new data
					$field = implode(",", $field);
					$value = implode("','", $data);
					$sql = "INSERT INTO cmp_imp_share_statement($field) VALUES('{$value}')";
					$mysqli->query($sql);
					unset($where, $value, $sql);
				}
	        }
	        fclose($handle);
			unset($field);
	    }
	}

	function process_welfare($file, $ext, $type){
		global $mysqli;
		if($ext == "xlsx") {
			$excelReader = PHPExcel_IOFactory::createReader("Excel2007");
			$excelReader->setReadDataOnly(true);
			$chunkSize = 1024;
			
			$excelObj = $excelReader->load($file);
			
			$objWorksheet = $excelObj->setActiveSheetIndex(0);
			$highest_row = $objWorksheet->getHighestRow();
			$highest_col = $objWorksheet->getHighestColumn();
			
			$row_index = 0;
			$row_e = 0;
			for ($startRow = 0; $startRow <= $highest_row; $startRow += $chunkSize) {
					$row_e = ($startRow + $chunkSize) > $highest_row ? $highest_row : ($startRow + $chunkSize);
					$data = $objWorksheet->rangeToArray('A'.($startRow + 1).':'.$highest_col.$row_e);
					
					foreach ($data as $row) {
						if($row_index++ > 0) {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 10; $i++) {
								if(in_array($i, array(5)))		$tmp[] = "'".excel2mysqlDate($row[$i])."'";
								else		$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_welfare(prename, firstname, lastname, membgroup_code, membgroup_desc, welfare_date, member_no, pay_type, ref_id, welfare_type) VALUES({$value})";
							$mysqli->query($sql);
						}
					}
					
					ob_flush();
					flush();
					unset($data);
			}
		}
	}
	
	/*
	function process_wf($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "member_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 3; $i++) {
									if(in_array($i, array(2)))					$tmp[] = "'".ConvertToSQLDate(T2U($row[$i]))."'";
									else																$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_wf(member_no, deptaccount_no, deptdtm) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	/*
	function process_wfkeep($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "member_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 8; $i++) {
									if(in_array($i, array(5, 6, 7)))			$tmp[] = (float)$row[$i];
									elseif(in_array($i, array(3)))				$tmp[] = "'".ConvertToSQLDate(T2U($row[$i]))."'";
									else																	$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_wfkeep(member_no, wfmember_no, wfaccount_name, operdtm, receipt_no, carcass_amt, fee_year, fee_app) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	/*
	function process_wfkeepname($data) {
			global $mysqli;
			foreach ($data as $row) {
					if(trim($row[0]) != "deptaccount_no") {
							unset($tmp, $value);
							$tmp = array();
							for($i = 0; $i < 4; $i++) {
									$tmp[] = "'".T2U($row[$i])."'";
							}
							$value = implode(",", $tmp);
							$sql = "INSERT INTO cmp_imp_wfkeepname(deptaccount_no, prename_desc, deptaccount_name, deptaccount_sname) VALUES({$value})";
							$mysqli->query($sql);
					}
			}
	}
	*/

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;
	if($do == "process-check"){
			if(file_exists(PATH."/uploads/tmp/import_process.inc.php")){
					$data = unserialize(file_get_contents(PATH."/uploads/tmp/import_process.inc.php"));
					$process = (int)$data["process"];
					$total = (int)$data["total"];
					echo "<i class='fa fa-spin fa-refresh'></i>&nbsp;ประมวลผล {$process} / {$total}";
			} else echo "<i class='fa fa-spin fa-refresh'></i>&nbsp;ประมวลผลไฟล์";
			exit();
	}

	if($do == "process"){
			$tmp = pathinfo($_POST["fname"]);
			$ext = $tmp['extension'];
			if (in_array($ext, array('csv', 'xlsx'))) {
				$file = PATH."/uploads/tmp/{$_POST["fname"]}";

				switch($_POST["ref"]) {
						case "address" 			: process_address($file); 				break;
						case "dept_master" 		: process_dept_master($file); 			break;
						case "dept_statement" 	: process_dept_statement($file); 		break;
						case "divavg" 			: process_divavg($file, $ext); 				break;
						case "keep_from" 		: process_keep_from($file); 			break;
						case "keep" 			: process_keep($file); 					break;
						case "keepdep" 			: process_keepdep($file); 				break;
						case "keepint" 			: process_keepint($file); 				break;
						case "keepshare" 		: process_keepshare($file);				break;
						case "loan_coll" 		: process_loan_coll($file); 			break;
						case "loan_master" 		: process_loan_master($file); 			break;
						case "loan_statement" 	: process_loan_statement($file); 		break;
						case "member" 			: process_member($file); 				break;
						case "share_master" 	: process_share_master($file); 			break;
						case "share_statement" 	: process_share_statement($file); 		break;
						case "wf" 				: process_wf($file); 					break;
						case "welfare" 				: process_welfare($file, $ext); 					break;
						default : break;
				}
				// unset($data);
			}

			$id = genid();
			$sql = "INSERT INTO cmp_imp_stat(stat_id, admin_id, import_file , update_time, update_ip) VALUES('{$id}', {$_SESSION["ADMIN_ID"]}, '{$_POST["ref"]}' , NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
			$mysqli->query($sql);
			exit();
	}

	if($do == "unlink") {
			if($_POST["fname"]) @unlink(PATH."/uploads/tmp/{$_POST["fname"]}");
			exit();
	}

	if($do == "record_count") {
			$sql = "SELECT COUNT(*) AS _c FROM cmp_imp_{$_POST["ref"]}";
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc();
			$c = number_format($row["_c"], 0);
			echo "ข้อมูลปัจจุบัน {$c} Rec.";
			exit();
	}

	if($do == "truncate") {
			$mysqli->query("TRUNCATE TABLE cmp_imp_{$_POST["ref"]}");
			exit();
	}

	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />

 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
<script src="<?php echo BASEPATH; ?>/scripts/blueimp.fileupload/9.9.3/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo BASEPATH; ?>/scripts/blueimp.fileupload/9.9.3/js/jquery.iframe-transport.js"></script>
<script src="<?php echo BASEPATH; ?>/scripts/blueimp.fileupload/9.9.3/js/jquery.fileupload.js"></script>
<script>
		$(document).ready(function(){
	/*
	 *	@ File upload
	 *****************************************************/
				var url = "import_excel.upload.php";
				var obj;

				$('#fileupload').fileupload({
						url: url,
						dataType: 'json',
						done: function (e, data) {
								//obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;กำลังประมวลผลไฟล์");
								
								$.each(data.result.files, function (index, file) {
									//console.log(file.name);
									process_file(file.name, obj.attr("ref"));

								});
								//obj.parents(".form-group").find(".help-block").html("");
								//$(".btn-import").removeAttr("disabled");
								//obj = null;
						},
						progressall: function (e, data) {
								$(".help-block").html("");
								$(".btn-import").attr("disabled", "disabled");
								var progress = parseInt(data.loaded / data.total * 100, 10);
								obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;อัพโหลด " + progress + "%");
						}
				}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');


	/*
	*			End File upload
	*****************************************************/

				$("body").on("click", ".btn-import", function() {
						obj = $(this);
						$('#fileupload').click();
				});

				//var timer_process;

				function process_file(filename, ref) {
						obj.parents(".form-group").find(".help-block").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;กำลังประมวลผลไฟล์");
						//timer_process = setInterval(function(){ check_process() }, 2000);
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "process"
											, "fname" : filename
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: true
							, success: function(data) {
									console.log(data);
									console.log("process " + filename + " complete");
									var obj_count = obj.parents(".form-group").find(".record-count");
									get_rec_count(obj_count, ref);

									//check_process_stop();
									obj.parents(".form-group").find(".help-block").html("");
									$(".btn-import").removeAttr("disabled");
									obj = null;
									// unlink(filename);
							}
						});
				}

				function check_process() {
						console.log("process-check");
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "process-check"
											, "_time" : Math.random()
										}
							, async: true
							, success: function(data) {
									if(obj) {
										console.log(data);
										obj.parents(".form-group").find(".help-block").html(data);
									}
							}
						});
				}

				function check_process_stop() {
						clearInterval(timer_process);
				}

				if($(".record-count").length) {
						$(".record-count").each(function() {
								get_rec_count($(this), $(this).attr("ref"));
						});
				}

				function unlink(filename) {
					$.ajax({
						type: "POST"
						, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
						, data: {
										"do" : "unlink"
										, "fname" : filename
										, "_time" : Math.random()
									}
						, async: false
						, success: function(data) {
								console.log("del " + filename + " complete")
						}
					});
				}

				function get_rec_count(obj_count, ref) {
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "record_count"
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: false
							, success: function(data) {
									obj_count.html(data);
							}
						});
				}

				$("body").on("click", ".btn-truncate-confirm", function() {
						var ref = $(this).attr("ref");
						var obj_count = obj.parents(".form-group").find(".record-count");
						$.ajax({
							type: "POST"
							, url: window.location.pathname.split( '/' )[window.location.pathname.split( '/' ).length-1]
							, data: {
											"do" : "truncate"
											, "ref" : ref
											, "_time" : Math.random()
										}
							, async: false
							, success: function(data) {
									$("#modal").modal("hide");
									obj_count.html("");
									get_rec_count(obj_count, ref);
									obj = null;
							}
						});
				});
				$("body").on("click", ".btn-truncate", function() {
						obj = $(this);
						$("#modal").find(".modal-title").html("<h3>ยืนยันการล้างข้อมูล</h3>");
						$("#modal").find(".modal-body").html("<div class='text-center'><h3>ต้องการล้างข้อมูล</h3><q>" + $(this).attr("title") + "</q></div>");
						$("#modal").find(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>ปิดหน้าต่าง</button><button type='button' class='btn btn-warning btn-truncate-confirm' ref='" + $(this).attr("ref") + "'><i class='fa fa-trash'></i>&nbsp;ต้องการล้างข้อมูล</button>");
						$("#modal").modal("toggle");
				});

		});
</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
	ob_start();
?>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "นำเข้าข้อมูล" );
	$smarty->assign("breadcrumb", array("นำเข้าข้อมูล" => "import_excel.php") );

	 ob_start();
?>
		<div class="row">
				<div class="col-md-12">
						<div class="panel panel-white">
								<div class="panel-heading clearfix">
										<h4 class="panel-title">&nbsp;</h4>
								</div>
								<div class="panel-body">
										<?php echo $msg->display() ;  ?>
										<?php
												$import_file = array(
																								//array("caption" => "Address"			, "ref" => "address"),
																										// array("caption" => "AG"					, "ref" => "ag"),
																								//array("caption" => "Dept Master"		, "ref" => "dept_master"),
																								//array("caption" => "Dept Statement"		, "ref" => "dept_statement"),
																								//array("caption" => "DivAvg"				, "ref" => "divavg"),
																										// array("caption" => "Insurance1"			, "ref" => "insurance1"),
																								//array("caption" => "Insurance"			, "ref" => "insurance2"),
																								//array("caption" => "Keep"				, "ref" => "keep"),
																										// array("caption" => "Keepdep"			, "ref" => "keepdep"),
																								//array("caption" => "KeepFrom"			, "ref" => "keep_from"),
																								//array("caption" => "Keepint"			, "ref" => "keepint"),
																								//array("caption" => "Keepshare"			, "ref" => "keepshare"),
																								//array("caption" => "Loan coll"			, "ref" => "loan_coll"),
																								//array("caption" => "Loan Master"		, "ref" => "loan_master"),
																								//array("caption" => "Loan Statement"		, "ref" => "loan_statement"),
																								//array("caption" => "Member"				, "ref" => "member"),
																								//array("caption" => "Share Master"		, "ref" => "share_master"),
																								//array("caption" => "Share Statement"	, "ref" => "share_statement"),
																										// array("caption" => "WF"					, "ref" => "wf"),
																										// array("caption" => "WFKeep"				, "ref" => "wfkeep"),
																										// array("caption" => "WFKeepName"			, "ref" => "wfkeepname"),
																								array("caption" => "สวัสดิการ"				, "ref" => "welfare"),
																						);
										?>
										<form class="form-horizontal" action="" method="post" >
												<input id="fileupload" type="file" name="files[]" class="hide" accept=".xlsx" />
												<?php foreach($import_file as $key => $val) { ?>
												<div class="form-group">
														<label class="col-sm-3 control-label">ไฟล์ <?php echo $val["caption"]; ?></label>
														<div class="col-sm-3">
																<button type="button" class="btn btn-info btn-block btn-import" ref="<?php echo $val["ref"]; ?>">นำเข้าข้อมูล</button>
																<div class="help-block text-center text-info"></div>
														</div>
														<div class="col-sm-2 form-control-static record-count" ref="<?php echo $val["ref"]; ?>"></div>
														<div class="col-sm-2">
																<button type="button" class="btn btn-danger btn-block btn-truncate" ref="<?php echo $val["ref"]; ?>" title="<?php echo $val["caption"]; ?>">ล้างข้อมูล</button>
														</div>
												</div>
												<?php } ?>
										</form>
								</div>
						</div>
				</div>
		</div>

		<div class="modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
						<div class="modal-content">
								<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h2 class="modal-title" id="myModalLabel"></h2>
								</div>
								<div class="modal-body"></div>
								<div class="modal-footer"></div>
						</div>
				</div>
		</div>
<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
