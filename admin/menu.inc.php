<?php ob_start(); 
	$file = basename($_SERVER['PHP_SELF']);
	$perlog = $cmpper->get_login_permission_code() ; 
 ?>
 <!--
	<div class="sidebar-header">
		<div class="sidebar-profile">
			<a href="javascript:void(0);" id="profile-menu-link">
				<div class="sidebar-profile-image">
					<img src="/timthumb.php?src=/images/blank.png&h=50&w=50" class="img-circle img-responsive" alt="">
				</div>
				<div class="sidebar-profile-details">
					<span><?php echo $admin["admin_name"] ?><br></span>
				</div>
			</a>
		</div>
	</div>
	-->
	<ul class="menu accordion-menu">
		<li class="active"><a href="index.php" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>หน้าหลัก</p></a></li>
		<!--
		<?php if(in_array("import" , $perlog )){  ?>
		<li><a href="import_excel.php" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-import"></span><p>นำเข้าข้อมูล</p></a></li>
		<?php  } ?>
		-->
		<?php if(in_array("setting" , $perlog )){  ?>
 		<li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon fa fa-gear"></span><p>ตั้งค่าระบบ</p><span class="arrow"></span></a>
			<ul class="sub-menu">
 				<li><a href="setting.php">ตั้งค่าระบบ</a></li>
				<li><a href="admin.php">ผู้ใช้งานระบบ</a></li>
 			</ul>
		</li>
		<?php  } ?>
		<?php if(in_array("website" , $perlog )){  ?>
		<li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon fa fa-desktop"></span><p>ข้อมูลเว็บไซต์</p><span class="arrow"></span></a>
			<ul class="sub-menu">
				<li><a href="page.php">หน้าเนื้อหา</a></li>
				<li><a href="menu.php">จัดการเมนู</a></li>
 				<li><a href="document.php">เอกสารดาวน์โหลด</a></li>
				<li><a href="chairman.php">ประธานคณะกรรมการ</a></li>
				<li><a href="boards.php">คณะกรรมการ</a></li>
				<li><a href="staff.php">คณะกรรมการ(โครงสร้าง)</a></li>
				<li><a href="staff_off.php">เจ้าหน้าที่สหกรณ์</a></li>
				<li><a href="staff_check.php">คณะผู้ตรวจสอบกิจการ</a></li>
				<li><a href="album.php">อัลบั้มภาพ</a></li>
				<li><a href="interest.php?type=deposit">อัตราดอกเบี้ยเงินฝาก</a></li>
				<li><a href="interest.php?type=loan" >อัตราดอกเบี้ยเงินกู้</a></li>
				<li><a href="link.php">ลิงค์ไปยังเว็บไซต์อื่น</a></li>
				<li><a href="journal.php?type=journal">วารสาร</a></li>
				<li><a href="journal.php?type=letter">จดหมายข่าว</a></li>
				<li><a href="slide.php"  > สไลด์ </a></li>
				<li><a href="banner.php"  > แบนเนอร์ </a></li>
				<li><a href="contactinfo.php">ติดต่อเรา </a></li>
				<li><a href="calendar.php">ปฏิทิน </a></li>
				<li><a href="importantday.php">วันสำคัญ </a></li> 
				

 			</ul>
		</li>
		<?php  } ?>
		<?php if(in_array("news" , $perlog )){  ?>
 		<li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-flash"></span><p>ข่าวสารสหกรณ์</p><span class="arrow"></span></a>
			<ul class="sub-menu"> 				
 				<li><a href="news.php?type=activity">ข่าวกิจกรรม</a></li>
				<!--<li><a href="news.php?type=new">ข่าวใหม่</a></li>
				<li><a href="news.php?type=credits">ข่าวสินเชื่อ</a></li>
				<li><a href="news.php?type=welfare">ข่าวสวัสดิการ</a></li>
				<li><a href="news.php?type=other">ข่าวประกาศอื่นๆ</a></li>
				<!--<li><a href="news.php?type=membership">สมาชิกภาพ</a></li>-->
				<li><a href="news.php?type=finance">สารสัมพันธ์</a></li>
				<li><a href="news.php?type=tender">ข่าวน่าสนใจ</a></li>
				<li><a href="news.php?type=notify">ข่าวประชาสัมพันธ์</a></li>
				<!-- 
				<li><a href="news.php?type=aid">ข่าวสมาคมฌาปนกิจสงเคราะห์</a></li>
				-->
			</ul>
		</li>
		<?php  } ?>
		<?php if(in_array("board" , $perlog )){  ?>
		
		
		<?php  } ?>
		<?php if(in_array("member" , $perlog )){  ?>
		<!-- 
            <li><a href="member.php" class="waves-effect waves-button"><span class="menu-icon fa fa-users"></span><p>ข้อมูลสมาชิก</p></a></li>
		-->
        <?php  } ?>
		<?php if(in_array("contact" , $perlog )){  ?>
  		<li><a href="contact.php" class="waves-effect waves-button"><span class="menu-icon fa fa-comment-o"></span><p>ข้อร้องเรียน/ข้อเสนอแนะ</p></a></li>
		<?php  } ?>
		<?php if(in_array("viewwebsite" , $perlog )){  ?>
		<!-- 
		<li><a href="#" class="waves-effect waves-button"><span class="menu-icon fa fa-area-chart "></span><p>สถิติผู้เข้าชมเว็บไซต์</p></a></li>
		-->
		<?php  } ?>
		<?php if(in_array("logging" , $perlog )){  ?>
		<li><a href="logging.php" class="waves-effect waves-button"><span class="menu-icon fa fa-terminal"></span><p>การเข้าใช้งานระบบ</p></a></li>
		<!-- 
		<li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon fa fa-mobile"></span><p>แอปพลิเคชั่น</p><span class="arrow"></span></a>
			<ul class="sub-menu"> 				
 				<li><a href="chart.system.php">สถิติการใช้งานแอป</a></li>
				<li><a href="switch_application.php">เปิด/ปิดการใช้งานแอป</a></li>
				<li><a href="mobile_message.php">ส่งข้อความ</a></li>
				<li><a href="mobile_message_group.php">กลุ่มผู้รับข้อความ</a></li>
			</ul>
		</li>
		-->
	<li><a href="userlog.php" class="waves-effect waves-button"><span class="menu-icon fa fa-terminal"></span><p>Log</p></a></li>	<?php  } ?>
	<?php if($_SESSION["ADMIN_ID"] == 1 ){ ?>
	<!-- 
	<li><a href="/loan/admin/setting_loan.php" class="waves-effect waves-button"><span class="menu-icon fa fa-terminal"></span><p>ระบบการขอกู้ออนไลน์</p></a></li>
	-->
	<?php } ?>

		<!-- 
		<li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-flash"></span><p>Levels</p><span class="arrow"></span></a>
			<ul class="sub-menu">
				<li class="droplink"><a href="#"><p>Level 1.1</p><span class="arrow"></span></a>
					<ul class="sub-menu">
						<li class="droplink"><a href="#"><p>Level 2.1</p><span class="arrow"></span></a>
							<ul class="sub-menu">
								<li><a href="#">Level 3.1</a></li>
							</ul>
						</li>
						<li><a href="#">Level 2.2</a></li>
					</ul>
				</li>
				<li><a href="#">Level 1.2</a></li>
			</ul>
		</li>
		-->
	</ul>
 <!-- <script>
 console.log(<?php echo $_SESSION["ADMIN_ID"]; ?>);
 </script> -->
	<?php
	unset($perlog) ; 
	$menu_right = ob_get_contents();
	ob_end_clean();
	$smarty->assign("menu_right", $menu_right);
