<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;

	$cmpper->check("setting") ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');


	if($_POST){
		$hiddenValue = (int)@$mysqli->real_escape_string(@$_POST["hiddenValue"]);
		$title = @$mysqli->real_escape_string(@$_POST["msg_title"]);
		$desc = @$mysqli->real_escape_string(@$_POST["msg_desc"]);
		$member_list = @$mysqli->real_escape_string(@$_POST["member_list"]);
		$sql = "SELECT set_id FROM cmp_application ORDER BY set_id DESC";
		$rs = $mysqli->query($sql);
		if ( $rs->num_rows ) {
			$row = $rs->fetch_assoc();
			$set_id = $row['set_id'];
			if ( $hiddenValue == 0 ) {
				$sql = "UPDATE cmp_application SET set_status = {$hiddenValue}, set_title = '{$title}', set_desc = '{$desc}'";
			} else if ( $hiddenValue == 1 ) {
				$sql = "UPDATE cmp_application SET set_status = {$hiddenValue}";
			} else if ( $hiddenValue == 2 ) {
				$sql = "UPDATE cmp_application SET set_status = {$hiddenValue}, set_title = '{$title}', set_desc = '{$desc}', set_member = '{$member_list}'";
			}
		} else {
			$sql = "INSERT INTO cmp_application (set_status,set_title,set_desc,set_member)
                VALUES({$hiddenValue},'{$title}','{$desc}','{$member_list}')";
		}
		$rs  = $mysqli->query($sql);
		echo $mysqli->error ;
		$msg->add("s" , "! บันทึกข้อมูลแล้ว" );

		echo "<script> window.location.href = \"?\"</script>";
		exit();
	}

	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />

<?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>
	<!-- <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script> -->
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>

<style>
	.row-left{ width: 20%; text-align: right; display: inline-block; margin: 10px 0; }
	.row-right{ width: 79%; display: inline-block; padding-left: 2em; margin: 10px 0; }
	.ver-top{ vertical-align: top;}
	.form-horizontal .radio{ padding-top: 4px; }
	.form-control {
		display: block;
		width: 100%;
		padding: .375rem .75rem;
		line-height: 1.5;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid #ced4da;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
	.radio input[type=radio]{
		margin-top: -1px;
		margin-left: -10px;
	}
</style>

<script>
	$(function(){
		$('input[name="rad"]').click(function(){
			var $radio = $(this);
			if ($radio.data('waschecked') == true) {
				$radio.prop('checked', false);
				$radio.data('waschecked', false);
			} else {
				$radio.data('waschecked', true);
			}
			$radio.siblings('input[name="rad"]').data('waschecked', false);
			
			if ( $radio[0].className == 'switch1' ) {
				$('textarea').prop('disabled', true);
				$('#input-box-1').prop('disabled', false);
				$('#input-box-2').prop('disabled', false);
				$('.hidden-value').val(0);
			} else if ( $radio[0].className == 'switch2' ) {
				$('textarea').prop('disabled', true);
				$('#input-box-1').prop('disabled', true);
				$('#input-box-2').prop('disabled', true);
				$('.hidden-value').val(1);
			} else {
				$('textarea').prop('disabled', false);
				$('#input-box-1').prop('disabled', false);
				$('#input-box-2').prop('disabled', false);
				$('.hidden-value').val(2);
			}

		});

		var myTxtArea = document.getElementById('area-text');
		myTxtArea.value = myTxtArea.value.replace(/^\s*|\s*$/g,'');
	});
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);

	$smarty->assign("title", "เปิด/ปิด การใช้งานแอป" );
	$smarty->assign("page_title", "เปิด/ปิด การใช้งานแอป" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("เปิด/ปิด การใช้งานแอป" => "#"));

	ob_start();

	$sql = "SELECT * FROM cmp_application";
	$rs = $mysqli->query($sql);
	$setting =  $rs->fetch_assoc();
	$member_list = trim($setting['set_member']);
	$set_title = trim($setting['set_title']);
	$set_desc = trim($setting['set_desc']);
?>
	 <div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ข้อมูลเว็บไซต์</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ; ?>
					<form class="form-horizontal" action="?" method="post" enctype="multipart/form-data">
                        <div class="row-left">การใช้งานแอปพลิเคชั่น</div>
						<div class="row-right">
							<div class="form-check">
								<input class="switch1" type="radio" name="rad" <?php echo ($setting['set_status'] == 0) ? 'checked' : '' ?>>
								<label>ปิดการใช้งานทั้งหมด</label>&nbsp;&nbsp;&nbsp;
								<input class="switch2" type="radio" name="rad" <?php echo ($setting['set_status'] == 1) ? 'checked' : '' ?>>
								<label>เปิดการใช้งานทั้งหมด</label>&nbsp;&nbsp;&nbsp;
								<input class="switch3" type="radio" name="rad" <?php echo ($setting['set_status'] == 2) ? 'checked' : '' ?>>
								<label>เปิดใช้เฉพาะรหัสต่อไปนี้</label>
								<input class="hidden-value" type="hidden" name="hiddenValue" value="<?php echo $setting['set_status'] ?>" />
							</div>
						</div>
						<div class="row-left ver-top">เปิดใช้งานเฉพาะรหัสต่อไปนี้</div>
						<div class="row-right">
							<textarea id="area-text" class="form-control" rows="6" cols="80" name="member_list" <?php echo ($setting['set_status'] == 2) ? '' : 'disabled' ?>>
								<?php echo $member_list; ?>
							</textarea>
							<br>
							เช่น 45,222,145682
						</div>
						<div class="row-left">หัวข้อ</div>
						<div class="row-right"><input type="text" id="input-box-1" class="form-control" name="msg_title" value="<?php echo $set_title; ?>" <?php echo ($setting['set_status'] == 1) ? 'disabled' : '' ?>></div>
						<div class="row-left">ข้อความ</div>
						<div class="row-right"><input type="text" id="input-box-2" class="form-control" name="msg_desc" value="<?php echo $set_desc; ?>" <?php echo ($setting['set_status'] == 1) ? 'disabled' : '' ?>></div>
						<div class="row-left"></div>
						<div class="row-right"><button type="submit" name="submit" class="btn btn-success">บันทึกข้อมูล</button></div>
					</form>
				</div>
			</div>
		</div>
	 </div>

<?php

	$content = ob_get_contents();
	ob_end_clean();

	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
