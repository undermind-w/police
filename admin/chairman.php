<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	
	if($_POST){
		if(@$_POST["in"] == "chairman" ){
			$cmplog->add_log("chairman-edit" , "แก้ไขข้อมูลประธานกรรมการ " ) ;
			 
			$st_board_chair_name = @$mysqli->real_escape_string(@$_POST["st_board_chair_name"]) ; 
			$st_board_chair_position = @$mysqli->real_escape_string(@$_POST["st_board_chair_position"]) ; 
			
			$st_board_chair_picture = ""; 
			if(!empty($_FILES["st_board_chair_picture"]["tmp_name"] )) {
				$ints = date("YmdGis") . @random_char(7) ; 
				
				if($_FILES["st_board_chair_picture"]["type"] =="image/png"||$_FILES["st_board_chair_picture"]["type"] =="image/x-png"){
					$imgsn = $ints.".png";
				}elseif($_FILES["st_board_chair_picture"]["type"] =="image/gif"){
					$imgsn = $ints.".gif";
				}elseif($_FILES["st_board_chair_picture"]["type"] =="image/pjpeg"||$_FILES["st_board_chair_picture"]["type"] =="image/jpeg"){
					$imgsn = $ints.".jpg";
				}
				if(!empty($imgsn)) {
					@copy($_FILES["st_board_chair_picture"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
					
					// CHECK SIZE IMAGE
					$size = getimagesize (PATH . "/uploads/contents/{$imgsn}" ) ; 
					if($size[0] > 350 ){
						$url = "http://{$_SERVER["SERVER_NAME"]}/timthumb.php?src=/uploads/contents/{$imgsn}&w=350&zc=0";
						$img = PATH . "/uploads/contents/{$imgsn}" ;
						file_put_contents($img, file_get_contents($url));
					}
					// END CHECK IMAGE
					
					$st_board_chair_picture = "{$imgsn}"  ;
				}
			}
			
			$sql_st_board_chair_picture = !empty($st_board_chair_picture) ? " , st_board_chair_picture = '{$st_board_chair_picture}' " : "" ;
			
			
			$sql = "UPDATE cmp_website SET 
								st_board_chair_name = '{$st_board_chair_name}' 
								, st_board_chair_position = '{$st_board_chair_position}' 
								{$sql_st_board_chair_picture}
								, updatedate = NOW() 
								WHERE st_id = 1  LIMIT 1 " ;
			$rs  = $mysqli->query($sql);
			echo $mysqli->error  ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			echo "<script> window.location.href = \"?\"</script>" ;
			exit();
		}
	}
 
	
 		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>
	<script>
		$(function(){
			$("#frm_document").validate();

		});
	</script >
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 
 
	
 	
	$smarty->assign("title", "ประธานกรรมการ" );
	$smarty->assign("page_title", "ประธานกรรมการ" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("ประธานกรรมการ" => "#") );
	 
	 ob_start();
	 $sql = "SELECT * FROM cmp_website WHERE st_id = 1 " ;
	 $rs = $mysqli->query($sql);
	$setting =  $rs->fetch_assoc();
?>
	 <div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ประธานกรรมการ</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_chair" class="form-horizontal" action="?" method="post" enctype="multipart/form-data"  >
						<input type="hidden" id="in" name="in" value="chairman" />
 						
						<div class="form-group">
 							<div class="col-sm-4 col-sm-offset-4">
								<?php if(!empty($setting["st_board_chair_picture"])){  ?>
								<div>
									<img style="margin:auto" class="img-responsive" src="/uploads/contents/<?php echo $setting["st_board_chair_picture"] ?>" />
								</div>
								<?php } ?>
								<input type="file" id="st_board_chair_picture" name="st_board_chair_picture"   class="form-control"  />
 							</div>
						</div>
						
						 <div class="form-group">
 							<div class="col-sm-4 col-sm-offset-4">
								<input type="text" class="form-control" id="st_board_chair_name" name="st_board_chair_name" placeholder="ชื่อ - สกุล" value="<?php echo @$setting["st_board_chair_name"] ?>" >
 							</div>
						</div>
						  
						  <div class="form-group">
 							<div class="col-sm-4 col-sm-offset-4">
								<input type="text" class="form-control" id="st_board_chair_position" name="st_board_chair_position" placeholder="ตำแหน่ง" value="<?php echo @$setting["st_board_chair_position"] ?>" >
 							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 col-sm-offset-4">
								<button type="submit" class="btn btn-success btn-block">บันทึกข้อมูล</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
<?php 
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 