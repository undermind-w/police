<style>
    .msg-send-content{
        background: #fff !important;
        padding: 20px;
    }
    .title-text, label{
        display: inline-block;
    }
    table{
        width: 100%;
    }
    .row-msg{
        height: 80px;
        margin: 10px 0;
    }
    .content-text{
        position: absolute;
        top: 29.2%;
    }
    .btn{
        background: #548159 !important;
        color: #fff !important;
        padding: 10px 30px !important;
    }
    .td-btn{
        text-align: center !important;
        padding-top: 20px;
    }
    .form-title, .drop-down-group, .member_list{
        font-size: 1.1em !important;
    }
    .drop-down-group{
        width: 200px !important;
    }
    .form-control {
        display: block;
        width: 100%;
        height: calc(1.5em + .75rem + 2px);
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .member_list{
        width: 100%;
    }
</style>

<script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({ 
        selector:'textarea.content',
        themes: "modern",   
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "textcolor colorpicker"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | image",
        images_upload_url: 'tinymce.upload.php',

        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', 'tinymce.upload.php');
            xhr.onload = function() {
                var json;
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);
                console.log('json', json)

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.location);
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        } 

    });
</script>

<?php
    define( 'API_ACCESS_KEY', 'AAAASxPbYAU:APA91bHnYzd06vU2Cgz1bElOVFkwkrg8bionKKYC_3jWLwz00dIbSE7OK-p7v6hDQe4XIbf0oTMPK6EB2iuYuygQ5zxUGA897C-pQ3UvcLfqj7et-DxiWRXxhVs_pL99wyoer_EScy_y' );
    function fcmPushMessage($title, $msg_id, $badge, $token) {         
        $data_json = array( "title" => $title, "badge" => $badge, "msg_id" => $msg_id );
        $forNotification = array( 'title' => $title, 'sound' => 'default', 'badge' => $badge, "click_action" => "FCM_PLUGIN_ACTIVITY");

        $fields = array( 'to' => $token, 'notification'	=> $forNotification, 'data' => $data_json, 'badge' => $badge);
        $headers = array( 'Authorization: key=' . API_ACCESS_KEY, 'Content-Type:application/json' );

        #Send Reponse To FireBase Server	 
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec( $ch );
        curl_close($ch);
        return $result;
    }

    $sql = "SELECT mobile_group_id, mobile_group_name FROM mobile_message_group WHERE is_active = 1 ORDER BY create_date ASC";
    $rs = $mysqli_system->query($sql);

    if (!empty($_POST)) {

        $group_id = $_POST['group'];
        $title = $_POST['title'];
        $msg = str_replace('src="', 'src="https://www.spktcoop.com/admin/', $_POST['content']);
        $msg = str_replace('../', 'https://www.spktcoop.com/', $msg);

        if ( trim($title) == '' OR trim($msg) == '' ) {

            echo "<script>setTimeout(() => { alert('กรุณากรอกข้อมูลให้ครบถ้วน'); }, 1000);</script>";

        } else {

            $sql = "INSERT INTO mobile_message (admin_delete, create_date, msg_title, msg_message)
            VALUES (0, NOW(), '{$title}', '{$msg}')";
            if ( $mysqli_system->query($sql) === TRUE ) {
                $msg_id = $mysqli_system->insert_id;
    
                if ( $group_id == 'all') { // all device
    
                    $member_id = '';
                    $sql = "SELECT mobile_token_message, member_id FROM mobile_token WHERE is_activate = 1 
                    AND is_delete = 0 AND mobile_token_message != '' AND mobile_token_message != 'null' AND mobile_token_message != '(null)'";
                    $rs = $mysqli_system->query($sql);
                    if ( $rs->num_rows ) {
                        while( ($row = $rs->fetch_assoc()) ) {
                            $member_id = $row['member_id'];
                            $token = $row['mobile_token_message'];
                            $sql = "INSERT INTO mobile_message_validate (msg_id, member_id, user_read, user_delete)
                            VALUES ({$msg_id}, '{$member_id}', 0, 0)";
                            $mysqli_system->query($sql);
    
                            $sql = "SELECT COUNT(tb1.validate_id) AS badge FROM mobile_message_validate AS tb1
                            INNER JOIN mobile_message AS tb2 ON tb2.msg_id = tb1.msg_id
                            WHERE tb2.admin_delete = 0 AND tb1.user_read = 0 AND tb1.user_delete = 0 AND tb1.member_id = '{$member_id}'";
                            $badge = $mysqli_system->query($sql);
                            $badgeRow = $badge->fetch_assoc();
                            $badge = ( $badgeRow['badge'] ) ? $badgeRow['badge'] : 0;
    
                            fcmPushMessage($title, $msg_id, $badge, $token);
                        }
                    } else {
                        echo "<script>alert('ไม่พบรายชื่อสมาชิก');</script>";
                    }
    
                } else if ( $group_id == 'people') {
                    
                    $member_list = explode(",", $_POST['member_list']);
                    for ( $i = 0; $i < COUNT($member_list); $i++ ) {
                        $member_id = trim($member_list[$i]);
    
                        $sql = "INSERT INTO mobile_message_validate (msg_id, member_id, user_read, user_delete)
                        VALUES ({$msg_id}, '{$member_id}', 0, 0)";
                        $mysqli_system->query($sql);
    
                        $sql = "SELECT mobile_token_message FROM mobile_token WHERE trim(member_id) = '{$member_id}' AND is_activate = 1 AND is_delete = 0 ORDER BY create_date DESC LIMIT 1";
                        $rs = $mysqli_system->query($sql);
                        $row = $rs->fetch_assoc();
                        $token = $row['mobile_token_message'];
                        
                        $sql = "SELECT COUNT(tb1.validate_id) AS badge FROM mobile_message_validate AS tb1
                        INNER JOIN mobile_message AS tb2 ON tb2.msg_id = tb1.msg_id
                        WHERE tb2.admin_delete = 0 AND tb1.user_read = 0 AND tb1.user_delete = 0 AND tb1.member_id = '{$member_id}'";
                        $badge = $mysqli_system->query($sql);
                        $badgeRow = $badge->fetch_assoc();
                        $badge = ( $badgeRow['badge'] ) ? $badgeRow['badge'] : 0;
    
                        fcmPushMessage($title, $msg_id, $badge, $token);
                    }
                    echo "<script>window.location.href = \"/admin/mobile_message.php\"</script>";
    
                } else {
                    
                    $sql = "SELECT member_list FROM mobile_message_group_person WHERE mobile_group_id = {$group_id}";
                    $rs = $mysqli_system->query($sql);
                    $row = $rs->fetch_assoc();
                    
                    $member_list = explode(",", $row['member_list']);
                    for ( $i = 0; $i < COUNT($member_list); $i++ ) {
                        $member_id = trim($member_list[$i]);
    
                        $sql = "INSERT INTO mobile_message_validate (msg_id, member_id, user_read, user_delete)
                        VALUES ({$msg_id}, '{$member_id}', 0, 0)";
                        $mysqli_system->query($sql);
    
                        $sql = "SELECT mobile_token_message FROM mobile_token WHERE trim(member_id) = '{$member_id}' AND is_activate = 1 AND is_delete = 0 ORDER BY create_date DESC LIMIT 1";
                        $rs = $mysqli_system->query($sql);
                        $row = $rs->fetch_assoc();
                        $token = $row['mobile_token_message'];
                        
                        $sql = "SELECT COUNT(tb1.validate_id) AS badge FROM mobile_message_validate AS tb1
                        INNER JOIN mobile_message AS tb2 ON tb2.msg_id = tb1.msg_id
                        WHERE tb2.admin_delete = 0 AND tb1.user_read = 0 AND tb1.user_delete = 0 AND tb1.member_id = '{$member_id}'";
                        $badge = $mysqli_system->query($sql);
                        $badgeRow = $badge->fetch_assoc();
                        $badge = ( $badgeRow['badge'] ) ? $badgeRow['badge'] : 0;
    
                        fcmPushMessage($title, $msg_id, $badge, $token);
                    }
                    echo "<script>window.location.href = \"/admin/mobile_message.php\"</script>";

                }
    
            } else {
                echo "Error: ".$mysqli_system->error;
            }

        }

    }

?>

<div class="msg-send-content">
    <?php require "alert.content.php"; ?>
    <form class="form-horizontal" action="?do=send" method="post" enctype="multipart/form-data">
        <table>
            <tbody>
                <tr>
                    <td width="15%" class="row-msg"><label>หัวเรื่อง : </label></td>
                    <td width="85%" class="row-msg"><input type="text" class="form-control form-title" name="title" /></td>
                </tr>
                <tr>
                    <td width="15%" class="row-msg"><label class="content-text">ข้อความ : </label></td>
                    <td width="85%" class="row-msg"><textarea class="content" name="content" rows="30"></textarea></td>
                </tr>
                <tr>
                    <td width="15%" class="row-msg"><label>ส่งถึง : </label></td>
                    <td width="85%" class="row-msg">
                        <select name="group" class="form-control drop-down-group" onChange="ChangeSelectBox(this.selectedIndex)">
                            <option value="all">ทั้งหมด</option>
                            <option value="people">รายบุคคล</option>
                            <?php while( ($row = $rs->fetch_assoc()) ) { ?>
                                <option value="<?php echo $row["mobile_group_id"];?>"><?php echo $row["mobile_group_name"];?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="15%"></td>
                    <td width="85%"><textarea class="form-control member_list" name="member_list" rows="5" style="display:none" placeholder="ตัวอย่าง 000001, 000002"></textarea></td>
                </tr>
                <tr>
                    <td width="15%"></td>
                    <td width="85%" class="td-btn"><input class="btn" type="submit" value="ตกลง" /></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>

<script>
    function ChangeSelectBox(index) {
        if ( index == 1 ) {
            document.getElementsByClassName("member_list")[0].style.display = 'block';
        } else {
            document.getElementsByClassName("member_list")[0].style.display = 'none';
        }
    }
</script>
