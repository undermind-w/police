<?php
	session_start();
	require "config.inc.php" ;
	require "menu.inc.php" ;
 	require "session.inc.php" ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	
	if($_POST){
		if($_POST["do"] == "password" ){
			
			$sql = "SELECT * FROM upc_admin WHERE  admin_id = {$admin_id} LIMIT 0 , 1  ";
			$rs = $mysqli->query($sql);
			$admin = $rs->fetch_assoc();
			
			$npassowrd = $mysqli->real_escape_string($_POST["new_password"]) ; 
			$cpassword = $mysqli->real_escape_string($_POST["confirm_password"]) ; 
				
			if(empty($npassowrd)){
				$msg->add("w" , "! กรุณากรอกรหัสผ่าน" ) ; 
				echo "<script> window.location.href = \"?\"</script>" ;
				exit();
				
			}
			elseif($npassowrd != $cpassword)
			{
				$msg->add("w" , "! รหัสผ่านไม่เหมือนกัน" ) ; 
				echo "<script> window.location.href = \"?\"</script>" ;
				exit();
				
			}
			 
			$sql = "UPDATE upc_admin SET admin_password = '{$npassowrd}' WHERE admin_id = {$admin_id}  LIMIT 1 " ;
			$rs  = $mysqli->query($sql);
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			echo "<script> window.location.href = \"?\"</script>" ;
			exit();
		}
	}
 
	
 		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>
	<script>
		$(function(){
			
			
		});
	</script >
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 
 
	
 	
	$smarty->assign("title", "เปลี่ยนรหัสผ่าน" );
	$smarty->assign("page", "เปลี่ยนรหัสผ่าน" );
	$smarty->assign("sub", "เปลี่ยนรหัสผ่าน" );
	$smarty->assign("breadcrumb", array("เปลี่ยนรหัสผ่าน" => "#") );
	 
	 ob_start();
?>
	<div class="row">
		<div class="col-md-12">
		<div class="portlet box green ">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i> เปลี่ยนรหัสผ่าน
				</div>
			</div>
			<div class="portlet-body form">
				<?php echo  $msg->display(); ?>
				<form class="form-horizontal" id="frm_change_passowrd" name="frm_change_passowrd" role="form" method="post" action="?">
					<input type="hidden" name="do" value="password" />
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-3 control-label">รหัสผ่านใหม่</label>
							<div class="col-md-9">
								<input type="password" name="new_password" class="form-control" placeholder="New Password">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">ยืนยันรหัสผ่านใหม่</label>
							<div class="col-md-9">
								<input type="password" name="confirm_password" class="form-control" placeholder="Confirm NewPassword">
							</div>
						</div>
					</div>
					<div class="form-actions fluid">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn green">บันทึกข้อมูล</button>
							<button type="button" class="btn default">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
<?php 
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 