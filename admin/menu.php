<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$menu_id = (int) @$_POST["menu_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
		$menu_root = (int) @$_POST["menu_root"] ; 
		
  		$menu_type = 1  ;
		$menu_title = @$mysqli->real_escape_string(@$_POST["menu_title"]) ;
		$menu_refid = @$mysqli->real_escape_string(@$_POST["menu_refid"]) ;
		$menu_url = @$mysqli->real_escape_string(@$_POST["menu_url"]) ;
		$menu_position = @$mysqli->real_escape_string(@$_POST["menu_position"]) ;
		
  		$menu_status = (int) @$_POST["menu_status"] ; 
		
		
	 
		if(empty($menu_id))
		{
			
			$sql = "SELECT MAX(menu_seq) as _max  FROM cmp_menu WHERE menu_position = '{$menu_position}' AND menu_root = '{$menu_root}'  " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error  ; 
			$max = $rs->fetch_assoc();
			$menu_seq = (int) $max["_max"] + 1 ; 	
			
			$sql = "INSERT INTO cmp_menu 
											( 
											admin_id
											, menu_seq 
											, menu_root
											, menu_type
											, menu_position 
											, menu_title 
 											, menu_refid 
											, menu_url 
   											, createdate
											, updatedate
											, menu_status 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$menu_seq}'
											, '{$menu_root}'
											, '{$menu_type}'
											, '{$menu_position}'
											, '{$menu_title}'
 											, '{$menu_refid}'
											, '{$menu_url}'
 											, NOW() 
											, NOW() 
											, {$menu_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$menu_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("menu-add" , "เพิ่มเมนู "  , $menu_id    ) ;
		}
		else
		{	
  			$sql = "UPDATE cmp_menu SET menu_type = '{$menu_type}' 
												, menu_title = '{$menu_title}'  
												, menu_refid = '{$menu_refid}'  
												, menu_url = '{$menu_url}'  
												, menu_type = '{$menu_type}' 
   												, updatedate = NOW() 
												, menu_status = {$menu_status}
												WHERE 
												menu_id = '{$menu_id}' LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("menu-edit" , "แก้ไขเมนู"  , $menu_id    ) ;
		}
		 
		echo "<script> window.location.href = \"?position={$menu_position}\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("menu"))  )
	{
		$menu_id = (int) @$_GET["mid"] ;  
		$menu_position = (int) @$_GET["position"] ;  
 		$sql = "DELETE FROM cmp_menu WHERE menu_id = '{$menu_id}'  LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("menu-delete" , "ลบเมนู"  , $menu_id    ) ;
		echo "<script> window.location.href = \"?position={$menu_position}\"</script>" ;
		exit();
	}
	
	
	if(@in_array(@$_GET["do"],array("down","up"))) {
		
		$menu_root = (int) @$_GET["rid"] ;  
		$menu_id = (int) @$_GET["mid"] ;  
		$menu_position = (int) @$_GET["position"] ;  
		
		$sql = "SELECT menu_id , menu_seq FROM cmp_menu WHERE menu_position = '{$menu_position}' AND  menu_root = '{$menu_root}'  ORDER BY menu_seq ASC ";
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$i = 0;
		while(($row = $rs->fetch_assoc())) {
			if($row["menu_id"]== @ $menu_id ) {
				$pos = $i;
			} else{
				$array[$i++] = $row["menu_id"];
			}
		}

		if(@$_GET["do"]=="down") {
			$pos++;
		} else {
			$pos--;
		}
			

		$count = count($array)+1;
		
		for($i=0;$i<$count;$i++) {
			if($i == $pos) {
				$tmp[$i] = @$menu_id ;
			} else {
				$tmp[$i] = @$array[0];
				array_shift($array);
			}
		}
		for($i = 0 ; $i<count($tmp) ;$i++) {
			$index = $i + 1 ; 
			$sql = "UPDATE cmp_menu SET menu_seq = '{$index}'  WHERE menu_id = '{$tmp[$i]}'";
			$mysqli->query($sql);
			echo $mysqli->error; 
		}
		
		$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
		echo "<script type=\"text/javascript\">document.location.href='?';</script>";
 		exit();
	}
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_menu").validate();
		$("#position1").change(function(){
			window.location.href = '?position=' + $(this).val(); 
		});
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
		
		if($("#page_detail").length) {
			$("#page_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
		}
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	
	$menu_position = (int) empty($_GET["position"]) ? 1 : $_GET["position"] ;
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		$menu_root = (int) $_GET["rid"] ; 
		if(@in_array($_GET["do"] , array("edit"))){
			$menu_id = (int) @$_GET["mid"] ; 
			$sql = "SELECT * FROM cmp_menu WHERE menu_id = {$menu_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "จัดการเมนู" );
		$smarty->assign("page_title", "จัดการเมนู" );
 		$smarty->assign("breadcrumb", array("จัดการเมนู" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$menu_id = (int) @$row["menu_id"]  ; 
 		$menu_status = (int) @$row["page_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">จัดการเมนู</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_page" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="menu_id" value="<?php echo $menu_id ;  ?>"/>
						<input type="hidden" name="menu_root" value="<?php echo $menu_root ;  ?>"/>
						<input type="hidden" name="menu_position" value="<?php echo $menu_position ;  ?>"/>
						
						<div class="form-group">
							<label for="menu_title" class="col-sm-3 control-label">ชื่อหน้า</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="menu_title" name="menu_title" placeholder="" value="<?php echo htmlspecialchars(@$row["menu_title"]) ?>" required="required" >
 							</div>
						</div>
						
						 <div class="form-group">
							<label for="menu_url" class="col-sm-3 control-label">ลื้งค์</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="menu_url" name="menu_url" placeholder="" value="<?php echo htmlspecialchars(@$row["menu_url"]) ?>" required="required" >
 							</div>
						</div>
 
  
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="menu_status" name="menu_status" <?php echo $row["menu_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "จัดการเมนู" );
		$smarty->assign("page_title", "จัดการเมนู" );
		$smarty->assign("breadcrumb", array("จัดการเมนู" => "#") );
		
		ob_start();
		/*
		$sql = "SELECT COUNT(page_id) as _c FROM cmp_menu" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		*/
		
		$menu_type = (int) $_GET["type"] ; 
		if(empty($menu_type)){
			$menu_type = 1 ; 
		}
		
		
		$sql = "SELECT * FROM  cmp_menu WHERE menu_root = 0 AND menu_position = '{$menu_position}'  ORDER BY menu_seq ASC " ;
		$rs1 = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows - ($per_page * ($page - 1 ) )  ; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">จัดการเมนู</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <select class="form-control pull-left" id="position1" style="width:200px" >
				<option value="1" <?php echo $menu_position == 1 ? "selected" :"" ;  ?> >ตำแหน่งบน</option>
				<option value="2" <?php echo $menu_position == 2 ? "selected" :"" ;  ?>  >ตำแหน่งล่าง (เกี่ยวกับสหกรณ์)</option>
				<option value="3" <?php echo $menu_position == 3 ? "selected" :"" ;  ?>  >ตำแหน่งล่าง (บริการสมาชิก)</option>
				<option value="4" <?php echo $menu_position == 4 ? "selected" :"" ;  ?>  >ตำแหน่งล่าง (อื่นๆ)</option>
			 </select>
			 <a class="btn btn-info pull-right" href="?do=add&position=<?php echo $menu_position ?>"  ><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
						<th class="text-center" colspan="" width="100" >ลำดับ</th>
						<th class="text-center" colspan="" width="100" ></th>
 						<th class="text-center" >ชื่อเมนู</th>
  						<th class="text-center" width="250" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					
					<?php 
					$i = 1 ; 
					while($row1 = $rs1->fetch_assoc()){
 
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i    ?></td>
						<td style="text-align:center" colspan="2" width="200" >
							<?php if($i != $rs1->num_rows ){ ?>
								<a href="?do=down&in=menu&mid=<?php echo $row1["menu_id"]; ?>&position=<?php echo $menu_position ?>"><i class="fa fa-arrow-down"></i></a>
							<?php } ?>
							
							<?php if($i != 1 ){ ?>
								<a href="?do=up&in=menu&mid=<?php echo $row1["menu_id"]; ?>&position=<?php echo $menu_position ?>"><i class="fa fa-arrow-up"></i></a>
							<?php }  ?>
						</td>
						<td><a target="_blank" href="#" ><?php echo @$row1["menu_title"] ?></a></td>
  						<td style="text-align:center" >
							<?php if($menu_position == 1 ){ ?>
							<a href="?in=menu&do=add&rid=<?php echo @$row1["menu_id"] ?>&position=<?php echo $menu_position ?>" >เพิ่มเมนูย่อย</a> 
							<?php } ?>
							<a href="?in=menu&do=edit&mid=<?php echo @$row1["menu_id"] ?>&position=<?php echo $menu_position ?>" ><i class="fa fa-edit"></i></a> 
							<a href="?in=menu&do=delete&mid=<?php echo @$row1["menu_id"] ?>&position=<?php echo $menu_position ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>  
 						</td>
                      </tr>
					<?php 
						$sql  = "SELECT * FROM cmp_menu WHERE menu_root = '{$row1["menu_id"]}' ORDER BY menu_seq ASC" ; 
						$rs2 = $mysqli->query($sql);
						echo $mysqli->error;  
						$j = 1 ; 
						while($row2 = $rs2->fetch_assoc()){
					?>
						 <tr>
							<td style="text-align:center" ></td>
							<td style="text-align:center" width="100"><?php echo $j ?></td>
							<td style="text-align:center" width="100" >
								<?php if($j != $rs2->num_rows ){ ?>
								<a href="?do=down&in=menu&mid=<?php echo $row2["menu_id"]; ?>&position=<?php echo $menu_position ?>&rid=<?php echo $row1["menu_id"] ?>"><i class="fa fa-arrow-down"></i></a>
							<?php } ?>
 							<?php if($j != 1 ){ ?>
								<a href="?do=up&in=menu&mid=<?php echo $row2["menu_id"]; ?>&position=<?php echo $menu_position ?>&rid=<?php echo $row1["menu_id"] ?>"><i class="fa fa-arrow-up"></i></a>
							<?php }  ?>
							</td>
							<td><a target="_blank" href="#" ><?php echo @$row2["menu_title"] ?></a></td>
							<td style="text-align:center" >
								<a href="?in=menu&do=edit&mid=<?php echo @$row2["menu_id"] ?>&position=<?php echo $menu_position ?>" ><i class="fa fa-edit"></i></a> 
								<a href="?in=menu&do=delete&mid=<?php echo @$row2["menu_id"] ?>&position=<?php echo $menu_position ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>  
							</td>
						  </tr>
					<?php 	
							$j ++ ; 
						}
						
						$i ++ ; 
					} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 