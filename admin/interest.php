<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
		
		if(@$_POST["in"] == "interest" ){
			$inr_id = (int) @$_POST["inr_id"] ; 
			$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
			
			$inr_code = @$mysqli->real_escape_string(@$_POST["inr_code"]) ;
			$inr_title = @$mysqli->real_escape_string(@$_POST["inr_title"]) ;
			$inr_url = @$mysqli->real_escape_string(@$_POST["inr_url"]) ;
			$inr_percent = (double) @$_POST["inr_percent"]  ;

			$inr_status = (int) @$_POST["inr_status"] ; 
			
			
		 
			if(empty($inr_id))
			{
				$sql = "INSERT INTO cmp_interest 
												( 
												admin_id
												, inr_code 
												, inr_title 
												, inr_percent 
												, inr_url 
												, createdate
												, updatedate
												, inr_status 
												) 
												VALUES  
												(
												'{$admin_id}'
												, '{$inr_code}'
												, '{$inr_title}'
												, '{$inr_percent}'
												, '{$inr_url}'
												, NOW() 
												, NOW() 
												, {$inr_status} ) " ;
				$mysqli->query($sql);
				echo $mysqli->error ;
				$inr_id = (int) $mysqli->insert_id ; 
				
				$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
				
				$cmplog->add_log("interest-add" , "เพิ่มอัตราดอกเบี้ย  " , $inr_id ) ;
				
			}
			else
			{	
				$sql = "UPDATE cmp_interest SET inr_title = '{$inr_title}' 
 													, inr_percent = '{$inr_percent}' 
													, inr_url = '{$inr_url}'
													, updatedate = NOW() 
													, inr_status = {$inr_status}
													WHERE 
													inr_id = {$inr_id} LIMIT 1  " ;
				$mysqli->query($sql);
				echo $mysqli->error ;								
				$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
				
				$cmplog->add_log("interest-edit" , "แก้ไขอัตราดอกเบี้ย  " , $inr_id ) ;
			}
			 
			echo "<script> window.location.href = \"?type={$inr_code}\"</script>" ;
			exit();
		}
		elseif(@$_POST["in"] == "config")
		{
			$inr_code = $mysqli->real_escape_string(@$_POST["inr_code"]);
			$st_interest_date = $mysqli->real_escape_string(@$_POST["st_interest_date"]);
			$sql = "UPDATE cmp_website SET st_interest_date = '{$st_interest_date}' , updatedate = NOW() WHERE st_id = 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
			$cmplog->add_log("interestconfig-edit" , "แก้ไชวันที่อัตราดอกเบี้ย  " ) ;

			echo "<script> window.location.href = \"?type={$inr_code}\"</script>" ;
			exit();
		}
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("interest"))  )
	{
		$inr_id = (int) @$_GET["iid"] ;
		$inr_code =  $_GET["type"] ;
		
 		$sql = "DELETE FROM cmp_interest WHERE inr_id = '{$inr_id}' LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("interest-delete" , "ลบอัตราดอกเบี้ย  " , $inr_id ) ;

		echo "<script> window.location.href = \"?type={$inr_code}\"</script>" ;
		exit();
	}
	
	if(@in_array(@$_GET["do"],array("down","up"))) {
		$inr_code = $mysqli->real_escape_string($_GET["type"]);
		
		$sql = "SELECT inr_id , inr_seq FROM cmp_interest WHERE inr_code = '{$inr_code}' ORDER BY inr_seq ASC ";
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$i = 0;
		while(($row = $rs->fetch_assoc())) {
			if($row["inr_id"]== @$_GET["iid"]) {
				$pos = $i;
			} else{
				$array[$i++] = $row["inr_id"];
			}
		}

		if(@$_GET["do"]=="down") {
			$pos++;
		} else {
			$pos--;
		}
			

		$count = count($array)+1;
		
		for($i=0;$i<$count;$i++) {
			if($i == $pos) {
				$tmp[$i] = @$_GET["iid"];
			} else {
				$tmp[$i] = @$array[0];
				array_shift($array);
			}
		}
		for($i = 0 ; $i<count($tmp) ;$i++) {
			$index = $i + 1 ; 
			$sql = "UPDATE cmp_interest SET inr_seq = '{$index}'  WHERE inr_id = '{$tmp[$i]}'";
			$mysqli->query($sql);
			echo $mysqli->error; 
		}
		
		$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
		echo "<script type=\"text/javascript\">document.location.href='?type={$inr_code}';</script>";
 		exit();
	}
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		$("#frm_interest").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	$typearray = array("deposit" => "อัตราดอกเบี้ยเงินฝาก" , "loan" => "อัตราดอกเบี้ยเงินกู้" ) ; 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$inr_id = (int) @$_GET["iid"] ; 
			$sql = "SELECT * FROM cmp_interest WHERE inr_id = {$inr_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		
		$type_title = $typearray[$type ]  ; 
		
		
		$smarty->assign("title", "{$type_title}" );
		$smarty->assign("page_title", "{$type_title}" );
 		$smarty->assign("breadcrumb", array("{$type_title}" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$inr_id = (int) @$row["inr_id"]  ; 
		
		$inr_status = (int) @$row["inr_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title"><?php echo $type_title ?></h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_album" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="inr_id" value="<?php echo $inr_id ;  ?>"/>
  						<input type="hidden" name="in" value="interest"/>
						<input type="hidden" name="inr_code" value="<?php echo $type ?>"/>
						
						<div class="form-group">
							<label for="inr_title" class="col-sm-3 control-label">รูปแบบ</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="inr_title" name="inr_title" placeholder="" value="<?php echo htmlspecialchars(@$row["inr_title"]) ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="inr_percent" class="col-sm-3 control-label">อัตราดอกเบี้ย</label>
							<div class="col-sm-3">
								<input type="text"  class="form-control" id="inr_percent" name="inr_percent" placeholder="" value="<?php echo @$row["inr_percent"] ?>" required="required" >
  							</div>
						</div>
						
						<div class="form-group">
							<label for="inr_percent" class="col-sm-3 control-label">ลืงค์รายละเอียด</label>
							<div class="col-sm-3">
								<input type="text"  class="form-control" id="inr_url" name="inr_url" placeholder="" value="<?php echo @htmlspecialchars(@$row["inr_url"]) ?>"  >
  							</div>
						</div>
	
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="inr_status" name="inr_status" <?php echo $row["inr_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?type=<?php echo $type ?>" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		
		$page = (int) @$_GET["page"]  ; 
		
		
		
		ob_start();
		
		
		$sql = "SELECT st_interest_date FROM  cmp_website WHERE st_id = 1 LIMIT 0 , 1 " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		$config =  $rs->fetch_assoc(); 
		
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
 		$sql = "SELECT COUNT(inr_id) as _c FROM cmp_interest WHERE inr_code = '{$type}' " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_interest WHERE inr_code like '{$type}' ORDER BY inr_seq ASC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
 		 
		
		$type_title = $typearray[$type ]  ; 
		
		$smarty->assign("title", "{$type_title}" );
		$smarty->assign("page_title", "{$type_title}" );
		$smarty->assign("breadcrumb", array("{$type_title}" => "#") );
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title"><?php echo $type_title ?></h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add&type=<?php echo $type ?>"  ><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			<form class="form-inline" id="frm_interest_date" action="?" method="post" style="margin-bottom: 10px;" >
				<input type="hidden" name="in" value="config"/>
				<div class="form-group">
					<label for="st_interest_date">วันที่เริ่มใช้</label>
					<input type="text" class="form-control" id="st_interest_date" name="st_interest_date" value="<?php echo @$config["st_interest_date"] ?>" placeholder="" style="width:300px">
				</div>
				<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
			</form>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
						<th class="text-center" width="100" >ลำดับ</th>
 						<th class="text-center" >รูปแบบ</th>
						<th class="text-center" width="100"  >อัตรา</th>
 						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i    ?></td>
						<td style="text-align:center" > 
							<?php if($i != $rs->num_rows ){ ?>
								<a href="?do=down&in=interest&iid=<?php echo $row["inr_id"]; ?>&type=<?php echo $type ?>"><i class="fa fa-arrow-down"></i></a>
							<?php } ?>
							
							<?php if($i != 1 ){ ?>
								<a href="?do=up&in=interest&iid=<?php echo $row["inr_id"]; ?>&type=<?php echo $type ?>"><i class="fa fa-arrow-up"></i></a>
							<?php }  ?>
						</td>
						<td><a href="<?php !empty($row["inr_url"]) ? $row["inr_url"] : "#" ;  ?>"><?php echo @$row["inr_title"] ?></a></td>
						<td style="text-align:center" ><?php echo @$row["inr_percent"] ?></td>
 						<td style="text-align:center" >
							<a href="?in=interest&type=<?php echo $type ?>&do=edit&iid=<?php echo @$row["inr_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=interest&type=<?php echo $type ?>&do=delete&iid=<?php echo @$row["inr_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						$i ++ ; 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 