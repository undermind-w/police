<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");
	
	$do = isset($_POST["do"]) ? $_POST["do"] : null ;
	
	if($do == "update") {
			$group_id			= $mysqli->real_escape_string($_POST["id"]);
			$group_name 	= $mysqli->real_escape_string($_POST["group_name"]);
			$group_mail 	= $mysqli->real_escape_string($_POST["group_mail"]);
			
			if(empty($group_id)) {
					$group_id = genid();
					$sql = "INSERT INTO cmp_mailing_list_group(group_id, group_name, group_mail, create_time, create_ip, update_time, update_ip)
									VALUES('{$group_id}', '{$group_name}', '{$group_mail}', NOW(), '{$_SERVER["REMOTE_ADDR"]}', NOW(), '{$_SERVER["REMOTE_ADDR"]}')";
					$mysqli->query($sql);
			} else {
					$sql = "UPDATE cmp_mailing_list_group SET
											group_name		= '{$group_name}'
										, group_mail		= '{$group_mail}'
										, update_time		= NOW()
										, update_ip 		= '{$_SERVER["REMOTE_ADDR"]}'
									WHERE group_id = '{$group_id}'
									";
					$mysqli->query($sql);
			}
			echo "<script>document.location.href = 'mailing_list.group.php';</script>";
			exit();
	}
	
	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	if($do == "del") {
			$sql = "DELETE FROM cmp_mailing_list_group WHERE group_id = '{$_GET["id"]}'";
			$mysqli->query($sql);
			$mysqli->query("OPTIMIZE TABLE cmp_mailing_list_group");
			echo "<script>document.location.href = 'mailing_list.group.php';</script>";
			exit();
	}
		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
	<script src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<link rel="stylesheet" href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" />
	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>
	<style>
			.table tbody tr, .table tbody td { background-color: #FFF; }
			.table tbody td:last-child span {
					border-right: 2px solid #888;
					padding: 0px 5px 0px 0px;
			}
			.table tbody td:last-child span:last-child {
					border-right: 0px;
			}
			
.bs-callout {
    padding: 20px;
    margin: 20px 0;
    border: 1px solid #eee;
    border-left-width: 5px;
    border-radius: 3px
}

.bs-callout h4 {
    margin-top: 0;
    margin-bottom: 5px
}
.bs-callout p { text-indent: 30px; }
.bs-callout i { text-indent: 0px; }
.bs-callout p:last-child { margin-bottom: 0 }

.bs-callout code {
    border-radius: 3px
}

.bs-callout+.bs-callout {
    margin-top: -5px
}

.bs-callout-danger {
    border-left-color: #ce4844
}

.bs-callout-danger h4 {
    color: #ce4844
}

.bs-callout-warning {
    border-left-color: #aa6708
}

.bs-callout-warning h4 {
    color: #aa6708
}

.bs-callout-info {
    border-left-color: #1b809e;
		background-color: #FFF;
}

.bs-callout-info h4 {
    color: #1b809e
}
	</style>
	<script>
		$(document).ready(function() {
				if($('.date-picker').length) {
						$('.date-picker').datepicker({
									orientation: "top auto"
								, autoclose: true
								, format : "yyyy-mm-dd"
						});
				}
				if($(".ckeditor").length) {
							$(".ckeditor").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js' });
				}
				
				$("body").on("click", ".btn-submit", function() { 
						$("#frm").submit();
				});
		});
	</script>
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 
	ob_start();
 ?>
	
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 	
	$smarty->assign("title", "ส่งเมล์ถึงสมาชิก" );
	$smarty->assign("breadcrumb", array("กลุ่มสมาชิก" => "mailing_list.group.php") );
	ob_start();

	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	
	if(!$do) {
?>
	<div class="row">
			<div class="col-xs-12">
					<div class="pull-left"></div>
					<div class="pull-right"><a class='btn btn-info' href="?do=add">เพิ่มกลุ่มสมาชิก</a></div>
					<div class="clearfix"></div>
			</div>
	</div>
	<table class="table table-bordered m-t-md">
			<thead>
					<tr>
							<th class="text-center" width="60">ลำดับ</th>
							<th>ชื่อกลุ่ม</th>
							<th class="text-center" width="150">จำนวนเมล์</th>
							<th class="text-center" width="150">&nbsp;</th>
					</tr>
			</thead>
			<tbody>
					<?php
							$list_status = array(1 => "<span class='text-warning'>รอการส่ง</span>", "<span class='text-info'>กำลังส่ง</span>", "<span class='text-success'>ส่งสำเร็จ</span>");
							$sql = "SELECT *
											FROM cmp_mailing_list_group
											ORDER BY CONVERT(group_name USING TIS620)";
							$rs = $mysqli->query($sql);
							if($rs->num_rows == 0) {
									echo "<tr><td class='text-center f-bold' colspan='4'>ยังไม่มีรายการค่ะ</td></tr>";
							} else {
									$i = 1;
									while(($row = $rs->fetch_assoc())) {
											$group_mail = explode(",", $row["group_mail"]);
											$num_mail = count($group_mail);
					?>
					<tr class="text-center">
							<td><?php echo number_format($i++, 0); ?>.</td>
							<td class="text-left"><?php echo $row["group_name"]; ?></td>
							<td><?php echo number_format($num_mail, 0); ?></td>
							<td>
									<span><a href="?do=edit&id=<?php echo $row["group_id"]; ?>"><i class="fa fa-pencil m-r-xxs"></i>แก้ไข</a></span>
									<span><a class="text-danger" href="?do=del&id=<?php echo $row["group_id"]; ?>"><i class="fa fa-trash m-r-xxs"></i>ลบ</a></span>
							</td>
					</tr>
					<?php 	
									}
							}
					?>
			</tbody>	
	</table>
	<?php
			} else {
				$row = array();
				$id = isset($_GET["id"]) ? $_GET["id"] : null ;
				if($id) {
						$sql = "SELECT *
										FROM cmp_mailing_list_group
										WHERE group_id = '{$id}'";
						$rs = $mysqli->query($sql);
						$row = $rs->fetch_assoc();
				}
				
				$group_name 	= isset($row["group_name"]) ? $row["group_name"]  : null;
				$group_mail 	= isset($row["group_mail"]) ? $row["group_mail"]  : null;

	?>
	<div class="row">
			<div class="col-xs-12">
					<div class="panel panel-white">
							<div class="panel-heading clearfix">
								<h4 class="panel-title">ส่งเมล์ถึงสมาชิก</h4>
							</div>
							<div class="panel-body">
									<?php echo $msg->display() ;  ?>
									<form class="form-horizontal" id="frm" method="post">
											<div class="form-group">
													<label for="group_name" class="col-sm-3 control-label">ชื่อกลุ่มสมาชิก</label>
													<div class="col-sm-9">
															<input class="form-control" type="text" id="group_name" name="group_name" value="<?php echo $group_name; ?>"  />
													</div>
											</div>
											<div class="form-group">
													<label for="group_mail" class="col-sm-3 control-label">อีเมล์</label>
													<div class="col-sm-9">
															<textarea class="form-control" id="group_mail" name="group_mail" rows="10"><?php echo $group_mail ?></textarea>
															<p class="help-block">กรณีที่มีหลายเมล์ให้ใช้เครื่องหมาย , (จุลภาค) คั่น เช่น mail1@cmcoop.co.th,mail2@cmcoop.co.th,mail3@cmcoop.co.th เป็นต้น</p>
													</div>
											</div>
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-10">
													<button class="btn btn-success btn-submit" type="button">บันทึกข้อมูล</button>
													<a class="btn btn-danger m-l-xxs" href="?">ออก</a>
													<input type="hidden" name="id" value="<?php echo $id ;  ?>"/>
													<input type="hidden" name="do" value="update"/>
												</div>
											</div>
									</form>
							</div>
					</div>
			</div>
	</div>
	<?php } ?>
<?php 
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");