<?php
	session_start();
	 

	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$day_id = (int) @$_POST["day_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
  		$day_title = @$mysqli->real_escape_string(@$_POST["day_title"]) ;
 		$publicdate = @$mysqli->real_escape_string(@$_POST["publicdate"]) ;
		
		$publicdate_d = $_POST["publicdate_d"] ; 
		$publicdate_m = $_POST["publicdate_m"] ; 
		
		$publicdate = "2020-{$publicdate_m}-{$publicdate_d}" ; 
 
  		$is_status = (int) @$_POST["is_status"] ; 
		
		if(!empty($_FILES["day_picture"]["tmp_name"] )) {
			$ints = date("YmdGis") . @random_char(7) ; 
			
			if($_FILES["day_picture"]["type"] =="image/png"||$_FILES["day_picture"]["type"] =="image/x-png"){
				$imgsn = $ints.".png";
			}elseif($_FILES["day_picture"]["type"] =="image/gif"){
				$imgsn = $ints.".gif";
			}elseif($_FILES["day_picture"]["type"] =="image/pjpeg"||$_FILES["day_picture"]["type"] =="image/jpeg"){
				$imgsn = $ints.".jpg";
			}
			if(!empty($imgsn)) {
				 
				@copy($_FILES["day_picture"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
				$day_picture = "{$imgsn}"  ;
 			}
		}
		
		if(empty($day_id))
		{
			$sql = "INSERT INTO cmp_importantday 
											( 
											admin_id
											, day_title
											, day_picture 
   											, publicdate
											, createdate
											, updatedate
											, is_status 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$day_title}'
											, '{$day_picture}'
 											, '{$publicdate}'
 											, NOW() 
											, NOW() 
											, {$is_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$day_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("importantday-add" , "เพิ่มหน้าเนื้อหา "  , $day_id    ) ;
		}
		else
		{	
			$sql_picture = !empty($day_picture) ? " , day_picture = '{$day_picture}' " : "" ; ;
  			$sql = "UPDATE cmp_importantday SET day_title = '{$day_title}' 
												{$sql_picture}
												, publicdate = '{$publicdate}'  
   												, updatedate = NOW() 
												, is_status = '{$is_status}'
												WHERE 
												day_id = {$day_id} 
												LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("importantday-edit" , "แก้ไขหน้าเนื้อหา"  , $day_id ) ;
		}
		 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("importantday"))  )
	{
		$day_id = (int) @$_GET["id"] ;  
 		$sql = "DELETE FROM cmp_importantday WHERE day_id = '{$day_id}'  LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("importantday-delete" , "ลบหน้าเนื้อหา"  , $day_id    ) ;
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_page").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "dd-mm-yyyy"
		});
		
		if($("#page_detail").length) {
			$("#page_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
		}
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$day_id = (int) @$_GET["id"] ; 
			$sql = "SELECT * FROM cmp_importantday WHERE day_id = {$day_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "วันสำคัญ " );
		$smarty->assign("page_title", "วันสำคัญ " );
 		$smarty->assign("breadcrumb", array("วันสำคัญ " => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$day_id = (int) @$row["day_id"]  ; 
 		$is_status = (int) @$row["is_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">วันสำคัญ </h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_importnatday" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="day_id" value="<?php echo $day_id ;  ?>"/>
 						
						<div class="form-group">
							<label for="day_title" class="col-sm-3 control-label">ชื่อวัน</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="day_title" name="day_title" placeholder="" value="<?php echo htmlspecialchars(@$row["day_title"]) ?>" required="required" >
 							</div>
						</div>
						<div class="form-group">
							<label for="publicdate" class="col-sm-3 control-label">ทุกวันที่/เดือน </label>
							<div class="col-sm-3">
								<?php 
									$publicdate_d = (int)  empty($row) ? date("d") : date("d" , strtotime($row["publicdate"])) ; 
									$publicdate_m = (int) empty($row) ? date("m") : date("m" , strtotime($row["publicdate"])) ;
								?>
								<select class="form-control" id="publicdate_m" name="publicdate_m"  style="float:left;width:200px;" >
								<?php  
								for($i = 1 ; $i <= 12 ; $i ++ ){
									$tmonth = MonthName($i );
									$selected = $publicdate_m == $i ? "selected" : "" ; 
									echo "<option value=\"{$i}\" {$selected} > {$tmonth}</option>" ; 
								}
								?>
								</select>
								<select class="form-control" id="publicdate_d" name="publicdate_d"  style="float:left;width:150px;margin-right:10px;" >
								<?php  
								for($i = 1 ; $i <= 31 ; $i ++ ){
									$selected = $publicdate_d == $i ? "selected" : "" ; 
									echo "<option value=\"{$i}\" {$selected} >{$i}</option>" ; 
								}
								?>
								</select>
								
  							</div>
						</div>
						
						<div class="form-group">
							<label for="day_picture" class="col-sm-3 control-label">ภาพประกอบ</label>
							<div class="col-sm-9">
								<input type="file"  class="" id="day_picture" name="day_picture"  >
								<?php if(!empty($row["day_picture"])){ ?>
									<br />
 									<img src="/uploads/contents/<?php echo $row["day_picture"] ?>" style="min-width:250px"  />									
								<?php } ?>
 							</div>
						</div>
  
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="is_status" name="is_status" <?php echo $row["is_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "วันสำคัญ " );
		$smarty->assign("page_title", "วันสำคัญ " );
		$smarty->assign("breadcrumb", array("วันสำคัญ " => "#") );
		
		ob_start();

		$where_key_word = '';
		 
		$sql = "SELECT COUNT(day_id) as _c FROM cmp_importantday  "  ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;

		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_importantday ORDER BY publicdate ASC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows - ($per_page * ($page - 1 ) )  ; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">วันสำคัญ</h4>
		</div>
		<div class="panel-body">
			<?php echo $msg->display() ;  ?>
			<a class="btn btn-info pull-right" href="?do=add"  >เพิ่มข้อมูล</a>
			<div style="clear: both;margin-bottom:15px"></div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
						<th class="text-center" width="150" >วันที่</th>
 						<th class="text-center" >ชื่อวันสำคัญ</th>
						<th class="text-center" width="100" >สถานะ</th>
  						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
						$d = date("d" , strtotime($row["publicdate"])); 
						$m = date("m" , strtotime($row["publicdate"])); 
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i --  ?></td>
						<td class="text-center" ><?php echo "{$d} " . monthName($m);   ;?></td>
						<td><?php echo @$row["day_title"] ?></td>
  						<td><?php echo @$row["is_status"] == 1 ? "<span class=\"label label-success\" >เปิดใช้งาน</span>" : "<span class=\"label label-danger\" >ไม่เปิดใช้งาน</span>" ;  ?></td>
						<td style="text-align:center" >
							<a href="?in=importantday&do=edit&id=<?php echo @$row["day_id"] ?>" ><i class="fa fa-edit"></i></a> 
 							<a href="?in=importantday&do=delete&id=<?php echo @$row["day_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>  
 						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 