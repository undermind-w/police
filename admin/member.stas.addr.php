<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	if(@$_POST){
		if(@$_POST["do"] == "stats1")
		{
			$stat_id = @$mysqli->real_escape_string(@$_POST["id"]);
			$type = array("telmobile" => "โทรศัพท์มือถือ" , "addr" => "ที่อยู่ปัจจุบัน" , "addrc"  => "ที่อยู่ที่ติดต่อได้" , "tel"  => "โทรศัพท์บ้าน" , "email" => "อีเมล" ) ;
			$sql = "SELECT * FROM cmp_imp_member_data_stat WHERE stat_id = '{$stat_id}' " ;
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc() ; 
 			$label = @$type[@$row["stat_type"]] ; 
			$detail = @$row["stat_data"] ; 
			echo json_encode(array("type" => $label , "detail" => $detail )); 
			exit() ; 
		}
	}
 	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	  
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		$("#frm_journal").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
		
		$('#model_profile_status').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) ; 
				var modal = $(this) ;
				$.ajax({
					method: "POST",
					url: "?",
					data: { 'do' : 'stats1' , id :  $(button).data("id")  }
				}).done(function( data ) {
					var json = JSON.parse(data) 
					modal.find("#txt_type").val(json.type) ; 
 					modal.find("#txt_new").val(json.detail)  ;

				 });
 			});
			
		$("#btnExport").click(function(){
			var year = $("#year").val() ; 
			var month =  $("#month").val() ; 
			console.log("member.stas.addr.export.php?year=" + year  + "&month=" + month );
			window.location.href = "member.stas.addr.export.php?year=" + year  + "&month=" + month   ;
			
		})
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		 
	}
	else
	{
		if(empty($_GET["year"]) || empty($_GET["month"])  )
		{
			$year = date("Y") ; 
			$month = date("m") ; 
			
			echo "<script> window.location.href = \"?year={$year}&month={$month}\"</script>" ;
			exit();
		}
		
		$year = (int) @$_GET["year"] ; 
		
		$month =(int) @$_GET["month"] ; 
 		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "สมาชิกเปลี่ยนแปลงที่อยู่" );
		$smarty->assign("page_title", "สมาชิกเปลี่ยนแปลงที่อยู่" );
		$smarty->assign("breadcrumb", array("สมาชิกเปลี่ยนแปลงที่อยู่" => "#") );
		
		ob_start();
		
		$yy = $year ; 
		$mm = sprintf("%02d", $month);
		$logtable = "cmp_action_log_{$yy}{$mm}" ; 
		
		
		
		//$sql = "SELECT COUNT(log_id) as _c FROM {$logtable} " ;
		
		$sql = "SELECT  COUNT( cmp_imp_member_data_stat.stat_id ) as _c
 						FROM cmp_imp_member_data_stat 
							LEFT JOIN cmp_imp_member ON  cmp_imp_member_data_stat.member_no = cmp_imp_member.member_no				
						WHERE YEAR(cmp_imp_member_data_stat.update_time) = '{$year}' 
							AND MONTH(cmp_imp_member_data_stat.update_time) = '{$month}'" ; 
		$rs = $mysqli->query($sql);
		
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 25 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT  cmp_imp_member_data_stat.stat_id  
						, cmp_imp_member.memname  
						, cmp_imp_member_data_stat.update_time 
						FROM cmp_imp_member_data_stat 
							LEFT JOIN cmp_imp_member ON  cmp_imp_member_data_stat.member_no = cmp_imp_member.member_no				
						WHERE YEAR(cmp_imp_member_data_stat.update_time) = '{$year}' 
							AND MONTH(cmp_imp_member_data_stat.update_time) = '{$month}'
						ORDER BY  cmp_imp_member_data_stat.update_time DESC
						LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		  
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">สมาชิกเปลี่ยนแปลงที่อยู่</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
 			 <div style="clear: both;margin-bottom:15px"></div>
			<form class="form-inline" action="?" method="get" style="margin-bottom:10px;"  >
				<div class="form-group">
					<label for="month">เดือน</label>
					<select id="month" name="month" class="form-control" >
						<?php for($i = 1 ; $i  <= 12 ; $i++){ ?>
						<option value="<?php echo $i ?>" <?php echo $month == $i ? "selected" : "" ;  ?> ><?php echo MonthName($i) ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<label for="year">ปี</label>
					<select id="year" name="year" class="form-control" >
						<?php for($i = 2000 ; $i  <= 2020 ; $i++){ ?>
						<option value="<?php echo $i ?>" <?php echo $year == $i ? "selected" : "" ;  ?> ><?php echo $i ?></option>
						<?php } ?>
					</select>
				</div>
				<button type="submit" class="btn btn-primary"> ดูข้อมูล </button>
				<a id="btnExport" href="#" class="btn btn-primary" style="margin-left:10px;"> Export To Excel </a>
 			</form> 
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>ชื่อ </th>
						<th>วันที่</th>
						<th width="75" >ดูข้อมูล</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$i = 1 ; 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
						<td><?php echo $row["memname"] ?></td>
						<td><?php echo @mysqldate2thaidate( $row["update_time"]  , "long" ,  true  ) ?></td>
						<td class="text-center"><a data-id="<?php echo $row["stat_id" ]?>" href="#" data-toggle="modal" data-target="#model_profile_status"  ><i class="fa fa-search"></i></a></td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 <!-- Modal -->
	<div class="modal fade" id="model_profile_status" tabindex="-1" role="dialog" aria-labelledby="model_profile_status" >
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modelAddrStatsLabel">ดูข้อมูล</h4>
			</div>
			<div class="modal-body">
				<form>
				 <div class="form-group">
					<label for="txt_old">เปลี่ยน</label>
					<input type="text" class="form-control" id="txt_type"  value="" disabled />
				 </div>
				 <div class="form-group">
					<label for="txt_new">ข้อมูลใหม่</label>
					<textarea class="form-control" id="txt_new" disabled rows="3"> </textarea>
				  </div>
				 </form>
			</div>
		   
		</div>
	  </div>
	</div>
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 