<?php
    require "system.connect.php";

    $action = $_POST['action'] ? $_POST['action'] : null;

    $data = [ "success" => 0 , "responseText" => "" ];

    if ( $action == 'get' ) {

        $group_id = $_POST['group_id'] ? $_POST['group_id'] : null;

        $sql = "SELECT member_list FROM mobile_message_group_person WHERE mobile_group_id = {$group_id}";
		$group = $mysqli_system->query($sql);
        $row = $group->fetch_assoc();
        if ( $group->num_rows ) {
            $data['success'] = 1;
            $person = explode(',', $row['member_list']);
            for ( $i = 0; $i < COUNT($person); $i++ ) {
                $data['member_list'][] = $person[$i];
            }
        }
        
    } else if ( $action == 'insert' ) {

        $group_name = $_POST['group_name'] ? $_POST['group_name'] : null;
        $data_member = $_POST['data'] ? $_POST['data'] : null;

        $sql = "SELECT mobile_group_id FROM mobile_message_group WHERE mobile_group_name = '{$group_name}'";
        $rs = $mysqli_system->query($sql);
        if ( !$rs->num_rows ) {

            $sql = "INSERT INTO mobile_message_group (mobile_group_name, create_date, is_active) VALUES ('{$group_name}', NOW(), 1)";
            if ( $mysqli_system->query($sql) === TRUE ) {
                $group_id = $mysqli_system->insert_id;
                
                $sql = "INSERT INTO mobile_message_group_person (mobile_group_id, member_list) VALUES ({$group_id}, '{$data_member}')";
                if ( $mysqli_system->query($sql) === TRUE ) {
                    $data['success'] = 1;
                }
            }

        } else {
            $data['success'] = 101;
        }

    } else if ( $action == 'edit' ) {

        $group_id = $_POST['group_id'] ? $_POST['group_id'] : null;
        $group_name = $_POST['group_name'] ? $_POST['group_name'] : null;
        $data_member = $_POST['data'] ? $_POST['data'] : null;

        $sql = "UPDATE mobile_message_group SET mobile_group_name = '{$group_name}' WHERE mobile_group_id = {$group_id}";
        if ( $mysqli_system->query($sql) === TRUE ) {
            $sql = "UPDATE mobile_message_group_person SET member_list = '{$data_member}' WHERE mobile_group_id = {$group_id}";
            if ( $mysqli_system->query($sql) === TRUE ) {
                $data['success'] = 1;
            }
        }

    } else if ( $action == 'delete' ) {

        $group_id = $_POST['group_id'] ? $_POST['group_id'] : null;

        $sql = "UPDATE mobile_message_group SET is_active = 0 WHERE mobile_group_id = {$group_id}";
        if ( $mysqli_system->query($sql) === TRUE ) {
            $data['success'] = 1;
        }

    }

    echo json_encode($data);
    exit();

?>