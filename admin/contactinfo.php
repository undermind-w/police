<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	
	if($_POST){
		if(@$_POST["in"] == "setting" ){
			 $cmplog->add_log("contactinfo-edit" , "แก้ไขข้อมูลติดต่อเรา " ) ;
			$ic_detail = htmlspecialchars(@$mysqli->real_escape_string(@$_POST["ic_detail"]) ,ENT_QUOTES ); 
			$ic_latitude = @$mysqli->real_escape_string(@$_POST["ic_latitude"]) ; 
			$ic_longitude = @$mysqli->real_escape_string(@$_POST["ic_longitude"]) ; 
  				
			 
			 
			$sql = "UPDATE cmp_info_contact SET 
								ic_detail = '{$ic_detail}' 
								, ic_latitude = '{$ic_latitude}' 
								, ic_longitude = '{$ic_longitude}' 
								, updatedate = NOW() 
								WHERE ic_id = 1  
								LIMIT 1 " ;
			$rs  = $mysqli->query($sql);
			echo $mysqli->error  ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			echo "<script> window.location.href = \"?\"</script>" ;
			exit();
		}
	}
 
	
 		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
		<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 	ob_start();
?>
	<script>
		$(function(){
			if($("#ic_detail").length) {
				$("#ic_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
			}
			
		});
	</script >
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 
 
	
 	
	$smarty->assign("title", "ข้อมูลติดต่อเรา" );
	$smarty->assign("page_title", "ข้อมูลติดต่อเรา" );
	$smarty->assign("sub", "" );
	$smarty->assign("breadcrumb", array("ข้อมูลติดต่อเรา" => "#") );
	 
	 ob_start();
	 $sql = "SELECT * FROM cmp_info_contact WHERE ic_id = 1 " ;
	 $rs = $mysqli->query($sql);
	$info =  $rs->fetch_assoc();
?>
	 <div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ข้อมูลติดต่อเรา</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form class="form-horizontal" action="?" method="post" >
						<input type="hidden" id="in" name="in" value="setting" />
						<div class="form-group">
							<label for="ic_latitude" class="col-sm-3 control-label">ละติจูด</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="ic_latitude" name="ic_latitude" placeholder="" value="<?php echo @$info["ic_latitude"] ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="ic_longitude" class="col-sm-3 control-label">ลองจิจูด</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="ic_longitude" name="ic_longitude" placeholder="" value="<?php echo @$info["ic_longitude"] ?>" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="ic_detail" class="col-sm-3 control-label">เนื้อหา</label>
							<div class="col-sm-9">
								 <textarea id="ic_detail" name="ic_detail" ><?php echo @$info["ic_detail"] ?></textarea>
 							</div>
						</div>
						 
						 
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
<?php 
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 