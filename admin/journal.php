<?php
	//phpinfo();
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 	
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$jol_id = (int) @$_POST["jol_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
  		$jol_title = @$mysqli->real_escape_string(@$_POST["jol_title"]) ;
  		
		
		$jol_pdf = "" ; 
		$jol_pdf_input = $_FILES["jol_pdf"] ; 
		if(!empty($jol_pdf_input["tmp_name"])) 
		{
			$ints = date("YmdGis") . @random_char(7) ; 
			$info = $jol_pdf_input["name"] ;
			$ext = pathinfo($info, PATHINFO_EXTENSION);
			if(in_array($ext, array( "pdf" )))
			{
				$imgsn = $ints . "." . $ext ; 

				@copy( $jol_pdf_input["tmp_name"] , PATH . "/uploads/journal/{$imgsn}" );
				
 				$jol_pdf = "{$imgsn}"  ;
				$mysqli->query($sql);
				echo $mysqli->error ; 
			}
		}
		$jol_picture = ""; 
		if(!empty($_FILES["jol_picture"]["tmp_name"] )) {
			$ints = date("YmdGis") . @random_char(7) ; 
			
			if($_FILES["jol_picture"]["type"] =="image/png"||$_FILES["jol_picture"]["type"] =="image/x-png"){
				$imgsn = $ints.".png";
			}elseif($_FILES["jol_picture"]["type"] =="image/gif"){
				$imgsn = $ints.".gif";
			}elseif($_FILES["jol_picture"]["type"] =="image/pjpeg"||$_FILES["jol_picture"]["type"] =="image/jpeg"){
				$imgsn = $ints.".jpg";
			}
			if(!empty($imgsn)) {
				 
				@copy($_FILES["jol_picture"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
				$jol_picture = "{$imgsn}"  ;
 			}
		}
		
		 
		
		$publicdate_date = @$mysqli->real_escape_string(@$_POST["publicdate"]) ;

		$publicdate_time_hour = (int) @$_POST["publicdate_time_hour"] ; 
		$publicdate_time_min = (int) @$_POST["publicdate_time_min"] ; 
		
		$publicdate_time_hour = sprintf("%02d",  $publicdate_time_hour  ) ; 
		$publicdate_time_min = sprintf("%02d",  $publicdate_time_min  ) ; 
		$publicdate  = $publicdate_date . " " . $publicdate_time_hour . ":" . $publicdate_time_min . ":00" ; 

		
		
  		$jol_status = (int) @$_POST["jol_status"] ; 
		$jol_type= @$mysqli->real_escape_string(@$_POST["jol_type"]) ;
		
	 
		if(empty($jol_id))
		{
			$sql = "INSERT INTO cmp_journal 
											( 
											admin_id 
											, jol_title 
											, jol_picture 
											, jol_pdf 
											, publicdate
  											, createdate
											, updatedate
											, jol_status 
											, jol_type 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$jol_title}'
											, '{$jol_picture}'
											, '{$jol_pdf}' 
											, '{$publicdate}'
 											, NOW() 
											, NOW() 
											, {$jol_status} 
											, '{$jol_type}' 
											) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$jol_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			
			$cmplog->add_log("journal-add" , "เพิ่มวารสารสหกรณ์ " , $jol_id ) ;
		}
		else
		{	
			$sql_jol_pdf = !empty($jol_pdf) ? ", jol_pdf = '{$jol_pdf}' "  : "" ;
			$sql_jol_picture = !empty($jol_picture)  ? ", jol_picture = '{$jol_picture}' "  : "" ;
			
 			$sql = "UPDATE cmp_journal SET jol_title = '{$jol_title}' 
												{$sql_jol_pdf}
												{$sql_jol_picture}
												, publicdate = '{$publicdate}'  
 												, updatedate = NOW() 
												, jol_status = {$jol_status}
												, jol_type = '{$jol_type}'
												WHERE 
												jol_id = {$jol_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("journal-edit" , "แก้ไขวารสารสหกรณ์ " , $jol_id ) ;
		}		 
		 
		echo "<script> window.location.href = \"?type={$jol_type}\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("journal"))  )
	{
		$jol_id = (int) @$_GET["jid"] ;  
		$jol_type = $_GET["type"] ;  
 		$sql = "DELETE FROM cmp_journal WHERE jol_id = {$jol_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("journal-delete" , "ลบวารสารสหกรณ์ " , $jol_id ) ;
		
		echo "<script> window.location.href = \"?type={$jol_type}\"</script>" ;
		exit();
	}
	
	  
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		$("#frm_journal").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	$jol_type = $_GET["type"]; 
	$type_mane = array('journal'=>'วารสารสหกรณ์','letter'=>'จดหมายข่าวสหกรณ์');	
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$jol_id = (int) @$_GET["jid"] ; 
			$sql = "SELECT * FROM cmp_journal WHERE jol_id = {$jol_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", $type_mane[$jol_type] );
		$smarty->assign("page_title", $type_mane[$jol_type] );
 		$smarty->assign("breadcrumb", array($type_mane[$jol_type] => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$jol_id = (int) @$row["jol_id"]  ; 
		
		
		$publicdate = @$row["publicdate"] ; 
		$publicdate = empty($publicdate) ? date("Y-m-d H:i:s") : $publicdate ; 
		$publicdatee_time = strtotime($publicdate);
		
		$publicdate_time_date =  date("Y-m-d" , $publicdatee_time ) ; 
		$publicdate_time_hour = (int) date("H" , $publicdatee_time ) ; 
		$publicdate_time_min = (int) date("i" , $publicdatee_time ) ; 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title"><?php echo $type_mane[$jol_type];?></h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_journal" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="jol_id" value="<?php echo $jol_id ;  ?>"/>
					  <input type="hidden" name="jol_type" value="<?php echo $jol_type ;  ?>"/>
  						
						<div class="form-group">
							<label for="jol_title" class="col-sm-3 control-label">ชื่อวารสาร</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="jol_title" name="jol_title" placeholder="" value="<?php echo htmlspecialchars(@$row["jol_title"]) ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="jol_picture" class="col-sm-3 control-label">ภาพประกอบ</label>
							<div class="col-sm-9">
								<input type="file"  class="" id="jol_picture" name="jol_picture"  >
								<?php if(!empty($row["jol_picture"])){ ?>
									<br />
 									<img src="/uploads/contents/<?php echo $row["jol_picture"] ?>" style="min-width:250px"  />									
								<?php } ?>
 							</div>
						</div>
						
						<div class="form-group">
							<label for="jol_picture" class="col-sm-3 control-label">ไฟล์ pdf วารสาร </label>
							<div class="col-sm-9">
								<input type="file"  class="" id="jol_pdf" name="jol_pdf" placeholder="" value="" >
								<?php if(!empty($row["jol_pdf"])){ ?>
									<br />
 									<a target="_blank" class="btn" href="/uploads/journal/<?php echo $row["jol_pdf"] ?>"    />ดูเอกสาร</a>							
								<?php } ?>
 							</div>
						</div>
						 
						<div class="form-group">
							<label for="publicdate" class="col-sm-3 control-label">วันที่แผยแผ่</label>
							<div class="col-sm-3">
								<input type="text"  class="form-control date-picker" id="publicdate" name="publicdate" placeholder="" value="<?php echo $publicdate_time_date  ?>" >
 							</div>
							<div class="col-sm-4" >
								<select class="form-control" id="publicdate_time_hour" name="publicdate_time_hour" style="width:45%;float:left" >
								   <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 24 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_hour == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
								<div style="float:left; padding: 4px;" > : </div>
								<select class="form-control" id="publicdate_time_min" name="publicdate_time_min" style="width:45%;float:left"  >
								   <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 60 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_min == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						
						 
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="jol_status" name="jol_status" <?php echo $row["jol_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?type=<?php echo $jol_type;?>" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", $type_mane[$jol_type] );
		$smarty->assign("page_title", $type_mane[$jol_type] );
		$smarty->assign("breadcrumb", array($type_mane[$jol_type] => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(jol_id) as _c FROM cmp_journal WHERE jol_type = '{$jol_type}'" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_journal WHERE jol_type = '{$jol_type}' ORDER BY createdate DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title"><?php echo $type_mane[$jol_type];?></h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add&type=<?php echo $jol_type;?>"  >เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="100" style="text-align:center" >ลำดับ</th>
						<th width="225" style="text-align:center" > วันที่แสดง  </th>
						<th width="" style="text-align:center" > ชื่อ<?php echo $type_mane[$jol_type];?></th>
						<th width="150" style="text-align:center" >จำนวนดาวน์โหลด</th>
						<th width="150" style="text-align:center" >จัดการ</th>
						
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
						<td style="text-align:center" ><?php echo $i ++  ?></td>
						<td><?php echo @mysqldate2thaidate(@$row["publicdate"] , "long" , true )  ?></td>
						<td><?php echo @$row["jol_title"] ?></td>
						<td style="text-align:center"  ><?php echo @$row["jol_count"] ?></td>
  						<td style="text-align:center" >
							<a href="?in=journal&do=edit&jid=<?php echo @$row["jol_id"] ?>&type=<?php echo $jol_type;?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=journal&do=delete&jid=<?php echo @$row["jol_id"] ?>&type=<?php echo $jol_type;?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 