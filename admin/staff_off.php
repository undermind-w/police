<?php
	session_start();
	require "config.inc.php" ;
 	require "session.inc.php" ;
	require "menu.inc.php" ;

	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');

	$do = isset($_POST["do"]) ? $_POST["do"] : null ;

	if($do == "cat-form-set") {
			$department_name = $mysqli->real_escape_string($_POST["department_name"]);
			$parent_id = $mysqli->real_escape_string($_POST["parent_id"]);
			if(empty($_POST["cat_id"])) {
					$department_seq = 0 ;
					$sql = "INSERT INTO cmp_departmentoff(department_name,department_seq,parent_id)
										VALUES('{$department_name}','{$department_seq}','{$parent_id}')";
					$mysqli->query($sql);
			} else {
					$sql = "UPDATE cmp_departmentoff SET
											department_name	= '{$department_name}'
									WHERE department_id = '{$_POST["cat_id"]}'";
					$mysqli->query($sql);
			}
			echo "<script>window.location.href = '?manage_cat';</script>";
			exit();
	}

	if($do == "document-form-set") {
 		$doc_id = (int) @$_POST["doc_id"];
		$admin_id = (int) @$_SESSION["ADMIN_ID"];
		$staff_name = @$mysqli->real_escape_string(@$_POST["staff_name"]) ;
		$position_name = @$mysqli->real_escape_string(@$_POST["position_name"]);
		$department_id = @$mysqli->real_escape_string(@$_POST["department_id"]);
		$boa_picture = "";
		if(!empty($_FILES["boa_picture"]["tmp_name"] )) {
			$ints = date("YmdGis") . @random_char(7);
			if($_FILES["boa_picture"]["type"] =="image/png"||$_FILES["boa_picture"]["type"] =="image/x-png"){
				$imgsn = $ints.".png";
			}elseif($_FILES["boa_picture"]["type"] =="image/gif"){
				$imgsn = $ints.".gif";
			}elseif($_FILES["boa_picture"]["type"] =="image/pjpeg"||$_FILES["boa_picture"]["type"] =="image/jpeg"){
				$imgsn = $ints.".jpg";
			}
			if(!empty($imgsn)) {
				@copy($_FILES["boa_picture"]["tmp_name"] , PATH . "/uploads/staff/{$imgsn}");
				// CHECK SIZE IMAGE
				$size = getimagesize (PATH . "/uploads/staff/{$imgsn}" ) ;
				if($size[0] > 350 ){
					$url = "http://{$_SERVER["SERVER_NAME"]}/timthumb.php?src=/uploads/staff/{$imgsn}&w=350&zc=0";
					$img = PATH . "/uploads/staff/{$imgsn}" ;
					file_put_contents($img, file_get_contents($url));
				}
				// END CHECK IMAGE
				$boa_picture = "{$imgsn}"  ;
			}
		}
		if(empty($doc_id))
		{
			$sql = "INSERT INTO staff_off

											(
												staff_name
											, staff_image
											, position_name
  											,department_id
											, admin_id
											)
											VALUES
											(
											'{$staff_name}'
											,'{$boa_picture}'
											, '{$position_name}'
											, '{$department_id}'
											, '{$admin_id}'
											) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$doc_id = (int) $mysqli->insert_id ;
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;
			}
		else
		{
			if (!empty($boa_picture)) {
					$sql_boa_picture = ", staff_image = '{$boa_picture}' ";
					$filename = $_POST["staff_image"];
					unlink(PATH . "/uploads/staff/{$filename}");
			} else {
					$sql_boa_picture = "";
			}
 			$sql = "UPDATE staff_off SET
													staff_name 	= '{$staff_name}'
													{$sql_boa_picture}
												, position_name	= '{$position_name}'
												, department_id = '{$department_id}'
 												, admin_id 	= '{$admin_id}'
												WHERE staff_id 	= {$doc_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$msg->add("s" , "! แก้ไขข้อมูลแล้ว" ) ;
		}

		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}

	if($_POST) exit();
	$do = isset($_GET["do"]) ? $_GET["do"] : null ;
	if(isset($_GET["manage_cat"]) && $do == "del") {
			$mysqli->query("DELETE FROM cmp_departmentoff WHERE department_id = '{$_GET["id"]}'");
			$mysqli->query("OPTIMIZE TABLE cmp_departmentoff");
			echo "<script>window.location.href = '?manage_cat';</script>";
			exit();
	}

	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("document"))  )
	{
		$staff_id = (int) @$_GET["did"] ;
		$filename = $_GET["img_name"];
		$session_userid =  mysql_real_escape_string($_GET["session_userid"]);
		unlink(PATH . "/uploads/staff/{$filename}");
 		$sql = "DELETE FROM staff_off WHERE staff_id  = {$staff_id} LIMIT 1 ";
		$mysqli->query($sql);
		echo $mysqli->error ;
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ;
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	// Up Down
	if(@in_array(@$_GET["do"],array("down","up"))) {
		$sql = "SELECT department_id , department_seq FROM cmp_departmentoff ORDER BY department_seq ASC";
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$i = 0;
		while(($row = $rs->fetch_assoc())) {
			if($row["department_id"] == @$_GET["sid"]) {
				$pos = $i;
			} else {
				$array[$i++] = $row["department_id"];
			}
		}
		if(@$_GET["do"]=="down") {
			$pos++;
		} else {
			$pos--;
		}
		$count = count($array)+1;
		for($i=0;$i<$count;$i++) {
			if($i == $pos) {
				$tmp[$i] = @$_GET["sid"];
			} else {
				$tmp[$i] = @$array[0];
				array_shift($array);
			}
		}
		for($i = 0 ; $i < count($tmp) ;$i++) {
			$index = $i + 1 ;
			$sql = "UPDATE cmp_departmentoff SET department_seq = '{$index}'  WHERE department_id = '{$tmp[$i]}'";
			$mysqli->query($sql);
			echo $mysqli->error;
		}
		$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;
		echo "<script type=\"text/javascript\">document.location.href='?manage_cat';</script>";
		exit();
	}

	if(@in_array(@$_GET["do"],array("down_staff","up_staff"))) {
		$did = @$_GET["did"];
		$sql = "SELECT staff_id , staff_seq , department_id FROM staff_off WHERE department_id = '{$did}' ORDER BY staff_seq ASC";
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$i = 0;
		while(($row = $rs->fetch_assoc())) {
			if($row["staff_id"] == @$_GET["sid"]) {
				$pos = $i;
			} else {
				$array[$i++] = $row["staff_id"];
			}
		}
		if(@$_GET["do"]=="down_staff") {
			$pos++;
		} else {
			$pos--;
		}
		$count = count($array)+1;
		for($i=0;$i<$count;$i++) {
			if($i == $pos) {
				$tmp[$i] = @$_GET["sid"];
			} else {
				$tmp[$i] = @$array[0];
				array_shift($array);
			}
		}
		for($i = 0 ; $i < count($tmp) ;$i++) {
			$index = $i + 1 ;
			$sql = "UPDATE staff_off SET staff_seq = '{$index}'  WHERE staff_id = '{$tmp[$i]}'";
			$mysqli->query($sql);
			echo $mysqli->error;
		}
		$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ;
		echo "<script type=\"text/javascript\">document.location.href='?cid={$did}';</script>";
		exit();
	}

	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<link href="/html/frontend/css/margin.css" rel="stylesheet" type="text/css"/>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	<style type="text/css">
			.date-picker {
					cursor: pointer;
					/* width: 250px; */
			}
			[id*="publicdate_time"] {
					cursor: pointer;
					width: 150px;
			}
 </style>
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);

	ob_start();
?>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);

	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
	$(function(){
		$("#frm_document").validate();
				$(".date-picker").datepicker({
					orientation: "top auto" ,
					autoclose: true ,
					format : "yyyy-mm-dd"
				});
	})
</script>

<script>
		$(document).ready(function() {
				$("body").on("click", ".btn-cat-submit", function() {
						$(".group-error").remove();
						if($("#cat_name").val() == "") {
								$(".group-cat").addClass("has-error");
								$("#cat_name").after("<p class='help-block text-danger group-error'>กรุณาใส่ชื่อหมวดหมู่</p>")
						} else {
								$(".group-cat").removeClass("has-error");
								$("#frm_cat").submit();
						}
				});

				$("body").on("change", "#filter", function() {
						document.location.href = "staff_off.php?cid=" + $(this).val();
				});
		});
</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 ob_start();

 ?>
 <?php
		if(isset($_GET["manage_cat"])) {
				$smarty->assign("title", "ฝ่าย" );
				$smarty->assign("page_title", "ฝ่าย" );
				$breadcrumb = array();
				$breadcrumb["เจ้าหน้าที่"] = "?";
		if(!isset($_GET["do"])) {

		$page = (int) @$_GET["page"]  ;
		$sql = "SELECT COUNT(department_id) as _c FROM cmp_departmentoff" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ;
		$per_page = 20 ;
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		$page_start = (($per_page * $page) - $per_page ) ;
		unset($sql, $rs, $row);
		//$sql = "SELECT * FROM cmp_department ORDER BY department_seq ASC LIMIT {$page_start}, {$per_page}";
		$sql = "SELECT * FROM cmp_departmentoff WHERE parent_id = '0' ORDER BY department_seq ASC";
		$rs = $mysqli->query($sql);
		$i =  1;
?>
	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">ฝ่าย</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-default pull-left" href="?">ย้อนกลับไป จัดการเจ้าหน้าที่</a>
			 <a class="btn btn-info pull-right" href="?manage_cat&do=add&parent_id=0">เพิ่มข้อมูลฝ่าย</a>
			 <div class="clearfix m-b-md"></div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="60">#</th>
						<th class="text-center" width="100">ลำดับ</th>
						<th class="text-center" width="100"></th>
						<th class="text-center"> ฝ่าย </th>
						<th class="text-center" width="200">จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php
							while(($row = $rs->fetch_assoc())) {
									if($row["department_id"] != null) {
					?>
					<tr>
							<td class="text-center"><?php echo $i;?></td>
							<td class="text-center"  colspan="2">
								<?php if($i != $rs->num_rows ){ ?>
									<a href="?do=down&sid=<?php echo $row["department_id"]; ?>"><i class="fa fa-arrow-down"></i></a>
								<?php } ?>

								<?php if($i != 1 ){ ?>
									<a href="?do=up&sid=<?php echo $row["department_id"]; ?>"><i class="fa fa-arrow-up"></i></a>
								<?php }  ?>
							</td>
							<td><?php echo $row["department_name"] ?></td>
							<td class="text-center">
									<a href="?manage_cat&do=add&parent_id=<?php echo @$row["department_id"] ?>">เพิ่มเมนูย่อย</a> |
									<a href="?manage_cat&do=edit&id=<?php echo $row["department_id"] ?>" >แก้ไข</a>
									<?php if(!$row["doc_num"]) { ?>
									|
									<a href="?manage_cat&do=del&id=<?php  echo $row["department_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้');">ลบ</a>
									<?php } ?>
							</td>
					</tr>

					<?php
						$sql2 = "SELECT * FROM  cmp_departmentoff WHERE parent_id = '{$row["department_id"]}' ORDER BY department_seq,parent_id ASC" ;
						$rs2 = $mysqli->query($sql2);
						echo $mysqli->error;
						$j = 1;
						while($row2 = $rs2->fetch_assoc()) {
							?>
							<tr>
								<td style="text-align:center"></td>
								<td style="text-align:center"><?php echo $j ?></td>
								<td style="text-align:center">
									<?php if ($j != $rs2->num_rows) { ?>
										<a href="?do=down&in=manage_cat&sid=<?php echo $row2["department_id"]; ?>"><i
													class="fa fa-arrow-down"></i></a>
									<?php } ?>

									<?php if ($j != 1) { ?>
										<a href="?do=up&in=manage_cat&sid=<?php echo $row2["department_id"]; ?>"><i
													class="fa fa-arrow-up"></i></a>
									<?php } ?>
								</td>
								<td style="text-align:left">
									<?php echo @$row2["department_name"] ?>
								</td>
								<td style="text-align:center">
									<a href="?manage_cat&do=edit&sid=<?php echo @$row2["department_id"] ?>"><i
												class="fa fa-edit"></i></a> |
									<a href="?manage_cat&do=del&id=<?php  echo @$row2["department_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้');">ลบ</a>
								</td>
							</tr>

							<?php
							$j++;
						}


					} $i++; }
				?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
<?php
				} else {
						$parent_id = $_GET["parent_id"];
						$sql_department = "SELECT department_id,department_name FROM cmp_departmentoff";
						$rs_department = $mysqli->query($sql_department);
						echo $mysqli->error ;
						$arr_department = array();
						while($row_department = $rs_department->fetch_assoc()){
							$arr_department[$row_department['department_id']] = $row_department['department_name'];
						}

						if(isset($_GET["id"])) {
								$sql = "SELECT * FROM cmp_departmentoff WHERE department_id = '{$_GET["id"]}'";
								$rs = $mysqli->query($sql);
								$row =$rs->fetch_assoc();
								$title = "แก้ไขฝ่าย";
						} else {
								$title = "เพิ่มฝ่าย";
						}
?>
<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title"><?php echo $title; ?></h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_cat" class="form-horizontal" action="" method="post"  enctype="multipart/form-data" >
						 <input type="hidden" name="parent_id" value="<?php echo $parent_id ;  ?>"/>
						<?php if(trim($parent_id) != 0){?>
							<div class="form-group">
								<label  class="col-sm-3 control-label">ชื่อฝ่ายหลัก</label>
								<label  class="col-sm-9 control-label" style="text-align: left;"><?php echo @$arr_department[$parent_id] ?></label>
							</div>
						<?php }?>
						<div class="form-group group-cat">
							<label for="doc_title" class="col-sm-3 control-label">ชื่อฝ่าย</label>
							<div class="col-sm-6">
								<input type="text"  class="form-control" id="cat_name" name="department_name" placeholder="ชื่อฝ่าย" value="<?php echo isset($row["department_name"]) ? $row["department_name"] : "" ?>" required="required" />
 							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button class="btn btn-success btn-cat-submit" type="button">บันทึกข้อมูล</button>
								<input type="hidden" name="do" 		value="cat-form-set" />
								<input type="hidden" name="cat_id" value="<?php echo isset($row["department_id"]) ? $row["department_id"] : "" ?>"/>
								<a class="btn btn-danger m-l-xxs" href="?manage_cat">ยกเลิก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>
<?php
				}
		} else {
			$smarty->assign("title", "เจ้าหน้าที่" );
			$smarty->assign("page_title", "เจ้าหน้าที่" );
			$breadcrumb = array();
			$breadcrumb["เจ้าหน้าที่่"] = "?";
 ?>
 <?php
	if(@in_array($_GET["do"] , array("add" , "edit") ) ) {
		$breadcrumb[$_GET["do"] == "add" ? "เพิ่มข้อมูล" : "แก้ไขข้อมูล"] = "#";
		if(@in_array($_GET["do"] , array("edit"))){
			$doc_id = (int) @$_GET["did"] ;
			$sql = "SELECT * FROM staff_off WHERE staff_id = {$doc_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error;
			$row = $rs->fetch_assoc() ;
		}

		$doc_id = (int) @$row["staff_id"]  ;
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">เพิ่มข้อมูล</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_document" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="doc_id" value="<?php echo $doc_id ;  ?>"/>
						<div class="form-group">
							<label for="doc_title" class="col-sm-3 control-label">ฝ่าย</label>
							<div class="col-sm-6">
									<select class="form-control" id="department_id" name="department_id">
									<?php
										$sql = "SELECT * FROM cmp_departmentoff WHERE parent_id = '0' ORDER BY department_seq,department_id";
										$rs_cat = $mysqli->query($sql);
										while(($row_cat  = $rs_cat->fetch_assoc())) {
                                            $cid = isset($row["department_id"]) ? $row["department_id"] : null ;
                                            $selected = $row_cat["department_id"] == $cid ? " selected" : "" ;
                                            $sql = "SELECT COUNT(department_id) AS C FROM cmp_departmentoff WHERE parent_id = '{$row_cat["department_id"]}' " ;
                                            $stmt3 = $mysqli->query($sql);
                                            $count3 = $stmt3->fetch_assoc();

                                            $num_rows4 = $count3["C"] ;
                                            if($num_rows4 > 0){
                                                echo "<option value='{$row_cat["department_id"]}'{$selected} style='color: #5a5a5a;' >{$row_cat["department_name"]}</option>";
                                                $sql  = "SELECT * FROM cmp_departmentoff WHERE parent_id = '{$row_cat["department_id"]}' ORDER BY department_seq,department_id" ;
												$stmt4 = $mysqli->query($sql);
                                                $j = 1 ;
												while(($row4  = $stmt4->fetch_assoc())) {
                                                    $selected = $row4["department_id"] == $cid ? " selected" : "";
                                                    $tab = "&nbsp;&nbsp;&nbsp;&nbsp;";
                                                    echo "<option value='{$row4["department_id"]}'{$selected} >{$tab}{$row4["department_name"]}</option>";
                                                }
                                            }else{
												echo "<option value='{$row_cat["department_id"]}'{$selected}>{$row_cat["department_name"]}</option>";
										    }
										}
									?>
									</select>
 							</div>
							<div class="col-sm-2">
									<a class="btn btn-info btn-block btn-manage-cat" href="?manage_cat">คลิกที่นี่เพื่อจัดการฝ่าย</a>
							</div>
						</div>

						<div class="form-group">
							<label for="doc_title" class="col-sm-3 control-label">ชื่อ - นามสกุล</label>
							<div class="col-sm-6">
								<input type="text"  class="form-control" id="doc_title" name="staff_name" placeholder="" value="<?php echo @$row["staff_name"] ?>" required="required" >
 							</div>
						</div>

						<div class="form-group">
							<label for="doc_title" class="col-sm-3 control-label"> ตำแหน่ง </label>
							<div class="col-sm-6">
								<input type="text"  class="form-control" id="position_name" name="position_name" placeholder="" value="<?php echo @$row["position_name"] ?>">
 							</div>
						</div>

						<div class="form-group">
							<label for="doc_title" class="col-sm-3 control-label"> รูปภาพ </label>
							<div class="col-sm-6">
								<input type="file"  class="form-control" id="boa_picture" name="boa_picture" >
								<?php if(!empty($row["staff_image"])){ ?>
									<br />
									<img src="/uploads/staff/<?php echo $row["staff_image"] ?>" style="min-width:250px"  />
								<?php } ?>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<input type="hidden" name="do" value="document-form-set" />
								<input type="hidden" name="staff_image" value="<?php echo $row["staff_image"] ?>">
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ยกเลิก</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	 </div>

<?php
	} else {
		$type = @$mysqli->real_escape_string($_GET["type"]) ;
		$page = (int) @$_GET["page"]  ;
		$cid = isset($_GET["cid"]) ? $_GET["cid"] : "" ;
		$where = !empty($cid) ? " WHERE cmp_departmentoff.department_id = '{$cid}' OR cmp_departmentoff.parent_id = '{$cid}' " : "" ;
		$sql = "SELECT COUNT(staff_off.staff_id) as _c FROM cmp_departmentoff INNER JOIN staff_off ON staff_off.department_id = cmp_departmentoff.department_id {$where}" ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ;
		$per_page = 50 ;
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		$page_start = (($per_page * $page) - $per_page );
		$sql = "SELECT * FROM cmp_departmentoff INNER JOIN staff_off ON staff_off.department_id = cmp_departmentoff.department_id {$where} ORDER BY staff_id ASC LIMIT {$page_start}, {$per_page}";
		$rs = $mysqli->query($sql);
		//echo $mysqli->error;
		$i =  1;
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">เจ้าหน้าที่</h4>
		</div>
			<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-left" href="?manage_cat">คลิกที่นี่เพื่อจัดการฝ่าย</a>
			 <a class="btn btn-info pull-right" href="?do=add">เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			<div class="form-inline" style="margin-bottom: 20px;">
				<div class="form-group">
						<select class="form-control" id="filter">
								<option value="">เลือกฝ่าย</option>
								<?php
										$sql = "SELECT * FROM cmp_departmentoff WHERE parent_id = '0' ORDER BY department_seq,department_id";
										$rs_cat = $mysqli->query($sql);
										while(($row_cat  = $rs_cat->fetch_assoc())) {
                                            $cid = isset($_GET["cid"]) ? $_GET["cid"] : null ;
                                            $selected = $row_cat["department_id"] == $cid ? " selected" : "" ;
                                            $sql = "SELECT COUNT(department_id) AS C FROM cmp_departmentoff WHERE parent_id = '{$row_cat["department_id"]}' " ;
                                            $stmt3 = $mysqli->query($sql);
                                            $count3 = $stmt3->fetch_assoc();

                                            $num_rows4 = $count3["C"] ;
                                            if($num_rows4 > 0){
                                                echo "<option value='{$row_cat["department_id"]}'{$selected} style='color: #5a5a5a;' >{$row_cat["department_name"]}</option>";
                                                $sql  = "SELECT * FROM cmp_departmentoff WHERE parent_id = '{$row_cat["department_id"]}' ORDER BY department_seq,department_id" ;
												$stmt4 = $mysqli->query($sql);
                                                $j = 1 ;
												while(($row4  = $stmt4->fetch_assoc())) {
                                                    $selected = $row4["department_id"] == $cid ? " selected" : "";
                                                    $tab = "&nbsp;&nbsp;&nbsp;&nbsp;";
                                                    echo "<option value='{$row4["department_id"]}'{$selected} >{$tab}{$row4["department_name"]}</option>";
                                                }
                                            }else{
												echo "<option value='{$row_cat["department_id"]}'{$selected}>{$row_cat["department_name"]}</option>";
										    }
										}
									?>
						</select>
				</div>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="50px" class="text-center">#</th>
						<?php if (!empty($cid)): ?>
						<th width="100px" class="text-center">ลำดับ</th>
						<?php endif; ?>
						<th class="text-center"> ประเภท  </th>
						<th class="text-center">ชื่อสกุล</th>
						<th class="text-center">ตำแหน่ง</th>
						<th class="text-center">รูปภาพ</th>
						<th width="120px" class="text-center">จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<?php
					while($row = $rs->fetch_assoc()){
					?>
							<td class="text-center"><?php echo $i;?></td>
							<?php if (!empty($cid)): ?>
							<td class="text-center">
								<?php if($i != $rs->num_rows ){ ?>
									<a href="?do=down_staff&sid=<?php echo $row["staff_id"];?>&did=<?php echo $row["department_id"];?>"><i class="fa fa-arrow-down"></i></a>
								<?php } ?>

								<?php if($i != 1 ){ ?>
									<a href="?do=up_staff&sid=<?php echo $row["staff_id"];?>&did=<?php echo $row["department_id"];?>"><i class="fa fa-arrow-up"></i></a>
								<?php }  ?>
							</td>
							<?php endif; ?>
							<td class="text-left"><?php echo @$row["department_name"] ?></td>
							<td><?php echo @$row["staff_name"] ?></td>
							<td><?php echo @$row["position_name"] ?></td>
							<?php if(empty($row["staff_image"] )){  ?>
							<td><img src="/timthumb.php?src=/uploads/staff/default.png&h=80&w=80" class="img-responsive" style="margin:auto"/></td>
							<?php }else{ ?>
							<td><img src="/timthumb.php?src=/uploads/staff/<?php echo $row["staff_image"] ?>&h=80&w=80" class="img-responsive" style="margin:auto"/></td>
							<?php }?>
							<td class="text-center">
									<a href="?in=document&do=edit&did=<?php echo @$row["staff_id"] ?>&img_name=<?php echo @$row["staff_image"] ?>&">แก้ไข</a> |
									<a href="?in=document&do=delete&did=<?php echo @$row["staff_id"] ?>&img_name=<?php echo @$row["staff_image"] ?>&" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; " >ลบ</a>
							</td>
					</tr>
					<?php
						$i++; }
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
<?php
				}
		}
		$content = ob_get_contents();
		ob_end_clean();
		$smarty->assign("breadcrumb", $breadcrumb);
		$smarty->assign("content", $content);
		$smarty->display(THEME.".tpl");
