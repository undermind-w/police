<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$slide_id = (int) @$_POST["slide_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
  		$slide_title = @$mysqli->real_escape_string(@$_POST["slide_title"]) ;
		$slide_url = @$mysqli->real_escape_string(@$_POST["slide_url"]) ;
		
		$startdate = @$mysqli->real_escape_string(@$_POST["startdate"]) ;
		$stopdate = @$mysqli->real_escape_string(@$_POST["stopdate"]) ;
		
		$imgsn = "" ; 
		$slide_picture = ""; 
		if(!empty($_FILES["slide_picture"]["tmp_name"] )) {
			$ints = date("YmdGis") . @random_char(7) ; 
			
			if($_FILES["slide_picture"]["type"] =="image/png"||$_FILES["slide_picture"]["type"] =="image/x-png"){
				$imgsn = $ints.".png";
			}elseif($_FILES["slide_picture"]["type"] =="image/gif"){
				$imgsn = $ints.".gif";
			}elseif($_FILES["slide_picture"]["type"] =="image/pjpeg"||$_FILES["slide_picture"]["type"] =="image/jpeg"){
				$imgsn = $ints.".jpg";
			}
			if(!empty($imgsn)) {
				@copy($_FILES["slide_picture"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
				$slide_picture = "{$imgsn}"  ;
			}
		}
		$imgsn = "" ; 
		$slide_background = ""; 
		if(!empty($_FILES["slide_background"]["tmp_name"] )) {
			$ints = date("YmdGis") . @random_char(7) ; 
			
			if($_FILES["slide_background"]["type"] =="image/png"||$_FILES["slide_background"]["type"] =="image/x-png"){
				$imgsn = $ints.".png";
			}elseif($_FILES["slide_background"]["type"] =="image/gif"){
				$imgsn = $ints.".gif";
			}elseif($_FILES["slide_background"]["type"] =="image/pjpeg"||$_FILES["slide_background"]["type"] =="image/jpeg"){
				$imgsn = $ints.".jpg";
			}
			if(!empty($imgsn)) {
				@copy($_FILES["slide_background"]["tmp_name"] , PATH . "/uploads/contents/{$imgsn}");
				$slide_background = "{$imgsn}"  ;
			}
		}
			
  		$slide_status = (int) @$_POST["slide_status"] ; 
		
		
	 
		if(empty($slide_id))
		{
			/*
			$sql = "SELECT MAX(slide_seq) as _max  FROM cmp_slide " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error  ; 
			$max = $rs->fetch_assoc();
			$slide_seq = (int) $max["_max"] + 1 ; 			
			*/
			$slide_seq = 0 ; 
			$sql = "INSERT INTO cmp_slide 
											( 
											admin_id
											, slide_seq
											, slide_title 
											, slide_picture 
											, slide_url 
											, startdate 
											, stopdate
   											, createdate
											, updatedate
											, slide_status 
											) 
											VALUES  
											(
											'{$admin_id}'
											, '{$slide_seq}'
											, '{$slide_title}'
											, '{$slide_picture}'
											, '{$slide_url}'
											, '{$startdate}'
											, '{$stopdate}'
 											, NOW() 
											, NOW() 
											, {$slide_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$slide_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("slide-add" , "เพิ่มสไลด์ "  , $slide_id    ) ;
		}
		else
		{	
			$sql_slide_picture = !empty($slide_picture) ? " ,  slide_picture = '{$slide_picture}' " : ""  ;
			$sql_slide_background = !empty($slide_background) ? " ,  slide_background = '{$slide_background}' " : ""  ;
			
 			$sql = "UPDATE cmp_slide SET slide_title = '{$slide_title}' 
												, slide_url = '{$slide_url}'  
												, startdate = '{$startdate}'  
												, stopdate = '{$stopdate}'  
												{$sql_slide_picture}
												{$sql_slide_background}
  												, updatedate = NOW() 
												, slide_status = {$slide_status}
												WHERE 
												slide_id = {$slide_id} LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("slide-edit" , "แก้ไขสไลด์ "  , $slide_id    ) ;
		}
		$sql = "SELECT slide_id FROM cmp_slide ORDER BY slide_seq ASC " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$i = 1 ; 
		while($item = $rs->fetch_assoc()){
			$sql = "UPDATE cmp_slide SET slide_seq = '{$i}'  WHERE slide_id = '{$item["slide_id"]}'";
			 $mysqli->query($sql);
			echo $mysqli->error ;
			$i ++ ; 
		}
		 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if(@in_array(@$_GET["do"],array("down","up"))) {
		$sql = "SELECT slide_id , slide_seq FROM cmp_slide ORDER BY slide_seq ASC ";
		$rs = $mysqli->query($sql);
		echo $mysqli->error ;
		$i = 0;
		while(($row = $rs->fetch_assoc())) {
			if($row["slide_id"]== @$_GET["sid"]) {
				$pos = $i;
			} else{
				$array[$i++] = $row["slide_id"];
			}
		}

		if(@$_GET["do"]=="down") {
			$pos++;
		} else {
			$pos--;
		}
			

		$count = count($array)+1;
		
		for($i=0;$i<$count;$i++) {
			if($i == $pos) {
				$tmp[$i] = @$_GET["sid"];
			} else {
				$tmp[$i] = @$array[0];
				array_shift($array);
			}
		}
		for($i = 0 ; $i<count($tmp) ;$i++) {
			$index = $i + 1 ; 
			$sql = "UPDATE cmp_slide SET slide_seq = '{$index}'  WHERE slide_id = '{$tmp[$i]}'";
			$mysqli->query($sql);
			echo $mysqli->error; 
		}
		
		$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
		echo "<script type=\"text/javascript\">document.location.href='?';</script>";
 		exit();
	}
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("slide"))  )
	{
		$slide_id = (int) @$_GET["sid"] ;  
 		$sql = "DELETE FROM cmp_slide WHERE slide_id = {$slide_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("slide-delete" , "ลบสไลด์ "  , $slide_id    ) ;
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("image"))  )
	{
		$slide_id = (int) @$_GET["sid"] ;  
 		$sql = "UPDATE cmp_slide SET slide_picture = '' WHERE slide_id = {$slide_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?in=slide&do=edit&sid={$slide_id}\"</script>" ;
		exit();
	}
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("background"))  )
	{
		$slide_id = (int) @$_GET["sid"] ;  
 		$sql = "UPDATE cmp_slide SET slide_background = '' WHERE slide_id = {$slide_id} LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		echo "<script> window.location.href = \"?in=slide&do=edit&sid={$slide_id}\"</script>" ;
		exit();
	}
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
	$(function(){
		$("#frm_banner").validate();
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$slide_id = (int) @$_GET["sid"] ; 
			$sql = "SELECT * FROM cmp_slide WHERE slide_id = {$slide_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "จัดการสไลด์" );
		$smarty->assign("page_title", "จัดการสไลด์" );
 		$smarty->assign("breadcrumb", array("จัดการสไลด์" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$slide_id = (int) @$row["slide_id"]  ; 
		
		$banner_status = (int) @$row["banner_status"]  ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">จัดการสไลด์</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_album" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
					  <input type="hidden" name="slide_id" value="<?php echo $slide_id ;  ?>"/>
  					 
						<div class="form-group">
							<label for="slide_url" class="col-sm-3 control-label">ลิงค์ภาพสไลด์</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="slide_url" name="slide_url" placeholder="" value="<?php echo @$row["slide_url"] ?>"   >
 							</div>
						</div>
 
 
						<div class="form-group">
							<label for="doc_item" class="col-sm-3 control-label">รูปภาพประกอบ</label>
  							<div class="col-sm-9">
								<input type="file" id="slide_picture" name="slide_picture"   >
								<?php if(!empty($row["slide_picture"])){ ?>
									<br />
									<img class="img-responsive" src="/uploads/contents/<?php echo $row["slide_picture"] ?>" />
									<a href="?in=image&do=delete&sid=<?php echo @$row["slide_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
								<?php } ?>
  							</div>
						</div>
						
						<div class="form-group">
							<label for="doc_item" class="col-sm-3 control-label">พื้นหลัง</label>
  							<div class="col-sm-9">
								<input type="file" id="slide_background" name="slide_background"   >
								<?php if(!empty($row["slide_background"])){ ?>
									<br />
									<img class="img-responsive" src="/uploads/contents/<?php echo $row["slide_background"] ?>" />
									<a href="?in=background&do=delete&sid=<?php echo @$row["slide_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
								<?php } ?>
  							</div>
						</div>
						
						<div class="form-group">
							<label for="startdate" class="col-sm-3 control-label">เริ่มวันที่</label>
							<div class="col-sm-3">
								<?php $startdate = !empty($row) ? date("Y-m-d" , strtotime($row["startdate"])) : date("Y-m-d") ?>
								<input type="text"  class="form-control date-picker" id="startdate" name="startdate" placeholder="" value="<?php echo  $startdate ?>"   >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="stopdate" class="col-sm-3 control-label">ถึงวันที่</label>
							<div class="col-sm-3">
								<?php $stopdate = !empty($row) ? date("Y-m-d" , strtotime($row["stopdate"])) : date("Y-m-d") ?>
								<input type="text"  class="form-control date-picker" id="stopdate" name="stopdate" placeholder="" value="<?php echo $stopdate  ?>"   >
 							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="slide_status" name="slide_status" <?php echo $row["slide_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1">  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "จัดการสไลด์" );
		$smarty->assign("page_title", "จัดการสไลด์" );
		$smarty->assign("breadcrumb", array("จัดการสไลด์" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(slide_id) as _c FROM cmp_slide " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 25 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_slide ORDER BY slide_seq ASC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  1; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">จัดการสไลด์</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  ><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
						<th class="text-center" width="100" >ลำดับ</th>
  						<th class="text-center" >รูปภาพ</th>
 						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
 					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i ?></td>
						<td style="text-align:center" > 
							<?php if($i != $rs->num_rows ){ ?>
								<a href="?do=down&in=slide&sid=<?php echo $row["slide_id"]; ?>"><i class="fa fa-arrow-down"></i></a>
							<?php } ?>
							
							<?php if($i != 1 ){ ?>
								<a href="?do=up&in=slide&sid=<?php echo $row["slide_id"]; ?>"><i class="fa fa-arrow-up"></i></a>
							<?php }  ?>
						</td>
                         <td style="text-align:center">
							<?php if(!empty($row["slide_picture"])){ ?>
							<a target="_blank" href="/uploads/contents/<?php @$row["slide_picture"] ?>">
							<img class="img-responsive" style="margin:auto" src="<?php echo !empty($row["slide_picture"]) ? "/timthumb.php?src=/uploads/contents/{$row["slide_picture"]}&w=450" : "" ;   ?>"  /></td>
							</a>
							<?php }else{ ?>
							-
							<?php } ?>
 						<td style="text-align:center" >
							<a href="?in=slide&do=edit&sid=<?php echo @$row["slide_id"] ?>" ><i class="fa fa-edit"></i></a> | 
							<a href="?in=slide&do=delete&sid=<?php echo @$row["slide_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>
						</td>
                      </tr>
					<?php 
						$i ++ ; 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 