<?php
    require "system.connect.php";

    $search = $_POST['search'] ? $_POST['search'] : null;

    $data = [ "success" => 0 , "responseText" => "ไม่พบสมาชิกคนนี้ในระบบ", 'test' => $search ];

    if (is_numeric($search)){

        $member_id = '';
        if ( strlen($search) == 1 ) {
            $member_id = '00000'.$search;
        } else if ( strlen($search) == 2 ) {
            $member_id = '0000'.$search;
        } else if ( strlen($search) == 3 ) {
            $member_id = '000'.$search;
        } else if ( strlen($search) == 4 ) {
            $member_id = '00'.$search;
        } else if ( strlen($search) == 5 ) {
            $member_id = '0'.$search;
        } else if ( strlen($search) == 6 ) {
            $member_id = $search;
        }

        $sql = "SELECT member_id FROM coop_mem_apply WHERE trim(member_id) = '{$member_id}'";       
        $rs = $mysqli_system->query($sql);
        if ( $rs->num_rows ) {
            $data = ["success" => 1 , "responseText" => ""];
            $row = $rs->fetch_assoc();
            $data['data'] = $row;
        }

    } else {

        $name = explode(' ', $search); 
        if ( sizeof($name) > 1 ) {
            $sql = "SELECT member_id FROM coop_mem_apply WHERE firstname_th = '{$name[0]}' AND lastname_th = '{$name[1]}'";
        } else {
            $sql = "SELECT member_id FROM coop_mem_apply WHERE firstname_th = '{$name[0]}' OR lastname_th = '{$name[0]}'"; 
        }      
        $rs = $mysqli_system->query($sql);
        if ( $rs->num_rows == 1 ) {
            $data = ["success" => 1 , "responseText" => ""];
            $row = $rs->fetch_assoc();
            $data['data'] = $row;
        } else if ( $rs->num_rows > 1 ) {
            $data = ["success" => 202 , "responseText" => ""];
        }

    }

    echo json_encode($data);
    exit();
?>