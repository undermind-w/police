<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
	
	if(@$_POST){
		if(@$_POST["do"] == "stats1")
		{
			$stat_id = @$mysqli->real_escape_string(@$_POST["id"]);
			$type = array("telmobile" => "โทรศัพท์มือถือ" , "addr" => "ที่อยู่ปัจจุบัน" , "addrc"  => "ที่อยู่ที่ติดต่อได้" , "tel"  => "โทรศัพท์บ้าน" ) ;
			$sql = "SELECT * FROM cmp_imp_member_data_stat WHERE stat_id = '{$stat_id}' " ;
			$rs = $mysqli->query($sql);
			$row = $rs->fetch_assoc() ; 
 			$label = @$type[@$row["stat_type"]] ; 
			$detail = @$row["stat_data"] ; 
			echo json_encode(array("type" => $label , "detail" => $detail )); 
			exit() ; 
		}
	}
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
	$year=date("Y")>2500?date("Y")-543:date("Y");
		 
	ob_start();
?>
	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
	<style>
		.table > tbody > tr > td
		, .table > tbody > tr > th
		, .table > tfoot > tr > td
		, .table > tfoot > tr > th
		, .table > thead > tr > td
		, .table > thead > tr > th
		, .table td{
			padding-left: 5px!important;
			padding-right: 5px!important;
		}
	</style>
	
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
 
<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
 
	ob_start();
 
	$sql = "SELECT DATE(create_time) as _date 
					, DAY(create_time) as _day 
					, MONTH(create_time) as _month 
					, COUNT(token) as _c 
				FROM  cmp_imp_member_login_session 
				WHERE create_time > DATE_ADD( NOW(),INTERVAL -30 DAY) GROUP BY DATE(create_time) " ;
	$rs = $mysqli->query($sql);
	echo $mysqli->error ;
	$i = 0 ; 
	$label = array(); 
	$data = array() ; 
	while($item = $rs->fetch_assoc()){
		$i ++ ; 
		
		$label[] = "[ {$i} , '{$item["_day"]}/{$item["_month"]}' ]" ; 
		$data[] = "[ {$i} , {$item["_c"]} ]" ; 
	}
	
	
	?>
	<script>
		
			
			
		var flot1 = function () {
			
			var data = [<?php  echo implode( ",", $data) ?>];
 			
			var dataset =  [
				{
					data: data ,
					color: "rgba(220,220,220,1)",
					lines: {
						show: true,
						fill: 0.2,
					},
					shadowSize: 0,
				}, {
					data: data,
					color: "#fff",
					lines: {
						show: false,
					},
					points: {
						show: true,
						fill: true,
						radius: 4,
						fillColor: "rgba(220,220,220,1)",
						lineWidth: 2
					},
					curvedLines: {
						apply: false,
					},
					shadowSize: 0
				} 
			];
			
			var ticks = [<?php echo implode("," ,  $label ) ?>];

			var plot1 = $.plot( "#flotchart1" , dataset, {
				series: {
					color: "#14D1BD",
					lines: {
						show: true,
						fill: 0.2
					},
					shadowSize: 0,
					curvedLines: {
						apply: true,
						active: true
					}
				},
				xaxis: {
					ticks: ticks,
				},
				legend: {
					show: false
				},
				grid: {
					color: "#AFAFAF",
					hoverable: true,
					borderWidth: 0,
					backgroundColor: '#FFF'
				}
			});
		} ; 
		
		var set_hegiht_panel_1 = function(){
			var h1 = $(".panel-stats-member-login").height();
			var h2 = $(".panel-stats-member-addr").height();
			var h3 = (h2 +  h1) - 46 ; 
			$(".panel-stats-admin .panel-body").height(h3) ; 
		} ; 
		
		$(function(){
			flot1() ; 
			set_hegiht_panel_1();
			
			$('#model_profile_status').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) ; 
				var modal = $(this) ;
				$.ajax({
					method: "POST",
					url: "?",
					data: { 'do' : 'stats1' , id :  $(button).data("id")  }
				}).done(function( data ) {
					var json = JSON.parse(data) 
					modal.find("#txt_type").val(json.type) ; 
 					modal.find("#txt_new").val(json.detail)  ;

				 });
 			});
		}) ; 
		
	</script>
<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
 
 
	
 	
	$smarty->assign("title", "หน้าแรก" );
	$smarty->assign("breadcrumb", array("Dashboard" => "index.php") );
	 
	 ob_start();
?>
	<div class="row">
		<!--<div class="col-md-9">
			<div class="panel panel-white">
				<div class="row">
					<div class="col-sm-12">
						<div class="visitors-chart">
							<div class="panel-heading">
								<h4 class="panel-title">สถิติผู้ใช้งานระบบ COOP ONLINE</h4>
							</div>
							<div class="panel-body">
								<div id="flotchart1"></div>
							</div>
						</div>
					</div>
					 
				</div>
			</div>
		</div>-->
		
		<div class="col-md-3">
			<!--<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">อัพเดทฐานข้อมูลเมื่อ</h3>
				</div>
				<div class="panel-body" style="    padding-bottom: 5px;">
					<div class="table-responsive" >
					<table class="table" style="    margin-bottom: 0;">
						<tbody>
						<?php 
							$sql = "SELECT cmp_admin.admin_name  
											, cmp_imp_stat.update_time 
											FROM cmp_imp_stat 
												LEFT JOIN cmp_admin ON  cmp_imp_stat.admin_id = cmp_admin.admin_id
											WHERE cmp_imp_stat.update_time <> '0000-00-00 00:00:00'
  											ORDER BY  cmp_imp_stat.update_time DESC 
											LIMIT  0 , 1 " ; 
							$rs = $mysqli->query($sql);
							echo $mysqli->error ; 
							while($item = $rs->fetch_assoc()){
						?>
						
							<tr>
								<td><?php echo @mysqldate2thaidate( $item["update_time"]  , "long" ,  true  ) ?> <br /> <strong> โดย <?php echo $item["admin_name"] ?>  </strong>  </td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
					<a class="pull-right" href="stats.import.php" >ดูทั้งหมด</a>
					</div>
				</div>
			</div>-->
			
			
			 
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<!--<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">กระทู้ล่าสุด</h3>
				</div>
				<div class="panel-body" style="min-height: ;" >
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>วันที่ </th>
									<th>กระทู้</th>
									<th>โดย</th>
  								</tr>
							</thead>
							<tbody>
								<?php
 									$sql = "SELECT  cmp_webboard_topic.craetedate
													, cmp_webboard_topic.topic_title
													, cmp_webboard_member.wmem_fullname
													FROM cmp_webboard_topic 
														LEFT JOIN cmp_webboard_member ON cmp_webboard_topic.topic_wmem_id =  cmp_webboard_member.wmem_id 
													ORDER BY cmp_webboard_topic.craetedate DESC 
													LIMIT  0 , 6 " ; 
									$rs = $mysqli->query($sql);
									echo $mysqli->error ; 
									while($item = $rs->fetch_assoc()){
								?>
								
								<tr>
									<td><?php echo mysqldate2thaidate( $item["craetedate"]  , "short" ,  true  ) ?></td>
									<td><?php echo @htmlspecialchars(@$item["topic_title"])  ?></td>
									<td><?php echo $item["wmem_fullname"] ?></td>
 								</tr>
								<?php } ?>
							</tbody>
						</table>
						<a class="pull-right" href="board_topic.php" >ดูทั้งหมด</a>
					</div>
 				</div>
			</div>-->
			
			<div class="panel panel-success panel-stats-admin">
				<div class="panel-heading">
					<h3 class="panel-title">ผู้ดูแลระบบ</h3>
				</div>
				<div class="panel-body" style="min-height: ;" >
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>ชื่อ </th>
									<th>วันที่</th>
									<th>รายละเอียด</th>
  								</tr>
							</thead>
							<tbody>
								<?php
									$log_now_table = $cmplog->get_now_table_log();
									$sql = "SELECT cmp_admin.admin_name 
													, tb1.createdate 
													, tb1.log_detail 
													FROM {$log_now_table} as tb1 LEFT JOIN  cmp_admin ON tb1.admin_id = cmp_admin.admin_id
													ORDER BY tb1.createdate DESC 
													LIMIT  0 , 11 " ; 
									$rs = $mysqli->query($sql);
									echo $mysqli->error ; 
									while($item = $rs->fetch_assoc()){
								?>
								
								<tr>
									<td><?php echo $item["admin_name"] ?></td>
									<td><?php echo mysqldate2thaidate( $item["createdate"]  , "long" ,  true  ) ?></td>
									<td><?php echo $item["log_detail"] ?></td>
 								</tr>
								<?php } ?>
							</tbody>
						</table>
						<a class="pull-right" href="logging.php" >ดูทั้งหมด</a>
					</div>
 				</div>
			</div>
			
		</div>
		<div class="col-md-6">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">ร้องเรียน/แจ้งข้อเสนอแนะ</h3>
				</div>
				<div class="panel-body" style="    padding-bottom: 5px;" >
					<div class="table-responsive">
					<table class="table"  style="    margin-bottom: 0;" >
						 
						<tbody>
						<?php 
							$sql = "SELECT * FROM cmp_contact 
											WHERE cmp_contact.createdate <> '0000-00-00 00:00:00'
  											ORDER BY  cmp_contact.createdate  DESC 
											LIMIT  0 ,  2 " ; 
							$rs = $mysqli->query($sql);
							echo $mysqli->error ; 
							while($item = $rs->fetch_assoc()){
						?>
						
							<tr>
								<td> 
									<?php echo html2text($item["contact_detail"] , 50 ) ?>
									<p style="padding-bottom:0;color:#CCC" >
										<?php echo @mysqldate2thaidate( $item["createdate"]  , "long" ,  true  ) ?> 
										<strong> โดย <?php echo $item["contact_name"] ?>  </strong>  
									</p>
								</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
					<a class="pull-right" href="contact.php" >ดูทั้งหมด</a>
					</div>
 				</div>
			</div>
					
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="model_profile_status" tabindex="-1" role="dialog" aria-labelledby="model_profile_status" >
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modelAddrStatsLabel">ดูข้อมูล</h4>
			</div>
			<div class="modal-body">
				<form>
				 <div class="form-group">
					<label for="txt_old">เปลี่ยน</label>
					<input type="text" class="form-control" id="txt_type"  value="" disabled />
				 </div>
				 <div class="form-group">
					<label for="txt_new">ข้อมูลใหม่</label>
					<textarea class="form-control" id="txt_new" disabled rows="3"> </textarea>
				  </div>
				 </form>
			</div>
		   
		</div>
	  </div>
	</div>
<?php 
	
	$content = ob_get_contents();
	ob_end_clean();
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 