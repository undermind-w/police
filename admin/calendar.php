<?php
	session_start();
	require "config.inc.php" ;
	require "session.inc.php" ;
	require "menu.inc.php" ;
 
	$cmpper->check("website") ;
	
	header("Content-Type:text/html;charset=utf-8");
	date_default_timezone_set('Asia/Bangkok');
 	 
	if($_POST)
	{
 		$cal_id = (int) @$_POST["cal_id"] ; 
		$admin_id = (int) @$_SESSION["ADMIN_ID"] ;
		
  		$cal_title = @$mysqli->real_escape_string(@$_POST["cal_title"]) ;
 		$cal_detail = @$mysqli->real_escape_string(@htmlspecialchars(@$_POST["cal_detail"], ENT_QUOTES)) ;

  		$cal_status = (int) @$_POST["cal_status"] ; 
		
		$publicdate_date = @$mysqli->real_escape_string(@$_POST["publicdate"]) ;
		 
		$publicdate_time_hour = (int) @$_POST["publicdate_time_hour"] ; 
		$publicdate_time_min = (int) @$_POST["publicdate_time_min"] ; 
		
		$publicdate_time_hour = sprintf("%02d",  $publicdate_time_hour  ) ; 
		$publicdate_time_min = sprintf("%02d",  $publicdate_time_min  ) ; 
		$publicdate  = $publicdate_date . " " . $publicdate_time_hour . ":" . $publicdate_time_min . ":00" ; 
		
	 
		if(empty($cal_id))
		{
			$sql = "INSERT INTO cmp_calendar 
											( 
											admin_id
 											, cal_title 
 											, cal_detail
											, publicdate 
   											, createdate
											, updatedate
											, cal_status 
											) 
											VALUES  
											(
											'{$admin_id}'
 											, '{$cal_title}'
 											, '{$cal_detail}'
											, '{$publicdate}'
 											, NOW() 
											, NOW() 
											, {$cal_status} ) " ;
			$mysqli->query($sql);
			echo $mysqli->error ;
			$cal_id = (int) $mysqli->insert_id ; 
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("page-add" , "เพิ่มเนื้อหาปฏิทิน "  , $cal_id    ) ;
		}
		else
		{	
  			$sql = "UPDATE cmp_calendar SET cal_title = '{$cal_title}' 
												, cal_detail = '{$cal_detail}'  
    												, updatedate = NOW() 
												, cal_status = '{$cal_status}'
												WHERE 
													cal_id = '{$cal_id}'
												LIMIT 1  " ;
			$mysqli->query($sql);
			echo $mysqli->error ;								
			$msg->add("s" , "! บันทึกข้อมูลแล้ว" ) ; 
			$cmplog->add_log("page-edit" , "แก้ไขเนื้อหาปฏิทิน"  , $cal_id    ) ;
		}
		 
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	} 
	
	if( @in_array(@$_GET["do"] , array("delete")) && @in_array(@$_GET["in"] , array("calendar"))  )
	{
		$cal_id = (int) @$_GET["cid"] ;  
 		$sql = "DELETE FROM cmp_calendar WHERE cal_id = '{$cal_id}' LIMIT 1 " ;
		$mysqli->query($sql);
		echo $mysqli->error ;
 		
		$msg->add("s" , "! ลบข้อมูลแล้ว" ) ; 
		$cmplog->add_log("page-delete" , "ลบเนื้อหาปฏิทิน"  , $cal_id    ) ;
		echo "<script> window.location.href = \"?\"</script>" ;
		exit();
	}
	
	
	 
	 
	

		 
	ob_start();
?>
	<link href="/html/meter/admin1/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css">
	

	<meta name="keywords" content="<?php echo KEYWORD; ?>" />
	<meta name="description" content="<?php echo DESC; ?>" />
 
 <?php
	$header = ob_get_contents();
	ob_end_clean();
	$smarty->assign("header", $header);
 
	ob_start();
?>
<script type="text/javascript" src="/html/meter/admin1/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

 	<script type="text/javascript" src="/scripts/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/scripts/ckeditor/adapters/jquery.js"></script>

<?php
	$scripts = ob_get_contents();
	ob_end_clean();
	$smarty->assign("scripts", $scripts);
  
	ob_start();
?>

<script>
	$(function(){
		$("#frm_calendar").validate();
		
		$(".date-picker").datepicker({
			orientation: "top auto" ,
			autoclose: true , 
			format : "yyyy-mm-dd"
		});
		
		if($("#cal_detail").length) {
			$("#cal_detail").ckeditor({ height : 640 , customConfig : '/scripts/ckeditor/config-admin.js'   });
		}
	})
</script>

<?php
	$footer = ob_get_contents();
	ob_end_clean();
	$smarty->assign("footer", $footer);
	 
	
 	if(@in_array($_GET["do"] , array("add" , "edit") ) )
	{
		
		if(@in_array($_GET["do"] , array("edit"))){
			$cal_id = (int) @$_GET["cid"] ; 
			$sql = "SELECT * FROM cmp_calendar WHERE cal_id = {$cal_id} LIMIT 0 , 1 " ;
			$rs = $mysqli->query($sql);
			echo $mysqli->error ;
			$row = $rs->fetch_assoc() ;
			$btitle = "แก้ไขข้อมูล" ;
		}else{
			$btitle = "เพิ่มข้อมูล" ;
			
		}
		
		$smarty->assign("title", "จัดกาารเนื้อหาปฏิทิน" );
		$smarty->assign("page_title", "จัดกาารเนื้อหาปฏิทิน" );
 		$smarty->assign("breadcrumb", array("จัดกาารเนื้อหาปฏิทิน" => "?" ,  "{$btitle}" => "#"  ) );
 
 
		$cal_id = (int) @$row["cal_id"]  ; 
 		$cal_status = (int) @$row["cal_status"]  ; 
		
		
		$publicdate = @$row["publicdate"] ; 
		$publicdate = empty($publicdate) ? date("Y-m-d H:i:s") : $publicdate ; 
		$publicdatee_time = strtotime($publicdate);
		
		$publicdate_time_date =  date("Y-m-d" , $publicdatee_time ) ; 
		$publicdate_time_hour = (int) date("H" , $publicdatee_time ) ; 
		$publicdate_time_min = (int) date("i" , $publicdatee_time ) ; 
	 
		
		ob_start();
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">หน้าเนื้อหา</h4>
				</div>
				<div class="panel-body">
					<?php echo $msg->display() ;  ?>
					<form id="frm_page" class="form-horizontal" action="?" method="post"  enctype="multipart/form-data" >
						<input type="hidden" name="cal_id" value="<?php echo $cal_id ;  ?>"/>
 						
						<div class="form-group">
							<label for="cal_title" class="col-sm-3 control-label">ชื่อวัน</label>
							<div class="col-sm-9">
								<input type="text"  class="form-control" id="cal_title" name="cal_title" placeholder="" value="<?php echo htmlspecialchars(@$row["cal_title"]) ?>" required="required" >
 							</div>
						</div>
						
						<div class="form-group">
							<label for="inr_percent" class="col-sm-3 control-label">เนื้อหา</label>
							<div class="col-sm-9">
								<textarea  class="form-control" id="cal_detail" name="cal_detail" ><?php echo @$row["cal_detail"] ?></textarea>
  							</div>
						</div>
						
						<div class="form-group">
							<label for="news_detail" class="col-sm-3 control-label">วันที่ </label>
							<div class="col-sm-9">
								<input type="text"  class="form-control date-picker" id="publicdate" name="publicdate" placeholder="" value="<?php echo $publicdate_time_date  ?>" style="width:150px;float:left;margin-right:5px;" >
 								<!-- 
								<select class="form-control" id="publicdate_time_hour" name="publicdate_time_hour" style="width:100px;float:left" >
								   <option value="" disabled="" selected="">กรุณาเลือกชัวโมง</option>
									<?php for($i = 0 ; $i < 24 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_hour == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
								<div style="float:left; padding-top: 7px;" > : </div>
								<select class="form-control" id="publicdate_time_min" name="publicdate_time_min" style="width:100px;float:left"  >
								   <option value="" disabled="" selected="">กรุณาเลือกนาที</option>
									<?php for($i = 0 ; $i < 60 ; $i ++   ){ ?>
									<option value="<?php echo $i ?>" <?php echo @$publicdate_time_min == $i ? "selected" :"" ;  ?> ><?php echo sprintf("%02d",  $i  ) ?></option>
									<?php } ?>
								</select>
								-->
							</div>
						</div>
						
  
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="cal_status" name="cal_status" <?php echo $row["cal_status"] == 1 || @$_GET["do"] == "add" ? "checked" : "" ; ?> value="1" />  เปิดการใช้งาน
									</label>
								</div>
							</div>
						</div>
						  
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-10">
								<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
								<a href="?" class="btn btn-danger" style="margin-left:5px;">ออก</a>
							</div>
						</div>
					</form>
				 
				</div>
			</div>
		</div>
	 </div>
 
<?php 
		$content = ob_get_contents();
		ob_end_clean();
	}
	else
	{
		$type = @$mysqli->real_escape_string($_GET["type"]) ; 
		$page = (int) @$_GET["page"]  ; 
		
		$smarty->assign("title", "จัดกาารเนื้อหาปฏิทิน" );
		$smarty->assign("page_title", "จัดกาารเนื้อหาปฏิทิน" );
		$smarty->assign("breadcrumb", array("จัดกาารเนื้อหาปฏิทิน" => "#") );
		
		ob_start();
		
		$sql = "SELECT COUNT(cal_id) as _c FROM cmp_calendar " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error ; 
		
		$count = $rs->fetch_assoc();
		$num_rows = $count["_c"] ; 
		$per_page = 10 ; 
		$page = isset($_GET["page"]) ? ((int) $_GET["page"]) : 1;
		
		$pagination = (new Pagination());
		$pagination->setCurrent($page);
		$pagination->setRPP($per_page);
		$pagination->setTotal($num_rows);
		$pagination->setCrumbs(25);
		$paging = $pagination->parse();
		
		$page_start = (($per_page * $page) - $per_page ) ; 
		
		$sql = "SELECT * FROM  cmp_calendar ORDER BY publicdate  DESC LIMIT {$page_start} , {$per_page}   " ;
		$rs = $mysqli->query($sql);
		echo $mysqli->error; 
		 
		
		
		$i =  $num_rows - ($per_page * ($page - 1 ) )  ; 
	 
?>

	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-heading clearfix">
			<h4 class="panel-title">หน้าเนื้อหา</h4>
		</div>
		<div class="panel-body">
			 <?php echo $msg->display() ;  ?>
			 <a class="btn btn-info pull-right" href="?do=add"  ><i class="fa fa-plus"></i> เพิ่มข้อมูล</a>
			 <div style="clear: both;margin-bottom:15px"></div>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center" width="50" >#</th>
						<th class="text-center" width="175" >วันที่</th>
 						<th class="text-center" >ชื่อวัน</th>
  						<th class="text-center" width="100" >จัดการ</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($row = $rs->fetch_assoc()){
					?>
					 <tr>
                        <td style="text-align:center" ><?php echo $i --  ?></td>
						<td style="text-align:center" ><?php echo mysqldate2thaidate($row["publicdate"] , "long" , false ) ;  ?></td>
						<td><a target="_blank" href="/calendar/<?php echo $row["cal_id"] ?>" ><?php echo htmlspecialchars($row["cal_title"]) ?></a></td>
  						<td style="text-align:center" >
							<a href="?in=calendar&do=edit&cid=<?php echo @$row["cal_id"] ?>" ><i class="fa fa-edit"></i></a> 
 							<a href="?in=calendar&do=delete&cid=<?php echo @$row["cal_id"] ?>" onclick="return confirm('ต้องการลบข้อมูลนี้')  ; "  ><i class="fa fa-trash-o" ></i></a>  
 						</td>
                      </tr>
					<?php 
						} 
					?>
				</tbody>
			</table>
			<?php echo $paging  ?>
		</div>
	</div>
	</div>
	</div>
 
<?php 	
		$content = ob_get_contents();
		ob_end_clean();
		
	}
	
	
	
	$smarty->assign("content", $content);
	$smarty->display(THEME.".tpl");
 